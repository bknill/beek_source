// EW communication tokens
var ew_key = 'FyZs6SYPz4OjLpxvOAje+umFbMqOXKQ/kg0GAZefy/JBu1GiDuMfEpaVuemk23NbGdlvOI/j/MI3HyOINu/2Hzn5X1tgYLFkAQouYfoBJmBBEJeFAOBZXGUCyAKVAS78rDitmCi0rXSV213oZkus31/bLE2ORzAT';
var ew_proj = 'BeekOrderly';

var ew_read_url = '';
var ew_update_url = '';
var ew_create_url = '';

var scenes_count = 1;
var scenes_min   = 1;

var fieldsetCount = 0;

var shooting_dates_calendar = null;

// this section contains hardcoded default values which supposed to come from EW side as well, but to make sure nothing fails we put it here too.
var invoice_setup = parseFloat(250).toFixed(2);
var invoice_membership = parseFloat(100).toFixed(2);
var invoice_price = 0;

var stage_setup = parseFloat(150).toFixed(2);
var stage_membership = parseFloat(99).toFixed(2);
var stage_deposit = parseFloat(100).toFixed(2);
var stage_price   = 0;

$(document).ready(function () {

	// see if we have non-default communication key token passed
	if ($.getUrlVar("o") != '') {
		ew_key = $.getUrlVar("o");
		ew_read_url = '/ewws/EWRead/.json?$genhotlink=' + ew_key + '&$genproject=' + ew_proj;
		ew_update_url = '/ewws/EWUpdate/.json?$genhotlink=' + ew_key + '&$genproject=' + ew_proj;
	} else {
		ew_create_url = '/ewws/EWCreate/.json?$genhotlink=' + ew_key + '&$genproject=' + ew_proj;
	}

	// init billing info step
	$('input[name="price_type"]:checked').parents('.price_type').find('input:disabled').removeAttr('disabled');

	// ensure switch between options
	$('input[name="price_type"]').change(function() {
		// enabled respective inputs
		$('input[name="price_type"]:checked').parents('.price_type').find('input:disabled').removeAttr('disabled');
		// disable other option
		$('input[name="price_type"]:not(:checked)').parents('.price_type').find('input:not([type="radio"])').attr('disabled', 'disabled');
	});
	// END of billing info init

	// handle disabled fields groups
	$('#billing_have_contact, #shoot_contact_different').change(function() {
		if($(this).is(':checked')) {
			$(this).parents('.disabled_group').find('input:disabled').removeAttr('disabled');
		}
		else {
			$(this).parents('.disabled_group').find('input:not([type="checkbox"])').attr('disabled', 'disabled');
		}
	});
	// END of disabled fields groups hadnling

	// make sure scenes step is hadnled properly
	$('#scenes_count').val(scenes_count);

	$('.scene_box').delegate(".addscene", 'click', function () {

		scenes_count++;

		$new_scene = $("#sceneTemplate").clone(true).removeAttr("id").attr("id", "fieldSet" + scenes_count).insertBefore("#sceneTemplate");
		$new_scene.children("p").children("input, textarea").each(function (i) {
			$(this).attr("id", $(this).attr("id").replace('tmpl-', '') + scenes_count);
			$(this).removeAttr('disabled')
		});

		// update scene ID with null by default
		$('#scene_id' + scenes_count).val('null');

		var f = $("#fieldSet" + scenes_count);
		f.html(f.html().replace("fieldSetID", "fieldSet" + scenes_count));

		$new_scene.appendTo("#mainField");
		$new_scene.show("fast");

		// hide all "Add" buttons but not the last
		$('.addscene').hide().last().show();

		// hide all "Remove" buttons and show only those that are in scenes numbered more than 1
		$('.removescene').hide();
		if (scenes_count > 1) {
			$('.removescene').last().show();
		}

		calc_price();

		$('#scenes_count').val(scenes_count);
	});

	// see if we need to READ from EW
	if (ew_read_url != '') {
		// Get existing data to prepopulate the form

		$.ajax({
			url:ew_read_url,
			success:function (data)
			{
				if (data.success)
				{
					// update customer data on the first tab
					$('#organisation').val(data.result.customer_name);
					$('#first_name').val(data.result.first_name);
					$('#last_name').val(data.result.last_name);
					$('#telephone').val(data.result.telephone);
					$('#email').val(data.result.email);
					$('#f_role').val(data.result.f_role);
					$('#address').val(data.result.address);

					// build scenes section
					$('#location').val(data.result.scene_location);

					var scenes_requested = data.result.scenes_count;
					var scenes           = JSON.parse(data.result.scenes);
					var scenes_num       = Object.size(scenes);
					var i = 1;

					// create respective number of scenes and populate with data if any
					$.each(scenes, function(scene_id, item)
					{
						$('#scene_name' + i).val(item.scene_name);
						$('#shooting_requirements' + i).val(item.shooting_requirements);
						$('#scene_id' + i).val(item.id);

						if (i < scenes_num)
						{
							i++;
							$(".addscene:last-child").click();
						}
					});

					// see if we need to create more scenes with empty fields as requested
					// identify their id's as null
					while (i < scenes_requested)
					{
						i++;
						$(".addscene:last-child").click();
					}

					// populate shoot section
					$('#time_of_day option[value="'+ data.result.shoot_time +'"]').attr('selected', 'selected');
					init_ng_cal(data.result.shoot_preffered_date);

					$('#shooting_instructions').val(data.result.shoot_instructions);

					// get invoice/stage pricing
					stage_setup = data.result.stage_setup;
					stage_membership = data.result.stage_membership;
					stage_deposit = data.result.stage_deposit;

					invoice_setup = data.result.invoice_setup;
					invoice_membership = data.result.invoice_membership;

					calc_price();
				}
				else
				{
					// add EW message
					$('.warning').append('<p>' + get_ew_error(data.errors[0].message) + '</p>');
					// block register button
					$('#registerButton').fadeOut();
					// show message
					$('.warning').fadeIn();
				}
			},
			error:function (jqXHR, textStatus, errorThrown)
			{
				// add EW message
				$('.warning').append('<p>' + get_ew_error(textStatus) + '</p>');
				// block register button
				$('#registerButton').fadeOut();
				// show message
				$('.warning').fadeIn();
			},
			dataType:"json"
		});
	}
	else
	{
		// we still need to init calendar
		setTimeout('init_ng_cal(null)', 500);
	}

	// prepopulate and calculate invoice/stage pricing
	$('#stage_setup').val(stage_setup);
	$('#stage_membership').val(stage_membership);
	$('#stage_deposit').val(stage_deposit);

	$('#invoice_setup').val(invoice_setup);
	$('#invoice_membership').val(invoice_membership);

	calc_price();
	// END of preloading data

	/*
	 number of fieldsets
	 */
	fieldsetCount = $('#formElem').children().length;

	/*
	 current position of fieldset / navigation link
	 */
	var current_step = 1;

	/*
	 sum and save the widths of each one of the fieldsets
	 set the final sum as the total width of the steps element
	 */
	var stepsWidth = 0;
	var widths = new Array();
	$('#steps .step').each(function (i) {
		var $step = $(this);
		widths[i] = stepsWidth;
		stepsWidth += $step.width();
	});
	$('#steps').width(stepsWidth);

	/*
	 to avoid problems in IE, focus the first input of the form
	 */
	$('#formElem').children(':first').find(':input:first').focus();

	/*
	 show the navigation bar
	 */
	$('#navigation').show();

	/*
	 when clicking on a navigation link
	 the form slides to the corresponding fieldset
	 */
	$('#navigation a').bind('click', function (e) {
		var $this = $(this);
		var prev = current_step;
		$this.closest('ul').find('li').removeClass('selected');
		$this.parent().addClass('selected');
		/*
		 we store the position of the link
		 in the current variable
		 */
		current_step = $this.parent().index() + 1;
		/*
		 animate / slide to the next or to the corresponding
		 fieldset. The order of the links in the navigation
		 is the order of the fieldsets.
		 Also, after sliding, we trigger the focus on the first
		 input element of the new fieldset
		 If we clicked on the last link (confirmation), then we validate
		 all the fieldsets, otherwise we validate the previous one
		 before the form slided
		 */
		$('#steps').stop().animate({
			marginLeft:'-' + widths[current_step - 1] + 'px'
		}, 500, function () {
			if (current_step == fieldsetCount)
				validateSteps();
			else
				validateStep(prev);
			$('#formElem').children(':nth-child(' + parseInt(current_step) + ')').find(':input:first').focus();
		});
		e.preventDefault();
	});

	/*
	 clicking on the tab (on the last input of each fieldset), makes the form
	 slide to the next step
	 */
	$('#formElem > fieldset').each(function () {
		var $fieldset = $(this);
		$fieldset.children(':last').find(':input').keydown(function (e) {
			if (e.which == 9) {
				$('#navigation li:nth-child(' + (parseInt(current_step) + 1) + ') a').click();
				/* force the blur for validation */
				$(this).blur();
				e.preventDefault();
			}
		});
	});

	/*
	 if there are errors don't allow the user to submit
	 */
	$('#registerButton').bind('click', function (e) {

		e.preventDefault();

		if ($('#formElem').data('errors')) {
			alert('Please correct the errors in the Form');
			return false;
		}

		if (!$('#i_agree:checked').length) {
			alert('Please accept Terms & Conditions');
			return false;
		}

		// prepare JSON object for Scenes tab fields
		var REST = {};
		var scenes = {};

		// prepare individual steps data for REST operation

		// tab 1
		REST.customer_name = $('#organisation').val();
		REST.first_name = $('#first_name').val();
		REST.last_name = $('#last_name').val();
		REST.telephone = $('#telephone').val();
		REST.email = $('#email').val();
		REST.role = $('#role').val();
		REST.address = $('#address').val();

		// tab 2
		// Collect scenes data and pack into JSON string
		$('[id^="fieldSet"]').each(function (i)
		{
			scenes[(i + 1)] = {};

			$('#fieldSet' + (i + 1))
				.find(':input')
				.not('button')
				.each(function ()
			{
				scenes[(i + 1)][$(this).attr('name')] = $(this).val();
			});

			// adding location field to each scene where it's not presented
			// being dummy here and rewrite its own value for the first iteration
			scenes[(i + 1)]['location'] = $('#location').val();
		});

		REST.scenes = JSON.stringify(scenes);

		// tab 3
		REST.shoot_time = $('#time_of_day').val();
		REST.shoot_preffered_date = $('#date1').val();
		REST.shoot_instructions = $('#shooting_instructions').val();
		REST.shoot_contact_different = $('#shoot_contact_different:checked').val();
		if ($('#shoot_contact_different').is(':checked'))
		{
			REST.shoot_full_name = $('#shooting_contact_name').val();
			REST.shoot_telephone = $('#shooting_telephone').val();
			REST.shoot_email     = $('#shooting_email').val();
		}
		else
		{
			REST.shoot_full_name = $('#first_name').val().concat(' ', $('#last_name').val());
			REST.shoot_telephone = $('#telephone').val();
			REST.shoot_email     = $('#email').val();
		}

		// tab 4
		REST.scenes_count = $('#scenes_count').val();

		REST.invoice_price = $('#invoice_price:checked').val();
		if ($('#invoice_price').is(':checked')) {
			REST.order_setup      = $('#invoice_setup').val();
			REST.order_membership = $('#invoice_membership').val();
			REST.order_total      = $('#invoice_total').val();
			REST.pricing_options  = 'Invoice';
		}

		REST.stage_price = $('#stage_price:checked').val();
		if ($('#stage_price').is(':checked')) {
			REST.order_deposit    = $('#stage_deposit').val();
			REST.order_setup      = $('#stage_setup').val();
			REST.order_membership = $('#stage_membership').val();
			REST.order_total      = $('#stage_total').val();
			REST.pricing_options  = 'Installments';
		}

		REST.billing_have_contact = $('#billing_have_contact:checked').val();
		if ($('#billing_have_contact').is(':checked'))
		{
			REST.billing_contact_name = $('#billing_contact_name').val();
			REST.billing_telephone = $('#billing_telephone').val();
			REST.billing_email = $('#billing_email').val();
		}
		else
		{
			REST.billing_contact_name = $('#first_name').val().concat(' ', $('#last_name').val());
			REST.billing_telephone    = $('#telephone').val();
			REST.billing_email        = $('#email').val();
		}

		// tab 5
		REST.i_agree = $('#i_agree:checked').val();

		// SEND AJAX POST to EW
		$.post(
				(ew_update_url != '') ? ew_update_url : ew_create_url,
				REST,
				"json"
		)
		.success(function(data) {
					if (data.success)
					{
						// redirect to success page
						location.replace('success.html');
					}
					else
					{
						// redirect to failure page
						location.replace('fail.html');
					}
				})
		.error(function() {
					// redirect to failure page
					location.replace('fail.html');
				});

		return false;
	});
});

function get_ew_error (original_ew_message) {

	var re = /[^\]]+$/gi;
	return re.exec(original_ew_message);
}

/*
 validates errors on all the fieldsets
 records if the Form has errors in $('#formElem').data()
 */
function validateSteps() {
	var FormErrors = false;
	for (var i = 1; i < fieldsetCount; ++i) {
		var error = validateStep(i);
		if (error == -1)
			FormErrors = true;
	}
	$('#formElem').data('errors', FormErrors);
}

/*
 validates one fieldset
 and returns -1 if errors found, or 1 if not
 */
function validateStep(step) {
	if (step == fieldsetCount) return;

	var error = 1;
	var hasError = false;
	$('#formElem').children(':nth-child(' + parseInt(step) + ')').find('input.required:not(:disabled), select.required').each(function () {
		var $this = $(this);
		var valueLength = jQuery.trim($this.val()).length;

		if (valueLength == '') {
			hasError = true;
			setError($this);
		}
		else
			resetError($this);
	});

	var $link = $('#navigation li:nth-child(' + parseInt(step) + ') a');
	$link.parent().find('.error,.checked').remove();

	var valclass = 'checked';
	if (hasError) {
		error = -1;
		valclass = 'error';
	}
	$('<span class="' + valclass + '"></span>').insertAfter($link);

	return error;
}

function setError($el) {
	$el.css('background-color', '#FFEDEF');
}

function resetError($el) {
	$el.css('background-color', '#FFFFFF');
}

// Method to init calendar
function init_ng_cal(default_selection)
{
	var config = {
		num_months:1,
		multi_selection:true,
		num_col:2,
		input:'date1', // the input field id
		start_date:'year - 1', // the start date (default is today)
		display_date:new Date(), // the display date (default is start_date)
		events:{
			onLoad:function () {
				// make this required field
				$('.ng_cal_input_field').addClass('required');
			}
		}
	};

	if (null != default_selection)
	{
		config.selected_date = default_selection.split(',');
	}

	// creating the calendar
	shooting_dates_calendar = new ng.Calendar(config);
}

// handler to remove scene
function removescene(id) {
	$(id).remove();

	// hide all "Add" buttons but not the last
	$('.addscene').hide().last().show();
	scenes_count--;

	calc_price();

	$('#scenes_count').val(scenes_count);
}

function calc_price() {

	invoice_price = parseFloat(scenes_count * invoice_setup) + parseFloat(scenes_count *invoice_membership);
	stage_price = parseFloat(scenes_count * stage_deposit) + parseFloat(scenes_count * stage_setup) + parseFloat(scenes_count *stage_membership);

	// reveal
	$('#invoice_setup').val(parseFloat(scenes_count * invoice_setup).toFixed(2));
	$('#invoice_membership').val(parseFloat(scenes_count *invoice_membership).toFixed(2));
	$('#invoice_total').val(invoice_price.toFixed(2));

	$('#stage_deposit').val(parseFloat(scenes_count * stage_deposit).toFixed(2));
	$('#stage_setup').val(parseFloat(scenes_count * stage_setup).toFixed(2));
	$('#stage_membership').val(parseFloat(scenes_count *stage_membership).toFixed(2));
	$('#stage_total').val(stage_price.toFixed(2));
}

/*
* Helper to get object (array) size
**/
Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};
