		$(document).tooltip({ tooltipClass: "custom-tooltip-styling" });
		$(document).ready(library());
		
		
		function library() {
			
			$.ajax({
				url: 'https://gms.beek.co/guides/library.jsonp?callback=?',
				async: false,
				callback: 'callback',
				crossDomain: true,
				contentType: 'application/json; charset=utf-8',
				type: 'GET',
				dataType: 'jsonp',
				timeout: 5000,
				success:  
		     
			function(data) {

				$.each(data, function(u, v){
				
				var string = v.title;
				var urltitle = string.replace(/ |'/g,'-');
				var id = v.id;
				var g_id = v.id;
				var desc = v.description;
				var OpenDiv;

				if (v.description == null)
				{
					var desc = v.title;
				}
				else
				{
					var length = 300;
					var desc = desc;
					var desc = desc.substring(0,length);
					var desc = desc.concat('..');
				};
				
				
					$("#container").append('<div id="item_'
						+ v.id + '" class="item guide"><img id="img_'
						+ v.id + '" src="http://cdn.beek.co/' 
						+ v.thumbName + '" title="'
						+ desc+ '" width="200" height="200" class="guide" /></div>');
						
					$("#guides").append('<div id="'
						+ v.title + '"><a href="http://beek.co/guide/'
						+ v.id + '-'
						+ urltitle +'"<img id="img_'
						+ v.title + '" alt="'
						+ urltitle +'" src="http://cdn.beek.co/' 
						+ v.thumbName + '" width="200" height="200" class="guide" /></a><p>'
						+ desc + '</p></div>');
					
					$('#item_'+ v.id + '').click(function(){
						
						clearup();
					
						$(this).addClass('large');
						$('#img_'+ v.id + '').addClass('hidden');
						$('#p_'+ v.id + '').addClass('hidden');

						$(this).append('<iframe id="iframe" src="http://beek.co/g'
							+ v.id + '"></iframe>');
						

						
						var $container = $('#container');
						$container.isotope('reLayout');
						
						setTimeout(function() {
							$('html, body').animate({
								scrollTop: $('#item_'+ v.id + '').offset().top -120
							 }, 500);
							$container.isotope('reLayout');
						},1000);
						
						var OpenDiv = v.id;
					}); 
					
					$('#img_'+ v.id + '').tooltip({
						content: function(){
						  var element = $( this );
						  var title = element.attr('title');
						  var description = '<p>' + title + '</p>';
						  return description
						}
					  });
					  $( '#img_'+ v.id + '').tooltip({ tooltipClass: "custom-tooltip-styling" });
					  $( '#img_'+ v.id + '').tooltip({ track: true });
					  $( '#img_'+ v.id + '').tooltip( "option", "position", { my: "center center-80", at: "right center" } );



					
					
					$('#item_'+ v.id + '').hover(function() {
					$('#item_'+ v.id + '').addClass('itemhover');
			          },function() {
					  $('#item_'+ v.id + '').removeClass('itemhover');
					    });
				    
					
				});	
				
				layout();
				var $container = $('#container');  
				setTimeout($container.isotope('reLayout'),2000);
				}
				
				
				
				});
	
		}
		
		function guideopen(id,target){
			var item = '#item_'+ id;
			var url = 'http://beek.co/g'+ id;
			clearup();
					
			 _gaq.push(['_trackEvent', 'guides', 'id']);


			if(isMobileBrowser())
			{
			console.log('is mobile');
				window.open(url);
				}
				
			else
				{
						console.log('isnt mobile');
						$(item).addClass('large');
						$('#img_'+ id + '').addClass('hidden');
						$('#p_'+ id + '').addClass('hidden');

						$(item).append('<iframe id="iframe" src="http://beek.co/g'
							+ id + '"></iframe>');
						
						var $container = $('#container');
						$container.isotope('reLayout');
						
						setTimeout(function() {
						$('html, body').animate({scrollTop: $(item).offset().top -100}, 2000);
						$container.isotope('reLayout');
						},500);
						
						$('#featuresarrow').addClass('move');
						var arrow = $('#featuresarrow').detach();
						$('#header').append(arrow);
						var OpenDiv = id;
				}
		}

			function layout(){
				$('#container').isotope({

				  itemSelector : '.item',
				  layoutMode: 'perfectMasonry',
				  perfectMasonry: {
					columnWidth: 100,
					rowHeight: 100
				},
				animationEngine: 'best-available',
				  animationOptions: {
					 duration: 750,
					 easing: 'linear',
					 queue: false
				   },

				sortBy : 'random',
				sortAscending : false
				});
				
			};
			

			function isMobileBrowser()
			{
				return (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));
			}
			
			function clearup() {
					if ($('#intro').length) {
						$('#intro').remove();						
					};
					if ($('#contact').length) {
						$('#contact').remove();						
					};
					if ($('#signup').length) {
						$('#signup').remove();						
					};
					if ($('#iframe').attr('id') != 'admin') {
						$('#iframe').remove();						
					};
						$('.large').removeClass('large');
						$('.hidden').removeClass('hidden');
						
					$('#container').isotope( 'reloadItems' ).isotope({ sortBy: 'original-order', sortAscending : true});
			}
			
			function scrolltop(){
					$('html, body').animate({scrollTop: $('body').offset().top -10}, 2000);	
			}
			
			function featuresarrow(){

					$('html, body').animate({scrollTop: $('#marketing').offset().top -150}, 1000,
					function(){
						$('#featuresarrow').removeClass('move');
						var arrow = $('#featuresarrow').detach();
						$('#what').append(arrow);
					});	
			}
			
			function features(){
				clearup();
				_gaq.push(['_trackEvent', 'features']);
				
				if( $('#what').hasClass('item')) {
					$('html, body').animate({scrollTop: $('#what').offset().top -0}, 2000);	
				}
				else
				{
					$('#features').find('.feature').addClass('item');
					var newItems = $('#features').html();
					$('#container')
					.isotope( 'insert', $( newItems ))
					.isotope( 'reloadItems' ).isotope({ sortBy: 'original-order', sortAscending : true});
				
					
					$('html, body').animate({scrollTop: $('#what').offset().top -300}, 2000);	
				};			 				
			}
			
			function Signup(){
				clearup();
				
				var $newItems = $('<div id="signup" class="item signup_iframe_div"><iframe id="signup_iframe" scrolling="no" class="signup_iframe" src="forms/signup.html"/></div>');

				$('#container')
				      .prepend( $newItems )
					  .isotope( 'addItems', $newItems )
					  .isotope( 'reloadItems' ).isotope({ sortBy: 'original-order', sortAscending : true});
				
				$('html, body').animate({scrollTop: $('#signup').offset().top -100}, 2000);	
				
				};
				
			function contact(){
				clearup();
				
				var $newItems = $('<div id="contact" class="item contact_iframe_div"><iframe id="contact_iframe" scrolling="no" class="contact_iframe" src="forms/contact.html"/></div>');

				$('#container')
				      .prepend( $newItems )
					  .isotope( 'addItems', $newItems )
					  .isotope( 'reloadItems' ).isotope({ sortBy: 'original-order', sortAscending : true});
				
				$('html, body').animate({scrollTop: $('#contact').offset().top -100}, 2000);	
				
				};
		
		
	 
	 			function login() {
				clearup();
				$('#header').animate({top: -114},500);
				$('#container').animate({top: 0},500);
				var $newItems = $('<div id="admin" class="item large admin"><iframe id="admin" class="admin" src="http://gms.beek.co"/></div>');
				$('#container')
				      .prepend( $newItems )
					  .isotope( 'addItems', $newItems )
					  .isotope( 'reloadItems' ).isotope({ sortBy: 'original-order', sortAscending : true});
				};
				
			
				
				function orderForm() {
				clearup();
				var $newItems = $('<div id="iframe" class="item large" style="width:100%;height:600px"><iframe id="admin" class="admin" src="http://order.beek.co"/></div>');
				$('#container')
				      .prepend( $newItems )
					  .isotope( 'addItems', $newItems )
					  .isotope( 'reloadItems' ).isotope({ sortBy: 'original-order', sortAscending : true});
				};
			