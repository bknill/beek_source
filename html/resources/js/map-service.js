function updateMarker(lat, lng, zoom, map) {
    var latlng = new google.maps.LatLng(lat, lng);
    map.cfg.center = latlng;
    map.cfg.zoom = Number(zoom);
    map.cfg.markers[0].setPosition(latlng);
}
function addMarker(event, map) {
    var latlng = new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());
    map.cfg.markers[0].setPosition(latlng);
}
//function addMarker(event, map) {
    //var latlng = new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());
    //map.cfg.markers[0].setPosition(latlng);
    //var latlng = new google.maps.LatLng(lat, lng);
    //map.cfg.markers[markers.size()-1].setPosition(latlng);
//}

function findLocation(map) {
    var searchString = document.getElementById('searchField').value;
    if (searchString == "") {
        return;
    }
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address':searchString }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var lat = results[0].geometry.location.lat();
            var lng = results[0].geometry.location.lng();
            var latlng = new google.maps.LatLng(lat, lng);
            var gmap = map.getMap();
            gmap.setCenter(latlng);
            gmap.setZoom(13);
            map.cfg.markers[0].setPosition(latlng);
        }
    });
}