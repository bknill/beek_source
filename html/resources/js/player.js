
var webApp = true;

	function start(){

        loadBeekJsFiles();

        if(guideThumb != "guide_default_thumb_1.png")
            $('#guideCoverImg').attr('src',"//d3ixl5oi39qj40.cloudfront.net/" + guideThumb);
    }

    function isIOSBrowser()
    {
        return (/iphone|ipad|ipod/i.test(navigator.userAgent.toLowerCase()));
    }

    function isMobileBrowser()
    {
        return (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));
    }

    function startMyApp()
    {
        var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false );
        if(iOS)
            launchIOSApp();

        var ua = navigator.userAgent.toLowerCase();
        var isAndroid = ua.indexOf("android") &gt; -1;
        if(isAndroid)
            launchAndroidApp();
     }

    function launchIOSApp()
    {
      document.location = 'beek://guide/#{GuideViewController.selectedGuide.id}';
      setTimeout( function()
      {
          document.location = 'itms://itunes.apple.com/us/app/beekAppId';
      }, 300);
    }

    function launchAndroidApp()
    {
      document.location = 'beek://guide/#{GuideViewController.selectedGuide.id}';
      setTimeout( function()
      {
          document.location = 'http://m.#{GuideViewController.domain}/android';
      }, 300);
    }

    
    function getIEVersion() {
        var match = navigator.userAgent.match(/(?:MSIE |Trident\/.*; rv:)(\d+)/);
        return match ? parseInt(match[1]) : undefined;
    }
	
	function isIE () {
    	return navigator.userAgent.toLowerCase().indexOf('msie') !== -1 || navigator.appVersion.toLowerCase().indexOf('trident/') > 0; 
    	  
    }

    function isIE11() {
        return !(window.ActiveXObject) && "ActiveXObject" in window;
    }

    
    function loadBeekJs()
    {
        $("#preview").hide();
        $("#guidehelpcontainer").hide();
        $("#swfcontainer").hide();
        $("#player-div").hide();
        loadBeekJsFiles();     
    }


    function trackSceneView(sceneId)
    {
        _gaq.push(['_trackPageview', guidePath + '/s' + sceneId]);
    }

    function trackEvent(category, action, label)
    {
        _gaq.push(['_trackEvent', category, action, label]);
    }

    


    function trackVisitKey(key)
    {
        _gaq.push(['_setCustomVar', 1, 'key', key, 1]);
    }
    

    
    	  
    function loadBeekJsFiles(){

    	Sid.js(["../../resources/js/three.min.js",
		    	"../../resources/js/Detector.js",
                "../../resources/js/soundjs.min.js",
                "../../resources/js/preloadjs.min.js"],
            function(){
                Sid.js("../../resources/js/beekjs/beek.min.js",
                    function(){
                        webApp = true;
                        init();
                    });
            }
        );

        Sid.css('../../resources/css/font-awesome/css/font-awesome.min.css');

    }

function removePreloader(){
    $( "#preloader" ).animate({
        left: "-100%"
    }, 1000, function() {
        $('#preloader').remove();
    });

}



//IE bug
    (function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());
