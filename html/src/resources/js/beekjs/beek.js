var container, camera, scene, renderer,box, composer, controls,dolly, manager, lastRender,displays,vrEffect;
var materials = [];
var texture_placeholder,
	isUserInteracting = false,
	target,
	previewTileCounter,
	fullscreen = false,
	_touchZoomDistanceEnd,
	_touchZoomDistanceStart = 0,
	_touchZoomDistanceEnd = 0,
	vrMode = false,
	waitingForMovement = false,
	fontsAvailable = false,
	readyToPlay = true,
	preloaderLocked = false,
	files,
	queue,
	isPaused = false,
	sleepMode,
	windowWidth = window.innerWidth,
	windowHeight = window.innerHeight;

var showHowColors = {
	grey: '#a7a9ac',
	purple: '#662d91',
	lightGrey: "#e6e8eb",
	purpleRGB: {
		R: 102, G: 45, B: 145
	},
	lightGreyRGB: {
		R: 230, G: 232, B: 235
	}
};


var soundFiles = [".mp3",".m4a",".m4u"];
var videoFiles = [".mp4"];
var imageFiles = [".jpg",".jepg",".png"];

var targetList = [];

var jsonpPrefix = 'https://gms.beek.co';
var cdnPrefix = 'https://d3ixl5oi39qj40.cloudfront.net/';//'https:' == document.location.protocol ? '//d3ixl5oi39qj40.cloudfront.net/' : '//cdn.beek.co/';

var FACES = "rludfb";
var TILES = ['0_0', '0_1', '1_0', '1_1'];

var tiles, bubbles;


WebVRConfig = {
	ROTATE_INSTRUCTIONS_DISABLED: false,
	BUFFER_SCALE: 1,
	TOUCH_PANNER_DISABLED: false
};


function init() {
	console.log('init()');

	showLoader();

	if ( ! webglAvailable() ){
		$('#preloader').append("<span class='guideTitle'><b>No WebGL!</b></span><br><br><span class='guideScene'>Please enable WebGL or use a browser that supports WebGL.</span>")
		return;
	}

/*	if(window.location.hostname == 'beekdev.co')
		jsonpPrefix = '//beekdev.co:9081';

	if(window.location.hostname == 'beeksuper')
		jsonpPrefix = '//beeksuper:9081';*/

	$(window).on("resize",onWindowResize);
	$(window).on("orientationchange",onWindowResize, false);

	createPlayer();
	initBeek();

	if(isIE()){
		createjs.FlashAudioPlugin.swfPath = jsPath + "flashaudio";
		createjs.Sound.registerPlugins([createjs.WebAudioPlugin, createjs.HTMLAudioPlugin, createjs.FlashAudioPlugin]);
	}

}


function createPlayer(){

	camera = new THREE.PerspectiveCamera(75, $(window).width() / $(window).height(), 1, 1024);

	scene = new THREE.Scene();

	target = new THREE.Vector3();

	controls = new THREE.VRControls(camera);
	controls.standing = true;

	if(isIOS() || isIE11())
	  cdnPrefix = "http://" + document.domain + "/cdn/";

	renderer = new THREE.WebGLRenderer({ antialias: isIOS() ? false : true });
	renderer.setSize( window.innerWidth ,window.innerHeight + 1);
	renderer.setPixelRatio( window.devicePixelRatio );
	document.body.appendChild(renderer.domElement);

	vrEffect = new THREE.VREffect(renderer);
	vrEffect.setSize(window.innerWidth, window.innerHeight);

	var params = {
		predistorted: true
	};
	manager = new WebVRManager(renderer, vrEffect, params);
	manager.on('modechange', onWebVRManagerModeChange);

	dolly = new THREE.Group();
	dolly.position.set( 0, 0, 0 );
	scene.add( dolly );

	camera.up.set( 0, 1, 0 );
	dolly.add( camera );
}

function initBeek() {
	console.log('initBeek()');

	if(webApp || (!webApp && !isOffline()))
		loadGuide(guideId);

	if(webApp){
		$.address.state('/');
		$.address.autoUpdate(false);
		$.address.value(document.location.pathname+document.location.search);
		$.address.autoUpdate(true);


		$.address.change(function(event) {
			var data = event.value.split('s');
			var id = parseInt( data[1] );

			var connect = event.value.split('c');
			var connectId = connect[1];

			if(currentScene && id)
				if(currentScene.id != id)
					getScene(id);
		});
	}


};

function loadGuide(guideId){
	console.log('loadGuide('+guideId);

	if(webApp || (!webApp && !isOffline())){
		$.getJSON(jsonpPrefix +'/guide/' + guideId + '/jsonp?callback=?', $.proxy(onGuideDataLoaded, this));
	}
	else
		checkForOfflineGuide(guideId);
}

function onGuideDataLoaded(data){

	console.log('onGuideDataLoaded');
	createGuide(data);

	if(gameTasks.length > 0){
		if(gameTasks[0].scene && (gameTasks[0].scene != sceneId))
			getScene(gameTasks[0].scene);
		else if(sceneId)
			getScene(sceneId);
	}else if(sceneId)
		getScene(sceneId);
	else
		getScene(guideSections[0].guideScenes[0].sceneId);

	historyScenes.push( sceneId );

	animate();

	if(connectId)
		connect();
}

function preloadMediaFiles(){

	console.log('preloadMediaFiles()');

	var ext = isMobile ? '/filesJsonPLowRes?callback=?' : '/filesJsonP?callback=?';

	$.getJSON(jsonpPrefix +'/guide/' + guideId + ext, $.proxy(function(files){

		queue = new createjs.LoadQueue();
		queue.installPlugin(createjs.Sound);
		queue.on("fileload", handleFileComplete);

		if(gameTasks.length > 0)
			queue.loadFile({id:'heartbeat',src:'resources/sounds/heartbeat.mp3',type:createjs.AbstractLoader.SOUND});

		files.forEach(function(file){

			if(currentScene.video != null && (file == currentScene.video.filename))
				return;

			var ext = file.substr(file.indexOf(".")),type;

			if(soundFiles.indexOf(ext) > -1)
				type = createjs.AbstractLoader.SOUND;
			else if(videoFiles.indexOf(ext) > -1)
				type = createjs.AbstractLoader.VIDEO;
			else if(imageFiles.indexOf(ext) > -1)
				type = createjs.AbstractLoader.IMAGE;



				queue.loadFile({id:file, src:cdnPrefix + file,type:type});
		});

		queue.setPaused(false);

		function handleFileComplete(event) {
			var item = event.item;
			var type = item.type;
			if(guideData != null)
				if(guideData.soundtrack != null && (item.id == guideData.soundtrack))
						var guidesound = createjs.Sound.play(guideData.soundtrack,{interrupt: createjs.Sound.INTERRUPT_ANY, loop:-1});
		}

	}, this));
}

function onWindowResize() {

	console.log("beek.onWindowResize");

	var w = window.innerWidth;
	var h = window.innerHeight;

	vrEffect.setSize(w,h);
	camera.aspect = w/h;

	if(window.innerWidth > window.innerHeight)
		camera.fov = 80;
	else
		camera.fov = 100;

	guide3DResize();

	camera.updateProjectionMatrix();
}

function detectHotspotClick(x,y){

	//	console.log('detectHotspotClick('+ x + "," + y+ ")" );

	isUserInteracting = true;

	var raycaster = new THREE.Raycaster();
	var mouse = new THREE.Vector2();

	mouse.x =  ( x /  window.innerWidth ) * 2 - 1;
	mouse.y = - ( y /  window.innerHeight ) * 2 + 1;

	raycaster.setFromCamera( mouse, camera );

	var intersects = raycaster.intersectObjects( targetList, true );

	if ( intersects.length > 0)
		hotspotClick(intersects[ 0 ].object);
	else
		draggingMenu = false;


}

function onDocumentMouseDown(e) {

	//console.log("onDocumentMouseDown()",e, window.innerWidth);
	draggingY = e.pageY;
	detectHotspotClick(e.clientX,e.clientY);

	if(awaitingInteraction)
		$( document ).trigger( "drag" );
}


function onDocumentMouseWheel(event) {

	if(!isIE())
		setZoom(camera.fov - event.originalEvent.wheelDeltaY * 0.05);

}


function setZoom(fov){

	camera.fov = fov;

	if(camera.fov < 30) camera.fov = 30;
	if(camera.fov > 120) camera.fov = 120

	camera.updateProjectionMatrix();

}

function tweenZoom(fov){

	var newFov = {nFov : camera.fov};
	var targetFov = {nFov : fov};


	var tweenZoom = new TWEEN.Tween( newFov ).to( targetFov, 100 ).easing(TWEEN.Easing.Cubic.Out);
	tweenZoom.onUpdate(function(){
		camera.fov = newFov.nFov;
		camera.updateProjectionMatrix();
	}).start();

}


function onDocumentTouchStart(e) {

	return;

	e.preventDefault();

	mouseControls = true;

	var event = e.originalEvent;


	if(connected && connectMode == MODE.RECEIVE)
		return;

	if (event.touches.length == 1) {
		var t=event.touches[0];
		draggingY = t.pageY;
		detectHotspotClick(t.pageX*2,t.pageY*2);
	}

	if (event.touches.length == 2) {

		var dx = event.touches[ 0 ].pageX - event.touches[ 1 ].pageX;
		var dy = event.touches[ 0 ].pageY - event.touches[ 1 ].pageY;
		_touchZoomDistanceEnd = _touchZoomDistanceStart = Math.sqrt( dx * dx + dy * dy );

	}
}

function onDocumentTouchMove(e) {

	var event = e.originalEvent;

	if (event.touches.length == 2 ) {

		var dx = event.touches[ 0 ].pageX - event.touches[ 1 ].pageX;
		var dy = event.touches[ 0 ].pageY - event.touches[ 1 ].pageY;
		_touchZoomDistanceEnd = Math.sqrt( dx * dx + dy * dy );

		var factor = _touchZoomDistanceStart / _touchZoomDistanceEnd;
		_touchZoomDistanceStart = _touchZoomDistanceEnd;
		setZoom(camera.fov * factor);

	}

	if(awaitingInteraction){
		$( document ).trigger( "drag" );
	}


}

function onDocumentTouchEnd( event ) {

	event.preventDefault();

	_touchZoomDistanceStart = _touchZoomDistanceEnd = 0;

	//	mouseControls = false;
}

zoomCamera = function () {

	if ( _state === STATE.TOUCH_ZOOM_PAN ) {

		var factor = _touchZoomDistanceStart / _touchZoomDistanceEnd;
		_touchZoomDistanceStart = _touchZoomDistanceEnd;
		_eye.multiplyScalar( factor );

	} else {

		var factor = 1.0 + ( _zoomEnd.y - _zoomStart.y ) * _this.zoomSpeed;

		if ( factor !== 1.0 && factor > 0.0 ) {

			_eye.multiplyScalar( factor );

			if ( _this.staticMoving ) {

				_zoomStart.copy( _zoomEnd );

			} else {

				_zoomStart.y += ( _zoomEnd.y - _zoomStart.y ) * this.dynamicDampingFactor;

			}

		}

	}

};


function animate(timestamp) {

	requestAnimationFrame(animate);

	update(timestamp);

}


function update(timestamp) {

	if(!readyToPlay)
		return;

	TWEEN.update();

	var delta = Math.min(timestamp - lastRender, 500);
	lastRender = timestamp;
	manager.render(scene, camera, timestamp);

	controls.update();

	if(composer)composer.render(0.1);

	if(menu)menuUpdate();

	if(isMobile && !draggingMenu && !sleepMode)
		detectHotpotInCentre();
}



function hotspotPanTiltToLatLon(pan,tilt){

	pan += 90;
	pan = pan > 0 ? pan : 360 + pan;
	pan = pan < 360 ? pan : 360 - pan;

	return { lat : tilt, lon : pan };
}

function panTiltToLatLon(pan,tilt){

	pan -= 90;
	pan = pan > 0 ? pan : 360 + pan;
	pan = pan < 360 ? pan : 360 - pan;

	return { lat : -tilt, lon : pan };
}




function launchFullscreen(element) {

	fullscreen = true;

	if(element.requestFullscreen) {
		element.requestFullscreen();
	} else if(element.mozRequestFullScreen) {
		element.mozRequestFullScreen();
	} else if(element.webkitRequestFullscreen) {
		element.webkitRequestFullscreen();
	} else if(element.msRequestFullscreen) {
		element.msRequestFullscreen();
	}


//			if(inIframe() == true)
//			{
//			    $( "#openGuide" ).toggle();
//			    $( "#openSceneInfo" ).toggle();
//			}
}


function exitFullscreen() {
	console.log('exitFullscreen()');

	fullscreen = false;
	if(document.exitFullscreen) {
		document.exitFullscreen();
	} else if(document.mozCancelFullScreen) {
		document.mozCancelFullScreen();
	} else if(document.webkitExitFullscreen) {
		document.webkitExitFullscreen();
	}


}




$(window).blur(function(){
	mainVolume = 0;
});
$(window).focus(function(){
	mainVolume = 0.5;
});


// requestAnim shim layer by Paul Irish
window.requestAnimationFrame = function(){
	return (
	window.requestAnimationFrame       ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame    ||
	window.oRequestAnimationFrame      ||
	window.msRequestAnimationFrame     ||
	function(/* function */ callback){
		window.setTimeout(callback, 1000 / 60);
	}
	);
}();

function dynamicSort(property) {
	var sortOrder = 1;
	if(property[0] === "-") {
		sortOrder = -1;
		property = property.substr(1);
	}
	return function (a,b) {
		var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
		return result * sortOrder;
	};
}


/*		function orientation(){
 if(window.innerHeight > window.innerWidth)
 return 'portrait';

 return 'landscape';
 }*/

function inIframe() {
	try {
		return window.self !== window.top;
	} catch (e) {

		iframeWidth = window.frameElement.offsetWidth,
			iframeHeight = window.frameElement.offsetHeight;

		return true;
	}
}

function showLoader(){
	console.log("Beek.showLoader()");
	$( "body" ).append(" <div class='spinner'> <div class='double-bounce1'></div> <div class='double-bounce2'></div> </div>");

}

function hideLoader(){
	console.log("Beek.hideLoader()");
	$( ".spinner" ).remove();
}


function isIOS(){
	return /iPhone|iPod/.test( navigator.userAgent );
}

function nearestPow2( aSize ){
	return Math.pow( 2, Math.round( Math.log( aSize ) / Math.log( 2 ) ) );
}



function  onWebVRManagerModeChange(mode){

	console.log(' onWebVRManagerModeChange',mode);

	onVRModeChangeUI(mode);
}


function waitForMovement(distance,callback){

	waitingForMovement = true;
	var y = camera.rotation.y;
	var x = camera.rotation.x;
	var check = setInterval(function(){

		if(!waitingForMovement)
			clearInterval(check);

		if(Math.abs(camera.rotation.y - y) > distance)
		{
			clearInterval(check);
			waitingForMovement = false;
			callback();
		}
	},100);

	return check;
}

function updateShader(brightness,contrast,instant){
	console.log('updateShader',brightness,contrast);

	var a= {b:0,c:0},t = {b:brightness,c:contrast};

	if(currentScene)
		if(currentScene.video instanceof Object ){
			a = {b: currentScene.video.brightness | 0, c: currentScene.video.contrast | 0};
			if(brightness == 0 && contrast == 0)
				t = {b:currentScene.video.brightness | 0,c:currentScene.video.contrast | 0};
		}

	if(panoShader != null){
		a.b = panoShader.uniforms[ "brightness" ].value;
		a.c = panoShader.uniforms[ "contrast" ].value;
	}

	if(instant == true && panoShader != null){
		panoShader.uniforms[ "brightness" ].value = brightness;
		panoShader.uniforms[ "contrast" ].value = contrast;
	}
 	else
		new TWEEN.Tween( a ).to( t, 1200  ).easing(TWEEN.Easing.Cubic.Out).onUpdate(function(){
			if(panoShader != null){
				panoShader.uniforms[ "brightness" ].value = a.b;
				panoShader.uniforms[ "contrast" ].value = a.c;
			}
		}).start();
}

var navU = navigator.userAgent;

// Android Mobile
var isAndroidMobile = navU.indexOf('Android') > -1 && navU.indexOf('Mozilla/5.0') > -1 && navU.indexOf('AppleWebKit') > -1;

// Apple webkit
var regExAppleWebKit = new RegExp(/AppleWebKit\/([\d.]+)/);
var resultAppleWebKitRegEx = regExAppleWebKit.exec(navU);
var appleWebKitVersion = (resultAppleWebKitRegEx === null ? null : parseFloat(regExAppleWebKit.exec(navU)[1]));

// Chrome
var regExChrome = new RegExp(/Chrome\/([\d.]+)/);
var resultChromeRegEx = regExChrome.exec(navU);
var chromeVersion = (resultChromeRegEx === null ? null : parseFloat(regExChrome.exec(navU)[1]));

// Native Android Browser
var isAndroidBrowser = isAndroidMobile && (appleWebKitVersion !== null && appleWebKitVersion < 537) || (chromeVersion !== null && chromeVersion < 37);

var isMobile = navigator.userAgent.match(/(iPad)|(iPhone)|(iPod)|(android)|(Android)|(webOS)/i);
var isTouch = (('ontouchstart' in window) || (navigator.msMaxTouchPoints > 0));

function panTiltToVector(pan,tilt,distance){

	distance = distance/4;
	var pr = THREE.Math.degToRad( pan + 90);
	var tr = THREE.Math.degToRad( tilt );

	var vector = new THREE.Vector3(distance * Math.cos(pr) * Math.cos(tr),distance * Math.sin(tr),distance * Math.sin(pr) * Math.cos(tr));

	return vector;
}

function hotspotPanTiltToLatLon(pan,tilt){

	pan += 90;
	pan = pan > 0 ? pan : 360 + pan;
	pan = pan < 360 ? pan : 360 - pan;

	return { lat : tilt, lon : pan };
}

function webglAvailable() {
	try {
		var canvas = document.createElement("canvas");
		return !!
				window.WebGLRenderingContext &&
			(canvas.getContext("webgl") ||
			canvas.getContext("experimental-webgl"));
	} catch(e) {
		return false;
	}
}

function removeFromArray(arr, item) {
	for(var i = arr.length; i--;) {
		if(arr[i] === item) {
			arr.splice(i, 1);
		}
	}
}

function waitForFontAwesome( callback ) {
	var retries = 100;

	var checkReady = function() {
		var canvas, context;
		retries -= 1;
		canvas = document.createElement('canvas');
		canvas.width = 20;
		canvas.height = 20;
		context = canvas.getContext('2d');
		context.fillStyle = 'rgba(0,0,0,1.0)';
		context.fillRect( 0, 0, 20, 20 );
		context.font = '16pt FontAwesome';
		context.textAlign = 'center';
		context.fillStyle = 'rgba(255,255,255,1.0)';
		context.fillText( '\uf0c8', 10, 18 );
		var data = context.getImageData( 2, 10, 1, 1 ).data;
		if ( data[0] !== 255 && data[1] !== 255 && data[2] !== 255 ) {
			//console.log( "FontAwesome is not yet available, retrying ..." );
			if ( retries > 0 ) {
				setTimeout( checkReady, 200 );
			}
		} else {
			//console.log( "FontAwesome is loaded" );
			if ( typeof callback === 'function' ) {
				callback();
			}
		}
	}

	checkReady();
};

function waitForWebfonts(fonts, callback) {
	var loadedFonts = 0;
	for(var i = 0, l = fonts.length; i < l; ++i) {
		(function(font) {
			var node = document.createElement('span');
			// Characters that vary significantly among different fonts
			node.innerHTML = 'giItT1WQy@!-/#';
			// Visible - so we can measure it - but not on the screen
			node.style.position      = 'absolute';
			node.style.left          = '-10000px';
			node.style.top           = '-10000px';
			// Large font size makes even subtle changes obvious
			node.style.fontSize      = '300px';
			// Reset any font properties
			node.style.fontFamily    = 'sans-serif';
			node.style.fontVariant   = 'normal';
			node.style.fontStyle     = 'normal';
			node.style.fontWeight    = 'normal';
			node.style.letterSpacing = '0';
			document.body.appendChild(node);

			// Remember width with no applied web font
			var width = node.offsetWidth;

			node.style.fontFamily = font + ', sans-serif';

			var interval;
			function checkFont() {
				// Compare current width with original width
				if(node && node.offsetWidth != width) {
					++loadedFonts;
					node.parentNode.removeChild(node);
					node = null;
				}

				// If all fonts have been loaded
				if(loadedFonts >= fonts.length) {
					if(interval) {
						clearInterval(interval);
					}
					if(loadedFonts == fonts.length) {
						callback();
						return true;
					}
				}
			};

			if(!checkFont()) {
				interval = setInterval(checkFont, 50);
			}
		})(fonts[i]);
	}
};

function iosDebug(message){
//	$( "#iOSDebug" ).append('<br/><span>' + message.toString() + '</span>');
}