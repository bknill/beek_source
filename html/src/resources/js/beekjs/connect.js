	var interval, id, socket,request,uuid, webSocket,subSocket, object, connected = false, atmosphereLoaded = false,
	MODE = { NONE: -1, BROADCAST: 0, RECEIVE: 1}, connectMode = -1, connections = 0;
	
	function loadAtmosphereJs(){
			try {
				if (atmosphereLoaded)
					return;

				Sid.js("../../resources/js/atmosphere.js", function () {

					atmosphereLoaded = true;
					socket = $.atmosphere;
					console.log("atmosphere loaded");
					connect();

				});
			}catch(e){
				console.log(e);
			}
			
	}
		


	function connect(){
	 try {
		 if (connected)
			 return;

		 if (!atmosphereLoaded) {
			 loadAtmosphereJs();
			 return;
		 }

		 console.log("connect()");

		 $("#connect").off('click', connect).on('click', closeConnection);


		 $("#connect i").toggleClass('fa-phone fa-phone-square');

		 id = connectId || makeId();

		 var url = "beek.co:9081/websocket/" + id;

		 request = new $.atmosphere.AtmosphereRequest();
		 request.url = "http://" + url;
		 request.webSocketUrl = "ws://" + url;
		 request.contentType = "application/json";
		 request.transport = 'websocket';
		 request.fallbackTransport = 'long-polling';

		 uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
			 var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
			 return v.toString(16);
		 });

		 request.onOpen = function (response) {

			 if (!connectId) {

				 connectMode = MODE.BROADCAST;

				 interval = window.setInterval(sendUpdate, 100);

				 var link = "beek.co/g" + guideId + "/s" + sceneId + "/c" + id;

				 $("body").append('<div id="connectInfo" class="connectInfo">' +
					 '<span>Send this link to share views<br/></span>' +
					 '<a>' + link + '</a>' +
					 '<br/><div id="connectionsCount"><span>0 connections</span></div></div>');
			 }
			 else {
				 $("body").append('<div id="connectInfo" class="connectInfo" >' +
					 '<span>Controlled by the sender.<br/> <a href="#" onclick="closeConnection()">Click to take control</a><br/>');


				 connectMode = MODE.RECEIVE;
				 sendUpdate();
			 }


			 console.log("atmosphere connected");
			 connected = true;

		 };

		 //if(connectId)
		 request.onMessage = function (response) {
			 onMessage(response);
		 };


		 request.onReconnect = function (request, response) {
			 console.log("atmosphere reconnecting");
		 };


		 request.onError = function (response) {
			 console.log("atmosphereError");
		 };


		 subSocket = socket.subscribe(request);
	 }catch(e){
		 console.log(e);
	 }
	}
	
	function closeConnection()
	{
		
		 $("#connect i").toggleClass('fa-phone fa-phone-square');
		 subSocket.disconnect();
    	 $("#connectInfo").remove();
    	 connectMode = MODE.NONE;
		  $( "#connect" ).off('click',closeConnection);
		   $( "#connect" ).on('click',connect);
		    connected = false;
	}
	
	function makeId()
	{
	    var text = "";
	    var possible = "ABDEFHIJKLMNOPQRTUVWXYZabdefhijklmnopqrtuvwxyz0123456789";

	    for( var i=0; i < 5; i++ )
	        text += possible.charAt(Math.floor(Math.random() * possible.length));

	    return text;
	}
	
	function sendUpdate()
	{
		object = new Object;
		object.sceneId = sceneId;
		object.guideId = guideId;
		object.lon = lon;
		object.lat = lat;
		object.fov = camera.fov;
		object.uuid = uuid;
		
		subSocket.push(JSON.stringify(object));
	}
	
	function onMessage(response)
	{	

		
		
        var message = response.responseBody.split('|')[1];
        
        try {
            var json = JSON.parse(message);
        } catch (e) {
            console.log('Error: ', e, message);
            return;
        }
        
        if(json)
        	if(uuid != json.uuid)
        	if(connectMode == MODE.RECEIVE)
        		processObject( json );
        	else
        		notifyConnection();
	 }
		

	
	function processObject( object )
	{	
	
		if(!connected)
			connected = true;
		
		if(!sceneLoaded)
			return;

		remoteUpdate(object.lon, object.lat, object.fov);
		
		if(object.sceneId && object.sceneId != sceneId)
			getScene(object.sceneId);
		
	}
	
	function notifyConnection()
	{
		connections++;
		var count = parseInt(connections) + " connection";
		if(connections > 1)count += 's';
		 $( "#connectionsCount span"  ).html(count);
	}
	

	