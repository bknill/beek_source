var gameInProgress, gameTask, gamePaused = false, gameTaskHtml = null, gameTasks = [], hotspotsToHide = [], awaitingInteraction = false, showingHotspotDescription = false,lrs,userDetails,userId, waitingSound;
var scorm;
var lmsConnected = false;

var verb = {};

verb.completeTask = {
			"id": "http://beek.co/completeTask",
		"name": {
		"en-US": "completed Task"
	},
	"description": {
		"en-US": "completed task"
	},
	"definition": {
		"name": { "en-US": "completed task" }
	}
};

verb.startModule = {
	"id": "http://beek.co/startModule",
	"name": {
		"en-US": "start module"
	},
	"description": {
		"en-US": "start module"
	},
	"definition": {
		"name": { "en-US": "start module" }
	}
};

verb.completeModule = {
	"id": "http://beek.co/completeModule",
	"name": {
		"en-US": "completed Module"
	},
	"description": {
		"en-US": "completed module"
	},
	"definition": {
		"name": { "en-US": "completed module" }
	}
};

verb.failTask = {
	"id": "http://beek.co/failTask",
	"name": {
		"en-US": "failed task"
	},
	"description": {
		"en-US": "failed task"
	},
	"definition": {
		"name": { "en-US": "failedTask" }
	}
};

verb.wrongHotspot = {
	"id": "http://beek.co/wrongHotspot",
	"name": {
		"en-US": "wrong hotspot"
	},
	"description": {
		"en-US": "wrong hotspot"
	},
	"definition": {
		"name": { "en-US": "wrong hotspot" }
	}
};

verb.hotspot = {
	"id": "http://beek.co/hotspot",
	"name": {
		"en-US": "hotspot"
	},
	"description": {
		"en-US": "hotspot"
	},
	"definition": {
		"name": { "en-US": "hotspot" }
	}
};
verb.voiceRecording = {
	"id": "http://beek.co/voiceRecording",
	"name": {
		"en-US": "voice recording"
	},
	"description": {
		"en-US": "voice recording"
	},
	"definition": {
		"name": { "en-US": "voice recording" }
	}
};


function game(data){

	console.log('game()',data);

	if(!data || data.length == 0)
		return;

	gameInProgress = true;
	hotspotsToHide = [];
	gameTask = null;
	gamePaused = false;

	gameTasks = data;
	//sort data by order
	gameTasks.sort(dynamicSort("order"));

	//create list of hidden hotspots
	for (var i in gameTasks){
		if(gameTasks[i].output){

			var output = JSON.parse( gameTasks[i].output );
			if(output instanceof Array){
				for(var o in output)
					if(output[o].id)
						hotspotsToHide.push(output[o].id);
			}else if(output instanceof Object){
				for(var o in output.hotspots)
					if(output.hotspots[o].id)
						hotspotsToHide.push(output.hotspots[o].id);

			}
		}
	}


	if(lrs == null && webApp)
		Sid.js('../../resources/js/tincan-min.js', initLrs);
	else if(lrs == null)
		initLrs();

	$(document).unbind('hotspot', hotspotEvent);
	$(document).unbind('hotspot', anyHotspotEvent);
	$(document).unbind( "drag", dragEvent);
	$(document).unbind('videoTimeUpdate',onVideoTimeUpdate);
	$(document).unbind('videoFinished', onVideoComplete);
	$(document).unbind('voiceoverEnded', onVoiceoverComplete);

/*	else if(scorm == null)
		Sid.js('../../resources/js/SCORM_API_wrapper.js', initScorm);*/

	//check we're not supposed to be only showing the first scene if it's required

	if(firstRun == false || gameTasks[0].code == 'userid')
		updateGameTask(gameTasks[0]);


}

function updateGameTask( task ){
	console.log("game.updateGameTask: " + task.title);
	if(task == null){
		completeGame();
		return;
	}

	if(task.guideId != guideId)
		return;

 //TODO this breaks because guide is loaded before new scene when guide is changed
/*	if(task.scene && currentScene)
		if(task.scene != currentScene.id){
			updateGameTask(gameTasks[ gameTasks.indexOf(task) + 1]);
			return;
		}*/

	gameTask = task;
	gameTask.complete = false;
	gameTaskHtml = null;
	//set the text

	//iosDebug("task.instructions :" + task.instructions + ";");

	if(task.instructions != null && (task.instructions.length > 1)){
		gameTaskHtml = '<span>' + task.title != null ? '<h1>' + task.title + '</h1>' : null;
		gameTaskHtml += task.instructions + '</span>';
	}

	awaitingInteraction = true;

	switch(gameTask.code){
		case 'findhotspots':
		$(document).bind('hotspot', hotspotEvent);
		var input = JSON.parse( task.input );
		task.hotspotsToFind = [];
		for(var o in input)
			task.hotspotsToFind.push( input[o] );

			waitingSound = createjs.Sound.play('heartbeat');
			waitingSound.loop = 10000;

		activateForTask(task);

		gamePaused = true;

		if(gameTaskHtml != null)
			updateTextBox(gameTaskHtml);
		pause();
		break;
		case 'anyhotspot':
			$(document).bind('hotspot', anyHotspotEvent);
			var input = JSON.parse( task.input );
			task.hotspotsToFind = [];
			for(var o in input)
				task.hotspotsToFind.push( input[o] );
			activateForTask(task);

			gamePaused = true;

			if(gameTaskHtml)
				updateTextBox(gameTaskHtml);
			pause();
			break;
		case 'multichoice':
			var input = JSON.parse( task.input );

			gameTask.choices = [];

			for(var o in input)
				gameTask.choices.push( input[o] );

			var correct = gameTask.choices.filter(function(choice){
				return choice.correct == true;
			});

			if(correct.length > 1)
				gameTask.choices.push({'title':'submit',"submit" : true});

			if(gameTaskHtml && gameTask.choices)
				updateTextBox(gameTaskHtml,gameTask.choices);

			$(document).bind( "submit", onMultiChoiceSubmit);

			gamePaused = true;

			pause();
			break;
		case 'panodrag':

			if(gameTaskHtml)
				updateTextBox(gameTaskHtml);

			$(document).bind( "drag", dragEvent);
			waitForMovement(0.5,taskComplete);
			break;
		case 'recordVoice':

			if(webApp)
			{
				nextTask();
				break;
			}

			recordVoice();
			gamePaused = true;


			pause();

			break;
		case 'userdetails':
			var input = JSON.parse( task.input );
			task.details = [];
			var details = "";


			for(var o in input){
				task.details.push( input[o] );

				var detail = input[o].detail;

				if( detail == 'mbox')
					detail = 'Email';

				if( detail == 'name')
					detail = 'Name';

				details =  details + '<br><span> '+ detail + '</span><input name="userDetail_'+ input[o].order +'" id="userDetail_'+ input[o].order +'" type="text">';
			}

			$('#gameText').html(html + '<br>' + details +  '<br/><br/><a href="#" class="userDetailsSubmit"><b>Submit</b></a>');
			$('.userDetailsSubmit').bind('click', userDetailsSubmit);
			pause();

			if(gameTaskHtml)
				updateTextBox(gameTaskHtml);
			break;
		case 'userid':

			if(userId == null || userId.length < 1){
				gamePaused = true;
				requestUserId(task);
				pause();
			}
			else
				nextTask(gameTask);

			break;
		case 'watchpanovideo':

			if(task.input != null)
				$(document).bind('videoTimeUpdate',onVideoTimeUpdate);
			else
				$(document).bind('videoFinished', onVideoComplete);

			if(gameTaskHtml != null)
				updateTextBox(gameTaskHtml);

			gamePaused = false;
			play();
			break;
		case 'finishvoiceover':

			if(task.input){
				var check = setInterval(function(){
					if(voiceover){
						if(voiceover.getCurrentTime() >= Number(task.input)){
							taskComplete();
							clearInterval(check);
						}
					}
				},100);
			}
			else
				$(document).bind('voiceoverEnded', onVoiceoverComplete);

			if(gameTaskHtml)
				updateTextBox(gameTaskHtml);

			gamePaused = false;
			play();
			break;
	}

//	updateScrollText("upText",null,'fa-question',true);

	var output = JSON.parse( task.output );

	if(output instanceof Array){
		task.hotspotsToShow = [];
		for(var o in output)
			task.hotspotsToShow.push( output[o] );

		hideForTask(task);
	}
	else
	if(output instanceof Object){

		task.hotspotsToShow = [];
		for(var o in output.hotspots)
			task.hotspotsToShow.push( output.hotspots[o] );

		task.wrongSceneId = output.wrongAnswerSceneId;
		task.wrongTaskId = output.wrongAnswerTaskId;

		hideForTask(task);
	}

	if(gameTaskHtml && gameTask.code != 'userid'){
		if(!fontsAvailable)
			waitForFontAwesome(function(){waitForWebfonts(['Lato'],function(){
				fontsAvailable = true;
				//updateArrow(task);
			})});
/*		else
			updateArrow(task);*/
	}
}


function updateArrow(task){
	console.log('game.updateArrow()');

	var icon = fontAwesome["fa-question-circle"];

	switch (task.code){
		case "watchpanovideo" :
		case  "finishvoiceover":
			icon = fontAwesome["fa-exclamation-circle"];
			break;
		case "findhotspots":
			icon = fontAwesome["fa-search"];
			break;
		case "multichoice":
			icon = fontAwesome["fa-question-circle"];
			break;
	}

//	instructionArrow.updateLabel(gameTasks.indexOf(task) + 1 +	 '/' + gameTasks.length,icon);
//	instructionArrow.show();
}

function requestUserId(task){
	//iosDebug('requestUserId');

	var input = "<span class='guideScene'>" + task.instructions + "</span><br/><input id='userIdInput' class='userIdInput' type='text'/>";

	if($('#preloader').length != null){
		$('#guideIntro').append(input);
		$('#play-button').hide();
		$('.userIdInput').change(function(){
			$('#play-button').show();
			if(isIOS() == false)
				$('#play-button').on("click",submitUserId);
			else
				$('#play-button').on("touchstart",submitUserId);
		});
	}
	else
		$('body').append(input);

	function submitUserId(){
		$('#play-button').unbind("click",submitUserId,false);
		//iosDebug('submitUserId');
		userId = $('#userIdInput').val();
		$('.userIdInput').remove();
		userDetails = {};
		userDetails.mbox = "mailto:" + userId;
		nextTask();
	}

}

function userDetailsSubmit(){

	userDetails = {};

	for(var i in gameTask.details){
		var val = $( "#userDetail_" + gameTask.details[i].order).val();
		userDetails[gameTask.details[i].detail] = val;
	}
	taskComplete();

}

function onMultiChoiceSubmit(){

	$(document).unbind( "submit", onMultiChoiceSubmit);

	if(multichoiceSubmit() == true)
		taskComplete();
	else
	 	wrongAnswer();
}

function multichoiceSubmit(){

	var correct = [];
	var selected = [];
	var possibles = [];

	for(var i in gameTask.choices){

		if(gameTask.choices[i].correct)
			possibles.push(gameTask.choices[i]);

		if(gameTask.choices[i].selected)
			selected.push(gameTask.choices[i]);

		if(gameTask.choices[i].selected && gameTask.choices[i].correct)
			correct.push(gameTask.choices[i]);
	}

	if(gameTask.choices.length == 1)
		return true;

	if(correct.length == selected.length && correct.length == possibles.length)
		return true;

	return false;

}


function onVideoComplete(){
	console.log('onVideoComplete');
	$(document).unbind('videoFinished', onVideoComplete);
	taskComplete();
}

function onVoiceoverComplete(){
	console.log('onVoiceoverComplete');
	$(document).unbind('voiceoverEnded',onVoiceoverComplete);
	taskComplete();
}

function hotspotEvent(event,hotspot){

	console.log("hotspotEvent");

	if(!gameTask)
	return;

	if(waitingSound){
		waitingSound.stop();
		waitingSound = null;
	}

	var data = selectedHotspot != null
		?  selectedHotspot.hotspotData || selectedHotspot.data
		: hotspot.hotspotData || hotspot.data;


	for (var i in gameTask.hotspotsToFind)
		if(gameTask.hotspotsToFind[i].id == data.id){
			$('#gameText').html( $('#gameText').html() + '<br/><br/><span><i class="fa fa-check"></i>  ' + gameTask.hotspotsToFind[i].title + '</span>');
			gameTask.hotspotsToFind.splice( i,1 );
			break;
		}

	if(gameTask.hotspotsToFind.length < 1){

		logResult(verb.hotspot,createHotspotObject(hotspot));
		taskComplete();
	}
	else
	{
		logResult(verb.wrongHotspot,createHotspotObject(hotspot));
		wrongAnswer();
	}

	$(document).unbind('hotspot', hotspotEvent);
}

function wrongAnswer(){

	console.log('wrongAnswer()');

	logResult(verb.failTask,createObject(gameTask));



	if(gameTask.wrongSceneId != null && gameTask.wrongTaskId == null){
		getScene(gameTask.wrongSceneId);
	}
	if(gameTask.wrongTaskId != null)
	{
		gameTasks.some(function wrongAnswerCheck(task){

			if(task.id == gameTask.wrongTaskId){

				if(gameTask.wrongSceneId == null)
					updateGameTask(task);
				else{
					$(document).bind("scene_loaded",onSceneLoaded);
					getScene(gameTask.wrongSceneId);
				}

				return true;
			}

			function onSceneLoaded(){
				console.log('onSceneLoaded()');
				$(document).unbind("scene_loaded",onSceneLoaded);
				updateGameTask(task);
			}
		});

		removeAnswerboxes();


	}
}

function hotspotClosedHandler(e){
	showingHotspotDescription = false;
	//nextTask();
}

function dragEvent(event){
	console.log('dragEvent()');

	if(waitingForMovement)
		waitingForMovement = false;

	gamePaused = false;

	if(gameTask.code == "panodrag")
		setTimeout(function(){	taskComplete();play()},1000);

	$(document).unbind('drag', dragEvent);
}
function anyHotspotEvent(e,hotspot){
	$(document).unbind('hotspot', anyHotspotEvent);

	logResult(verb.hotspot,createHotspotObject(hotspot));
	taskComplete();
}

function taskComplete(){
	console.log( 'taskComplete()');

	if(gameTask.guideId != guideId)
		return;

	gameTask.complete = true;
	gamePaused = true;

	logResult(verb.completeTask, createObject(gameTask));

	if(gameTask.hotspotsToShow != null){
		showForTask(gameTask);
	}

/*	if(gameTask.feedback != null && (gameTask.length > 1)){
		pause();
		updateTextBox('<span>'+gameTask.feedback+'</span>');
		//removeAnswerboxes();
		setTimeout(next,3000,gameTask);
		//$( document ).on( "menuClosed",next);
	}
	else*/
		setTimeout(next,100,gameTask);

	function next(task){

		if(task.guideId != guideId)
			return;

		$( document ).off( "menuClosed",next);
		pause();
		if(gameTask.load_scene){
			getScene(gameTask.load_scene);
			$(document).bind('sceneLoaded', onSceneLoaded);

			function onSceneLoaded(){
				console.log('onSceneLoaded');
				gamePaused = false;
				$(document).unbind('sceneLoaded', onSceneLoaded);
				nextTask(task);
			};
		}
		else
			setTimeout(nextTask,200,task);
	}

}

function nextTask(currentTask){
	//iosDebug( 'nextTask()' );

	if(currentTask != null)
		if(currentTask.guideId != guideId)
			return;

	gamePaused = false;

	if(gameTasks.indexOf(gameTask) == gameTasks.length - 1){
		completeGame();
		return;
	}

	if(gameTask.output != null){
		var output = JSON.parse( gameTask.output );
		for(var i in hiddenHotspots)
			for(var o in output)
				if(hiddenHotspots[i].id == output[o].id)
					showHiddenHotspot( hiddenHotspots[i]);
	}

	setTimeout(updateGameTask, 500, gameTasks[ gameTasks.indexOf(gameTask) + 1] );

}

function completeGame(){
	//routine to build normal page
	console.log('completeGame()');
	gameInProgress = false;
	logResult();
	clearTextbox();
	menu.showMenuItems();
}

function createObject(gameTask){
	return {
		"id": "http://beek.co/g" + guideId + "/s" + currentScene.id + "/t"+ gameTask.id,
		"definition": {
			"name": { "taskId": gameTask.id }
		}
	}
}

function createHotspotObject(hotspot){
	return {
		"id": "http://beek.co/g" + guideId + "/s" + currentScene.id + "/h"+ hotspot.id,
		"definition": {
			"name": { "id": hotspot.id.toString() }
		}
	}
}

function createVoiceObject(gameTask,url){
	return {
		"id": "http://beek.co/g" + guideId + "/s" + currentScene.id + "/t"+ gameTask.id,
		"definition": {
			"name": { "id": url }
		}
	}
}

function initLrs(){
	console.log("initLrs()");

	if(userId){
		userDetails = {};
		userDetails.account = {
			"homePage": "http://showhow.nz",
			"name": userId
		}

	}


	try {
		lrs = new TinCan.LRS(
			{
				endpoint: "	https://cloud.scorm.com/tc/6L1HLLI0ZF/",
				username: "JN0HJKUR8q7mOiPZvAA",
				password: "wrak5RF0sLPJZ4oKdeE",
				allowFail: false
			}
		);
	}
	catch (ex) {
		console.log("Failed to setup LRS object: " + ex);
		// TODO: do something with error, can't communicate with LRS
	}
}


function logResult(verb,object){
	iosDebug('logResult');

	if(lmsConnected == true){
		scorm.set("cmi.completion_status", "completed");
		return;
	}
	else if(userDetails == null || userId == null){
		console.log('no user details');
		return;
	}

	var statement = new TinCan.Statement(
		{
			actor: userDetails,
			verb: verb,
			object: object
		}
	);

	lrs.saveStatement(
		statement,
		{
			callback: function (err, xhr) {
				if (err !== null) {
					if (xhr !== null) {
						iosDebug("Failed to save statement: " + xhr.responseText + " (" + xhr.status + ")");
						// TODO: do something with error, didn't save statement

						return;
					}

					iosDebug("Failed to save statement: " + err);
					// TODO: do something with error, didn't save statement
					return;
				}

				iosDebug("Statement saved");
				// TOOO: do something with success (possibly ignore)
			}
		}
	);
}

function initScorm(){

	scorm = pipwerks.SCORM;
	//scorm.init returns a boolean
	lmsConnected = scorm.init();
	//If the scorm.init function succeeded...
	if(lmsConnected){
		//Let's get the completion status to see if the course has already been completed
		var completionstatus = scorm.get("cmi.completion_status");
		//If the course has already been completed...
		if(completionstatus == "completed" || completionstatus == "passed"){
			//...let's display a message and close the browser window
			handleError("You have already completed this course. You do not need to continue.");
		}
		//Now let's get the username from the LMS
		var learnername = scorm.get("cmi.learner_name");
		//If the name was successfully retrieved...
		if(learnername){
			//...let's display the username in a page element named "learnername"
			//userDetails.name = learnername;
			console.log(learnername);
		}
		//If the course couldn't connect to the LMS for some reason...
	} else {
		//... let's alert the user then close the window.
		handleError("Error: Course could not connect with the LMS");
	}


	function handleError(msg){
		console.log(msg);
	}
}

function recordVoice(){

	console.log("recordVoice");
	if(gameTaskHtml != null)
		updateTextBox(gameTaskHtml,[{'title':'Start Recording','event' :'startRecording'}]);
	$(document).bind("startRecording",startRecordingVoice);

	var voiceUrl;

	function onVoiceRecordingComplete(event,url){
		console.log('onVoiceRecordingComplete',url);

		voiceUrl = url;

		$(document).unbind("voiceRecordComplete",onVoiceRecordingComplete);
		$(document).bind("reRecord",startRecordingVoice);
		$(document).bind("submitVoice",submitVoiceRecording);

		if(gameTaskHtml != null)
			updateTextBox(gameTaskHtml,[{'title':'re-record','event':'reRecord'},{'title':'submit','event':'submitVoice'}]);
	}

	function startRecordingVoice(){

		console.log('startRecording');
		$(document).unbind("startRecording",startRecordingVoice);
		$(document).unbind("reRecord",startRecordingVoice);


		if(gameTaskHtml != null)
			updateTextBox(gameTaskHtml,[{'title':'Stop Recording','event':'stopRecord'}]);

		if(webApp == false)
			appRecordAudio();

		$(document).bind("stopRecord",stopRecordingVoice);
	}

	function stopRecordingVoice(){
		console.log('stopRecordingVoice');
		$(document).bind("voiceRecordComplete",onVoiceRecordingComplete);
		$(document).unbind("stopRecord",stopRecordingVoice);

		//setTimeout(function(){$(document).trigger("voiceRecordComplete");console.log("****************")},2000);
		$(document).trigger("stopRecordingVoice");
	}

	function submitVoiceRecording(url){
		$(document).unbind("submitVoice",submitVoiceRecording);
		logResult(verb.voiceRecording,createVoiceObject(gameTask,voiceUrl));
		taskComplete();
	}

}

function onVideoTimeUpdate(event,time){

	if(time > Number(gameTask.input)){
		$(document).unbind('videoTimeUpdate',onVideoTimeUpdate);
		taskComplete();
	}

}