var guideSections = [],
	childSections = [],
	topLevelSections = [],
	guideSection,
	guideSectionOrder = [],
	guideScenes = [],
	guideSceneOrder = [],
	guideScene,
	guideData,
	infoOpen,
	guideSoundtrack,
	headerWidth;

	//this organises all the guideData 
	function createGuide( data ){
		console.log('createGuide',data);

		guideId = data.id;
        guideData = data;
		gameTasks = [];
		guideSections = [];
		guideScenes = [];
		childSections = [];
		topLevelSections = [];
		guideSectionOrder = [];
		guideSceneOrder = [];


		//add sections
		for(var section in guideData.guideSections){
			guideSections.push(guideData.guideSections[section]);
			//add scenes
			for(var scene in guideData.guideSections[section].guideScenes)
				guideScenes.push(guideData.guideSections[section].guideScenes[scene]);


			guideData.guideSections[section].guideScenes.sort(dynamicSort("order"));
		};

		guideSections.sort(dynamicSort("order"));
		
		//find child sections and check for map
		for(var cs in guideSections) {

			if (guideSections[cs].parent_section_id) {

				for (var s in guideSections)
					if (guideSections[s].id == guideSections[cs].parent_section_id) {
						if (!guideSections[s].childSections)
							guideSections[s].childSections = [];

						guideSections[s].childSections.push(guideSections[cs]);
						guideSections[cs].parent = guideSections[s];
					}
			}
			else
				topLevelSections.push(guideSections[cs]);

/*			if(guideSections[cs].title.toLowerCase().indexOf('map') > -1){
				mapSections.push(guideSections[cs]);
				loadMaps();
			}*/
		}
		
		//set child sections and scenes order
		for(var s in guideSections){
			if(guideSections[s].childSections)
				guideSections[s].childSections.sort(dynamicSort("order"));
			if(guideSections[s].guideScenes){
				guideSections[s].guideScenes.sort(dynamicSort("order"));
			}


		}

		
		//create the orders for the next button
		for(var s in guideSections){
		 if(!guideSections[s].parent_section_id){

			 guideSectionOrder.push(guideSections[s]);
			 for (var sc in guideSections[s].guideScenes)
			 guideSceneOrder.push(guideSections[s].guideScenes[sc]);

				 for (var cs in guideSections[s].childSections){
				 guideSectionOrder.push(guideSections[s].childSections[cs]);
				 for (var sc in guideSections[s].childSections[cs].guideScenes)
				 guideSceneOrder.push(guideSections[s].childSections[cs].guideScenes[sc]);

					 for (var gcs in guideSections[s].childSections[cs].childSections){
					 guideSectionOrder.push(guideSections[s].childSections[cs].childSections[gcs]);
					 for (var sc in guideSections[s].childSections[cs].childSections[gcs].guideScenes)
					 guideSceneOrder.push(guideSections[s].childSections[cs].childSections[gcs].guideScenes[sc]);

						 for (var ggcs in guideSections[s].childSections[cs].childSections[gcs].childSections){
						 guideSectionOrder.push(guideSections[s].childSections[cs].childSections[gcs].childSections[ggcs]);
						 for (var sc in guideSections[s].childSections[cs].childSections[gcs].childSections[ggcs].guideScenes)
						 guideSceneOrder.push(guideSections[s].childSections[cs].childSections[gcs].childSections[ggcs].guideScenes[sc]);

						 }
					 }

				 }
			 }
		 }



		//check to see if video player is required
		for(var sc in guideScenes)
			if(guideScenes[sc].transitionVideo)
				loadVideoPlayer();

		guideScenes.forEach(function(guideScene){

			if(guideScene.voiceover){
				if(typeof guideScene.scene.voiceover === 'string')
					try{
						guideScene.voiceover = JSON.parse(guideScene.voiceover)
					}catch(e){
						var filename = guideScene.voiceover;
						guideScene.voiceover = [];
						guideScene.push(filename);
					}
				guideScene.voiceover.forEach(function(voiceover){
					createjs.Sound.registerSound(cdnPrefix + voiceover,voiceover);
				})
			}
			else if(guideScene.scene.voiceover){
				if(typeof guideScene.scene.voiceover === 'string')
					try{
						guideScene.scene.voiceover = JSON.parse(guideScene.scene.voiceover)
					}catch(e){
						var filename = guideScene.scene.voiceover;
						guideScene.scene.voiceover = {};
						guideScene.scene.voiceover.filename = filename;
					}
				createjs.Sound.registerSound(cdnPrefix + guideScene.scene.voiceover.filename,guideScene.scene.id);
			}
		});

		createjs.Sound.addEventListener("fileload", handleFileLoad);
		function handleFileLoad(event) {
			if(currentScene)
				if(event.id == currentScene.id && voiceover)
					if(voiceover.state !== "playSucceeded")
						voiceover.play();
		}

/*		if(guideScenes[0].transitionVideo)
			firstSceneVideo = true;*/

		//set default section
		guideSection = guideSections[0];

		create3DGuide(topLevelSections.length < 2 ? guideSection.guideScenes : topLevelSections);

		if(data.gameTasks.length)
			setTimeout(game,200,data.gameTasks);
		else
			$(document).bind('videoFinished',nextScene);
	}



	//set the guideScene variable
	function updateGuideScene(id){
		console.log('updateGuideScene('+id+')' );
		
		for(var s in guideScenes)
			if(guideScenes[s].sceneId == id){
				guideScene = guideScenes[s];
				//processGuideSceneHotspots();
			}
		//if there's a video load it
		if(guideScene){
			if(guideScene.transitionVideo)
				loadVideo(guideScene.transitionVideo);

			//if there's a voiceover load it
			if(guideScene.voiceover)
				currentScene.voiceover = guideScene.voiceover;
		}


	}

	
	function processGuideSceneHotspots(){
		
		console.log('processGuideSceneHotspots()');

        for (ix in guideScene.photos) 
            photos.push(guideScene.photos[ix]); 
        
        for (ix in guideScene.posters) 
            posters.push(guideScene.posters[ix]);

        for (ix in guideScene.sounds) 
            sounds.push(guideScene.sounds[ix]); 
	}
	


	
	function cleanText( text, tags ){
		var $desc = (text)
	      
		//filter out Flash rubbish code
		    .replace(/<p.+?>/ig, tags ? "<p> " : "")
	        .replace(/<L.+?>/ig,tags ? "<li>": "")
	        .replace(/<[b|u]>/ig, " ")
	        .replace(/<\/[b|u]>/ig, " ")
	        .replace(/<TEXTFORMAT.+?>/g, " ")
	        .replace(/<\/TEXTFORMAT>/g, " ")
	        .replace(/<FONT.+?>/g, " ")
	        .replace(/<\/FONT>/g, " ")
	        .replace(/<a.+?event:(.+?)" TARGET.*?>(.*?)<\/a>/ig,tags ? "<a href=\"javascript:guideLinkClicked(\'$1\')\"  >$2</a>" : "");


		return $desc;
	}
	
	
	function guideLinkClicked(data){

		isUserInteracting = true;
		autoplaying = false;
		
		guideLink(data);
	}
	
	function guideLink(data){
		
		console.log('guideLink(' + data + ')');

		if(!data)
			return;
		
		if(data.indexOf('|') > 0){
			var look = data.split('|');
			panoLook(look[0], look[1], look[2]);
		}
		else
			hotspotLook(data);
		
		if(infoOpen){
			 var t = $('.sceneText').html().replace( data + "')",data + "')\" class=\"guideLinkSelected");
			 $('.sceneText').html(t);
			 $('#sceneText').find('h1').css({'width' : headerWidth });
			 
		}
	}
	
	


	function sceneInGuide(sceneId){
		
		for(var i in guideScenes)
			if(guideScenes[i].scene.id == sceneId)
				return true;
		
		return false;
	}
	
	function getGuideScene( sceneId ){
		
		for(var i in guideScenes)
			if(guideScenes[i].scene.id == sceneId )
				return guideScenes[i];
		
		return null;
		
	}
	
	function getGuideSection( sectionId ){
		
		for(var i in guideSections)
			if(guideSections[i].id == sectionId )
				return guideSections[i];
		
		return null;	
	}
	

	
	function volumeChange(){
		$('#volume i').toggleClass('fa-volume-off fa-volume-up');
		
		if(volume > 0){
			volume = 0;
			if(currentVideo) currentVideo.mute();
			onVRModeChangeUI(3);
		}
			
		else{
			volume = 1;
			if(currentVideo) currentVideo.unmute();
			onVRModeChangeUI(1);
		}

		updateSceneSounds( lon );
	}
	
	function zoomChange(){
		$('#zoom i').toggleClass('fa-search-plus fa-search-minus');
		
		if($('#zoom i').hasClass('fa-search-plus'))
			tweenZoom(50);
		else
			tweenZoom(20);
		
		userInteracting = true;

	}
	function getOrientation(){

		if(window.innerHeight > window.innerWidth)
			return 'portrait'
		else
			return 'landscape'
	}


	function nextScene(){
		console.log('nextScene()');
		var index = guideSceneOrder.indexOf(getGuideScene(currentScene.id));

		if(index < guideSceneOrder.length - 1)
			getScene(guideSceneOrder[index+1].scene.id);
		else
			getScene(guideSceneOrder[0].scene.id);
	}
	
	function playSoundtrack(sountrack){

		guideSoundtrack = new createjs.Sound.play(sountrack,{ loop:-1});
		if(guideSoundtrack.playState !== "playSucceeded")
			guideSoundtrack.on("fileloaded",guideSoundtrack.play);
	}