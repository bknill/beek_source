/**
 * Created by Ben on 15/10/2016.
 */

var menu,menuItemsList = [], currentMenuItems,currentText, answersList = [],textbox, menuActive = false,  draggingMenu = false, isScrolling = false;
var circleRadius = 100,
    startAngle = 180,
    startRadians,
    incrementAngle = 8,
    mpi = Math.PI/180,
    incrementRadians = incrementAngle * mpi,
    answerBoxStart,
    answerBoxRadians,
    targetCircle,
    draggingY,
    vFOV = 100 * Math.PI / 180,
    menuWidth,
    menuScale = 0.15,
    instructionArrow,
    homeButton;

function create3DGuide(menuItems){
    console.log('create3DGuide()');

    if(menu != null && menuItemsList.length > 0){
        menuItemsList.forEach(function (item) {
            menu.remove(item);
            removeFromArray(targetList,item);
        });

        menuItemsList = [];
    }
    else{

        menu = new THREE.Object3D();
       // camera.eulerOrder = "YXZ";
        menu.offsetX =  menu.rotation.x = -Math.PI * 0.8;
        menu.visible = false;
        camera.add(menu);
    }

    currentMenuItems = menuItems;

    if(!textbox)
        startRadians = startAngle * mpi;
    else
        startRadians = (startAngle - 220) * mpi;

   var  h = 2 * Math.tan( vFOV / 2 ) * circleRadius;
   var a = window.innerWidth > window.innerHeight ? window.innerWidth/ window.innerHeight : window.innerHeight/ window.innerWidth;
   menuWidth = (h * a)/2;

    menu.hideMenuItems = function(){
        menuItemsList.forEach(function(item){item.visible = false})
    };

    menu.showMenuItems = function(){
        menuItemsList.forEach(function(item){item.visible = true})
    };

    if(menuItems.length > 1)
        menuItems.forEach(function(item,index){

            var menuItem = createMenuItem(item.title,menuWidth);
                menuItem.menuData = item;
                menuItem.position.y =  Math.sin(startRadians) * circleRadius;
                menuItem.position.z = -0 + (Math.cos(startRadians) * circleRadius);
                menuItem.position.x =  0;
                menuItem.rotation.x = - index * incrementRadians;
                menuItem.scale.set(menuScale,menuScale,menuScale);

            startRadians += incrementRadians;
            menuItemsList.push(menuItem);

            targetList.push(menuItem);
            menu.add(menuItem);

            if(item.sceneId === sceneId){
                menuItem.deactivate();
            }

        });

    if(guideData.gameTasks.length)
        menu.hideMenuItems();
/*    else if(instructionArrow)
        instructionArrow.updateLabel(menuItems.length);*/

   if(isMobile && !targetCircle)
        addUI();

  //  if(!instructionArrow)
    //    addInstructionArrow();
   //    addControlsHelp();
    if(!homeButton)
        addHomeButton();
}

function openMenu(){
    console.log('openMenu()');
    menuActive = true;

    $( document ).trigger( "menuOpen" );

    setTimeout(function(){
      pause();
        updateShader(-0.3,0);
    },200);

    menu.visible = true;
   targetList.forEach(function(item){
        if(!item.menuData && !item.choiceData && typeof item.disable == 'function')
            item.disable();
    });

   // instructionArrow.hide();

}

function closeMenu(){
    console.log('closeMenu()');
    menuActive = false;

    $( document ).trigger( "menuClosed" );

    if(sleepMode)
        sleepMode = false;

    setTimeout(function(){
        if(readyToPlay == true)
            play();

        updateShader(0, 0);

        if(selectedHotspot == homeButton)
            selectedHotspot = null;

    },200);


    menu.visible = false;

    //little shimmy to offset menu
    draggingMenu = true;
    menu.rotation.x = menu.offsetX;
    draggingMenu = false;

targetList.forEach(function(item){
    if(!item.menuData && !item.choiceData && typeof item.enable == 'function')
         item.enable();
    });

}

function createMenuItem(label,width){

    if(label.length > 30)
        label = label.substr(0,30) + "..";
    //R: 230, G: 232, B: 235
    var labelMesh = new textSprite (label, 50 , showHowColors.purple, false, "Lato","rgba(230, 232, 235, 0.6)",null,null,width * 5);
   // labelMesh.scale.set(menuScale,menuScale,menuScale);
    labelMesh.position.set(0,0,2);

    var height =40;
    var line = new THREE.Mesh(new THREE.PlaneGeometry(width * 5,10),new THREE.MeshBasicMaterial({color: showHowColors.purple}));

    line.position.set(0,-40,0);
    line.visible = false;

    var menuItem = new THREE.Object3D();
    menuItem.add(labelMesh);
    menuItem.add(line);

    menuItem.translate(width/2, height/2, 0);

    menuItem.activated = true;
    menuItem.active = true;

    menuItem.cameraQuaternion = camera.quaternion.clone();

    menuItem.activate = function activate(){

        if(menuItem.activated == false)
            menuItem.activated = true;
        else
         return;

        labelMesh.material.opacity = 1;
        line.visible = true;
    };

    menuItem.deactivate = function deactivate(){

        if(menuItem.activated == true)
            menuItem.activated = false;
        else
            return;

        labelMesh.material.opacity = 0.8;
        line.visible = false;
    };

    return menuItem;
}

function updateMenuItems(id){

    if(menuItemsList)
    menuItemsList.forEach(function(item){
        if(item.menuData.sceneId === id)
         item.deactivate();
        else if(item.activated === false)
            item.activate();

    })
}


function createFontAwesomeIcon(parent,icon,x,y,colour){

    var existing = parent.getObjectByName("fontAwesomeIcon");
    if(existing) parent.remove(existing);

    if(icon){

        var fontAwesomeIcon = new textSprite (icon, 70 , colour || '#ffffff', false ,"FontAwesome" );
        fontAwesomeIcon.scale.set(0.1,0.1,0.1);
        fontAwesomeIcon.name = "fontAwesomeIcon";


        fontAwesomeIcon.position.x = x || parent.width/2 - fontAwesomeIcon.width/3;
        fontAwesomeIcon.position.y = y || -0.5;
        fontAwesomeIcon.position.z = 5;
        parent.add(fontAwesomeIcon);
        parent.icon = fontAwesomeIcon;
    }
}



function onScrollMouseMove(event){
    console.log('onScrollMouseMove()',draggingY,draggingMenu,isScrolling);

    var y = event.pageY || event.touches[0].pageY;

    if(Math.abs(y - draggingY) > 20){
        isScrolling = true;
    }
    if (draggingY)
        menu.rotation.x += ((draggingY - y)/renderer.domElement.height)/(Math.PI*2);
}

function onScrollMouseUp(event){
    console.log("onScrollMouseUp()");
    isScrolling = false;
    document.removeEventListener('mousemove', onScrollMouseMove, false);
    document.removeEventListener('touchmove', onScrollMouseMove, false);
    document.removeEventListener('mouseup', onScrollMouseUp, false);
    document.removeEventListener('touchend', onScrollMouseUp, false);
}

function onMenuItemMouseDown(item){
    console.log("onMenuItemMouseDown()");
    //addMouseScrolling(item);
    menuItemHandler(item);
}

var oPreviousCoords = {
    'x': 0,
    'y': 0
}

function addMouseScrolling(item){

    //if(!menu.visible)
        return;

   console.log("addMouseScrolling()");
    camera.originX = camera.rotation.x;
    menu.originX = menu.rotation.x;

    isScrolling = false;
    draggingMenu = true;

    if(isMobile)
    waitForMovement(1,function(){
        if(!isScrolling)draggingMenu = false;
    })

    document.addEventListener('mousemove', onScrollMouseMove, false);
    document.addEventListener('mouseup', onScrollMouseUp, false);
    document.addEventListener('touchmove', onScrollMouseMove, false);
    document.addEventListener('touchend', onScrollMouseUp, false);

    setTimeout(function(){

        if(!isScrolling){

            setTimeout(function() {

                if(!item)
                    return;


                if (item.menuData)
                    menuItemHandler(item);
                else if (item.choiceData)
                    answerItemHandler(item);

                if(isMobile)
                waitForMovement(0.5,function(){
                    draggingMenu = false;
                })
            },200);

            document.removeEventListener('mousemove', onScrollMouseMove, false);
            document.removeEventListener('mouseup', onScrollMouseUp, false);
            document.removeEventListener('touchmove', onScrollMouseMove, false);
            document.removeEventListener('touchend', onScrollMouseUp, false);

        }
        else
            openMenu();

    },200);
}

function menuItemHandler(item){
    console.log('menuItemHandler()');

    var menuItem = item.menuData || item.parent.menuData;

    if(menuItem.sceneId){

        selectedHotspot = null;
        getScene(menuItem.sceneId);
    }

    else if(menuItem.guideScenes || menuItem.childSections)
    {
        var menuItems = [];

        if(!menuItem.topLevel){

            var backButton = {};
                backButton.title = "< Back";
            if(menuItem.parent){
                backButton.guideScenes = menuItem.parent.guideScenes;
                backButton.childSections = menuItem.parent.childSections;
                backButton.parent = menuItem.parent.parent;
            }else{
                backButton.childSections = topLevelSections;
                backButton.topLevel = true;
            }

            menuItems.push(backButton);
        }

        if(menuItem.guideScenes)
        if(menuItem.guideScenes.length > 0){
            menuItem.guideScenes.forEach(function (item) {
                menuItems.push(item);
            });
            //getScene(menuItem.guideScenes[0].scene.id);
        }

        if(menuItem.childSections)
             menuItem.childSections.forEach(function (item) {
             menuItems.push(item);
        });

        menuItemsList.forEach(function(menuItem){
            if(item != menuItem){

                var position = menuItem.position;
                position.opacity = 1;
                var target = { x : -menuWidth, opacity:0 };
                var tween = new TWEEN.Tween( position ).to( target, 300 ).onUpdate(function(){
                    menuItem.position.x = position.x;
                }).onComplete(function(){
                    menuItem.visible = false;
                }).start();

            }
        });
       setTimeout(function(){create3DGuide(menuItems)},500);
    }
}


function menuUpdate(){

      var tilt = isMobile ? controls.getVRDisplay().poseSensor_.orientationOut_[0] * 2 : controls.getVRDisplay().phi_;


      if(tilt < 0 && !draggingMenu){
          var speed = Math.PI;// menuItemsList.length > 4 ? menuItemsList.length/Math.PI  :  menuItemsList.length;
          menu.rotation.x = menu.offsetX - ( tilt * speed);

          var pan =  isMobile ? controls.getVRDisplay().poseSensor_.orientationOut_[1] * 2 : controls.getVRDisplay().theta_;
          if(homeButton)
              homeButton.update(pan);
      }


        if(tilt < -0.6 && !menuActive)
            openMenu();

        else if (tilt > -0.6 && menuActive)
              closeMenu();
}

function updateTextBox(text,choices){

    console.log('updateTextBox()',text,choices);
    clearTextbox();
    if(text){

        if($('#htmlTextBoxHolder').length < 1)
            $('body').append('<div id ="htmlTextBoxHolder">'+text+'</div>');
        else
            $('#htmlTextBoxHolder').html(text);

        $('#htmlTextBoxHolder').css({
            'width': menuWidth * 5,
            'height':'auto',
            'position': 'absolute',
            'top': window.innerHeight,
            'padding': '20px 20px',
            'background-color':"rgba(230, 232, 235, 0.5)",
            'color': showHowColors.purple,
            'font-size': '70px',
            'visibility' : 'visible',
            'border-radius': '20px',
            'text-align': 'center',
            'box-shadow': '10px 10px 5px #888888'
        });

        $('#htmlTextBoxHolder h1').css({
            'font-size': '100px',
            'text-align': 'center',
            'margin-bottom' : '10px',
             'border-bottom': '5px solid grey'
        });

        if(text.length > 1)
            html2canvas($("#htmlTextBoxHolder"), {
                onrendered: function(canvas) {
                    positionTextbox(canvas);
                    if(choices)
                    setTimeout(answerBoxes,100,choices,$('#htmlTextBoxHolder').height() + 40);

                },
                width: $('#htmlTextBoxHolder').width() + 40,
                height: $('#htmlTextBoxHolder').height() + 40
            });


    }
    else{
        positionTextbox();
        if(choices)
            answerBoxes(choices);
    }

    function positionTextbox(canvas){

        clearTextbox();

        textbox = new THREE.Object3D;

        if(canvas){
            var texture = new THREE.Texture(canvas);
            texture.needsUpdate = true;

            var textboxBody = new THREE.Mesh(
                new THREE.BoxGeometry(canvas.width, canvas.height,1),
                new THREE.MeshBasicMaterial({
                    map: texture,
                    transparent: true
                })
            );
            var scalar =  menuScale;

            var dist = -100;
            var pLocal = new THREE.Vector3( 0, 0, dist );
            var target = pLocal.applyMatrix4( camera.matrixWorld );


            textbox.position.copy( target );
            textbox.position.y = 0;
            textbox.lookAt(camera.position);

            textbox.height = canvas.height * scalar;
            textbox.width = canvas.width * scalar;
            textbox.scale.set(scalar,scalar,scalar);
            textbox.name = 'textbox';
            targetList.push(textbox);
            textbox.add(textboxBody);
            textboxBody.position.y = canvas.height/2;

            scene.add(textbox);
        }
    }

}

function clearTextbox(){

    removeAnswerboxes();
    if(textbox){
        for (var i = textbox.children.length - 1; i >= 0; i--) {
            textbox.remove(textbox.children[i]);
        }
        scene.remove(textbox);
    }
    $('#htmlTextBoxHolder').empty();
}


function removeAnswerboxes(){
    console.log('removeAnwerBoxes()');

    answersList.forEach(function(answer){
        answer.children.forEach(function(child){
            answer.remove(child);
        });
        textbox.remove(answer);
        removeFromArray(targetList,answer);
    })
    answersList = [];
}

function answerBoxes(choices,textboxHeight){

  if(answersList.length > 0)
    removeAnswerboxes();

    if(choices != null)
        for(var i = 0; i < choices.length;i++)
            addAnswerBox(choices[i],i);

    taskLocked = true;
}


function addAnswerBox(choice,index){

    console.log('addAnswerBox',choice);
    var answer = createMenuItem(choice.title,menuWidth);
    answer.choiceData = choice;

    answer.position.x = 0;
    answer.position.y = -50 - (90 * index);
    answer.position.z = 0;

    answer.cameraQuaternion = camera.quaternion.clone();

    textbox.add(answer);

    answersList.push(answer);
    targetList.push(answer);

    answer.select = function(){
        answer.choiceData.selected = true;
        answer.selected = true;
        answer.activate();
    };
    answer.deselect = function(){
        answer.choiceData.selected = false;
        answer.selected = false;
        answer.deactivate();
    }

    answer.deactivate();

    return answer;
}

function onAnswerItemMouseDown(item){
    console.log('onAnswerItemMouseDown()',item);
   // addMouseScrolling(item);
    answerItemHandler(item);
}

function answerItemHandler(item){
    console.log('answerItemHandler()',item);

    var correct = gameTask.choices.filter(function(choice){return choice.correct == true;});

    if(item.choiceData.submit)
        submitButton(item);
    else if (correct.length == 1) {

        item.select();
        setTimeout(function () {
            if (item.choiceData.correct == true)
                removeAnswerboxes();
            else
                item.deselect();

            $(document).trigger("submit");
        }, 500);
    }
    else if(item.choiceData.selected == null || item.choiceData.selected == false)
        item.select();
    else
        item.deselect();

    if(item.choiceData.event != null)
        $(document).trigger(item.choiceData.event,item);
}

function submitButton(button){
    console.log('submitButton()',answersList.length);

    button.activate();

    if(multichoiceSubmit()){
        setTimeout(function(){
            removeAnswerboxes();
            $(document).trigger("submit",button);
        },1000);

    }
    else{
        setTimeout(function(){
            createFontAwesomeIcon(button);
          answersList.forEach(function(item){
                item.deselect();
            })
        },700);

    }
};



function addUI(){

    var o = {opacity: 1,opacity2: 0,scale : 0.1 };
    var t = {opacity: 0,opacity2: 0.8,scale : 1};

    var radius   = 15,
        segments = 32,
        material = new THREE.MeshBasicMaterial( { color: showHowColors.lightGrey, transparent:true,opacity:0.5 } ),
        outerMaterial = new THREE.MeshBasicMaterial( { color: showHowColors.lightGrey, transparent:true,opacity:o.opacity } ),
        outerMaterial2 = new THREE.MeshBasicMaterial( { color: showHowColors.purple, transparent:true } ),
        outerMaterialInitial = new THREE.MeshBasicMaterial( { color: showHowColors.purple, transparent:true } ),
        geometry = new THREE.CircleBufferGeometry( radius, segments),
        outerGeometry = new THREE.RingBufferGeometry( radius, radius * 4, segments *4,segments *4 ),
        outerGeometryInitial = new THREE.RingBufferGeometry( (radius * 4) -5, radius * 4, segments *4,segments *4 );

    targetCircle =  new THREE.Mesh( geometry, material );


    targetCircle.position.set(0,0,-90);
    targetCircle.scale.set(o.scale,o.scale,o.scale);
    targetCircle.activeCircle =  new THREE.Mesh( outerGeometry, outerMaterial );
    targetCircle.activeCircle.scale.set(o.scale,o.scale,o.scale);
    targetCircle.add(targetCircle.activeCircle);

    targetCircle.activeCircle2 =  new THREE.Mesh( outerGeometry, outerMaterial2 );
    targetCircle.activeCircle2.scale.set(o.scale,o.scale,o.scale);
    targetCircle.activeCircle2.position.z = -1;
    targetCircle.add(targetCircle.activeCircle2);

    targetCircle.activeCircleInit =  new THREE.Mesh( outerGeometryInitial, outerMaterialInitial );
    targetCircle.add(targetCircle.activeCircleInit);


    targetCircle.activeCircle.visible = false;
    targetCircle.activeCircle2.visible = false;
    targetCircle.activeCircleInit.visible = false;


    targetCircle.isActive = false;

    targetCircle.activate = function(){

        if(targetCircle.isActive)
            return;
        targetCircle.activeCircleInit.visible = true;
        targetCircle.isActive = true;
        targetCircle.tween = new TWEEN.Tween(o).to(t,2500).onUpdate(function(){
            outerMaterial.opacity = o.opacity;
            outerMaterial2.opacity = o.opacity2;
            targetCircle.activeCircle.scale.set(o.scale,o.scale,o.scale);
            targetCircle.activeCircle2.scale.set(o.scale,o.scale,o.scale);
        }).onComplete(function(){
            outerMaterial2.opacity = 1;
            setTimeout(function(){

                outerMaterial2.opacity = 0.9;

                if(!selectedHotspot)
                    return;

                if(selectedHotspot.data)
                    onHotspotMouseDown(selectedHotspot);
                else if(selectedHotspot.menuData && (selectedHotspot.menuData.sceneId !== currentScene.id))
                    menuItemHandler(selectedHotspot);
                else if(selectedHotspot.choiceData)
                    answerItemHandler(selectedHotspot);
                else if(selectedHotspot.hotspotData)
                    onLegacyHotspotMouseDown(selectedHotspot);
                else if(selectedHotspot.name == 'homeButton'){
                    homeButton.activate();
                    //selectedHotspot = null;
                }

                targetCircle.deactivate();
            },200);
        }).start();

        targetCircle.activeCircle.visible = true;
        targetCircle.activeCircle2.visible = true;

    }

    targetCircle.deactivate = function(){

        if(!targetCircle.isActive)
            return;

        targetCircle.isActive = false;

        targetCircle.activeCircleInit.visible = false;

        if(targetCircle.tween)
            targetCircle.tween.stop();

        targetCircle.activeCircle.visible = false;
        targetCircle.activeCircle2.visible = false;
        //o = {opacity: 0.2,scale : 0.33333};
        o = {opacity: 1,opacity2: 0,scale : 0.1 };
        t = {opacity: 0,opacity2: 0.8,scale : 1};
        targetCircle.activeCircle.scale.set(o.scale,o.scale,o.scale);
        targetCircle.activeCircle2.scale.set(o.scale,o.scale,o.scale);
        outerMaterial.opacity = o.opacity;
        outerMaterial2.opacity = o.opacity2;
    }


    $(document).bind("scene_loaded",targetCircle.deactivate);

    camera.add( targetCircle );
}

function addInstructionArrow(){

    var geometry = new THREE.Geometry();
    var v1 = new THREE.Vector3(0,1,0);
    var v2 = new THREE.Vector3(-50,-50,0);
    var v3 = new THREE.Vector3(50,-50,0);

    geometry.vertices.push(v1);
    geometry.vertices.push(v2);
    geometry.vertices.push(v3);

    geometry.faces.push(new THREE.Face3(0, 1, 2));
   geometry.computeFaceNormals();

    geometry.center();

    instructionArrow = new THREE.Object3D();

    var mat = new THREE.MeshBasicMaterial({color: showHowColors.lightGrey,transparent: true,opacity:0.8});
    mat.side = THREE.DoubleSide;
    var geo = new THREE.EdgesGeometry( geometry );
    var eMaterial = new THREE.LineBasicMaterial( { color: showHowColors.purple, linewidth: 1 } );
    eMaterial.side = THREE.DoubleSide;
    var edges = new THREE.LineSegments( geo, eMaterial );

    var triangle = new THREE.Mesh(geometry, mat );
    instructionArrow.add(triangle);
    instructionArrow.scale.set(0.2,0.2,0.2);
    instructionArrow.rotation.set(0,0,Math.PI);

    instructionArrow.updateLabel = function(text,icon){

        if(instructionArrow.label)
            instructionArrow.remove(instructionArrow.label);
        if(instructionArrow.icon)
            instructionArrow.remove(instructionArrow.icon);


        instructionArrow.label = new textSprite (text, 60 , showHowColors.purple, true, "Lato" );
        instructionArrow.label.name = "label";

        var labelScale = 0.2;
        var iconScale = 0.25;

        if(icon){
            instructionArrow.icon = new textSprite (icon, 80 , showHowColors.purple, true, "FontAwesome" );
            instructionArrow.icon.scale.set(iconScale ,iconScale ,iconScale );
            instructionArrow.icon.rotation.set(0,0,Math.PI);
            instructionArrow.icon.position.set(0,-12,1);
            instructionArrow.add(instructionArrow.icon);
        }

        instructionArrow.label.scale.set(labelScale,labelScale,labelScale);
        instructionArrow.label.rotation.set(0,0,Math.PI);
        instructionArrow.label.position.set(0,5,1);
        instructionArrow.add(instructionArrow.label);

        instructionArrow.waitingForObedience = true;

        instructionArrow.scale.set(0.2,0.2,0.2);
    };

    instructionArrow.rotateLeft = function(){

        if(!instructionArrow.visible)
            instructionArrow.visible = true;

        if(instructionArrow.label)
        instructionArrow.label.visible = false;
        if(instructionArrow.icon)
            instructionArrow.icon.visible = false;

       instructionArrow.rotation.set(0,0,Math.PI/2);
        instructionArrow.position.set(-10,0,-50);

        instructionArrow.scale.set(0.1,0.1,0.1);
    };

    instructionArrow.rotateRight = function(){

        if(!instructionArrow.visible)
            instructionArrow.visible = true;

        if(instructionArrow.label)
            instructionArrow.label.visible = false;
        if(instructionArrow.icon)
            instructionArrow.icon.visible = false;

      instructionArrow.rotation.set(0,0,-Math.PI/2);
        instructionArrow.position.set(10,0,-50);

        instructionArrow.scale.set(0.1,0.1,0.1);
    };

    instructionArrow.hide = function(){


        instructionArrow.visible = false;
        instructionArrow.waitingForObedience = false;

        if(instructionArrow.tween)
            instructionArrow.tween.stop();
    };

    instructionArrow.show = function(){
        instructionArrow.visible = true;
        instructionArrow.position.x = 0;
        instructionArrow.position.set(0,-20,-50);
        instructionArrow.rotation.set(0,0,Math.PI);
        instructionArrow.waitingForObedience = true;

        setTimeout(function(){
            if( instructionArrow.waitingForObedience)
                instructionArrow.warn();
        },3000)
    };

    instructionArrow.warn = function(){

        console.log('warn');

        var origin = instructionArrow.position.y;
        var distance = 2;
        var time = 300;
        var o = {y:instructionArrow.position.y};
        var t = {y: o.y + distance};

        instructionArrow.tweenUp = new TWEEN.Tween(o).to(t,time).onStart(function(){
            instructionArrow.tween = instructionArrow.tweenUp;
        }).onUpdate(function(){
            instructionArrow.position.y =o.y;
        }).onComplete(function(){
            t.y = origin - distance;
        });

        instructionArrow.tweenDown = new TWEEN.Tween(o).to(t,time).onStart(function(){
            instructionArrow.tween = instructionArrow.tweenDown;
        }).onUpdate(function(){
            instructionArrow.position.y = o.y;
        }).onComplete(function(){
            t.y = origin;
        });

        instructionArrow.tweenUp.chain(instructionArrow.tweenDown);
        instructionArrow.tweenDown.chain(instructionArrow.tweenUp);

        instructionArrow.tweenUp.start();


    };

    instructionArrow.waitingForObedience = false;

    camera.add( instructionArrow );

    instructionArrow.rotation.set(0,0,Math.PI);
    instructionArrow.visible = false;

    var range = Math.PI/4;
    var currentPoint;

    instructionArrow.update = function(points,time){

        if(selectedHotspot ||  instructionArrow.waitingForObedience || menuActive)
            return;

        var inProgress = points.some(function (point) {
            if (time > point.startTime && time < point.endTime){

                if(currentPoint != point)
                    currentPoint = point;

                return true;
            }
        });

        if(inProgress){
            var d = camera.getWorldDirection();
            var diff = new THREE.Vector3().copy(currentPoint.vector);
            diff.sub(camera.position);
            var theta = Math.atan2(diff.x,diff.z) - Math.atan2(d.x,d.z);


            if(Math.abs(theta) > range){
                if(theta > 0 && theta < Math.PI)
                    instructionArrow.rotateLeft();
                else if(theta < 0 || theta > Math.PI)
                    instructionArrow.rotateRight();
            }
            else
                instructionArrow.hide();
        }
        else{
            instructionArrow.hide();
            currentPoint = null;
        }
    }
}

function onVRModeChangeUI(mode){
    pause();

    if(mode == 3) {
        vrMode = true;
        readyToPlay = false;
        targetCircle.scale.set(0.1,0.1,0.1);
        setTimeout(function(){ readyToPlay = true },2000);
        menu.scale.set(0.05,0.05,0.05);
    }

    if(mode == 1){
        fullscreen = false;
        vrMode = false;
       // setZoom(80);
    }

    if(mode == 2){
        vrMode = false;
        fullscreen = true;
    }

}

function guide3DResize(){

    if(instructionArrow)
    if(window.innerWidth > window.innerHeight)
        instructionArrow.position.set(0,-2,-5);
    else
        instructionArrow.position.set(0,-5,-5);
}

function addControlsHelp(){



    var instuctions = new THREE.Object3D;
    instuctions .scale.set(0.1,0.1,0.1);
    instuctions.position.set(0,30,-150);

    waitForFontAwesome(function(){
        addDragInstructions();
    })

    function addDragInstructions(){


        clearInstructions();
        var upText =  new textSprite ("Have a look around" , 80 , showHowColors.purple, true, "Lato",showHowColors.lightGrey );
        upText.position.y = 200;
        var iconLeft = new textSprite (fontAwesome["fa-chevron-left"], 80 , showHowColors.purple, true, "FontAwesome",showHowColors.lightGrey,null,"right" );
        var text =  new textSprite (isMobile ? "drag with your finger or rotate your device" : "drag with your mouse" , 80 , showHowColors.purple, true, "Lato",showHowColors.lightGrey );
        var iconRight = new textSprite (fontAwesome["fa-chevron-right"], 80 , showHowColors.purple, true, "FontAwesome" ,showHowColors.lightGrey,null,"left");
        iconLeft.position.x = -text.width/2 - iconLeft.width/2;
        iconLeft.position.z = -1;
        iconRight.position.x = text.width/2 +  iconLeft.width/2;
        iconRight.position.z = -1;
        instuctions.add(upText);
        instuctions.add(iconLeft);
        instuctions.add(iconRight);
        instuctions.add(text);

        var posX = camera.rotation.y;

        var dragCheck  = setInterval(function(){
            if(Math.abs(posX - camera.rotation.y) > 0.3 || menuActive){
                clearInstructions(false);

                if(!menuActive)
                    setTimeout(addLookdownInstructions,500);

                clearInterval(dragCheck);
            }
        },100)
    }

    function addLookdownInstructions(){

        var iconLeft = new textSprite (fontAwesome["fa-chevron-down"], 80 , showHowColors.purple, true, "FontAwesome",showHowColors.lightGrey,null,"right" );
        var text =  new textSprite (guideData.gameTasks.length  ? isMobile ? "tilt your device down for tasks" : "drag up with your mouse for tasks" : isMobile ? "tilt your device down for menu" : "look down for menu", 80 , showHowColors.purple, true, "Lato",showHowColors.lightGrey );
         iconLeft.position.x = -text.width/2 - iconLeft.width/2;

        instuctions.add(iconLeft);
        instuctions.add(text);

        var InstuctionsCheck  = setInterval(function(){
            if(menuActive){
                clearInstructions(true);
                clearInterval(InstuctionsCheck);
            }
        },100)
    }

    function clearInstructions(remove){

        for( var i = instuctions.children.length - 1; i >= 0; i--) {instuctions.remove(instuctions.children[i])}


        if(remove)
            camera.remove(instuctions);
    }

    camera.add(instuctions);
    instructionArrow.hide();
}

function addHomeButton(){
    var loader = new THREE.TextureLoader();
    loader.setCrossOrigin("");

    loader.load(
        webApp ? "/resources/images/home.png" : "resources/images/home.png",
        function ( texture ) {

            homeButton = new THREE.Mesh(
                new THREE.PlaneGeometry(texture.image.width, texture.image.height),
                new THREE.MeshBasicMaterial({
                    map: texture,
                    transparent: true
                })
            );

            homeButton.position.set(0,-400,0);
            homeButton.scale.set(0.1,0.1,0.1);
            homeButton.default = {};
            homeButton.default.guideId = guideId;
            homeButton.default.sceneId = sceneId;
            homeButton.name = 'homeButton';
            homeButton.active = true;

            homeButton.update = function(theta){
                homeButton.rotation.z =  theta +  dolly.rotation.y;
            };

            homeButton.activate = function(){

                console.log("homeButton.activate");

                updateShader(-0.5,0);

                if(guideId == homeButton.default.guideId && sceneId == homeButton.default.sceneId){
                    sleepMode = true;
                    return;
                }

                if(guideId != homeButton.default.guideId)
                    loadGuide(homeButton.default.guideId);

                if(sceneId != homeButton.default.sceneId)
                    getScene(homeButton.default.sceneId);

                if(video){
                    video.pause();
                    if(videoSettings.startTime)
                        video.currentTime = videoSettings.startTime || 0;
                    else
                        video.currentTime = 0;
                    video.load();
                }

                gamePaused = false;

                if(gameTasks.length)
                    updateGameTask(gameTasks[0]);

                pause();

            }

            homeButton.lookAt(camera.position);
            scene.add(homeButton);
            targetList.push(homeButton);
        }
    );

};

