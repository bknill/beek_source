var hs_bubble,
	refractSphereCamera,
	selectedHotspot,
	viewingHotspot,
	hotspotLat,
	hotspotLon,
	closeButton = false,
	group,
	photos,
	posters,
	sounds,
	hiddenHotspots = [],
	hotspots = [],
	hotspotScaleFactor = 0.4,
	hotspotTweenTime = 1500,
	hotspotRenderList = [],
	renderInProgress = false;



function createHotspot(data,replaceHotspot){

	//iosDebug('createHotspot',data.title);

	if(data.config == null)
		return;

	var hotspot = replaceHotspot || new THREE.Object3D();

	hotspot.data = data;
	hotspot.name = data.id;
	hotspot.active = hotspot.data.loadSceneId != null || hotspot.data.loadGuideId != null;

	//activate any inactive hotspots that are required for game tasks and hide ones that will be shown
	if(gameTasks.length > 0)
		gameTasks.forEach(function(task){
			if(task.hotspotsToFind != null){
				if(task.hotspotsToFind.forEach(function(hstofind){
						if(hstofind.id == data.id){
							hotspot.active = true;
						}

					}));

			}
			if(task.hotspotsToShow != null){
				if(task.hotspotsToShow.forEach(function(hstofind){
						if(hstofind.id == data.id){
							hotspot.visible = false;
						}
					}));

			}
		});

	if(hotspotsToHide.indexOf(hotspot.name) > -1)
		hotspot.visible = false;

	if(typeof hotspot.data.config === 'string')
		hotspot.data.config = JSON.parse(data.config);

	if(hotspot.data.config.position)
		hotspot.position.copy(hotspot.data.config.position);
	else
		return;

	if(hotspot.data.config.rotation)
		hotspot.rotation.copy(hotspot.data.config.rotation);
	else
		hotspot.lookAt(camera.position);

	scene.add(hotspot);
	hotspots.push(hotspot);
	targetList.push(hotspot);
	hotspot.updateMatrixWorld();

	if(hotspot.data.config.scale)
		hotspot.scale.set(hotspot.data.config.scale,hotspot.data.config.scale,hotspot.data.config.scale);

	var _opacity =  hotspot.data.config.opacity || 1;

	hotspot.setOpacity = function(opacity){

		_opacity = opacity;

		if(hotspot.material)
			hotspot.material.opacity =  opacity;

		//console.log("setting opacity",opacity)

		hotspot.children.forEach(function(child){
			if(child.material && child.name !== 'box'){
				child.material.transparent = true;
				child.material.opacity = opacity;
			}
		})
	};

	hotspot.getOpacity = function(){
		return _opacity;
	}



	if(hotspot.data.behaviour)
		addBehaviours(hotspot);

	if(hotspot.data.button)
		createHotspotButton(hotspot);

	if(data.media){

		hotspot.active = true;

		if(typeof data.media === 'string')
			data.media = JSON.parse(data.media);

		if(data.media.type == "shape")
			createShapeHotspot(hotspot, data);

		if(data.media.files)
			data.media.files.forEach(function(file){
				if(file.type == '.jpeg' || file.type == '.jpg' || file.type == '.png')
					createImageHotspot(hotspot, file);
			});
	}
	if(data.description){
		hotspot.active = true;
		createHotspotDescription(hotspot);
	}

	if(hotspot.data.config.opacity)
		hotspot.setOpacity(hotspot.data.config.opacity);

	hotspot.clear = function(){
		if(hotspot.behaviours != null)
			hotspot.removeListeners();
	};

}



function legacyHotspots(){

	for(var b in bubbles)
		if(!hotspotHidden(bubbles[b], 'bubble' ))
			bubbleToHotspot(bubbles[b]);

	/*	for(var p in photos)
	 if(!hotspotHidden(photos[p], 'photo' ))
	 createPhoto(photos[p]);*/

	for(var p in posters)
		if(!hotspotHidden(posters[p], 'poster' ))
			posterToHotspot(posters[p]);

	/*	for(var s in sounds)
	 if(!hotspotHidden(sounds[s], 'sound' ))
	 createSound(sounds[s]);*/

}

function bubbleToHotspot(bubble){
	var data = {};
	data.config = {};
	data.config.position = panTiltToVector(bubble.pan,bubble.tilt,bubble.distance);
	data.config.scale = hotspotScaleFactor;
	data.button = {};
	data.button.label = bubble.titleMerged;
	data.loadSceneId = bubble.targetSceneId;
	createHotspot(data);
}

function posterToHotspot(poster){

	if(!poster.text)
		return;

	var data = {};
	data.config = {};
	data.config.position = panTiltToVector(poster.pan,poster.tilt,poster.distance);
	data.config.scale = 0.333;

	data.id = poster.id;

	if(poster.showIcon){
		data.button = {};
		data.button.icon = "fa-info";
	}
	data.description = {};
	data.description.text =  poster.text.replace(/<.+?>/g, "");

	if(poster.frame == 1)
		data.description.setting = {'option':1,'fontcolor':'#ffffff','backgroundcolor':null};
	else if(poster.frame == 2)
		data.description.setting = {'option':2,'fontcolor':'#000000','backgroundcolor':null};
	else if(poster.frame == 3)
		data.description.setting = {'option':3,'fontcolor': showHowColors.purple,'backgroundcolor': showHowColors.lightGrey};

	createHotspot(data);
}

function hotspotHidden( hotspot, type ){

	if(hotspotsToHide.indexOf( hotspot.id ) < 0 )
		return false;
	else{
		hotspot.type = type;
		hiddenHotspots.push( hotspot );
	}

	return true;
}

function showHiddenHotspot( hotspot ){

	if( hotspot.type == 'bubble')
		bubble(hotspot);

	if( hotspot.type == 'photo')
		createPhoto(hotspot);

	if( hotspot.type == 'poster')
		createPoster(hotspot);

	if( hotspot.type == 'sound')
		createSound(hotspot);
}

function activateForTask(task){
	iosDebug('activateForTask ' + task.hotspotsToFind.length);
	task.hotspotsToFind.forEach(function(hotspotToFind){
		hotspots.some(function(hotspot){
			iosDebug(hotspotToFind.id + "," + hotspot.data.id);
			if(hotspotToFind.id == hotspot.data.id){
				iosDebug('setting active');
				hotspot.active = true;
				return true;
			}
		})
	})

}

function hideForTask(task){
	task.hotspotsToShow.forEach(function(hotspotToShow){
		hotspots.some(function(hotspot){
			if(hotspotToShow.id == hotspot.data.id){
				hotspot.visible = false;
				hotspot.active = false;
				return true;
			}
		})
	})

}

function showForTask(task){
	task.hotspotsToShow.forEach(function(hotspotToShow){
		hotspots.some(function(hotspot){
			if(hotspotToShow.id == hotspot.data.id){
				hotspot.visible = true;
				hotspot.active = true;
				return true;
			}
		})
	})

}

function addBehaviours(hotspot){
	console.log('addBehaviours');

	if(typeof hotspot.data.behaviour === 'string')
		hotspot.behaviours = JSON.parse(hotspot.data.behaviour);
	else if(typeof hotspot.data.behaviour === 'object')
		hotspot.behaviours = hotspot.data.behaviour;


	hotspot.removeListeners = function(){
		console.log('remove behaviours');

		if(hotspot.currentTween)
			hotspot.currentTween.stop();

		$(document).unbind("play",hotspot.behaviourPlay);
		$(document).unbind("pause",hotspot.behaviourPause);

		$(document).unbind("panoVideoPlay",hotspot.behaviourPlay);
		$(document).unbind("panoVideoPause",hotspot.behaviourPause);
		$(document).unbind("panoVideoEnded",hotspot.behaviourRestart);

		$(document).unbind("voiceoverPlay",hotspot.behaviourPlay);
		$(document).unbind("voiceoverEnded",hotspot.behaviourRestart);
		$(document).unbind("voiceoverPause",hotspot.behaviourPause);
	};

	hotspot.behaviours.values.forEach(function(behaviour,index){

		behaviour.origin = {
			pX : index == 0 ? hotspot.position.x : hotspot.behaviours.values[index-1].position.x,
			pY : index == 0 ? hotspot.position.y : hotspot.behaviours.values[index-1].position.y,
			pZ : index == 0 ? hotspot.position.z : hotspot.behaviours.values[index-1].position.z,
			scale :index == 0 ? hotspot.scale.x : hotspot.behaviours.values[index-1].scale,
			opacity : index == 0 ? hotspot.getOpacity() : hotspot.behaviours.values[index-1].opacity
		};

		behaviour.target = {
			pX : behaviour.position.x,
			pY : behaviour.position.y,
			pZ : behaviour.position.z,
			scale : behaviour.scale,
			opacity : behaviour.opacity
		};

		behaviour.offsetTime = index == 0 ? behaviour.time : behaviour.time - hotspot.behaviours.values[index-1].time;

		behaviour.tween = new TWEEN.Tween(behaviour.origin).to(behaviour.target,behaviour.offsetTime * 1000)
			.onStart(function(){
				//console.log('behaviourStart',hotspot.data.title);
				hotspot.currentTween = behaviour.tween;
			})
			.onUpdate(function(){
				//console.log('behaviourUpdate',hotspot.data.title);
				hotspot.position.set(behaviour.origin.pX,behaviour.origin.pY,behaviour.origin.pZ);
				hotspot.lookAt(camera.position);
				hotspot.scale.set(behaviour.origin.scale,behaviour.origin.scale,behaviour.origin.scale);
				hotspot.setOpacity(behaviour.origin.opacity);

			}).onComplete(function(){
				//console.log('behaviourComplete',hotspot.data.title);
				if(!hotspot.behaviours)
					return;

				behaviour.origin.pX = index != 0 ? hotspot.behaviours.values[index-1].position.x : hotspot.data.config.position.x;
				behaviour.origin.pY = index != 0 ? hotspot.behaviours.values[index-1].position.y : hotspot.data.config.position.y;
				behaviour.origin.pZ = index != 0 ? hotspot.behaviours.values[index-1].position.z : hotspot.data.config.position.z;
				behaviour.origin.scale = index != 0 ? hotspot.behaviours.values[index-1].scale : hotspot.data.config.scale;
				behaviour.origin.opacity = index != 0 ? hotspot.behaviours.values[index-1].opacity : hotspot.data.config.opacity;

				hotspot.currentTween = null;
				hotspot.removeListeners();
			});

		if(index > 0)
			hotspot.behaviours.values[index-1].tween.chain(behaviour.tween);
	});

	hotspot.behaviourPlay = function (){
		console.log('behaviourPlay()', hotspot.data.title);
		$(document).bind("pause",hotspot.behaviourPause);
		$(document).unbind("panoVideoPlay",hotspot.behaviourPlay);
		$(document).bind("panoVideoEnded",hotspot.behaviourRestart);
		$(document).bind("panoVideoPause",hotspot.behaviourPause);

		if(!hotspot.currentTween && hotspot.behaviours)
			hotspot.behaviours.values[0].tween.start();
		else if(hotspot.currentTween)
			hotspot.currentTween.play();
	}
	hotspot.behaviourPause = function(){
		console.log('behaviourPause()', hotspot.data.title);

		$(document).bind("panoVideoPlay",hotspot.behaviourPlay);
		$(document).bind("play",hotspot.behaviourPlay);
		$(document).unbind("panoVideoPause",hotspot.behaviourPause);
		$(document).unbind("pause",hotspot.behaviourPause);
		if(!hotspot.behaviours)
			return;

		if(!hotspot.currentTween)
			hotspot.behaviours.values[0].tween.pause();
		else
			hotspot.currentTween.pause();
	}

	hotspot.behaviourRestart = function(){

		$(document).unbind("panoVideoPlay",hotspot.behaviourPlay);
		$(document).unbind("panoVideoEnded",hotspot.behaviourRestart);
		$(document).bind("panoVideoPause",hotspot.behaviourPause);

		if(hotspot.behaviours)
			hotspot.behaviours.values[0].tween.start();
	}


	if(hotspot.behaviours.type == 'video' && hotspot.behaviours.values.length > 0){

		if(video && (video.currentTime > 0 && (!isPaused || gamePaused)))
			hotspot.behaviourPlay();
		else
			$(document).bind("panoVideoPlay",hotspot.behaviourPlay);
	}
	else if(hotspot.behaviours.type == 'voice' && hotspot.behaviours.values.length > 0){
		$(document).bind("voiceoverPlay",hotspot.behaviourPlay);
		$(document).bind("voiceoverEnded",hotspot.behaviourRestart);
		$(document).bind("voiceoverPause",hotspot.behaviourPause);
	}


}


function createHotspotButton(hotspot){

	var data = hotspot.data,label,icon;

	var action = data.loadSceneId != null || data.media != null || (data.description != null && (data.description.text != null));

	if(typeof data.button === 'string' && data.button.length > 0)
		data.button = JSON.parse(data.button);

	if(data.button.label != null){
		label = new textSprite (data.button.label, 80 , showHowColors.purple, true, "Lato" , action ? "#fff" : showHowColors.lightGrey , null,data.button.icon != null ?"left" : null );
		label.scale.set(hotspotScaleFactor,hotspotScaleFactor,hotspotScaleFactor);
		label.material.opacity = hotspot.data.config.opacity || 1;

		var labelBox = new THREE.Box3().setFromObject( label );
		hotspot.add(label);
	}

	if(data.button.icon != null){
		icon = new textSprite(fontAwesome[data.button.icon][0], 80 , showHowColors.purple, false ,"FontAwesome",action ? "#fff" : showHowColors.lightGrey , action ? showHowColors.purple : showHowColors.grey, labelBox  ?"right" : null);
		icon.scale.set(hotspotScaleFactor,hotspotScaleFactor,hotspotScaleFactor);
		icon.material.opacity = hotspot.data.config.opacity || 1;
		if(labelBox != null)
			icon.position.x = labelBox.min.x - ((icon.width  * hotspotScaleFactor)/2);

		hotspot.add(icon);
	}

	if(data.loadSceneId != null){
		var arrow = hotspotArrow(labelBox.min.x/5);
		var box = new THREE.Box3().setFromObject( arrow );
		arrow.position.x = data.button.icon != null ? labelBox.min.x/5 - ((icon.width  * hotspotScaleFactor)/2) - box.min.x : labelBox.min.x/5 - box.min.x;
		arrow.position.y =  labelBox.min.y + box.min.y + 2;
		arrow.position.z = 1;
		hotspot.add(arrow);
	}

	hotspot.hideButton = function(){
		if(label != null)label.visible = false;
		if(icon != null)icon.visible = false;
	}

	hotspot.showButton = function(){
		if(label != null)label.visible = true;
		if(icon != null)icon.visible = true;
	}
}


function textSprite(text,size,color,italics,font,backgroundColor,borderColor,hideEdge,width) {

	console.log(text,font);

	var font = font || "Lato";

	font = size + "px " + font;

	var canvas = createHiDPICanvas(400, 400);
	var context = canvas.getContext('2d');
	context.font = font;
	context.textAlign = "center";

	var metrics = context.measureText(text),
		textWidth = metrics.width;

	canvas.width = width || textWidth + (size/2);

	canvas.height = size * 1.5;
	context.font = font;


	if(typeof hideEdge != 'string')hideEdge = null;

	var radius = 15;

	if(borderColor){
		context.fillStyle = borderColor;

		roundRect(context, 0, 0, canvas.width, canvas.height, {
			tl: hideEdge == "left" ?  null : radius,
			br: hideEdge == "right" ?  null : radius,
			tr: hideEdge == "right" ?  null : radius,
			bl: hideEdge == "left" ?  null : radius
		}, true);
	}

	if(backgroundColor)
	{

		context.fillStyle = backgroundColor;
		if(borderColor)
			roundRect(context,hideEdge ? hideEdge == "left"
					? 0
					: hideEdge == "right"
					? 1
					: 1
					: 1
				, 0, hideEdge ? canvas.width-1 : canvas.width-2, canvas.height, {
					tl: hideEdge == "left" ?  null : radius,
					br: hideEdge == "right" ?  null : radius,
					tr: hideEdge == "right" ?  null : radius,
					bl: hideEdge == "left" ?  null : radius
				}, true);
		else
			roundRect(context, 0, 0, canvas.width, canvas.height, {
				tl: hideEdge == "left" ?  null : radius,
				br: hideEdge == "right" ?  null : radius,
				tr: hideEdge == "right" ?  null : radius,
				bl: hideEdge == "left" ?  null : radius
			}, true);
	}

	context.fillStyle = color;
	context.fillText(text, (canvas.width/2) - (textWidth/2), size + 3);

	var texture = new THREE.Texture(canvas);
	texture.needsUpdate = true;

	var geometry = new THREE.PlaneBufferGeometry(canvas.width, canvas.height);
	//	geometry.center();

	var mesh = new THREE.Mesh(geometry
		,
		new THREE.MeshBasicMaterial({
			map: texture,
			transparent: true,  depthWrite: false
		})
	);

	mesh.width = canvas.width;
	return mesh;
}

function roundRect(ctx, x, y, width, height, radius, fill, stroke) {
	if (typeof stroke == 'undefined') {
		stroke = true;
	}
	if (typeof radius === 'undefined') {
		radius = 5;
	}
	if (typeof radius === 'number') {
		radius = {tl: radius, tr: radius, br: radius, bl: radius};
	} else {
		var defaultRadius = {tl: 0, tr: 0, br: 0, bl: 0};
		for (var side in defaultRadius) {
			radius[side] = radius[side] || defaultRadius[side];
		}
	}
	ctx.beginPath();
	ctx.moveTo(x + radius.tl, y);
	ctx.lineTo(x + width - radius.tr, y);
	ctx.quadraticCurveTo(x + width, y, x + width, y + radius.tr);
	ctx.lineTo(x + width, y + height - radius.br);
	ctx.quadraticCurveTo(x + width, y + height, x + width - radius.br, y + height);
	ctx.lineTo(x + radius.bl, y + height);
	ctx.quadraticCurveTo(x, y + height, x, y + height - radius.bl);
	ctx.lineTo(x, y + radius.tl);
	ctx.quadraticCurveTo(x, y, x + radius.tl, y);
	ctx.closePath();
	if (fill) {
		ctx.fill();
	}
	if (stroke) {
		ctx.stroke();
	}

}

createHiDPICanvas = function(w, h, ratio) {
	if (!ratio) { ratio = PIXEL_RATIO; }
	var can = document.createElement("canvas");
	can.width = w * ratio;
	can.height = h * ratio;
	can.style.width = w + "px";
	can.style.height = h + "px";
	can.getContext("2d").setTransform(ratio, 0, 0, ratio, 0, 0);
	return can;
};

var PIXEL_RATIO = (function () {
	var ctx = document.createElement("canvas").getContext("2d"),
		dpr = window.devicePixelRatio || 1,
		bsr = ctx.webkitBackingStorePixelRatio ||
			ctx.mozBackingStorePixelRatio ||
			ctx.msBackingStorePixelRatio ||
			ctx.oBackingStorePixelRatio ||
			ctx.backingStorePixelRatio || 1;

	return dpr / bsr;
})();

function createHotspotDescription(hotspot){
	if(typeof hotspot.data.description === 'string')
		hotspot.data.description = JSON.parse(hotspot.data.description);

	var  h = 2 * Math.tan( vFOV / 2 ) * circleRadius;
	var a = window.innerWidth > window.innerHeight ? window.innerWidth/ window.innerHeight : window.innerHeight/ window.innerWidth;
	var width = (h * a) * 5;

	if(hotspot.data.media && (hotspot.data.media.image))
			var checkWidth = setInterval(waitForWidth,100);
	else if(fontsAvailable)
		renderHotspotText(hotspot, width);
	else
		waitForWebfonts("Lato",function(){renderHotspotText(hotspot, width);})


	function waitForWidth(){
		if(hotspot.data.media.image){
			if(hotspot.data.media.image.width){
				clearInterval(checkWidth);
				width = hotspot.data.media.image.width;
				renderHotspotText(hotspot, width , true);
			}
		}
		/*		else if(hotspot.data.media.shape){
		 if(hotspot.data.media.shape.width){
		 clearInterval(checkWidth);
		 width = hotspot.data.media.shape.width * 5;
		 renderHotspotText(hotspot, width, true);
		 }
		 }*/
	}
}

function renderHotspotText(hotspot, width, media){

	console.log("renderHotspotText");

	if(renderInProgress === true)
		hotspotRenderList.push({hotspot: hotspot, width : width, media : media});
	else
		render(hotspot,width,media);

	function render(hotspot,width,media) {

		renderInProgress = true;

		var data = hotspot.data;

		var textWidth = width;
		var text = data.description.text;

		if(text.indexOf('\\n') > - 1){

			var lines = text.split("\\n");

			lines.forEach(function(line){

				if(!textWidth)
					textWidth = getTextWidth(line, "90pt Lato");
				else if(getTextWidth(line, "90pt Lato") > textWidth)
					textWidth = getTextWidth(line, "90pt Lato");
			})

			text = lines.join("<br>");
		}
		else
			textWidth = getTextWidth(text, "90pt Lato");

		if (width > textWidth)
			width = textWidth;


		$('#html2canvasRenderer').append('<div id ="hotspot_' + data.id + '"><span>' + text + '</span></div>');

		if(!data.description.setting)
			data.description.setting = {'option':3,'fontcolor': showHowColors.purple,'backgroundcolor': showHowColors.lightGrey};

		$('#hotspot_' + data.id).css({
			'width': width,
			'text-align': width == textWidth ? 'center' : 'left',
			'position': 'absolute',
			'top': '0px',
			'padding': '50px 50px',
			'background-color': data.description.setting.backgroundcolor,
			'color': data.description.setting.fontcolor != null ? data.description.setting.fontcolor : showHowColors.purple,
			'font-size': media ? '50px' : '90px',
			'visibility': 'visible',
			'border-radius' : '15px',
			'text-shadow': data.description.setting.backgroundcolor == null
				? data.description.setting.fontcolor == "#ffffff"
				? '5px 5px 0px #333333, 7px 7px 0px #707070'
				: '5px 5px 0px #eee, 7px 7px 0px #000000'
				: null
		});

		if (data.description.text)
			html2canvas($("#hotspot_" + data.id), {
				onrendered: function (canvas) {

					console.log('Have rendered');

					var texture = new THREE.Texture(canvas);
					texture.needsUpdate = true;

					var textbox = new THREE.Mesh(
						new THREE.PlaneBufferGeometry(canvas.width, canvas.height),
						new THREE.MeshBasicMaterial({
							map: texture,
							transparent: true
						})
					);
					textbox.scale.set(hotspotScaleFactor, hotspotScaleFactor, hotspotScaleFactor);
					hotspot.add(textbox);

					hotspot.hideDescription = function () {

						var o = {scale: hotspot.data.config.scale},t = {scale: 0.1};
						var tween = new TWEEN.Tween(o).to(t,hotspotTweenTime).onUpdate(function(){
							textbox.scale.set(o.scale,o.scale,o.scale);
						}).onComplete(function(){
							textbox.visible = false;
						}).easing(TWEEN.Easing.Cubic.InOut).start();
					}
					hotspot.showDescription = function () {

						textbox.visible = true;
						var o = {scale: 0.1},t = {scale: hotspot.data.config.scale};

						var tween = new TWEEN.Tween(o).to(t,hotspotTweenTime).onUpdate(function(){
							textbox.scale.set(o.scale,o.scale,o.scale);
						}).easing(TWEEN.Easing.Cubic.InOut).start();
					}

					if (hotspot.data.button)
						textbox.visible = false;

					if (hotspot.data.media) {
						if (hotspot.data.media.image) {
							textbox.visible = false;
							textbox.position.y = -(hotspot.data.media.image.height * hotspotScaleFactor) / 2 - (canvas.height * hotspotScaleFactor) / 2;
							textbox.position.z = -1;
						}
					}

					if(hotspot.data.config.opacity)
						hotspot.setOpacity(hotspot.data.config.opacity);

					setTimeout(function () {
						$("#hotspot_" + data.id).remove();

						if(hotspotRenderList.length > 0){
							var next = hotspotRenderList.pop();
							render(next.hotspot,next.width,next.media);
						}
						else
							renderInProgress = false;
					}, 10);
				},

				width: $("#hotspot_" + data.id).width() + 100,
				height: $("#hotspot_" + data.id).height() + 100
			});
	}
}



function getTextWidth(text, font) {
	// re-use canvas object for better performance
	var canvas = getTextWidth.canvas || (getTextWidth.canvas = document.createElement("canvas"));
	var context = canvas.getContext("2d");
	context.font = font;
	var metrics = context.measureText(text);
	return metrics.width;
}

function createImageHotspot(hotspot,file){

	hotspot.data.media.image = {};

	var loader = new THREE.TextureLoader();
	loader.setCrossOrigin("");

	loader.load(
		cdnPrefix + file.name,
		function ( texture ) {

			var media = new THREE.Mesh(
				new THREE.PlaneGeometry(texture.image.width, texture.image.height),
				new THREE.MeshBasicMaterial({
					map: texture,
					transparent: true
				})
			);
			media.scale.set(hotspotScaleFactor,hotspotScaleFactor,hotspotScaleFactor);
			hotspot.add(media);
			hotspot.data.media.image = texture.image;

			hotspot.hideMedia = function(){
				media.visible = false;
			};
			hotspot.showMedia = function(){
				media.visible = true;
			};

			if(hotspot.data.button)
				hotspot.hideMedia();

			if(hotspot.data.config.opacity)
				hotspot.setOpacity(hotspot.data.config.opacity);
		}
	);
}

function createShapeHotspot(hotspot,data){

	hotspot.data.media.shape = {};

	if(data.media.points.length > 3) {

		var mat = new THREE.MeshBasicMaterial({
			color: 0xffffff,
			opacity: 0.00,
			transparent: true
		});

		var pointVectors = [];

		data.media.points.forEach(function(point){
			pointVectors.push(new THREE.Vector3().copy(point));
		});

		var geom = new THREE.ConvexGeometry(pointVectors);
		var mesh = new THREE.Mesh(geom, mat);

		scene.add(mesh);
		hotspot.updateMatrixWorld();
		THREE.SceneUtils.attach(mesh, scene, hotspot);
		mesh.position.z = -2;

		hotspot.hideMedia = function(){
			mesh.visible = false;
		}
		hotspot.showMedia = function(){
			mesh.visible = true;
		}

	}

}


function detectHotpotInCentre(){

	var vrRayCaster = new THREE.Raycaster();
	vrRayCaster.set( camera.getWorldPosition(), camera.getWorldDirection() );
	var intersects = vrRayCaster.intersectObjects( targetList, true );

	if ( intersects.length > 0 )
		hotspotInFocus(intersects[0].object);
	else
		hotspotInFocus(null);

}

function hotspotClick(object){
	console.log('hotspots.hotspotClick()',object);


	if(!object)
		return;

	if(object.tweening || object.sound)
		return;

	if(object.parent)
		if(object.parent.tweening || object.parent.sound)
			return;

	if(typeof object.activate === 'function')
		object.activate();

	if(object.name == "textbox"){
		addMouseScrolling();
		return;
	}


	if(viewingHotspot)
		if((viewingHotspot.data && viewingHotspot == object.parent) || viewingHotspot && object.name == "close") {
			deselectHotspot();
			return;
		}


	if(object.parent.data)
		onHotspotMouseDown(object.parent);
	else if(object.parent.menuData)
		onMenuItemMouseDown(object.parent);
	else if(object.parent.choiceData)
		answerItemHandler(object.parent);
	else if(object.parent.hotspotData)
		onLegacyHotspotMouseDown(object);


	isUserInteracting = false;

}

//var radius = 75,endPercent = 100,circ = Math.PI * 2,quart = Math.PI / 2;
function onHotspotMouseDown(hotspot){

	console.log("onHotspotMouseDown",hotspot);

	if(hotspot.hotspotData){
		onLegacyHotspotMouseDown(hotspot.children[0]);
		return;
	}

	if(!hotspot.data)
		return;

	if(hotspot.sound)
		return;

	if((hotspot.data.loadSceneId || hotspot.data.loadGuideId) && (!hotspot.data.description && !hotspot.data.media)){
		if(hotspot.data.loadSceneId)
			getScene(hotspot.data.loadSceneId);
		if(hotspot.data.loadGuideId){
			sceneId = null;
			isPaused = false;
			loadGuide(hotspot.data.loadGuideId);
		}

		$( document ).trigger("hotspot",hotspot );
	}
	else if(hotspot.data.media) {

		//actions where it starts of as a button
		if (hotspot.data.description){
			if (hotspot.data.button)
				hotspot.hideButton();
			hotspot.showDescription();
			setTimeout(tweenHotpotToCamera, 100, hotspot);
		}
		else if (hotspot.data.media) {
			if (hotspot.data.media.image){
				if(hotspot.data.button)
					hotspot.hideButton();
				hotspot.showMedia();
				setTimeout(tweenHotpotToCamera, 100, hotspot);
			}
			else
				setTimeout(tweenHotpotToCamera, 100, hotspot);
		}



	}
	else if(hotspot.data.description && hotspot.data.button){
		hotspot.hideButton();
		hotspot.showDescription();
		setTimeout(tweenHotpotToCamera, 100, hotspot);
	}
	else{
		$( document ).trigger("hotspot",hotspot );

	}




}

function onLegacyHotspotMouseDown(object){
	//sort bubbles
	console.log(object);

	trackEvent( 'hotspot', 'media', guideId+'|'+ object.parent.hotspotData.id +'|' +currentScene.id );

	if(object.parent.hotspotData.targetSceneId){
		//object.material.opacity = 0.5;
		//tweenToobject.parent(object.parent, function(){checkForSceneToLoad()}, true );
		getScene(object.parent.hotspotData.targetSceneId);
		trackEvent( 'hotspot', 'bubble', guideId+'|'+ object.parent.hotspotData.targetSceneId +'|' +currentScene.id );
		$( document ).trigger( "hotspot" );
	}
	//everything else tweens
	else if(!viewingHotspot){
		//if unselectable tween camera towards hotspot then zoom
		if(object.parent.hotspotData.selectable == false){
			$( document ).trigger( "hotspot" );
			//tweenToobject.parent(object.parent, function(){tweenHotpotToCamera( object.parent ); if(object.parent.hotspotData.buttonSceneId) checkForSceneToLoad(); }, true );
			if(object.parent.hotspotData.buttonSceneId != null)
				getScene(object.parent.hotspotData.buttonSceneId);
		}
		else
			tweenHotpotToCamera( object.parent );
	}
}


function hotspotInFocus(object){

	if(object != null && viewingHotspot == null && (object.name !== 'textbox')){

		if(object.material.opacity < 0.1)
			return;

		if(gameTask != null && object.parent != null)
			if(gameTask.hotspotsToFind != null && object.parent.hotspotData != null)
				if(!gameTask.hotspotsToFind.some(function(item){return item.id == object.parent.hotspotData.id}))
					return;

		if(selectedHotspot == null) {

			selectedHotspot = object.name == 'homeButton' ? object :  object.parent;

			if(selectedHotspot.menuData != null && (selectedHotspot.menuData.sceneId === currentScene.id))
				return;

			setTimeout(function(){
				if(selectedHotspot != null && (selectedHotspot.active == true)){

					//camera hasn't moved since hotspot was added.. the user hasn't yet selected this
					if(selectedHotspot.cameraQuaternion != null && (selectedHotspot.cameraQuaternion.equals(camera.quaternion)))
						return;

					if(selectedHotspot.hotspotData)
						if(!selectedHotspot.hotspotData.media || !selectedHotspot.hotspotData.button)
							return;

					pause();
					targetCircle.activate();

					//answers rotate towards camera - this probably shouldn't go here
					if(selectedHotspot.choiceData != null){
							var vector = selectedHotspot.worldToLocal( camera.getWorldPosition() );
							vector.y = -1.5;
							selectedHotspot.lookAt( vector );
						}
				}
			},10);
		}
	}
	else if(selectedHotspot != null && viewingHotspot == null)
	{
		if(selectedHotspot.sound != null)
			return;

		if(selectedHotspot == homeButton)
			selectedHotspot = null;

		targetCircle.deactivate();

		if(selectedHotspot.choiceData != null){
			selectedHotspot.rotation.set(0,0,0);
		}

		selectedHotspot = null;

		setTimeout(function(){
			if(menuActive == false)
				play();
		},200);

	}
	else if(object == null && viewingHotspot != null){
		if(viewingHotspot.tweening == false && viewingHotspot.sound == null){
			deselectHotspot("hotspotInFocus now out of focus");
		}

	}

}

function tweenHotpotToCamera(object){
	console.log('hotspots.tweenHotpotToCamera('+ object.toString() +')');

	viewingHotspot = object;

	pause();

	viewingHotspot.tweening = true;
	updateShader(-0.5,-0.5);

	object.originalPosition = object.position.clone();
	object.originalQuaternion = object.quaternion.clone();

	//magic maths to get objects around the same size of the screen
	var vFOV = camera.fov * Math.PI / 180;
	var pLocal,cPos;
	var bbox=getCompoundBoundingBox( object );
	var sizeY = bbox.max.y-bbox.min.y;
	var sizeX= bbox.max.x-bbox.min.x;

	sizeX*=object.scale.x;
	sizeY*=object.scale.y;

	// ALEX
	// assuming FOV is vertical
	var ratio=window.innerWidth/window.innerHeight;
	var uSpace= vrMode ? 1 : 0.5;//used space
	vFOV/=2;
	sizeY/=2;sizeX/=2;// half size
	sizeY*=uSpace;sizeX*=uSpace;

	var tanFov=Math.tan( vFOV );
	var distY=sizeY/tanFov;
	var distX=sizeX/(ratio*tanFov);

	if(distX<distY)distX=distY;
		pLocal= new THREE.Vector3( 0, 0, -distX );

	cPos = camera.position.clone();
	cPos.y -= 1.5;

	//apply the direction the camera is facing
	var target = pLocal.applyMatrix4( camera.matrixWorld );
	var targetLook = cPos.applyMatrix4( camera.matrixWorld );


	object.position.copy(target);
	object.lookAt(targetLook);
	var targetRotation=object.quaternion.clone();
	object.position.copy(object.originalPosition);
	object.quaternion.copy(object.originalQuaternion);

	var tweenMove = new TWEEN.Tween(object.position).to(target, hotspotTweenTime).easing(TWEEN.Easing.Cubic.InOut);

	tweenMove.onUpdate(function(){
		var dstF=object.originalPosition.distanceTo(target);
		var dstC=object.originalPosition.distanceTo(object.position);
		var k=dstC/dstF;
		THREE.Quaternion.slerp(object.originalQuaternion,targetRotation,object.quaternion,k);
	});
	tweenMove.onComplete(function(){
		if(viewingHotspot)
			viewingHotspot.tweening = false;
		else
			return;

		//play any sounds
		if(viewingHotspot.data.media){
			if(viewingHotspot.data.media.files)
				viewingHotspot.data.media.files.some(function(file){

					if(soundFiles.indexOf(file.type) > -1){

						viewingHotspot.sound = {};

						if(waitingSound){
							waitingSound.stop();
							waitingSound = null;
						}

						if(createjs.Sound.loadComplete(cdnPrefix + file.name)){
							viewingHotspot.sound = createjs.Sound.play(file.name);
							viewingHotspot.sound.on("complete",onComplete);
						}
						else{
							if(queue)
								queue.setPaused(true);
							createjs.Sound.registerSound(cdnPrefix + file.name, file.name); // url, id
							createjs.Sound.addEventListener("fileload", handleFileLoad);
							function handleFileLoad(event) {
								createjs.Sound.removeEventListener("fileload", handleFileLoad);
								if(viewingHotspot){
									viewingHotspot.sound = createjs.Sound.play(file.name);
									viewingHotspot.sound.on("complete",onComplete);
								}

							}

						}

						function onComplete(){
							if(!viewingHotspot)
							return;

							viewingHotspot.sound = null;

							if(viewingHotspot.data.loadSceneId)
								getScene(viewingHotspot.data.loadSceneId);
							else
								deselectHotspot("sound is complete");
						};

					}
					else
						waitForMovement(0.5,deselectHotspot);
				})
		}
		else if(viewingHotspot.data.loadSceneId)
			getScene(viewingHotspot.data.loadSceneId);
		else
			waitForMovement(0.5,deselectHotspot);
	});
	tweenMove.start();
	object.rotationAmount=0;

	if(object.getOpacity() < 1){
		var o ={opacity : object.getOpacity()},t = {opacity : 1}
		var tweenOpacity = new TWEEN.Tween(o).to(t, hotspotTweenTime).easing(TWEEN.Easing.Cubic.InOut).onUpdate(function(){
			object.setOpacity(o.opacity)
		}).start();
	}

}



function tweenToSelectedHotspot(object , callback, zoom, auto){
	console.log('tweenToSelectedHotspot('+ object.hotspotData.title +')');

	return;

	var newLatLon = {lat: lat, lon : lon};
	var targetLatLon = hotspotPanTiltToLatLon(object.hotspotData.pan,object.hotspotData.tilt );
	var newFov = {nFov : camera.fov};
	var targetFov = {nFov : object.hotspotData.distance*0.03};


	//if its been spun around a lot this number gets big
	if(Math.abs(newLatLon.lon) > 360){
		var f = newLatLon.lon/360;
		f = f - Math.floor(f);
		newLatLon.lon = f * 360;
	}

	if(newLatLon.lon < 0)
		newLatLon.lon += 360;

	if(Math.abs(newLatLon.lon - targetLatLon.lon) > 180)
		targetLatLon.lon += targetLatLon.lon > 360 ? -360 : 360;

	var diff = newLatLon.lon - targetLatLon.lon;

	var time = auto == true ? Math.abs(diff) *  150 : Math.abs(diff) * 100;

	var loadsScene = selectedHotspot.hotspotData.targetSceneId != null;

	//set up zoom tween
	var tweenZoom = new TWEEN.Tween( newFov ).to( targetFov, time ).easing(TWEEN.Easing.Cubic.Out);
	tweenZoom.onUpdate(function(){
		camera.fov = newFov.nFov;
		camera.updateProjectionMatrix();
	});

	//tween lon/lat to tween camera
	var tweenLon = new TWEEN.Tween(newLatLon).to(targetLatLon, time).easing(TWEEN.Easing.Cubic.Out);;
	tweenLon.onUpdate(function(){
		lon = newLatLon.lon;
		lat = newLatLon.lat;

		if(loadsScene && sceneReady)
			loadScene();
		else if( !loadsScene && lon < newLatLon.lon + 5 && lon > newLatLon.lon - 5 && zoom)
			tweenZoom.start();
	});
	tweenLon.onComplete(function(){if(callback)callback.call(); $( document ).trigger( "hotspot" )});
	tweenLon.start();

}

function checkForSceneToLoad(){
	console.log('checkForSceneToLoad()');
	if(sceneReady)
		loadScene();
	else if(selectedHotspot)
		setTimeout(checkForSceneToLoad,100);
}

function deselectHotspot(i)
{

	console.log('deselect',viewingHotspot,i);

	if(viewingHotspot == null || viewingHotspot.sound != null)
		return;

	$(document).trigger('hotspot',viewingHotspot );

	viewingHotspot.tweening = true;

	updateShader(0, 0);

	if(viewingHotspot.hotspotData != null)
		if(viewingHotspot.hotspotData.selectable == true)
			$( document ).trigger( "hotspot" );

	var shs = viewingHotspot;
	viewingHotspot = null;

	var srcRotation=shs.quaternion.clone();
	//var targetRotation=shs.originalQuaternion.clone();
	var srcPos=shs.position.clone();

	if((shs.data.button != null || (shs.data.media != null  && (shs.data.media.image != null ))) && shs.data.description != null )
		shs.hideDescription();

	var tweenMove=new TWEEN.Tween(shs.position).to(shs.originalPosition, 1000).easing(TWEEN.Easing.Cubic.InOut);
	tweenMove.onComplete(function(){

		if(shs.data == null)
			return;

		if(inIframe())
			if((shs.data.button != null  && shs.data.media != null ))
				shs.hideMedia();

		if(shs.data.button != null )
			shs.showButton();

		shs.tweening = false;
	});

	tweenMove.onUpdate(function(){
		if(shs != null){
			var dstF=srcPos.distanceTo(shs.originalPosition);
			var dstC=srcPos.distanceTo(shs.position);
			var k=dstC/dstF;
			THREE.Quaternion.slerp(srcRotation,shs.originalQuaternion,shs.quaternion,k);//
		}
	});
	tweenMove.start();

	if(shs.data != null )
		if(shs.data.config.opacity < 1){
			var o ={opacity : 1},t = {opacity : shs.data.config.opacity}
			var tweenOpacity = new TWEEN.Tween(o).to(t, hotspotTweenTime).easing(TWEEN.Easing.Cubic.InOut).onUpdate(function(){
				shs.setOpacity(o.opacity)
			}).start();
		}
}


function powerOf2Down(value)
{
	if(value < 80)
		return 64;
	else if(value < 150)
		return 128;
	else if(value < 400)
		return 256;
	else if(value < 800)
		return 512;

	return 1024;
}

function powerOf2Up(value)
{
	if(value <= 64)
		return 64;
	else if(value <= 128)
		return 128;
	else if(value <= 256)
		return 256;
	else if(value <= 512)
		return 512;

	return 1024;
}
function getCompoundBoundingBox(object3D) {
	var box = null;
	object3D.traverse(function (obj3D) {

		var geometry = obj3D.geometry;
		if (geometry === undefined) return;
		geometry.computeBoundingBox();
		if (box === null) {
			box = geometry.boundingBox;
		} else {
			box.union(geometry.boundingBox);
		}
	});
	return box;
}

function hotspotArrow(width){
	var geometry = new THREE.Geometry();
	var v1 = new THREE.Vector3(0,-width/4,0);
	var v2 = new THREE.Vector3(-width,-width,0);
	var v3 = new THREE.Vector3(width,-width,0);

	geometry.vertices.push(v1);
	geometry.vertices.push(v2);
	geometry.vertices.push(v3);

	geometry.faces.push(new THREE.Face3(0, 1, 2));
	geometry.center();

	var arrow = new THREE.Object3D();
	var mat = new THREE.MeshBasicMaterial({color: "#fff"});
	mat.side  = THREE.DoubleSide;
	var triangle = new THREE.Mesh(geometry, mat);

	arrow.add(triangle);
	return arrow;
}




