	function createPhoto(data){
	   
		var photo = new THREE.Object3D();
			photo.hotspotData = data;
			photo.position.copy( panTiltToVector( data.pan, data.tilt, 
                //(data.distance / 2500) * 1024/*/1.491954023*/) 
                data.distance *0.503122703*Math.cos(Math.PI*currentScene.fov/360)
                ));
            console.log(JSON.stringify(data));

		if(data.rotationX)   photo.rotation.x = -data.rotationX;
		   photo.rotation.y = data.rotationY != null? Math.PI - data.rotationY : 0;
		   photo.rotation.order="YXZ";
   
   //photo.scale.x=photo.scale.y=   photo.scale.z=0.71;
   //var k=data.distance/2500;



			//for those pesky ones with no values
			if(photo.rotation.x == 0 && photo.rotation.y == 0 && photo.rotation.z == 0)
				photo.lookAt( camera.position );
		

	    var image = new Image();
		    image.crossOrigin = '';
		    image.onload = function() {
		    	
		    	 var texture = new THREE.Texture(texture_placeholder);
		    	 var photoMaterial = new THREE.MeshBasicMaterial({
		    		 map: texture,
		 	        transparent: true
		 	    });
		    	
		        texture.image = this;
		        texture.needsUpdate = true;
		        photoMaterial.side = THREE.DoubleSide;

		        var factor,w,h,scale;
			   factor=this.width>this.height?this.width:this.height;
	                factor=512/factor;


			w=this.width*0.05*factor;
			h=this.height*0.05*factor;
		        
			var photoGeom = new THREE.PlaneBufferGeometry( w  , h  );

		    	var photoMesh = new THREE.Mesh( photoGeom, photoMaterial );
		    	
		    	photo.castShadow = true;
		    	photo.frustumCulled = true;
				photo.width = w;
				photo.height = h;
				photo.add( photoMesh );
		        scene.add( photo );
		        
		    };
		    
		    image.src = cdnPrefix  + '/' +  data.file.realName;

		photo.disable = function(){photo.visible = false}
		photo.enable = function(){photo.visible = true}

    	targetList.push( photo );
			
	}