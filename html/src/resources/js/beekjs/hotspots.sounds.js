var context, 
	volume = 0.2, 
	initialised = false,
	enabled = true,
	voiceoverToPlay,
	sceneSounds = [];


	function createSound(data){
		
		console.log( 'createSound()' );
		
		if(!data.file)
			return;
		
		if(!initialised)
			initialiseWebAudio();
		
		var soundFileName = cdnPrefix + '/' + data.file.realName;
		console.log(soundFileName);
		
		loadSound( soundFileName, data );
	};
	
	function loadSound(url, data ) {
	    var request = new XMLHttpRequest();
	    request.open('GET', url, true);
	    request.responseType = 'arraybuffer';
	    // Decode asynchronously
	    request.onload = function() {
	    	if(context)
	        context.decodeAudioData(request.response, function(buffer) {
	            myAudioBuffer = buffer;
	            playSound(myAudioBuffer, data);//Play Audio Track
	        });
	    }
	    request.send();
	 }
	
	function playSound(buffer,  data) {
		
		if(!enabled)
			return;
		
		var sound = {};
		sound.source = context.createBufferSource();
		sound.volume = context.createGain();
		sound.source.connect(sound.volume);
		sound.volume.connect(context.destination);
		sound.volume.gain.value = volume * (1 - (data.distance / 2000) );
		sound.source.buffer = buffer;
		sound.data = data;
		
	     if(data.looping == '1')
	    	 sound.source.loop = true;
	     
	     //sound.source.connect(context.destination);
	     sound.source.start();
	     
	     sceneSounds.push( sound );
	 }
	
	
	function initialiseWebAudio(){
		
		initialised = true;
		// Create a new audio context.
		 try {
			    // Fix up for prefixing
			    window.AudioContext = window.AudioContext||window.webkitAudioContext;
			    context = new AudioContext();
				context.listener.setPosition(0, 0, 0);
				mainVolume = context.createGain();
				mainVolume.connect(context.destination);
				mainVolume.gain.value = 0.5;

			 	if(voiceoverToPlay)
					loadVoiceover(voiceoverToPlay);

			  }
			  catch(e) {
				  enabled = false;
		  }
		
	}	
	
	
	function updateSceneSounds( lon )
	{
	
		for(var s in sceneSounds){
			var sound = sceneSounds[s];

			if(sound.data){
			
			var soundPan = sound.data.pan;
			var soundVol = 1 - (sound.data.distance / 2000);

			if (soundPan < 0)
				soundPan = (soundPan % 360) + 360;
			else
				soundPan = soundPan % 360;

			
			var distance = Math.abs(soundPan - (lon + 180)) % 360;
	
			if(distance > 180)
				distance = 180 - (distance - 180);
				
			var panVolume = (1- distance/180) * (sound.data.ambience * sound.data.ambience);

			//Turn off 3D sound when ambiance is 1
			if(sound.data.ambience == 1)
				panVolume = 1;
			
			}
			else
				{
				soundVol = 1;
				panVolume = 1;
				
				}
				
			
			// Final compiled volume.
			sound.volume.gain.value = (volume * soundVol * panVolume);
			

		}
	}
	
	
	
	function loadVoiceover(voiceover)
	{
		console.log('hotspots.sounds.loadVoiceover()');
		if(!initialised){
			initialiseWebAudio();
			voiceoverToPlay = voiceover;
		}

		voiceoverToPlay = null;

		var url = cdnPrefix + '/' + voiceover;
		
	    var request = new XMLHttpRequest();
	    request.open('GET', url, true);
	    request.responseType = 'arraybuffer';
		console.log(url);
	    // Decode asynchronously
	    request.onload = function() {
	        context.decodeAudioData(request.response, function(buffer) {
	            myAudioBuffer = buffer;
	            playVoiceover(myAudioBuffer);//Play Audio Track
	        });
	    }
	    request.send();
	}
	
	function playVoiceover(buffer)
	{
		
		if(!initialised)
			initialiseWebAudio();
		
		var sound = {};
		sound.source = context.createBufferSource();
		sound.volume = context.createGain();
		sound.source.connect(sound.volume);
	    sound.source.loop = true;
		sound.source.buffer = buffer;
		sound.volume.connect(context.destination);
		sound.volume.gain.value = 1;

	    
		sound.source.start();
	    sceneSounds.push( sound );
		
	}
	