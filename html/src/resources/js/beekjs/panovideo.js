var video, videoSettings, source, panoVideoMesh, panoVideoShader, firstPlay = true;

var checkInterval  = 50.0,lastPlayPos = 0,currentPlayPos = 0,bufferingDetected = false,bufferCheck;

function panovideo(data) {
    console.log('panovideo()',data);

   if(!video){
        video = document.createElement( 'video' );
        source = document.createElement('source');
        video.crossOrigin = "anonymous";
        video.preload = 'auto';
        video.setAttribute('webkit-playsinline', 'true');
        video.setAttribute('playsinline', 'true');
        source.setAttribute('type',"video/mp4");
       video.addEventListener('ontimeupdate',onTimeUpdate);
   }
    else if(video)
    video.pause();

    videoSettings = {};
    var filename;


    try {

        if(data.charAt(0) == '"')
            data = data.substr(1,data.length-2);

        videoSettings = JSON.parse(data.replace(/\\/g, ''));

        if(typeof videoSettings === 'string')
            videoSettings = JSON.parse(videoSettings);

        currentScene.video = videoSettings;
    } catch (e) {
        filename = data.video;
    }

    if(!webApp && videoSettings.lowRes)
        filename = videoSettings.lowRes;
    else if(isMobile && videoSettings.lowRes)
        filename = videoSettings.lowRes;
    else if(!isMobile && videoSettings.hiRes)
        filename = videoSettings.hiRes;
    else if(videoSettings.filename)
        filename =videoSettings.filename;


   if((isIOS() || isIE()) && webApp)
       cdnPrefix = "http://" + document.domain + "/cdn";

    var serverPath = cdnPrefix;

    var exists = false;

    if(!webApp && (storedMediaFiles.length))
    {
        exists = storedMediaFiles.some(function(fileEntry){
            if(fileEntry.name == filename){
                source.setAttribute('src', fileEntry.nativeURL);
                return true;
            }
        });
    }

    if(!exists){
        if(queue) {
            var loaded = queue.getResult(filename);
            if (loaded)
                source.src = loaded.src;
            else
                source.setAttribute('src', serverPath + filename);
        }
        else
            source.setAttribute('src', serverPath + filename);
    }




    enableInlineVideo(video);

    video.appendChild(source);
    video.load();

    if(bufferCheck)
        clearInterval(bufferCheck);

    (function () {

        THREE.noVideoTextureSupport = isIE11();


        THREE.VideoTexture = (function () {
            var _videoTexture = THREE.VideoTexture;
            var _ieVideoTexture = function ( video, mapping, wrapS, wrapT, magFilter, minFilter, format, type, anisotropy ) {
                if (THREE.noVideoTextureSupport) {
                    var scope = this;

                    scope.video = video;
                    scope.ctx2d = document.createElement('canvas').getContext('2d');
                    var canvas = scope.ctx2d.canvas;
                    canvas.width = 2048;
                    canvas.height = 1024;

                    scope.ctx2d.drawImage(scope.video, 0, 0, canvas.width, canvas.height);
                    THREE.Texture.call( scope, scope.ctx2d.canvas, mapping, wrapS, wrapT, magFilter, minFilter, format, type, anisotropy );

                    scope.generateMipmaps = false;

                    function update() {
                        requestAnimationFrame( update );
                        if ( video.readyState >= video.HAVE_CURRENT_DATA ) {
                            scope.ctx2d.drawImage(scope.video, 0, 0, canvas.width, canvas.height);
                            scope.needsUpdate = true;
                        }
                    }

                    update();
                }
            };
            return (THREE.noVideoTextureSupport ? _ieVideoTexture : _videoTexture);
        })();

        THREE.VideoTexture.prototype = Object.create( THREE.Texture.prototype );
        THREE.VideoTexture.prototype.constructor = THREE.VideoTexture;

    })();


    if(firstPlay){
        setTimeout(function(){
            hideLoader();
            $('#preloader').append('<div id="play-button" class="playButton"><i class="fa fa-play"></i></div>');

            if(isIOS() == false)
                $('#play-button').on("click",touchStart);
            else
                $('#play-button').on("touchstart", touchStart);

            if($('.userIdInput').length)
                $('#play-button').hide();

        },100)

    }
    else
        panoVideoPlay();

}

function touchStart(event) {

    iosDebug('touchStart()')

    $("#play-button").remove();
    showLoader();
    firstPlay = false;

    if(video != null){
        video.play();

        checkPlayBack();
    }

    //preloadMediaFiles();
}

function panoVideoPlay(){
    iosDebug('panoVideoPlay');
    checkPlayBack();
    video.play();
}

var checkPlayBack = function(event){

    var panoPlayBackInterval = setInterval(function(){
       // iosDebug(video.currentTime);
        if (video.currentTime > 0 || (firstPlay && video.readyState > 0)){
            clearInterval(panoPlayBackInterval);
            //  video.pause();
            showVideo(video);
            bufferCheck = setInterval(checkBuffering, checkInterval);
        }
    },100);
}

function showVideo(video){

    iosDebug('showVideo');

    var panoTexture = new THREE.VideoTexture( video );
        panoTexture.minFilter = THREE.LinearFilter;
        panoTexture.magFilter = THREE.LinearFilter;

    panoShader = THREE.BrightnessContrastShader;
    panoShader.uniforms[ "brightness" ].value = -1;

    if(currentScene.video instanceof Object){
        panoShader.uniforms[ "contrast" ].value = currentScene.video.contrast | 0;
        panoShader.uniforms[ "hue" ].value = currentScene.video.hue | 0;
        panoShader.uniforms[ "saturation" ].value = currentScene.video.saturation | 0;
    }
    panoShader.uniforms[ "tDiffuse" ].value = panoTexture;

    panoVideoMesh = new THREE.Mesh( new THREE.SphereGeometry( 500, 60, 40 ), new THREE.ShaderMaterial(panoShader) );
    panoVideoMesh.scale.x = -1;
    panoVideoMesh.rotation.y = Math.PI / 2;
    panoVideoMesh.receiveShadow = true;

    loadScene();
    scene.remove(box);
    scene.add(panoVideoMesh);

    if(videoSettings.volume != null)video.volume = videoSettings.volume;

    if(videoSettings.startTime != null)video.currentTime = videoSettings.startTime;

    video.ontimeupdate = function() {onTimeUpdate()};
    // video.onended = function(){videoEnded()];

  if(isPaused === true)
    video.pause();

    if(voiceover != null && isPaused === false)
        voiceover.play();

    hideLoader();

    if(preloaderLocked == false)
        removePreloader();



}

function onTimeUpdate(e){



    if (videoSettings.endTime){
        if (videoSettings.endTime > 0)
            if (video.currentTime >= videoSettings.endTime)
                videoEnded(1);
    }
    else if(video.currentTime >= video.duration - 1)
        videoEnded(2);

    if(videoSettings.focus)
        instructionArrow.update(videoSettings.focus.points,video.currentTime);

    $(document).trigger('videoTimeUpdate',video.currentTime);
}

function videoEnded(e){
   // iosDebug("videoEnded()",arguments.callee.caller.name,e);
    console.log('videoEnded',e);
    video.ontimeupdate = null;
    video.pause();
    $(document).trigger('videoFinished');

/*    setTimeout(function(){
        if(loadingSceneId == currentScene.id){
            clearScene();
            video.currentTime = videoSettings.startTime || 0;
            createScene();
        }
    },100)*/

}

function checkBuffering() {
    currentPlayPos = video.currentTime;

    var offset = 1 / checkInterval;

    if (
        !bufferingDetected
        && currentPlayPos < (lastPlayPos + offset)
        && !video.paused
    ) {
       // iosDebug('buffer paused');
        $(document).trigger("panoVideoPause");
        bufferingDetected = true;
    }

    if (
        bufferingDetected
        && currentPlayPos > (lastPlayPos + offset)
        && !video.paused
    ) {
        $(document).trigger("panoVideoPlay");
        bufferingDetected = false;
    }
    lastPlayPos = currentPlayPos;
}



/*! npm.im/iphone-inline-video 2.0.1 */
var enableInlineVideo=function(){"use strict";/*! npm.im/intervalometer */
    function e(e,i,n,r){function t(n){d=i(t,r),e(n-(a||n)),a=n}var d,a;return{start:function(){d||t(0)},stop:function(){n(d),d=null,a=0}}}function i(i){return e(i,requestAnimationFrame,cancelAnimationFrame)}function n(e,i,n,r){function t(i){Boolean(e[n])===Boolean(r)&&i.stopImmediatePropagation(),delete e[n]}return e.addEventListener(i,t,!1),t}function r(e,i,n,r){function t(){return n[i]}function d(e){n[i]=e}r&&d(e[i]),Object.defineProperty(e,i,{get:t,set:d})}function t(e,i,n){n.addEventListener(i,function(){return e.dispatchEvent(new Event(i))})}function d(e,i){Promise.resolve().then(function(){e.dispatchEvent(new Event(i))})}function a(e){var i=new Audio;return t(e,"play",i),t(e,"playing",i),t(e,"pause",i),i.crossOrigin=e.crossOrigin,i.src=e.src||e.currentSrc||"data:",i}function o(e,i,n){(m||0)+200<Date.now()&&(e[b]=!0,m=Date.now()),n||(e.currentTime=i),w[++T%3]=100*i|0}function u(e){return e.driver.currentTime>=e.video.duration}function s(e){var i=this;i.video.readyState>=i.video.HAVE_FUTURE_DATA?(i.hasAudio||(i.driver.currentTime=i.video.currentTime+e*i.video.playbackRate/1e3,i.video.loop&&u(i)&&(i.driver.currentTime=0)),o(i.video,i.driver.currentTime)):i.video.networkState===i.video.NETWORK_IDLE&&0===i.video.buffered.length&&i.video.load(),i.video.ended&&(delete i.video[b],i.video.pause(!0))}function c(){var e=this,i=e[y];return e.webkitDisplayingFullscreen?void e[g]():("data:"!==i.driver.src&&i.driver.src!==e.src&&(o(e,0,!0),i.driver.src=e.src),void(e.paused&&(i.paused=!1,0===e.buffered.length&&e.load(),i.driver.play(),i.updater.start(),i.hasAudio||(d(e,"play"),i.video.readyState>=i.video.HAVE_ENOUGH_DATA&&d(e,"playing")))))}function v(e){var i=this,n=i[y];n.driver.pause(),n.updater.stop(),i.webkitDisplayingFullscreen&&i[E](),n.paused&&!e||(n.paused=!0,n.hasAudio||d(i,"pause"),i.ended&&(i[b]=!0,d(i,"ended")))}function p(e,n){var r=e[y]={};r.paused=!0,r.hasAudio=n,r.video=e,r.updater=i(s.bind(r)),n?r.driver=a(e):(e.addEventListener("canplay",function(){e.paused||d(e,"playing")}),r.driver={src:e.src||e.currentSrc||"data:",muted:!0,paused:!0,pause:function(){r.driver.paused=!0},play:function(){r.driver.paused=!1,u(r)&&o(e,0)},get ended(){return u(r)}}),e.addEventListener("emptied",function(){var i=!r.driver.src||"data:"===r.driver.src;r.driver.src&&r.driver.src!==e.src&&(o(e,0,!0),r.driver.src=e.src,i?r.driver.play():r.updater.stop())},!1),e.addEventListener("webkitbeginfullscreen",function(){e.paused?n&&0===r.driver.buffered.length&&r.driver.load():(e.pause(),e[g]())}),n&&(e.addEventListener("webkitendfullscreen",function(){r.driver.currentTime=e.currentTime}),e.addEventListener("seeking",function(){w.indexOf(100*e.currentTime|0)<0&&(r.driver.currentTime=e.currentTime)}))}function l(e){var i=e[y];e[g]=e.play,e[E]=e.pause,e.play=c,e.pause=v,r(e,"paused",i.driver),r(e,"muted",i.driver,!0),r(e,"playbackRate",i.driver,!0),r(e,"ended",i.driver),r(e,"loop",i.driver,!0),n(e,"seeking"),n(e,"seeked"),n(e,"timeupdate",b,!1),n(e,"ended",b,!1)}function f(e,i){if(void 0===i&&(i={}),!e[y]){if(!i.everywhere){if(!h)return;if(!(i.iPad||i.ipad?/iPhone|iPod|iPad/:/iPhone|iPod/).test(navigator.userAgent))return}!e.paused&&e.webkitDisplayingFullscreen&&e.pause(),p(e,!e.muted),l(e),e.classList.add("IIV"),e.muted&&e.autoplay&&e.play(),/iPhone|iPod|iPad/.test(navigator.platform)||console.warn("iphone-inline-video is not guaranteed to work in emulated environments")}}var m,h="object-fit"in document.head.style&&!matchMedia("(-webkit-video-playable-inline)").matches,y="bfred-it:iphone-inline-video",b="bfred-it:iphone-inline-video:event",g="bfred-it:iphone-inline-video:nativeplay",E="bfred-it:iphone-inline-video:nativepause",w=[],T=0;return f}();








