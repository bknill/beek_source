	var sceneReady = false,
		firstRun = true,
		sceneLoaded = false,
		loadingSceneId,
		currentScene,
		sphere,
		boards,
		bubbles,
		historyScenes = [],
		panoShader,
		sceneId,
		voiceover,
		voiceoverTimer;


	function getScene(id) {
		console.log('scene.getScene('+id+')' );

	if(id == loadingSceneId)
			return;

		updateShader(-0.9,-0);
		pause();

		if(hotspots.length > 0)
		hotspots.forEach(function(hotspot){scene.remove(hotspot)});

		if(webApp || (!webApp && !isOffline())){
			var jsonpURL = jsonpPrefix + '/scene/' + id + '/jsonp?callback=?';
			$.getJSON(jsonpURL, $.proxy(startScene, this));
		}
		else
			checkForOfflineScene(id);


	    sceneId = loadingSceneId = id;

	};


	function startScene(sceneObject) {
		console.log('scene.startScene()',sceneObject );

		currentScene = sceneObject;

		sceneLoaded = false;

		historyScenes.push( sceneObject.id );

		if(sceneObject != null && webApp == true && gameInProgress != true)
			$.address.value('g' + guideId + '/s' + sceneObject.id);

		if(getGuideScene(currentScene.id).video != null  && (getGuideScene(currentScene.id).video.length > 0))
			panovideo(getGuideScene(currentScene.id).video);
		else if(currentScene.outputVideo != null)
			panovideo(currentScene.outputVideo);
		else if(currentScene.video != null)
			panovideo(currentScene.video);

		else{

			var filename = 'scene_' + currentScene.id + "_pano_" + currentScene.panoIncrement + "_EX.jpg";

			if(!webApp && (storedMediaFiles.length)){
					storedMediaFiles.some(function(fileEntry){
						if(fileEntry.name == filename){
							loadSphereTexture(fileEntry.nativeURL);
							return true;
						}
					});
			}
			else if(queue) {
					var loaded = queue.getResult(filename);

					if (loaded)
						loadSphereTexture(loaded.src);
					else
						loadPanoTiles();
				}
			else
				loadPanoTiles();
		}

		updateMenuItems(sceneObject.id);

		updateShader(-1,0,true);

		setDefaults();

	}


	function loadPanoTiles(){
		console.log('loadPanoTiles');
		//get tiles
		tiles = [];
		materials = [];
		previewTileCounter = 0;
		for (var face in FACES) {
			materials.push(loadTexture(cdnPrefix + 'scene_' + currentScene.id + "_pano_" +
			currentScene.panoIncrement + "_" + FACES[face] + "/9/0_0.jpg",
				function() {
					previewTileCounter++;
					if (previewTileCounter == 6) {

						sceneReady = true;
						loadScene();
						createBox();
						loadSphereTexture(cdnPrefix + 'scene_' + currentScene.id + "_pano_" + currentScene.panoIncrement + "_EX.jpg",currentScene.id);
					}
				}
			));
			for (var tile in TILES) tiles.push(
				cdnPrefix + 'scene_' + currentScene.id + "_pano_" + currentScene.panoIncrement + "_" + FACES[face] + "/10/" + TILES[tile] + ".jpg"
			);
		}
	}


	function loadSphereTexture(path) {
		console.log(path);

		$.ajax({
			url:path,
			type:'HEAD',
			error: function()
			{

			},
			success: function()
			{
				var id = currentScene.id;
				THREE.ImageUtils.crossOrigin = '';
				var texture = THREE.ImageUtils.loadTexture(path, {} ,function(){

					if(id != currentScene.id)
						return;

					panoShader = THREE.BrightnessContrastShader;
					panoShader.uniforms[ "brightness" ].value = 0;
					panoShader.uniforms[ "contrast" ].value = 0;
					panoShader.uniforms[ "tDiffuse" ].value = texture;
					var panoMaterial = new THREE.ShaderMaterial(panoShader);

					createSphere(panoMaterial);
				});

				texture.magFilter = THREE.LinearFilter;
				texture.minFilter = THREE.NearestFilter;
			}
		});
	}

	function processHotspots(sceneObject){

		console.log("hotspots");

		for (i in sceneObject.hotspots)
			createHotspot(sceneObject.hotspots[i]);

		//get bubbles
        bubbles = [];
        for (i in sceneObject.bubbles)
        	bubbles.push(sceneObject.bubbles[i]);

        //get boards
        boards = [];
        for (i in sceneObject.boards)
            boards.push(sceneObject.boards[i]);

        //get photos
        photos = [];
        for (i in sceneObject.photos)
            photos.push(sceneObject.photos[i]);

        //get posters
        posters = [];
        for (i in sceneObject.posters)
            posters.push(sceneObject.posters[i]);

        //get sounds
        sounds = [];
        for (i in sceneObject.sounds)
            sounds.push(sceneObject.sounds[i]);

		legacyHotspots();

	}


	function clearScene() {
		console.log('scene.clearScene()');

		selectedHotspot = null;
		viewingHotspot = null;
		//targetList = [];

/*		if(sceneSounds){
				for(i in sceneSounds)
					sceneSounds[i].source.stop();

			sceneSounds = [];
		}*/

		if(voiceover != null)
			clearInterval(voiceoverTimer);


		hotspots.forEach(function(hotspot){

			hotspot.clear();

			scene.remove(hotspot);
			targetList.forEach(function(item){
				if(hotspot.id == item.id && (item.data || item.hotspotData))
				targetList.splice(targetList.indexOf(item),1);
			})
		});

		hotspots = [];

		var obj, i,o;

		for ( i = scene.children.length - 1; i >= 0 ; i -- ) {
		    obj = scene.children[ i ];
		    if (obj !== dolly && obj !== panoVideoMesh && obj !== homeButton) {

		    	if(obj.hotspotData || obj.data){
					obj.clear();

					for ( o = obj.children.length - 1; o >= 0 ; o -- )
						obj.remove(obj.children[o]);
				}
		         scene.remove(obj);
		    }
		}
	};

	function createBox(){
		console.log("scene.createBox()");
		//the inital pano cube
	    box = new THREE.Mesh(new THREE.BoxGeometry(1024,1024,1024, 20, 20, 10,10), new THREE.MeshFaceMaterial(materials));
	    box.scale.x = -1;
		scene.box = box;
	    scene.add(box);
	}

	function createSphere(material){
		console.log("createSphere(material)");

		var geometry = new THREE.SphereGeometry( 500, 60, 40 );
		sphere = new THREE.Mesh( geometry, material );
		sphere.scale.x = -1;
		sphere.rotation.y = Math.PI / 2;
		scene.add(sphere);

		if(scene.box){
			scene.remove(box);
			box = null;
		}
	}

	function loadTexture(path,callback) {

		var texture = new THREE.Texture(texture_placeholder);
		texture.generateMipmaps = true;
		texture.magFilter = THREE.LinearFilter;
		texture.minFilter = THREE.LinearFilter;

		var material = new THREE.MeshBasicMaterial({
			map: texture,
			depthWrite:false
		});

		var image = new Image();
		image.crossOrigin = '';

		image.onload = function() {
			texture.image = this;
			texture.needsUpdate = true;

			callback(material);
		};
		image.src = path;

		return material;
	}

	function setDefaults(){
		console.log('setDefaults()');

		if(currentScene.cameraDefault != null){
			var defaults = JSON.parse(currentScene.cameraDefault);

			var pan = isMobile ? controls.getVRDisplay().poseSensor_.orientationOut_[1] * 2 : controls.getVRDisplay().theta_;


			if(defaults.theta){
				dolly.rotation.y = defaults.theta - pan;
			}

		}
		else if(currentScene.pan != null){
			dolly.rotation.x = 0;
			dolly.rotation.y = (180 - currentScene.pan)%360 * (Math.PI/180);
			dolly.rotation.z = 0;
		}

		if(window.innerWidth > window.innerHeight)
			camera.fov = 90;
		else
			camera.fov = 110;

		camera.updateProjectionMatrix();

		menu.rotation.set(menu.offsetX,0,0);

		sceneReady = false;
    	isUserInteracting = false;
	}


	function loadScene(){

		console.log('scene.loadScene()');
		selectedHotspot = null;

		//$( document ).trigger( "scene_loaded" );

	    	if(!firstRun)
	    		clearScene();
	    	else
		   		setUp();

	    createScene();
		hideLoader();

	}

	function createScene(){
		console.log("scene.createScene()");


		if(voiceover != null){
			voiceover.setPaused(true);
			voiceover.setMuted(true);
		}

		sceneLoaded = true;
		updateGuideScene(currentScene.id);

		if(fontsAvailable != true && webApp == true)
			waitForFontAwesome(function(){waitForWebfonts(['Lato'],function(){
				fontsAvailable = true;
				processHotspots(currentScene)});
			})
		else
			processHotspots(currentScene);


		if(currentScene.voiceover != null)playVoiceover();

		//setDefaults();


		setTimeout(function(){
			$(document).trigger('sceneLoaded');


				if(videoSettings != null)
					updateShader(videoSettings.brightness || 0,videoSettings.contrast || 0);
				else
					updateShader(0,0);


			play();

		},200);


	}



   function setUp(){
	   console.log('scene.setUp()');

	   if(guideData.gameTasks.length > 0 && gameTask == null){
		  // pause();
		   iosDebug("scene is starting game");
		   updateGameTask(gameTasks[0]);
	   }

	   if(currentScene.video == null || inIframe())
	   		removePreloader();

	  // addControlsHelp();
	   if(webApp != null || (!webApp && !isOffline()))
	   	preloadMediaFiles();


	   $(document.body).on('mousedown', onDocumentMouseDown);
	   $(document.body).on('mousewheel', onDocumentMouseWheel);
	   $(document.body).on('touchstart', onDocumentTouchStart);
	   $(document.body).on('touchstart', onDocumentTouchStart);
	   $(document.body).on('touchmove', onDocumentTouchMove);

	// queue.setPaused(false);

		firstRun = false;
   }


	function playVoiceover(){

		console.log('playVoiceover');

		if(voiceover != null)
			voiceover.paused = true;

		if(typeof currentScene.voiceover === 'string')
			try{
				currentScene.voiceover = JSON.parse(currentScene.voiceover)
			}catch(e){
				var filename = currentScene.voiceover;
				currentScene.voiceover = {};
				currentScene.voiceover.filename = filename;
			}

		var props = {};
		props.loop = 0;

		console.log(currentScene.voiceover.startTime);

		if(currentScene.voiceover.startTime != null){
			props.startTime = currentScene.voiceover.startTime * 1000;
		}

		if(currentScene.voiceover.endTime != null){
			if(currentScene.voiceover.startTime != null)
				props.duration = (currentScene.voiceover.endTime - currentScene.voiceover.startTime) * 1000;
			else
				props.duration = currentScene.voiceover.endTime * 1000;
		}

		console.log(props);

		if(currentScene.voiceover.length == undefined){
			voiceover = new createjs.Sound.play(currentScene.id,props);
			if(voiceover.playState !== "playSucceeded")
				voiceover.on("fileloaded",voiceover.play);

			voiceover.on("complete",function(){
				$(document).trigger("voiceoverEnded");
			});
		}
		else{
			var count = 0;



			voiceover = new createjs.Sound.play(currentScene.voiceover[0],props);
			voiceover.sceneId = currentScene.id;
			if(voiceover.playState !== "playSucceeded")
				voiceover.on("fileloaded",onLoaded);

			voiceover.on("complete",onComplete);

			function onLoaded(){
				voiceover.off("fileloaded",onLoaded);

				if(voiceover.sceneId == currentScene.id)
					voiceover.play();
			}


			function onComplete(){

				voiceover.off("complete",onComplete);

				if(voiceover.sceneId !== currentScene.id)
					return;
				count++;
				if(currentScene.voiceover[count] != null)
					voiceover = new createjs.Sound.play(currentScene.voiceover[count],{interrupt: createjs.Sound.INTERRUPT_ANY, loop:0});
				else
					$(document).trigger("voiceoverEnded");
			}


		}





		if(currentScene.voiceover.focus)
			voiceoverTimer = setInterval(function(){
				if(currentScene.voiceover && voiceover){
					if(currentScene.voiceover.focus && instructionArrow){
						instructionArrow.update(currentScene.voiceover.focus.points,voiceover.position/1000);
					}
					else
						clearInterval(voiceoverTimer)
				}
				else
					clearInterval(voiceoverTimer)
			},100);

	}


	function pause(){

		if(isPaused == true)
			return;

		$(document).trigger("pause");

		isPaused = true;

		if(video != null) {
		    video.pause();
			$(document).trigger("panoVideoPause");
		}
		if(voiceover != null) voiceover.paused = true;
	}


	function play(){

		if(gamePaused == true || isPaused == false)
			return;

		$(document).trigger("play");

		isPaused = false;

		if(video != null) {
			video.play();
			$(document).trigger("panoVideoPlay");
		}

		if(voiceover != null && currentScene.voiceover != null)voiceover.paused = false;
	}
