var $pop, 
	duration, 
	buffered, 
	currentProgress,	
	currentVideo,
    videoPlayerLoaded,
    videoPlaying,
    firstSceneVideo;

function loadVideoPlayer(){
		console.log('beek.loadVideoPlayer()');
		
		if(videoPlayerLoaded)
			return;
		
    	Sid.js("../../resources/js/popcorn.min.js", function(){
    		
    		videoPlayerLoaded = true;
    		
    	});
		
	}
	
	
	function loadVideo(video){
		console.log('loadVideo('+ video +')');
		if(videoPlayerLoaded)
			addVideoPlayer(video);
		else{
			loadVideoPlayer();
			setTimeout(loadVideo,100,video);
		}

	}
	
	
	function addVideoPlayer(video){
		console.log("addVideoPlayer("+video+")");
		$('body').append(
				 " <div id='video-container' class='video-container'><video id='video' crossorigin='anonymous' class='video-player'>"
				+ "<source src='" + cdnPrefix +"/"+ video +"'/>"
				+"  </video><div id='spinner'><i class='fa fa-circle-o-notch fa-spin'></i></div></div>"
		);
		
		videoPlaying = true;
		
		currentVideo = Popcorn('#video');
		currentVideo.preload( "auto" );
		
	
		$('#spinner').hide();
		$pop = Popcorn( "video" );
		$pop.on( "ended", function( e ) {closeVideo();},false );	
		$pop.on( "canplaythrough", function( e ) {videoReady();},false );	
		$pop.on( "suspend", function( e ) {videoSuspended();}, false );
		
	}
	
	function videoReady(){
		console.log('videoReady()');
		
		if(!currentVideo)
			return;
		
		$pop.off( "canplaythrough" );
		
		var progressFired = false;
		
		$pop.on('progress', function()
				{
				  
				  //calculate video time required to download before playing again
				  buffered = currentVideo.buffered().end(0);
				  
				  if(buffered > 5)
				  {
					  $pop.off('progress');
					  progressFired = true;
					  setTimeout(function(e){currentVideo.play();videoStart();},100);
					 
				  } 
				  
				}, false);
		
		//fail safe for blocked UI when no progress events fired
		setTimeout(function(){
			if(!progressFired)	{
				currentVideo.play();
				videoStart();
			}	
		},5000);
		

	}
	
	function videoStart(){
		
			if(!firstRun)
				clearScene();
			else
				setUp();
			
			$('#video-container').css('z-index',3);
			//$('#prevButton').hide();
			$('#openGuide').hide();
			$('#openSceneInfo').hide();
			$('#zoom').hide();
		
	}
	
	
	function closeVideo( ){
		console.log('closeVideo()');
		
		$pop.off( "ended" );	
		$pop.off( "suspend" );	
		$pop.off( "progress" );	

		//fade out video and then destroy
		$("#video").fadeTo(1000 , 0, function(e){
			currentVideo.src = '';
			currentVideo.load();
			currentVideo.destroy();
			currentVideo = null;
			$('#video-container').empty();
			$('#video-container').remove();
			$('#spinner').remove();
			
			openSceneInfo();

		});

		videoPlaying = false;
		createScene();

		//show hidden UI
		$('#prevButton').show();
		$('#openGuide').show();
		$('#openSceneInfo').show();
		$('#zoom').show();
	}
	
	function videoSuspended(){
		console.log('videoSuspended()');
		
		//if no metadata return
		if(currentVideo.buffered().length > 0)
			currentProgress = currentVideo.buffered().end(0);
		else
			return;

		//if it's basically loaded enough just play
		if(Math.abs(buffered - currentProgress) <= 1)
			return;
		
		//pause the video while wait for load
		currentVideo.pause();
		$('#spinner').show();
		
		var progressFired = false;
		
		//add progress that fires on download etc, this seems to stop sometimes
		$pop.on('progress', function()
				{
				  progressFired = true;
				  
				  //calculate video time required to download before playing again
				  duration = currentVideo.duration();
				  buffered = currentVideo.buffered().end(0);
				  var current =  currentProgress + 5;
				  var target = current < duration ? current : duration;
				  
				  if(buffered > target)
				  {
					  $('#spinner').hide();
					  $pop.off('progress');
					  setTimeout(function(e){currentVideo.play();},100);
					 
					  // if it's got enough data for the full length no more interuptions
				  } else if(buffered == duration)
				  {
					  $('#spinner').hide();
					  $pop.off('progress');
					  $pop.off('suspend');
					  setTimeout(function(e){currentVideo.play();},100);
				  }
				  
				}, false);
		
		//fail safe for blocked UI when no progress events fired
		setTimeout(function(){
			if(!progressFired)	{
				currentVideo.play();
			}	
		},5000);
	}
	