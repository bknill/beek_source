var webApp = true;

	function start(){
		console.log('debug start()');
/*	    //no html5 or admin load Flash
	  if(subdomain == 'gms' || !canvas2DSupported || isPreIE10())
		      preloadFlash();
		   else*/
        if(guideThumb != "guide_default_thumb_1.png")
            $('#guideCoverImg').attr('src',"//d3ixl5oi39qj40.cloudfront.net/" + guideThumb);

		loadBeekJs();

	}



	function showHideHelp(){
	        $('#guidehelpcontainer').hide();
	        $('#descriptionHolder').hide();
	        
    };
    
  

    function isIOSBrowser()
    {
        return (/iphone|ipad|ipod/i.test(navigator.userAgent.toLowerCase()));
    }

    function isMobileBrowser()
    {
        return (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));
    }

    function startMyApp()
    {
        var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false );
        if(iOS)
            launchIOSApp();

        var ua = navigator.userAgent.toLowerCase();
        var isAndroid = ua.indexOf("android") &gt; -1;
        if(isAndroid)
            launchAndroidApp();
     }

    function launchIOSApp()
    {
      document.location = 'beek://guide/#{GuideViewController.selectedGuide.id}';
      setTimeout( function()
      {
          document.location = 'itms://itunes.apple.com/us/app/beekAppId';
      }, 300);
    }

    function launchAndroidApp()
    {
      document.location = 'beek://guide/#{GuideViewController.selectedGuide.id}';
      setTimeout( function()
      {
          document.location = 'http://m.#{GuideViewController.domain}/android';
      }, 300);
    }


    function isIE () {
    	return navigator.userAgent.toLowerCase().indexOf('msie') !== -1 || navigator.appVersion.toLowerCase().indexOf('trident/') > 0; 
    	  
    }

    function isIE11() {
        return !(window.ActiveXObject) && "ActiveXObject" in window;
    }

    var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);

    function getIEVersion() {
        var match = navigator.userAgent.match(/(?:MSIE |Trident\/.*; rv:)(\d+)/);
        return match ? parseInt(match[1]) : undefined;
    }

    function isPreIE10(){
        return isIE() && getIEVersion() <= 10;
    }

    function isPreIE11(){
    	return isIE() && getIEVersion() <= 11;
    }

    
    function loadBeekJs()
    {
    	console.log('loadBeekJs()');
        $("#preview").hide();
        $("#guidehelpcontainer").hide();
        $("#swfcontainer").hide();
        $("#player-div").hide();

        loadBeekJsFiles();     
    }

 

    function trackSceneView(sceneId)
    {
        _gaq.push(['_trackPageview', guidePath + '/s' + sceneId]);
    }

    function trackEvent(category, action, label)
    {
        _gaq.push(['_trackEvent', category, action, label]);
    }

    


        $(window).resize(function() {
            showHideHelp();
        });
        

        function player_DoFSCommand(command, args) {
        if (command == "openWindow") { 
            var windowArgs = args.split("|"); 
            var domain = windowArgs[0]; 
            var width = windowArgs[1]; 
            var height = windowArgs[2]; 
            
            domain.replace('"', '');

            $('.iframeshim').css("left",(($( window ).width() - width)/2) + 10 +"px");
            $('.iframeshim').css("top",(($( window ).height() - height)/2) + 30 +"px");
            $('.iframeshim').css("width",width - 20 +"px");
            $('.iframeshim').css("height",height - 40 +"px");
            $('.iframeshim').attr("src",domain);

            $('.iframe-container').show();
       };
        
        if (command == "closeWindow") {$('.iframe-container').hide();}
    }



    	  
    function loadBeekJsFiles(){

        console.log('loadBeekJsFiles()',jsPath);
    	
    	Sid.js([jsPath +"three.min.js",
                jsPath +"html2canvas.js",
                jsPath +"preloadjs.min.js",
                jsPath +"tween.js",
                jsPath +"soundjs.min.js",
                jsPath +"beekjs/Detector.js",
				jsPath + "beekjs/beek.js",
                jsPath +"beekjs/scene.js",
                jsPath +"beekjs/guide.js",
                jsPath +"beekjs/guide3D.js",
                jsPath +"beekjs/game.js",
                jsPath + "beekjs/video.js",
                jsPath +"beekjs/hotspots.js",
                jsPath +"beekjs/connect.js",
                jsPath +"beekjs/panovideo.js",
                jsPath +"beekjs/FontAwesomeIcons.js"],
				
				function() {

                    console.log('loadBeekJsFiles(),2nd Stage');

                    Sid.js([
                        jsPath +"VRControls.js",
                        jsPath +"VREffect.js",
                        jsPath +"webvr-polyfill.js",
                        jsPath +"webvr-manager.js",
                        jsPath +"webvr-manager.js",
                 //       jsPath +"flashaudioplugin.min.js",
                        jsPath +"beekjs/shaders/EffectComposer.js",
                        jsPath +"beekjs/shaders/RenderPass.js",
                        jsPath +"beekjs/shaders/ShaderPass.js",
                       jsPath +"beekjs/shaders/BrightnessContrastShader.js",
                       jsPath +"beekjs/shaders/ColorCorrectionShader.js",
                        jsPath +"beekjs/shaders/FocusShader.js",
                        jsPath +"beekjs/ConvexGeometry.js"
                     //   jsPath +"beekjs/MediaStreamRecorder.js"
                    ],init);

                    Sid.css(cssPath + '/font-awesome/css/font-awesome.min.css');



                }
        );

    };
    
    function removePreloader(){
        console.log('removePreloader');
        $( "#preloader" ).animate({
            left: "-100%"
        }, 1000, function() {
            console.log('removed');
            $('#preloader').remove();
        });

    }
 
