PrimeFaces.widget.AccordionPanel.prototype.addTabHtml = function(content) {
    console.log('start');
     var length = this.jq.children(".ui-accordion-header").length;
    this.unselect($(this.jqId + "_active").val());
    $(this.jqId + "_active").before(
	'<h3 class="ui-accordion-header ui-helper-reset ui-state-default ui-corner-all" role="tab" aria-expanded="false">'
		+'<span class="ui-icon ui-icon-triangle-1-e"></span>'
		+'<a href="#" tabindex="-1">NEW SCENE</a>'
	+'</h3>'
	+'<div id="scenesAccordionId:'+ length +':scene_tab" class="ui-accordion-content ui-helper-reset ui-widget-content ui-helper-hidden" role="tabpanel" aria-hidden="true">'
		+content
        + '</div>'
    );
    this.headers = this.jq.children(".ui-accordion-header");
    this.panels = this.jq.children(".ui-accordion-content");
    this.headers.unbind();
    this.bindEvents();
    this.select(length);
};
PrimeFaces.widget.AccordionPanel.prototype.addEmptyTabHtml = function(sceneId,sceneName) {
    var length = this.jq.children(".ui-accordion-header").length;
    this.unselect($(this.jqId + "_active").val());
    $(this.jqId + "_active").before(
        '<h3 id="'+ this.id + ':' + sceneId +':scene_tab_header" class="ui-accordion-header ui-helper-reset ui-state-default ui-corner-all ui-state-disabled" role="tab" aria-expanded="false">'
            +'<span class="ui-icon ui-icon-triangle-1-e"></span>'
            +'<a href="#" tabindex="-1">'+sceneName+'</a>'
        +'</h3>'
        +'<div id="'+ this.id + ':' + length +':scene_tab" class="ui-accordion-content ui-helper-reset ui-widget-content ui-helper-hidden" role="tabpanel" aria-hidden="true">'
        + '</div>'
    );
    this.headers = this.jq.children(".ui-accordion-header");
    this.panels = this.jq.children(".ui-accordion-content");
    this.headers.unbind();
    this.bindEvents();
};
PrimeFaces.widget.AccordionPanel.prototype.enableTabHtml = function(success, sceneId, sceneName, message, content) {
    var sceneTabHeader = $(this.jqId + "\\:"+ sceneId + "\\:scene_tab_header");
    if(sceneTabHeader.length !=0 ){
        sceneTabHeader.removeClass('ui-state-disabled');
        if(success === 'true'){
            sceneTabHeader.find('a').html(sceneName +' - '+ sceneId);
            sceneTabHeader.next().html(content);
        }else{
            sceneTabHeader.find('a').html(sceneName +' - '+ sceneId +' - '+ message);
            sceneTabHeader.next().html('');
        }
    }
}
PrimeFaces.widget.AccordionPanel.prototype.updateTabHtml = function(sceneId,sceneName,message) {
    var sceneTabHeader = $(this.jqId + "\\:"+ sceneId + "\\:scene_tab_header");
    if(sceneTabHeader.length !=0 ){
        if(sceneName !== undefined){
            sceneTabHeader.find('a').html(sceneName +' - '+ sceneId +' - '+ message);
        }else{
            sceneTabHeader.find('a').append(' - '+sceneId+' - '+message);
        }
    }
}
PrimeFaces.widget.AccordionPanel.prototype.addTab = function(index) {
    this.unselect($(this.jqId + "_active").val());
    $(this.jqId + "_active").before(
        '<h3 class="ui-accordion-header ui-helper-reset ui-state-default ui-state-active ui-corner-top tabstyle" role="tab" aria-expanded="false" style="text-align:left;">'+
            '<span class="ui-icon ui-icon-triangle-1-e"></span>'+
            '<a href="#" tabindex="-1">'+
            'SCENE'+
            '<span style="float:right; padding-right: 20px;">'+
            '<span class="ui-icon ui-icon-close uploadedimage"></span>'+
            'Uploaded'+
            '<img width="15" height="1" src="/javax.faces.resource/spacer/dot_clear.gif.jsf?ln=primefaces&amp;v=3.2-SNAPSHOT">'+
            '<span class="ui-icon ui-icon-close defaultimage"></span>'+
            'Default'+
            '</span>'+
            '</a>'+
            '</h3>'+
            '<div id="ap:'+ index +':scene_tab" class="ui-accordion-content ui-helper-reset ui-widget-content" role="tabpanel" aria-hidden="false">'+
            '<span id="ap:'+ index +':scene_container">'+
            '</span>'+
            '</div>'
    );
    this.headers = this.jq.children(".ui-accordion-header");
    this.panels = this.jq.children(".ui-accordion-content");
    this.headers.unbind();
    this.bindEvents();
    var length = this.jq.children(".ui-accordion-header").length;
    $(this.jqId + "_active").val(--length);
};
PrimeFaces.widget.AccordionPanel.prototype.removeTab = function(index) {
    if($(this.jqId + "\\:"+ index + "\\:scene_tab").length !=0 ){
        this.headers.splice($(this.jqId + "_active").val(),1);
        this.panels.splice($(this.jqId +"_active").val(),1);
        this.cfg.active = -1;
        this.onshowHandlers = [];
        $(this.jqId + "\\:" + index + "\\:scene_tab").prev().remove();
        $(this.jqId + "\\:" + index + "\\:scene_tab").remove();
        $(this.jqId + "_active").val(-1)
    }
};

