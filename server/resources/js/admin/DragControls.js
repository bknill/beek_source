/*
 * @author zz85 / https://github.com/zz85
 * @author mrdoob / http://mrdoob.com
 * Running this will allow you to drag three.js objects around the screen.
 */

THREE.DragControls = function ( _objects, _camera, _domElement ) {

    if ( _objects instanceof THREE.Camera ) {

        console.warn( 'THREE.DragControls: Constructor now expects ( objects, camera, domElement )' );
        var temp = _objects; _objects = _camera; _camera = temp;

    }

    var _plane = new THREE.Plane();
    var _raycaster = new THREE.Raycaster();

    var _mouse = new THREE.Vector2();
    var _offset = new THREE.Vector3();
    var _intersection = new THREE.Vector3();

    //rotate with right mouse button
    var _right = false;
    var _mouseDown = false;
    var _rotateStartPoint = new THREE.Vector3(0, 0, 1);
    var _rotateEndPoint = new THREE.Vector3(0, 0, 1);

    var _curQuaternion;
    var windowHalfX = player.width / 2;
    var windowHalfY = player.height / 2;
    var _rotationSpeed = 2;


    var _startPoint = {
        x: 0,
        y: 0
    };

    var _lastMoveTimestamp,
        _moveReleaseTimeDelta = 50;

    var _deltaX = 0,
        _deltaY = 0;

    var _selected = null, _hovered = null;

    //

    var scope = this;

    function activate() {

        _domElement.addEventListener( 'mousemove', onDocumentMouseMove, false );
        _domElement.addEventListener( 'mousedown', onDocumentMouseDown, false );
        _domElement.addEventListener( 'mouseup', onDocumentMouseUp, false );

    }

    function deactivate() {

        _domElement.removeEventListener( 'mousemove', onDocumentMouseMove, false );
        _domElement.removeEventListener( 'mousedown', onDocumentMouseDown, false );
        _domElement.removeEventListener( 'mouseup', onDocumentMouseUp, false );

    }

    function dispose() {

        deactivate();

    }

    function onDocumentMouseMove( event ) {

        event.preventDefault();

        var offset = $("#container").offset();
        var rect = _domElement.getBoundingClientRect();
        _mouse.x = ( ( event.clientX - rect.left - (offset.left/2) ) / ( rect.width - rect.left ) ) * 2 - 1;
        _mouse.y = - ( ( event.clientY - rect.top ) / ( rect.bottom - rect.top) ) * 2 + 1;

        _raycaster.setFromCamera( _mouse, _camera );

        if ( _selected && scope.enabled ) {

            if ( _raycaster.ray.intersectPlane( _plane, _intersection ) ) {

                _deltaX = event.x - _startPoint.x;
                _deltaY = event.y - _startPoint.y;


                if(_right){



                    if(Math.abs(_selected.position.z) > Math.abs(_selected.position.x)){
                        _selected.rotation.x += (_deltaY/50);
                        _selected.rotation.y += (_deltaX/50);
                    }
                    else{
                        _selected.rotation.z -= (_deltaY/50);
                        _selected.rotation.y -= (_deltaX/50);
                    }

                    if(!_selected.rotation.order)
                        _selected.rotation.order = "XYZ";

                    updateRotationSliders(_selected.rotation);

                    _startPoint.x = event.x;
                    _startPoint.y = event.y;

                    _lastMoveTimestamp = new Date();
                }
                else{
                    _selected.position.copy( _intersection.sub( _offset ) );

       /*             if(Math.abs(_mouse.x) > 0.2){
                        _selected.lookAt(camera.position);
                    }*/

                }

            }

            scope.dispatchEvent( { type: 'drag', object: _selected } );

            return;

        }

        _raycaster.setFromCamera( _mouse, _camera );

        var intersects = _raycaster.intersectObjects( _objects , true  );

        if ( intersects.length > 0 ) {

            var object = intersects[ 0 ].object;

            if(object.type === "Mesh")
                if(object.parent)
                    if(object.parent.type == "Object3D")
                        object = object.parent;

            _plane.setFromNormalAndCoplanarPoint( _camera.getWorldDirection( _plane.normal ), object.position );

            if ( _hovered !== object ) {

                scope.dispatchEvent( { type: 'hoveron', object: object } );

                _domElement.style.cursor = 'pointer';
                _hovered = object;

            }

        } else {

            if ( _hovered !== null ) {

                scope.dispatchEvent( { type: 'hoveroff', object: _hovered } );

                _domElement.style.cursor = 'auto';
                _hovered = null;

            }

        }

    }

    function onDocumentMouseDown( event ) {

        event.preventDefault();

        _right = event.which == 3 || event.button == 2;

        _raycaster.setFromCamera( _mouse, _camera );

        var intersects = _raycaster.intersectObjects( _objects , true );

        if ( intersects.length > 0 ) {

            _selected = intersects[ 0 ].object;

            console.log(_selected);
           if(_selected.type == "Mesh" && _selected.parent.type == "Object3D")
             _selected = _selected.parent;

            if ( _raycaster.ray.intersectPlane( _plane, _intersection ) ) {

                if(_right){
                    _mouseDown = true;

                    _startPoint = {
                        x: event.clientX,
                        y: event.clientY
                    };

                    _rotateStartPoint = _rotateEndPoint = projectOnTrackball(0, 0);
                }
                else
                    _offset.copy( _intersection ).sub( _selected.position );
            }

            _domElement.style.cursor = 'move';

            scope.dispatchEvent( { type: 'dragstart', object: _selected } );

        }
        else
            scope.dispatchEvent( { type: 'nodrag' } );


    }

    function onDocumentMouseUp( event ) {

       // console.log('onDocumentMouseUp()',_selected);

        event.preventDefault();

        if ( _selected ) {
            scope.dispatchEvent( { type: 'dragend', object: _selected } );
            _selected = null;

        }
        scope.dispatchEvent( { type: 'dragend', object: null } );



        _domElement.style.cursor = 'auto';

    }

    activate();

    // API

    this.enabled = true;

    this.activate = activate;
    this.deactivate = deactivate;
    this.dispose = dispose;

    // Backward compatibility

    this.   setObjects = function () {

        console.error( 'THREE.DragControls: setObjects() has been removed.' );

    };

    this.on = function ( type, listener ) {

        console.warn( 'THREE.DragControls: on() has been deprecated. Use addEventListener() instead.' );
        scope.addEventListener( type, listener );

    };

    this.off = function ( type, listener ) {

        console.warn( 'THREE.DragControls: off() has been deprecated. Use removeEventListener() instead.' );
        scope.removeEventListener( type, listener );

    };

    this.notify = function ( type ) {

        console.error( 'THREE.DragControls: notify() has been deprecated. Use dispatchEvent() instead.' );
        scope.dispatchEvent( { type: type } );

    };

    function projectOnTrackball(touchX, touchY)
    {
        var mouseOnBall = new THREE.Vector3();

        mouseOnBall.set(
            clamp(touchX / windowHalfX, -1, 1), clamp(-touchY / windowHalfY, -1, 1),
            0.0
        );

        var length = mouseOnBall.length();

        if (length > 1.0)
        {
            mouseOnBall.normalize();
        }
        else
        {
            mouseOnBall.z = Math.sqrt(1.0 - length * length);
        }

        return mouseOnBall;
    }

    function rotateMatrix(rotateStart, rotateEnd)
    {
        var axis = new THREE.Vector3(),
            quaternion = new THREE.Quaternion();

        var angle = Math.acos(rotateStart.dot(rotateEnd) / rotateStart.length() / rotateEnd.length());

        if (angle)
        {
            axis.crossVectors(rotateStart, rotateEnd).normalize();
            angle *= _rotationSpeed;
            quaternion.setFromAxisAngle(axis, angle);
        }
        return quaternion;
    }

    function clamp(value, min, max)
    {
        return Math.min(Math.max(value, min), max);
    }

    var handleRotation = function()
    {
        _rotateEndPoint = projectOnTrackball(_deltaX, _deltaY);

        var rotateQuaternion = rotateMatrix(_rotateStartPoint, _rotateEndPoint);
        _curQuaternion = _selected.quaternion;
        _curQuaternion.multiplyQuaternions(rotateQuaternion, _curQuaternion);
        _curQuaternion.normalize();
        _selected.setRotationFromQuaternion(_curQuaternion);

        _rotateEndPoint = _rotateStartPoint;
    };

};

THREE.DragControls.prototype = Object.create( THREE.EventDispatcher.prototype );
THREE.DragControls.prototype.constructor = THREE.DragControls;