/**
 * Created by Ben on 6/02/2017.
 */

var camera, scene, renderer,box, deviceControls, dragControls, container,fontsAvailable = false;
var materials = [];
var texture_placeholder,
    isUserInteracting = false,
    onMouseDownMouseX = 0,
    onMouseDownMouseY = 0,
    lon = 90,
    lastLon,
    onMouseDownLon = 0,
    lat = 0,
    lastLat,
    onMouseDownLat = 0,
    phi = 0,
    theta = 0,
    target,
    previewTileCounter,
    isMobile,
    fullscreen = false,
    _touchZoomDistanceEnd,
    _touchZoomDistanceStart = 0,
    _touchZoomDistanceEnd = 0,
    tweenRemote,
    width,
    height,
    shakeEvent,
    draggingHotspot;


var targetList = [];
var projector, mouse = { x: 0, y: 0 };

var iframeWidth, iframeHeight;


var lrs;


var jsonpPrefix = 'http://gms.beek.co';
var cdnPrefix = 'https:' == document.location.protocol ? '//d3ixl5oi39qj40.cloudfront.net' : '//cdn.beek.co';

var FACES = "rludfb";
var TILES = ['0_0', '0_1', '1_0', '1_1'];

var tiles;

var player = {width:0,height:0};

var showHowColors = {
    grey: '#a7a9ac',
    purple: '#662d91',
    lightGrey: "#e6e8eb"
};

function initGuideAdmin() {

    document.getElementById("container").addEventListener('mousedown', onDocumentMouseDown, false);


    player.width = $("#container").width()/2;
    player.height = 400;

    if(window.location.hostname == 'beekdev.co')
        jsonpPrefix = 'http://beekdev.co:9081';

    if(window.location.hostname == 'beeksuper')
        jsonpPrefix = 'http://beeksuper:9081';


    $.getJSON( + guideId + '/jsonp?callback=?', $.proxy(createGuide, this));

    $(document).on("panoVideoLoaded",updateGuideSceneVideoSlider);


}
function initTaskAdmin(xhr, status, args) {


    console.log("initTaskAdmin",xhr, status, args);

    player.width = $("#taskContainer").width();
    player.height = 500;//$(".ui-tabs-panels").height() * 0.5;

    if(window.location.hostname == 'beekdev.co')
        jsonpPrefix = 'http://beekdev.co:9081';

    if(window.location.hostname == 'beeksuper')
        jsonpPrefix = 'http://beeksuper:9081';

    createPlayer('taskContainer');

    console.log(args.sceneData);
    if(args.sceneData)
        startScene(JSON.parse(args.sceneData),true);
    animate();

    //	$(document).on("voiceover",createAudioSliders);
}


function dynamicSort(property) {
    var sortOrder = 1;
    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a,b) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    };
}

function waitForFontAwesome( callback ) {
    var retries = 5;

    var checkReady = function() {
        var canvas, context;
        retries -= 1;
        canvas = document.createElement('canvas');
        canvas.width = 20;
        canvas.height = 20;
        context = canvas.getContext('2d');
        context.fillStyle = 'rgba(0,0,0,1.0)';
        context.fillRect( 0, 0, 20, 20 );
        context.font = '16pt FontAwesome';
        context.textAlign = 'center';
        context.fillStyle = 'rgba(255,255,255,1.0)';
        context.fillText( '\uf0c8', 10, 18 );
        var data = context.getImageData( 2, 10, 1, 1 ).data;
        if ( data[0] !== 255 && data[1] !== 255 && data[2] !== 255 ) {
            //console.log( "FontAwesome is not yet available, retrying ..." );
            if ( retries > 0 ) {
                setTimeout( checkReady, 200 );
            }
        } else {
            //console.log( "FontAwesome is loaded" );
            if ( typeof callback === 'function' ) {
                callback();
            }
        }
    }

    checkReady();
};

function waitForWebfonts(fonts, callback) {
    var loadedFonts = 0;
    for(var i = 0, l = fonts.length; i < l; ++i) {
        (function(font) {
            var node = document.createElement('span');
            // Characters that vary significantly among different fonts
            node.innerHTML = 'giItT1WQy@!-/#';
            // Visible - so we can measure it - but not on the screen
            node.style.position      = 'absolute';
            node.style.left          = '-10000px';
            node.style.top           = '-10000px';
            // Large font size makes even subtle changes obvious
            node.style.fontSize      = '300px';
            // Reset any font properties
            node.style.fontFamily    = 'sans-serif';
            node.style.fontVariant   = 'normal';
            node.style.fontStyle     = 'normal';
            node.style.fontWeight    = 'normal';
            node.style.letterSpacing = '0';
            document.body.appendChild(node);

            // Remember width with no applied web font
            var width = node.offsetWidth;

            node.style.fontFamily = font + ', sans-serif';

            var interval;
            function checkFont() {
                // Compare current width with original width
                if(node && node.offsetWidth != width) {
                    ++loadedFonts;
                    node.parentNode.removeChild(node);
                    node = null;
                }

                // If all fonts have been loaded
                if(loadedFonts >= fonts.length) {
                    if(interval) {
                        clearInterval(interval);
                    }
                    if(loadedFonts == fonts.length) {
                        callback();
                        return true;
                    }
                }
            };

            if(!checkFont()) {
                interval = setInterval(checkFont, 50);
            }
        })(fonts[i]);
    }
};

function taskSceneChange_handler(value){

    getScene(value);
    $( document ).on( "scene_loaded",updateTaskDialogue );
    updateTaskDialogue();
}

function updateTaskDialogue(value){

    $( document ).off( "scene_loaded",updateTaskDialogue );

    console.log('updateTaskDialogue',value);

    var code = value || $("#task\\:Code").val();
    var timer;


  //  if(code == "watchpanovideo"){

    setTimeout(function(){

        if(video != null){

            $("#timeSlider").slider({
                min : currentScene.video.startTime || 0.0,
                max : currentScene.video.endTime || currentScene.video.duration,
                step : 0.001,
                value : $('#task\\:taskTime').val() || video.duration,
                slide:  function( event, ui ) {video.currentTime = ui.value},
                change: function( event, ui ) {
                  //  PF('sceneSelector').selectValue(currentScene.id);

                    $('#task\\:taskTime').val(ui.value.toFixed(2));

                }
            });


        }

/*        if(currentScene)
        PF('sceneSelector').selectValue(currentScene.id);*/


    },1000);

}

function playVoiceover(filename){
    console.log('playAudio',filename);
   var audio = new Audio('http://cdn.beek.co/' + filename);
    audio.play();
    return false;
}

function clearGameDialogue(){

/*    $('#task\\:taskTime').val(null);
    $('#task\\:taskScene').val(null);*/
}

function updateGuideSceneVideoSlider(event,videoSettings){

    console.log("updateGuideSceneVideoSlider()",videoSettings);

    video.slider = $("#videoTimeSlider").data("ionRangeSlider");

    if(videoSettings.duration == null)
        videoSettings.duration = video.duration;

    if(!video.slider)
        $("#videoTimeSlider").ionRangeSlider({
            type: "double",
            grid: true,
            min: 0,
            step: 0.01,
            max: videoSettings.duration,
            from: videoSettings.startTime,
            to: videoSettings.endTime || videoSettings.duration,
            onChange: sliderChangeHandler,
            onFinish: sliderFinishHandler
        });
    else
        video.slider.update({
            max: videoSettings.duration,
            from: videoSettings.startTime,
            to: videoSettings.endTime || videoSettings.duration
        });

    $('#videoMarker').remove();
    $("#videoSlider").append('<span class="timeMarker" id="videoMarker">|</span>');

    video.updateTimer = setInterval(function(){
        if(video)
            $('#videoMarker').css('left',(video.currentTime / video.duration) * $("#videoSlider .irs").width());
    },100);


    $('#videoSlider .irs').click(function (e){
        var elm = $(this);
        var xPos = e.pageX - elm.offset().left;
        var perc = xPos/$('#videoSlider .irs').width();

        video.currentTime = video.duration * perc;
    });
}

function sliderChangeHandler(data){
    if(data.from != videoSettings.startTime){
        videoSettings.startTime = video.currentTime = data.from;
        video.play();
        if(voiceover){
            voiceover.stop();
            voiceover.play();

        }


    }
    else if(data.to != currentScene.video.endTime){
        videoSettings.endTime = video.currentTime  = data.to;
       // video.pause();
    }

}

function sliderFinishHandler(data){

    if(currentScene.video.endTime > video.currentTime)
        video.play();

   setGuideSceneVideoSettings();
}

function setGuideSceneVideoSettings(){

    $("#tabView\\:descriptions\\:guideSceneVideoSettings").val(JSON.stringify(videoSettings));
    saveGuide();
}

function onGuideTabChange(index){
    console.log(index);

    switch (index){
        case 6:
            getInitialReportsData();
            break;
    }
}

function openFileURL(file){
    console.log("openFileURL",file);
    window.open(cdnPrefix + "/" + file,'_blank');
}