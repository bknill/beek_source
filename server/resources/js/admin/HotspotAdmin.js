/**
 * Created by Ben on 17/01/2017.
 */
var selectedHotspot = null,recordingBehaviours;
var editModes = {
    button: 0,
    text: 1,
    media: 2,
    shape:3,
    behaviour: 4
}
var editMode = editModes.button;

var gameInProgress = false;


function addHotspot(xhr, status, args){

    if(!args.hotspot)
        return;

    var hotspot = new THREE.Object3D();
    hotspot.data = args.hotspot;

    var temp = new THREE.Mesh(new THREE.PlaneGeometry(200, 200), new THREE.MeshBasicMaterial({  color: 0x00ff00 }));
    temp.name = "temp";
    temp.material.transparent = true;
    hotspot.add(temp);

    var dist = 200;
    var vec = new THREE.Vector3( 0, 0, -dist );
    vec.applyQuaternion( camera.quaternion );

    hotspot.position.copy( vec );
    hotspot.lookAt(camera.position);

    hotspot.name = hotspot.data.id;

    hotspot.data.config = {};
    hotspot.data.config.scale = 1;
    hotspot.data.config.opacity = 1;
    hotspot.data.config.position = hotspot.position.clone();
    hotspot.data.config.rotation = hotspot.position.clone();
    scene.add(hotspot);
    hotspots.push(hotspot);

    selectedHotspot = hotspot;

    var _opacity = 1;

    hotspot.setOpacity = function(opacity){

        _opacity = opacity;

        if(hotspot.material)
            hotspot.material.opacity =  opacity;

        hotspot.children.forEach(function(child){
            if(child.material && child.name !== 'box' && child.name !== "temp"){
                child.material.transparent = true;
                child.material.opacity = opacity;
            }
        })
    };

    hotspot.getOpacity = function(){
        return _opacity;
    }

    $('#modeTab\\:sceneSettings\\:hotspotSettings\\:selectedHotspotId').val(hotspot.data.id);
    updateSelectedHotspot();
 //  onHotspotChange(hotspot.data.id);
}

function removeHotspot(){
    console.log('removeHotspot()');
    scene.remove(selectedHotspot);
    selectedHotspot = null;
}


function onDragHotspotStart(event){
    draggingHotspot = true;

    if((!selectedHotspot || selectedHotspot != event.object) && event.object.data){
        $('#modeTab\\:sceneSettings\\:hotspotSettings\\:selectedHotspotId').val(event.object.data.id);
        selectedHotspot = event.object;
        updateSelectedHotspot();
    }

    if(editMode == editModes.behaviour)
        recordingBehaviours = setInterval(updateBehaviour,500);

}

function onDragHotspotEnd(event){

    draggingHotspot = false;

    if(!selectedHotspot){
        console.log("should be selecting hotspot");
        $('#modeTab\\:sceneSettings\\:hotspotSettings\\:selectedHotspotId').val(null);
        updateSelectedHotspot();
        return;
    }

    if(recordingBehaviours)
        clearInterval(recordingBehaviours);

    if(editMode == editModes.behaviour && sceneEditMode == sceneEditModes.hotspots)
        updateBehaviour();
    else if(selectedHotspot.data && selectedHotspot == event.object){
        selectedHotspot.data.config.position = event.object.position;
        selectedHotspot.data.config.rotation = event.object.rotation;
    }
    saveHotspot();

}

function onDragEmpty(event){
    if(selectedHotspot != null){
        $('#modeTab\\:sceneSettings\\:hotspotSettings\\:selectedHotspotId').val(null);
        selectedHotspot = null;
        updateSelectedHotspot();
        updateBoxHelper();
    }
}


function saveHotspot(){

   console.log('saveHotspot()',selectedHotspot.data);

    if($('#modeTab\\:sceneSettings\\:hotspotSettings\\:hotspotId').val() != selectedHotspot.data.id)
     return;

    if(!selectedHotspot.data)
    return;

   // var changeTitle = function(){ return selectedHotspot.data.title.indexOf('New hotspot') > -1};

/*    if(changeTitle){
        findBetterName(changeTitle);
        $('#modeTab\\:sceneSettings\\:hotspotSettings\\:hotspotTitle').val(selectedHotspot.data.title);
    }*/

    if(typeof selectedHotspot.data.config === 'object')
        $('#modeTab\\:sceneSettings\\:hotspotSettings\\:config').val(JSON.stringify(selectedHotspot.data.config));
   if(typeof selectedHotspot.data.description === 'object' && selectedHotspot.data.description != null)
        $('#modeTab\\:sceneSettings\\:hotspotSettings\\:description').val(Object.keys(selectedHotspot.data.description).length > 1 ?  JSON.stringify(selectedHotspot.data.description) : null);
    if(typeof selectedHotspot.data.media === 'object' && selectedHotspot.data.media != null)
        $('#modeTab\\:sceneSettings\\:hotspotSettings\\:media').val(Object.keys(selectedHotspot.data.media).length > 0 ? JSON.stringify(selectedHotspot.data.media) : null);
    if(typeof selectedHotspot.data.button === 'object' && selectedHotspot.data.button != null)
        $('#modeTab\\:sceneSettings\\:hotspotSettings\\:button').val(Object.keys(selectedHotspot.data.button).length > 0 ?  JSON.stringify( selectedHotspot.data.button) : null);
    if(typeof selectedHotspot.data.behaviour === 'object' && selectedHotspot.data.behaviour != null)
        $('#modeTab\\:sceneSettings\\:hotspotSettings\\:behaviour').val(Object.keys(selectedHotspot.data.behaviour).length > 0 ? JSON.stringify(selectedHotspot.data.behaviour) : null);


    saveSelectedHotspot();

}

function findBetterName(changeTitle){
    if (selectedHotspot.data.button) {
        if (selectedHotspot.data.button.label)
            selectedHotspot.data.title = selectedHotspot.data.button.label.substr(0, 20);
    }
    else if (selectedHotspot.data.description && changeTitle) {
        if(selectedHotspot.data.description.text)
        selectedHotspot.data.title = selectedHotspot.data.description.text.substr(0, 20);
    }
/*    else if (selectedHotspot.data.media && changeTitle) {
        if (selectedHotspot.data.media.files)
            selectedHotspot.data.title = selectedHotspot.data.media.files[0].type;
    }*/

}

function updateHotspot(){
    console.log('updateHotspot()');

    if(PF("loadSceneTable")){
        var id = PF("loadSceneTable").selection[0];

        if(id)
            selectedHotspot.data.loadSceneId = id;
        else
            selectedHotspot.data.loadSceneId = null;
    }

    for ( i = selectedHotspot.children.length - 1; i >= 0 ; i -- ){
        if(selectedHotspot.children[i] !== selectedHotspot.box)
        selectedHotspot.remove(selectedHotspot.children[i]);
    }

    renderInProgress = false;

    setTimeout(createHotspot,100,selectedHotspot.data,selectedHotspot);

}

function handleHotspotChange(){
//    console.log('handleHotspotChange()',selectedHotspot);
    selectedHotspot = null;
}

function onHotspotChange(newId){

    var id = newId || $('#modeTab\\:sceneSettings\\:hotspotSettings\\:hotspotId').val();

    if (!selectedHotspot || selectedHotspot.data.id != id)
        hotspots.forEach(function(hotspot){
            if(hotspot.data.id === id)
                selectedHotspot = hotspot;
        });

    if(!selectedHotspot)
    return;

    if(PF("hotspotTab"))PF("hotspotTab").select(0);

    $("#scaleSlider").slider({
        min : 0.001,
        max : 1,
        step : 0.001,
        value : selectedHotspot.data.config.scale || 1,
        slide: function( event, ui ) {	selectedHotspot.data.config.scale = ui.value; selectedHotspot.scale.set(ui.value,ui.value,ui.value)},
        change: onScaleChange
    });

    $("#opacitySlider").slider({
        min : 0.001,
        max : 1,
        step : 0.001,
        value : selectedHotspot.data.config.opacity || 1,
        slide: function( event, ui ) {
                selectedHotspot.setOpacity(ui.value);
        },
        change: onOpacityChange
    });



    $("#rotationXSlider").slider({
        min : -Math.PI,
        max : Math.PI,
        step : 0.001,
        value : selectedHotspot.data.config.rotation._x || 0,
         slide: function( event, ui ) {
             selectedHotspot.rotation.x = ui.value;
            },
        stop: function( event, ui ) {updateRotationFromSlider("_x",ui.value)}
    });

    $("#rotationYSlider").slider({
        min : -Math.PI,
        max : Math.PI,
        step : 0.001,
        value : selectedHotspot.data.config.rotation._y || 0,
        slide: function( event, ui ) {
            selectedHotspot.rotation.y = ui.value;
        },
        stop: function( event, ui ) {updateRotationFromSlider("_y",ui.value)}
    });

    $("#rotationZSlider").slider({
        min : -Math.PI,
        max : Math.PI,
        step : 0.001,
        value : selectedHotspot.data.config.rotation._z || 0,
        slide: function( event, ui ) {
            selectedHotspot.rotation.z = ui.value;
        },
        stop: function( event, ui ) {updateRotationFromSlider("_z",ui.value)}
    });

    function updateRotationFromSlider(value,axis){

        if(axis == "_x")
            selectedHotspot.data.config.rotation._x = value;
        else if(axis == "_y")
            selectedHotspot.data.config.rotation._y = value;
        else if(axis == "_z")
            selectedHotspot.data.config.rotation._z = value;

        if(!selectedHotspot.data.config.rotation._x)
            selectedHotspot.data.config.rotation._x = 0;
        if(!selectedHotspot.data.config.rotation._y)
            selectedHotspot.data.config.rotation._y = 0;
        if(!selectedHotspot.data.config.rotation._z)
            selectedHotspot.data.config.rotation._z = 0;

        selectedHotspot.data.config.rotation.order = "XYZ";

        saveHotspot();
    }

    if((selectedHotspot.data.config.interactive == null || selectedHotspot.data.config.interactive == true) && PF("interactive") != null)
        PF("interactive").check();
    if((selectedHotspot.data.config.visible == null || selectedHotspot.data.config.visible == true) && PF("visible") != null)
        PF("visible").check();

    updateButton();
    updateBoxHelper();
    saveHotspot();
}

function updateRotationSliders(data){
    $("#rotationXSlider").slider("value",data._x);
    $("#rotationYSlider").slider("value",data._y);
    $("#rotationZSlider").slider("value",data._z);
}



function clearBoxHelpers(){
    hotspots.forEach(function(hotspot){
        if(hotspot.box){
            hotspot.remove(hotspot.box);
            // THREE.SceneUtils.detach(hotspot.box, scene, hotspot);
            hotspot.box = null;
        }

    });
}

function updateBoxHelper(){

    clearBoxHelpers();

    if(!selectedHotspot)
        return;

    var bbHelper = new THREE.BoundingBoxHelper( selectedHotspot, 0xffffff );
        bbHelper.visible = false;
    scene.add( bbHelper );
    bbHelper.update();

    var boxHelper = new THREE.BoxHelper( bbHelper );
        boxHelper.material.color.set( 0x00ff00 );
        boxHelper.name = "box";
    scene.add( boxHelper );
    scene.remove(bbHelper);

    selectedHotspot.updateMatrixWorld();
    THREE.SceneUtils.attach(boxHelper, scene, selectedHotspot);
    selectedHotspot.box = boxHelper;

}

function onScaleChange(event, ui){

    if(editMode == editModes.behaviour){
        selectedHotspot.data.scale = ui.value;
        updateBehaviour();
    }

    else
        selectedHotspot.data.config.scale = ui.value;

    saveHotspot();
}


function onOpacityChange(event, ui){


    if(editMode == editModes.behaviour){
        selectedHotspot.data.opacity = ui.value;
        updateBehaviour();

    }

    else
        selectedHotspot.data.config.opacity = ui.value;

    saveHotspot();
}

function updateButton(){

    $('#icons').empty();


    var $label = $('#modeTab\\:sceneSettings\\:hotspotSettings\\:hotspotOptions\\:buttonLabel');
    $label.val('');

    for (var icon in fontAwesome) {
        if (fontAwesome.hasOwnProperty(icon)) {
            if(fontAwesome[icon][1])
                $('#icons').append('<span id="'+icon+'" class="icons fa '+icon+'">');
        }
    }
    $('#icons').on('click',handleIconClick);

    if(selectedHotspot.data.button) {
        if (typeof selectedHotspot.data.button === 'string' && selectedHotspot.data.button.length > 0)
            selectedHotspot.data.button = JSON.parse(selectedHotspot.data.button);

        if (selectedHotspot.data.button.icon)
            $('#' + selectedHotspot.data.button.icon).addClass('selectedIcon');

        if (selectedHotspot.data.button.label)
            $label.val(selectedHotspot.data.button.label);

    }

    $label.change(function(){
        if(!selectedHotspot.data.button)
            selectedHotspot.data.button = {};


        if($label.val())
            selectedHotspot.data.button.label = $label.val();
        else
            delete selectedHotspot.data.button.label;

        saveHotspot();
       updateHotspot();
    });

    $label.keyup(function(){

        if(!selectedHotspot)
            return;

        if(!selectedHotspot.data.button)
            selectedHotspot.data.button = {};

        selectedHotspot.data.button.label = $label.val();
        updateHotspot();
    })

}

function handleIconClick(event){

    if(event.target.id == "icons" || !selectedHotspot)
        return;

    if(!selectedHotspot.data.button)
            selectedHotspot.data.button = {};

    if($('#'+event.target.id).hasClass('selectedIcon')){
        clearSelectedIcons();
        $('#modeTab\\:sceneSettings\\:hotspotSettings\\:icon').empty();
        delete selectedHotspot.data.button.icon;
    }
    else{
        clearSelectedIcons();
        $('#'+event.target.id).addClass('selectedIcon');

        selectedHotspot.data.button.icon = event.target.id;
    }

    saveHotspot();
    updateHotspot();
}

function clearSelectedIcons(){
    $('#icons').children('.fa').each(function() {
        $(this).removeClass('selectedIcon');
    });

}

function updateHotspotDescription(){

    var $descriptionHolder = $('#modeTab\\:sceneSettings\\:hotspotSettings\\:hotspotOptions\\:hotspotDescriptionTextArea');
    var $font = $('#modeTab\\:sceneSettings\\:hotspotSettings\\:hotspotOptions\\:font');
    var $textWidthInput = $('#modeTab\\:sceneSettings\\:hotspotSettings\\:hotspotOptions\\:textWidthInput');

    if(!selectedHotspot || editMode != editModes.text)
        return;

    if(selectedHotspot.data.description){
        if(typeof selectedHotspot.data.description === 'string')
            selectedHotspot.data.description = JSON.parse(selectedHotspot.data.description);

        if(selectedHotspot.data.description.text)
            $descriptionHolder.val(selectedHotspot.data.description.text);
        else
            $descriptionHolder.empty();

        if( selectedHotspot.data.description.font)
            $font.val(selectedHotspot.data.description.font);

        if( selectedHotspot.data.description.width)
            $textWidthInput.val(selectedHotspot.data.description.width);


        if(selectedHotspot.data.description.setting && PF('descriptionOptions'))
            PF('descriptionOptions').jq.find('input:radio[value="'+selectedHotspot.data.description.setting.option+'"]').parent().next().trigger('click.selectOneRadio');
    }
    else{
        $descriptionHolder.empty();
        selectedHotspot.data.description = {};
        PF('descriptionOptions').jq.find('input:radio[value="3"]').parent().next().trigger('click.selectOneRadio');
    }


    $descriptionHolder.keyup(function(){
        if($descriptionHolder.val().length > 0)
            selectedHotspot.data.description.text = $descriptionHolder.val();
        else
            selectedHotspot.data.description.text = "";

        if($font.val().length > 0)
            selectedHotspot.data.description.font = $font.val();

        if($textWidthInput.val().length > 0)
            selectedHotspot.data.description.width = $textWidthInput.val();
 //     updateHotspot();
    });

    saveHotspot();
    updateHotspot();


}


function handleInteractiveCheck(val){

    selectedHotspot.data.config.interactive = val;
    saveHotspot();
}

function handleVisibleCheck(val){

    selectedHotspot.data.config.visible = val;
    saveHotspot();
}

function descriptionAlign(val){
        selectedHotspot.data.description.align = val;
        saveHotspot();
        updateHotspot();
}

function hotspotDescriptionChange(){

    var $descriptionHolder = $('#modeTab\\:sceneSettings\\:hotspotSettings\\:hotspotOptions\\:hotspotDescriptionTextArea');
    var $font = $('#modeTab\\:sceneSettings\\:hotspotSettings\\:hotspotOptions\\:font');
    var $textWidthInput = $('#modeTab\\:sceneSettings\\:hotspotSettings\\:hotspotOptions\\:textWidthInput');

    if($descriptionHolder.val().length > 0)
        selectedHotspot.data.description.text = $descriptionHolder.val();
    else
        selectedHotspot.data.description.text = "";

    if($font.val().length > 0)
        selectedHotspot.data.description.font = $font.val();

    if($textWidthInput.val().length > 0)
        selectedHotspot.data.description.width = $textWidthInput.val();

    if(selectedHotspot.data.description.text && !selectedHotspot.data.description.setting)
        handleDescriptionSettingsChange(3);
    else
    {
        if(!selectedHotspot.data.description.text || selectedHotspot.data.description.text == 0){
            delete selectedHotspot.data.description.text;
            delete selectedHotspot.data.description.image;
        }

        updateHotspot();

        if(selectedHotspot.data.description.text)
        $(document).on("hotspotRendered",function(event,canvas){


            var data = canvas.toDataURL('image/png');
            var sh = selectedHotspot;

            $.ajax({
                url:'/scene/'+currentScene.id+'/hotspotImage',
                data:{imageValue:data},
                type:'POST',
                success:function(data, status) {
                    if(data.payload){
                        if(selectedHotspot != null && (selectedHotspot == sh)){
                            selectedHotspot.data.description.image = data.payload;
                            console.log(data.payload);
                            saveHotspot();
                        }
                    }
                }
            });
        });
    }

}


function handleDescriptionSettingsChange(value){

    if(selectedHotspot.data.description.setting)
        if(selectedHotspot.data.description.setting.option == value)
             return;

    if(value == 1)
        selectedHotspot.data.description.setting = {'option':1,'fontcolor':'#ffffff','backgroundcolor':null};
    if(value == 2)
        selectedHotspot.data.description.setting = {'option':2,'fontcolor':'#000000','backgroundcolor':null};
    if(value == 3)
        selectedHotspot.data.description.setting = {'option':3,'fontcolor': showHowColors.purple,'backgroundcolor': showHowColors.lightGrey};

    saveHotspot();

    if(selectedHotspot.data.description.text)
        updateHotspot();
}

function updateMedia(){

}

function updateBehaviourSlider(){

    if(!selectedHotspot || editMode != editModes.behaviour)
        return;


   // console.log('updateBehaviourSlider()',selectedHotspot);


    if(video){
        $("#behaviourVideoTimeSlider").slider({
            min : currentScene.video.startTime || 0.0,
            max : currentScene.video.endTime | currentScene.video.duration,
            step : 0.001,
            value : 0,
            slide: onVideoTimeSlide,
            change: function( event, ui ) { $('#behaviourTimeSliderTime').text(ui.value.toFixed(2))}
        });
    }

    if(voiceover){
        $("#behaviourVoiceTimeSlider").slider({
            min : 0.0,
            max : voiceover.getDuration(),
            step : 0.001,
            value : 0,
            slide :function( event, ui ) { voiceover.pause() },
            stop: function( event, ui ) {
                voiceover.setCurrentTime(ui.value);
                $('#behaviourTimeSliderTime').text(ui.value.toFixed(2));
            },
            change: function( event, ui ) {$('#behaviourVoiceSliderTime').text(ui.value)}
        });
    }

    if(selectedHotspot.data.behaviour){
        if(typeof selectedHotspot.data.behaviour === 'string' && selectedHotspot.data.behaviour.length > 0)
            selectedHotspot.data.behaviour = JSON.parse(selectedHotspot.data.behaviour);
    }
    else{
        selectedHotspot.data.behaviour = {};
        selectedHotspot.data.behaviour.type = currentScene.video ? "video" : currentScene.voiceover ? "voiceover" : null;
        selectedHotspot.data.behaviour.values = [];
    }

    if(PF("behaviourType") && selectedHotspot.data.behaviour.type != null)
        PF('behaviourType').selectValue(selectedHotspot.data.behaviour.type);

    updateBehaviourDialogue();

    onBehaviourTypeSelectionChange(selectedHotspot.data.behaviour.type);

    var update = setInterval(updateSliders,100);


}

function onVideoTimeSlide(event,ui){
    video.pause();
    video.currentTime = ui.value;
    $('#behaviourTimeSliderTime').text(ui.value.toFixed(2));

    if(!selectedHotspot)
    return;

    if(selectedHotspot.behaviours){

    var behaviour;

    selectedHotspot.behaviours.values.some(function(b){
        if(b.time > ui.value){
            behaviour = b;
            return true;
        }
    })


     if(behaviour != null){

         var time = behaviour.time - ui.value;
         var percComplete = time/behaviour.offsetTime;

         var difference = {
             pX :(behaviour.target.pX - behaviour.origin.pX) * percComplete,
             pY : (behaviour.target.pY - behaviour.origin.pY) * percComplete,
             pZ : (behaviour.target.pZ - behaviour.origin.pZ) * percComplete,
             scale: (behaviour.target.scale - behaviour.origin.scale) * percComplete,
             opacity: (behaviour.target.opacity - behaviour.origin.opacity) * percComplete
         };

         selectedHotspot.position.set(behaviour.origin.pX + difference.pX,behaviour.origin.pY +difference.pY,behaviour.origin.pZ + difference.pZ);
         selectedHotspot.scale.set(behaviour.target.scale + difference.scale,behaviour.target.scale + difference.scale,behaviour.target.scale + difference.scale);
         selectedHotspot.setOpacity(behaviour.target.opacity + difference.opacity);
     }

    }

}

function updateSliders(){

    if(editMode != editModes.behaviour)
        return;

    if(video)
        $( "#behaviourVideoTimeSlider" ).slider( "option", "value", video.currentTime.toFixed(2) );

    if(voiceover)
        if(voiceover.getPlaying())
            $( "#behaviourVoiceTimeSlider" ).slider( "option", "value", voiceover.getCurrentTime().toFixed(2) );
}


function onBehaviourTypeSelectionChange(value){

    if(selectedHotspot)
    selectedHotspot.data.behaviour.type = value;

    if(value == 'video')
    {

        $("#behaviourVideo").show();
        $("#behaviourVoice").hide();
    }
    else if (value == 'voice')
    {
        $("#behaviourVideo").hide();
        $("#behaviourVoice").show();
    }
}

function updateBehaviour(){
    console.log('updateBehaviour()');

    if(!selectedHotspot || editMode != editModes.behaviour || sceneEditMode != sceneEditModes.hotspots)
        return;

    if(!selectedHotspot.data.behaviour)
        selectedHotspot.data.behaviour = {};

    if(!selectedHotspot.data.behaviour.type)
        selectedHotspot.data.behaviour.type = 'video';

    var currentTime = selectedHotspot.data.behaviour.type == 'video'
        ? video.currentTime
        : selectedHotspot.data.behaviour.type == 'voice'
            ? voiceover.getCurrentTime()
            : null;

    var behaviour = null;

    if(typeof selectedHotspot.data.behaviour === 'object'){
        if(selectedHotspot.data.behaviour.values)
        selectedHotspot.data.behaviour.values.some(function(b){
            if(b.time === currentTime)
                behaviour = b;
        });
        else
            selectedHotspot.data.behaviour.values = [];
    }
    else
        updateBehaviourSlider();

    if(behaviour == null){
        behaviour = {};
        behaviour.time = currentTime;

        if(selectedHotspot.data.behaviour.values)
            selectedHotspot.data.behaviour.values.push(behaviour);
    };

    behaviour.position = selectedHotspot.position.clone();
   // behaviour.rotation = selectedHotspot.rotation.clone();
    behaviour.scale = selectedHotspot.data.scale || selectedHotspot.data.config.scale;
    behaviour.opacity = selectedHotspot.data.opacity || selectedHotspot.data.config.opacity;


    updateBehaviourDialogue();
    addBehaviours(selectedHotspot);
}

function updateBehaviourDialogue(){

    $('#behaviours').empty();

    if(!selectedHotspot.data.behaviour)
    return;

    selectedHotspot.data.behaviour.values = selectedHotspot.data.behaviour.values.sort(function (a, b) {
        return parseFloat(a.time) - parseFloat(b.time);
    });

    selectedHotspot.data.behaviour.values.forEach(function(behaviour){

        if(!behaviour.time)
        return;

        var id = behaviour.time.toString().replace(".","-");

        $('#behaviours').append('<div id="be_'+id+'" ><span><b>'+behaviour.time+' sec </b></span></div>');

        $('#be_' + id).append('<i id="ic_'+id+'" style="float: right" class="fa fa-fw fa-trash" ></i><br/>');

        if(behaviour.position && (behaviour.position.x))
            $('#be_' + id).append('<span>Position (X:'+behaviour.position.x.toFixed(2) +', Y:'+behaviour.position.y.toFixed(2) +', Z:'+behaviour.position.z.toFixed(2) +')</span><br/>');

      //  if(behaviour.rotation)
      //      $('#be_' + id).append('<span>Rotation (X:'+behaviour.rotation._x.toFixed(2) +', Y:'+behaviour.rotation._y.toFixed(2) +', Z:'+behaviour.rotation._z.toFixed(2) +')</span><br/>');

        if(behaviour.scale)
            $('#be_' + id).append('<span>Scale: '+behaviour.scale+'</span><br/>');

        if(behaviour.opacity)
            $('#be_' + id).append('<span>Opacity: '+behaviour.opacity+'</span><br/>');

        $('#be_' + id).click(selectBehaviour);
        $('#ic_' + id).click(removeBehaviour);
    });

    saveHotspot();
}

function removeBehaviour(e){
    e.stopPropagation();
    var id = this.id.substr(3).replace("-",".");

    selectedHotspot.data.behaviour.values = $.grep(selectedHotspot.data.behaviour.values, function(e){
        return e.time != id;
    });

    updateBehaviourDialogue();

}

function clearBehaviours(){

    if(selectedHotspot.tween){
        selectedHotspot.tween.stop();
        delete selectedHotspot.tween;
    }


    if(selectedHotspot.data.behaviour){
        delete selectedHotspot.data.behaviour;
        delete selectedHotspot.behaviours;
        saveHotspot();
    }

    updateBehaviourDialogue();
}


function selectBehaviour(e){
    var id = this.id.substr(3).replace("-",".");

    selectedHotspot.data.behaviour.values.some(function(b){
        if(b.time == id) {

            $( "#behaviourVideoTimeSlider" ).slider( "option", "value", b.time );

            video.currentTime = b.time;

            if(b.position)
                selectedHotspot.position.copy(b.position);

            if(b.rotation)
                selectedHotspot.rotation.copy(b.rotation);

            if(b.scale)
                selectedHotspot.scale.set(b.scale,b.scale,b.scale);

            if(b.opacity)
                selectedHotspot.opacity = b.opacity;
        }
    })
}

function pauseBehaviours(){
    hotspots.forEach(function(hotspot){
        if(hotspot.currentTween)
            hotspot.currentTween.stop();

       // if(typeof hotspot.setConfiguration() === 'function')
        //    hotspot.setConfiguration();
    });
}

function resetRotation(){
    selectedHotspot.lookAt(camera.position);
    updateRotationSliders(selectedHotspot.rotation);

}

function onMediaUploadComplete(filename){


    if(!selectedHotspot.data.media)
        selectedHotspot.data.media = {};

    if(!selectedHotspot.data.media.files)
        selectedHotspot.data.media.files = [];

    var type = filename.toLowerCase().substring(filename.lastIndexOf('.'));

/*    var mediaTypes = [".mp4",".jpg",".jpeg",".png"];
    var audioTypes = [".mp3",".m4u",".m4a"];*/

    var file = {};
        file.type = type;
        file.name = filename;
    selectedHotspot.data.media.files.push(file);

    updateFileList();
    saveHotspot();
    updateHotspot();
}

function updateFileList(){

    $('#hotspotFiles').empty();

    if(!selectedHotspot)
        return;

    if(!selectedHotspot.data.media)
    return;

    if(selectedHotspot.data.media.files)
    selectedHotspot.data.media.files.forEach(function(file,index){
        $('#hotspotFiles').append('<a><span id="file_'+index+'">' + index + '- ' + file.type + '</span></a> <i id="file_delete_'+index+'" class="fa fa-fw fa-trash" ></i><br/>');
        $('#file_' + index).click(file,showMedia);
        $('#file_delete_' + index).click(file,deleteMedia);
    });
}

function showMedia(event){
    window.open('http://cdn.beek.co/' + event.data.name, '_blank');
}

function deleteMedia(event){

    selectedHotspot.data.media.files.splice(selectedHotspot.data.media.files.indexOf(event.data));

    if(selectedHotspot.data.media.files.length < 1)
    delete selectedHotspot.data.media.files;

    updateFileList();

    saveHotspot();
    updateHotspot();
}

function handleHotspotOptionTabChange(index){

    editMode = index;

    if(index == 1){
        updateHotspotDescription();
        pauseBehaviours();
    }
    if(index == 2){
        updateMedia();
        updateFileList();
        pauseBehaviours();
    }
    if(index == 3){
        updateShape();
    }
    if(index == 4){
        updateBehaviourSlider();
    }

}

function updateShape(){

    if(!selectedHotspot.data.media)
        return;

    if(selectedHotspot.data.media.fill)
        PF('fill').selectValue(selectedHotspot.data.media.fill);

    if(selectedHotspot.data.media.stroke)
        PF('stroke').selectValue(selectedHotspot.data.media.stroke);
}

var raycaster = new THREE.Raycaster();
var mouse = new THREE.Vector2();
var points = [];

function drawShape(edit){

    if(edit){

        if(selectedHotspot.children[0].name == 'temp'){
            selectedHotspot.children[0].material.opacity = 0.2;
        }

        selectedHotspot.data.media = {};
        selectedHotspot.data.media.shape = "points";
        selectedHotspot.data.media.points = [];
        dragControls.deactivate();
        renderer.domElement.addEventListener( 'mousedown', addPoint );

    }  else{

        saveHotspot();
        updateHotspot();
        renderer.domElement.removeEventListener( 'mousedown', addPoint );
        dragControls.activate();
    }
}

function addPoint(event){

    event.stopPropagation();


    var offset = $("#container").offset();
    var rect = renderer.domElement.getBoundingClientRect();
    mouse.x = ( ( (event.clientX - rect.left) - (offset.left/2) ) / ( rect.width - rect.left ) ) * 2 - 1;
    mouse.y = - ( ( event.clientY - rect.top ) / ( rect.bottom - rect.top) ) * 2 + 1;

    raycaster.setFromCamera( mouse, camera );

    var objects = [];
    objects.push(selectedHotspot);
    objects.push(panoVideoMesh);

    var intersects = raycaster.intersectObjects( objects, true  );

    if ( intersects.length > 0 ){

        var point = new THREE.Mesh(new THREE.SphereGeometry(5, 5, 5), new THREE.MeshBasicMaterial({  color: 0x00ffff }));
        point.name = "point_" + point.uuid;

        var vector = new THREE.Vector3().copy( intersects[ 0 ].point );

        intersects[ 0 ].object.worldToLocal( vector );

        var localPoint = {};
        localPoint.x = vector.x + 100;
        localPoint.y = 100 - vector.y;

        point.position.copy(intersects[0].point);
        scene.add(point);
        selectedHotspot.data.media.points.push(localPoint);
        points.push(point);
    }

}

function handleShapeFilledCheck(val){
    console.log("handleShapeFilledCheck(" + val);
    selectedHotspot.data.media.fill = val;

    saveHotspot();
    updateHotspot();
}

function handleShapeStrokeCheck(val){
    console.log("handleShapeStrokeCheck(" + val);
    selectedHotspot.data.media.stroke = val;

    saveHotspot();
    updateHotspot();
}

function createShape(type){

    if(!selectedHotspot.data.media)
        selectedHotspot.data.media = {};

    selectedHotspot.data.media.shape = type;

    saveHotspot();
    updateHotspot();
}

function clearShape(){
    points = [];
    if(selectedHotspot.data.media.type == 'shape')
        delete selectedHotspot.data.media.type;
    if(selectedHotspot.data.media.points)
        delete selectedHotspot.data.media.points;

    selectedHotspot.children.forEach(function(child){
        if(child.name == 'shape')
            selectedHotspot.remove(child);
    });

    selectedHotspot.points.forEach(function(point){
            scene.remove(point);
    });

    if(selectedHotspot.children[0].name == 'temp'){
        selectedHotspot.children[0].material.opacity = 0.2;
    }
}

function hotspotDuplicateSettings(id){

    var data;

    hotspots.some(function(item){
        if(item.name == id){
            data= item.data;
            return true;
        }

    });

    if(data){
        selectedHotspot.data.config.rotation = data.config.rotation;
        selectedHotspot.data.config.scale = data.config.scale;
        updateHotspot();
    }
}