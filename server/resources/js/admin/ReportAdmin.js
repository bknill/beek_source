/**
 * Created by Ben on 24/08/2017.
 */
var verb = {};
var _tasks = [];
var _actor;

verb.completeTask = {
    "id": "http://beek.co/completeTask",
    "name": {
        "en-US": "completed Task"
    },
    "description": {
        "en-US": "completed task"
    },
    "definition": {
        "name": { "en-US": "completed task" }
    }
};

verb.startModule = {
    "id": "http://beek.co/startModule",
    "name": {
        "en-US": "start module"
    },
    "description": {
        "en-US": "start module"
    },
    "definition": {
        "name": { "en-US": "start module" }
    }
};

verb.completeModule = {
    "id": "http://beek.co/completeModule",
    "name": {
        "en-US": "completed Module"
    },
    "description": {
        "en-US": "completed module"
    },
    "definition": {
        "name": { "en-US": "completed module" }
    }
};

verb.failTask = {
    "id": "http://beek.co/failTask",
    "name": {
        "en-US": "failed task"
    },
    "description": {
        "en-US": "failed task"
    },
    "definition": {
        "name": { "en-US": "failedTask" }
    }
};

verb.wrongHotspot = {
    "id": "http://beek.co/wrongHotspot",
    "name": {
        "en-US": "wrong hotspot"
    },
    "description": {
        "en-US": "wrong hotspot"
    },
    "definition": {
        "name": { "en-US": "wrong hotspot" }
    }
};

verb.hotspot = {
    "id": "http://beek.co/hotspot",
    "name": {
        "en-US": "hotspot"
    },
    "description": {
        "en-US": "hotspot"
    },
    "definition": {
        "name": { "en-US": "hotspot" }
    }
};
verb.voiceRecording = {
    "id": "http://beek.co/voiceRecording",
    "name": {
        "en-US": "voice recording"
    },
    "description": {
        "en-US": "voice recording"
    },
    "definition": {
        "name": { "en-US": "voice recording" }
    }
};

function createObject(guideId, sceneId, taskId){
    return {
        "id": "http://beek.co/g" + guideId + "/s" + sceneId + "/t" + taskId,
        "definition": {
            "name": {
                "taskId": taskId
            }
        }
    }
}

function createAgent(mbox){
    return {
        "mbox": mbox
    }
}

function createHotspotObject(guideId, sceneId, hotspotId){
    return {
        "id": "http://beek.co/g" + guideId + "/s" + sceneId + "/h"+ hotspotId
    }
}

function createVoiceObject(gameTask,sceneId, taskId){
    return {
        "id": "http://beek.co/g" + guideId + "/s" + sceneId + "/t"+ taskId,
        "definition": {
            "name": { "id": url }
        }
    }
}

function setupLRS(tasks){
    console.log('setupLRS');
    _tasks = tasks;

    try {
        lrs = new TinCan.LRS(
            {
                endpoint: "	https://cloud.scorm.com/tc/6L1HLLI0ZF/",
                username: "oFS-6vV2S8yKJBpb7VU",
                password: "EWzy_KsKyqf5-ex2rmE",
                allowFail: false
            }
        );

    }
    catch (ex) {
        console.log("Failed to setup LRS object: " + ex);
        // TODO: do something with error, can't communicate with LRS
    }
}

function queryLRS(agent,verb,object,callback){

   // console.log('queryLRS',verb,object);

    var params = {};
    params.since = "2017-04-01T00:00:00Z";

    if(agent != null)
        params.agent = new TinCan.Agent(agent);
    if(verb != null)
        params.verb = new TinCan.Verb(verb);
    if(object != null)
        params.activity = new TinCan.Activity(object);


    lrs.queryStatements(
        {
            params: params,
            callback: function (err, sr) {
                if (err !== null) {
                    console.log("Failed to query statements: " + err);
                    // TODO: do something with error, didn't get statements
                    return;
                }

                if (sr.more.length > 1) {

                    console.log("sr.more",sr.more);
                    // TODO: additional page(s) of statements should be fetched
                }
                callback(sr);
                // TODO: do something with statements in sr.statements
            }
        }
    );
}

function getInitialReportsData(){

    var previousScene = null;

    //http://beek.co/g387/s2098/t711

   // queryLRS(null,null,createObject(387,2098,711),function(data){populateList(_tasks[0],data)});

    _tasks.forEach(function(task){

        if(task.scene){
            queryLRS(null,null,createObject(task.guideId,task.scene,task.id),function(data){populateList(task,data)});
            previousScene = task.scene;
        }
        else if(previousScene != null)
            queryLRS(null,null,createObject(task.guideId,previousScene,task.id),function(data){populateList(task,data)});
    })
}

var recordsDisplay = [];
function populateList(task,data){

    data.statements.forEach(function(statement){


        var record ={
            actor: statement.actor.mbox.replace("mailto:",""),
            verb: statement.verb.id,
            date: new Date(statement.timestamp),
            task : task
        };

       var existing = null;
        recordsDisplay.some(function(r){
           if(r.actor == record.actor){
               existing = r;
               return;
           }
       })

        if(existing != null){
           if(record.verb == verb.completeTask.id)
            existing.correct ++;
            else if(record.verb == verb.failTask.id)
               existing.incorrect ++;

            if(task && existing.task && (record.verb === verb.completeTask.id))
                if(parseInt(task.order) > parseInt(existing.task.order))
                    existing.task = task;
        }
        else
        {
            recordsDisplay.push({
                id: recordsDisplay.length +1,
                actor: record.actor,
                date : record.date,
                task : task,
                correct: record.verb == verb.completeTask.id ? 1 : 0,
                incorrect: record.verb == verb.failTask.id ? 1 : 0
            });
        }
    });

    $("#recordsTable").empty();
    recordsDisplay.forEach(function(record){
        $("#recordsTable").append("<div id='r"+ record.id+"'><a href='#' ><span>"+record.actor +" </span> <span> completed: "+record.correct+" </span> <span> failed: "+record.incorrect+" </span><span> Achieved: " +(record.task.order + 1)+ "/"+(_tasks.length)+"</span></div>");
        $("#r"+record.id).bind("click",function(){loadRecord(record)});
    });

}
//var date = statement.timestamp.split("T");
/**/

function loadRecord(record){
    _actor = createAgent(record.actor);
    PF('recordForm').show();
}

function recordFormReady(){

    _tasks.forEach(function(task){
        queryLRS(_actor,null,createObject(task.guideId,task.scene,task.id),function(data){applyTaskData(task,data)});

        if(task.code == 'findhotspots'){
            var data = $("#task_" + task.id + "_hotspots").val();

            if(data){
                var list = JSON.parse(data);
                list.forEach(function(id){
                    queryLRS(_actor,null,createHotspotObject(task.guideId,task.scene,id),function(data){applyHotspotData(task,id,data)});
                })
            }
       }
    })
}

function applyTaskData(task,data){

    if(data.statements.length)
    data.statements.forEach(function(statement) {

        var date = statement.timestamp.split("T");

        if(statement.verb.id == verb.completeTask.id)
            $("#task_" + task.id).append("<span>completed</span><span>"+date[1]+" </span><br/>");
        else if(statement.verb.id == verb.failTask.id)
            $("#task_" + task.id).append("<span>failed</span><span> "+date[1]+" </span><br/>");
    })
}

function applyHotspotData(task,id,data){
    console.log("hotspot data",data.statements.length);

    if(data.statements.length)
        data.statements.forEach(function(statement) {
           console.log("in " + task.id + " hotspot " + id + " was clicked");
        });
}