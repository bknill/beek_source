var camera, scene, dolly, renderer,box,
	deviceControls, dragControls, container,fontsAvailable = false,
	controls,manager,lastRender, dragSphere,recordFocusInterval,equiManaged,cubeCamera,greenSphere,interupted = false,voiceoverWave;
var materials = [];
var texture_placeholder,
    isUserInteracting = false,
    phi = 0,
    theta = 0,
    target,
	previewTileCounter,
	isMobile,
	animationFrame,
	fullscreen = false,
	_touchZoomDistanceEnd,
	_touchZoomDistanceStart = 0,
	_touchZoomDistanceEnd = 0,
	draggingHotspot;


var showHowColors = {
	grey: '#a7a9ac',
	purple: '#662d91',
	lightGrey: "#e6e8eb"
};

var sceneEditMode,
	sceneEditModes = {
	defaults : 0,
	voiceover : 1,
	video : 2,
	hotspots : 3
};



var targetList = [];
var projector, mouse = { x: 0, y: 0 };

var iframeWidth, iframeHeight;

var jsonpPrefix = 'http://gms.beek.co';
var cdnPrefix =  '//cdn.beek.co';

var FACES = "rludfb";
var TILES = ['0_0', '0_1', '1_0', '1_1'];

var tiles, bubbles;

var player = {width:0,height:0};

var hasFocus = true;

/*window.addEventListener('error', function(e) {
	console.log(e);
}, true);*/




	function initSceneAdmin(xhr, status, args) {


		console.log("initSceneAdmin",xhr, status, args);

		player.width = $("#container").width();
		player.height = 500;//$(".ui-tabs-panels").height() * 0.5;

		if(window.location.hostname == 'beekdev.co')
			jsonpPrefix = 'http://beekdev.co:9081';

		if(window.location.hostname == 'beeksuper')
			jsonpPrefix = 'http://beeksuper:9081';

			createPlayer('container');

		console.log(args.sceneData);
		if(args.sceneData)
		startScene(JSON.parse(args.sceneData));
		animate();

	//	$(document).on("voiceover",createAudioSliders);
	}



	function createPlayer(entity){

		console.log('createPlayer()');

		container = document.getElementById(entity);

		container.oncontextmenu = function ()
		{
			//showCustomMenu();
			return false;     // cancel default menu
		}

		camera = new THREE.PerspectiveCamera(75, player.width / player.height, 1, 1024);

		scene = new THREE.Scene();

		// preserveDrawingBuffer: true  is required for getting thumbnail
		renderer = new THREE.WebGLRenderer({antialias: false, alpha:true, preserveDrawingBuffer: true  });
		renderer.setPixelRatio( window.devicePixelRatio );
		renderer.setClearColor( 0x00ff00, 0 );
		renderer.setSize(player.width, player.height);
		renderer.render(scene, camera);
		container.appendChild(renderer.domElement);

		controls = new THREE.VRControls(camera);
		controls.standing = true;

		var vrEffect = new THREE.VREffect(renderer);
		vrEffect.setSize(player.width, player.height);

		var params = {predistorted: true};
		manager = new WebVRManager(renderer, vrEffect, params);

		scene.add(camera);

		dragControls = new THREE.DragControls(hotspots, camera, renderer.domElement);
		dragControls.addEventListener( 'dragstart', function ( event ) { onDragHotspotStart(event)} );
		dragControls.addEventListener( 'dragend', function ( event ) { onDragHotspotEnd(event)  } );
		dragControls.addEventListener( 'nodrag', function ( event ) { onDragEmpty()  } );
		dragControls.deactivate();
	 }

     function removePlayer(){
		 console.log("removePlayer()");
		 $('#container').empty();
		 $('#taskContainer').empty();
		 $('#videoMarker').remove();

		 if(PF('playButton').input[0].checked)
		 	PF('playButton').toggle();

		 clearSliders();

		 pause();

		 if(video)
			 removePanoVideo();

		 if(voiceover)
			 voiceover.stop();

		 clearScene();

		 cancelAnimationFrame(animationFrame);

		 if(currentScene)
		 	currentScene = null;

		 scene = null;
	 }

    function onWindowResize() {

		console.log("beek.onWindowResize");

		camera.aspect = player.width/ player.height;
		camera.updateProjectionMatrix();
		renderer.setSize( player.width, player.height );
    }

function createAudioSliders(){
	console.log('createAudioSlider',currentScene.voiceover, $('#audio_waveform').contents().length);

	if(video){
		video.currentTime = videoSettings.startTime || 0;
		pause();
	}

	if(currentScene.voiceover && $('#audio_waveform').contents().length == 0){
		$('#audio_waveform').css('width','95%');
		voiceover = WaveSurfer.create({
			container: '#audio_waveform'
		});
		voiceover.load('http://cdn.beek.co/' + currentScene.voiceover.filename);

		voiceover.on('ready', function () {

			voiceover.enableDragSelection({});

			 voiceover.playRegion = function(time){
				voiceover.region.play();
			};

			voiceover.region = voiceover.addRegion({
				start: currentScene.voiceover.startTime || 0, // time in seconds
				end: currentScene.voiceover.endTime || voiceover.getDuration(), // time in seconds
				color: 'hsla(100, 100%, 30%, 0.1)'
			});


			var EQ = [
				{
					f: 32,
					type: 'lowshelf'
				}, {
					f: 64,
					type: 'peaking'
				}, {
					f: 125,
					type: 'peaking'
				}, {
					f: 250,
					type: 'peaking'
				}, {
					f: 500,
					type: 'peaking'
				}, {
					f: 1000,
					type: 'peaking'
				}, {
					f: 2000,
					type: 'peaking'
				}, {
					f: 4000,
					type: 'peaking'
				}, {
					f: 8000,
					type: 'peaking'
				}, {
					f: 16000,
					type: 'highshelf'
				}
			];

			// Create filters
			var filters = EQ.map(function (band) {
				var filter = voiceover.backend.ac.createBiquadFilter();
				filter.type = band.type;
				filter.gain.value = 0;
				filter.Q.value = 1;
				filter.frequency.value = band.f;
				return filter;
			});

			// Connect filters to wavesurfer
			voiceover.backend.setFilters(filters);

			// Bind filters to vertical range sliders
			var container = document.querySelector('#audio_equalizer');
			filters.forEach(function (filter) {
				var input = document.createElement('input');
				voiceover.util.extend(input, {
					type: 'range',
					min: -40,
					max: 40,
					value: 0,
					title: filter.frequency.value
				});
				input.style.display = 'inline-block';
				input.setAttribute('orient', 'vertical');
				voiceover.drawer.style(input, {
					'webkitAppearance': 'slider-vertical',
					width: '50px',
					height: '100px'
				});
				container.appendChild(input);

				var onChange = function (e) {
					filter.gain.value = ~~e.target.value;

					var values = [];

					filters.forEach(function (filter) {
						var value = {};
						value.name = filter.frequency.value;
						value.gain = filter.gain.value;
						values.push(value);
					});
					currentScene.voiceover.filters = values;
					setVoiceoverSettings();
				};

				input.addEventListener('input', onChange);
				input.addEventListener('change', onChange);
			});

			// For debugging
			voiceover.filters = filters;

		});

		voiceover.on('region-update-end',function(event){
			currentScene.voiceover.startTime = event.start;
			currentScene.voiceover.endTime = event.end;
			setVoiceoverSettings();
		});

		voiceover.on('seek',function(event){
			video.currentTime = event;
		});

		if(currentScene.voiceover.focus)
			setTimeout(function(){
				currentScene.voiceover.focus.points.forEach(function(point){
					addFocusMarker(point);
				});
			},200);
		else
			$('#modeTab\\:sceneSettings\\:deleteAudioFocusTrack').hide();

	}




}

function createVideoSliders(){
	console.log('createVideoSlider');

	if(currentScene.video){

		$("#brightnessSlider").slider({
			min : -1,
			max : 1,
			step : 0.001,
			value : currentScene.video.brightness,
			slide: function( event, ui ) {	panoShader.uniforms[ "brightness" ].value = ui.value},
			change: function( event, ui ) {currentScene.video.brightness = ui.value; setVideoSettings();}
		});
		$("#contrastSlider").slider({
			min : -1,
			max : 1,
			step : 0.001,
			value : currentScene.video.contrast,
			slide: function( event, ui ) {panoShader.uniforms[ "contrast" ].value = ui.value},
			change: function( event, ui ) {currentScene.video.contrast = ui.value; setVideoSettings();}
		});
		$("#hueSlider").slider({
			min : -1,
			max : 1,
			step : 0.001,
			value : currentScene.video.hue,
			slide: function( event, ui ) {panoShader.uniforms[ "hue" ].value = ui.value},
			change: function( event, ui ) {currentScene.video.hue = ui.value; setVideoSettings();}
		});
		$("#saturationSlider").slider({
			min : -1,
			max : 1,
			step : 0.001,
			value : currentScene.video.saturation,
			slide: function( event, ui ) {panoShader.uniforms[ "saturation" ].value = ui.value},
			change: function( event, ui ) {currentScene.video.saturation = ui.value; setVideoSettings();}
		});

		$("#volumeSlider").slider({
			min : 0.01,
			max : 1,
			step : 0.01,
			value : currentScene.video.volume || 1,
			slide: function( event, ui ) {video.volume = ui.value},
			change: function( event, ui ) {currentScene.video.volume = ui.value; setVideoSettings();}
		});

		video.slider = $("#videoTimeSlider").data("ionRangeSlider");

		if(!video.slider)
			$("#videoTimeSlider").ionRangeSlider({
				type: "double",
				grid: true,
				min: 0,
				step: 0.01,
				max: currentScene.video.duration,
				from: currentScene.video.startTime,
				to: currentScene.video.endTime || currentScene.video.duration,
				onChange: sliderChangeHandler,
				onFinish: sliderFinishHandler
			});
		else
			video.slider.update({
				max: currentScene.video.duration,
				from: currentScene.video.startTime,
				to: currentScene.video.endTime || currentScene.video.duration
			});



		$('#videoMarker').remove();
		$("#videoSlider").append('<span class="timeMarker" id="videoMarker">|</span>');

		video.updateTimer = setInterval(function(){
			if(video)
				$('#videoMarker').css('left',(video.currentTime / video.duration) * $("#videoSlider .irs").width());
		},100);


		$('#videoSlider .irs').click(function (e){
			var elm = $(this);
			var xPos = e.pageX - elm.offset().left;
			var perc = xPos/$('#videoSlider .irs').width();

			video.currentTime = video.duration * perc;
		});


		if(currentScene.video.focus)
			setTimeout(function(){
				currentScene.video.focus.points.forEach(function(point){
					addFocusMarker(point);
				});
			},200);
		else
			$('#modeTab\\:sceneSettings\\:deleteVideoFocusTrack').hide();

	}



	function sliderChangeHandler(data){
		if(data.from != currentScene.video.startTime){
			currentScene.video.startTime = video.currentTime = data.from;
			video.pause();
		}
		else if(data.to != currentScene.video.endTime){
			currentScene.video.endTime = video.currentTime  = data.to;
			video.pause();
		}

	}

	function sliderFinishHandler(data){

		if(currentScene.video.endTime > video.currentTime)
			video.play();

		setVideoSettings();
	}


}

function clearSliders(){

	console.log('clearSliders');

	if(voiceover){
/*
		if(voiceover.slider)
			voiceover.slider.destroy();
*/

		if(voiceover.updateTimer)
		clearInterval(voiceover.updateTimer);

	}

	if(video){
		if(video.slider)
			video.slider.destroy();

		if(video.updateTimer)
			clearInterval(video.updateTimer);
	}


}

function setVideoSettings(){
	$("#modeTab\\:modalScene\\:videoSettings").val(JSON.stringify(currentScene.video));
	updateVideoSettings();
}

function deleteVoiceover(){
	currentScene.voiceover = null;
	$('#modeTab\\:modalScene\\:voiceoverSettings').val(null);
	updateVoiceover();
	if(voiceover != null)
		voiceover.stop();
}


function setVoiceoverSettings(){

	console.log('setVoiceoverSettings()',currentScene.voiceover);
	if(typeof currentScene.voiceover === 'object'){
		$('#modeTab\\:modalScene\\:voiceoverSettings').val(JSON.stringify(currentScene.voiceover));
		updateVoiceover();
	}
}


function handleRecordFocus(recording){

	if(recording){

		if(sceneEditMode == sceneEditModes.video){

			if(currentScene.voiceover)
				if(currentScene.voiceover.focus){
					showMessage("Focus Points","This scene voiceover already has focus points. Please remove these before adding focus points to this video");
					return false;
				}

			if(!currentScene.video.focus){
				currentScene.video.focus = {};
				currentScene.video.focus.points = [];
			}

			currentScene.video.focus.points.push({
				startTime : video.currentTime,
				vector: getWorldVector()
			});
		}
		else if(sceneEditMode == sceneEditModes.voiceover){

			if(currentScene.video)
				if(currentScene.video.focus){
					showMessage("Focus Points","This scene video already has focus points. Please remove these before adding focus points to this voiceover");
					return false;
				}

			if(typeof currentScene.voiceover !== 'object'){
				var filename = currentScene.voiceover;
				currentScene.voiceover = {};
				currentScene.voiceover.filename = filename;
			}

			if(!currentScene.voiceover.focus){
				currentScene.voiceover.focus = {};
				currentScene.voiceover.focus.points = [];
			}

			currentScene.voiceover.focus.points.push({
				startTime : voiceover.getCurrentTime(),
				vector: getWorldVector()
			});
		}



		recordFocusInterval = setInterval(recordFocus,1000);
	}

	else if(recordFocusInterval){

		if(sceneEditMode == sceneEditModes.video && currentScene.video.focus){
			currentScene.video.focus.points[currentScene.video.focus.points.length-1].endTime = video.currentTime;
			addFocusMarker(currentScene.video.focus.points[currentScene.video.focus.points.length-1]);
			$('#modeTab\\:sceneSettings\\:deleteVideoFocusTrack').show();

			setVideoSettings();
		}
		else if(sceneEditMode == sceneEditModes.voiceover && currentScene.voiceover.focus){
			currentScene.voiceover.focus.points[currentScene.voiceover.focus.points.length-1].endTime = voiceover.getCurrentTime();
			addFocusMarker(currentScene.voiceover.focus.points[currentScene.voiceover.focus.points.length-1]);
			$('#modeTab\\:sceneSettings\\:deleteVideoFocusTrack').show();

			setVoiceoverSettings();
		}


		clearInterval(recordFocusInterval);


	}


	function recordFocus(){


		if(sceneEditMode == sceneEditModes.video && currentScene.video) {
			if (currentScene.video.focus.points[currentScene.video.focus.points.length - 1].vector.x != getWorldVector().x) {
				currentScene.video.focus.points[currentScene.video.focus.points.length - 1].endTime = video.currentTime;

				addFocusMarker(currentScene.video.focus.points[currentScene.video.focus.points.length - 1]);

				currentScene.video.focus.points.push({
					startTime: video.currentTime,
					vector: getWorldVector()
				});

				setVideoSettings();
			}
		}

		else if(sceneEditMode == sceneEditModes.voiceover && currentScene.voiceover) {
			if (currentScene.voiceover.focus.points[currentScene.voiceover.focus.points.length - 1].vector.x != getWorldVector().x) {
				currentScene.voiceover.focus.points[currentScene.voiceover.focus.points.length - 1].endTime = voiceover.getCurrentTime();

				addFocusMarker(currentScene.voiceover.focus.points[currentScene.voiceover.focus.points.length - 1]);

				currentScene.voiceover.focus.points.push({
					startTime: video.currentTime,
					vector: getWorldVector()
				});

				setVoiceoverSettings();
			}
		}
	}

	function getWorldVector(){
		var dist = 200;
		var vec = new THREE.Vector3( 0, 0, -dist );
		vec.applyQuaternion( camera.quaternion );
		return vec;
	}
}

function addFocusMarker(focusMarker){

	var id = focusMarker.startTime.toString().replace('.','');

	if(sceneEditMode == sceneEditModes.video){
		$("#videoSlider").append('<div class="focusMarker" id="focusMarker_'+id+'">');
		var left = (focusMarker.startTime / video.duration) * $("#videoSlider .irs").width();
		var width = ((focusMarker.endTime - focusMarker.startTime)/video.duration) * $("#videoSlider .irs").width();
		$("#focusMarker_" + id).css({"left" : left,"width": width});
	}
	else if(sceneEditMode == sceneEditModes.voiceover){
		$("#audioSlider").append('<div class="focusMarker" id="focusMarker_'+id+'">');
		var left = (focusMarker.startTime / voiceover.getDuration()) * $("#audioSlider .irs").width();
		var width = ((focusMarker.endTime - focusMarker.startTime)/voiceover.getDuration()) * $("#audioSlider .irs").width();
		$("#focusMarker_" + id).css({"left" : left,"width": width});
	}

}

function clearFocusPoints(){

	if(sceneEditMode == sceneEditModes.video) {
		if (currentScene.video.focus)
			delete currentScene.video.focus;

		$(".focusMarker").remove();
		$('#modeTab\\:sceneSettings\\:deleteVideoFocusTrack').hide();

		setVideoSettings();
	}
	else if(sceneEditMode == sceneEditModes.voiceover) {
		if (currentScene.voiceover.focus)
			delete currentScene.voiceover.focus;

		$(".focusMarker").remove();
		$('#modeTab\\:sceneSettings\\:deleteAudioFocusTrack').hide();

		setVoiceoverSettings();
	}
}


function detectHotspotClick(){
        isUserInteracting = true;


        if(selectedHotspot && !viewingHotspot){
        	TWEEN.removeAll();
        	selectedHotspot = null;
        }

		var offset = container.offset();
		var rect = renderer.domElement.getBoundingClientRect();
		mouse.x = ( ( (event.clientX - rect.left) - (offset.left/2) ) / ( rect.width - rect.left ) ) * 2 - 1;
		mouse.y = - ( ( event.clientY - rect.top ) / ( rect.bottom - rect.top) ) * 2 + 1;

		raycaster.setFromCamera( mouse, camera );

    	var intersects = raycaster.intersectObjects( hotspots, true );

    	if ( intersects.length > 0){
    		hotspotClick(intersects[ 0 ].object);
    	}
    }

    function onDocumentMouseDown(e) {

        e.preventDefault();

		if(draggingHotspot)
		return;

		var rect = renderer.domElement.getBoundingClientRect();
		mouse.x = ( ( event.clientX - rect.left  ) / ( rect.width - rect.left ) ) * 2 - 1;
		mouse.y = - ( ( event.clientY - rect.top ) / ( rect.bottom - rect.top) ) * 2 + 1;

     	detectHotspotClick();
    }



    function onDocumentMouseWheel(event) {

    		 setZoom(camera.fov - event.wheelDeltaY * 0.05);

    }


    function setZoom(fov){

        camera.fov = fov;

        if(camera.fov < 30) camera.fov = 30;
        if(camera.fov > 100) camera.fov = 100;

        camera.updateProjectionMatrix();

    }

    function tweenZoom(fov){

		var newFov = {nFov : camera.fov};
		var targetFov = {nFov : fov};


		var tweenZoom = new TWEEN.Tween( newFov ).to( targetFov, 100 ).easing(TWEEN.Easing.Cubic.Out);
		tweenZoom.onUpdate(function(){
			camera.fov = newFov.nFov;
			camera.updateProjectionMatrix();
		}).start();

    }

    function remoteUpdate(rLon, rLat, rFov){

    	if(rLon == lon && rLat == lat && rFov == camera.fov)
    		return;

    		lon = rLon;
    		lat = rLat;
    		setZoom(rFov);

    }


    zoomCamera = function () {

		if ( _state === STATE.TOUCH_ZOOM_PAN ) {

			var factor = _touchZoomDistanceStart / _touchZoomDistanceEnd;
			_touchZoomDistanceStart = _touchZoomDistanceEnd;
			_eye.multiplyScalar( factor );

		} else {

			var factor = 1.0 + ( _zoomEnd.y - _zoomStart.y ) * _this.zoomSpeed;

			if ( factor !== 1.0 && factor > 0.0 ) {

				_eye.multiplyScalar( factor );

				if ( _this.staticMoving ) {

					_zoomStart.copy( _zoomEnd );

				} else {

					_zoomStart.y += ( _zoomEnd.y - _zoomStart.y ) * this.dynamicDampingFactor;

				}

			}

		}

	};


    function animate(timestamp) {

		if(scene != null){
			animationFrame = requestAnimationFrame(animate);
			update(timestamp);
		}




    }

    function update(timestamp) {

	if(controls && !draggingHotspot && scene != null)
		controls.update();

		var delta = Math.min(timestamp - lastRender, 500);
		lastRender = timestamp;

		if(!interupted)
		manager.render(scene, camera, timestamp);

  //   renderer.render(scene, camera);

		TWEEN.update();

		if(menu)menuUpdate();
	}


    function latLonToVector(lat,lon){
        lat = Math.max(-85, Math.min(85, lat));
        phi = THREE.Math.degToRad(90 - lat);
        theta = THREE.Math.degToRad(lon);

        var vector = new THREE.Vector3();
        vector.x = 512 * Math.sin(phi) * Math.cos(theta);
        vector.y = 512 * Math.cos(phi);
        vector.z = 512 * Math.sin(phi) * Math.sin(theta);

    	return vector;
    }

    function vectorToLatLon(position){

    	// another solution posted on SO if this one is unreliable
    	//var lat = Math.atan2( Math.sqrt( target.x*target.x + target.z*target.z) , target.y) ;
    	//var lon = Math.atan2( target.x, target.y);

    	return {lat : Math.acos(position.y / 512), lon  : Math.atan(position.x / position.z) };
    }

    function panTiltToVector(pan,tilt,distance){

        distance = distance/4;
        var pr = THREE.Math.degToRad( pan + 90);
        var tr = THREE.Math.degToRad( tilt );

        var vector = new THREE.Vector3(distance * Math.cos(pr) * Math.cos(tr),distance * Math.sin(tr),distance * Math.sin(pr) * Math.cos(tr));

        return vector;
    }

    function hotspotPanTiltToLatLon(pan,tilt){

    	pan += 90;
    	pan = pan > 0 ? pan : 360 + pan;
    	pan = pan < 360 ? pan : 360 - pan;

    	return { lat : tilt, lon : pan };
    }

    function panTiltToLatLon(pan,tilt){

    	pan -= 90;
    	pan = pan > 0 ? pan : 360 + pan;
    	pan = pan < 360 ? pan : 360 - pan;

    	return { lat : -tilt, lon : pan };
    }


    var isTouch = (('ontouchstart' in window) || (navigator.msMaxTouchPoints > 0));


function setDefaultView(){
	console.log('setDefaultView()');

	var canvas = document.createElement('canvas');
	var ctx = canvas.getContext('2d');
	canvas.width = 400;
	canvas.height = 400;

	ctx.drawImage(renderer.domElement,- (renderer.domElement.width/2 - canvas.width/2),- (renderer.domElement.height/2 - canvas.height/2), renderer.domElement.width, renderer.domElement.height);
	var resizedImageData = canvas.toDataURL('image/png');

	$("#modeTab\\:sceneSettings\\:sceneSettingsForm\\:sceneThumbImage").attr("src",resizedImageData);


	$.ajax({
		url:'/scene/'+currentScene.id+'/thumb',
		data:{imageValue:resizedImageData},
		type:'POST'
	});
}

function setScenePan(){

	var defaults = {
		theta:controls.getVRDisplay().theta_,
		phi:controls.getVRDisplay().phi_,
		fov: camera.fov
	};

	$("#modeTab\\:sceneSettings\\:sceneSettingsForm\\:scenePan").val((JSON.stringify(defaults)));
	updateSelectedScenePan();
}

function onAudioUploadComplete(filename){
	console.log('onAudioUploadComplete('+filename+')');


	if(voiceover)
		voiceover.stop();

	currentScene.voiceover = {};
	currentScene.voiceover.filename = filename;

	setVoiceoverSettings();
	$('#audio_waveform').empty();
	voiceover = null;
	//voiceover = createSound(currentScene.voiceover);
	createAudioSliders();

}

function updateVideoType(type){
	console.log(type);
    currentScene.video.type = type;
    setVideoSettings();
}

function launchFullscreen(element) {

		fullscreen = true;

		  if(element.requestFullscreen) {
		    element.requestFullscreen();
		  } else if(element.mozRequestFullScreen) {
		    element.mozRequestFullScreen();
		  } else if(element.webkitRequestFullscreen) {
		    element.webkitRequestFullscreen();
		  } else if(element.msRequestFullscreen) {
		    element.msRequestFullscreen();
		  }


//			if(inIframe() == true)
//			{
//			    $( "#openGuide" ).toggle();
//			    $( "#openSceneInfo" ).toggle();
//			}
		}


	function exitFullscreen() {
		console.log('exitFullscreen()');

		fullscreen = false;
		  if(document.exitFullscreen) {
		    document.exitFullscreen();
		  } else if(document.mozCancelFullScreen) {
		    document.mozCancelFullScreen();
		  } else if(document.webkitExitFullscreen) {
		    document.webkitExitFullscreen();
		  }

			if(inIframe() == true)
			{
			    $( "#openGuide" ).toggle();
			    $( "#openSceneInfo" ).toggle();
			}

		}




	$(window).blur(function(){
		  mainVolume = 0;
		});
		$(window).focus(function(){
			mainVolume = 0.5;
		});


		// requestAnim shim layer by Paul Irish
		window.requestAnimationFrame = function(){
		    return (
		        window.requestAnimationFrame       ||
		        window.webkitRequestAnimationFrame ||
		        window.mozRequestAnimationFrame    ||
		        window.oRequestAnimationFrame      ||
		        window.msRequestAnimationFrame     ||
		        function(/* function */ callback){
		            window.setTimeout(callback, 1000 / 60);
		        }
		    );
		}();

		function dynamicSort(property) {
		    var sortOrder = 1;
		    if(property[0] === "-") {
		        sortOrder = -1;
		        property = property.substr(1);
		    }
		    return function (a,b) {
		        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
		        return result * sortOrder;
		    };
		}


/*		function orientation(){
			if(window.innerHeight > window.innerWidth)
				return 'portrait';

			return 'landscape';
		}*/

		function inIframe() {
		    try {
		        return window.self !== window.top;
		    } catch (e) {

		    	iframeWidth = window.frameElement.offsetWidth,
		    	iframeHeight = window.frameElement.offsetHeight;

		        return true;
		    }
		}

		function showLoader(){
		console.log("Beek.showLoader()");
		$( "body" ).append(" <div class='spinner'> <div class='double-bounce1'></div> <div class='double-bounce2'></div> </div>");

		}

		function hideLoader(){
			console.log("Beek.hideLoader()");
			$( ".spinner" ).remove();
		}

	function shakeEventDidOccur () {

		deviceControls.pan = data.longitude;
		data.manualControl = false;

	}

	function isIOS(){
		return /iPhone|iPod/.test( navigator.userAgent );
	}

function nearestPow2( aSize ){
	return Math.pow( 2, Math.round( Math.log( aSize ) / Math.log( 2 ) ) );
}

function isIE11() {
	return !(window.ActiveXObject) && "ActiveXObject" in window;
}



function handleSceneSettingsTabChange(index){

	dragControls.deactivate();
	clearSliders();
	clearBoxHelpers();

	if(recordFocusInterval)
		clearInterval(recordFocusInterval);

	sceneEditMode = index;


	switch(index){
		case 1:
			createAudioSliders();
			break;
		case 2:
			createVideoSliders();
			break;
		case 3:
		    if(PF("hotspotTab"))PF("hotspotTab").select(0);
			pause();
			pauseBehaviours();
			dragControls.activate();
			break;
	}
}

function handlePlayButtonChange(paused){

	if(paused){
		if(video) {
			video.pause();
			$(document).trigger("panoVideoPause");
		}
		if(voiceover)voiceover.pause();
	}
	else{
		if(video) {
			video.play();
			$(document).trigger("panoVideoPlay");
		}

		if(voiceover){
			if(video)
			video.currentTime = currentScene.video.startTime || 0;
			voiceover.playRegion();
		}
	}

}

function pause(){
	PF("playButton").check();
}

function play(){
	PF("playButton").uncheck();
}


function showMessage(summary,detail){
	if(typeof guideId !== 'undefined')
		PF('growl').renderMessage({"summary":summary,
			"detail":detail,
			"severity":"info"})
	else
		PF('sceneGrowl').renderMessage({"summary":summary,
			"detail":detail,
			"severity":"info"})

}

function waitForFontAwesome( callback ) {
	var retries = 5;

	var checkReady = function() {
		var canvas, context;
		retries -= 1;
		canvas = document.createElement('canvas');
		canvas.width = 20;
		canvas.height = 20;
		context = canvas.getContext('2d');
		context.fillStyle = 'rgba(0,0,0,1.0)';
		context.fillRect( 0, 0, 20, 20 );
		context.font = '16pt FontAwesome';
		context.textAlign = 'center';
		context.fillStyle = 'rgba(255,255,255,1.0)';
		context.fillText( '\uf0c8', 10, 18 );
		var data = context.getImageData( 2, 10, 1, 1 ).data;
		if ( data[0] !== 255 && data[1] !== 255 && data[2] !== 255 ) {
			//console.log( "FontAwesome is not yet available, retrying ..." );
			if ( retries > 0 ) {
				setTimeout( checkReady, 200 );
			}
		} else {
			//console.log( "FontAwesome is loaded" );
			if ( typeof callback === 'function' ) {
				callback();
			}
		}
	}

	checkReady();
};

function waitForWebfonts(fonts, callback) {
	var loadedFonts = 0;
	for(var i = 0, l = fonts.length; i < l; ++i) {
		(function(font) {
			var node = document.createElement('span');
			// Characters that vary significantly among different fonts
			node.innerHTML = 'giItT1WQy@!-/#';
			// Visible - so we can measure it - but not on the screen
			node.style.position      = 'absolute';
			node.style.left          = '-10000px';
			node.style.top           = '-10000px';
			// Large font size makes even subtle changes obvious
			node.style.fontSize      = '300px';
			// Reset any font properties
			node.style.fontFamily    = 'sans-serif';
			node.style.fontVariant   = 'normal';
			node.style.fontStyle     = 'normal';
			node.style.fontWeight    = 'normal';
			node.style.letterSpacing = '0';
			document.body.appendChild(node);

			// Remember width with no applied web font
			var width = node.offsetWidth;

			node.style.fontFamily = font + ', sans-serif';

			var interval;
			function checkFont() {
				// Compare current width with original width
				if(node && node.offsetWidth != width) {
					++loadedFonts;
					node.parentNode.removeChild(node);
					node = null;
				}

				// If all fonts have been loaded
				if(loadedFonts >= fonts.length) {
					if(interval) {
						clearInterval(interval);
					}
					if(loadedFonts == fonts.length) {
						callback();
						return true;
					}
				}
			};

			if(!checkFont()) {
				interval = setInterval(checkFont, 50);
			}
		})(fonts[i]);
	}
};

function updateShader(brightness,contrast){

	var a= {b:0,c:0},t = {b:brightness,c:contrast};

	if(currentScene)
		if(currentScene.video instanceof Object ){
			a = {b: currentScene.video.brightness | 0, c: currentScene.video.contrast | 0};
			if(brightness == 0 && contrast == 0)
				t = {b:currentScene.video.brightness | 0,c:currentScene.video.contrast | 0};
		}

	if(panoShader != null){
		a.b = panoShader.uniforms[ "brightness" ].value;
		a.c = panoShader.uniforms[ "contrast" ].value;
	}

	new TWEEN.Tween( a ).to( t, 1200  ).easing(TWEEN.Easing.Cubic.Out).onUpdate(function(){
		if(panoShader != null){
			panoShader.uniforms[ "brightness" ].value = a.b;
			panoShader.uniforms[ "contrast" ].value = a.c;
		}
	}).start();
}

function updateScenePanoVideo(args){

	console.log('updateScenePanoVideo(',args);

	if(args.newVideoFileName){

		video.pause();

		source.setAttribute('src',cdnPrefix + "/" + args.newVideoFileName);
		video.appendChild(source);

		video.load();
		video.play();

		video.addEventListener('loadeddata', function() {
			console.log('loaded video, updating settings');
			currentScene.video = {};
			currentScene.video.filename = args.newVideoFileName;
			currentScene.video.hiRes = null;
			currentScene.video.lowRes = null;
			currentScene.video.startTime = 0.0;
			currentScene.video.endTime = 0.0;
			currentScene.video.duration = this.duration;
			currentScene.video.width = this.videoWidth;
			currentScene.video.height = this.videoHeight;

			setVideoSettings();
		}, false);
		//checkPlayBack();
	}

	scene.panoIncrement ++
}


function fixPFDialogToggleMaximize(dlg) {
	if (undefined == PF(dlg).doToggleMaximize) {
		PF(dlg).doToggleMaximize = PF(dlg).toggleMaximize;
		PF(dlg).toggleMaximize = function () {
			this.doToggleMaximize();
			var marginsDiff = this.content.outerHeight() - this.content.height();
			var newHeight = this.jq.innerHeight() - this.titlebar.outerHeight() - marginsDiff;
			this.content.height(newHeight);
		};
	}
}

function updateVideoImage(){
	console.log('updateVideoImage()');

	pause();

	if(panoVideoMesh != null)
		panoVideoMesh.visible = false;

	equiManaged = new CubemapToEquirectangular( renderer, false );
	cubeCamera = new THREE.CubeCamera( .1, 1000, 4096 );
	cubeCamera.position.copy( camera.position );
	cubeCamera.rotation.set(0,Math.PI/2,0);
	var options = { format: THREE.RGBAFormat, magFilter: THREE.LinearFilter, minFilter: THREE.LinearFilter };
	cubeCamera.renderTarget = new THREE.WebGLRenderTargetCube( 4096 , 4096 , options );

	cubeCamera.updateCubeMap( renderer, scene );
	equiManaged.setSize( 3840, 1920 );

	var data = equiManaged.convert( cubeCamera, false);
	processVideoImage(data);

	cubeCamera = null;
	equiManaged = null;

	if(panoVideoMesh != null)
		panoVideoMesh.visible = true;

}

function processVideoImage(imageData){

	var canvas = document.createElement('canvas');
	var ctx = canvas.getContext('2d');
	canvas.width =3840;
	canvas.height = 1920;

	ctx.putImageData( imageData, 0, 0 );
	var resizedImageData = canvas.toDataURL('image/png');

		$.ajax({
			url:'/scene/'+currentScene.id+'/hotspotImage',
			data:{imageValue:resizedImageData},
			type:'POST',
			success:function(data, status) {
				if(data.payload){
					currentScene.video.videoImage = data.payload;
					setVideoSettings();

					ctx.clearRect(0, 0, canvas.width, canvas.height);

				}
			}
		});
}