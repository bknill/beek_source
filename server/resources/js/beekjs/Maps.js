/**
 * Created by Ben on 13/03/2016.
 */
var mapsInitiated,
    mapShowing,
    map,
    mapSections = [];

function loadMaps(){

    if(mapsInitiated)
        return;

    console.log("Maps.loadMaps()");

    mapsInitiated = true;
    Sid.js("https://api.mapbox.com/mapbox.js/v2.3.0/mapbox.js",onMapLoaded);


    Sid.css(['https://api.mapbox.com/mapbox.js/v2.3.0/mapbox.css',
        'https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/MarkerCluster.css']
    );

    $( document ).bind( "scene_loaded",onSceneLoaded );
}

function createMap(sections){
    console.log("Maps.map(" + sections.length + ")");
    mapShowing = true;

    scene.visible = false;

    var markers = [];

    L.mapbox.accessToken = 'pk.eyJ1IjoiYmtuaWxsIiwiYSI6Imh4dHNYbXcifQ.dSnZY6_OVlg3ryUCjPloiA';

    map = L.mapbox.map('container', 'bknill.hp29237j', { zoomControl: false });

   var markersCluster = new L.MarkerClusterGroup();

    new L.Control.Zoom({ position: 'bottomleft' }).addTo(map);


    $(sections).each(function(index, section){
        if(section.guideScenes.length > 0){

           var scene;

            //some scenes seem to have a broken lat/lon
            for(var s in section.guideScenes){
                if(section.guideScenes[s].scene.longitude != 0){
                    scene = section.guideScenes[s].scene;
                    break;
                }
            }

                var obj = {};
                    obj.type = "Feature";
                    obj.geometry = {};
                    obj.geometry.type = 'Point';
                    obj.geometry.coordinates = [scene.longitude, scene.latitude];
                    obj.properties = {};
                    obj.properties.title = section.title;

                var cssIcon = L.divIcon({
                    // Specify a class name we can refer to in CSS.
                    className: 'css-icon',
                    html: '<span sectionId="'+section.id+'" sceneId="'+scene.id+'">' + section.title + '</span>',
                    // Set marker width and height
                    iconSize: [100, 50]
                });



                var marker = L.marker([scene.latitude, scene.longitude], {icon: cssIcon});
                marker.sceneId = scene.id;
                marker.sectionId = section.id;
                marker.on('click', markerClick);
               // marker.addTo(map);
                markers.push(marker);
                markersCluster.addLayer(marker);

    }});

    var group = new L.featureGroup(markers);


   map.addLayer(markersCluster);
    map.fitBounds(group.getBounds());

    $('.css-icon span').bind('click',function(){
        var sceneId = $(this).attr('sceneId');
        var sectionId = $(this).attr('sectionId');

        getScene(sceneId);

        //if(guideOpen)
        $('#li_' + sectionId + ' .next').trigger('click');
    })

}

function markerClick(e){
    console.log(this);
    var sceneId = e.target.sceneId;
    var sectionId =  e.target.sectionId;

    getScene(sceneId);

    //if(guideOpen)
    $('#li_' + sectionId + ' .next').trigger('click');


}

function onMapLoaded(e){
   Sid.js("https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/leaflet.markercluster.js",onClustererLoaded);
}

function onClustererLoaded(e){

   /* $(topLevelSections).each(function(index, section){
            if(section.title.toLowerCase().indexOf("map") > -1)
                createMap(topLevelSections);
        })*/

    if(topLevelSections[0].title.toLowerCase().indexOf("map") > -1)
        createMap(topLevelSections);
}

function onSceneLoaded(e){

    if(mapShowing && !firstRun)
        clearMap();
}

function clearMap(){

    console.log('Maps.clearMap()');
    if(map)
     map.remove();
    mapShowing = false;
    scene.visible = true;
}