  var autoInt, autoLength, autoplaying, autoLinks = [];
   
   function autoplay( links ){
	   console.log('autoplay()');

	   if(connected)
	   	return;

	   autoLength = links.length;
	   autoLinks = links;
	   autoInt = 0;
	   autoplaying = true;
	   
	   setTimeout(nextAuto, 3000, autoInt);
   }
   
   function nextAuto( int ){
	   console.log('nextAuto()');
	   
	   if(isUserInteracting || selectedHotspot || !autoplaying || guideOpen || videoPlaying || gameInProgress)
		   return;
	   
	   guideLink( autoLinks[int] );
	   
	   if(autoInt < autoLength - 1)
		   autoInt ++;
	   else
		   autoInt = 0;
   }
   
   function panoLook( pan, tilt, zoom ){
	   console.log('panoLook()');
	   
		var newLatLon = {lat: lat, lon : lon};
		var targetLatLon = panTiltToLatLon( Number(pan), Number(tilt) );
		var newFov = {nFov : camera.fov};
		var targetFov = {nFov : zoom * 0.8};
		var distance = Math.abs(targetLatLon.lon - newLatLon.lon)*10;
		
		if(newLatLon.lon < 0)
			targetLatLon.lon -= 360;
		if(newLatLon.lon > 360)
			targetLatLon.lon += 360;
		
		//set up zoom tween
		var tweenZoom = new TWEEN.Tween( newFov ).to(targetFov, 2000).easing(TWEEN.Easing.Sinusoidal.InOut);
		tweenZoom.onUpdate(function(){
			camera.fov = newFov.nFov;
			camera.updateProjectionMatrix();
			});
		
		tweenZoom.onComplete(function(){
			   console.log('tweenZoom.onComplete()');
			   setTimeout(nextAuto, 3000, autoInt); 
			});

		var zoomStarted = false;
		//tween lon/lat to tween camera
		var tweenLon = new TWEEN.Tween(newLatLon).to(targetLatLon, distance).easing(TWEEN.Easing.Sinusoidal.Out);
		tweenLon.onUpdate(function(){
			lon = newLatLon.lon;
			lat = newLatLon.lat;

			if(lon < newLatLon.lon + 5 && lon > newLatLon.lon - 5 && !zoomStarted) {
					zoomStarted = true;
					tweenZoom.start();
				}
			});
		
		tweenLon.start();

		//reset scene text following highlight change
		$('.sceneText').html( processedGuideSceneText );		
		updateH1Margin();
   }
   
   function hotspotLook( id ){
	   console.log('hotspotLook(' + id + ')');
	   		for(var hs in targetList)
	   			if(targetList[hs].hotspotData.id == id){
	   				var hotspot = targetList[hs];
	   				var call = function(){	setTimeout(nextAuto, 3000, autoInt); };
		    		tweenToSelectedHotspot( hotspot , call ,true, true );
	   		}
	   
   		//reset scene text following highlight change
		$('.sceneText').html( processedGuideSceneText );
		updateH1Margin();
   }