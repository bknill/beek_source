var camera, scene, renderer,box, composer, deviceControls, pauseRenderer;
var materials = [];
var texture_placeholder,
    isUserInteracting = false,
    onMouseDownMouseX = 0,
    onMouseDownMouseY = 0,
    lon = 90,
	lastLon,
    onMouseDownLon = 0,
    lat = 0,
	lastLat,
    onMouseDownLat = 0,
    phi = 0,
    theta = 0,
    target,
	previewTileCounter,
	isMobile,
	fullscreen = false,
	_touchZoomDistanceEnd,
	_touchZoomDistanceStart = 0,
	_touchZoomDistanceEnd = 0,
	tweenRemote,
	width,
	height,
	shakeEvent;

	var data = {
		manualControl 	: false,
		longitude 		: 0,
		latitude		: 0,
		savedLongitude 	: 0,
		savedLatitude	: 0,
		savedX			: 0,
		savedY			: 0,
		winWidth		: $(window).width(),
		winHeight		: $(window).height(),
		viewerWidth		: $(window).width(),
		viewerHeight	: $(window).height(),
		resizeListener	: null,
		direction		: 0,
		tiltFB			: 0,
		tiltLR			: 0,
		isIOS			: (navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false)
	};


	STATE = { NONE: -1, ROTATE: 0, ZOOM: 1, PAN: 2, TOUCH_ROTATE: 3, TOUCH_ZOOM_PAN: 4 };
	//_panStart = new THREE.Vector2(),
	//_panEnd = new THREE.Vector2(),
	_state = STATE.NONE,
	_prevState = STATE.NONE;


var targetList = [];
var projector, mouse = { x: 0, y: 0 };

var iframeWidth, iframeHeight;

var jsonpPrefix = '//gms.beek.co';
var cdnPrefix =  '//cdn.beek.co';

var FACES = "rludfb";
var TILES = ['0_0', '0_1', '1_0', '1_1'];

var tiles, bubbles;

var hasFocus = true;



	function init() {
		console.log('init()');

		var mobile = window.matchMedia("only screen and (max-width: 760px)");
		if(mobile.matches)
			isMobile = true;


		$("body").scrollTop(1);

		$(function() {
			function orientationChange(e) {
				$("body").scrollTop(1);
			}
			$("body").css({ height: "+=300" }).scrollTop(1);
			$(window).bind("orientationchange", orientationChange);
		});



	    document.getElementById("container").addEventListener('mousedown', onDocumentMouseDown, false);
	    document.getElementById("container").addEventListener('mouseup', onDocumentMouseUp, false);
	    document.getElementById("container").addEventListener('mousewheel', onDocumentMouseWheel, false);
	    document.getElementById("container").addEventListener('touchstart', onDocumentTouchStart, false);
	    document.getElementById("container").addEventListener('touchmove', onDocumentTouchMove, false);
	    document.getElementById("container").addEventListener( 'touchend', onDocumentTouchEnd, false );

		$(window).on("resize",onWindowResize);
		//window.addEventListener('shake', shakeEventDidOccur, false);

		createPlayer();
		initBeek();

		showProgress(0.6);

	   // Sid.css(['//fonts.googleapis.com/css?family=Lato:400,300,900']);

	}

	function createPlayer(){
		var container;

		container = document.getElementById('container');

		camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 1024);


		scene = new THREE.Scene();

		target = new THREE.Vector3();

		projector = new THREE.Projector();

		if ( ! Detector.webgl )
			renderer = new THREE.CanvasRenderer();
		else
			renderer = new THREE.WebGLRenderer({antialias: true});

		renderer.setSize(window.innerWidth, window.innerHeight);
		renderer.shadowMapEnabled = true;
		renderer.autoClear = false;

		renderer.render(scene, camera);
		container.appendChild(renderer.domElement);


		if (window.DeviceOrientationEvent && /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {

		 deviceControls = new DeviceOrientationControls( camera );

		 shakeEvent = new Shake({
		 threshold: 5, // optional shake strength threshold
		 timeout: 100 // optional, determines the frequency of event generation
		 });
		 }
		 else
		 data.manualControl = true;

	 }

	function initBeek() {
	console.log('initBeek()');

	    $.getJSON(jsonpPrefix +'/guide/' + guideId + '/jsonp?callback=?', $.proxy(function(data){

	       if(sceneId)
			getScene(sceneId);

	       createGuide(data);

	        historyScenes.push( sceneId );

			showProgress();
		    animate();


		    if(connectId)
		    	connect();


	    }, this));

	    //address changes
	    $.address.state('/');
	    // switching off auto update in order to avoid adding value to the browser history
		 $.address.autoUpdate(false);
		 $.address.value(document.location.pathname+document.location.search);
		 $.address.autoUpdate(true);


	    //handles back function
	    $.address.change(function(event) {
	    	console.log('change address');
	    	var data = event.value.split('s');
	        var id = parseInt( data[1] );

	        var connect = event.value.split('c');
	        var connectId = connect[1];


	        if(currentScene && id)
		        if(currentScene.id != id)
		        	getScene(id);
	    });

	};


    function onWindowResize() {

		console.log("beek.onWindowResize");

		camera.aspect = window.innerWidth / window.innerHeight;
		camera.updateProjectionMatrix();
		renderer.setSize( window.innerWidth, window.innerHeight );
    }

    function detectHotspotClick(){
    	console.log('detectHotspotClick()');
        isUserInteracting = true;

        if(autoplaying){
        	TWEEN.removeAll();
        	autoplaying = false;
        }

        if(selectedHotspot && !viewingHotspot){
        	TWEEN.removeAll();
        	selectedHotspot = null;
        }

    	var vector = new THREE.Vector3( mouse.x, mouse.y, 1 );

    	projector.unprojectVector( vector, camera );
    	//projector.unproject();

    	var ray = new THREE.Raycaster( camera.position, vector.sub( camera.position ).normalize() );

    	var intersects = ray.intersectObjects( targetList, true );

    	if ( intersects.length > 0){
    		hotspotClick(intersects[ 0 ].object);
    	}
    }

    function onDocumentMouseDown(e) {

        e.preventDefault();


      	if(connected && connectMode == MODE.RECEIVE || mapShowing)
      		return;

       if(selectedHotspot)
    	   if(!selectedHotspot.clickable)
    		   return;


    	// update the mouse variable
   		mouse.x =  ( e.clientX / (guideOpen ? window.innerWidth + $('#guide').width() : window.innerWidth) ) * 2 - 1;
    	mouse.y = - ( e.clientY / window.innerHeight ) * 2 + 1;



		data.manualControl = true;

		var x = e.clientX || e.originalEvent.touches[0].clientX;
		var y = e.clientY || e.originalEvent.touches[0].clientY;

		data.savedX = x;
		data.savedY = y;

		data.savedLongitude = data.longitude;
		data.savedLatitude = data.latitude;


    	detectHotspotClick();


    	//if(lightValue < lightsOn)
    	//	setLights(lightsOn);

    	if(awaitingInteraction){
    		$( document ).trigger( "drag" );
    	}

		document.getElementById("container").addEventListener('mousemove', onDocumentMouseMove, false);

    }

    function onDocumentMouseMove(e) {

		if(data.manualControl){
			var x = e.clientX || e.originalEvent.touches[0].clientX;
			var y = e.clientY || e.originalEvent.touches[0].clientY;

			 data.longitude = (data.savedX - x) * 0.2 + data.savedLongitude;
			 data.latitude  = (y - data.savedY) * 0.2 + data.savedLatitude;

		//	lon = (onPointerDownPointerX - event.clientX) * 0.2 + onPointerDownLon;
		//	lat = (event.clientY - onPointerDownPointerY) * 0.2 + onPointerDownLat;


		}
    }

    function onDocumentMouseUp(event) {

		data.manualControl = false;
		document.getElementById("container").removeEventListener('mousemove', onDocumentMouseMove, false);

    }

    function onDocumentMouseWheel(event) {
    	console.log(isIE());
    	 if(!isIE())
    		 setZoom(camera.fov - event.wheelDeltaY * 0.05);

    }


    function setZoom(fov){

        camera.fov = fov;

        if(camera.fov < 30) camera.fov = 30;
        if(camera.fov > 100) camera.fov = 100;

        camera.updateProjectionMatrix();

    }

    function tweenZoom(fov){

		var newFov = {nFov : camera.fov};
		var targetFov = {nFov : fov};


		var tweenZoom = new TWEEN.Tween( newFov ).to( targetFov, 100 ).easing(TWEEN.Easing.Cubic.Out);
		tweenZoom.onUpdate(function(){
			camera.fov = newFov.nFov;
			camera.updateProjectionMatrix();
		}).start();

    }

    function remoteUpdate(rLon, rLat, rFov){

    	if(rLon == lon && rLat == lat && rFov == camera.fov)
    		return;

    		lon = rLon;
    		lat = rLat;
    		setZoom(rFov);

    }

    function onDocumentTouchStart(event) {

        event.preventDefault();

		console.log("touch start");

      	if(connected && connectMode == MODE.RECEIVE)
      		return;

        if (event.touches.length == 1) {
            var e=event.touches[0];

			data.manualControl = true;


			var x = e.clientX || e.originalEvent.touches[0].clientX;
			var y = e.clientY || e.originalEvent.touches[0].clientY;

			data.savedX = x;
			data.savedY = y;

			data.savedLongitude = data.longitude;
			data.savedLatitude  = data.latitude;

            mouse.x = (e.clientX / window.innerWidth) * 2 -1;
            mouse.y = -(e.clientY / window.innerHeight) * 2 + 1;

        	detectHotspotClick();

        }

        if (event.touches.length == 2) {

        	_state = STATE.TOUCH_ZOOM_PAN;
	        var dx = event.touches[ 0 ].pageX - event.touches[ 1 ].pageX;
	        var dy = event.touches[ 0 ].pageY - event.touches[ 1 ].pageY;
	        _touchZoomDistanceEnd = _touchZoomDistanceStart = Math.sqrt( dx * dx + dy * dy );

        }

    }

    function onDocumentTouchMove(event) {

        if (event.touches.length == 1 && data.manualControl) {

			var x = event.clientX || event.touches[0].clientX;
			var y = event.clientY || event.touches[0].clientY;
			data.longitude = (data.savedX - x) * 0.1 + data.savedLongitude;
			data.latitude  = (y - data.savedY) * 0.1 + data.savedLatitude;

        }

        if (event.touches.length == 2 && data.manualControl) {

        	   var dx = event.touches[ 0 ].pageX - event.touches[ 1 ].pageX;
               var dy = event.touches[ 0 ].pageY - event.touches[ 1 ].pageY;
               _touchZoomDistanceEnd = Math.sqrt( dx * dx + dy * dy );

  			var factor = _touchZoomDistanceStart / _touchZoomDistanceEnd;
			_touchZoomDistanceStart = _touchZoomDistanceEnd;
	    	setZoom(camera.fov * factor);

        }

    	if(awaitingInteraction){
    		$( document ).trigger( "drag" );
    	}

    }

    function onDocumentTouchEnd( event ) {

			event.preventDefault();

			_touchZoomDistanceStart = _touchZoomDistanceEnd = 0;

		shakeEvent.start();
		deviceControls.pan = data.longitude;

		lastLat = lat;
		lastLon = lon;

	}

    zoomCamera = function () {

		if ( _state === STATE.TOUCH_ZOOM_PAN ) {

			var factor = _touchZoomDistanceStart / _touchZoomDistanceEnd;
			_touchZoomDistanceStart = _touchZoomDistanceEnd;
			_eye.multiplyScalar( factor );

		} else {

			var factor = 1.0 + ( _zoomEnd.y - _zoomStart.y ) * _this.zoomSpeed;

			if ( factor !== 1.0 && factor > 0.0 ) {

				_eye.multiplyScalar( factor );

				if ( _this.staticMoving ) {

					_zoomStart.copy( _zoomEnd );

				} else {

					_zoomStart.y += ( _zoomEnd.y - _zoomStart.y ) * this.dynamicDampingFactor;

				}

			}

		}

	};


    function animate() {

        requestAnimationFrame(animate);
        update();

    }

    function update() {

		TWEEN.update();

		if(mapShowing)
			return;


		if(deviceControls && !data.manualControl)
			deviceControls.update();
		else
		{

			//moving the camera according to current latitude (vertical movement) and longitude (horizontal movement)
			target.x = 500 * Math.sin(THREE.Math.degToRad(90 - data.latitude)) * Math.cos(THREE.Math.degToRad(data.longitude));
			target.y = 500 * Math.cos(THREE.Math.degToRad(90 - data.latitude));
			target.z = 500 * Math.sin(THREE.Math.degToRad(90 - data.latitude)) * Math.sin(THREE.Math.degToRad(data.longitude));


			camera.lookAt(target);

			if(deviceControls)
				deviceControls.sceneDefaultQuat.copy(camera.quaternion);
		}




    	if(autoplaying && autoLength == 0)
			data.longitude += 0.05;

        if(sceneSounds.length > 0)
        	updateSceneSounds( lon );


		//if(iOSVideo)
		// updateIOSVideo();

        renderer.render(scene, camera);

    }


    function latLonToVector(lat,lon){
        lat = Math.max(-85, Math.min(85, lat));
        phi = THREE.Math.degToRad(90 - lat);
        theta = THREE.Math.degToRad(lon);

        var vector = new THREE.Vector3();
        vector.x = 512 * Math.sin(phi) * Math.cos(theta);
        vector.y = 512 * Math.cos(phi);
        vector.z = 512 * Math.sin(phi) * Math.sin(theta);

    	return vector;
    }

    function vectorToLatLon(position){

    	// another solution posted on SO if this one is unreliable
    	//var lat = Math.atan2( Math.sqrt( target.x*target.x + target.z*target.z) , target.y) ;
    	//var lon = Math.atan2( target.x, target.y);

    	return {lat : Math.acos(position.y / 512), lon  : Math.atan(position.x / position.z) };
    }

    function panTiltToVector(pan,tilt,distance){

        distance = distance/4;
        var pr = THREE.Math.degToRad( pan + 90);
        var tr = THREE.Math.degToRad( tilt );

        var vector = new THREE.Vector3(distance * Math.cos(pr) * Math.cos(tr),distance * Math.sin(tr),distance * Math.sin(pr) * Math.cos(tr));

        return vector;
    }

    function hotspotPanTiltToLatLon(pan,tilt){

    	pan += 90;
    	pan = pan > 0 ? pan : 360 + pan;
    	pan = pan < 360 ? pan : 360 - pan;

    	return { lat : tilt, lon : pan };
    }

    function panTiltToLatLon(pan,tilt){

    	pan -= 90;
    	pan = pan > 0 ? pan : 360 + pan;
    	pan = pan < 360 ? pan : 360 - pan;

    	return { lat : -tilt, lon : pan };
    }


    var isTouch = (('ontouchstart' in window) || (navigator.msMaxTouchPoints > 0));


    function filters(){

    	return;

	    composer = new THREE.EffectComposer( renderer);

    	filmPass = new THREE.ShaderPass( THREE.FilmShader );
		filmPass.uniforms[ "sCount" ].value = 800;
		filmPass.uniforms[ "sIntensity" ].value = 0.9;
		filmPass.uniforms[ "nIntensity" ].value = 0.4;

		composer.addPass( filmPass );

    }


	function launchFullscreen(element) {

		fullscreen = true;

		  if(element.requestFullscreen) {
		    element.requestFullscreen();
		  } else if(element.mozRequestFullScreen) {
		    element.mozRequestFullScreen();
		  } else if(element.webkitRequestFullscreen) {
		    element.webkitRequestFullscreen();
		  } else if(element.msRequestFullscreen) {
		    element.msRequestFullscreen();
		  }


//			if(inIframe() == true)
//			{
//			    $( "#openGuide" ).toggle();
//			    $( "#openSceneInfo" ).toggle();
//			}
		}


	function exitFullscreen() {
		console.log('exitFullscreen()');

		fullscreen = false;
		  if(document.exitFullscreen) {
		    document.exitFullscreen();
		  } else if(document.mozCancelFullScreen) {
		    document.mozCancelFullScreen();
		  } else if(document.webkitExitFullscreen) {
		    document.webkitExitFullscreen();
		  }

			if(inIframe() == true)
			{
			    $( "#openGuide" ).toggle();
			    $( "#openSceneInfo" ).toggle();
			}

		}




	$(window).blur(function(){
		  mainVolume = 0;
		});
		$(window).focus(function(){
			mainVolume = 0.5;
		});


		// requestAnim shim layer by Paul Irish
		window.requestAnimationFrame = function(){
		    return (
		        window.requestAnimationFrame       ||
		        window.webkitRequestAnimationFrame ||
		        window.mozRequestAnimationFrame    ||
		        window.oRequestAnimationFrame      ||
		        window.msRequestAnimationFrame     ||
		        function(/* function */ callback){
		            window.setTimeout(callback, 1000 / 60);
		        }
		    );
		}();

		function dynamicSort(property) {
		    var sortOrder = 1;
		    if(property[0] === "-") {
		        sortOrder = -1;
		        property = property.substr(1);
		    }
		    return function (a,b) {
		        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
		        return result * sortOrder;
		    };
		}


/*		function orientation(){
			if(window.innerHeight > window.innerWidth)
				return 'portrait';

			return 'landscape';
		}*/

		function inIframe() {
		    try {
		        return window.self !== window.top;
		    } catch (e) {

		    	iframeWidth = window.frameElement.offsetWidth,
		    	iframeHeight = window.frameElement.offsetHeight;

		        return true;
		    }
		}

		function showLoader(){
		console.log("Beek.showLoader()");
		$( "body" ).append(" <div class='spinner'> <div class='double-bounce1'></div> <div class='double-bounce2'></div> </div>");

		}

		function hideLoader(){
			console.log("Beek.hideLoader()");
			$( ".spinner" ).remove();
		}

	function shakeEventDidOccur () {

		deviceControls.pan = data.longitude;
		data.manualControl = false;

	}

	function isIOS(){
		return /iPhone|iPod/.test( navigator.userAgent );
	}

function nearestPow2( aSize ){
	return Math.pow( 2, Math.round( Math.log( aSize ) / Math.log( 2 ) ) );
}