var guideSections = [],
	childSections = [],
	topLevelSections = [],
	guideSection,
	guideSectionOrder = [],
	guideScenes = [],
	guideSceneOrder = [],
	guideScene,
	guideData,
	minimalSpace = false,
	$dragging,
	guideOpen,
	infoOpen,
	guideOrigin,
	sectionX,
	processedGuideSceneText,
	headerWidth;

//this organises all the guideData
function createGuide( data ){

	guideData = data;

	//add sections
	for(var section in guideData.guideSections){
		guideSections.push(guideData.guideSections[section]);
		//add scenes
		for(var scene in guideData.guideSections[section].guideScenes)
			guideScenes.push(guideData.guideSections[section].guideScenes[scene]);


		guideData.guideSections[section].guideScenes.sort(dynamicSort("order"));
	};

	guideSections.sort(dynamicSort("order"));

	//find child sections and check for map
	for(var cs in guideSections) {

		if (guideSections[cs].parent_section_id) {

			for (var s in guideSections)
				if (guideSections[s].id == guideSections[cs].parent_section_id) {
					if (!guideSections[s].childSections)
						guideSections[s].childSections = [];

					guideSections[s].childSections.push(guideSections[cs]);
					guideSections[cs].parent = guideSections[s];
				}
		}
		else
			topLevelSections.push(guideSections[cs]);

		/*			if(guideSections[cs].title.toLowerCase().indexOf('map') > -1){
		 mapSections.push(guideSections[cs]);
		 loadMaps();
		 }*/
	}

	//set child sections and scenes order
	for(var s in guideSections){
		if(guideSections[s].childSections)
			guideSections[s].childSections.sort(dynamicSort("order"));
		if(guideSections[s].guideScenes){
			guideSections[s].guideScenes.sort(dynamicSort("order"));
		}


	}


	//create the orders for the next button
	for(var s in guideSections){
		if(!guideSections[s].parent_section_id){

			guideSectionOrder.push(guideSections[s]);
			for (var sc in guideSections[s].guideScenes)
				guideSceneOrder.push(guideSections[s].guideScenes[sc]);

			for (var cs in guideSections[s].childSections){
				guideSectionOrder.push(guideSections[s].childSections[cs]);
				for (var sc in guideSections[s].childSections[cs].guideScenes)
					guideSceneOrder.push(guideSections[s].childSections[cs].guideScenes[sc]);

				for (var gcs in guideSections[s].childSections[cs].childSections){
					guideSectionOrder.push(guideSections[s].childSections[cs].childSections[gcs]);
					for (var sc in guideSections[s].childSections[cs].childSections[gcs].guideScenes)
						guideSceneOrder.push(guideSections[s].childSections[cs].childSections[gcs].guideScenes[sc]);

					for (var ggcs in guideSections[s].childSections[cs].childSections[gcs].childSections){
						guideSectionOrder.push(guideSections[s].childSections[cs].childSections[gcs].childSections[ggcs]);
						for (var sc in guideSections[s].childSections[cs].childSections[gcs].childSections[ggcs].guideScenes)
							guideSceneOrder.push(guideSections[s].childSections[cs].childSections[gcs].childSections[ggcs].guideScenes[sc]);

					}
				}

			}
		}
	}
	if(guideData.gameTasks.length)
		setupLRS(guideData.gameTasks);

	return;

	if(!sceneId)
		getScene(guideSections[0].guideScenes[0].sceneId);

	//check to see if video player is required

	/*	if(guideScenes[0].transitionVideo)
		firstSceneVideo = true;*/



	//set default section
	guideSection = guideSections[0];

	createPlayer();

	animate();

	if(sceneId)
		getScene(sceneId);
	//create3DGuide(topLevelSections.length < 2 ? guideSection.guideScenes : topLevelSections);
	//addButtons();
	//updateScrollText("downText",guideData.title);
}



//set the guideScene variable
function updateGuideScene(id){
	console.log('updateGuideScene('+id+')' + firstRun);

	/*		for(var s in guideScenes)
	 if(guideScenes[s].sceneId == id){
	 guideScene = guideScenes[s];
	 processGuideSceneHotspots();

	 }*/

	//if there's a video load it
	if(guideScene){
		if(guideScene.transitionVideo)
			loadVideo(guideScene.transitionVideo);

		//if there's a voiceover load it
		if(guideScene.voiceover)
			loadVoiceover(guideScene.voiceover);
	}


}


function processGuideSceneHotspots(){

	console.log('processGuideSceneHotspots()');

	for (ix in guideScene.photos)
		photos.push(guideScene.photos[ix]);

	for (ix in guideScene.posters)
		posters.push(guideScene.posters[ix]);

	for (ix in guideScene.sounds)
		sounds.push(guideScene.sounds[ix]);
}




function cleanText( text, tags ){
	var $desc = (text)

		//filter out Flash rubbish code
		.replace(/<p.+?>/ig, tags ? "<p> " : "")
		.replace(/<L.+?>/ig,tags ? "<li>": "")
		.replace(/<[b|u]>/ig, " ")
		.replace(/<\/[b|u]>/ig, " ")
		.replace(/<TEXTFORMAT.+?>/g, " ")
		.replace(/<\/TEXTFORMAT>/g, " ")
		.replace(/<FONT.+?>/g, " ")
		.replace(/<\/FONT>/g, " ")
		.replace(/<a.+?event:(.+?)" TARGET.*?>(.*?)<\/a>/ig,tags ? "<a href=\"javascript:guideLinkClicked(\'$1\')\"  >$2</a>" : "");


	return $desc;
}





function sceneInGuide(sceneId){

	for(var i in guideScenes)
		if(guideScenes[i].scene.id == sceneId)
			return true;

	return false;
}

function getGuideScene( sceneId ){

	for(var i in guideScenes)
		if(guideScenes[i].scene.id == sceneId )
			return guideScenes[i];

	return null;

}

function getGuideSection( sectionId ){

	for(var i in guideSections)
		if(guideSections[i].id == sectionId )
			return guideSections[i];

	return null;
}






function getOrientation(){

	if(window.innerHeight > window.innerWidth)
		return 'portrait'
	else
		return 'landscape'
}



