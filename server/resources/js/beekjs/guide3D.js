/**
 * Created by Ben on 15/10/2016.
 */

var menu,menuItemsList = [], currentMenuItems,currentText, answersList = [],textbox, menuActive = false,  draggingMenu = false, isScrolling = false;
var circleRadius = 180,
    startAngle = 180,
    startRadians,
    incrementAngle = 17,
    mpi = Math.PI/180,
    incrementRadians = incrementAngle * mpi,
    answerBoxStart,
    answerBoxRadians,
    targetCircle,
    draggingY,
    vFOV = 100 * Math.PI / 180,
    menuWidth;

function create3DGuide(menuItems){

    console.log('create3DGuide',menuItems);

    if(menu != null && menuItemsList.length > 0){
        menuItemsList.forEach(function (item) {
            menu.remove(item);
            removeFromArray(targetList,item);
        });

        menuItemsList = [];
    }
    else{

        menu = new THREE.Object3D();
        camera.eulerOrder = "YXZ";
        menu.offsetX =  menu.rotation.x = -Math.PI * 0.8;
        menu.visible = false;
        camera.add(menu);
    }

    currentMenuItems = menuItems;

    if(!textbox)
        startRadians = startAngle * mpi;
    else
        startRadians = (startAngle - 220) * mpi;

   var  h = 2 * Math.tan( vFOV / 2 ) * circleRadius;
   var a = window.innerWidth > window.innerHeight ? window.innerWidth/ window.innerHeight : window.innerHeight/ window.innerWidth;
   menuWidth = (h * a)/2;

    if(!gameInProgress)
    menuItems.forEach(function(item,index){

        var menuItem = createMenuItem(item.title,menuWidth);
            menuItem.menuData = item;
            menuItem.position.y =  Math.sin(startRadians) * circleRadius;
            menuItem.position.z = -0 + (Math.cos(startRadians) * circleRadius);
            menuItem.position.x =  0;
            menuItem.rotation.x = - index * incrementRadians;

        startRadians += incrementRadians;
        menuItemsList.push(menuItem);

        targetList.push(menuItem);
        menu.add(menuItem);

    });


    if(isMobile)addUI();

}

function openMenu(){
    console.log('openMenu()',menu);
    menuActive = true;
   updateShader(-0.6,0);
    menu.visible = true;
   targetList.forEach(function(item){
        if(!item.menuData && !item.choiceData && typeof item.disable == 'function')
            item.disable();
    });

}

function closeMenu(){
    console.log('closeMenu()');
    menuActive = false;


     updateShader(0, 0);

    menu.visible = false;

    //little shimmy to offset menu
    draggingMenu = true;
    menu.rotation.x = menu.offsetX;
    draggingMenu = false;

targetList.forEach(function(item){
    if(!item.menuData && !item.choiceData && typeof item.enable == 'function')
         item.enable();
    });

}

function createMenuItem(label,width){

    if(label.length > 30)
        label = label.substr(0,30) + "..";

    var labelMesh = new textSprite (label, 90 , showHowColors.purple, true );
    labelMesh.scale.set(0.2,0.2,0.2);
    labelMesh.position.set(0,0,2);

    var height =40;


    var hsGeom = new THREE.PlaneBufferGeometry( width || (labelMesh.width + 20) * 0.2 , height);
       // hsGeom.translate( width/2, height/2, 0 );

    var hsMaterial = new THREE.MeshBasicMaterial({color: showHowColors.grey});
        hsMaterial.side = THREE.DoubleSide;
    var background = new THREE.Mesh( hsGeom, hsMaterial );

    var menuItem = new THREE.Object3D();
    menuItem.add(labelMesh);
    menuItem.add(background);
    menuItem.background = background;
    menuItem.frustumCulled = true;

    menuItem.translate(width/2, height/2, 0);

    menuItem.activated = false;

    menuItem.activate = function activate(stop){

        if(!menuItem.activated)
            menuItem.activated = true;
        else
         return;

        if(!menuItem.bar){
            var box = new THREE.PlaneBufferGeometry( 1, height);
            var mat = new THREE.MeshBasicMaterial( { color: showHowColors.lightGrey } );
            var bar = new THREE.Mesh(box, mat );
            bar.position.set(0,0,1);
            menuItem.add(bar);
            menuItem.bar = bar;
        }

        menuItem.barInProgress = true;
        var b = {width:1};
        var t = {width: width || (labelMesh.width + 20) * 0.2 };


        if(menuItem.selected)
        {
            b = {width:menuWidth};
            t = {width:0.00000001};
        }

        menuItem.tween = new TWEEN.Tween( b ).to( t, mouseControls ? 2500 : 2500 ).onUpdate(function(){

            if(selectedHotspot == menuItem || mouseControls)
                menuItem.bar.scale.set(b.width, 1, 1 );
            else{
                menuItem.deactivate();
            }


        }).onComplete(function(){

            menuItem.barInProgress = false;

            if(stop)
                return;
            else if(menuItem.hotspotData)
                hotspotClick(menuItem);
            else if(menuItem.menuData)
                menuItemHandler(menuItem);
            else if(menuItem.choiceData)
                answerItemHandler(menuItem);

        }).start();



    };

    menuItem.deactivate = function deactivate(){

        if(menuItem.activated)
            menuItem.activated = false;
        else
            return;


        if(menuItem.tween != null){
            TWEEN.remove(menuItem.tween);
            menuItem.tween = null;
        }

        if(isMobile && menuItem.selected){

            if(menuItem.barInProgress)
                menuItem.bar.scale.set(menuWidth, 1, 1 );

            return;
        }

        if(menuItem.bar != null){
             menuItem.remove(menuItem.bar);
             menuItem.bar = null;
         }

    };


    return menuItem;
}



function createFontAwesomeIcon(parent,icon,x,y,colour){

    var existing = parent.getObjectByName("fontAwesomeIcon");
    if(existing) parent.remove(existing);


    if(icon){
        var fontAwesomeIcon = new textSprite (icon, 90 , colour | '#fff', false ,"FontAwesome" );
        fontAwesomeIcon.scale.set(0.2,0.2,0.2);
        fontAwesomeIcon.name = "fontAwesomeIcon";

        fontAwesomeIcon.position.x = x | parent.width/2 - (fontAwesomeIcon.width * 0.2) - 2;
        console.log(fontAwesomeIcon.position.x,parent.width,fontAwesomeIcon.width);
        fontAwesomeIcon.position.y = y | 1;
        fontAwesomeIcon.position.z = 2;
        parent.add(fontAwesomeIcon);
        parent.icon = fontAwesomeIcon;
    }
}



function onScrollMouseMove(event){
    console.log('onScrollMouseMove()',draggingY,draggingMenu,isScrolling);

    var y = event.pageY || event.touches[0].pageY;

    if(Math.abs(y - draggingY) > 20){
        isScrolling = true;
    }
    if (draggingY)
        menu.rotation.x += ((draggingY - y)/renderer.domElement.height)/(Math.PI*2);
}

function onScrollMouseUp(event){
    console.log("onScrollMouseUp()");
    isScrolling = false;
    document.removeEventListener('mousemove', onScrollMouseMove, false);
    document.removeEventListener('touchmove', onScrollMouseMove, false);
    document.removeEventListener('mouseup', onScrollMouseUp, false);
    document.removeEventListener('touchend', onScrollMouseUp, false);
}

function onMenuItemMouseDown(item){
    console.log("onMenuItemMouseDown()");
    addMouseScrolling(item);
}

var oPreviousCoords = {
    'x': 0,
    'y': 0
}

function addMouseScrolling(item){

    if(!menu.visible)
        return;

   console.log("addMouseScrolling()");
    camera.originX = camera.rotation.x;
    menu.originX = menu.rotation.x;

    isScrolling = false;
    draggingMenu = true;

    if(isMobile)
    waitForMovement(1,function(){
        if(!isScrolling)draggingMenu = false;
    })

    document.addEventListener('mousemove', onScrollMouseMove, false);
    document.addEventListener('mouseup', onScrollMouseUp, false);
    document.addEventListener('touchmove', onScrollMouseMove, false);
    document.addEventListener('touchend', onScrollMouseUp, false);

    setTimeout(function(){

        if(!isScrolling){

            setTimeout(function() {

                if (item.menuData)
                    menuItemHandler(item);
                else if (item.choiceData)
                    answerItemHandler(item);

                if(isMobile)
                waitForMovement(0.5,function(){
                    draggingMenu = false;
                })
            },200);

            document.removeEventListener('mousemove', onScrollMouseMove, false);
            document.removeEventListener('mouseup', onScrollMouseUp, false);
            document.removeEventListener('touchmove', onScrollMouseMove, false);
            document.removeEventListener('touchend', onScrollMouseUp, false);

        }
        else
            openMenu();

    },200);
}

function menuItemHandler(item){
    console.log('menuItemHandler()');

    var menuItem = item.menuData || item.parent.menuData;

    if(menuItem.sceneId){

        item.activate();
        selectedHotspot = null;
        getScene(menuItem.sceneId);

        menuItemsList.forEach(function(item){
            if(item.menuData.sceneId != menuItem.sceneId)
                item.deactivate();
        })


    }

    else if(menuItem.guideScenes || menuItem.childSections)
    {
        var menuItems = [];

        if(!menuItem.topLevel){

            var backButton = {};
                backButton.title = "< Back";
            if(menuItem.parent){
                backButton.guideScenes = menuItem.parent.guideScenes;
                backButton.childSections = menuItem.parent.childSections;
                backButton.parent = menuItem.parent.parent;
            }else{
                backButton.childSections = topLevelSections;
                backButton.topLevel = true;
            }

            menuItems.push(backButton);
        }

        if(menuItem.guideScenes)
        if(menuItem.guideScenes.length > 0){
            menuItem.guideScenes.forEach(function (item) {
                menuItems.push(item);
            });
            //getScene(menuItem.guideScenes[0].scene.id);
        }

        if(menuItem.childSections)
             menuItem.childSections.forEach(function (item) {
             menuItems.push(item);
        });

        menuItemsList.forEach(function(menuItem){
            if(item != menuItem){

                var position = menuItem.position;
                position.opacity = 1;
                var target = { x : -menuWidth, opacity:0 };
                var tween = new TWEEN.Tween( position ).to( target, 300 ).onUpdate(function(){
                    menuItem.position.x = position.x;
                }).onComplete(function(){
                    menuItem.visible = false;
                }).start();

            }
        });
       setTimeout(function(){create3DGuide(menuItems)},500);
    }
}


function menuUpdate(){

      if(camera.rotation.x < 0 && !draggingMenu)
          menu.rotation.x = menu.offsetX -(camera.rotation.x * Math.PI);

        if(camera.rotation.x < -0.4 && !menuActive)
             openMenu();
        else if (camera.rotation.x > -0.4 && menuActive)
              closeMenu();
}

function updateTextBox(text){
    console.log('updateTextBox()');

    if(text){

        menu.remove(textbox);
        answersList = [];

        $('#container').append('<div id ="htmlTextBoxHolder">'+text+'</div>');

        $('#htmlTextBoxHolder').css({
            'width': menuWidth * 5,
            'position': 'absolute',
            'top': window.innerHeight,
            'padding': '100px 100px',
            // 'background-color':'#ffffff',
            'background-color':'rgba(167, 169,172, 1)',
            'color': showHowColors.purple,
            'font-size': '90px',
            'visibility' : 'visible'
        });

        if(text.length > 1)
            html2canvas($("#htmlTextBoxHolder"), {
                onrendered: function(canvas) {
                    positionTextbox(canvas);
                    $('#htmlTextBoxHolder').remove();
                },

                width: $('#htmlTextBoxHolder').width() + 200,
                height: $('#htmlTextBoxHolder').height() + 200
            });


    }
    else{
        positionTextbox();

    }

    function positionTextbox(canvas){

        if(canvas){
            var texture = new THREE.Texture(canvas);
            texture.needsUpdate = true;

            textbox = new THREE.Mesh(
                new THREE.BoxGeometry(canvas.width, canvas.height,1),
                new THREE.MeshBasicMaterial({
                    map: texture,
                    transparent: true
                })
            );
            var scalar = vrMode ? 0.1 : 0.2;
            textbox.height = canvas.height * scalar;
            textbox.width = canvas.width * scalar;
            textbox.scale.set(scalar,scalar,scalar);
            textbox.extendable = false;
            textbox.name = 'textbox';
            targetList.push(textbox);
            menu.add(textbox);
        }

        if(textbox){

            startRadians = startAngle * mpi;

            textbox.position.y =  Math.sin(startRadians) * circleRadius;
            textbox.position.z =  Math.cos(-startRadians) * circleRadius;
            textbox.position.x =  0;
            textbox.rotation.x = Math.PI - startRadians;
        }

        if(gameTask){
            if(gameTask.choices){
                answerBoxes(gameTask.choices);
                textbox.extendable = true;
            }
        }
    }

}

function createButton(label){
    var submit = {};
    submit.title = label;
    submit.submit = true;
    return submit;
}

function answerBoxes(choices){

    answersList = [];
    answerBoxStart = startAngle - 210;

    if(choices){

        if(choices.length > 1 && choices.length == gameTask.choices.length)
            choices.push(createButton("Submit"));

        answerBoxRadians = answerBoxStart * mpi;

        for(var i = 0; i < choices.length;i++)
            addAnswerBox(choices[i],i);
    }

    taskLocked = true;
}


function addAnswerBox(choice,index){

    var answer = createMenuItem(choice.title,menuWidth);
    answer.choiceData = choice;

    answer.visible = true;
    answer.width = textbox.width - 50;

    answer.position.x =  0;
    answer.position.y =  Math.sin(answerBoxRadians) * circleRadius;
    answer.position.z = - Math.cos(answerBoxRadians) * circleRadius;

    var scale =  1;
    answer.scale.set(scale,scale,scale);

    answer.rotation.x = answerBoxRadians;
    menu.add(answer);
    answersList.push(answer);
    targetList.push(answer);
    answerBoxRadians -= incrementRadians;

    menu.add(answer);
    answersList.push(answer);
    targetList.push(answer);

    if(!choice.submit && gameTask.choices.length > 1)
        createFontAwesomeIcon(answer,fontAwesome["fa-circle-o"]);

    answer.select = function(){

        answer.choiceData.selected = true;
        answer.selected = true;
        answer.icon.visible = true;

        if(!answer.choiceData.submit)
            createFontAwesomeIcon(answer,fontAwesome["fa-check-circle-o"]);

    };
    answer.deselect = function(){

        answer.choiceData.selected = false;
        answer.selected = false;
        answer.icon.visible = false;

        if(!answer.choiceData.submit)
         createFontAwesomeIcon(answer,fontAwesome["fa-circle-o"]);

    }

    return answer;
}

function onAnswerItemMouseDown(item){
    console.log('onAnswerItemMouseDown()');
    addMouseScrolling(item);
}

function answerItemHandler(item){
    console.log('answerItemHandler()');

   if(gameTask.choices)
    if(item.choiceData.submit || gameTask.choices.length == 1)
        submitButton(item);
    else if(!item.choiceData.selected){
         item.activate(true);
        item.select();
    }
    else{
        item.deselect();
       item.deactivate(true);

    }




}

function submitButton(button){
    console.log('submitButton()');

    if(multichoiceSubmit()){
        console.log('returned true');

        createFontAwesomeIcon(button,fontAwesome["fa-check"]);

        setTimeout(function(){
            taskComplete();

        },1000);

    }
    else{
        console.log('returned false');
        createFontAwesomeIcon(button,fontAwesome["fa-times"]);

        setTimeout(function(){
            createFontAwesomeIcon(button);
          answersList.forEach(function(item){
                item.deselect();
            })
        },700);

    }
};


function deselectItems(){
    targetList.forEach(function(item){
        if(item.bar && !item.selected)
            item.deactivate();
    });
}


function mouseMode(){
    mouseControls = true;
   // camera.remove(targetCircle);
}

function addUI(){

/*    if(!mouseControls)
        return;*/

    var radius   = 5,
        segments = 16,
        material = new THREE.LineBasicMaterial( { color: 0xffffff } ),
        geometry = new THREE.CircleGeometry( radius, segments );

// Remove center vertex
    geometry.vertices.shift();
    targetCircle =  new THREE.Line( geometry, material );
    targetCircle.name = "targetCircle";

    camera.add( targetCircle );
    targetCircle.position.set(0,0,-200);

}

function onVRModeChangeUI(mode){
    console.log(mode);
    if(mode == 3) {
        vrMode = true;
        camera.fov /= 2;
    }

    if(mode == 1){
        vrMode = false;
        camera.fov *= 2;
    }

}

