	function board(board){

			signBoard = new THREE.Object3D();
			signBoard.name = 'signBoard';
			var width = 0;

			var count = board.boardSigns.length;
			var height = count * 30;
			
			for(var i in board.boardSigns){
				
				var boardSign = textSprite ( board.boardSigns[i].scene.title, 50, "#000000" );
				boardSign.scale.normalize().multiplyScalar(0.25);
				boardSign.position.y =  (i * 15) - (count * 5) - 2.5;
				boardSign.position.x = -5;
				boardSign.name = board.boardSigns[i].scene.title;
				boardSign.hotspotData = board.boardSigns[i];
				
				boardSign.position.z = 1;
				
				signBoard.add( boardSign );
				
				var newWidth = boardSign.geometry.parameters.width/2  + 25;
				if(newWidth > width)
					width = newWidth;
			}
			
			for(var i in board.boardSigns){
				var triangleGeom = new THREE.Geometry();
				var v1 = new THREE.Vector3(0,0,0);var v2 = new THREE.Vector3(30,0,0);var v3 = new THREE.Vector3(30,30,0);
				triangleGeom.vertices.push( v1 );triangleGeom.vertices.push( v2 );triangleGeom.vertices.push( v3 );

				triangleGeom.faces.push( new THREE.Face3( 0, 1, 2 ) );
				
				var triangle = new THREE.Mesh( triangleGeom, new THREE.MeshBasicMaterial({color : 0x000000}) );
				triangle.position.y =  (i * 15) - (count * 5) - 1.5;
				triangle.position.x = (width * 0.175) - 7.5;
				triangle.position.z = 1;
				triangle.scale.normalize().multiplyScalar(0.3);
				
				switch(board.boardSigns[i].direction){
				
					case 0://right
					triangle.rotation.z = 0.75;
					triangle.position.y -= 2.5;
					break;
					
					case 1://left
					triangle.rotation.z = -2.35;
					triangle.position.y += 2.5;
					triangle.position.x += 2.5;
					break;
					
					case 2://up
					triangle.rotation.z = 2.35;
					triangle.position.y -= 2.5;
					triangle.position.x += 5;
					break;
					
					case 3://down
					triangle.rotation.z = -0.75;
					//triangle.position.y += 2.5;
					triangle.position.x -= 2.5;
					break;
				}

				triangle.position.z = 1;
				
				signBoard.add(triangle);
			
			}
			
			var cubeGeom = new THREE.BoxGeometry( width *0.35, count * 18, 0.1);
			//var mirrorCubeCamera = new THREE.CubeCamera( 1, 1024 , 100 );

			var material = Detector.webgl ? new THREE.MeshPhongMaterial({color: 0xFFFFFF}) :  new THREE.MeshBasicMaterial({color: 0xFFFFFF});
		    material.side = THREE.DoubleSide;
			boardBg = new THREE.Mesh( cubeGeom, material );


			signBoard.add( boardBg );	
			//signBoard.add( mirrorCubeCamera );
			
			signBoard.hotspotData = board;
			signBoard.clickable = true;
			signBoard.frustumCulled = true;
			signBoard.position.copy( panTiltToVector( board.pan, board.tilt, board.distance ) );
			signBoard.lookAt( camera.position );
			signBoard.rotation.x = isNaN(parseInt(board.rotationX)) ? 0 : -board.rotationX;
			signBoard.rotation.y = isNaN(parseInt(board.rotationY)) ? 0 : -board.rotationY + Math.PI;
			signBoard.rotation.z = isNaN(parseInt(board.rotationZ)) ? 0 : -board.rotationZ;

			
			if(signBoard.rotation.x == signBoard.rotation.y == signBoard.rotation.z == 0)
				signBoard.lookAt( camera.position );

			scene.add( signBoard );
			//mirrorCubeCamera.position = signBoard.position;
			//mirrorCubeCamera.updateCubeMap( renderer, scene );
	    	targetList.push( signBoard );
	    	

		
	}
	
	