

function bubble(bubble){

		console.log('bubble');
		
		if(!sceneInGuide( bubble.targetSceneId ) )
			return;

		console.log('bubble added');
			var guideScene = getGuideScene(bubble.targetSceneId);
		
        var hsText = textSprite (bubble.title == null ? guideScene.titleMerged : bubble.title, 30 , "#000", true );
	    var hsGeom = new THREE.PlaneBufferGeometry( hsText.geometry.parameters.width*1.3, hsText.geometry.parameters.height*1.1);

	    var hsMaterial = new THREE.MeshBasicMaterial({color: 0xFFFFFF});
	    
	    hsMaterial.side = THREE.DoubleSide;
		var hsCube = new THREE.Mesh( hsGeom, hsMaterial );
		
		var triangleGeom = new THREE.Geometry();
		var v1 = new THREE.Vector3(0,0,0);var v2 = new THREE.Vector3(30,0,0);var v3 = new THREE.Vector3(30,30,0);
		triangleGeom.vertices.push( v1 );triangleGeom.vertices.push( v2 );triangleGeom.vertices.push( v3 );

		triangleGeom.faces.push( new THREE.Face3( 0, 1, 2 ) );
		triangleGeom.computeFaceNormals();
		
		var triangle = new THREE.Mesh( triangleGeom, hsMaterial );
		triangle.rotation.z = -0.75;
		triangle.position.y = -hsText.geometry.parameters.height*0.4;
		triangle.position.x -= 15;

		hsText.position.z = 20;
		
		hsBubble = new THREE.Object3D();
		hsBubble.position.copy( panTiltToVector( bubble.pan, bubble.tilt, bubble.distance ) );	
		hsBubble.lookAt( camera.position );
		hsBubble.scale.normalize().multiplyScalar(0.4);
		hsBubble.hotspotData = bubble;
		hsBubble.rotateY = Math.PI;
		hsBubble.frustumCulled = true;
		
		hsBubble.add( hsCube );	
		hsBubble.add( hsText );
		hsBubble.add( triangle );
		scene.add(hsBubble);
    	
    	targetList.push( hsBubble );
		
	}