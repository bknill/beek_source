var hs_bubble,
	refractSphereCamera,
	selectedHotspot,
	viewingHotspot,
	hotspotLat,
	hotspotLon,
	closeButton = false,
	group,
	photos,
	posters,
	sounds,
	hiddenHotspots = [],
	hotspots = [],
	hotspotScaleFactor = 0.4,
	hotspotTweenTime = 1500,
	hotspotRenderList = [],
	renderInProgress = false;



function createHotspot(data,replaceHotspot){

	console.log('createHotspot',data);

	if(!data.config)
		return;

	var hotspot = replaceHotspot || new THREE.Object3D();

	hotspot.data = data;
	hotspot.name = data.id;
	hotspot.active = hotspot.data.loadSceneId != null;

	//activate any inactive hotspots that are required for game tasks
	if(gameInProgress)
		gameTasks.some(function(task){
			if(task.hotspotsToFind){
				if(task.hotspotsToFind.some(function(hstofind){
						if(hstofind.id == data.id){
							hotspot.active = true;
							return true;
						}

					}))
					return true;
			}
		});

	if(typeof hotspot.data.config === 'string')
		hotspot.data.config = JSON.parse(data.config);

	if(hotspot.data.config.position)
		hotspot.position.copy(hotspot.data.config.position);
	else
		return;

	if(hotspot.data.config.rotation)
		hotspot.rotation.copy(hotspot.data.config.rotation);
	else
		hotspot.lookAt(camera.position);

	scene.add(hotspot);
	hotspots.push(hotspot);
	targetList.push(hotspot);
	hotspot.updateMatrixWorld();

	if(hotspot.data.config.scale)
		hotspot.scale.set(hotspot.data.config.scale,hotspot.data.config.scale,hotspot.data.config.scale);

	var _opacity = 1;

	hotspot.setOpacity = function(opacity){

		_opacity = opacity;

		if(hotspot.material)
			hotspot.material.opacity =  opacity;

		hotspot.children.forEach(function(child){
			if(child.material && child.name !== 'box'){
				child.material.transparent = true;
				child.material.opacity = opacity;
			}
		})
	};

	hotspot.getOpacity = function(){
		return _opacity;
	}



	if(hotspot.data.behaviour)
		addBehaviours(hotspot);

	if(hotspot.data.button)
		createHotspotButton(hotspot);

	if(data.media){

		hotspot.active = true;

		if(typeof data.media === 'string')
			data.media = JSON.parse(data.media);

		if(data.media.shape)
			createShapeHotspot(hotspot, data);

		if(data.media.files)
			data.media.files.forEach(function(file){
				if(file.type == '.jpeg' || file.type == '.jpg' || file.type == '.png')
					createImageHotspot(hotspot, file);
			});
	}
	if(data.description){

		hotspot.active = true;

		createHotspotDescription(hotspot);
	}

	if(hotspot.data.config.opacity)
		hotspot.setOpacity(hotspot.data.config.opacity);

}






function hotspotHidden( hotspot, type ){

	if(hotspotsToHide.indexOf( hotspot.id ) < 0 )
		return false;
	else{
		hotspot.type = type;
		hiddenHotspots.push( hotspot );
	}

	return true;
}

function showHiddenHotspot( hotspot ){

	if( hotspot.type == 'bubble')
		bubble(hotspot);

	if( hotspot.type == 'photo')
		createPhoto(hotspot);

	if( hotspot.type == 'poster')
		createPoster(hotspot);

	if( hotspot.type == 'sound')
		createSound(hotspot);
}

function activateForTask(task){
	task.hotspotsToFind.forEach(function(hotspotToFind){
		hotspots.some(function(hotspot){
			if(hotspotToFind.id == hotspot.data.id){
				hotspot.active = true;
				return true;
			}
		})
	})

}

function addBehaviours(hotspot){
	console.log('addBehaviours');

	if(typeof hotspot.data.behaviour === 'string')
		hotspot.behaviours = JSON.parse(hotspot.data.behaviour);
	else if(typeof hotspot.data.behaviour === 'object')
		hotspot.behaviours = hotspot.data.behaviour;


	hotspot.behaviours.values.forEach(function(behaviour,index){

		behaviour.origin = {
			pX : index == 0 ? hotspot.position.x : hotspot.behaviours.values[index-1].position.x,
			pY : index == 0 ? hotspot.position.y : hotspot.behaviours.values[index-1].position.y,
			pZ : index == 0 ? hotspot.position.z : hotspot.behaviours.values[index-1].position.z,
			scale :index == 0 ? hotspot.scale.x : hotspot.behaviours.values[index-1].scale,
			opacity : index == 0 ? hotspot.getOpacity() : hotspot.behaviours.values[index-1].opacity
		};

		behaviour.target = {
			pX : behaviour.position.x,
			pY : behaviour.position.y,
			pZ : behaviour.position.z,
			scale : behaviour.scale,
			opacity : behaviour.opacity
		};

		behaviour.offsetTime = index == 0 ? 0 : behaviour.time - hotspot.behaviours.values[index-1].time;

		behaviour.tween = new TWEEN.Tween(behaviour.origin).to(behaviour.target,behaviour.offsetTime * 1000)
			.onStart(function(){
				hotspot.currentTween = behaviour.tween;
			})
			.onUpdate(function(){

				hotspot.position.set(behaviour.origin.pX,behaviour.origin.pY,behaviour.origin.pZ);
				hotspot.lookAt(camera.position);
				hotspot.scale.set(behaviour.origin.scale,behaviour.origin.scale,behaviour.origin.scale);
				hotspot.setOpacity(behaviour.origin.opacity);

			}).onComplete(function(){

				if(!hotspot.behaviours)
					return;

				behaviour.origin.pX = index != 0 ? hotspot.behaviours.values[index-1].position.x : hotspot.data.config.position.x;
				behaviour.origin.pY = index != 0 ? hotspot.behaviours.values[index-1].position.y : hotspot.data.config.position.y;
				behaviour.origin.pZ = index != 0 ? hotspot.behaviours.values[index-1].position.z : hotspot.data.config.position.z;
				behaviour.origin.scale = index != 0 ? hotspot.behaviours.values[index-1].scale : hotspot.data.config.scale;
				behaviour.origin.opacity = index != 0 ? hotspot.behaviours.values[index-1].opacity : hotspot.data.config.opacity;

				hotspot.currentTween = null;
			});

		if(index > 0)
			hotspot.behaviours.values[index-1].tween.chain(behaviour.tween);
	});


	if(hotspot.behaviours.type == 'video' && hotspot.behaviours.values.length > 0){

		if(video.isPlaying)
			behaviourPlay();

		$(document).on("panoVideoPlay",behaviourPlay);
		$(document).on("panoVideoEnded",behaviourRestart);
		$(document).on("panoVideoPause",behaviourPause);
	}
	else if(hotspot.behaviours.type == 'voice' && hotspot.behaviours.values.length > 0){
		$(document).on("voiceoverPlay",behaviourPlay);
		$(document).on("voiceoverEnded",behaviourRestart);
		$(document).on("voiceoverPause",behaviourPause);
	}

	function behaviourPlay(){
		if(!hotspot.currentTween && hotspot.behaviours)
			hotspot.behaviours.values[0].tween.start();
		else if(hotspot.currentTween)
			hotspot.currentTween.start();
	}
	function behaviourPause(){

		if(!hotspot.behaviours)
			return;

		if(!hotspot.currentTween)
			hotspot.behaviours.values[0].tween.stop();
		else
			hotspot.currentTween.stop();
	}

	function behaviourRestart(){
		if(hotspot.behaviours)
			hotspot.behaviours.values[0].tween.start();
	}

}


function createHotspotButton(hotspot){

	var data = hotspot.data,label,icon;

	var action = data.loadSceneId != null || data.media != null || (data.description != null && (data.description.text != null));

	if(typeof data.button === 'string' && data.button.length > 0)
		data.button = JSON.parse(data.button);

	if(data.button.label){
		label = new textSprite (data.button.label, 80 , showHowColors.purple, true, "Lato" , action ? "#fff" : showHowColors.lightGrey , null,data.button.icon != null ?"left" : null );
		label.scale.set(hotspotScaleFactor,hotspotScaleFactor,hotspotScaleFactor);
		label.material.opacity = hotspot.data.config.opacity || 1;

		var labelBox = new THREE.Box3().setFromObject( label );
		hotspot.add(label);
	}

	if(data.button.icon){
		icon = new textSprite (fontAwesome[data.button.icon][0], 80 , showHowColors.purple, false ,"FontAwesome",action ? "#fff" : showHowColors.lightGrey , action ? showHowColors.purple : showHowColors.grey, labelBox  ?"right" : null);
		icon.scale.set(hotspotScaleFactor,hotspotScaleFactor,hotspotScaleFactor);
		icon.material.opacity = hotspot.data.config.opacity || 1;
		if(labelBox)
			icon.position.x = labelBox.min.x - ((icon.width  * hotspotScaleFactor)/2);

		hotspot.add(icon);
	}

	if(data.loadSceneId){
		var arrow = hotspotArrow(labelBox.min.x/5);
		var box = new THREE.Box3().setFromObject( arrow );
		arrow.position.x = data.button.icon != null ? labelBox.min.x/5 - ((icon.width  * hotspotScaleFactor)/2) - box.min.x : labelBox.min.x/5 - box.min.x;
		arrow.position.y =  labelBox.min.y + box.min.y + 2;
		arrow.position.z = 1;
		hotspot.add(arrow);
	}

	hotspot.hideButton = function(){
		if(label)label.visible = false;
		if(icon)icon.visible = false;
	}

	hotspot.showButton = function(){
		if(label)label.visible = true;
		if(icon)icon.visible = true;
	}
}


function textSprite(text,size,color,italics,font,backgroundColor,borderColor,hideEdge,width) {

	var font = font || "Lato";

	font = size + "px " + font;

	var canvas = createHiDPICanvas(400, 400);
	var context = canvas.getContext('2d');
	context.font = font;
	context.textAlign = "center";

	var metrics = context.measureText(text),
		textWidth = metrics.width;

	canvas.width = width || textWidth + (size/2);

	canvas.height = size * 1.5;
	context.font = font;


	if(typeof hideEdge != 'string')hideEdge = null;

	var radius = 15;

	if(borderColor){
		context.fillStyle = borderColor;

		roundRect(context, 0, 0, canvas.width, canvas.height, {
			tl: hideEdge == "left" ?  null : radius,
			br: hideEdge == "right" ?  null : radius,
			tr: hideEdge == "right" ?  null : radius,
			bl: hideEdge == "left" ?  null : radius
		}, true);
	}

	if(backgroundColor)
	{

		context.fillStyle = backgroundColor;
		if(borderColor)
			roundRect(context,hideEdge ? hideEdge == "left"
					? 0
					: hideEdge == "right"
					? 1
					: 1
					: 1
				, 0, hideEdge ? canvas.width-1 : canvas.width-2, canvas.height, {
					tl: hideEdge == "left" ?  null : radius,
					br: hideEdge == "right" ?  null : radius,
					tr: hideEdge == "right" ?  null : radius,
					bl: hideEdge == "left" ?  null : radius
				}, true);
		else
			roundRect(context, 0, 0, canvas.width, canvas.height, {
				tl: hideEdge == "left" ?  null : radius,
				br: hideEdge == "right" ?  null : radius,
				tr: hideEdge == "right" ?  null : radius,
				bl: hideEdge == "left" ?  null : radius
			}, true);
	}

	context.fillStyle = color;
	context.fillText(text, (canvas.width/2) - (textWidth/2), size + 3);

	var texture = new THREE.Texture(canvas);
	texture.needsUpdate = true;

	var geometry = new THREE.PlaneBufferGeometry(canvas.width, canvas.height);
	//	geometry.center();

	var mesh = new THREE.Mesh(geometry
		,
		new THREE.MeshBasicMaterial({
			map: texture,
			transparent: true,  depthWrite: false
		})
	);

	mesh.width = canvas.width;
	return mesh;
}

function roundRect(ctx, x, y, width, height, radius, fill, stroke) {
	if (typeof stroke == 'undefined') {
		stroke = true;
	}
	if (typeof radius === 'undefined') {
		radius = 5;
	}
	if (typeof radius === 'number') {
		radius = {tl: radius, tr: radius, br: radius, bl: radius};
	} else {
		var defaultRadius = {tl: 0, tr: 0, br: 0, bl: 0};
		for (var side in defaultRadius) {
			radius[side] = radius[side] || defaultRadius[side];
		}
	}
	ctx.beginPath();
	ctx.moveTo(x + radius.tl, y);
	ctx.lineTo(x + width - radius.tr, y);
	ctx.quadraticCurveTo(x + width, y, x + width, y + radius.tr);
	ctx.lineTo(x + width, y + height - radius.br);
	ctx.quadraticCurveTo(x + width, y + height, x + width - radius.br, y + height);
	ctx.lineTo(x + radius.bl, y + height);
	ctx.quadraticCurveTo(x, y + height, x, y + height - radius.bl);
	ctx.lineTo(x, y + radius.tl);
	ctx.quadraticCurveTo(x, y, x + radius.tl, y);
	ctx.closePath();
	if (fill) {
		ctx.fill();
	}
	if (stroke) {
		ctx.stroke();
	}

}

createHiDPICanvas = function(w, h, ratio) {
	if (!ratio) { ratio = PIXEL_RATIO; }
	var can = document.createElement("canvas");
	can.width = w * ratio;
	can.height = h * ratio;
	can.style.width = w + "px";
	can.style.height = h + "px";
	can.getContext("2d").setTransform(ratio, 0, 0, ratio, 0, 0);
	return can;
};

var PIXEL_RATIO = (function () {
	var ctx = document.createElement("canvas").getContext("2d"),
		dpr = window.devicePixelRatio || 1,
		bsr = ctx.webkitBackingStorePixelRatio ||
			ctx.mozBackingStorePixelRatio ||
			ctx.msBackingStorePixelRatio ||
			ctx.oBackingStorePixelRatio ||
			ctx.backingStorePixelRatio || 1;

	return dpr / bsr;
})();

function createHotspotDescription(hotspot){

	console.log('createHotspotDescription');

	if(typeof hotspot.data.description === 'string')
		hotspot.data.description = JSON.parse(hotspot.data.description);

	var  h = 2 * Math.tan( vFOV / 2 ) * circleRadius;
	var a = window.innerWidth > window.innerHeight ? window.innerWidth/ window.innerHeight : window.innerHeight/ window.innerWidth;
	var width = (h * a) * 5;

	if(hotspot.data.media){
		if(hotspot.data.media.image)
			var checkWidth = setInterval(waitForWidth,100);
		else
			renderHotspotText(hotspot, width * 0.05);
	}
	else if (hotspot.data.description.text)
		renderHotspotText(hotspot, width);

	function waitForWidth(){
		if(hotspot.data.media.image){
			if(hotspot.data.media.image.width){
				clearInterval(checkWidth);
				width = hotspot.data.media.image.width;
				renderHotspotText(hotspot, width, true);
			}
		}
		/*		else if(hotspot.data.media.shape){
		 if(hotspot.data.media.shape.width){
		 clearInterval(checkWidth);
		 width = hotspot.data.media.shape.width * 5;
		 renderHotspotText(hotspot, width, true);
		 }
		 }*/
	}
}


function renderHotspotText(hotspot, width, media){

	console.log("renderHotspotText",width);

	if(renderInProgress === true)
		hotspotRenderList.push({hotspot: hotspot, width : width, media : media});
	else
		render(hotspot,width,media);

	function render(hotspot,width,media) {

		console.log("render",hotspot);

		renderInProgress = true;

		var data = hotspot.data;

		var textWidth;
		var text = data.description.text;

		if(!data.description.width)
			data.description.width = width;
		else
			width = data.description.width;

		if(text.indexOf('\\n') > - 1){

			var lines = text.split("\\n");

			lines.forEach(function(line){

				if(!textWidth)
					textWidth = getTextWidth(line, "90pt Lato");
				else if(getTextWidth(line, "90pt Lato") > textWidth)
					textWidth = getTextWidth(line, "90pt Lato");
			})

			text = lines.join("<br>");
		}
		else
			textWidth = getTextWidth(text, "90pt Lato");

		if (width > textWidth)
			width = textWidth;

        $('#html2canvasRenderer').empty();
		$('#html2canvasRenderer').append('<div id ="hotspot_' + data.id + '"><span style="-webkit-text-stroke: 5px #000">' + text + '</span></div>');

		if(!data.description.setting)
			data.description.setting = {'option':3,'fontcolor': showHowColors.purple,'backgroundcolor': showHowColors.lightGrey};

		var align;

		if(data.description.align != null){
			switch (data.description.align){
				case 1: align = 'left'; break;
				case 2: align = 'center'; break;
				case 3: align = 'right'; break;
			}
		}

		$('#html2canvasRenderer').css({
			'width': width,
			'text-align': align || (width == textWidth ? 'center' : 'left'),
			'position': 'absolute',
			'font-family': data.description.font || "Lato",
			'top': '0px',
			'padding': '50px 50px',
			'background-color': data.description.setting.backgroundcolor,
			'color': data.description.setting.fontcolor != null ? data.description.setting.fontcolor : showHowColors.purple,
			'font-size': media ? '50px' : '90px',
			'visibility': 'visible',
			'border-radius' : '15px',
			'text-shadow': data.description.setting.backgroundcolor == null
				? data.description.setting.fontcolor == "#ffffff"
				? '-3px -3px 0 #000, 3px -3px 0 #000, -3px 3px 0 #000, 3px 3px 0 #000'
				: '-3px -3px 0 #fff, 3px -3px 0 #fff,-3px 5px 0 #fff, 3px 5px 0 #fff'
				: null,
		    '-webkit-text-stroke': '5px #000'
		});

			html2canvas($("#hotspot_" + data.id), {
				onrendered: function (canvas) {

					$(document).trigger("hotspotRendered",canvas);

					var texture = new THREE.Texture(canvas);
					texture.needsUpdate = true;

					var textbox = new THREE.Mesh(
						new THREE.PlaneBufferGeometry(canvas.width, canvas.height),
						new THREE.MeshBasicMaterial({
							map: texture,
							transparent: true
						})
					);
					textbox.scale.set(hotspotScaleFactor, hotspotScaleFactor, hotspotScaleFactor);
					hotspot.add(textbox);

					hotspot.hideDescription = function () {

						textbox.visible = false;
					}
					hotspot.showDescription = function () {

						textbox.visible = true;

					}

					if (hotspot.data.button)
						textbox.visible = false;

					if (hotspot.data.media) {
						if (hotspot.data.media.image) {
						//	textbox.visible = false;
							textbox.position.y = -(hotspot.data.media.image.height * hotspotScaleFactor) / 2 - (canvas.height * hotspotScaleFactor) / 2;
							textbox.position.z = -1;

						}
					}

					if(hotspot.data.config.opacity)
						hotspot.setOpacity(hotspot.data.config.opacity);

					setTimeout(function () {
						$("#hotspot_" + data.id).remove();

						if(hotspotRenderList.length > 0){
							var next = hotspotRenderList.pop();
							render(next.hotspot,next.width,next.media);
						}
						else
							renderInProgress = false;
					}, 10);
				},

				width: $("#hotspot_" + data.id).width() + 100,
				height: $("#hotspot_" + data.id).height() + 100
			});
	}
}



function getTextWidth(text, font) {
	// re-use canvas object for better performance
	var canvas = getTextWidth.canvas || (getTextWidth.canvas = document.createElement("canvas"));
	var context = canvas.getContext("2d");
	context.font = font;
	var metrics = context.measureText(text);
	return metrics.width;
}

function createImageHotspot(hotspot,file){

	hotspot.data.media.image = {};

	var loader = new THREE.TextureLoader();
	loader.setCrossOrigin("");

	loader.load(
		cdnPrefix + '/' + file.name,
		function ( texture ) {

			var media = new THREE.Mesh(
				new THREE.PlaneGeometry(texture.image.width, texture.image.height),
				new THREE.MeshBasicMaterial({
					map: texture,
					transparent: true
				})
			);
			media.scale.set(hotspotScaleFactor,hotspotScaleFactor,hotspotScaleFactor);
			hotspot.add(media);
			hotspot.data.media.image = texture.image;

			hotspot.hideMedia = function(){
				media.visible = false;
			};
			hotspot.showMedia = function(){
				media.visible = true;
			};

			if(hotspot.data.button)
				hotspot.hideMedia();

			if(hotspot.data.config.opacity)
				hotspot.setOpacity(hotspot.data.config.opacity);
		}
	);
}

function createShapeHotspot(hotspot,data){

	console.log('createShapeHotspot');

	var texture;
	var canvasRender;
	var canvas = createHiDPICanvas(400, 400);
	var ctx = canvas.getContext('2d');

	ctx.lineWidth="10";
	switch (parseInt(data.media.stroke)){
		case 0:
			ctx.strokeStyle = showHowColors.purple;
			break;
		case 1:
			ctx.strokeStyle = showHowColors.lightGrey;
			break;
		case 2:
			ctx.strokeStyle = showHowColors.grey;
			break;
	}
	switch (parseInt(data.media.fill)){
		case 0:
			ctx.fillStyle = showHowColors.purple;
			break;
		case 1:
			ctx.fillStyle = showHowColors.lightGrey;
			break;
		case 2:
			ctx.fillStyle = showHowColors.grey;
			break;
	}

	if(data.media.points && data.media.points.length > 0 ){

		ctx.beginPath();
		data.media.points.forEach(function(point,index){
			if(index == 0){
				ctx.moveTo(100 + point.x , 100 +point.y );
			}
			else
				ctx.lineTo(100 +point.x , 100 +point.y );
		});
		ctx.lineTo(100 +data.media.points[0].x, 100 +data.media.points[0].y);
	}
	else if(data.media.shape == "square"){
		ctx.rect(20,20,360,360);
	}
	else if (data.media.shape == "circle"){
		ctx.arc(200,200,190,0,2*Math.PI);
	}

	if(data.media.stroke > -1)
		ctx.stroke();

	if(data.media.fill > -1)
		ctx.fill();

	texture = new THREE.Texture(canvas);
	texture.needsUpdate = true;

	canvasRender = new THREE.Mesh(
		new THREE.PlaneBufferGeometry(canvas.width, canvas.height),
		new THREE.MeshBasicMaterial({
			map: texture,
			transparent: true
		})
	);
	canvasRender.scale.set(0.5,0.5,0.5);
	hotspot.add(canvasRender);
	hotspot.shape = canvasRender;

}


function detectHotpotInCentre(){

	var vrRayCaster = new THREE.Raycaster();
	vrRayCaster.set( camera.getWorldPosition(), camera.getWorldDirection() );
	var intersects = vrRayCaster.intersectObjects( targetList, true );

	if ( intersects.length > 0 )
		hotspotInFocus(intersects[0].object);
	else
		hotspotInFocus(null);

}

function hotspotClick(object){
	console.log('hotspots.hotspotClick()');


	if(!object)
		return;

	if(object.tweening)
		return;

	if(object.parent)
		if(object.parent.tweening)
			return;

	if(object.name == "textbox"){
		addMouseScrolling();
		return;
	}


	if(viewingHotspot)
		if((viewingHotspot.data && viewingHotspot == object.parent) || viewingHotspot && object.name == "close") {
			deselectHotspot();
			return;
		}


	if(object.parent.data)
		onHotspotMouseDown(object.parent);
	else if(object.parent.menuData)
		onMenuItemMouseDown(object.parent);
	else if(object.parent.choiceData)
		onAnswerItemMouseDown(object.parent);
	else if(object.parent.hotspotData)
		onLegacyHotspotMouseDown(object);


	isUserInteracting = false;

}

//var radius = 75,endPercent = 100,circ = Math.PI * 2,quart = Math.PI / 2;
function onHotspotMouseDown(hotspot){

	$( document ).trigger( "hotspot",hotspot );

	if(hotspot.hotspotData){
		onLegacyHotspotMouseDown(hotspot.children[0]);
		return;
	}

	if(!hotspot.data)
		return;

	if(hotspot.data.loadSceneId){
		getScene(hotspot.data.loadSceneId);
	}
	else if(hotspot.data.description || hotspot.data.media){

		if(hotspot.data.button)
			hotspot.hideButton();

		if(hotspot.data.description)
			hotspot.showDescription();

		if(hotspot.data.media){
			if(hotspot.data.media.image)
				hotspot.showMedia();

			//if(hotspot.data.media.shape)
		}

		setTimeout(tweenHotpotToCamera,100,hotspot);
	}



}

function onLegacyHotspotMouseDown(object){
	//sort bubbles
	console.log(object);

	trackEvent( 'hotspot', 'media', guideId+'|'+ object.parent.hotspotData.id +'|' +currentScene.id );

	if(object.parent.hotspotData.targetSceneId){
		//object.material.opacity = 0.5;
		//tweenToobject.parent(object.parent, function(){checkForSceneToLoad()}, true );
		getScene(object.parent.hotspotData.targetSceneId);
		trackEvent( 'hotspot', 'bubble', guideId+'|'+ object.parent.hotspotData.targetSceneId +'|' +currentScene.id );
		$( document ).trigger( "hotspot" );
	}
	//everything else tweens
	else if(!viewingHotspot){
		//if unselectable tween camera towards hotspot then zoom
		if(object.parent.hotspotData.selectable == false){
			$( document ).trigger( "hotspot" );
			//tweenToobject.parent(object.parent, function(){tweenHotpotToCamera( object.parent ); if(object.parent.hotspotData.buttonSceneId) checkForSceneToLoad(); }, true );
			if(object.parent.hotspotData.buttonSceneId != null)
				getScene(object.parent.hotspotData.buttonSceneId);
		}
		else
			tweenHotpotToCamera( object.parent );
	}
}


function hotspotInFocus(object){

	if(object && !viewingHotspot && (object.name !== 'textbox')){

		if(object.material.opacity < 0.1)
			return;

		if(gameTask)
			if(gameTask.hotspotsToFind && object.parent.hotspotData)
				if(!gameTask.hotspotsToFind.some(function(item){return item.id == object.parent.hotspotData.id}))
					return;

		if(!selectedHotspot) {
			selectedHotspot = object.parent;

			if(selectedHotspot.menuData && (selectedHotspot.menuData.sceneId === currentScene.id))
				return;

			setTimeout(function(){
				if(selectedHotspot && selectedHotspot.active){
					pause();
					targetCircle.activate();
				}


			},10);
		}
	}
	else if(selectedHotspot && !viewingHotspot)
	{

		targetCircle.deactivate();

		selectedHotspot = null;
		deselectItems();

		setTimeout(function(){
			if(!menuActive)
				play();
		},200);

	}
	else if(!object && viewingHotspot){
		if(!viewingHotspot.tweening)
			deselectHotspot();
	}

}

function tweenHotpotToCamera(object){
	console.log('hotspots.tweenHotpotToCamera('+ object.toString() +')');

	viewingHotspot = object;

	console.log(viewingHotspot);

	viewingHotspot.tweening = true;
	updateShader(-0.5,-0.5);

	object.originalPosition = object.position.clone();
	object.originalQuaternion = object.quaternion.clone();

	//magic maths to get objects around the same size of the screen
	var vFOV = camera.fov * Math.PI / 180;
	var pLocal,cPos;
	var bbox=getCompoundBoundingBox( object );
	var sizeY = bbox.max.y-bbox.min.y;
	var sizeX= bbox.max.x-bbox.min.x;

	sizeX*=object.scale.x;
	sizeY*=object.scale.y;

	// ALEX
	// assuming FOV is vertical
	var ratio=window.innerWidth/window.innerHeight;
	var uSpace= vrMode ? 1 : 0.5;//used space
	vFOV/=2;
	sizeY/=2;sizeX/=2;// half size
	sizeY*=uSpace;sizeX*=uSpace;
	var mode=0;
	/*	if(object.hotspotData.description)
	 {
	 if(ratio>=1)//landscape
	 {
	 sizeX*=1/0.6;
	 mode=1;
	 }else
	 {
	 sizeY*=1/0.6;
	 mode=2;
	 }
	 }*/
	var tanFov=Math.tan( vFOV );
	var distY=sizeY/tanFov;
	var distX=sizeX/(ratio*tanFov);
	if(distX<distY)distX=distY;
	pLocal= new THREE.Vector3( 0, 0, -distX );

	cPos = camera.position.clone();
	cPos.y -= 1.5;

	/*
	 // offset to the
	 switch(mode)
	 {
	 case 1:{
	 var W2=distX*(ratio*tanFov);
	 W2*=0.4;
	 pLocal.x-=W2;
	 cPos.x-=W2;
	 }break;
	 case 2:{
	 var H2=distX*tanFov;
	 H2*=0.4;
	 pLocal.y=H2;
	 cPos.y+=H2;
	 }break;
	 }
	 */

	//apply the direction the camera is facing
	var target = pLocal.applyMatrix4( camera.matrixWorld );
	var targetLook = cPos.applyMatrix4( camera.matrixWorld );


	object.position.copy(target);
	object.lookAt(targetLook);
	var targetRotation=object.quaternion.clone();
	object.position.copy(object.originalPosition);
	object.quaternion.copy(object.originalQuaternion);

	var tweenMove = new TWEEN.Tween(object.position).to(target, hotspotTweenTime).easing(TWEEN.Easing.Cubic.InOut);

	tweenMove.onUpdate(function(){
		var dstF=object.originalPosition.distanceTo(target);
		var dstC=object.originalPosition.distanceTo(object.position);
		var k=dstC/dstF;
		THREE.Quaternion.slerp(object.originalQuaternion,targetRotation,object.quaternion,k);
	});
	tweenMove.onComplete(function(){
		if(viewingHotspot)
			viewingHotspot.tweening = false;

		waitForMovement(0.5,deselectHotspot);
	});
	tweenMove.start();
	object.rotationAmount=0;

	if(object.getOpacity() < 1){
		var o ={opacity : object.getOpacity()},t = {opacity : 1}
		var tweenOpacity = new TWEEN.Tween(o).to(t, hotspotTweenTime).easing(TWEEN.Easing.Cubic.InOut).onUpdate(function(){
			object.setOpacity(o.opacity)
		}).start();
	}

}



function tweenToSelectedHotspot(object , callback, zoom, auto){
	console.log('tweenToSelectedHotspot('+ object.hotspotData.title +')');

	return;

	var newLatLon = {lat: lat, lon : lon};
	var targetLatLon = hotspotPanTiltToLatLon(object.hotspotData.pan,object.hotspotData.tilt );
	var newFov = {nFov : camera.fov};
	var targetFov = {nFov : object.hotspotData.distance*0.03};


	//if its been spun around a lot this number gets big
	if(Math.abs(newLatLon.lon) > 360){
		var f = newLatLon.lon/360;
		f = f - Math.floor(f);
		newLatLon.lon = f * 360;
	}



	if(newLatLon.lon < 0)
		newLatLon.lon += 360;

	if(Math.abs(newLatLon.lon - targetLatLon.lon) > 180)
		targetLatLon.lon += targetLatLon.lon > 360 ? -360 : 360;

	var diff = newLatLon.lon - targetLatLon.lon;


	var time = auto == true ? Math.abs(diff) *  150 : Math.abs(diff) * 100;

	var loadsScene = selectedHotspot.hotspotData.targetSceneId != null;

	//set up zoom tween
	var tweenZoom = new TWEEN.Tween( newFov ).to( targetFov, time ).easing(TWEEN.Easing.Cubic.Out);
	tweenZoom.onUpdate(function(){
		camera.fov = newFov.nFov;
		camera.updateProjectionMatrix();
	});

	//tween lon/lat to tween camera
	var tweenLon = new TWEEN.Tween(newLatLon).to(targetLatLon, time).easing(TWEEN.Easing.Cubic.Out);;
	tweenLon.onUpdate(function(){
		lon = newLatLon.lon;
		lat = newLatLon.lat;

		if(loadsScene && sceneReady)
			loadScene();
		else if( !loadsScene && lon < newLatLon.lon + 5 && lon > newLatLon.lon - 5 && zoom)
			tweenZoom.start();
	});
	tweenLon.onComplete(function(){if(callback)callback.call(); $( document ).trigger( "hotspot" )});
	tweenLon.start();

}

function checkForSceneToLoad(){
	console.log('checkForSceneToLoad()');
	if(sceneReady)
		loadScene();
	else if(selectedHotspot)
		setTimeout(checkForSceneToLoad,100);
}

function deselectHotspot()
{


	console.log('deselect',viewingHotspot);

	if(!viewingHotspot)
		return;

	viewingHotspot.tweening = true;

	updateShader(0, 0);


	if(viewingHotspot.hotspotData)
		if(viewingHotspot.hotspotData.selectable)
			$( document ).trigger( "hotspot" );

	var shs = viewingHotspot;
	viewingHotspot = null;

	var srcRotation=shs.quaternion.clone();
	//var targetRotation=shs.originalQuaternion.clone();
	var srcPos=shs.position.clone();


	if((shs.data.button || shs.data.media) && shs.data.description)
		shs.hideDescription();

	var tweenMove=new TWEEN.Tween(shs.position).to(shs.originalPosition, 1000).easing(TWEEN.Easing.Cubic.InOut);
	tweenMove.onComplete(function(){

		if(!shs.data)
			return;

		if(inIframe())

			if((shs.data.button && shs.data.media))
				shs.hideMedia();

		if(shs.data.button)
			shs.showButton();

		shs.tweening = false;
	});

	tweenMove.onUpdate(function(){
		if(shs){
			var dstF=srcPos.distanceTo(shs.originalPosition);
			var dstC=srcPos.distanceTo(shs.position);
			var k=dstC/dstF;
			THREE.Quaternion.slerp(srcRotation,shs.originalQuaternion,shs.quaternion,k);//
		}
	});
	tweenMove.start();

	if(shs.data)
		if(shs.data.config.opacity < 1){
			var o ={opacity : 1},t = {opacity : shs.data.config.opacity}
			var tweenOpacity = new TWEEN.Tween(o).to(t, hotspotTweenTime).easing(TWEEN.Easing.Cubic.InOut).onUpdate(function(){
				shs.setOpacity(o.opacity)
			}).start();
		}
}


function powerOf2Down(value)
{
	if(value < 80)
		return 64;
	else if(value < 150)
		return 128;
	else if(value < 400)
		return 256;
	else if(value < 800)
		return 512;

	return 1024;
}

function powerOf2Up(value)
{
	if(value <= 64)
		return 64;
	else if(value <= 128)
		return 128;
	else if(value <= 256)
		return 256;
	else if(value <= 512)
		return 512;

	return 1024;
}
function getCompoundBoundingBox(object3D) {
	var box = null;
	object3D.traverse(function (obj3D) {

		var geometry = obj3D.geometry;
		if (geometry === undefined) return;
		geometry.computeBoundingBox();
		if (box === null) {
			box = geometry.boundingBox;
		} else {
			box.union(geometry.boundingBox);
		}
	});
	return box;
}

function hotspotArrow(width){
	var geometry = new THREE.Geometry();
	var v1 = new THREE.Vector3(0,-width/4,0);
	var v2 = new THREE.Vector3(-width,-width,0);
	var v3 = new THREE.Vector3(width,-width,0);

	geometry.vertices.push(v1);
	geometry.vertices.push(v2);
	geometry.vertices.push(v3);

	geometry.faces.push(new THREE.Face3(0, 1, 2));
	geometry.center();

	var arrow = new THREE.Object3D();
	var mat = new THREE.MeshBasicMaterial({color: "#fff"});
	mat.side  = THREE.DoubleSide;
	var triangle = new THREE.Mesh(geometry, mat);

	arrow.add(triangle);
	return arrow;
}





