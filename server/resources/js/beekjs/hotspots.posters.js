var postersToBuild = [];

function createPoster(data){

	console.log('poster()');

	var poster = new THREE.Object3D();
	poster.hotspotData = data;
	poster.position.copy( panTiltToVector( data.pan, data.tilt, (data.distance / 2000) * 1024) );
	poster.scale.normalize().multiplyScalar(0.1);
	poster.lookAt( camera.position );


	if(!data.showIcon){
		if(data.rotationX!==null)poster.rotation.x = -data.rotationX;

		poster.rotation.order="YXZ";
		poster.frustumCulled = true;
		poster.rotation.y = data.rotationY != null
			? Math.PI - data.rotationY
			: 0;

		poster.rotation.z = 0;
	}

	//for those pesky ones with no values
	if(poster.rotation.x == 0 || poster.rotation.y ==  0)
		poster.lookAt( camera.position );

	var p2B = {};
	p2B.poster = poster;
	p2B.data = data;

	postersToBuild.push(p2B);

	if(postersToBuild.length == posters.length)
		buildPosters( postersToBuild.pop() );

}

function buildPosters(p2B){
	console.log('buildPosters');
	if(p2B)
		setTimeout(html2CanvasPoster,100, p2B.data, p2B.poster );
}

function html2CanvasPoster(data, poster){


	if(!$('#posterHolder').length)
		$('body').append('<div id ="posterHolder"/>');

	$('#posterHolder').append('<span>' + cleanPosterText( data.text ) + '</span>');

	console.log(cleanPosterText( data.text ));
	//set poster size
	if(data.frame == "0"){
		$('#posterHolder').css({
			'background-color': 'transparent',
		    'padding': '10px 10px',
			'width': '60%'
		});
	}
	else{
		$('#posterHolder').css({
			'background-color':'#ffffff',
		'padding': '10px 10px',
			'width': '60%'
		});
	}


	html2canvas(posterHolder, {
		onrendered: function(canvas) {
			var texture = new THREE.Texture(canvas);
			texture.needsUpdate = true;

			var posterText = new THREE.Mesh(
				new THREE.BoxGeometry(canvas.width, canvas.height,1),
				new THREE.MeshBasicMaterial({
					map: texture,
					transparent: true
				})
			);

			if(data.showIcon){
				posterText.visible = false;
				icon = createIcon();
				poster.add( icon );
			}

			poster.add( posterText );
			scene.add( poster );

			targetList.push( poster );

			$('#posterHolder').empty();
			buildPosters(postersToBuild.pop());

		},

		width: $('#posterHolder').width() + 50,
		height: $('#posterHolder').height() + 50
	});


}


var PIXEL_RATIO = (function () {
	var ctx = document.createElement("canvas").getContext("2d"),
		dpr = window.devicePixelRatio || 1,
		bsr = ctx.webkitBackingStorePixelRatio ||
			ctx.mozBackingStorePixelRatio ||
			ctx.msBackingStorePixelRatio ||
			ctx.oBackingStorePixelRatio ||
			ctx.backingStorePixelRatio || 1;

	return dpr / bsr;
})();



createHiDPICanvas = function(w, h, ratio) {
	if (!ratio) { ratio = PIXEL_RATIO; }
	var can = document.createElement("canvas");
	can.width = w * ratio;
	can.height = h * ratio;
	can.style.width = w + "px";
	can.style.height = h + "px";
	can.getContext("2d").setTransform(ratio, 0, 0, ratio, 0, 0);
	return can;
};



function createIcon() {
	var canvas = createHiDPICanvas(400, 400);
	var context = canvas.getContext('2d');
	context.beginPath();
	context.arc(150, 150, 100, 0, 2 * Math.PI, false);
	context.fillStyle = 'rgba(0, 0, 0, 0.4)';
	context.fill();
	context.lineWidth = 10;
	context.strokeStyle = '#fff';
	context.stroke();

	context.font = "bold 160px helvetica neue";
	context.fillStyle = '#fff';
	context.fillText("i", 130, 210);

	var texture = new THREE.Texture(canvas);
	texture.needsUpdate = true;

	var mesh = new THREE.Mesh(
		new THREE.BoxGeometry(canvas.width, canvas.height,1),
		new THREE.MeshLambertMaterial({
			map: texture,
			transparent: true
		})
	);

	return mesh;

};

function cleanPosterText( text ){


	//var lines = text.split('</P>');
	//var processedLines = [];

	//    for(var i in lines){
	var $desc = (text)
		// .replace(/<p.+?>/ig,  "")
		// .replace(/<\/p.+?>/ig,  "")
		// .replace(/<L.+?>/ig, "")
		// .replace(/<[b|u]>/ig, " ")
		//.replace(/&apos;/ig, "'")
		//.replace(/&amp;/ig, "&")
		//  .replace(/<\/[b|u]>/ig, " ")
		.replace(/<TEXTFORMAT.+?>/g, "")
		.replace(/<\/TEXTFORMAT>/g, "")
		 .replace(/FACE="*"/g, "font-family = 'Lato', sans-serif")
		// .replace(/<\/FONT>/g, "")
		.replace(/<a.+?event:(.+?)" TARGET.*?>(.*?)<\/a>/ig, "");

	//if($desc.length > 1)
	//processedLines.push($desc);

	//}

	return $desc;
}


