var video, source, panoVideoMesh,panoShader, videoSettings, firstPlay = true;

var bufferingInterval, checkInterval  = 50.0,lastPlayPos = 0,currentPlayPos = 0,bufferingDetected = false;


function panovideo(videoData) {

    console.log('panoVideo()',videoData);

    video = null;

  if(!video){
        video = document.createElement( 'video' );
         source = document.createElement('source');

        //if(!gameInProgress)
            //video.loop = true;

        video.crossOrigin = "anonymous";
        video.preload = 'auto';
        video.setAttribute('webkit-playsinline', 'true');
        source.setAttribute('type',"video/mp4");

    }

    videoSettings = {};

    try {
        videoSettings = JSON.parse(videoData);

        if(typeof videoSettings === 'string')
            videoSettings = JSON.parse(videoSettings);

    } catch (e) {
        console.log(e);
        videoSettings.filename = videoSettings;
    }




    currentScene.video = videoSettings;

    if(videoSettings.filename)
        source.setAttribute('src',cdnPrefix + "/" + videoSettings.filename);
    else if(videoSettings.lowRes)
        source.setAttribute('src',cdnPrefix + "/" + videoSettings.lowRes);


    console.log(source.src);

    if(videoSettings.volume)
        video.volume = videoSettings.volume;

    video.appendChild(source);

    video.play();
    checkPlayBack(videoSettings);

}


var checkPlayBack = function(videoSettings){

    var panoPlayBackInterval = setInterval(function(){
        if (video.currentTime > 0 || video.readyState > 0){
          clearInterval(panoPlayBackInterval);

            $(document).trigger("panoVideoLoaded",videoSettings);

            $("#tab\\:modalScene\\:duration").val(video.duration);
            videoSettings.duration = video.duration;

        //    console.log(video.videoWidth,currentScene.video.width);

/*            if(video.videoWidth != currentScene.video.width){
                currentScene.video.height = video.videoWidth;

                if(video.videoHeight != currentScene.video.height)
                    currentScene.video.height = video.videoHeight;

                if(currentScene.video.hiRes != null)
                    currentScene.video.filename = currentScene.video.hiRes;

                console.log("reset settings");
                setVideoSettings();
            }*/

            if(typeof guideId === 'undefined')
                updateVideoDuration();
            
            showVideo(video);

            if(videoSettings.startTime)
             video.currentTime = videoSettings.startTime;

            video.ontimeupdate = function() {

                if(!video)
                    return;

                    if(videoSettings.endTime > 0)
                        if(video.currentTime > videoSettings.endTime){
                            video.currentTime = videoSettings.startTime || 0;
                            onVideoEnded();
                        }


            };

            video.onended = onVideoEnded();
            video.onpause = onVideoPaused();

        }
   },10);
}

function onVideoEnded(event){
    $(document).trigger("panoVideoEnded");
    video.isPlaying = false;
}

function onVideoPaused(event){
    $(document).trigger("panoVideoPause");
    video.isPlaying = false;
}

function showVideo(video){
    console.log("panoVideo.showVideo()");

    var panoTexture = new THREE.VideoTexture( video );
    panoTexture.minFilter = THREE.LinearFilter;
    panoTexture.magFilter = THREE.LinearFilter;

    panoShader = THREE.BrightnessContrastShader;
    panoShader.uniforms[ "brightness" ].value = currentScene.video.brightness | 0;
    panoShader.uniforms[ "contrast" ].value = currentScene.video.contrast | 0;
    panoShader.uniforms[ "hue" ].value = currentScene.video.hue | 0;
    panoShader.uniforms[ "saturation" ].value = currentScene.video.saturation | 0;
    panoShader.uniforms[ "tDiffuse" ].value = panoTexture;

    panoVideoMesh = new THREE.Mesh( new THREE.SphereGeometry( 500, 60, 40 ), new THREE.ShaderMaterial(panoShader) );
    panoVideoMesh.scale.x = -1;
    panoVideoMesh.rotation.y = Math.PI / 2;

    video.isPlaying = true;

    $(document).trigger("panoVideoPlay");

    bufferingInterval = setInterval(checkBuffering, checkInterval);

    scene.add(panoVideoMesh);

    loadScene();
}

function checkBuffering() {

        if(!video){
        clearInterval(bufferingInterval);
        return;
    }
    currentPlayPos = video.currentTime;
    var offset = 1 / checkInterval;

    if (
        !bufferingDetected
        && currentPlayPos < (lastPlayPos + offset)
        && !video.paused
    ) {
        $(document).trigger("panoVideoPause");
        bufferingDetected = true;
    }

    if (
        bufferingDetected
        && currentPlayPos > (lastPlayPos + offset)
        && !video.paused
    ) {
        $(document).trigger("panoVideoPlay");
        bufferingDetected = false;
    }
    lastPlayPos = currentPlayPos;
}

function removePanoVideo(){
    video.pause();
    video.remove();
    source.remove();

    video = null;
    clearInterval(bufferingInterval);
}


var makeVideoPlayableInline=function(){"use strict";function e(e){function r(t){n=requestAnimationFrame(r),e(t-(i||t)),i=t}var n,i;this.start=function(){n||r(0)},this.stop=function(){cancelAnimationFrame(n),n=null,i=0}}function r(e,r,n,i){function t(r){Boolean(e[n])===Boolean(i)&&r.stopImmediatePropagation(),delete e[n]}return e.addEventListener(r,t,!1),t}function n(e,r,n,i){function t(){return n[r]}function d(e){n[r]=e}i&&d(e[r]),Object.defineProperty(e,r,{get:t,set:d})}function i(e,r,n){n.addEventListener(r,function(){return e.dispatchEvent(new Event(r))})}function t(e,r){Promise.resolve().then(function(){e.dispatchEvent(new Event(r))})}function d(e){var r=new Audio;return i(e,"play",r),i(e,"playing",r),i(e,"pause",r),r.crossOrigin=e.crossOrigin,r.src=e.src||e.currentSrc||"data:",r}function a(e,r,n){(f||0)+200<Date.now()&&(e[h]=!0,f=Date.now()),n||(e.currentTime=r),T[++w%3]=100*r|0}function o(e){return e.driver.currentTime>=e.video.duration}function u(e){var r=this;r.video.readyState>=r.video.HAVE_FUTURE_DATA?(r.hasAudio||(r.driver.currentTime=r.video.currentTime+e*r.video.playbackRate/1e3,r.video.loop&&o(r)&&(r.driver.currentTime=0)),a(r.video,r.driver.currentTime)):r.video.networkState!==r.video.NETWORK_IDLE||r.video.buffered.length||r.video.load(),r.video.ended&&(delete r.video[h],r.video.pause(!0))}function s(){var e=this,r=e[g];return e.webkitDisplayingFullscreen?void e[b]():("data:"!==r.driver.src&&r.driver.src!==e.src&&(a(e,0,!0),r.driver.src=e.src),void(e.paused&&(r.paused=!1,e.buffered.length||e.load(),r.driver.play(),r.updater.start(),r.hasAudio||(t(e,"play"),r.video.readyState>=r.video.HAVE_ENOUGH_DATA&&t(e,"playing")))))}function c(e){var r=this,n=r[g];n.driver.pause(),n.updater.stop(),r.webkitDisplayingFullscreen&&r[E](),n.paused&&!e||(n.paused=!0,n.hasAudio||t(r,"pause"),r.ended&&(r[h]=!0,t(r,"ended")))}function v(r,n){var i=r[g]={};i.paused=!0,i.hasAudio=n,i.video=r,i.updater=new e(u.bind(i)),n?i.driver=d(r):(r.addEventListener("canplay",function(){r.paused||t(r,"playing")}),i.driver={src:r.src||r.currentSrc||"data:",muted:!0,paused:!0,pause:function(){i.driver.paused=!0},play:function(){i.driver.paused=!1,o(i)&&a(r,0)},get ended(){return o(i)}}),r.addEventListener("emptied",function(){var e=!i.driver.src||"data:"===i.driver.src;i.driver.src&&i.driver.src!==r.src&&(a(r,0,!0),i.driver.src=r.src,e?i.driver.play():i.updater.stop())},!1),r.addEventListener("webkitbeginfullscreen",function(){r.paused?n&&!i.driver.buffered.length&&i.driver.load():(r.pause(),r[b]())}),n&&(r.addEventListener("webkitendfullscreen",function(){i.driver.currentTime=r.currentTime}),r.addEventListener("seeking",function(){T.indexOf(100*r.currentTime|0)<0&&(i.driver.currentTime=r.currentTime)}))}function p(e){var i=e[g];e[b]=e.play,e[E]=e.pause,e.play=s,e.pause=c,n(e,"paused",i.driver),n(e,"muted",i.driver,!0),n(e,"playbackRate",i.driver,!0),n(e,"ended",i.driver),n(e,"loop",i.driver,!0),r(e,"seeking"),r(e,"seeked"),r(e,"timeupdate",h,!1),r(e,"ended",h,!1)}function l(e,r,n){void 0===r&&(r=!0),void 0===n&&(n=!0),n&&!y||e[g]||(v(e,r),p(e),e.classList.add("IIV"),!r&&e.autoplay&&e.play(),"MacIntel"!==navigator.platform&&"Windows"!==navigator.platform||console.warn("iphone-inline-video is not guaranteed to work in emulated environments"))}var f,m="undefined"==typeof Symbol?function(e){return"@"+(e||"@")+Math.random()}:Symbol,y=/iPhone|iPod/i.test(navigator.userAgent)&&void 0===document.head.style.grid,g=m(),h=m(),b=m("nativeplay"),E=m("nativepause"),T=[],w=0;return l.isWhitelisted=y,l}();