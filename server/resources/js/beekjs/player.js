    function player_DoFSCommand(command, args) {
        if (command == "openWindow") { 
            var windowArgs = args.split("|"); 
            var domain = windowArgs[0]; 
            var width = windowArgs[1]; 
            var height = windowArgs[2]; 
            
            domain.replace('"', '');

            $('.iframeshim').css("left",(($( window ).width() - width)/2) + 10 +"px");
            $('.iframeshim').css("top",(($( window ).height() - height)/2) + 30 +"px");
            $('.iframeshim').css("width",width - 20 +"px");
            $('.iframeshim').css("height",height - 40 +"px");
            $('.iframeshim').attr("src",domain);

            $('.iframe-container').show();
       };
        
        if (command == "closeWindow") {$('.iframe-container').hide();}
    }
    

    function showPreloader()
    {
        document.getElementById("preloader").style.display = "block";
        document.getElementById("preloader").style.visibility = "visible";
        showProgress(0.2);
    }

    function preloadFlash()
    {
        showHideHelp();
        showPreloader();
        buildMenu();

        // fail safe in-case the thumb does not load.
        setTimeout("loadFlash()", 1000);

        loadImmediately = true;
    }

    function setSwfStage(stage)
    {
        switch(stage)
        {
            case STAGE_SWF_LOADED:
                showProgress(0.5);
                $('#loading').replaceWith('<h1>LOADING ASSETS</h1>');
                break;

            case STAGE_GUIDE_LOADED:
                showProgress(0.7);
                $('#loading').replaceWith('<h1>INITIALISING</h1>');
                break;

            case STAGE_SCENE_LOADED:
                showFlash();
                break;
        }
    }
    
    function showFlash()
    {
        clearTimeout(timeout);

        showProgress(1, setSwfVisible);
    }

    function setSwfVisible()
    {
        $('#swfContainer').css('left', '0');
        $('#preloader').remove();
        $('#preview').hide();
        $('#container').remove();
        $('#guide').remove();
    }

    function trackVisitKey(key)
    {
        _gaq.push(['_setCustomVar', 1, 'key', key, 1]);
    }

    function loadBeekJsFiles(){

    	showPreloader();
    	$('body').append('<div id="guide" class="guide"> <nav id="primary_nav_wrap"><ul id="guideList" class="guide"></ul></nav></div><div class="text" id="text">hello</div>');

    	Sid.js([
                "../../resources/js/beekjs/three.min.js",
                "../../resources/js/beekjs/tween.min.js",
                "../../resources/js/beekjs/Detector.js",
                "../../resources/js/beekjs/DeviceOrientationControls.js",
                "../../resources/js/beekjs/beek.js",
                "../../resources/js/beekjs/scene.js",
                "../../resources/js/beekjs/hotspots.js",
                "../../resources/js/beekjs/guide.js"
	    	],

			function(){showProgress(0.8);init();}
		);



    }
