
	function nextButton(){
		
		$( "body" ).append('<div id = "nextButton" class="nextPrev" ><i class="fa fa-chevron-right"></i></div>'); 
		$('#nextButton').click(function(){ nextScene(); });
	}
	
	function prevButton(){
		$( "body" ).append('<div id = "prevButton" class="nextPrev" ><i class="fa fa-chevron-left"></i></div>');
		$('#prevButton').click(function(){ prevScene(); });
		
	}
	
	function removePrevButton(){
		$('#prevButton').remove();
	}
	
	function nextScene(){

		
		 if (selectedHotspot){
			var int = targetList.indexOf( selectedHotspot );
			
			deselectHotspot();
			
			var next =  int + 1 < targetList.length -1
						? targetList[ int + 1 ] 
						: targetList[ 0 ]; 
			
			var nextFunct = function (){setTimeout(tweenHotpotToCamera,200, next )};			
			tweenToSelectedHotspot(  next, nextFunct );
			trackEvent( 'hotspot', 'next', guideId+'|'+ next.id +'|' +currentScene.id );
			
		}
		
		else if(videoPlaying){
			closeVideo();
			
			
		}
		
		else{
			console.log('loading next scene');
			var int = guideSceneOrder.indexOf( guideScene );
			
			var next =  int + 1 < guideSceneOrder.length 
						? guideSceneOrder[ int + 1 ] 
						: guideSceneOrder[ 0 ]; 
	
			guideScene = next;			
			console.log('next scene is ' + next.scene.title);
			getScene(  next.scene.id );
			
			trackEvent( 'NextPrev', 'next', guideId+'|'+ next.scene.id +'|' +currentScene.id );
		}

	}
	
	function prevScene(){
		
		
		if(guideOpen){
			var int = guideSectionOrder.indexOf( guideSection ) || 0 ;
			var prevSection =  int > 0
						? guideSectionOrder[ int - 1 ] 
						: guideSectionOrder[ guideSectionOrder.length - 1 ]; 
			guideSection = prevSection;
			
			var left = parseInt( $('.'+ prevSection.id).css('left') ) - $(window).width() * 0.1;
			$('#guide').animate({'left' :  - left},"slow");
			
			trackEvent( 'guide', 'prev', guideId +'|' +currentScene.id );
			return;
		}
		else if (selectedHotspot){
			var int = targetList.indexOf( selectedHotspot );
			
			deselectHotspot();
			
			var prev =  int > 0
						? targetList[ int - 1 ] 
						: targetList[ targetList.length - 1 ]; 
			
			var prevFunct = function (){setTimeout(tweenHotpotToCamera,200, prev )};			
			tweenToSelectedHotspot(  prev, prevFunct );
			
			trackEvent( 'hotspot', 'next', guideId+'|'+ prev.id +'|' +currentScene.id );
			
		}
		else
			history.go(-1);		
		
	}
	