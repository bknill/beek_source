	var sceneReady = false,
		firstRun = true,
		sceneLoaded = false,
		loadingSceneId,
		currentScene,
		sphere,
		boards,
		bubbles,
		historyScenes = [],
		panoShader,
		audioContext,
		voiceover,
		sceneId;
		//sceneDefaultQuaternion = new THREE.Quaternion();

	function getScene(id) {
		console.log('scene.getScene('+id+')' );

		if(id == loadingSceneId)
			return;

		if(video)
			video.pause();

		var jsonpURL = jsonpPrefix + '/scene/' + id + '/jsonp?callback=?';
		$.getJSON(jsonpURL, $.proxy(startScene, this));

		sceneId = loadingSceneId = id;

		if(!firstRun)
			updateShader(-1,0);
	};

	
	
	function startScene(sceneObject,useOutput) {
		console.log('scene.startScene()',sceneObject,useOutput );

		clearScene();

		currentScene = sceneObject;

		sceneLoaded = false;

		if(voiceover)
			voiceover.stop();



		if(typeof guideId !== undefined && getGuideScene(sceneObject.id) != null && (getGuideScene(sceneObject.id).video != null))
			panovideo(getGuideScene(sceneObject.id).video);
		else if(currentScene.outputVideo != null && useOutput)
			panovideo(currentScene.outputVideo);
		else if(currentScene.video != null)
			panovideo(currentScene.video);
		else
			loadPanoTiles();
	}
	
	function processHotspots(sceneObject){

		console.log('processHotspots',sceneObject.hotspots);

		for (ix in sceneObject.hotspots)
			createHotspot(sceneObject.hotspots[ix]);

		//get bubbles
        bubbles = [];
        for (ix in sceneObject.bubbles) 
        	bubbles.push(sceneObject.bubbles[ix]);

        //get boards
        boards = [];
        for (ix in sceneObject.boards) 
            boards.push(sceneObject.boards[ix]);
        
        //get photos
        photos = [];
        for (i in sceneObject.photos) 
            photos.push(sceneObject.photos[i]);
        
        //get posters
        posters = [];
        for (i in sceneObject.posters) 
            posters.push(sceneObject.posters[i]);
        
        //get sounds
        sounds = [];
        for (i in sceneObject.sounds) 
            sounds.push(sceneObject.sounds[i]);

	}

	
	function clearScene() {
		console.log('scene.clearScene()');


		if(!scene)
		return;

		selectedHotspot = null;
		viewingHotspot = null;
		targetList = [];

		
		var obj, i,o;
		for ( i = scene.children.length - 1; i >= 0 ; i -- ) {
		    obj = scene.children[ i ];
		    if ( obj !== projector && obj !== camera) {
		    	if(obj.hotspotData || obj.data)
		    		{
		    		   for ( o = obj.children.length - 1; o >= 0 ; o -- ) 
		    			   obj.remove(obj.children[o]);
   		}
		    	
		         scene.remove(obj);
		    }
		}

	};
	
	function createBox(){
		console.log("scene.createBox()");
		//the inital pano cube
	    box = new THREE.Mesh(new THREE.BoxGeometry(1024,1024,1024, 20, 20, 10,10), new THREE.MeshFaceMaterial(materials));
	    box.scale.x = -1;
		
	    scene.add(box);
	}

	function loadTexture(path,callback) {

		var texture = new THREE.Texture(texture_placeholder);
		texture.generateMipmaps = true;
		texture.magFilter = THREE.LinearFilter;
		texture.minFilter = THREE.LinearFilter;

		var material = new THREE.MeshBasicMaterial({
			map: texture,
			depthWrite:false
		});

		var image = new Image();
		image.crossOrigin = '';

		image.onload = function() {
			texture.image = this;
			texture.needsUpdate = true;

			callback(material);
		};
		image.src = path;

		return material;
	}
	
	function setDefaults(){
		console.log('scene.setDefaults()',currentScene);

		if(currentScene.cameraDefault != null){
			var defaults = JSON.parse(currentScene.cameraDefault);

			if(defaults.theta){
				controls.getVRDisplay().theta_ = defaults.theta;
				controls.getVRDisplay().phi_ = defaults.phi;
			}
		}

		sceneReady = false;
    	isUserInteracting = false;
	}

	
	function loadScene(){
		
		console.log('scene.loadScene()');
		selectedHotspot = null;

    	//TWEEN.removeAll();

		$( document ).trigger( "scene_loaded" );

	    createScene();

	}
	
	function createScene(){

		console.log("scene.createScene()");

    	setDefaults();

		sceneLoaded = true;

		if(!fontsAvailable)
			waitForFontAwesome(function(){waitForWebfonts(['Lato'],function(){
				fontsAvailable = true;
				processHotspots(currentScene)});
			})
		else
			processHotspots(currentScene);

		if(currentScene.voiceover){
			if(typeof currentScene.voiceover === 'string')
				try{
					currentScene.voiceover = JSON.parse(currentScene.voiceover)
				}catch(e){
					var filename = currentScene.voiceover;
					currentScene.voiceover = {};
					currentScene.voiceover.filename = filename;
				}

			$(document).trigger("voiceover");
		}
	}

   function delayTileLoad(){
	   loadTiles(queuedTiles);
	   queuedTiles = null;
	   
   }
   
   
   function setUp(){
	   console.log('scene.setUp()');
       $("#preloader").remove();
       //give it a moment to organise tiles before guide load

		firstRun = false;
   }
 
   
   function addLights(){
   
	   console.log('scene.addLights()');
	   
	   hsLight = new THREE.SpotLight( 0xffffff,0,500 );
	   hsLight.position.set(0, 0, 300);
	   camera.add( hsLight );
	   scene.add( camera );
	
		hemiLight = new THREE.HemisphereLight( 0xffffff, 0xffffff, 0.8 );
		scene.add( hemiLight);
		
		var ambiLight = new THREE.AmbientLight( 0x404040 ); // soft white light
		scene.add( ambiLight );
	   
   }
   
   function setLights(value){
	   console.log("setLights(" + value + ")" );

	    lightValue = value;
	   
	   if( hemiLight )
		   var tween = new TWEEN.Tween( hemiLight ).to( {intensity: value}, 1000 ).start();

   }

	function loadPanoTiles(){
		//get tiles
		tiles = [];
		materials = [];
		previewTileCounter = 0;
		for (var face in FACES) {
			materials.push(loadTexture(cdnPrefix + '/scene_' + currentScene.id + "_pano_" +
				currentScene.panoIncrement + "_" + FACES[face] + "/9/0_0.jpg",
				function() {
					previewTileCounter++;
					if (previewTileCounter == 6) {
						sceneReady = true;
						loadScene();
						createBox();
						loadSphereTexture(cdnPrefix + '/scene_' + currentScene.id + "_pano_" + currentScene.panoIncrement + "_EX.jpg",currentScene.id);
					}
				}
			));
			for (var tile in TILES) tiles.push(
				cdnPrefix + '/scene_' + currentScene.id + "_pano_" + currentScene.panoIncrement + "_" + FACES[face] + "/10/" + TILES[tile] + ".jpg"
			);
		}
	}

	function loadSphereTexture(path) {
		console.log('loadSphereTexture');
		var id = currentScene.id;
		THREE.ImageUtils.crossOrigin = '';
		var texture = THREE.ImageUtils.loadTexture(path, {} ,function(){

			if(id != currentScene.id)
				return;

			panoShader = THREE.BrightnessContrastShader;
			panoShader.uniforms[ "brightness" ].value = 0;
			panoShader.uniforms[ "contrast" ].value = 0;
			panoShader.uniforms[ "tDiffuse" ].value = texture;
			var panoMaterial = new THREE.ShaderMaterial(panoShader);

			createSphere(panoMaterial);

		});

		texture.magFilter = THREE.LinearFilter;
		texture.minFilter = THREE.NearestFilter;



	}

	function createSphere(material){
		console.log("createSphere(material)");

		var geometry = new THREE.SphereGeometry( 500, 60, 40 );
		sphere = new THREE.Mesh( geometry, material );
		sphere.scale.x = -1;
		sphere.rotation.y = Math.PI / 2;
		scene.add(sphere);

		if(scene.box){
			scene.remove(box);
			box = null;
		}



	}



