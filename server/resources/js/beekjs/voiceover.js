/**
 * Created by Ben on 25/01/2017.
 */

var AudioContext = AudioContext || webkitAudioContext, context = new AudioContext();

function createSound(voiceover) {
    console.log('createSound(' + voiceover.filename + ')');

    var url = cdnPrefix + '/' + voiceover.filename;
    var buffer;

    var request = new XMLHttpRequest();
    request.open('GET', url, true);
    request.responseType = 'arraybuffer';

    // Decode asynchronously
    request.onload = function() {
        context.decodeAudioData(request.response, function(b) {
            buffer = b;

            if(voiceover.startTime)
                pausedAt = voiceover.startTime;

           play();
        });
    }
    request.send();

    var sourceNode = null,
        startedAt = 0,
        pausedAt = 0,
        playing = false,
        volume = context.createGain();



    var play = function() {
        var offset = pausedAt;

        sourceNode = context.createBufferSource();
        sourceNode.connect(context.destination);
        sourceNode.connect(volume);
        volume.gain.value = 1;
        sourceNode.buffer = buffer;
        sourceNode.start(0, offset);
        sourceNode.onended = onEnded;
        sourceNode.onStateChange = onStateChange;
        //sourceNode.loop = true;
        startedAt = context.currentTime - offset;
        pausedAt = 0;
        playing = true;
         $(document).trigger("voiceoverPlay");
    };

    function onEnded(event){
        $(document).trigger("voiceoverEnded");
        //play();
    }

    function onStateChange(event){
        console.log(event.state);
    }

    var pause = function() {
        var elapsed = context.currentTime - startedAt;
        stop();
        pausedAt = elapsed;
        $(document).trigger("voiceoverPause");
    };

    var stop = function() {
        if (sourceNode) {
            sourceNode.disconnect();
            sourceNode.stop(0);
            sourceNode = null;
        }
        pausedAt = 0;
        startedAt = 0;
        playing = false;
    };

    var getPlaying = function() {
        return playing;
    };

    var getCurrentTime = function() {
        if(pausedAt) {
            return pausedAt;
        }
        if(startedAt) {
            return context.currentTime - startedAt;
        }
        return 0;
    };

    var setCurrentTime = function(time) {
        pausedAt = time;
    };

    var getDuration = function() {
        if(buffer)
         return buffer.duration;
        else
            return null;
    };

    return {
        getCurrentTime: getCurrentTime,
        setCurrentTime: setCurrentTime,
        getDuration: getDuration,
        getPlaying: getPlaying,
        play: play,
        pause: pause,
        stop: stop
    };
}

