/**
 * Created by Ben on 19/09/2015.
 */
google.load('visualization', '1', {'packages': ['geomap', 'corechart']});


function drawMap(feed) {

    var data = new google.visualization.DataTable();
    data.addColumn('string', 'City');
    data.addColumn('number', 'Visits');
    data.addColumn({type:'string', role:'tooltip', 'p': {'html': true}});
    data.addRows(feed);

    var options = {
        sizeAxis: { minValue: 0, maxValue: 100 },
        displayMode: 'markers',
        colorAxis: {colors: ['#33ADFF', '#003D66']},
        width : '100%',
        height : '600px',
        enableRegionInteractivity: false,
        keepAspectRatio: true,
        legend : 'none',
        tooltip: {isHtml: true}
    };

    var container = document.getElementById('report_map');
    var geomap = new google.visualization.GeoChart(container);
    geomap.draw(data, options);
};




function drawChart(title, metric,  data, div) {

    var chartData = new google.visualization.DataTable();
    chartData.addColumn('string', title);
    chartData.addColumn('number', metric);
    chartData.addRows(data);

    var options = {
        title: title,
        sliceVisibilityThreshold: 0.01,
        chartArea:{left:0,top:0,width:'100%'}
        // is3D: true,
        // tooltip: {isHtml: true}
    };

    var chart = new google.visualization.PieChart(document.getElementById(div));

    chart.draw(chartData, options);
}