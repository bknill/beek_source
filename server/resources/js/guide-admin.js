var selectedTreeNode = "";
var dialogMode = "";
var data = {};
var scenes = {};
var guideId;
function showRootTreeNodeChilds(){
    //var rootTreeNode = $('#t1').find('td[data-nodetype="root"]');
    //PF('treeWidget').showNodeChildren($(rootTreeNode));
    bindTreeNodeClickEvent();
}
function addScrollingRegions(){
    $("#centerUnit").find("> :first-child").after('<div id="scrollRegionTop" style="opacity: 0; background-color: red;width: 100%; height: 20px;z-index:5;position:absolute;left:0px"/>');
    $("#centerUnit").append('<div id="scrollRegionBottom" style="opacity: 0;background-color: red;width: 100%; height: 20px;z-index:5;position:absolute;bottom:0px;"/>');
}
function bindTreeNodeClickEvent(){
    var c = ".ui-treenode-content";
    PF('treeWidget').jq.off("contextmenu.tree-content", c).on("contextmenu.tree-content", c, null, function (event) {
        handleTreeNodeClick($(this).parent(),event);
    });
}
function handleUpdateScenes(scenesJSON){
    scenes = scenesJSON;
    initTableNodeDragging();
}


function loadPreview(id){
    console.log(id);
}



function loadGuide(id, json, host){
    if(json === undefined ){
        if(id !== undefined && id !== ""
                && host !== undefined && host !== ""){
            $.getJSON('http://'+host+'/guide/'+id+'/json', function(response) {
                data = response.payload;
                createGuide(data);
            });
        }
    }else{
        data = json.payload;
    }

}



/*function saveGuide(id, host){

    refreshData();

    $.post(
            'http://'+host+'/guide/'+id+'/save',
            {json: JSON.stringify(data)},null,"json")
    .done(function (result) {        
        if(result.error > 0){
            PF('growl').renderMessage({
                "summary": "Failed",
                "detail": "Error occurred while saving guide",
                "severity": "error"
            });
        }else{
            PF('growl').renderMessage({
                "summary":"Successful",
                "detail":"Guide Successfully Saved",
                "severity":"info"
            });
        }
        PF('blockUI').hide();
    }).fail(function () {
            PF('growl').renderMessage({
            "summary": "Failed",
            "detail": "Error occurred while saving guide",
            "severity": "error"
        });
        PF('blockUI').hide();
    });
}*/
function addGuideSection(sectionId, sectionName, parentSectionId){
    try{
        if (sectionName !== undefined
                && sectionId !== undefined && sectionId !== "") {
            
            if(parentSectionId === undefined || parentSectionId === ""){
                parentSectionId = null;
            }
            
            var guideSection = {
                "guideId": data.id,
                "guideScenes": [],
                "parent_section_id": parentSectionId,
                "title": sectionName,
                "id": sectionId,
                order:'999'
            };
            data.guideSections.push(guideSection);

            $('#hidden\\:newSection').val(JSON.stringify(guideSection));
            addNewSection();



        }else{
            return false;
        }      
    }catch(e){
        return false;
    }
    return true;
}
function editGuideSection(id, sectionName){
    try{
        var guideSections = data.guideSections;
        if (id !== undefined && id !== ""
                && sectionName !== undefined
                && guideSections !== undefined) {
            
            $.each(guideSections, function (index, guideSection) {       
                if (id === guideSection.id) {
                    guideSection.title = sectionName;
                    $('#hidden\\:newSection').val(JSON.stringify(guideSection));
                    editSection();

                    return false;
                }
            });
            
        }else{
            return false;
        }
    }catch(e){
        return false;
    }
    return true;    
}
function deleteGuideSection(id){
    var guideSections = data.guideSections;

    guideSections.forEach(function (guideSection) {
        if(guideSection.id == id){
            $(' #hidden\\:newSection').val(simpleStringify(guideSection));
            deleteSection();

        }
    });
}

function simpleStringify (object){
    var simpleObject = {};
    for (var prop in object ){
        if (!object.hasOwnProperty(prop)){
            continue;
        }
        if (typeof(object[prop]) == 'object'){
            continue;
        }
        if (typeof(object[prop]) == 'function'){
            continue;
        }
        simpleObject[prop] = object[prop];
    }
    return JSON.stringify(simpleObject); // returns cleaned up JSON
};

function deleteGuideScene(id) {
    console.log("deleteGuideScene(" + id + ")");
    var guideSections = data.guideSections;

    if (id !== undefined && id !== ""
        && guideSections !== undefined) {
        var found = false;
        $.each(guideSections, function (index, guideSection) {
                var guideScenes = guideSection.guideScenes;
                $.each(guideScenes, function (index, guideScene) {
                    console.log(guideScene);
                    if(guideScene != null)
                        if (id === guideScene.id) {
                            console.log('matches');
                            guideScenes.splice(index, 1);
                            $(' #hidden\\:newScene').val(JSON.stringify(guideScene));
                            deleteScene();
                            found = true;
                            return false;
                        }
                });
                if(found === true)return false;
            }
        );

    }
}

function setSelectedNode(node){
    console.log('setSelectedNode');
    selectedTreeNode = node;
}

function guideSceneMoveFromTree(newGuideSceneId, oldGuideSceneId, oldParentSectionId, newParentSectionId){
    var guideSections = data.guideSections;
    try{
        if (newGuideSceneId !== undefined && newGuideSceneId !== ""
                && oldGuideSceneId !== undefined && oldGuideSceneId !== ""
                && oldParentSectionId !== undefined && oldParentSectionId !== ""
                && newParentSectionId !== undefined && newParentSectionId !== ""
                && guideSections !== undefined) {            
            
            var guideSection = undefined;
            var guideScene = undefined;
                            var done = false;

                            for (var i = 0; i < guideSections.length; i++) {
                                if (oldParentSectionId === guideSections[i].id) {
                                    var guideScenes = guideSections[i].guideScenes;
                                    for (var j = 0; j < guideScenes.length; j++) {
                                        if (oldGuideSceneId === guideScenes[j].id) {
                                            if (guideSection !== undefined) {
                                                //guideScenes[j].id = newGuideSceneId;
                                                guideScenes[j].guideSectionId = guideSection.id;
                                                guideScenes[j].order = guideSection.guideScenes.length;
                                                guideSection.guideScenes.push(guideScenes[j]);
                                                updateSceneMove([{name: 'oldParentSectionId', value: oldParentSectionId}, {name: 'scene', value: JSON.stringify(guideScenes[j])}]);
                                                done = true;
                                            } else {
                                                guideScene = guideScenes[j];
                                            }
                            guideScenes.splice(j, 1);
                            break;
                        }
                    }
                } else if (newParentSectionId === guideSections[i].id) {
                    if (guideScene !== undefined) {
                        guideScene.id = newGuideSceneId;
                        guideScene.guideSectionId = newParentSectionId;
                        guideScene.order = guideSections[i].guideScenes.length;
                        guideSections[i].guideScenes.push(guideScene);
                        updateSceneMove([{name: 'oldParentSectionId', value: oldParentSectionId}, {name: 'scene', value: JSON.stringify(guideScene)}]);
                        done = true;

                    } else {
                        guideSection = guideSections[i];
                    }
                }
                if(done === true) break;
            }
        }
    }catch(e){
        return false;
    }
    return true;
}
function printIt(){
    $.each(data.guideSections,function(i,section){
         console.log(section.title + " = " +section.order);
         $.each(section.guideScenes,function(i,scene){
            console.log('    '+scene.titleMerged + " = "+ scene.order);
        });
    });
}
///////////////////////////////////////////////////////////////////////////////
function handleTreeNodeClick(node,event){
    var $node = node;
    var sceneId = $node.find(".scene-id").val();

/*    if(sceneId)
        getScene(sceneId);*/

    selectedTreeNode = node;
}
function handleTreeNodeAddClick(){

    if(selectedTreeNode === "") return false;
    dialogMode = "add";
        
    PF('dialog').titlebar.children('span.ui-dialog-title').html('Add New Section');
    $("#name").val('');
    PF('dialog').show();
}
function handleTreeNodeEditClick(){    
    if(selectedTreeNode === "") return false;    
    dialogMode = "edit";    
    
    PF('dialog').titlebar.children('span.ui-dialog-title').html('Edit Section');
    $("#name").val(selectedTreeNode.find(".section-node, .scene-node").text());
    PF('dialog').show();
}
function handleTreeNodeDeleteClick(event){
    console.log(selectedTreeNode);
    if(selectedTreeNode === "") return false;    

    PF('confirmDialog').icon[0].setAttribute('class', PF('confirmDialog').icon[0].getAttribute('class') + ' ui-icon-alert');
    PF('confirmDialog').title[0].innerHTML = event.target.innerHTML;
    PF('confirmDialog').show();
}
function onAdd(){

    var sectionName = $('#name').val();
    if(sectionName === '')
        return;
    var source = selectedTreeNode.children().first();
    var parentSectionId = $(source).find(".section-node-id").val();
    
    var sectionId = uuid.v4();

    var targetGreatGrandParent = $(source).parents(".ui-treenode-children-container").eq(2).prev();
    if ($(targetGreatGrandParent).length !== 0) {
        PF('dialog').hide();
        return;
    }

    if(addGuideSection(sectionId, sectionName, parentSectionId) === true){
        insertTreeNode(sectionId, sectionName, null, source, true);
        initTreeNodeDropping();
        initTreeNodeDragging();

    }
    
    PF('dialog').hide();
    selectedTreeNode = "";
}
function onEdit(){

    var sectionName = $('#name').val();
    if(sectionName === '')
        return;
    var source = selectedTreeNode.children().first();
    var sectionId = $(source).find(".section-node-id").val();
    if(editGuideSection(sectionId, sectionName) === true){
        editTreeNode(sectionName, source);
    }
    
    PF('dialog').hide();
    selectedTreeNode = "";
}
function onDelete(){
    var source = selectedTreeNode.children().first();

    if($(source).find(".section-node-id").length !== 0){    
        var sectionId = $(source).find(".section-node-id").val();

        deleteGuideSection(sectionId);
            removeTreeNode(source);

    }else{        
        var sourceParent = $(source)
            .parents(".ui-treenode-children-container")
            .eq(0)
            .prev();
        var sceneId = $(source).find(".scene-node-id").val();
        deleteGuideScene(sceneId);
            removeTreeNode(source);

    }
    
    PF('confirmDialog').hide();
    selectedTreeNode = "";
    //printIt();
}
//////////////////////////////////////////////////////////////////////////////////////
function initGuideNodeDragging(){
    if($('.guide-item').length === 0){
        return;
    }
    $('.guide-item').draggable({
        helper: function (event) {
            var node = $(this);
            return node
                .attr('id', 'guide-from-table')
                .clone()
                .appendTo(document.body)
                .css('padding', 5);
        },
        start: function(){
        },
        stop: function(){
        }});
}
function initTableNodeDragging() {
    $('.ui-datagrid .scene-row').draggable({
        helper: function (event) {
            var node = $(this).parent().parent();
            return node
                    .attr('id', 'scene-from-table')
                    .clone()
                    .appendTo(document.body)
                    .css('padding', 5);
        },
    start: function(){
        $(this).css({opacity:0});
    },
    stop: function(){
        $(this).css({opacity:1});
    }});
}
function initTreeNodeDragging() {
    var parentNodeId = null;
    $('.section-node').parent().draggable({
        cursorAt: {left: -3, top: -3},
        start: function( event, ui ) {
            startAnimateTreeScrolling();
            parentNodeId = $(this).find(".section-parent-node-id").val();
            $(' .section-'+parentNodeId+','+'.scene-section-'+parentNodeId).parent().droppable({
                greedy: true,
                helper: 'clone',
                accept: '.section-node, .ui-treenode-content',
                activeClass: 'ui-state-active',
                hoverClass: 'ui-state-highlight',
                tolerance: 'pointer',
                drop: function (event, ui) {
                    var target = event.target;
                    var source = ui.draggable;
                    var sourceId = ui.helper.attr('id');
                    if (sourceId === undefined || sourceId === "")
                        return;
                    if (sourceId === 'section-from-tree') {
                        var sourceTable = $(source).parents("table").eq(0)[0];
                        var targetTable = $(target).parents("table").eq(0)[0];
                        if($(sourceTable).prev().is(targetTable) || $(sourceTable).next().is(targetTable)){
                            return;
                        }
                        updateGuideSectionTreeNodeOrder(source, target);
                        updateGuideSectionOrder(source);
                    }
                }
            });
        },
        stop: function( event, ui ) {
            stopAnimateTreeScrolling();
            $('.section-'+parentNodeId+','+'.scene-section-'+parentNodeId).parent().droppable('destroy');
        },
        helper: function (event) {
            var node = $(this).clone();
            node.children().first().remove();
            return node
                .attr('id', 'section-from-tree')
                .appendTo(document.body)
                .css('font-weight','normal')
                .css('padding', 4);
        }
    });
    $('.scene-node').parent().draggable({
        cursorAt: {left: -3, top: -3},
        start: function( event, ui ) {
            startAnimateTreeScrolling();
            parentNodeId = $(this).find(".scene-parent-node-id").val();
            $('.scene-'+parentNodeId+','+'.scene-section-'+parentNodeId).parent().droppable({
                greedy: true,
                helper: 'clone',
                accept:'.section-node, .ui-treenode-content',
                activeClass: 'ui-state-active',
                hoverClass: 'ui-state-highlight',
                tolerance: 'pointer',
                drop: function (event, ui) {
                    var target = event.target;
                    var source = ui.draggable;
                    var sourceId = ui.helper.attr('id');
                    if (sourceId === undefined || sourceId === "")
                        return;
                    if (sourceId === 'scene-from-tree') {
                        var sourceTable = $(source).parents("table").eq(0)[0];
                        var targetTable = $(target).parents("table").eq(0)[0];
                        if($(sourceTable).prev().is(targetTable) || $(sourceTable).next().is(targetTable)){
                            return;
                        }
                        updateGuideSceneTreeNodeOrder(source, target);
                        updateGuideSceneOrder(source);
                    }
                }
            });
        },
        stop: function( event, ui ) {
            stopAnimateTreeScrolling();
            $('.scene-'+parentNodeId+','+'.scene-section-'+parentNodeId).parent().droppable('destroy');
        },
        helper: function (event) {
            var node = $(this).clone();
            return node
                    .attr('id', 'scene-from-tree')
                    .appendTo(document.body)
                    .css('font-weight','normal')
                    .css('padding', 4);
        }
    });
}
function startAnimateTreeScrolling(){
    var content = $("#centerUnit .ui-layout-unit-content");
    var containerHeight = PF('treeWidget').jq.height();
    var duration = containerHeight/0.5;
    $('#scrollRegionTop').hover(
        function(event){content.animate({scrollTop: 0}, duration, "linear");},
        function(event){content.stop();}
    );
    $('#scrollRegionBottom').hover(
        function(event){content.animate({scrollTop: containerHeight}, duration, "linear");},
        function(event){content.stop();}
    );
}
function stopAnimateTreeScrolling(){
    $('#scrollRegionTop').unbind();
    $('#scrollRegionBottom').unbind();
    $("#centerUnit .ui-layout-unit-content").stop();
}
function updateGuideSectionTreeNodeOrder(source, target){
    var isTargetFirstElement = ($(target).parents("table").eq(0).prev().length == 0) || ($(target).parents("table").eq(0).prev().find(".section-node-id").length == 0);
    var dropRegion = null;

    if($(source).parents("table").eq(0).next().next().length === 0) {
        dropRegion = $(source).parents("table").eq(0).prev();
    }else{
        dropRegion = $(source).parents("table").eq(0).next();
    }
    var sectionNode = $(source).parents("table").eq(0);
    if(isTargetFirstElement){
        $(sectionNode).insertAfter($(target).parents("table").eq(0));
        $(dropRegion).insertAfter($(sectionNode));
    }else{
        $(sectionNode).insertBefore($(target).parents("table").eq(0));
        $(dropRegion).insertBefore($(sectionNode));
    }
}
function updateGuideSceneTreeNodeOrder(source, target){
    var isTargetFirstElement = $(target).parents("table").eq(0).prev().length == 0;
    var dropRegion = null;

    if($(source).parents("table").eq(0).prev().prev().length === 0) {
        dropRegion = $(source).parents("table").eq(0).next();
    }else{
        dropRegion = $(source).parents("table").eq(0).prev();
    }
    var sceneNode = $(source).parents("table").eq(0);
    if(isTargetFirstElement){
        $(sceneNode).insertAfter($(target).parents("table").eq(0));
        $(dropRegion).insertAfter($(sceneNode));
    }else{
        $(sceneNode).insertBefore($(target).parents("table").eq(0));
        $(dropRegion).insertBefore($(sceneNode));
    }
}

function updateGuideSectionOrder(source){
    var guideSections = data.guideSections;
    var guideSectionParentId = $(source).find(".section-parent-node-id").val();
    var subsetSections = $(source).parents("div").eq(0).find("input[value="+guideSectionParentId+"].section-parent-node-id").next();
    var lookup = {};
    for(var i = 0; i < subsetSections.length; i++){
        lookup[$(subsetSections[i]).val()] = i;
    }
    console.log(lookup);
    $('#updatedSectionOrder').val(JSON.stringify(lookup));
    updateSectionOrder();
}

function updateGuideSceneOrder(source){
    console.log('updateGuideSceneOrder');
    var guideSections = data.guideSections;
    var guideSceneParentId = $(source).find(".scene-parent-node-id").val();
    var subsetScenes = $(source).parents("div").eq(0).find(".scene-node-id");
    var lookup = {};
    for(var i = 0; i < subsetScenes.length; i++)
        lookup[$(subsetScenes[i]).val()] = i;

    $('#updatedSceneOrder').val(JSON.stringify(lookup));
    updateSceneOrder();
}


function initTreeNodeDropping(){
    $('.section-node').parent().droppable({
        greedy: true,
        helper: 'clone',
        accept: function(draggable) {
            return ($(draggable).find(".scene-node").length > 0) || ($(draggable).hasClass("scene-row") || $(draggable).attr('id') === "guide-from-table");
        },
        activeClass: 'ui-state-active',
        hoverClass: 'ui-state-highlight',
        tolerance: 'pointer',
        drop: function (event, ui) {
            var targetElement = event.target;
            var sourceElement = ui.draggable;
            var sourceElementId = ui.helper.attr('id');
            if (sourceElementId === undefined || sourceElementId === "")
                return;
            if (sourceElementId === 'scene-from-table') {
                handleSceneDropFromTable(sourceElement, targetElement);
            } else if (sourceElementId === 'scene-from-tree') {
                handleSceneDropFromTree(sourceElement, targetElement);
            } else if (sourceElementId === 'guide-from-table') {
                handleGuideDropFromTable(sourceElement, targetElement);
            }
        }
    });
}
function handleGuideDropFromTable(source, target){
    var id = uuid.v4();
    var childGuideId = $(source).find(".guide-item-id").val();
    var guideName = $(source).find(".guide-item-name").val();
    var thumb = $(source).find('img').attr('src');
    var sectionId = $(target).find(".section-node-id").val();

    var childContainer = $(target).parent().next();
    if (childContainer.length > 0) {
        var childGuideSection = childContainer.find("> .ui-treenode-children > table > tbody > tr ").find('>td[data-nodetype = child-guide-root-section]');
        if(childGuideSection.length > 0){
            return;
        }
    }
    updateGuideSection([{name: 'childGuideId', value: childGuideId}, {name: 'sectionId', value: sectionId}]);
}

function onUpdateGuideSectionComplete(xhr, status, args){
    if(status == 'success'){
        if(args.child_guide_id !== undefined && args.section_id !== undefined){

            var childGuideId = args.child_guide_id;
            var sectionId = args.section_id;
            var guideSections = data.guideSections;
            for (var i = 0; i < guideSections.length; i++) {
                if (sectionId === guideSections[i].id) {
                    guideSections[i].childGuideId = childGuideId;
                    break;
                }
            }
        }
    }
    initTreeNodeDragging();
    initTreeNodeDropping();
    bindTreeNodeClickEvent();
    PF('blockTree').hide();
}
function handleSceneDropFromTable(source, target) {
    var id = uuid.v4();
    var sceneId = source.attr('id');
    var sceneName = source.text().trim();
    var thumb = source.children('img').attr('src');
    var sectionId = $(target).find(".section-node-id").val();
    if(guideSceneMoveFromTable(id, sceneId, sceneName, sectionId)){
        insertTreeNode(id, sceneName, thumb, target, false);
       $(source).remove();
        initTreeNodeDragging();
        
    }
    //printIt();
    PF('treeWidget').showNodeChildren($(target).parent());

}
function guideSceneMoveFromTable(id, sceneId, sceneName, sectionId){
    var guideSections = data.guideSections;

    console.log("guideSceneMoveFromTable",id,sceneId,sceneName,sectionId,guideSections,scenes);

    try{
        if (id !== undefined && id !== ""
            && sceneId !== undefined && sceneId !== ""
            && sceneName !== undefined && sceneName !== ""
            && sectionId !== undefined && sectionId !== ""
            && guideSections !== undefined) {


            for (var i = 0; i < guideSections.length; i++) {
                if (sectionId === guideSections[i].id) {
                    for (var j = 0; j < scenes.length; j++) {
                        if (sceneId === scenes[j].id) {
                            var guideScene = {
                                "rssReaders": [],
                                "photos": [],
                                "guideSectionId": sectionId,
                                "scene": scenes[j],
                                "id": id,
                                "titleMerged":sceneName,
                                "videos": [],
                                "posters": [],
                                "order": 999,
                                "sceneId": scenes[j].id,
                                "sounds": []
                            };
                            guideSections[i].guideScenes.push(guideScene);

                            console.log(guideScene);
                            $('#hidden\\:newScene').val(JSON.stringify(guideScene));
                            addNewScene();
                            break;
                        }
                    }
                    break;
                }
            }


        }
    }catch(e){
        return false;
    }
    return true;
}
function handleSceneDropFromTree(source, target) {
    var sourceParent = $(source)
            .parents(".ui-treenode-children-container")
            .eq(0)
            .prev();

    if ($(target).parent()[0] === $(sourceParent)[0]) {
        return;
    } else { 
        
        var oldParentSectionId = $(sourceParent).children().first().find(".section-node-id").val();
        var newParentSectionId = $(target).find(".section-node-id").val();   
        var oldGuideSceneId = $(source).find(".scene-node-id").val();
        var newGuideSceneId = uuid.v4();
        if(guideSceneMoveFromTree(newGuideSceneId, oldGuideSceneId, oldParentSectionId, newParentSectionId) === true){
            var thumb = source.children('img').attr('src');
            insertTreeNode(oldGuideSceneId, source.text().trim(),thumb, target, false);
            removeTreeNode(source);
            initTreeNodeDragging();
        }     
        //printIt();
        PF('treeWidget').showNodeChildren($(target).parent());
    }
}
function insertTreeNode(id, name, thumb, target, isSection) {
	console.log(thumb);
    if(isSection){
        var targetGreatGrandParent = $(target)
                .parents(".ui-treenode-children-container")
                .eq(2)
                .prev();
        if ($(targetGreatGrandParent).length !== 0) {
            return false;
        }
    }
    var childContainer = $(target).parent().next();
    var parentNodeId = $(target).find(".section-node-id").val();
    var dropTargetUp =   null;
    var dropTargetDown = null;

    if (childContainer.size() === 0) {
        $(target).parent().removeClass('ui-treenode-leaf');
        $(target).parent().addClass('ui-treenode-parent');
        $(target).prepend('<span class="ui-tree-toggler ui-icon ui-icon-minus"></span>');

        if(isSection){
            dropTargetUp =   '<table><tbody><tr><td class="ui-treenode-connector"><table class="ui-treenode-connector-table"><tbody><tr><td></td></tr><tr><td class="ui-treenode-connector-line"></td></tr></tbody></table></td><td class="ui-treenode ui-treenode-leaf ui-treenode-unselected" data-nodetype="dropRegion" data-rowkey="1_5_9_0"><div class="ui-treenode-content ui-tree-selectable ui-state-default ui-corner-all"><span></span><div class="section-'+ parentNodeId +'"></div></div></td></tr></tbody></table>';
            dropTargetDown = '<table><tbody><tr><td class="ui-treenode-connector"><table class="ui-treenode-connector-table"><tbody><tr><td class="ui-treenode-connector-line"></td></tr><tr><td></td></tr></tbody></table></td><td class="ui-treenode ui-treenode-leaf ui-treenode-unselected" data-nodetype="dropRegion" data-rowkey="1_5_9_2"><div class="ui-treenode-content ui-tree-selectable ui-state-default ui-corner-all"><span></span><div class="section-'+ parentNodeId +'"></div></div></td></tr></tbody></table>';

            if(thumb == null){
                $(target).parent().parent()
                    .append('<td class="ui-treenode-children-container" style=""><div class="ui-treenode-children">' +
                    dropTargetUp +
                    '<table><tbody><tr><td class="ui-treenode-connector"><table class="ui-treenode-connector-table"><tbody><tr><td class="ui-treenode-connector-line"></td></tr><tr><td class="ui-treenode-connector-line"></td></tr></tbody></table></td><td class="ui-treenode ui-treenode-leaf ui-treenode-unselected" data-nodetype="section"><div class="ui-treenode-content ui-tree-selectable ui-state-default ui-corner-all ui-droppable"><span></span><input type="hidden" class="section-parent-node-id" value="'+ parentNodeId +'"></input><input type="hidden" class="section-node-id" value="' + id + '"></input><span class="section-node">' + name + '</span></div></td></tr></tbody></table>' +
                    dropTargetDown +
                    '</div></td>');
            }else{
                $(target).parent().parent()
                    .append('<td class="ui-treenode-children-container" style=""><div class="ui-treenode-children">' +
                    dropTargetUp +
                    '<table><tbody><tr><td class="ui-treenode-connector"><table class="ui-treenode-connector-table"><tbody><tr><td class="ui-treenode-connector-line"></td></tr><tr><td class="ui-treenode-connector-line"></td></tr></tbody></table></td><td class="ui-treenode ui-treenode-leaf ui-treenode-unselected" data-nodetype="section"><div class="ui-treenode-content ui-tree-selectable ui-state-default ui-corner-all ui-droppable"><span></span><input type="hidden" class="section-parent-node-id" value="'+ parentNodeId +'"></input><input type="hidden" class="section-node-id" value="' + id + '"></input><img src="'+ thumb +'" width="50" height="50"/><span class="section-node">' + name + '</span></div></td></tr></tbody></table>' +
                    dropTargetDown +
                    '</div></td>');
            }
        }else {
            dropTargetUp =   '<table><tbody><tr><td class="ui-treenode-connector"><table class="ui-treenode-connector-table"><tbody><tr><td></td></tr><tr><td class="ui-treenode-connector-line"></td></tr></tbody></table></td><td class="ui-treenode ui-treenode-leaf ui-treenode-unselected" data-nodetype="dropRegion" data-rowkey="1_5_9_0"><div class="ui-treenode-content ui-tree-selectable ui-state-default ui-corner-all"><span></span><div class="scene-'+ parentNodeId +'"></div></div></td></tr></tbody></table>';
            dropTargetDown = '<table><tbody><tr><td class="ui-treenode-connector"><table class="ui-treenode-connector-table"><tbody><tr><td class="ui-treenode-connector-line"></td></tr><tr><td></td></tr></tbody></table></td><td class="ui-treenode ui-treenode-leaf ui-treenode-unselected" data-nodetype="dropRegion" data-rowkey="1_5_9_2"><div class="ui-treenode-content ui-tree-selectable ui-state-default ui-corner-all"><span></span><div class="scene-section-'+ parentNodeId +'"></div></div></td></tr></tbody></table>';

            if(thumb == null){
                $(target).parent().parent()
                    .append('<td class="ui-treenode-children-container" style=""><div class="ui-treenode-children">' +
                    dropTargetUp +
                    '<table><tbody><tr><td class="ui-treenode-connector"><table class="ui-treenode-connector-table"><tbody><tr><td class="ui-treenode-connector-line"></td></tr><tr><td class="ui-treenode-connector-line"></td></tr></tbody></table></td><td class="ui-treenode ui-treenode-leaf ui-treenode-unselected" data-nodetype="scene"><div class="ui-treenode-content ui-tree-selectable ui-state-default ui-corner-all ui-draggable"><span></span><input type="hidden" class="scene-parent-node-id" value="'+ parentNodeId +'"></input><input type="hidden" class="scene-node-id" value="' + id + '"></input><span class="scene-node">' + name + '</span></div></td></tr></tbody></table>' +
                    dropTargetDown +
                    '</div></td>');
            }else{
                $(target).parent().parent()
                    .append('<td class="ui-treenode-children-container" style=""><div class="ui-treenode-children">' +
                        dropTargetUp +
                        '<table><tbody><tr><td class="ui-treenode-connector"><table class="ui-treenode-connector-table"><tbody><tr><td class="ui-treenode-connector-line"></td></tr><tr><td class="ui-treenode-connector-line"></td></tr></tbody></table></td><td class="ui-treenode ui-treenode-leaf ui-treenode-unselected" data-nodetype="scene"><div class="ui-treenode-content ui-tree-selectable ui-state-default ui-corner-all ui-draggable"><span></span><input type="hidden" class="scene-parent-node-id" value="'+ parentNodeId +'"></input><input type="hidden" class="scene-node-id" value="' + id + '"></input><img src="'+ thumb +'" width="50" height="50"/><span class="scene-node">' + name + '</span></div></td></tr></tbody></table>' +
                        dropTargetDown +
                    '</div></td>');
            }
        }
    } else if (childContainer.size() === 1) {
        if (childContainer.find("> .ui-treenode-children > table").size() === 1) {
        } else {
            var connectorNode = null;

            if(isSection){
                if(parentNodeId === undefined){
                    parentNodeId = "root";
                }
                dropTargetDown = '<table><tbody><tr><td class="ui-treenode-connector"><table class="ui-treenode-connector-table"><tbody><tr><td class="ui-treenode-connector-line"></td></tr><tr><td></td></tr></tbody></table></td><td class="ui-treenode ui-treenode-leaf ui-treenode-unselected" data-nodetype="dropRegion" data-rowkey="1_5_9_2"><div class="ui-treenode-content ui-tree-selectable ui-state-default ui-corner-all"><span></span><div class="section-'+ parentNodeId +'"></div></div></td></tr></tbody></table>';
                connectorNode = childContainer.find("> .ui-treenode-children > table").last();
                connectorNode
                    .find(".ui-treenode-connector-table")
                    .eq(0)
                    .find(" > tbody > tr")
                    .last()
                    .replaceWith('<tr><td class="ui-treenode-connector-line"></td></tr>');

                if(thumb == null){
                    childContainer
                        .find("> .ui-treenode-children")
                        .append('<table><tbody><tr><td class="ui-treenode-connector"><table class="ui-treenode-connector-table"><tbody><tr><td class="ui-treenode-connector-line"></td></tr><tr><td class="ui-treenode-connector-line"></td></tr></tbody></table></td><td class="ui-treenode ui-treenode-leaf ui-treenode-unselected" data-nodetype="section"><div class="ui-treenode-content ui-tree-selectable ui-state-default ui-corner-all ui-droppable"><span></span><input type="hidden" class="section-parent-node-id" value="'+ parentNodeId +'"></input><input type="hidden" class="section-node-id" value="' + id + '"></input><span class="section-node">' + name + '</span></div></td></tr></tbody></table>' +dropTargetDown);
                }else{
                    childContainer
                        .find("> .ui-treenode-children")
                        .append('<table><tbody><tr><td class="ui-treenode-connector"><table class="ui-treenode-connector-table"><tbody><tr><td class="ui-treenode-connector-line"></td></tr><tr><td class="ui-treenode-connector-line"></td></tr></tbody></table></td><td class="ui-treenode ui-treenode-leaf ui-treenode-unselected" data-nodetype="section"><div class="ui-treenode-content ui-tree-selectable ui-state-default ui-corner-all ui-droppable"><span></span><input type="hidden" class="section-parent-node-id" value="'+ parentNodeId +'"></input><input type="hidden" class="section-node-id" value="' + id + '"></input><img src="'+ thumb +'" width="50" height="50"/><span class="section-node">' + name + '</span></div></td></tr></tbody></table>' +dropTargetDown);
                }
            }else {
                if(thumb == null){
                    //childContainer
                    //    .find("> .ui-treenode-children")
                    //    .append('<table><tbody><tr><td class="ui-treenode-connector"><table class="ui-treenode-connector-table"><tbody><tr><td class="ui-treenode-connector-line"></td></tr><tr><td class="ui-treenode-connector-line"></td></tr></tbody></table></td><td class="ui-treenode ui-treenode-leaf ui-treenode-unselected" data-nodetype="scene"><div class="ui-treenode-content ui-tree-selectable ui-state-default ui-corner-all ui-draggable"><span></span><input type="hidden" class="scene-parent-node-id" value="'+ parentNodeId +'"></input><input type="hidden" class="scene-node-id" value="' + id + '"></input><span class="scene-node">' + name + '</span></div></td></tr></tbody></table>'+dropTargetDown);
                }else{
                    var firstSection = childContainer.find("> .ui-treenode-children > table > tbody > tr ").find('> td[data-nodetype = section],>td[data-nodetype = child-guide-root-section]').first();
                    if(firstSection.length > 0){
                        var firstSectionTable = $(firstSection).parents("table").eq(0);
                        $(firstSectionTable).prev().find(" > tbody > tr > td").last().find(" > div > div").attr("class","scene-"+parentNodeId);

                        dropTargetDown = '<table><tbody><tr><td class="ui-treenode-connector"><table class="ui-treenode-connector-table"><tbody><tr><td class="ui-treenode-connector-line"></td></tr><tr><td class="ui-treenode-connector-line"></td></tr></tbody></table></td><td class="ui-treenode ui-treenode-leaf ui-treenode-unselected" data-nodetype="dropRegion" data-rowkey="1_5_9_2"><div class="ui-treenode-content ui-tree-selectable ui-state-default ui-corner-all"><span></span><div class="scene-section-'+ parentNodeId +'"></div></div></td></tr></tbody></table>';
                        $(firstSectionTable).before('<table><tbody><tr><td class="ui-treenode-connector"><table class="ui-treenode-connector-table"><tbody><tr><td class="ui-treenode-connector-line"></td></tr><tr><td class="ui-treenode-connector-line"></td></tr></tbody></table></td><td class="ui-treenode ui-treenode-leaf ui-treenode-unselected" data-nodetype="scene"><div class="ui-treenode-content ui-tree-selectable ui-state-default ui-corner-all ui-draggable"><span></span><input type="hidden" class="scene-parent-node-id" value="'+ parentNodeId +'"></input><input type="hidden" class="scene-node-id" value="' + id + '"></input><span class="scene-node">' + name + '</span><br/><img src="'+ thumb +'" width="50" height="50"/></div></td></tr></tbody></table>'+dropTargetDown);
                    }else{
                        connectorNode = childContainer.find("> .ui-treenode-children > table").last();
                        connectorNode.find(" > tbody > tr > td").last().find(" > div > div").attr("class","scene-"+parentNodeId);
                        connectorNode
                            .find(".ui-treenode-connector-table")
                            .eq(0)
                            .find(" > tbody > tr")
                            .last()
                            .replaceWith('<tr><td class="ui-treenode-connector-line"></td></tr>');

                        dropTargetDown = '<table><tbody><tr><td class="ui-treenode-connector"><table class="ui-treenode-connector-table"><tbody><tr><td class="ui-treenode-connector-line"></td></tr><tr><td></td></tr></tbody></table></td><td class="ui-treenode ui-treenode-leaf ui-treenode-unselected" data-nodetype="dropRegion" data-rowkey="1_5_9_2"><div class="ui-treenode-content ui-tree-selectable ui-state-default ui-corner-all"><span></span><div class="scene-section-'+ parentNodeId +'"></div></div></td></tr></tbody></table>';
                        childContainer
                            .find("> .ui-treenode-children").children().last()
                            .after('<table><tbody><tr><td class="ui-treenode-connector"><table class="ui-treenode-connector-table"><tbody><tr><td class="ui-treenode-connector-line"></td></tr><tr><td class="ui-treenode-connector-line"></td></tr></tbody></table></td><td class="ui-treenode ui-treenode-leaf ui-treenode-unselected" data-nodetype="scene"><div class="ui-treenode-content ui-tree-selectable ui-state-default ui-corner-all ui-draggable"><span></span><input type="hidden" class="scene-parent-node-id" value="'+ parentNodeId +'"></input><input type="hidden" class="scene-node-id" value="' + id + '"></input><img src="'+ thumb +'" width="50" height="40"/><span style="vertical-align: middle; line-height: 30px" class="scene-node">' + name + '</span></div></td></tr></tbody></table>'+dropTargetDown);
                    }
                }
            }
        }
    }
}
function editTreeNode(name, source){       
    $(source).find(".section-node").html(name);

}
function removeTreeNode(source){
    if ($(source).parents("table").eq(0).prev().prev().size() === 1 && $(source).parents("table").eq(0).next().next().size() === 1) {
        $(source).parents("table").eq(0).prev().detach();
        $(source).parents("table").eq(0).detach();
    } else if ($(source).parents("table").eq(0).prev().prev().size() === 1) {
        $(source).parents("table").eq(0).prev()
                .find(".ui-treenode-connector-table > tbody").eq(0)
                .find(" > tr").last()
                .replaceWith('<tr><td></td></tr>');
        $(source).parents("table").eq(0).next().detach();
        $(source).parents("table").eq(0).detach();
    } else if ($(source).parents("table").eq(0).next().next().size() === 1) {
        $(source).parents("table").eq(0).next()
                .find(".ui-treenode-connector-table > tbody").eq(0)
                .find(" > tr")
                .first()
                .replaceWith('<tr><td></td></tr>');
        $(source).parents("table").eq(0).prev().detach();
        $(source).parents("table").eq(0).detach();
    } else {
        var parentNode = $(source).parents(".ui-treenode-children-container").eq(0).prev();
        $(parentNode).removeClass('ui-treenode-parent');
        $(parentNode).addClass('ui-treenode-leaf');
        $(parentNode).find(" > div").children().first().remove();
        $(source).parents(".ui-treenode-children-container").eq(0).detach();
        $(source).parents("table").eq(0).prev().detach();
    }


}

var open;

function showPreview(){

    PF('layout').toggle('west');

    $('#editButton .ui-button-text').text(function(i, text){
        return text === "Expand" ? "Close" : "Expand";
    })


}

function moveButtons()
{
    $('#editButton').appendTo($("#previewLayoutUnit").children(".ui-layout-unit-header"));
    $('#editButton').removeClass("ui-widget").removeClass("ui-commandlink");

    $('#refreshButton').appendTo($("#previewLayoutUnit").children(".ui-layout-unit-header"));
    $('#refreshButton').removeClass("ui-widget").removeClass("ui-commandlink");


}

function delayedRefreshData(){
  //  setTimeout(refreshData,1000);

}

function refreshData(){

    try {
        document.getElementById('player').update();
    }
    catch (e)
    {
        console.log(e);
    }


}

function loadAdmin(){
    document.getElementById('player').loadAdmin();
}

function reorderGameTasks(){

    var table = document.getElementById('tabView:taskList:gameTasks_data');
    var lookup = {};

    for(var i=0; i<table.rows.length; i++){
        var cell = table.rows[i].cells[0];
        var id = $(cell).find('.task-id').val();
        lookup[$(cell).find('.task-id').val()] = i;
    }

    $('#hidden\\:updatedTaskOrder').val(JSON.stringify(lookup));
    console.log($('#hidden\\:updatedTaskOrder').val());
    updateTaskOrder();

}

function loadTask(id){
    document.getElementById('player').taskChange(id);
}