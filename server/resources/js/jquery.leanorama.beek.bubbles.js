/*
    LEANORAMA - jQuery Plugin for displaying and navigating panoramic images.
    Beek Bubbles Extension

    Version 0.1.0
    -------------
    Copyright 2013 Leandigo (www.leandigo.com). All rights reserved.
    Use is subject to terms.
    
*/

$.fn.leanorama.extensions.push(function() {
    this.EV_BUBBLE_CLICK       = 'leanoramaBubbleClick';
    
    // This holds all the bubbles, their handles and their data
    var $bubbles    = []
    ,   $map        = map({name: 'beek-bubble-map'})
    ;

    // Create a bubble on panorama's surface
    function create_bubble(data) {
        // Bubble's geometry and type - corresponds with the model
        var geometry    = data.geometry || 'bubble'

        // The index for the bubble inside the bubble array
        ,   ix          = $bubbles.length
        
        // Create the bubble and the cube
        ,   $bubble         = div({class: 'leanorama-hotspot leanorama-hotspot-' + geometry, id: data.id})
        ,   $surface        = this.$surface.clone()
        ,   $innercube      = $('.leanorama-cube', $surface)
        ;

        $innercube.css({
            top : this.$cube.height() / -4 + 'px',
            left: this.$cube.width() / -4 + 'px'
        }).removeClass('leanorama-cube').addClass('leanorama-bubbles-cube');
        this.$surface.after($surface);
        $('.leanorama-side', $innercube).css('background', 'none');
        //$($('.leanorama-side', $innercube)[data.face]).append($bubble);
        $('.leanorama-side', $innercube).each(function(ix, el, arr) {
            ix === 0 ? $(el).append($bubble) : $(el).remove();
        });

        $bubble.css({top: data.y, left: '50%'});
        $bubble.popover({
            trigger: 'manual',
            placement: 'top',
            html: true,
            content: data.name,
            container: $bubble
        }).popover('show');

        // Hack hack hack - make bubbles look antialiased
        $bubble.children().wrapAll(div({
            'class': 'wrapper',
            'style': '-webkit-transform: scale3d(0.5, 0.5, 1); width: 200%; height: 200%;'
        }));

        $('.popover', $bubble).append(
            div({'class': 'dot top-left',       style: 'position: absolute; height: 0; width: 0; top: 0; left: 0'}),
            div({'class': 'dot bottom-left',    style: 'position: absolute; height: 0; width: 0; top: 100%; left: 0'}),
            div({'class': 'dot top-right',      style: 'position: absolute; height: 0; width: 0; top: 0; left: 100%'}),
            div({'class': 'dot bottom-right',   style: 'position: absolute; height: 0; width: 0; top: 100%; left: 100%'})
        );

        var changingScene = false;
        var click = $.proxy(function(data) {
            this.$el.trigger(this.EV_BUBBLE_CLICK);
            this.goto({
                lon: data.lon,
                lat: data.lat,
                callback: $.proxy(function() {
                    this.$container.transition({scale: 2}, $.proxy(function() {
                        if (changingScene) return;
                        changingScene = true;
                        this.beekScene.sceneId = data.value;
                        this.getScene();
                    }, this));
                }, this)
            }, this);
        }, this);

        $bubbles[ix] = { hotspot: $bubble, data: data, surface: $surface, click: click };
        
        // Fugly fugly hack hack hack!!!
        this.lon += 0.01;
    }

    // When leanorama's view is changed, we'll be moving the hotspots' handles
    this.$el.bind(this.EV_VIEW_CHANGED, $.proxy(function() {
        $map.html('');
        for (var ix in $bubbles) {
            // Set variables for current hotspot
            var $bubble     = $bubbles[ix].hotspot
            ,   $surface    = $bubbles[ix].surface
            ,   data        = $bubbles[ix].data
            ,   click       = $bubbles[ix].click
            ;

            $surface.transition({ z: this.perspective, rotateX: this.lat, rotateY: degnormalize(this.lon - data.lon + 90) }, 0);

            var dots = [];
            var outerRect = this.$el[0].getBoundingClientRect();
            $('.dot', $bubble).each($.proxy(function(ix, el, err) {
                var rect = el.getBoundingClientRect();

                dots.push({left: rect.left - outerRect.left, top: rect.top - outerRect.top})
            }, this));

            if (dots[0].top < dots[1].top) {
                var dotData = {};
                $.extend(dotData, data);
                $map.append(
                    area({
                        shape: 'poly',
                        coords: dots[0].left + ',' + dots[0].top + ', ' +
                                dots[1].left + ',' + dots[1].top + ', ' +
                                dots[3].left + ',' + dots[3].top + ', ' +
                                dots[2].left + ',' + dots[2].top
                    })
                    .click($.proxy(function(data) { click(data) },this, dotData))
                    .on('touchend', $.proxy(function(data) { click(data) },this, dotData))
                )
            }
        }
    }, this));

    this.$el.bind(this.EV_ENGINE_STOPPED, function() {
        $('.leanorama-hotspot-handle').animate({ opacity: this.fading ? 0 : 1 }, this.fading ? 2000 : 0);
    });
    
    // Create the bubbles
    this.$el.bind(this.EV_ENGINE_STARTED, $.proxy(function() {
        for (ix in this.beekBubbles) {
            this.beekBubbles[ix].lon = degnormalize(this.beekBubbles[ix].lon);
            cartesian(this.beekBubbles[ix]);
            create_bubble.call(this, this.beekBubbles[ix]);
        }
        this.$container.append(img({
            src     : 'http://gms.beektest.co/resources/img/fabric.png',
            style   : 'position: absolute; top:0; left:0; width: 100%; height: 100%; opacity: 0',
            usemap  : '#beek-bubble-map'
        }));
        this.$container.append($map);
    }, this));
    
    // Helper functions
    // Create a DIV element
    function div(attrs) {
        return $(document.createElement('div')).attr(attrs);
    }
    // Create a SPAN element
    function span(attrs) {
        return $(document.createElement('span')).attr(attrs);
    }
    // Create a IMG element
    function img(attrs) {
        return $(document.createElement('img')).attr(attrs);
    }
    // Create a MAP element
    function map(attrs) {
        return $(document.createElement('map')).attr(attrs);
    }
    // Create a AREA element
    function area(attrs) {
        return $(document.createElement('area')).attr(attrs);
    }

    // Normalize angles => -180 <= deg <= 180
    function degnormalize(deg) {
        while (deg > 180)  deg -= 360;
        while (deg < -180) deg += 360;
        return deg;
    }

    function cartesian(hotspot) {
        var lon_rot     = [90, 180, -90, 0]
        ,   nlon        = hotspot.lon
        ,   nlat        = hotspot.lat
        ,   face        = -1
        ;

        for (jx in lon_rot) {
            nlon = degnormalize(hotspot.lon - lon_rot[jx]);
            nlon -= nlon == 45  ? 0.1 : 0;
            nlon += nlon == -45 ? 0.1 : 0;
            if (nlon <= 45 && nlon >= -45) {
                face = jx;
                break;
            }
        }

        // No matter what the calculation yields, always set to face 0
        face = 0;

        var y           = 512
        ,   phi         = (90 - nlat) / 180 * Math.PI
        ,   theta       = (90 - nlon) / 180 * Math.PI
        ,   rho         = y / (Math.sin(phi) * Math.sin(theta))
        ,   x           = Math.round(y + rho * Math.sin(phi) * Math.cos(theta))
        ,   z           = Math.round(y - rho * Math.cos(phi))
        ;

        if (face >= 0) {
            hotspot.face = face;
            hotspot.x = x / 2;
            hotspot.y = z / 2;
            hotspot.rho = rho;
        }
    }
});