/*
    LEANORAMA - jQuery Plugin for displaying and navigating panoramic images.
    Guide Extension - dispalys navigation guide for Beek scenes

    Version 0.1.0
    -------------
    Copyright 2013 Leandigo (www.leandigo.com). All rights reserved.
    Use is subject to terms.
*/

$.fn.leanorama.extensions.push(function start() {

    this.createGuide = function(data) {
        // Extension container
        var $guide      = div({'class': 'leanorama-beek-guide leanorama-beek-guide-hidden'}).appendTo(this.$scope)
            // Guide 'toggle' button
        ,   $toggle     = $('<div/>').addClass('leanorama-beek-guide-toggle').appendTo(this.$scope).hide()
			// Toogle arrow
        ,   $togglearrow = $('<div/>').addClass('leanorama-beek-guide-toggle-arrow-right').appendTo($toggle)
		// Guide 'home' button
        ,   $home       = div({'class': 'leanorama-beek-guide-home'}).html('Home').appendTo($guide)
            // Guide wrapper
        ,   $wrapper    = div({'class': 'leanorama-beek-guide-wrapper'}).appendTo($guide)
            // Guide 'back' button
        ,   $back       = div({'class': 'leanorama-beek-guide-back'}).html('Back').appendTo($guide)
            // Guide JS object
        ,   guide       = {}
        ,   leanorama   = this  // YOU DIDN'T SEE THAT!
            // Currently loaded $content
        ,   $curr
            // Back button history
        ,   ghistory     = []
        ;


        // Init the Guide toggle button and arrow
        $toggle.click(function() {
		
		  if ($guide.hasClass('leanorama-beek-guide-hidden')) {
                    $guide.removeClass('leanorama-beek-guide-hidden');
					$toggle.addClass('leanorama-beek-guide-toggle-open');
					$togglearrow.addClass('leanorama-beek-guide-toggle-arrow-left');
                } else {
                    $guide.addClass('leanorama-beek-guide-hidden');
                    $toggle.removeClass('leanorama-beek-guide-toggle-open');
					$togglearrow.removeClass('leanorama-beek-guide-toggle-arrow-left');
                }
			
        });

        $(window).keypress(function(e) {
            if (e.originalEvent.which == 103) {
			if ($guide.hasClass('leanorama-beek-guide-hidden')) {
                    $guide.removeClass('leanorama-beek-guide-hidden');
					$toggle.addClass('leanorama-beek-guide-toggle-open');
                } else {
                    $guide.addClass('leanorama-beek-guide-hidden');
                    $toggle.removeClass('leanorama-beek-guide-toggle-open');
                }
            }
        });


				
		function toggleguide() {
				if ($guide.hasClass('leanorama-beek-guide-hidden')) {
                    $guide.removeClass('leanorama-beek-guide-hidden');
					$toggle.addClass('leanorama-beek-guide-toggle-open');
					$togglearrow.addClass('leanorama-beek-guide-toggle-arrow-left');
                } else {
                    $guide.addClass('leanorama-beek-guide-hidden');
                    $toggle.removeClass('leanorama-beek-guide-toggle-open');
					$togglearrow.removeClass('leanorama-beek-guide-toggle-arrow-left');
                }

		};		
		
        // Add Guide Contents page with Sections list
        var $sections   = ul();

        guide.$content = div({'class': 'leanorama-beek-guide-page'}).append(
            h2().html(data.title),
            p().html(data.description),
            $sections
        );

        guide.sections = {};
        guide.scenes = {};

        for (var s in data.guideSections) {
            var section         = data.guideSections[s]
            ,   sectionId       = section.guideSectionId || section.id
            ,   $scenes         = ul()
            ;

            guide.sections[sectionId] = {
                $content: div({'class': 'leanorama-beek-guide-page'}).append(
                    h2().html(section.title),
                    $scenes
                ),
                scenes: {}
            };

            $sections.append(li().html(section.title).attr('data-section', sectionId).data('sectionId', sectionId));

            for(var j in section.guideScenes) {
                var scene       = section.guideScenes[j]
                ,   sceneId     = scene.sceneId || scene.id
                ,   desc        = (scene.description || scene.scene.description || "")
                        .replace(/(<[^a|p|u|b][^/a|p|u|b].+?>)/ig,"")
                        .replace(/<p.+?>/ig, "<p>")
                        .replace(/<a.+?event:(.+?)" TARGET.*?>(.*?)<\/a>/ig, "<a href=\"javascript:void(0)\" data-goto=\"$1\" class=\"guide-link\">$2</a>")
                ,   $content    = div({'class': 'leanorama-beek-guide-page'})
                ;

                guide.scenes[sceneId] = {
                    $content: $content.append(
                        h2().html(scene.title),
                        p().html(desc)
                    )
                };

                $scenes.append(
                    li().html(scene.title || scene.scene.title)
                        .attr('data-scene', scene.sceneId)
                        .data('sceneId', sceneId)
                );
            }
        }

        // Init the main Guide page
        page(guide.$content);
        $toggle.fadeIn('1000');

        // Load a Guide page
        function page($content, back) {
            // Load the page if it's different from the one currently loaded
            if ($content != $curr) {
                back ? ghistory.pop() : $curr && ghistory.push($curr);  // Push current to history unless it's the initial page
                $curr = $content;               // Set the current pointer to the loaded page
                $wrapper.children().remove();   // Replace the current page with the new one
                $wrapper.append($content);

                if (ghistory.length > 0) $('.leanorama-beek-guide-back').show();
            }
        }

        this.scene_page = function(sceneId) {
            var c = guide.scenes[sceneId] || guide;
            page(c.$content);
        }

        $guide.on('click', 'li[data-scene]', function(e) {
            var sceneId = $(e.originalEvent.target).attr('data-scene');
            if (leanorama.scene.id != sceneId) {
                leanorama.beekScene.sceneId = sceneId;
                leanorama.getScene();
            }
            page(guide.scenes[sceneId].$content);
        });

        $guide.on('click', 'li[data-section]', function(e) {
            page(guide.sections[$(e.originalEvent.target).attr('data-section')].$content);
        });

        $guide.on('click', 'a[data-goto]', $.proxy(function(e) {
            var val = $(e.currentTarget).attr('data-goto');

            if (val.indexOf("|") != -1) {
                this.goto({lon: parseInt(val.split("|")[0], 10) - 90, lat: parseInt(val.split("|")[1], 10)});
            }
        }, this));

        // Bind 'home' & 'back' buttons
        $home.click(function() {
            page(guide.$content);

        });
		
        $back.click(function() {
            ghistory.length > 0 && page(ghistory[ghistory.length - 1], true);
            if (ghistory.length == 0) $(this).hide(); // Hide back button if the history is empty
			}).hide();
        this.guide = guide;
    };


    function _el(el, attrs) { return $(document.createElement(el)).attr(attrs || {}); }

    function div(attrs)     { return _el('div',     attrs) }
    function span(attrs)    { return _el('span',    attrs) }
    function h2(attrs)      { return _el('h2',      attrs) }
    function ul(attrs)      { return _el('ul',      attrs) }
    function li(attrs)      { return _el('li',      attrs) }
    function p(attrs)       { return _el('p',       attrs) }

});