$.fn.leanorama.extensions.push(function start() {
    function initBeek() {
        if (!this.beek) return;
        params = beekParams(this.beek);
        if (params.scene) {
            this.beekScene = {sceneId: params.scene};
        }
        if (params.guide) {
            $.getJSON(this.beek.jsonpPrefix + '/guide/' + params.guide + '/jsonp?callback=?', $.proxy(function(data) {
                this.createGuide(data);
                if (!params.scene) this.beekScene = {
                        sceneId:
                            data.firstScene ||
                            data.guideSections[0].firstScene ||
                            data.guideSections[0].guideScenes[0].sceneId
                    };
                this.getScene();
            }, this));
        } else {
            this.getScene();
        }
    }

    function beekParams(param) {
        var ret = {};
        if (param.guide) ret.guide = param.guide.toString();
        if (param.scene) ret.scene = param.scene.toString();
//        var kwargs = window.location.search.substr(1).split('&');
//        var ret = {};
//        for (ix in kwargs) {
//            var keyval = kwargs[ix].split('=');
//            ret[keyval[0]] = keyval[1];
//        }
        return ret;
    }
    this.$el.on(this.EV_INIT, $.proxy(initBeek, this));
});
