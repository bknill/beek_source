$.fn.leanorama.extensions.push(function() {
    this.EV_BEEK_SCENE_STARTED    = 'leanoramaBeekStarted';

    var FACES = "frblud";
    var noDeepZoom = {
        u:  true,
        d:  true
    };
    var TILES = ['0_0','0_1','1_0','1_1'];

    function startScene(scene) {
        if (!this.beekScene) return;
        this.sides = [];
        for (var face in FACES) {
            var tiles = [];
            if (FACES[face] in noDeepZoom) {
                tiles.push (this.beek.cdnPrefix + '/scene_' + scene.id + "_pano_" + scene.panoIncrement + "_" + FACES[face] + "/9/0_0.jpg");
            }
            else {
                for (var tile in TILES) tiles.push (
                    this.beek.cdnPrefix + '/scene_' + scene.id + "_pano_" + scene.panoIncrement + "_" + FACES[face] + "/10/" + TILES[tile] + ".jpg"
                )
            }
            this.sides.push(tiles);
        }
        this.beekBubbles = [];
        for (ix in scene.bubbles) {
            var bubble = scene.bubbles[ix];
            this.beekBubbles.push({
                type: 'nav',
                lat: bubble.tilt,
                lon: bubble.pan + 90,
                name: bubble.titleMerged,
                value: bubble.targetSceneId
            })
        }

        this.scene      = scene;
        this.infobox    = scene.title;
        this.lon        = parseInt(scene.pan, 10) + 90;
        this.lat        = parseInt(scene.tilt, 10);
        this.guide && this.scene_page(scene.id);
        this.restart();

        // Record which scene is currently loaded
        this.$el.data('scene', scene.id);
    }

    this.getScene = function() {
        var jsonpURL = this.beek.jsonpPrefix + '/scene/' + this.beekScene.sceneId + '/jsonp?callback=?';
        $.getJSON(jsonpURL, $.proxy(startScene, this));
    };
});
