// Define the overlay, derived from google.maps.OverlayView
function Label(opt_options) {
     // Initialization
     this.setValues(opt_options);
 
     // Here go the label styles
     var span = this.span_ = document.createElement('span');
     span.style.cssText = 'position: relative; left: -50%; top: -35px; ' +
                          'white-space: nowrap;color:#000000;' +
                          'padding: 2px;font-family: Arial; font-weight: bold;' +
                          'font-size: 12px;';
 
     var div = this.div_ = document.createElement('div');
     div.appendChild(span);
     div.style.cssText = 'position: absolute; display: none';
};
 
Label.prototype = new google.maps.OverlayView;
 
Label.prototype.onAdd = function() {
     var pane = this.getPanes().overlayImage;
     pane.appendChild(this.div_);
 
     // Ensures the label is redrawn if the text or position is changed.
     var me = this;
     this.listeners_ = [
          google.maps.event.addListener(this, 'position_changed',
               function() { me.draw(); }),
          google.maps.event.addListener(this, 'text_changed',
               function() { me.draw(); }),
          google.maps.event.addListener(this, 'zindex_changed',
               function() { me.draw(); })
     ];
};
 
// Implement onRemove
Label.prototype.onRemove = function() {
     this.div_.parentNode.removeChild(this.div_);
 
     // Label is removed from the map, stop updating its position/text.
     for (var i = 0, I = this.listeners_.length; i < I; ++i) {
          google.maps.event.removeListener(this.listeners_[i]);
     }
};
 
// Implement draw
Label.prototype.draw = function() {
     var projection = this.getProjection();
     var position = projection.fromLatLngToDivPixel(this.get('position'));
     var div = this.div_;
     div.style.left = position.x + 'px';
     div.style.top = position.y + 'px';
     div.style.display = 'block';
     div.style.zIndex = this.get('zIndex'); //ALLOW LABEL TO OVERLAY MARKER
     this.span_.innerHTML = this.get('text').toString();
};

var lat;
var lon;
var original_center;
var markerClusters = [];
var view;


function initialize(allLocs, allLats, allLons, allIDs, _view) {
    console.log("initialize()");
    var myOptions = {
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById("map_canvas"),
        myOptions);

    view = _view;
    setMarkers(map, allLocs, allLats, allLons, allIDs);
    setMapCenter();

}

function clearOverlays() {

    console.log("clearOverlays()");

    for (var i = 0; i < markers.length; i++ ) {
        markers[i].setMap(null);
    }
    markers.length = 0;

    for (var i = 0; i < markerClusters.length; i++ ) {
        markerClusters[i].clearMarkers();
        markerClusters[i].markers = [];
    }
    markerClusters = [];
    markers = [];
}

function updateMarkers(allLocs, allLats, allLons, allIDs){
    console.log(" updateMarkers()" + allLocs, allLats, allLons, allIDs);
    clearOverlays();
    setMarkers(map, allLocs, allLats, allLons, allIDs);
    setMapCenter();
}

function resizeMap(){
    console.log("resizeMap()");
    var z = map.getZoom();
    var c = map.getCenter();
    google.maps.event.trigger(map, 'resize');
    //and set them again here
    map.setZoom(z);
    map.setCenter(c);
}

function setMapCenter() {
	

    var bounds = new google.maps.LatLngBounds();
    for(i=0;i<markers.length;i++) {
        bounds.extend(markers[i].getPosition());
    }

    map.fitBounds(bounds);


    google.maps.event.trigger(map, 'resize');
}

var markers = [];
//function setMarkers(map, locations) {
function setMarkers(map, titles, lats, lons, IDs) {


	
	var image = {
		    url: '/resources/js/marker3.png',
		    // This marker is 20 pixels wide by 32 pixels tall.
		    //size: new google.maps.Size(120, 132),
		    // The origin for this image is 0,0.
		    origin: new google.maps.Point(0,0),
		    // The anchor for this image is the base of the flagpole at 0,32.
		    anchor: new google.maps.Point(50, 38)
	};

     var shape = {
          coord: [1, 1, 1, 20, 18, 20, 18 , 1],
          type: 'poly'
     };
     //should check that titles.length == lat.length and lon.length
     for (var i = 0; i < titles.length; i++)
    	 {
    	 	}
     
     for (var i = 0; i < titles.length; i++) {
    	
    	 if (titles[i].indexOf("%27") != -1)
    		 {
    		 	var temp = titles[i].replace("%27","'");
    		 	titles[i] = temp;
    		 }
    	 
    	 var mapping = IDs[i] + "/edit";

        if(view === 'guideAdmin'){
            titles[i] ="<a href='#' onclick='updateGuideTable([{name: \"locationId\", value: "+IDs[i]+"}])' />" + titles[i] + '</a>';
        } else {
            titles[i] ="<a href=" + mapping +" ' />" + titles[i] + "</a>";
        }


         var myLatLng = new google.maps.LatLng(lats[i], lons[i]);
         var marker = new google.maps.Marker({
              position: myLatLng,
              map: map,
              //shadow: shadow,
              icon: image,
              shape: shape,
              title: titles[i],
             zIndex: i
         });
         
 
          var label = new Label({
               map: map
          });
          label.set('zIndex', 1234);
          label.bindTo('position', marker, 'position');
          label.set('text', titles[i]);
          
          markers.push(marker);
          
          (function(m,l){
  			google.maps.event.addListener(m,'map_changed',function(){l.setMap(this.getMap());});
  		  })(marker,label);
     }
   

     
   var mcOptions = {gridSize: 70};  
   var markerCluster = new MarkerClusterer(map, markers, mcOptions);
    markerClusters.push(markerCluster);
}

