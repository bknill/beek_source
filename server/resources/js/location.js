function onAddSceneComplete(xhr, status, args,videoToStitch) {
    console.log('onAddSceneComplete()' + status);
    console.log(args);
    if(status == 'success'){
        if(args.temp_sceneId !== undefined && args.sceneId !== undefined && args.sceneName !== undefined){
            var temp_sceneId = args.temp_sceneId;
            var sceneId = args.sceneId;
            var sceneName = args.sceneName;

            var iid = 'sceneId_'+temp_sceneId;
            $('#'+iid).attr('id','sceneId_'+sceneId);


            if(!videoToStitch)
                uploadNextFile($('#wizard_form\\:tabView\\:sceneId'));
            else{
                uploadVideosToStitch($('#wizard_form\\:tabView\\:sceneId'));
                PF('upload2').disable();
                PF('uploadVids').disable();
            }

        }
    }
}

function onFileSelect(e){

    if (e.target.id === PF('fu').chooseButton.find('input[type=file]')[0].id) {
        var secondLastFile = PF('fu').files[PF('fu').files.length - 2];
        if (secondLastFile !== undefined) {
            if (secondLastFile.row[0].id === undefined || secondLastFile.row[0].id === "") {
                if (PF('fu').files.length > 0) {
                    PF('fu').removeFile(secondLastFile);
                }
            }
        }
    }
}

function onVideoToStitchFileSelect(e){

    if (e.target.id === PF('vu').chooseButton.find('input[type=file]')[0].id) {
        var secondLastFile = PF('vu').files[PF('vu').files.length - 3];
        if (secondLastFile !== undefined) {
            if (secondLastFile.row[0].id === undefined || secondLastFile.row[0].id === "") {
                if (PF('vu').files.length > 2) {
                    PF('vu').removeFile(secondLastFile);
                }
            }
        }
    }
}

function uploadNextFile(element){

    if (PF('fu').files.length > 0) {
        if (PF('fu').files[0].row[0].id !== undefined && PF('fu').files[0].row[0].id !== ""){
            updateHiddenSceneId(element);
            PF('fu').files[0].row.data('filedata').submit();

            return true;
        }
    }
    return false;
}


function uploadVideosToStitch(element){
    console.log('uploadVideosToStitch()');
    if (PF('vu').files.length > 0) {
        if (PF('vu').files[0].row[0].id !== undefined && PF('vu').files[0].row[0].id !== ""){
            updateHiddenSceneId(element, true);
            PF('vu').files[0].row.data('filedata').submit();
            return true;
        }
    }
    return false;
}

function updateHiddenSceneId(element, videoStitch){
    console.log('updateHiddenSceneId()');
    if (PF('fu').files.length > 0) {
        var firstFile = PF('fu').files[0];
        if (firstFile !== undefined) {
            var temp = PF('fu').files[0].row[0].id;
            var sceneId = temp.substr(8);

            if(sceneId != counter)
                element.val(sceneId);
            if ($(firstFile).attr('title') === undefined) {
                $(firstFile).attr('title', 'Uploading');
                updateSceneTab(sceneId, undefined, 'Upload Starts');
            }
        }
    }
    else if (PF('vu').files.length > 0 && videoStitch) {
        var firstFile = PF('vu').files[0];
        if (firstFile !== undefined) {
            var temp = PF('vu').files[0].row[0].id;
            var sceneId = temp.substr(8);
            console.log(sceneId);
            if(sceneId != "0")
            element.val(sceneId);
            if ($(firstFile).attr('title') === undefined) {
                $(firstFile).attr('title', 'Uploading');
                updateSceneTab(sceneId, undefined, 'Upload Starts');
            }
        }
    }
}

function initHiddenSceneId(element, videoStitch){
    console.log('initHiddenSceneId()' + videoStitch);

    if (PF('fu').files.length > 0) {

        var lastFile = PF('fu').files[PF('fu').files.length - 1];
        if (lastFile !== undefined) {
            if ($(lastFile)[0].row.attr('id') === undefined) {
                $(lastFile)[0].row.attr('id', 'sceneId_' + counter);
                element.val(counter);
                counter++;
                return true;
            } else {
                return false;
            }
        }
    }
    else if (videoStitch && PF('vu').files.length > 0) {
        console.log('video Stitch');
        var lastFile = PF('vu').files[PF('vu').files.length - 1];
        var secondLastFile = PF('vu').files[PF('vu').files.length - 2];
        var thirdLastFile = PF('vu').files[PF('vu').files.length - 3];

       if (lastFile !== undefined && secondLastFile !== undefined) {

           if ($(lastFile)[0].row.attr('id') === undefined)
               $(lastFile)[0].row.attr('id', 'sceneId_' + counter);

           if ($(secondLastFile)[0].row.attr('id') === undefined)
               $(secondLastFile)[0].row.attr('id', 'sceneId_' + counter);

           if(thirdLastFile)
           if ($(thirdLastFile)[0].row.attr('id') === undefined)
               $(thirdLastFile)[0].row.attr('id', 'sceneId_' + counter);

           element.val(counter);
           counter++;
           return true;
       }
         else
           return false;

        }
        else
         return false;

}

function sceneAdminPlayer(scene){

}

function updateSceneId(id){
    console.log(id);

    $("#wizard_form\\:tabView\\:sceneId").val(id);

}


function updateSceneUploadId(id){
    console.log(id);
    var lastFile = PF('su').files[PF('su').files.length - 1];
    $(lastFile)[0].row.attr('sceneId', id);
}