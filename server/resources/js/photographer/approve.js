function updateContainers(sceneId,accordionId,width,height,hasMap) {
    console.log("scsene Id = "+sceneId);
    var activeIndex = $("#"+accordionId+"_active").val();
    if(hasMap){
        updateGMap(sceneId,accordionId);
    }
    if(previousActiveIndex != activeIndex && previousActiveIndex != null){
        removeFlashContainer(previousActiveIndex,accordionId);
     }
    addFlashContainer(sceneId,accordionId,width,height);
    previousActiveIndex = activeIndex;
}
function updateGMap(sceneId,accordionId){
    var lat = $("#td_"+sceneId+"_hidden").children(":nth-child(2)").val();
    var lng = $("#td_"+sceneId+"_hidden").children(":nth-child(3)").val();
    var zoom = $("#td_"+sceneId+"_hidden").children(":nth-child(4)").val();
    var activeIndex = $("#"+accordionId+"_active").val();
    if(sceneId != null)
        $("#"+accordionId+"\\:" + activeIndex + "\\:scene_map").html(
            '<script id="'+accordionId+'":"' + activeIndex + '":scene_map_s" type="text/javascript">' +
                "$(function () {" +
                "PrimeFaces.cw('GMap','_" + sceneId + "', {id:'"+accordionId+":" + activeIndex + ":scene_map', mapTypeId:google.maps.MapTypeId.ROADMAP, center:new google.maps.LatLng(" + lat + "," + lng + "), zoom:" + zoom + ", fitBounds:false,markers:[new google.maps.Marker({position:new google.maps.LatLng(" + lat + "," + lng + "), id:'markerf04b5dea-c30b-4506-bc71-5a04eb8c74cf', draggable:true})],disableDoubleClickZoom:true, onPointClick:function (event) {" +
                "addMarker(event,_" + sceneId + ");" +
                "}});" +
                "});" +
                '</script>'
        );
    updateMarker(lat, lng, zoom, this['_' + sceneId]);
}
function addFlashContainer(sceneId,accordionId,width,height) {
    var activeIndex = $("#"+accordionId+"_active").val();
    var url = "/scene/"+sceneId+"/view";
    var flash_container = $("#"+accordionId+"\\:" + activeIndex + "\\:flash_container_content");
    flash_container.html(
        '<iframe src ='+url+' scrolling="no" frameborder="no" height="'+height+'" width="'+width+'"></iframe>'
    );
}
function removeFlashContainer(index,accordionId){
    console.log("remove index = "+index);
    var flash_container = $("#"+accordionId+"\\:" + index + "\\:flash_container_content");
    flash_container.html('');
}
function resetAccordionPanel(){
    var currentTabIndex = w.getStepIndex(w.currentStep);
    if(currentTabIndex == 0){
        $('#ap_active').val(-1);
    }else if(currentTabIndex == 1){
        $('#ap1_active').val(-1);
    }else if(currentTabIndex == 2){
        $('#ap2_active').val(-1);
    }else if(currentTabIndex == 3){
        $('#ap3_active').val(-1);
    }
}
function changeTabHeaderMessage(sceneId,sceneTitle,checkBox,label){
    var activeIndex = $("#ap_active").val();
    if($(checkBox).attr("checked")){
        $("#"+sceneId).html(
            "SCENE "+sceneTitle+
                 "<span style='float:right; padding-right: 20px;'>"+label+"</span>");
        if(label == "Accepted"){
            $("#ap\\:"+activeIndex+"\\:rejectCheckBox_input").removeAttr('checked');
            $("#ap\\:"+activeIndex+"\\:rejectCheckBox").children(":nth-child(2)").removeClass("ui-state-active").children(".ui-chkbox-icon").removeClass("ui-icon ui-icon-check");
        }else{
            $("#ap\\:"+activeIndex+"\\:acceptCheckBox_input").removeAttr('checked');
            $("#ap\\:"+activeIndex+"\\:acceptCheckBox").children(":nth-child(2)").removeClass("ui-state-active").children(".ui-chkbox-icon").removeClass("ui-icon ui-icon-check");
        }
    }else{
        $("#"+sceneId).html(
            "SCENE "+sceneTitle+
                "<span style='float:right; padding-right: 20px;'> Awaiting Changes </span>");
    }
}