function openFileDialog() {
    var file = document.getElementById("hiddenBrowseButton");
    file.value = '';
    file.content = '';
    file.click();
}
function updateContainers(scene_Id,load_flash_container) {
    var sceneId = scene_Id;
    var activeIndex = null;
    var tabs  = total_tabs;
    if(sceneId == null){
        activeIndex = $("#ap_active").val();
        sceneId = $("#ap\\:" + --tabs + "\\:scene_id").val();
    }
    var lat = $("#td_"+sceneId+"_hidden").children(":nth-child(2)").val();
    var lng = $("#td_"+sceneId+"_hidden").children(":nth-child(3)").val();
    var zoom = $("#td_"+sceneId+"_hidden").children(":nth-child(4)").val();

    var map_id = $(".scene_"+sceneId+"_map").attr('id');
    activeIndex = map_id.substring(map_id.indexOf(':')+1,map_id.lastIndexOf(':'));
    if(sceneId != null)
        $("#ap\\:" + activeIndex + "\\:scene_map").html(
            '<script id="ap:" + activeIndex + ":scene_map_s" type="text/javascript">' +
                "$(function () {" +
                "PrimeFaces.cw('GMap','_" + sceneId + "', {id:'ap:" + activeIndex + ":scene_map', mapTypeId:google.maps.MapTypeId.HYBRID, center:new google.maps.LatLng(" + lat + "," + lng + "), zoom:" + zoom + ", fitBounds:false,markers:[new google.maps.Marker({position:new google.maps.LatLng(" + lat + "," + lng + "), id:'markerf04b5dea-c30b-4506-bc71-5a04eb8c74cf', draggable:true})],disableDoubleClickZoom:true, onPointClick:function (event) {" +
                "addMarker(event,_" + sceneId + ");" +
                "}});" +
                "});" +
             '</script>'
        );
    updateMarker(lat, lng, zoom, this['_' + sceneId]);
    $("#tab_active_index").val(activeIndex);

    if(previousActiveIndex != activeIndex && previousActiveIndex != null)
        removeFlashContainer(previousActiveIndex);
    //if(load_flash_container)
        addFlashContainer(sceneId);
    previousActiveIndex = activeIndex;
}
function updateClientGMapData(sceneId,map) {
    var lat = map.cfg.markers[0].getPosition().lat();
    var lng = map.cfg.markers[0].getPosition().lng();
    var zoom = map.map.zoom;
    $("#td_"+sceneId+"_hidden").children(":nth-child(2)").val(lat);
    $("#td_"+sceneId+"_hidden").children(":nth-child(3)").val(lng);
    $("#td_"+sceneId+"_hidden").children(":nth-child(4)").val(zoom);
}
function deleteTab(sceneId){
    var map_id = $(".scene_"+sceneId+"_map").attr('id');
    var activeIndex = map_id.substring(map_id.indexOf(':')+1,map_id.lastIndexOf(':'));
    counter--;
    if(counter <=0 )counter = 0;
    accordionPanel.removeTab(activeIndex);
}
function showUploadedTick(sceneId){
    var component = $("#"+sceneId);
    if(component != null){
        var uploadedIcon = $(component).find(".uploadedimage");
        if(!uploadedIcon.hasClass("ui-icon-check")){
            uploadedIcon.removeClass("ui-icon ui-icon-close uploadedimage")
                        .addClass("ui-icon ui-icon-check uploadedimage");
        }
    }
}
function showUploadedCross(sceneId){
    var component = $("#"+sceneId);
    if(component != null){
        var uploadedIcon = $(component).find(".uploadedimage");
        if(!uploadedIcon.hasClass("ui-icon-close")){
            uploadedIcon.removeClass("ui-icon ui-icon-check uploadedimage")
                        .addClass("ui-icon ui-icon-close uploadedimage");
        }
    }
}
function showDefaultTick(sceneId){
    var component = $("#"+sceneId);
    if(component != null){
        var defaultIcon = $(component).find(".defaultimage");
        if(!defaultIcon.hasClass(("ui-icon-check"))){
            defaultIcon.removeClass("ui-icon ui-icon-close defaultimage")
                        .addClass("ui-icon ui-icon-check defaultimage");
        }
    }
}
function showDefaultCross(sceneId){
    var component = $("#"+sceneId);
    if(component != null){
        var defaultIcon = $(component).find(".defaultimage");
        if(!defaultIcon.hasClass("ui-icon-close")){
            defaultIcon.removeClass("ui-icon ui-icon-check defaultimage")
                        .addClass("ui-icon ui-icon-close defaultimage");
        }
    }
}
function handlePollComplete(xhr, status, args) {
    if(args.location!=null){
        var json =   jQuery.parseJSON(args.location);
        jQuery.each(json.location.scenes , function(){
            if(this['uploaded']){
                showUploadedTick(this['id']);
            }
            if(this['default']){
                showDefaultTick(this['id']);
            }
        });
        var activeSceneId = getActiveScene();
        if(activeSceneId != undefined && !hasFlashContainer()){
            addFlashContainer(activeSceneId);
        }
    }
    checkForApprove();
}
function addFlashContainer(sceneId) {
    if(sceneId == 0)return;
    var activeIndex = $("#tab_active_index").val();
    if(isUploaded(sceneId)){
        var url = "/views/scene/view.jsf?s="+sceneId+"&autologin=true";
        var flash_container = $("#ap\\:" + activeIndex + "\\:flash_container_content");
        flash_container.html(
            '<iframe src ='+url+' scrolling="yes" frameborder="no" height="500" width="850"></iframe>'
        );
    }
}
function removeFlashContainer(index){
    var flash_container = $("#ap\\:" + index + "\\:flash_container_content");
    flash_container.html('');
}
function hasFlashContainer(){
    var activeIndex = $("#tab_active_index").val();
    var flash_container = $("#ap\\:" + activeIndex + "\\:flash_container_content");
    return (flash_container.children().length == 1)?true:false;
}
function getActiveScene(){
    var activeIndex = $("#tab_active_index").val();
    return $("#ap\\:" + activeIndex + "\\:scene_id").val();
}
function isUploaded(sceneId){
    var component = $("#"+sceneId);
    if(component != null){
        var uploadedIcon = $(component).find(".uploadedimage");
        return uploadedIcon.hasClass("ui-icon-check");
    }
    return false;
}
function checkForApprove(){
    var allTicksFlag = true;
    var anchorComponents = $("#ap>h3>a");
    anchorComponents.each(function(index){
       if(!$(this).find(".uploadedimage").hasClass("ui-icon-check")){
           allTicksFlag = false;
           return false;
       }
       if(!$(this).find(".defaultimage").hasClass("ui-icon-check")){
           allTicksFlag = false;
           return false;
       }
    });
    if(allTicksFlag && anchorComponents.length > 0){
        next_button.enable();
    }else{
        next_button.disable();
    }
}
function removeNullFromComboBox(){
    $("#default_scene_panel>ul>li").each(function(index){
        if($(this).html() == 'null'){
            $(this).remove();
            default_scene_combobox.options.splice(index,1);
            default_scene_combobox.input.splice(index,1);
            default_scene_combobox.items.splice(index,1);
            default_scene_combobox.triggers.splice(index,1);
        }
    })
    $("#default_scene_input option[value='']").remove();
}