package co.beek;

import java.io.File;

public class Constants {
	public static String TEMP_DIR_NAME = "temp_dir";
	public static String TEMP_DIR_PATH = "." + File.separator + TEMP_DIR_NAME
			+ File.separator;

    public static String NATIVE_DIR_NAME = "native";
    public static final String NATIVE_DIR_PATH = "." + File.separator + NATIVE_DIR_NAME
            + File.separator;

	public static int ERROR_SERVICE = 1000;
	
	public static String domain;
}
