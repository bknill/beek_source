package co.beek.jobs;

import java.io.IOException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import co.beek.Constants;
import co.beek.pano.model.beans.APImethods;
import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.enterprizewizard.EWCorrespondenceResponse;
import co.beek.pano.model.dao.entities.GuideReport;
import co.beek.pano.model.dao.googleanalytics.LandingPage;
import co.beek.pano.service.dataService.MailService;
import co.beek.pano.service.dataService.enterprizewizardService.EWService;
import co.beek.pano.service.dataService.googleAnalytics.GAService;
import co.beek.pano.service.dataService.guideService.GuideService;
import co.beek.util.DateUtil;

import com.google.gdata.util.ServiceException;

@Service
@Scope("singleton")
public class CacheReportsJob {
	@Inject
	private GuideService guideService;

	@Inject
	private GAService gaService;

	@Inject
	private EWService ewService;

	@Inject
	private MailService emailService;

	@PostConstruct
	public void init() {
		System.out.println("starting guide stats cache service");
	}

	private static boolean running = false;
	private static List<GuideReport> guides;
	private static int delay = 0;
	

	//@Scheduled(fixedDelay = DateUtil.DAY_MILLIS)
	public void cacheGuideStats() throws InterruptedException {
		System.out.println("CacheReportsJob.cacheGuideStats()");
		if (running)
			return;
		running = true;

		if(guides == null || guides.size() < 1)
			guides = guideService.getAllGuidesReports();
		

		try {

			// Set the values for querying google analytics
			String startDate = DateUtil.getGAStartDate();
			String endDate = DateUtil.getCurrentDate();
			System.out.println("Number of Guides:" + guides.size());
			for (int i = guides.size()-1; i> -1; i--) {
				 cacheGuideStats(guides.get(i), startDate, endDate);
				 guides.remove(i);
			 }


		} catch (Exception e) {
			
			if(e.getMessage().contains("503")){
				
				Random r = new Random();
				int Low = 10;
				int High = 1000;
				int R = r.nextInt(High-Low) + Low;
				
				Thread.sleep((delay + 1000 * 2) + R);
				cacheGuideStats();
			}
			
			String message = "Could not finish cache reports. Cached up to " + guides.size() + " with a total of "
					+ APImethods.numQueries
					+ " queries before the following error: \r\n"
					+ ExceptionUtils.getStackTrace(e);
			System.out.println(message);
			emailService.emailError("Error Caching guide stats", message);
		}

		running = false;
	}

	private void cacheGuideStats(GuideReport guide, String startDate,
			String endDate) throws Exception, ParseException, ServiceException {
		//System.out.println("cacheGuideStats: " + guide.getId());

		// caches the data and stores it on guide object
		guide = gaService.cacheGAData(guide, startDate, endDate);

		// if (!guide.getTeamId().equals(EWTeam.TEAM_SUPER_USER)
		// && isReportDay(guide))
		// guide = notifyGuideReport(guide);

		//System.out.println("Total queries: " + APImethods.numQueries);
		guideService.saveGuideReport(guide);
	}

	private boolean isReportDay(GuideReport guide) throws ParseException {
		Calendar today = Calendar.getInstance();
		Calendar lastReport = Calendar.getInstance();
		lastReport.setTime(guide.getEmailReportTime());

		long milisBetween = today.getTimeInMillis()
				- lastReport.getTimeInMillis();

		System.out.println(today.get(Calendar.DAY_OF_MONTH));
		System.out.println(milisBetween / DateUtil.DAY_MILLIS);

		System.out.println(today.toString());
		System.out.println(lastReport.toString());

		// If today is report day, and the guide has not sent a report today
		// OR it has been more than a month since the last report.
		boolean todayIsReportDay = today.get(Calendar.DAY_OF_MONTH) == 21;
		boolean noReportsToday = milisBetween > DateUtil.DAY_MILLIS;
		boolean moreThank30Days = milisBetween > DateUtil.MONTH_MILLIS;

		return (todayIsReportDay && noReportsToday) || moreThank30Days;
	}

	private String getGuideId(String path) {
		if (path.indexOf("g") != -1 && path.indexOf("s") != -1) // g8/s123
			return path.substring(path.indexOf("/g") + 2, path.indexOf("/s"));

		else if (path.indexOf("/g/") != -1) // g/8
			return path.substring(path.indexOf("/g/") + 3);

		else if (path.indexOf("/g") != -1) // g8
			return path.substring(path.indexOf("/g") + 2);

		return null;
	}

	private GuideReport notifyGuideReport(GuideReport guide) throws IOException {
		LandingPage data = guide.getLandingPageData();

		// TODO pdf creation and uploading is canceled for the time being.

		String url = Constants.domain + "/guide/{id}/report";
		url = url.replace("{id}", guide.getId());

		String body = "Your guide report is ready.\\r\\n";
		body += "\\r\\n";
		body += "Guide Title" + guide.getTitle() + "\\r\\n";
		body += "Visits" + guide.getTotalVisits() + "\\r\\n";
		body += "Time" + data.getAvgMinutesOnSite() + "\\r\\n";
		body += "Page Views Per Visit" + data.getPageViewsPerVisit() + "\\r\\n";
		// body += "Conversions" + guide.getTotalConversions() + "\\r\\n";
		body += "\\r\\n";
		body += "<a href='" + url + "'>View more here</a>\\r\\n";

		url = URLEncoder.encode(url, "UTF-8");
		body = URLEncoder.encode(body, "UTF-8");

		EWCorrespondenceResponse response = ewService.sendEmail(
				EWContact.ADMIN_USER_USERNAME, EWContact.ADMIN_USER_PASSWORD,
				guide.getTeamId(), guide.getId(), "GuideReportTemplate", body, url, null);

		// update email report time to current date
		if (response.getSuccess())
			guide.setEmailReportTime(new Date());

		else
			emailService.emailError("Error emailing via EW",
					response.getErrorMessage());

		return guide;
	}
}
