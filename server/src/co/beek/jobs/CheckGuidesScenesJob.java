package co.beek.jobs;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import co.beek.pano.service.dataService.MailService;
import co.beek.pano.service.dataService.guideService.GuideService;
import co.beek.pano.service.dataService.sceneService.SceneService;
import co.beek.util.DateUtil;

@Service
@Scope("singleton")
public class CheckGuidesScenesJob {

	@Inject
	private GuideService guideService;

	@Inject
	private SceneService sceneService;

	@Inject
	private MailService mailService;

	@PostConstruct
	public void init() {

	}

	/**
	 * Scheduled task that is run every month 
	 * but more probably whenever the server is restarted.
	 */
	@Scheduled(fixedDelay = DateUtil.MONTH_MILLIS)
	public void processTaskQueue() {
		//checkScenes();
		//checkGuides();
	}

	private void checkScenes() {
		List<String> allSceneIds = sceneService.getAllSceneIds();
		String errors = "";
		int errorCount = 0;
		for (String sceneId : allSceneIds) {
			try {
				sceneService.getSceneDetail(sceneId);

			} catch (Exception e) {
				errorCount++;
				errors += "Error getting scene details for scene: " + sceneId
						+ "\r\n";
				errors += ExceptionUtils.getStackTrace(e);
			}
		}

		if (errorCount > 0) {
			errors = "Errors in " + errorCount + " Scenes\r\n\r\n" + errors;
			mailService.emailError("Error checking scenes", errors);
		}
	}

	private void checkGuides() {
		List<String> allGuideIds = guideService.getAllGuideIds();
		String errors = "";
		int errorCount = 0;
		for (String guideId : allGuideIds) {
			try {
				guideService.getGuideDetail(guideId);

			} catch (Exception e) {
				errorCount++;
				errors += "Error getting scene details for guide: " + guideId
						+ "\r\n";
				errors += ExceptionUtils.getStackTrace(e);
			}
		}

		if (errorCount > 0) {
			errors = "Errors in " + errorCount + " Guides\r\n\r\n" + errors;
			mailService.emailError("Error checking guides", errors);
		}
	}

}
