package co.beek.jobs;

import co.beek.Constants;
import co.beek.pano.model.dao.entities.Scene;
import co.beek.pano.model.dao.entities.ScenePanoTask;
import co.beek.pano.service.dataService.MailService;
import co.beek.pano.service.dataService.sceneService.SceneService;
import co.beek.pano.service.dataService.taskService.TaskService;
import co.beek.pano.service.dataService.uploadService.S3UploadService;
import co.beek.util.DateUtil;

import com.panozona.converter.DeepZoomTiler;
import com.panozona.converter.EquirectangularToCubic;

import ec2.service.PanoService;
import ec2.service.ProcessingService;
import org.jets3t.service.S3ServiceException;
import org.primefaces.push.EventBus;
import org.primefaces.push.EventBusFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.apache.sanselan.Sanselan;
import org.apache.sanselan.common.IImageMetadata;
import org.apache.sanselan.common.byteSources.ByteSourceFile;
import org.apache.sanselan.formats.jpeg.JpegImageMetadata;
import org.apache.sanselan.formats.jpeg.exifRewrite.ExifRewriter;
import org.apache.sanselan.formats.jpeg.xmp.JpegXmpRewriter;
import org.apache.sanselan.formats.tiff.TiffImageMetadata;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;

import org.apache.sanselan.formats.tiff.write.TiffOutputSet;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.NoSuchAlgorithmException;

@Service
@Scope("singleton")
public class PanoProcessingJob {

	@Inject
	private TaskService taskService;
	@Inject
	private SceneService sceneService;
	@Inject
	private S3UploadService s3UploadService;

	@Inject
	private ProcessingService processingService;

	@Inject
	private MailService emailService;

	@Value("#{buildProperties.assetsBucket}")
	private String assetsBucket;

	private ScenePanoTask currentTask;


	@PostConstruct
	public void init() {

	}

	@Scheduled(fixedDelay = DateUtil.MINUTE_MILLIS)
	private void processTask(){
		processingService.start();
	}

	/**
	 * Scheduled task that is run every 5 mins. Looks for the task, if found,
	 * executes the task. Every 10 seconds now.
	 */
	//@Scheduled(fixedDelay = DateUtil.MINUTE_MILLIS)
	public void processTaskQueue() {





		// Check if a task is currently being processed
		if (currentTask != null)
			return;


		currentTask = taskService.getTaskFromQueue();

		// Check if no task was found in the queue
		if (currentTask == null)
			return;

		processTask(currentTask);



		// Update running so more tasks can be executed
		currentTask = null;
	}

	private void processTask(ScenePanoTask task) {


		EventBus pushContext = null;

		if(EventBusFactory.getDefault() != null)
			pushContext = EventBusFactory.getDefault().eventBus();

		System.out.println("ScenePanoService.processTask("
				+ task.getTempFileName() + ")");
		// if file exist then do processing otherwise delete that task
        String pushString = null;


        try {

			task.status = ScenePanoTask.PROCESSING;
			taskService.updateTask(task);

            pushString = "{\"success\": \"true\", \"eventName\": \"updateSceneTab\", \"sceneId\": \""+task.getSceneId()+"\", \"sceneName\": \""+task.getScene().getTitle()+"\", \"message\": \"Processing Starts\"}";

			if(pushContext != null)
			pushContext.publish("/", pushString);

			processPanoImage(task);

            pushString = "{\"success\": \"true\", \"eventName\": \"enableSceneTab\", \"sceneId\": \""+task.getSceneId()+"\", \"sceneName\": \""+task.getScene().getTitle()+"\", \"message\": \"Processing Finished\"}";
			if(pushContext != null)
			pushContext.publish("/", pushString);

		} catch (Throwable e) {
            pushString = "{\"success\": \"false\", \"eventName\": \"enableSceneTab\", \"sceneId\": \""+task.getSceneId()+"\", \"sceneName\": \""+task.getScene().getTitle()+"\", \"message\": \"Processing Failed\"}";

			if(pushContext != null)
			pushContext.publish("/", pushString);

			String error = "Error occured while processing pano image: "
					+ e.getMessage() + e.getStackTrace().toString();
			System.out.println(error);
			emailService.emailError("Error Processing image", error);
			
		}finally{
			// Delete the task now, so can't be executed twice.
			taskService.deleteTask(currentTask);
		}
	}


	private void processPanoImage(ScenePanoTask task) throws Throwable {

		Scene scene = task.getScene();

		System.out.println("PanoProcessingJob.processPanoImage("
				+ scene.getTitle() + ", " + task.getTempFileName() + ")");

		File tempFile = new File(Constants.TEMP_DIR_PATH + task.getTempFileName());
		File jpegImageFile = tempFile;
		if (!tempFile.exists())
			throw new Exception("Temp file did not exist when transforming");

		File tilesOutputDir = null;
		File cubicOutputDir = null;
		System.out.println("PanoIncrement Before:" + scene.getPanoIncrement());
		try {
			// Increment the pano before performing any tasks
			scene.incrementPano();

			// Create the required directories
			cubicOutputDir = getCubicDir(scene);
			tilesOutputDir = getTilesDir(scene);

			File newTempFile = new File(Constants.TEMP_DIR_PATH
					+ scene.getPanoName());
			tempFile.renameTo(newTempFile);
			tempFile = newTempFile;
			System.out.println("Renamed to: " + tempFile.getName());

			// Populate and upload the temp directories
			processPanoImage(task, scene, tempFile, cubicOutputDir, tilesOutputDir);
			
			
	        jpegImageFile = tempFile;

	        //Add GPS settings to EXIF
	    	File tempExifFile = new File(Constants.TEMP_DIR_PATH + "scene_" +  scene.getId() + "_pano_" + scene.getPanoIncrement() + "_EXIF.jpg");
			 File XMPFile = setExifGPSTag(jpegImageFile, tempExifFile, scene);


			//convertToATF(cubicOutputDir);

	         
	     	// upload the pano image with updated EXIF.
			s3UploadService.uploadFile(assetsBucket,  XMPFile);
				
	         
	         System.out.println("Data has been changed successfull!");

			// update the scene with the new pano increment
			sceneService.saveScene(scene);

		} finally {
			if (tempFile != null && tempFile.exists())
				tempFile.delete();

			if (jpegImageFile != null && jpegImageFile.exists())
				jpegImageFile.delete();

			if (tilesOutputDir != null && tilesOutputDir.exists())
				deleteDir(tilesOutputDir);

			if (cubicOutputDir != null && cubicOutputDir.exists())
				deleteDir(cubicOutputDir);

			System.out.println("PanoIncrement after:"
					+ scene.getPanoIncrement());
		}
	}

	private File getCubicDir(Scene scene) {
		File dir = new File(Constants.TEMP_DIR_PATH + scene.getPanoPrefix()
				+ "_cubic");
		dir.mkdir();

		return dir;
	}

	private File getTilesDir(Scene scene) {
		File dir = new File(Constants.TEMP_DIR_PATH + scene.getPanoPrefix()
				+ "_tiles");
		dir.mkdir();

		return dir;
	}

	private void deleteDir(File d) {
		for (File r : d.listFiles()) {
			if (r.isDirectory()) {
				deleteDir(r);
				r.delete();
			} else {
				r.delete();
			}
		}
		d.delete();
	}

	/**
	 * Transforms the tempfile and populates the 2 temp directories. Uploads the
	 * contents of tiles dir to S3.
	 * 
	 * @param scene
	 * @param tempFile
	 * @param cubicOutputDir
	 * @param tilesOutputDir
	 * @throws Exception
	 */
	private void processPanoImage(ScenePanoTask task, Scene scene,
			File tempFile, File cubicOutputDir, File tilesOutputDir)
			throws Throwable {
		System.out.println("ScenePanoService.processPanoImage("
				+ scene.getTitle() + ", " + tempFile.getName() + ")");

		// should alreayd be at processing by now
		//task.status = ScenePanoTask.PROCESSING
		//taskService.updateTask(task);

		// First step is to create the cubic images in the cubic dir
		EquirectangularToCubic equirectToCubic = new EquirectangularToCubic();
		equirectToCubic.processImageFile(tempFile, cubicOutputDir);

		//task.status = "Creating image tiles.";
		//taskService.updateTask(task);

		// For each cubic image, create the deep zoom tiles
		for (File f : cubicOutputDir.listFiles())
			DeepZoomTiler.processImageFile(f, tilesOutputDir);

		task.status = ScenePanoTask.UPLOADING;
		taskService.updateTask(task);

		// Upload the contents of the tiles dir to S3
		s3UploadService.uploadDir(tilesOutputDir, assetsBucket);

	}

	public Boolean convertExistingSceneToATF(Scene scene) throws Exception {

		String url1 = "http://cdn.beek.co/scene_" + scene.getId() + "_pano_" + scene.getPanoIncrement() + "_EX.jpg";
		String url2 = "http://cdn.beek.co/scene_" + scene.getId() + "_pano_" + scene.getPanoIncrement() + ".jpg";

		BufferedImage img = null;

		URL url_1 = new URL(url1);
		URL url_2 = new URL(url2);
		HttpURLConnection huc_1 = (HttpURLConnection) url_1.openConnection();
		int responseCode1 = huc_1.getResponseCode();

		HttpURLConnection huc_2 = (HttpURLConnection) url_2.openConnection();
		int responseCode2 = huc_2.getResponseCode();

		if(responseCode1 == 200)
			img = ImageIO.read(url_1);
		else if(responseCode2 == 200)
			img = ImageIO.read(url_2);
		else
			return false;


		File cubicOutputDir = getCubicDir(scene);

		File tempFile = new File(Constants.TEMP_DIR_PATH
				+ scene.getPanoName());

		ImageIO.write(img, "jpg", tempFile );


		EquirectangularToCubic equirectToCubic = new EquirectangularToCubic();
		equirectToCubic.processImageFile(tempFile, cubicOutputDir);
		convertToATF(cubicOutputDir);

		if (tempFile != null && tempFile.exists())
			tempFile.delete();


		if (cubicOutputDir != null && cubicOutputDir.exists())
			deleteDir(cubicOutputDir);

		return true;



	}

	private void convertAllScenes(){

		for(String sceneId : sceneService.getAllSceneIds()) {

			try {
				String url = "http://cdn.beek.co/scene_" + sceneService.getScene(sceneId).getId() + "_pano_" + sceneService.getScene(sceneId).getPanoIncrement() + ".atf";
				URL url_1 = new URL(url);
				HttpURLConnection huc_1 = (HttpURLConnection) url_1.openConnection();
				int responseCode1 = huc_1.getResponseCode();

				if(responseCode1 == 200){
					System.out.println("scene " + sceneId + " is processed" );
					continue;
				}

				Boolean converted = convertExistingSceneToATF(sceneService.getScene(sceneId));

				if(converted)
					System.out.println(sceneId + " converted");
				else
					System.out.println(sceneId + " failed");

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	private void convertToATF(File cubicOutputDir) throws IOException, InterruptedException, S3ServiceException, NoSuchAlgorithmException {

		String first = "";

		for (File child : cubicOutputDir.listFiles()) {

				char face = child.getPath().charAt(child.getPath().lastIndexOf('.')-1);
				String var = "";

			switch(face){
				case 'l' :
					var = "0";
					break;
				case 'r' :
					var = "1";
					break;
				case 'd' :
					var = "2";
					break;
				case 'u' :
					var = "3";
					break;
				case 'b' :
					var = "4";
					break;
				case 'f' :
					var = "5";
					break;
			}

				String newFilename = child.getPath().substring(0, child.getPath().lastIndexOf('.')-1) + var + ".png";

				File file = new File(newFilename);

			if(var.equals("0"))
				first = file.getCanonicalPath();

				BufferedImage bufferedImage = ImageIO.read(child);

				BufferedImage resized = new BufferedImage(1024, 1024, bufferedImage.getType());
				Graphics2D g = resized.createGraphics();
				g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
				g.drawImage(bufferedImage, 0, 0, 1024, 1024, 0, 0, bufferedImage.getWidth(), bufferedImage.getHeight(), null);
				g.dispose();

				ImageIO.write(resized, "png", file );
		}

		File atfFile = new File(first.substring(0,first.lastIndexOf('.') -2) + ".atf");
		String png2aft;

	//	if(Constants.domain.equals("beekdev.co"))
			png2aft = "C:\\atftools\\png2atf.exe -c d -r -q 0 -f 0 -m -i " + first + " -o " + first.substring(0,first.lastIndexOf('.') -2) + ".atf";
		//else
		//	png2aft = "png2atf -c d -r -q 0 -f 0 -m -i " + first + " -o " + first.substring(0,first.lastIndexOf('.') -2) + ".atf";

		final Process p = Runtime.getRuntime().exec(png2aft);

		new Thread(new Runnable() {
			public void run() {
				BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
				String line = null;

				try {
					while ((line = input.readLine()) != null)
						System.out.println(line);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}).start();

		p.waitFor();

		s3UploadService.uploadFile(assetsBucket,  atfFile);
		System.out.println("Uploaded " + atfFile.getName());
	}


	public File setExifGPSTag(File jpegImageFile, File dst, Scene scene) throws Throwable{
		  OutputStream os = null;
		  try
		  {
		      TiffOutputSet outputSet = null;
		
		      // note that metadata might be null if no metadata is found.
		      IImageMetadata metadata = Sanselan.getMetadata(jpegImageFile);
		      JpegImageMetadata jpegMetadata = (JpegImageMetadata) metadata;
		      if (null != jpegMetadata)
		      {
		          // note that exif might be null if no Exif metadata is found.
		          TiffImageMetadata exif = jpegMetadata.getExif();
		
		          if (null != exif)
		              outputSet = exif.getOutputSet();
		          
		      }
		
		      if (null == outputSet)
		          outputSet = new TiffOutputSet();
		
		          double longitude = scene.getLongitude();
		          double latitude = scene.getLatitude(); 

		       /*   TagInfo tag  = new TagInfo(
		                    "GPSAltitudeRef",
		                    0x0005,
		                    TiffConstants.FIELD_TYPE_DESCRIPTION_BYTE,
		                    1,
		                    TiffConstants.EXIF_DIRECTORY_GPS);
	
		          Byte b = new Byte( (byte) TiffConstants.GPS_TAG_GPS_ALTITUDE_REF_VALUE_ABOVE_SEA_LEVEL );
		          
		          TiffOutputField altitude = TiffOutputField.create(tag, outputSet.byteOrder, b);
					TiffOutputDirectory exifDirectory = outputSet.getOrCreateExifDirectory();

					exifDirectory.add( altitude );*/
					outputSet.setGPSInDegrees(longitude, latitude);
		          
			      os = new FileOutputStream(dst);
			      os = new BufferedOutputStream(os);
			
			     new ExifRewriter().updateExifMetadataLossless(jpegImageFile, os, outputSet);
			
			      os.close();
			      os = null;
			      
			      BufferedImage bimg = ImageIO.read( jpegImageFile );
			      int width          = bimg.getWidth();
			      int height         = bimg.getHeight();
			      
			      
		      	File tempXMPFile = new File(Constants.TEMP_DIR_PATH+ "scene_" +  scene.getId() + "_pano_" + scene.getPanoIncrement() + "_EX_temp.jpg");
		     	File XMPFile = new File(Constants.TEMP_DIR_PATH+ "scene_" +  scene.getId() + "_pano_" + scene.getPanoIncrement() + "_EX.jpg");
			    
		      	String xmpXml = Sanselan.getXmpXml(jpegImageFile) + "<rdf:Description rdf:about=** xmlns:GPano=*http://ns.google.com/photos/1.0/panorama/*>"
		      				+"	<GPano:UsePanoramaViewer>True</GPano:UsePanoramaViewer>"
		      				+"	<GPano:ProjectionType>equirectangular</GPano:ProjectionType>"
	        		  		+"  <GPano:CroppedAreaImageWidthPixels>"+ width +"</GPano:CroppedAreaImageWidthPixels>"
	        		  		+"  <GPano:CroppedAreaImageHeightPixels>"+ height +"</GPano:CroppedAreaImageHeightPixels>"
	        		  		+"  <GPano:FullPanoWidthPixels>"+ width +"</GPano:FullPanoWidthPixels>"
	        		  		+"  <GPano:FullPanoHeightPixels>"+ height +"</GPano:FullPanoHeightPixels>"
	        		  		+"  <GPano:PoseHeadingDegrees>0.0</GPano:PoseHeadingDegrees>"
						    +"  <GPano:CroppedAreaLeftPixels>0</GPano:CroppedAreaLeftPixels>"
						    +"  <GPano:CroppedAreaTopPixels>0</GPano:CroppedAreaTopPixels>"
	        		  		+ "</rdf:Description>";
		      	
		      	xmpXml.replace('*', '"');

	            JpegXmpRewriter xmpWriter = new JpegXmpRewriter();
	            xmpWriter.removeXmpXml(new ByteSourceFile(dst), new BufferedOutputStream(new FileOutputStream(tempXMPFile)));    
	            xmpWriter.updateXmpXml(new ByteSourceFile(tempXMPFile), new BufferedOutputStream(new FileOutputStream(XMPFile)), xmpXml); 
	            
	           // File test = new File("C://Users/ben/Pictures/XMP/test.jpg");
	          //  xmpWriter.updateXmpXml(new ByteSourceFile(tempXMPFile), new BufferedOutputStream(new FileOutputStream(test)), xmpXml); 
	              
	            
	            return XMPFile;
				      

		  } finally
		  {
		      if (os != null)
		          try
		          {
		              os.close();
		          } catch (IOException e)
		          {
		
		          }
		  }
		}
	
}
