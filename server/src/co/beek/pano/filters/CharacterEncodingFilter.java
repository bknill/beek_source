package co.beek.pano.filters;

import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class CharacterEncodingFilter implements Filter {

    protected String encoding;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        boolean isMultiPartRequest = ServletFileUpload.isMultipartContent((HttpServletRequest)servletRequest);
        if(isMultiPartRequest){
            filterChain.doFilter(new RequestWrapper(servletRequest), servletResponse);
        }else{
            filterChain.doFilter(servletRequest,servletResponse);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        encoding = filterConfig.getInitParameter("encoding");
    }

    @Override
    public void destroy() {
        encoding = null;
    }

    private class RequestWrapper extends HttpServletRequestWrapper {

        public RequestWrapper(ServletRequest request) {
            super((HttpServletRequest) request);
        }

        @Override
        public String getParameter(String name) {
            String actualValue = super.getParameter(name);
            String encodedValue = null;
            if (actualValue != null) {
                try {
                    encodedValue = new String(actualValue.getBytes("ISO-8859-1"), encoding);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                return encodedValue;
            } else {
                return actualValue;
            }
        }
    }
}