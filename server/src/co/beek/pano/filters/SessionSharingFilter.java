package co.beek.pano.filters;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SessionSharingFilter implements Filter {
    protected String domain;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        domain = filterConfig.getInitParameter("domain");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        updateCookie((HttpServletRequest)servletRequest,(HttpServletResponse)servletResponse);
        filterChain.doFilter(servletRequest,servletResponse);
    }

    @Override
    public void destroy() {
        domain = null;
    }
    private void updateCookie(HttpServletRequest request, HttpServletResponse response){
    	// Transforms the server name into the subdomain independent cookie domain
    	String cookieDomain = "."+request.getServerName().replace("gms.", "");
    	
    	for (Cookie cookie : request.getCookies()) {
            if (!response.containsHeader("Set-Cookie")) {
                Cookie tempCookie = (Cookie)cookie.clone();
                tempCookie.setValue(request.getSession().getId());
                tempCookie.setDomain(cookieDomain);
                tempCookie.setHttpOnly(true);
                response.addCookie(tempCookie);
            }
        }
    }
}
