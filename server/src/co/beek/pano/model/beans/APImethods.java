/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.beek.pano.model.beans;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;

import com.google.gdata.client.analytics.AnalyticsService;
import com.google.gdata.client.analytics.DataQuery;
import com.google.gdata.data.Link;
import com.google.gdata.data.analytics.Aggregates;
import com.google.gdata.data.analytics.DataEntry;
import com.google.gdata.data.analytics.DataFeed;
import com.google.gdata.data.analytics.DataSource;
import com.google.gdata.data.analytics.Dimension;
import com.google.gdata.data.analytics.Metric;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.analytics.Analytics;
import com.google.api.services.analytics.AnalyticsScopes;
import com.google.api.services.analytics.model.Accounts;
import com.google.api.services.analytics.model.GaData;
import com.google.api.services.analytics.model.Profiles;
import com.google.api.services.analytics.model.Webproperties;

/**
 * Sample program demonstrating how to make a data request to the GA Data Export
 * using client login authentication as well as accessing important data in the
 * feed.
 */
public class APImethods {

	private static final String PROFILE_ID = "54190402";
	private static final String URL = "https://www.googleapis.com/analytics/v2.4/data";
	private static final String API_KEY = "AIzaSyD7LPriAgkDl6i-VjHZNCZhBGDl3kaF714";
	private static final String KEY_FILE_LOCATION = "resources/keys/gmsdata-ba8e5b1c9c0d.p12";
	private static final String SERVICE_ACCOUNT_EMAIL = "538912218845-j15u3d9eoi2est8i624rt57bfh1112uh@developer.gserviceaccount.com";
	private static final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance(); 
	private static final String APPLICATION_NAME = "Hello Analytics";
	
	public static int numQueries;
	private static Analytics analytics;

	// ------------------------------------------------------
	// Configure GA API
	// ------------------------------------------------------

	 public static Analytics initialise() {
		    try {
		      analytics = initializeAnalytics();
		      return analytics;
		    } catch (Exception e) {
		      e.printStackTrace();
		    }
			return null;
		  }
	
	 
	 private static HttpRequestInitializer setHttpTimeout(final HttpRequestInitializer requestInitializer) {
		  return new HttpRequestInitializer() {
		    @Override
		    public void initialize(HttpRequest httpRequest) throws IOException {
		      requestInitializer.initialize(httpRequest);
		      httpRequest.setConnectTimeout(3 * 60000);  // 3 minutes connect timeout
		      httpRequest.setReadTimeout(3 * 60000);  // 3 minutes read timeout
		    }};

	 }
	
	public static Analytics initializeAnalytics() throws Exception {
	    // Initializes an authorized analytics service object.

	    // Construct a GoogleCredential object with the service account email
	    // and p12 file downloaded from the developer console.
	    HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
	    GoogleCredential credential = new GoogleCredential.Builder()
	        .setTransport(httpTransport)
	        .setJsonFactory(JSON_FACTORY)
	        .setServiceAccountId(SERVICE_ACCOUNT_EMAIL)
	        .setServiceAccountPrivateKeyFromP12File(new File(KEY_FILE_LOCATION))
	        .setServiceAccountScopes(AnalyticsScopes.all())
	        .build();

	    // Construct the Analytics service object.
	    return new Analytics.Builder(httpTransport, JSON_FACTORY,setHttpTimeout(credential))
	        .setApplicationName(APPLICATION_NAME).build();
	  }

	// ------------------------------------------------------
	// GA Data Feed
	// ------------------------------------------------------
	// first build the query

	public static GaData makeRequest(String startDate, String endDate, String metrics, String dimensions, String sort, String filters) throws Exception {

		numQueries ++;
		
    return analytics.data().ga()
        .get("ga:" + PROFILE_ID, startDate, endDate, metrics)
        .setDimensions(dimensions)
        .setSort(sort)
        .setFilters(filters)
        .execute();
			  
	}


}