package co.beek.pano.model.beans;

import org.primefaces.event.map.OverlaySelectEvent;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.Marker;

public class BeekMapModel extends DefaultMapModel {
	private static final long serialVersionUID = -9174496319896786210L;

	public BeekMapModel(double lat, double lng) {
		LatLng latlng = new LatLng(lat, lng);
		
		Marker marker = new Marker(latlng, "mapMarker");
		marker.setDraggable(true);
		
		addOverlay(marker);
	}
}