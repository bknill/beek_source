package co.beek.pano.model.beans;

//import org.primefaces.component.menuitem.MenuItem;
//import org.primefaces.model.DefaultMenuModel;
//import org.primefaces.model.MenuModel;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
//
//import javax.faces.event.AbortProcessingException;
//import javax.faces.event.ActionEvent;
//import javax.faces.event.ActionListener;
import java.io.Serializable;

@Component
@Scope("session")
public class BreadCrumBean implements Serializable {
//    private MenuModel menuModel;
//
//    public BreadCrumBean() {
//        menuModel = new DefaultMenuModel();
//        MenuItem homeMunu = new MenuItem();
//        homeMunu.setId("homeId");
//        homeMunu.setValue("Home");
//        homeMunu.addActionListener(new ActionHelper());
//        homeMunu.setUrl("/admin/views/home.xhtml");
//        menuModel.addMenuItem(homeMunu);
//    }
//
//    public void buildBreadCrumMenus(String phase, boolean forward, Long id) {
//        if (phase.equals("Destination")) {
//            if (forward) {
//                addDestinationMenu();
//            } else {
//                removeUptoLocationMenu();
//            }
//        } else if (phase.equals("Location")) {
//            if (forward) {
//                addLocationMenu(id);
//            } else {
//                removeUptoSceneMenu();
//            }
//        } else if (phase.equals("Scene")) {
//            if (forward) {
//                addSceneMenu(id);
//            }
//        }
//    }
//
//    private void addDestinationMenu() {
//        MenuItem destMunu = new MenuItem();
//        destMunu.setId("destId");
//        destMunu.setValue("Destinations");
//        destMunu.setUrl("/admin/views/destinations.xhtml");
//        menuModel.addMenuItem(destMunu);
//    }
//
//    private void removeUptoDestinationMenu() {
//        menuModel.getMenuItems().remove(3);
//        menuModel.getMenuItems().remove(2);
//        menuModel.getMenuItems().remove(1);
//    }
//
//    private void addLocationMenu(Long id) {
//        MenuItem loctMunu = new MenuItem();
//        loctMunu.setId("locId");
//        loctMunu.setValue("Locations");
//        loctMunu.setUrl("/admin/views/locations.xhtml?id=" + id);
//        menuModel.addMenuItem(loctMunu);
//    }
//
//    private void removeUptoLocationMenu() {
//        menuModel.getMenuItems().remove(3);
//        menuModel.getMenuItems().remove(2);
//    }
//
//    private void addSceneMenu(Long id) {
//        MenuItem sceneMunu = new MenuItem();
//        sceneMunu.setId("sceneId");
//        sceneMunu.setValue("Scenes");
//        sceneMunu.setUrl("/admin/views/scenes.xhtml?id=" + id);
//        menuModel.addMenuItem(sceneMunu);
//    }
//
//    private void removeUptoSceneMenu() {
//        menuModel.getMenuItems().remove(3);
//    }
//
//    public MenuModel getMenuModel() {
//        return menuModel;
//    }
//    private class ActionHelper implements ActionListener,Serializable{
//
//        @Override
//        public void processAction(ActionEvent actionEvent) throws AbortProcessingException {
//            System.out.println(actionEvent.toString());
//        }
//    }
}