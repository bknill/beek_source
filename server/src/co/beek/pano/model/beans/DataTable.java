package co.beek.pano.model.beans;

import java.util.ArrayList;

public class DataTable extends ArrayList<DataListRow> {

	public void addRow(String val1, String val2) {
		add(new DataListRow(val1, val2));
	}

	public void addRow(String val1, int val2) {
		add(new DataListRow(val1, Integer.toString(val2)));
	}

	public void addRow(String val1, long val2) {
		add(new DataListRow(val1, Long.toString(val2)));
	}

}
