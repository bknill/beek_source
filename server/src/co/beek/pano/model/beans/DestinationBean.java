package co.beek.pano.model.beans;

import co.beek.pano.model.dao.entities.Destination;
import co.beek.pano.service.dataService.destinationService.DestinationService;
import org.hibernate.HibernateException;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SelectableDataModel;
import org.primefaces.model.SortOrder;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class DestinationBean extends LazyDataModel<Destination> implements SelectableDataModel<Destination>, Serializable {
    private int rows;
    private int first;
    private int currentPage;
    private DestinationService destinationService;

    public DestinationBean(DestinationService service) {
        rows = 10;
        //setRowCount((int) service.countDestinations());
        this.destinationService = service;
    }

//    @Override
//    public List<Destination> load(int f, int p, String sortField, SortOrder sortOrder, Map<String, String> stringStringMap) {
//        try {
//           // List<Destination> destinationsList = destinationService.getDestinations(f, p);
//           // return destinationsList;
//        	return null;
//        } catch (HibernateException e) {
//            e.printStackTrace();
//            setRowCount(0);
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Database Error", "Error occurred while getting destinations"));
//            return null;
//        }
//    }

    @Override
    public String getRowKey(Destination destination) {
        if (destination == null) return null;
        return destination.getId();
    }

    @Override
    public Destination getRowData(String rowKey) {
        if (rowKey == null || rowKey.equals("null")) return null;
        int key = Integer.parseInt(rowKey);
        List<Destination> dataSource = (List<Destination>) getWrappedData();
        for (Destination destination : dataSource) {
            if (destination.getId() == rowKey)
                return destination;
        }
        return null;
    }

    public int getFirst() {
        return first;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }
}
