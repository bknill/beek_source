package co.beek.pano.model.beans;

import org.primefaces.event.map.OverlaySelectEvent;
import org.primefaces.event.map.MarkerDragEvent;
import org.primefaces.event.map.PointSelectEvent;
import org.primefaces.event.map.StateChangeEvent;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;

import java.io.Serializable;

public class GMapBean implements Serializable {
    private int zoom;
    private double lat;
    private double lng;
    private MapModel mapModel;

    public GMapBean() {
        mapModel = new DefaultMapModel();
        LatLng latlng = new LatLng(0, 0);
        mapModel.addOverlay(new Marker(latlng, ""));
        for(Marker marker : mapModel.getMarkers()) {
            marker.setDraggable(true);
        }
        zoom = 13;
        lat = -41.283;
        lng = 174.783;
    }

    public void onStateChange(StateChangeEvent event) {
        zoom = event.getZoomLevel();
        lat = event.getCenter().getLat();
        lng = event.getCenter().getLng();
    }

    public void onPointSelect(PointSelectEvent event) {
        lat = event.getLatLng().getLat();
        lng = event.getLatLng().getLng();
    }

    public void onMarkerDrag(MarkerDragEvent event) {
        lat = event.getMarker().getLatlng().getLat();
        lng = event.getMarker().getLatlng().getLng();
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public int getZoom() {
        return zoom;
    }

    public void setZoom(int zoom) {
        this.zoom = zoom;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public MapModel getMapModel() {
        return mapModel;
    }
}