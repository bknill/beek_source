package co.beek.pano.model.beans;

import java.io.Serializable;

/**
 * Created by Ben on 15/09/2015.
 */
public class GameTaskHotspot implements Serializable {
    private static final long serialVersionUID = 1L;

    public GameTaskHotspot(){

    }

    private String id;

    private String title;

    private Boolean visible;

    public Boolean selected;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }
}
