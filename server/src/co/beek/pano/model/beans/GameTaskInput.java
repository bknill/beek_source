package co.beek.pano.model.beans;

import co.beek.pano.model.dao.entities.GameChoice;

import javax.persistence.Entity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ben on 6/09/2017.
 */
@Entity
public class GameTaskInput  implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;

    public String VideoTime;
    public String VoiceTime;
    public ArrayList<GameTaskHotspot> hotspots = new ArrayList<GameTaskHotspot>();
    public ArrayList<GameChoice> choices = new ArrayList<GameChoice>();
    public ArrayList<String> userIds = new ArrayList<String>();
    public String voiceFile;

    public String getVideoTime() {
        return VideoTime;
    }

    public void setVideoTime(String videoTime) {
        VideoTime = videoTime;
    }

    public String getVoiceTime() {
        return VoiceTime;
    }

    public void setVoiceTime(String voiceTime) {
        VoiceTime = voiceTime;
    }

    public String getVoiceFile() {
        return voiceFile;
    }

    public void setVoiceFile(String voiceFile) {
        this.voiceFile = voiceFile;
    }

    public ArrayList<GameTaskHotspot> getHotspots() {
        return hotspots;
    }

    public void setHotspots(ArrayList<GameTaskHotspot> hotspots) {
        this.hotspots = hotspots;
    }

    public ArrayList<GameChoice> getChoices() {
        return choices;
    }

    public void setChoices(ArrayList<GameChoice> choices) {
        this.choices = choices;
    }

    public ArrayList<String> getUserIds() {
        return userIds;
    }

    public void setUserIds(ArrayList<String> userIds) {
        this.userIds = userIds;
    }
}
