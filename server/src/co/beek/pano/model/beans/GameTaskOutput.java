package co.beek.pano.model.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ben on 15/09/2015.
 */
public class GameTaskOutput implements Serializable {

    public ArrayList<GameTaskHotspot> hotspots = new ArrayList<GameTaskHotspot>();
    private String wrongAnswerSceneId;
    private String wrongAnswerTaskId;

    private String rightAnswerSceneId;
    private String rightAnswerTaskId;
    private String rightAnswerDescription;
    private String rightAnswerVoiceFile;
    private String rightAnswerVideoTime;

    private String wrongAnswerDescription;
    private String wrongAnswerVoiceFile;
    private String wrongAnswerVideoTime;


    public GameTaskOutput (){

    }

    public ArrayList<GameTaskHotspot> getHotspots() {
        return hotspots;
    }

    public void setHotspots(ArrayList<GameTaskHotspot> hotspots) {
        this.hotspots = hotspots;
    }

    public String getWrongAnswerSceneId() {
        return wrongAnswerSceneId;
    }

    public void setWrongAnswerSceneId(String wrongAnswerSceneId) {
        this.wrongAnswerSceneId = wrongAnswerSceneId;
    }

    public String getWrongAnswerTaskId() {
        return wrongAnswerTaskId;
    }

    public void setWrongAnswerTaskId(String wrongAnswerTaskId) {
        this.wrongAnswerTaskId = wrongAnswerTaskId;
    }

    public String getWrongAnswerVoiceFile() {
        return wrongAnswerVoiceFile;
    }

    public void setWrongAnswerVoiceFile(String wrongAnswerVoiceFile) {
        this.wrongAnswerVoiceFile = wrongAnswerVoiceFile;
    }

    public String getRightAnswerSceneId() {
        return rightAnswerSceneId;
    }

    public void setRightAnswerSceneId(String rightAnswerSceneId) {
        this.rightAnswerSceneId = rightAnswerSceneId;
    }

    public String getRightAnswerTaskId() {
        return rightAnswerTaskId;
    }

    public void setRightAnswerTaskId(String rightAnswerTaskId) {
        this.rightAnswerTaskId = rightAnswerTaskId;
    }

    public String getRightAnswerDescription() {
        return rightAnswerDescription;
    }

    public void setRightAnswerDescription(String rightAnswerDescription) {
        this.rightAnswerDescription = rightAnswerDescription;
    }

    public String getRightAnswerVoiceFile() {
        return rightAnswerVoiceFile;
    }

    public void setRightAnswerVoiceFile(String rightAnswerVoiceFile) {
        this.rightAnswerVoiceFile = rightAnswerVoiceFile;
    }

    public String getWrongAnswerDescription() {
        return wrongAnswerDescription;
    }

    public void setWrongAnswerDescription(String wrongAnswerDescription) {
        this.wrongAnswerDescription = wrongAnswerDescription;
    }

    public String getRightAnswerVideoTime() {
        return rightAnswerVideoTime;
    }

    public void setRightAnswerVideoTime(String rightAnswerVideoTime) {
        this.rightAnswerVideoTime = rightAnswerVideoTime;
    }

    public String getWrongAnswerVideoTime() {
        return wrongAnswerVideoTime;
    }

    public void setWrongAnswerVideoTime(String wrongAnswerVideoTime) {
        this.wrongAnswerVideoTime = wrongAnswerVideoTime;
    }
}
