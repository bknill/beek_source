package co.beek.pano.model.beans;

import co.beek.pano.service.dataService.guideService.GuideService;

import java.io.Serializable;

public class GuideViewBean implements Serializable{
    private GuideService guideService;

    public GuideViewBean(GuideService service) {
        this.guideService = service;
    }
}
