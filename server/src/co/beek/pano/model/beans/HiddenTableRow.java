package co.beek.pano.model.beans;

public class HiddenTableRow {
	
	private String elementTitle;
	private int timesClicked;
	private int timesConverted;
	
	public HiddenTableRow (String elementTitle, int timesClicked )
	{
		this.elementTitle = elementTitle;
		this.timesClicked = timesClicked;
		this.timesConverted = 0;
				
	}
	
	public HiddenTableRow (String elementTitle, int timesClicked, int timesConverted )
	{
		this.elementTitle = elementTitle;
		this.timesClicked = timesClicked;
		this.timesConverted = timesConverted;
				
	}



	public String getElementTitle() {
		return elementTitle;
	}

	public void setElementTitle(String elementTitle) {
		this.elementTitle = elementTitle;
	}

	public int getTimesClicked() {
		return timesClicked;
	}

	public void setTimesClicked(int timesClicked) {
		this.timesClicked = timesClicked;
	}
	
	public int getTimesConverted() {
		return timesConverted;
	}
	
	public void setTimesConverted(int timesConverted) {
		this.timesConverted = timesConverted;
	}
}
