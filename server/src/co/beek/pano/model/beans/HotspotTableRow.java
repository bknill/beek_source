package co.beek.pano.model.beans;

public class HotspotTableRow {
	
	private String start;
	private String end;
	private int timesChosen;
	private String type;
	
	public HotspotTableRow(String start,  String end, String type, int timesChosen) {
		this.start = start;
		this.end = end;
		this.timesChosen = timesChosen;
		this.type = type;
	}


	public String getStart() {
		return start;
	}


	public void setStart(String start) {
		this.start = start;
	}


	public String getEnd() {
		return end;
	}


	public void setEnd(String end) {
		this.end = end;
	}


	public int getTimesChosen() {
		return timesChosen;
	}

	public void setTimesChosen(int timesChosen) {
		this.timesChosen = timesChosen;
	}
	
	public void addTimesChosen ( int additionalTimesChosen)
	{
		this.timesChosen += additionalTimesChosen;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((end == null) ? 0 : end.hashCode());
		result = prime * result + ((start == null) ? 0 : start.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof HotspotTableRow))
			return false;
		HotspotTableRow other = (HotspotTableRow) obj;
		if (end == null) {
			if (other.end != null)
				return false;
		} else if (!end.equals(other.end))
			return false;
		if (start == null) {
			if (other.start != null)
				return false;
		} else if (!start.equals(other.start))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}


	public String toString(){
		return "Path: "+this.start+" to "+this.end+" Type: "+this.type+" times chosen: "+timesChosen;
	}
	
	

}
