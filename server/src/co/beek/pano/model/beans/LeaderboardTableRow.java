package co.beek.pano.model.beans;



public class LeaderboardTableRow {
	
	String id;
	String title;
	String location;
	


	int conversions;
	int visits;
	double time;
	
	/**
	 * Contructor for guides
	 * @param id
	 * @param guide
	 * @param conversions
	 * @param visits
	 * @param time
	 */
	public LeaderboardTableRow(String id, String title, int conversions, int visits, double time) {
		this.id = id;
		this.title = title;
		this.conversions = conversions;
		this.visits = visits;
		this.time = time;
	}
	/**
	 * Contructor for scenes
	 * @param id
	 * @param title
	 * @param location
	 * @param conversions
	 * @param visits
	 * @param time
	 */
	public LeaderboardTableRow(String id, String title, String location,  int conversions, int visits, double time) {
		this.id = id;
		this.title = title;
		this.location = location;
		this.conversions = conversions;
		this.visits = visits;
		this.time = time;
	}

	/**
	 * Returns the score of a guide. 
	 * The formula is:
	 * Conversions^(6/5) + Visits^(4/5) + Average Time Spent^(3/5)
	 * and it is rounded to the nearest integer
	 */
	public int getScore(){
		return (int) Math.floor( Math.pow((double)conversions, 1.2) 
				+ Math.pow((double)visits, .8) 
				+ Math.pow(time/(double)visits, .6) + .5 );
	}
	
	public void setid(String id) {
		this.id = id;
	}
	
	public String getid() {
		return id;
	}

	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}


	public int getConversions() {
		return conversions;
	}


	public void setConversions(int conversions) {
		this.conversions = conversions;
	}


	public int getVisits() {
		return visits;
	}


	public void setVisits(int visits) {
		this.visits = visits;
	}


	public double getTime() {
		return time;
	}
	
	public double getAverageTime() {
		return time/(double)visits;
	}


	public void setTime(double time) {
		this.time = time;
	}
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
	public String toString(){
		return this.id+"-"+this.title+": conversions"+this.conversions+"visits"+this.visits;
	}
	
	

}
