package co.beek.pano.model.beans;

import co.beek.pano.model.dao.entities.Location;
import co.beek.pano.service.dataService.locationService.LocationService;
import org.hibernate.HibernateException;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SelectableDataModel;
import org.primefaces.model.SortOrder;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class LocationBean extends LazyDataModel<Location> implements
		SelectableDataModel<Location>, Serializable {
	private int rows;
	private int first;
	private int currentPage;
	private String destinationId;
	private LocationService locationService;

	public LocationBean(LocationService service) {
		rows = 10;
		this.locationService = service;
	}

//	@Override
//	public List<Location> load(int f, int p, String s, SortOrder sortOrder,
//			Map<String, String> stringStringMap) {
//		try {
//			// List<Location> locationsList = locationService.getLocations(f, p,
//			// destinationId);
//			// return locationsList;
//			return null;
//		} catch (HibernateException e) {
//			e.printStackTrace();
//			setRowCount(0);
//			FacesContext.getCurrentInstance().addMessage(
//					null,
//					new FacesMessage("Database Error",
//							"Error occurred while getting locations"));
//			return null;
//		}
//	}

	@Override
	public Long getRowKey(Location location) {
		if (location == null)
			return null;
		return Long.parseLong(location.getId());
	}

	@Override
	public Location getRowData(String rowKey) {
		if (rowKey == null || rowKey.equals("null"))
			return null;
		int key = Integer.parseInt(rowKey);
		List<Location> dataSource = (List<Location>) getWrappedData();
		for (Location location : dataSource) {
			if (location.getId() == rowKey)
				return location;
		}
		return null;
	}

	public int getFirst() {
		return first;
	}

	public void setFirst(int first) {
		this.first = first;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public String getDestinationId() {
		return destinationId;
	}

	public void setDestinationId(String destinationId) {
		this.destinationId = destinationId;
	}
}
