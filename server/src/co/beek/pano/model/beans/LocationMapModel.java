package co.beek.pano.model.beans;

import org.primefaces.event.map.OverlaySelectEvent;
import org.primefaces.event.map.MarkerDragEvent;
import org.primefaces.event.map.PointSelectEvent;
import org.primefaces.event.map.StateChangeEvent;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.Marker;

import co.beek.pano.model.dao.entities.Location;

public class LocationMapModel extends DefaultMapModel {
	private static final long serialVersionUID = -9174496319896786210L;

	private Location location;

	public LocationMapModel(Location location) {
		super();
		this.location = location;

		LatLng latlng = new LatLng(0,0);
		addOverlay(new Marker(latlng, "mapMarker"));
		for (Marker marker : getMarkers()) {
			marker.setDraggable(true);
		}
	}
	
	/**
	 * Needed for searching and other kinds of changes in the map.
	 * @param event
	 */
	public void onStateChange(StateChangeEvent event) {
		location.setLatitude(event.getCenter().getLat());
		location.setLongitude(event.getCenter().getLng());
	}

	public void onPointSelect(PointSelectEvent event) {
		location.setLatitude(event.getLatLng().getLat());
		location.setLongitude(event.getLatLng().getLng());
	}

	public void onMarkerDrag(MarkerDragEvent event) {
		location.setLatitude(event.getMarker().getLatlng().getLat());
		location.setLongitude(event.getMarker().getLatlng().getLng());
	}
}