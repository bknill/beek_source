package co.beek.pano.model.beans;

import java.util.HashMap;

public class ReferralSceneTableRow {

	private String title;
	private String vehicle;
	private int timesClicked;
	private long sceneId;
	private HashMap<String,String> vehicleLookup;
	
	public ReferralSceneTableRow (long sceneId, String title, String vehicle, int timesClicked) {
		
		this.makeLookup();
		this.sceneId = sceneId;
		this.title = title;
		this.vehicle = vehicleLookup.get(vehicle);
		this.timesClicked = timesClicked;
		
	}
	
	public ReferralSceneTableRow ( String vehicle, int timesClicked) {
		
		this.makeLookup();
		this.vehicle = vehicleLookup.get(vehicle);
		this.timesClicked = timesClicked;
		
	}
	
	private void makeLookup () {
		vehicleLookup = new HashMap<String, String>();
		vehicleLookup.put("bubble", "Clicked on a bubble leading to your scene");
		vehicleLookup.put("location_page", "Clicked on your scene in the location page");
		vehicleLookup.put("destination_page", "Clicked on your scene in the destination page");
		vehicleLookup.put("sign_board", "Clicked on a sign board leading to your scene");
		vehicleLookup.put("sign_post", "Clicked on a a sign post leading to your scene");
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getVehicle() {
		return vehicle;
	}

	public void setVehicle(String vehicle) {
		this.vehicle = vehicle;
	}

	public int getTimesClicked() {
		return timesClicked;
	}

	public void setTimesClicked(int timesClicked) {
		this.timesClicked = timesClicked;
	}
	
	public long getSceneId() {
		return sceneId;
	}

	public void setSceneId(long sceneId) {
		this.sceneId = sceneId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((vehicle == null) ? 0 : vehicle.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ReferralSceneTableRow))
			return false;
		ReferralSceneTableRow other = (ReferralSceneTableRow) obj;
		if (vehicle == null) {
			if (other.vehicle != null)
				return false;
		} else if (!vehicle.equals(other.vehicle))
			return false;
		return true;
	}








}
