package co.beek.pano.model.beans;

import co.beek.pano.model.dao.entities.Scene;
import co.beek.pano.service.dataService.sceneService.SceneService;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SelectableDataModel;

import java.io.Serializable;
import java.util.List;

public class SceneBean extends LazyDataModel<Scene> implements SelectableDataModel<Scene>, Serializable {
    private int rows;
    private int first;
    private int currentPage;
    private long locationId;
    private SceneService sceneService;

    public SceneBean(SceneService service) {
        rows = 10;
        this.sceneService = service;
    }

//    @Override
//    public List<Scene> load(int f, int p, String s, SortOrder sortOrder, Map<String, String> stringStringMap) {
//        try{
//            List<Scene> scenesList = sceneService.getScenes(f, p, locationId);
//            return scenesList;
//        }catch(HibernateException e){
//            e.printStackTrace();
//            setRowCount(0);
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Error","Database error occurred while getting scenes"));
//            return null;
//        }
//    }

    @Override
    public String getRowKey(Scene scene) {
        if (scene == null) return null;
        return scene.getId();
    }

    @Override
    public Scene getRowData(String rowKey) {
        if (rowKey == null || rowKey.equals("null")) return null;
        int key = Integer.parseInt(rowKey);
        List<Scene> dataSource = (List<Scene>) getWrappedData();
        for (Scene scene : dataSource) {
            if (scene.getId() == rowKey)
                return scene;
        }
        return null;
    }

    public int getFirst() {
        return first;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public void setLocationId(long locationId) {
        this.locationId = locationId;
    }

    public long getLocationId() {
        return locationId;
    }
}
