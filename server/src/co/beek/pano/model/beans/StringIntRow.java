package co.beek.pano.model.beans;

public class StringIntRow {
	private String first;
	private int second;

	public StringIntRow(String first, int second) {
		this.first = first;
		this.second = second;
	}

	public String getFirst() {
		return first;
	}

	public void setFirst(String first) {
		this.first = first;
	}

	public int getSecond() {
		return second;
	}

	public void setSecond(int second) {
		this.second = second;
	}

}
