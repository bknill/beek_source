package co.beek.pano.model.beans;

import java.util.Comparator;

/**
 * Sorts descending by the second column of list of table row utils
 *
 */
public class TableRowUtilComparator implements Comparator<TableRowUtil> {
    public int compare(TableRowUtil r1, TableRowUtil r2) {
        return Integer.parseInt(r2.getC2()) - Integer.parseInt(r1.getC2() );
    }
}