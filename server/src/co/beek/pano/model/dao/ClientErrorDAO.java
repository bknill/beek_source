package co.beek.pano.model.dao;

import co.beek.pano.model.dao.entities.ClientError;

public interface ClientErrorDAO {
	public void logError(ClientError error);
}
