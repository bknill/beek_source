package co.beek.pano.model.dao;

import javax.inject.Inject;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import co.beek.pano.model.dao.entities.ClientError;

@Repository
@Scope("prototype")
@Transactional
public class ClientErrorDAOImpl implements ClientErrorDAO {
    @Inject
    private SessionFactory sessionFactory;


    @Override
    public void logError(ClientError error) throws HibernateException {
        sessionFactory.getCurrentSession().save(error);
    }
}