package co.beek.pano.model.dao;

import java.util.List;

import javax.inject.Inject;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.beek.pano.model.dao.entities.Destination;

@Repository
@Scope("prototype")
public class DestinationDAOImpl implements DestinationDAO {
	@Inject
	private SessionFactory sessionFactory;

	@Override
	public Destination getDestination(String destinationId)
			throws HibernateException {
		return (Destination) sessionFactory.getCurrentSession().get(
				Destination.class, destinationId);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Destination> getAllDestinations() {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"from Destination d");
		return (List<Destination>) query.list();
	}

	@Override
	public Destination getDestinationByTitle(String title)
			throws HibernateException {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"from Destination d where d.title like :title");
		query.setParameter("title", '%' + title + '%');
		@SuppressWarnings("unchecked")
		List<Destination> result = (List<Destination>) query.list();

		if (result.size() > 0)
			return result.get(0);

		return null;
	}

	@Override
	public void addDestination(Destination destination)
			throws HibernateException {
		sessionFactory.getCurrentSession().save(destination);
	}

	@Override
	public void updateDestination(Destination destination)
			throws HibernateException {
		sessionFactory.getCurrentSession().update(destination);
	}

	@Override
	public void deleteDestination(Destination destination)
			throws HibernateException {
		sessionFactory.getCurrentSession().delete(destination);
	}

//	@Override
//	public long countDestinations() throws HibernateException {
//		Query query = sessionFactory.getCurrentSession().createQuery(
//				"select count(*) from Destination d");
//		return (Long) query.uniqueResult();
//	}
//
//	@Override
//	public List<Destination> getDestinations(int first, int pageSize)
//			throws HibernateException {
//		Query query = sessionFactory
//				.getCurrentSession()
//				.createQuery(
//						"select new Destination(d.id,d.title) from Destination d order by d.id");
//		query.setFirstResult(first);
//		query.setMaxResults(pageSize);
//		return (List<Destination>) query.list();
//	}

}
