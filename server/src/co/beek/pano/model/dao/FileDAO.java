package co.beek.pano.model.dao;

import co.beek.pano.model.dao.entities.FileData;

import java.util.List;

import org.hibernate.HibernateException;

public interface FileDAO {
    public List<FileData> getPublicFiles();

    public void addFile(FileData file);
}
