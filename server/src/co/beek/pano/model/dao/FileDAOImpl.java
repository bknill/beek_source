package co.beek.pano.model.dao;

import java.util.List;

import javax.inject.Inject;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.beek.pano.model.dao.enterprizewizard.EWTeam;
import co.beek.pano.model.dao.entities.FileData;

@Repository
@Scope("prototype")
public class FileDAOImpl implements FileDAO {
    @Inject
    private SessionFactory sessionFactory;

    @Override
    @SuppressWarnings("unchecked")
    public List<FileData> getPublicFiles() throws HibernateException {
        Query query = sessionFactory.getCurrentSession().createQuery(
                "from FileData f where f.teamId = :id");
		query.setParameter("id", EWTeam.TEAM_SUPER_USER);

        return (List<FileData>) query.list();
    }

    @Override
    public void addFile(FileData file) throws HibernateException {
        sessionFactory.getCurrentSession().save(file);
    }
}
