package co.beek.pano.model.dao;

import co.beek.pano.model.dao.entities.GameTask;

/**
 * Created by Ben on 22/10/2015.
 */
public interface GameTaskDAO  {

    public GameTask getGameTask(String taskId);

    public void deleteGameTask(GameTask task);

}
