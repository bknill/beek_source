package co.beek.pano.model.dao;

import co.beek.pano.model.dao.entities.GameTask;
import co.beek.pano.model.dao.entities.Guide;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;

/**
 * Created by Ben on 22/10/2015.
 */
@Repository
@Scope("prototype")
public class GameTaskDAOImpl implements GameTaskDAO {

    @Inject
    private SessionFactory sessionFactory;

    @Override
    public GameTask getGameTask(String taskId) throws HibernateException {
        return (GameTask) sessionFactory.getCurrentSession().get(GameTask.class,
                taskId
        );
    }

    @Override
    public void deleteGameTask(GameTask task){
        sessionFactory.getCurrentSession().merge(task);
    }

}
