package co.beek.pano.model.dao;

import java.util.List;

import co.beek.pano.model.dao.entities.Guide;
import co.beek.pano.model.dao.entities.GuideDetail;
import co.beek.pano.model.dao.entities.GuideReport;
import co.beek.pano.model.dao.entities.GuideScene;
import co.beek.pano.model.dao.entities.GuideSection;

public interface GuideDAO {
    public Guide getGuide(String guideId);

    public GuideDetail getGuideDetail(String guideId);

    public GuideReport getGuideReport(String guideId);

    public void addGuide(GuideDetail guide);
    
    public void updateGuide(GuideDetail guide);

    public void updateGuide(GuideReport guide);

    public void addGuide(Guide guide);

    public void updateGuide(Guide guide);

    public void deleteGuide(GuideDetail guide);

    public Guide mergeGuide(Guide guide);
    
	
	public List<Guide> getGuidesForDestination(String destinationId);
	
	public List<Guide> getGuidesForTeam(String teamId);

	public List<GuideScene> getGuideScenes(String sceneId);

	public GuideSection getGuideSection(String guideSectionId);

	public List<Guide> getAllGuides();
}
