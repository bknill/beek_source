package co.beek.pano.model.dao;

import java.util.List;

import javax.inject.Inject;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.beek.pano.model.dao.entities.Guide;
import co.beek.pano.model.dao.entities.GuideDetail;
import co.beek.pano.model.dao.entities.GuideReport;
import co.beek.pano.model.dao.entities.GuideScene;
import co.beek.pano.model.dao.entities.GuideSection;
import co.beek.pano.model.dao.entities.Location;

@Repository
@Scope("prototype")
public class GuideDAOImpl implements GuideDAO {

	@Inject
	private SessionFactory sessionFactory;

	@Override
	public Guide getGuide(String guideId) throws HibernateException {
		return (Guide) sessionFactory.getCurrentSession().get(Guide.class,
				guideId);
	}

	@Override
	public GuideDetail getGuideDetail(String guideId) throws HibernateException {
		return (GuideDetail) sessionFactory.getCurrentSession().get(
				GuideDetail.class, guideId);
	}
	
	@Override
	public GuideReport getGuideReport(String guideId) throws HibernateException {
		return (GuideReport) sessionFactory.getCurrentSession().get(
				GuideReport.class, guideId);
	}

	@Override
	public void addGuide(GuideDetail guide) throws HibernateException {
		sessionFactory.getCurrentSession().save(guide);
	}

	@Override
	public void updateGuide(GuideDetail guide) throws HibernateException {
		sessionFactory.getCurrentSession().merge(guide);
	}

	@Override
	public void updateGuide(GuideReport guide) throws HibernateException {
		sessionFactory.getCurrentSession().merge(guide);
	}

	@Override
	public void addGuide(Guide guide) throws HibernateException {
		sessionFactory.getCurrentSession().save(guide);
	}

	@Override
	public void updateGuide(Guide guide) throws HibernateException {
		sessionFactory.getCurrentSession().merge(guide);
	}

	@Override
	public void deleteGuide(GuideDetail guide) throws HibernateException {
		sessionFactory.getCurrentSession().delete(guide);
	}

	@Override
	public Guide mergeGuide(Guide guide) throws HibernateException {
		return (Guide) sessionFactory.getCurrentSession().merge(guide);
	}
	

	@SuppressWarnings("unchecked")
	public List<Guide> getGuidesForDestination(String destinationId) {
		Query query = sessionFactory
				.getCurrentSession()
				.createQuery(
						"from Guide l where l.destinationId = :destinationId order by l.score DESC");
		query.setParameter("destinationId", destinationId);
		return (List<Guide>) query.list();
	}

	@SuppressWarnings("unchecked")
	public List<Guide> getGuidesForTeam(String teamId) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"from Guide l where l.teamId = :teamId");
		query.setParameter("teamId", teamId);
		return (List<Guide>) query.list();
	}

	@SuppressWarnings("unchecked")
	public List<GuideScene> getGuideScenes(String sceneId) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"from GuideScene gs where gs.sceneId = :sceneId");
		query.setParameter("sceneId", sceneId);
		return (List<GuideScene>) query.list();
	}
	
	public GuideSection getGuideSection(String guideSectionId)
	{
		return (GuideSection) sessionFactory.getCurrentSession().get(
				GuideSection.class, guideSectionId);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Guide> getAllGuides() {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"FROM Guide l");
		return (List<Guide>) query.list();
	}
	
}
