package co.beek.pano.model.dao;

import co.beek.pano.model.dao.entities.GuideScene;
import co.beek.pano.model.dao.entities.Scene;
import co.beek.pano.model.dao.entities.SceneDetail;

import java.util.List;

public interface GuideSceneDAO {
    public GuideScene getGuideScene(String guideSceneId);

    public GuideScene updateGuideScene(GuideScene guideScene);

    public GuideScene addGuideScene(GuideScene guideScene);

    public void deleteGuideScene(GuideScene guideScene);

}
