package co.beek.pano.model.dao;

import co.beek.pano.model.dao.entities.GuideScene;
import co.beek.pano.model.dao.entities.Scene;
import co.beek.pano.model.dao.entities.SceneDetail;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.List;

@Repository
@Scope("prototype")
public class GuideSceneDAOImpl implements GuideSceneDAO {
	@Inject
	private SessionFactory sessionFactory;

	@Override
	public GuideScene getGuideScene(String guideSceneId) throws HibernateException {
		return (GuideScene) sessionFactory.getCurrentSession().get(GuideScene.class,
				guideSceneId);
	}


	@Override
	public GuideScene addGuideScene(GuideScene guideScene) throws HibernateException {
		guideScene.setId((String) sessionFactory.getCurrentSession().save(guideScene));
		return  guideScene;
	}

	@Override
	public GuideScene updateGuideScene(GuideScene guideScene) throws HibernateException {
		return (GuideScene) sessionFactory.getCurrentSession().merge(guideScene);
	}

	@Override
	public void deleteGuideScene(GuideScene guideScene) throws HibernateException {
		sessionFactory.getCurrentSession().delete(guideScene);
	}

}