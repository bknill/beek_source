package co.beek.pano.model.dao;

import co.beek.pano.model.dao.entities.Hotspot;


public interface HotspotsDAO {
	public Hotspot getHotspot(String hotspotId);

	public Hotspot addHotspot(Hotspot hotspot);

	public void saveHotspot(Hotspot hotspot);

	public void deleteHotspot(Hotspot hotspot);
}
