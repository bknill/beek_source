package co.beek.pano.model.dao;

import javax.inject.Inject;

import co.beek.pano.model.dao.entities.Hotspot;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.beek.pano.model.dao.hotspots.Photo;

@Repository
@Scope("prototype")
public class HotspotsDAOImpl implements HotspotsDAO {
	@Inject
	private SessionFactory sessionFactory;

	@Override
	public Hotspot getHotspot(String hotspotId) throws HibernateException {
		return (Hotspot) sessionFactory.getCurrentSession().get(Hotspot.class,
				hotspotId);
	}

	@Override
	public Hotspot addHotspot(Hotspot hotspot)  throws HibernateException {
		hotspot.setId((String) sessionFactory.getCurrentSession().save(hotspot));
		return hotspot;
	}

	@Override
	public void saveHotspot(Hotspot hotspot) {
		sessionFactory.getCurrentSession().merge(hotspot);
	}

	@Override
	public void deleteHotspot(Hotspot hotspot) {
		sessionFactory.getCurrentSession().delete(hotspot);
	}

}
