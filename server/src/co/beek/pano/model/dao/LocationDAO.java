package co.beek.pano.model.dao;

import java.awt.Rectangle;
import java.util.List;

import co.beek.pano.model.dao.entities.Location;

public interface LocationDAO {
    public Location getLocation(String locationId);
    
    public Location getLocationForOrder(String photoShootId);

    public void addLocation(Location location);

    public void updateLocation(Location location);

    public void deleteLocation(Location location);

    public List<Location> getLocationsForDestination(String destinationId);

    public List<Location> getLocations(Rectangle rect);
    
    public List<Location> getLocations(double minLat, double minLon, double maxLat, double maxLon);

    public List<Location> getLocations(String teamId);
    
    public List<Location> getAllLocations();

}
