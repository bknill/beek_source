package co.beek.pano.model.dao;

import java.awt.Rectangle;
import java.util.List;

import javax.inject.Inject;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.beek.pano.model.dao.entities.Destination;
import co.beek.pano.model.dao.entities.Location;

@Repository
@Scope("prototype")
public class LocationDAOImpl implements LocationDAO {
	@Inject
	private SessionFactory sessionFactory;

	@Override
	public Location getLocation(String locationId) throws HibernateException {
		return (Location) sessionFactory.getCurrentSession().get(
				Location.class, locationId);
	}


	@Override
	public Location getLocationForOrder(String photoShootId)
			throws HibernateException {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"from Location l where l.photoShootId like :psId");
		query.setParameter("psId", photoShootId);

		@SuppressWarnings("rawtypes")
		List list = query.list();
		if (list.size() > 0)
			return (Location) list.get(0);

		return null;
	}

	@Override
	public void addLocation(Location location) throws HibernateException {
		sessionFactory.getCurrentSession().save(location);
	}

	@Override
	public void updateLocation(Location location) throws HibernateException {
		sessionFactory.getCurrentSession().merge(location);
	}

	@Override
	public void deleteLocation(Location location) throws HibernateException {
		sessionFactory.getCurrentSession().delete(location);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Location> getLocations(Rectangle rect)
			throws HibernateException {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"from Location l ");
		return (List<Location>) query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Location> getLocations(double minLat, double minLon,
			double maxLat, double maxLon) throws HibernateException {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"FROM Location loc WHERE loc.status != :status "
						+ "AND loc.latitude < :minLat "
						+ "AND loc.longitude > :minLon "
						+ "AND loc.latitude > :maxLat "
						+ "AND loc.longitude < :maxLon ");
		query.setParameter("status", Location.STATE_CANCELLED);
		query.setParameter("minLat", minLat);
		query.setParameter("minLon", minLon);
		query.setParameter("maxLat", maxLat);
		query.setParameter("maxLon", maxLon);
		return (List<Location>) query.list();
	}

	@SuppressWarnings("unchecked")
	public List<Location> getLocations(String teamId) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"from Location l where l.teamId = :teamId");
		query.setParameter("teamId", teamId);
		return (List<Location>) query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Location> getLocationsForDestination(String destinationId) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"from Location loc where loc.destinationId = :destId "
						+ "AND loc.status != :status");
		query.setParameter("destId", destinationId);
		query.setParameter("status", Location.STATE_CANCELLED);
		return (List<Location>) query.list();
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<Location> getAllLocations() {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"from Location l where l.status > -1");
		return (List<Location>) query.list();
	}

}