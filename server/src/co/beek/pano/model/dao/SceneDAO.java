package co.beek.pano.model.dao;

import co.beek.pano.model.dao.entities.Scene;
import co.beek.pano.model.dao.entities.SceneDetail;

import java.util.List;

public interface SceneDAO {
    public Scene getScene(String sceneId);

    public SceneDetail getSceneDetail(String sceneId);

    public Scene addScene(Scene scene);

    public Scene updateScene(Scene scene);

    public void updateSceneDetail(SceneDetail scene);

    public void deleteScene(Scene scene);

    public long countScenes(long locationId);

    public List<Scene> getScenes(int first, int pageSize, long locationId);

    public List<Scene> getScenes(String locationId);
    
    public List<Scene> getAllScenes(String locationId);

    public List<Scene> getAllApprovedScenes(String locationId);

    public List<Scene> getScenes(double minLat, double minLon, double maxLat, double maxLon);

}
