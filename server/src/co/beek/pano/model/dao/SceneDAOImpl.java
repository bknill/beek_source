package co.beek.pano.model.dao;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import co.beek.pano.model.dao.entities.GuideScene;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.beek.pano.model.dao.entities.Scene;
import co.beek.pano.model.dao.entities.SceneDetail;

@Repository
@Scope("prototype")
public class SceneDAOImpl implements SceneDAO {
	@Inject
	private SessionFactory sessionFactory;

	@Override
	public Scene getScene(String sceneId) throws HibernateException {
		return (Scene) sessionFactory.getCurrentSession().get(Scene.class,
				sceneId);
	}

	@Override
	public SceneDetail getSceneDetail(String sceneId) throws HibernateException {
		return (SceneDetail) sessionFactory.getCurrentSession().get(
				SceneDetail.class, sceneId);
	}

	@Override
	public Scene addScene(Scene scene) throws HibernateException {
		scene.setId((String) sessionFactory.getCurrentSession().save(scene));
		return scene;
	}

	@Override
	public Scene updateScene(Scene scene) throws HibernateException {
		return (Scene) sessionFactory.getCurrentSession().merge(scene);
	}

	@Override
	public void updateSceneDetail(SceneDetail scene) throws HibernateException {
		sessionFactory.getCurrentSession().merge(scene);
	}

	@Override
	public void deleteScene(Scene scene) throws HibernateException {
		sessionFactory.getCurrentSession().delete(scene);
	}

	@Override
	public long countScenes(long locationId) throws HibernateException {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"select count(*) from Scene as s where s.locationId = :locId");
		query.setParameter("locId", locationId);
		return (Long) query.uniqueResult();
	}

	@Override
	public List<Scene> getScenes(int first, int pageSize, long locationId)
			throws HibernateException {
		Query query = sessionFactory
				.getCurrentSession()
				.createQuery(
						"select new Scene(s.id, s.title) from Scene s where s.locationId = :locId order by s.id");
		query.setParameter("locId", locationId);
		query.setFirstResult(first);
		query.setMaxResults(pageSize);
		return (List<Scene>) query.list();
	}

	/**
	 * Returns all the approved scenes for a location
	 */
	@Override
	public List<Scene> getScenes(String locationId) throws HibernateException {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"from Scene s where s.locationId = :locId  "
						+ "AND s.status = :statusApproved  ");
		query.setParameter("locId", locationId);
		query.setParameter("statusApproved",
				Scene.SceneStatus.ACCEPTED.getStatus());
		return (List<Scene>) query.list();
	}

	@Override
	public List<Scene> getAllScenes(String locationId)
			throws HibernateException {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"from Scene s where s.locationId = :locId AND s.status > -1");
		query.setParameter("locId", locationId);
		return (List<Scene>) query.list();
	}

	@Override
	public List<Scene> getAllApprovedScenes(String locationId)
			throws HibernateException {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"from Scene s where s.locationId = :locId AND s.status > 0");
		query.setParameter("locId", locationId);
		return (List<Scene>) query.list();
	}

	@Override
		 public List<Scene> getScenes(double minLat, double minLon, double maxLat,
									  double maxLon) throws HibernateException {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"FROM Scene s " + "WHERE s.latitude > :minLat "
						+ "AND s.longitude > :minLon "
						+ "AND s.latitude < :maxLat "
						+ "AND s.longitude < :maxLon ");
		query.setParameter("minLat", minLat);
		query.setParameter("minLon", minLon);
		query.setParameter("maxLat", maxLat);
		query.setParameter("maxLon", maxLon);
		return (List<Scene>) query.list();
	}

}