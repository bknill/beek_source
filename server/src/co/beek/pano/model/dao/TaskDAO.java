package co.beek.pano.model.dao;

import co.beek.pano.model.dao.entities.ScenePanoTask;

public interface TaskDAO {
    public ScenePanoTask getTaskFromQueue();
    
    public ScenePanoTask getSceneTask(String sceneId);

    public ScenePanoTask getGuideTask(String guideId);

    public void addTaskToQueue(ScenePanoTask task);
    
    public void updateTask(ScenePanoTask queue);

    public void deleteTaskFromQueue(ScenePanoTask task);
}
