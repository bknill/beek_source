package co.beek.pano.model.dao;

import javax.inject.Inject;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import co.beek.pano.model.dao.entities.ScenePanoTask;

@Repository
@Scope("prototype")
public class TaskDAOImpl implements TaskDAO {
	@Inject
	private SessionFactory sessionFactory;

	@Override
	public ScenePanoTask getTaskFromQueue() throws HibernateException {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"select t from ScenePanoTask t WHERE t.status = :status");
		query.setMaxResults(1);
		query.setParameter("status", ScenePanoTask.PENDING);
		return (ScenePanoTask) query.uniqueResult();
	}

	@Override
		 public ScenePanoTask getSceneTask(String sceneId) throws HibernateException {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"FROM ScenePanoTask t WHERE t.sceneId = :sceneId AND t.guideId = null");
		query.setParameter("sceneId", sceneId);
		return (ScenePanoTask) query.uniqueResult();
	}

	@Override
	public ScenePanoTask getGuideTask(String guideId) throws HibernateException {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"FROM ScenePanoTask t WHERE t.guideId = :guideId AND t.sceneId = null");
		query.setParameter("guideId", guideId);
		return (ScenePanoTask) query.uniqueResult();
	}

	@Override
	public void addTaskToQueue(ScenePanoTask queue) throws HibernateException {
		sessionFactory.getCurrentSession().save(queue);
	}

	@Override
	public void updateTask(ScenePanoTask queue) throws HibernateException {
		sessionFactory.getCurrentSession().merge(queue);
	}

	@Override
	public void deleteTaskFromQueue(ScenePanoTask queue)
			throws HibernateException {
		sessionFactory.getCurrentSession().delete(queue);
	}
}