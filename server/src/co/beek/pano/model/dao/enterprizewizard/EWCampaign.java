package co.beek.pano.model.dao.enterprizewizard;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import flexjson.JSON;


public class EWCampaign implements Serializable {

	private static final long serialVersionUID = -8968689243491431989L;
	

	public String campaign_name;
	public String id;
	public String CampaignId;

	
	public String getcampaign_name() {
		return campaign_name;
	}
	
	public void setcampaign_name(String campaign_name) 
	{this.campaign_name=campaign_name;}
	
	public String getCampaignId() {
		return id;
	}
	
	public String getid() {
		return id;
	}
	
	public void setCampaignId(String CampaignId) 
	{this.id=CampaignId;}

}




