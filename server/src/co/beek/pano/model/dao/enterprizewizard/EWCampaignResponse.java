package co.beek.pano.model.dao.enterprizewizard;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import co.beek.pano.model.dao.entities.GuideDetail;

public class EWCampaignResponse extends EWResponse implements Serializable {

	private static final long serialVersionUID = 1502811422513474126L;

	public static String getCampaigns(String username, String password) {
		String url = "http://crm.beek.co/ewws/EWSearch/.json?$KB=BeekOrderly";
		
		url += "&$table=campaign";
		url += "&$login=" + username.trim();
		url += "&$lang=en";
		url += "&$password=" + password.trim();
		url += "&field=id";
		url += "&field=campaign_name";
		
		return url;
	}
	
	public static String newCampaign(String username, String password, String newCampaign) throws UnsupportedEncodingException {
		String url = "http://crm.beek.co/ewws/EWCreate/.json?$KB=BeekOrderly";
		
		url += "&$table=campaign";
		url += "&$login=" + username.trim();
		url += "&$lang=en";
		url += "&$password=" + password.trim();
		url += "&campaign_name=" + URLEncoder.encode(newCampaign, "UTF-8");
		
		return url;
	}

	
	public static String updateCampaignEmail(String username, String password, String id, String email, String subject) throws UnsupportedEncodingException {
		String url = "http://crm.beek.co/ewws/EWUpdate/.json?$KB=BeekOrderly";
		
		url += "&$table=campaign";
		url += "&$login=" + username.trim();
		url += "&$password=" + password.trim();
		url += "&$lang=en";
		url += "&id=" + id;
		url += "&email_body=" + URLEncoder.encode(email, "UTF-8");
		url += "&email_subject=" + URLEncoder.encode(subject, "UTF-8");
		
		return url;
	}
	
	
	
	private List<EWCampaign> Campaigns = new ArrayList<EWCampaign>();

	public void setResult(List<EWCampaign> value) {
		this.Campaigns = value;
	}

	public List<EWCampaign> getCampaigns() {
		return Campaigns;
	}

	public boolean loginSuccessful() {
		return getSuccess() && Campaigns != null && Campaigns.size() > 0;
	}





}
