package co.beek.pano.model.dao.enterprizewizard;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import flexjson.JSON;

/*
 * {
 *       "last_name":"Client",
 *       "DAO_dao3_link":[
 *          {
 *             "f_group":"Customer",
 *             "id":305
 *          }
 *       ],
 *       "DAOcontacts_to_destination":[
 *          {
 *             "destination":"Wellington",
 *             "id":3
 *          },
 *          {
 *             "destination":"Testland",
 *             "id":10
 *          }
 *       ],
 *       "client_token":"CLIENT_5_",
 *       "DAO_dao3_link1":[
 *          {
 *             "teams":"CUSTOMER_4_",
 *             "id":395
 *          }
 *       ],
 *       "contact_history":null,
 *       "f_group":[
 *          "Customer"
 *       ],
 *       "first_name":"Test",
 *       "destination":[
 *          "Wellington",
 *          "Testland"
 *       ],
 *       "type":"Customer",
 *       "teams":[
 *          "CUSTOMER_4_"
 *       ],
 *       "_login":"beek_client",
 *       "id":"385"
 *    }
 */
public class EWContact implements Serializable {
	private static final long serialVersionUID = -2185825134766725997L;

	public static String ADMIN_USER_USERNAME = "gms";
	public static String ADMIN_USER_PASSWORD = "B33kb33k";

	// TODO change these for obscure values
	public static String SUPER_USER_ID = "99";
	public static String SUPER_USER_USERNAME = "ben@beek.co";
	public static String SUPER_USER_PASSWORD = "password";

	public static EWContact getSuperUserContact() {
		// Create a list of teams

		// Create our super user
		EWContact superUser = new EWContact();
		superUser.id = EWContact.SUPER_USER_ID;
		superUser.first_name = "Ben";
		superUser.last_name = "Knill";
		superUser.setType("employees");

		return superUser;
	}



	public String id;

	public String first_name;

	public String last_name;

	public String client_token;

	public String f_role;
	
	public String type;

	public String lat; 
	
	public String lon;
	
	public String email;

	public String welcome_message;

	public String stitchtemplate;
	
	public List<String> guideids = new ArrayList<String>();
	
	public List<String> locationids = new ArrayList<String>();
	
	public List<String> guideid0 = new ArrayList<String>();
	
	public List<String> locationids0 = new ArrayList<String>();

	public List<String> destination = new ArrayList<String>();
	
	public List<EWTeam> DAO_dao3_link1 = new ArrayList<EWTeam>();

	public List<EWGroup> DAO_dao3_link = new ArrayList<EWGroup>();

	private List<EWCustomerToTeam> DAOcontacts_to_customer_teams = new ArrayList<EWCustomerToTeam>();

	public String getFirstName() {
		return first_name;
	}

	public String getLastName() {
		return last_name;
	}
	
	public String getWelcomeMessage() {
		return welcome_message;
	}

	public void setWelcomeMessage(String welcome_message) {
		this.welcome_message = welcome_message;
	}

	/**
	 * Returns true of the EWContact has not had the correct settings applied
	 * email.
	 */
	@JSON(include = false)
	public boolean isIncomplete() {
		// super user is always complete
		if ( isAdmin())
			return false;

		return false; //DAO_dao3_link.size() == 0
				//|| destination.size() == 0; //DAO_dao3_link1.size() == 0 || 
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}
	
	public String getLat() {
		return lat;
	}
	
	public void setLon(String lon) {
		this.lon = lon;
	}
	
	public String getLon() {
		return lon;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public String getType() {
		return type;
	}
	
	public void populateGuideLocationIDs() {
		//guideid = guideid0;
		//locationids = locationids;
	}
	
	
	public void setGuideID(List<String> guideids) {
		this.guideids = guideids;
	}

	public List<String> getGuideID() {
		return guideids;
	}
	
	public void setLocationID(List<String> locationids) {
		this.locationids = locationids;
	}

	public List<String> getLocationsID() {
		return locationids;
	}
	
	public void setGuideID0(List<String> guideid0) {
		this.guideid0 = guideid0;
	}

	public List<String> getGuideID0() {
		return guideid0;
	}
	
	public void setLocationIDs0(List<String> locationids0) {
		this.locationids0 = locationids0;
	}

	public List<String> getLocationIDs0() {
		return locationids0;
	}

	
	public void setDestinations(List<String> destination) {
		this.destination = destination;
	}

	public List<String> getDestinations() {
		return destination;
	}
	

	public List<EWGroup> getGroups() {
		return DAO_dao3_link;
	}

	public void setGroups(List<EWGroup> groups) {
		DAO_dao3_link = groups;
	}

	public List<EWTeam> getTeams() {
		return DAO_dao3_link1;
	}

	public void setEWTeams(List<EWTeam> teams) {
		DAO_dao3_link1 = teams;
	}

	public List<EWCustomerToTeam> getCustomerTeams() {
		return getDAOcontacts_to_customer_teams();
	}

	public void setCustomerTeams(List<EWCustomerToTeam> customerteams) {
		setDAOcontacts_to_customer_teams(customerteams);
	}

	public String getStitchtemplate() {
		return stitchtemplate;
	}

	public void setStitchtemplate(String stitchTemplate) {
		this.stitchtemplate = stitchTemplate;
	}

	@JSON(include = false)
	public boolean isInGroup(String groupId) {
		for (EWGroup group : DAO_dao3_link)
			if (group.getF_group().equals(groupId))
				return true;

		return false;
	}

	@JSON(include = false)
	public boolean isInTeam(String teamId) {
		if (isAdmin())
			return true;

		for (EWTeam team : DAO_dao3_link1)
			if (team.getTeams().equals(teamId))
				return true;

		return false;
	}
	
	public boolean hasGuide(String guideId) {
		if (isAdmin())
			return true;

		if (guideids.contains( guideId ))
				return true;

		return false;
	}
	
	public boolean hasLocation(String locationId) {
		if (isAdmin())
			return true;

		if (locationids.contains( locationId ))
				return true;

		return false;
	}



	@JSON(include = false)
	public void addTeam(EWTeam team) {
		DAO_dao3_link1.add(team);
	}

	@JSON(include = false)
	public boolean isSuperUser() {
		return type.equals("employees");
	}

	@JSON(include = false)
	public boolean isCustomer() {
		return isInGroup(EWGroup.GROUP_CUSTOMER);
		// return f_role.equals(EWGroup.GROUP_CUSTOMER);
	}

	@JSON(include = false)
	public boolean isPhotographer() {
		return isInGroup(EWGroup.GROUP_PHOTOGRAPHER);
		// return f_role.equals(EWGroup.GROUP_PHOTOGRAPHER);
	}

	@JSON(include = false)
	public boolean isAdmin() {
		return isInGroup(EWGroup.GROUP_ADMIN);
		// return f_role.equals(EWGroup.GROUP_ADMIN);
	}

	public String teamsCommaDelimited() {
		List<String> teamIds = new ArrayList<String>();

		for (EWTeam team : DAO_dao3_link1)
			teamIds.add(team.getTeams());

		return StringUtils.join(teamIds, ",");
	}

	public String groupsCommaDelimited() {
		List<String> groupIds = new ArrayList<String>();

		for (EWGroup group : DAO_dao3_link)
			groupIds.add(group.getF_group());

		return StringUtils.join(groupIds, ",");
	}

	public List<EWCustomerToTeam> getDAOcontacts_to_customer_teams() {
		return DAOcontacts_to_customer_teams;
	}

	public void setDAOcontacts_to_customer_teams(
			List<EWCustomerToTeam> dAOcontacts_to_customer_teams) {
		Collections.sort(dAOcontacts_to_customer_teams, TeamComparator.instance);
		DAOcontacts_to_customer_teams = dAOcontacts_to_customer_teams;
	}
}

class TeamComparator implements Comparator<EWCustomerToTeam> {
	
	public static final TeamComparator instance = new TeamComparator();
	
	public int compare(EWCustomerToTeam object1, EWCustomerToTeam object2) {
		return object1.getName().compareTo(object2.getName());
	}
}

