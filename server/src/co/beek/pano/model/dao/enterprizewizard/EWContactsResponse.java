package co.beek.pano.model.dao.enterprizewizard;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import co.beek.pano.model.dao.entities.GuideDetail;
import co.beek.pano.model.dao.entities.Location;

/**
 * { "success":true, "message":"", "result":[ { "first_name":"Sales",
 * "client_token":"CLIENT_5_", "type":"employees", "contact_history":null,
 * "_login":"beek_sales", "DAO_dao3_link1":[ { "teams":"Sales Team", "id":390 },
 * { "teams":"CUSTOMER_4_", "id":395 } ], "teams":[ "Sales Team", "CUSTOMER_4_"
 * ], "last_name":"Guy", "DAO_dao3_link":[ { "f_group":"Sales", "id":300 } ],
 * "f_group":[ "Sales" ], "id":"380" } ] }
 * 
 * @author Daniel
 * 
 */
public class EWContactsResponse extends EWResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	public static String format(String username, String password) {
		String url = "http://crm.beek.co/ewws/EWSearch/.json?$KB=BeekOrderly";
			url += "&$table=contacts&$lang=en";

		url += "&$login=" + username.trim();
		url += "&$password=" + password.trim();
		url += "&query=_login=%27$global.my__login%27";
		//url += "&field=_dao3_link";
		//url += "&field=id";
		url += "&field=first_name";
		url += "&field=last_name";
		//url += "&field=team_token_name";
		//url += "&field=team_token_name";
		//url += "&field=f_group";
		//url += "&field=destination";
		//url += "&field=contacts_to_customer_teams";
		//url += "&field=readable_team_name";
		//url += "&field=_dao3_link1";
		//url += "&field=id";
		//url += "&field=teams";
		url += "&field=guideids";
		url += "&field=locationids";
		url += "&field=type";
		//url += "&field=lat";
		//url += "&field=lon";
		//url += "&field=welcome_message";
		//url += "&field=stitchtemplate";

		return url;
	}
	

	public static String formatUpdateGuideID(String username, String password, String id, String type, String guideid) {
		String url = "http://crm.beek.co/ewws/EWUpdate/.json?$KB=BeekOrderly";
		
		if (!type.contains("contact"))
			url += "&$table=contacts." + type;
		else
			url += "&$table=contacts";
		url += "&$login=" + username.trim();
		url += "&$lang=en";
		url += "&$password=" + password.trim();
		url += "&id=" + id;
		url += "&new_guide=" + guideid;

		return url;
	}

	public static String formatUpdateTemplate(String username, String password, String id, String template) {
		String url = "http://crm.beek.co/ewws/EWUpdate/.json?$KB=BeekOrderly";

		url += "&$table=contacts";
		url += "&$login=" + username.trim();
		url += "&$lang=en";
		url += "&$password=" + password.trim();
		url += "&id=" + id;
		url += "&stitchtemplate=" + template;

		return url;
	}
	
	public static String formatUpdateLocationID(String username, String password, String id, String type, String locationid) {
		String url = "http://crm.beek.co/ewws/EWUpdate/.json?$KB=BeekOrderly";

		if (!type.contains("contact"))
			url += "&$table=contacts." + type;
		else
			url += "&$table=contacts";
		url += "&$login=" + username.trim();
		url += "&$lang=en";
		url += "&$password=" + password.trim();
		url += "&id=" + id;
		url += "&new_location=" + locationid;

		return url;
	}



	public static String formatCreateGuide(String username, String password,
			GuideDetail guide) throws UnsupportedEncodingException {
		
		String url = "http://crm.beek.co/ewws/EWCreate/.json?$KB=BeekOrderly";

		url += "&$table=guides";
		url += "&$login=" + username.trim();
		url += "&$lang=en";
		url += "&$password=" + password.trim();
		url += "&guideid=" + guide.getId();
		url += "&guide_name=" + URLEncoder.encode(guide.getTitle(), "UTF-8");

		return url;
				
	}
	
	public static String formatCreateLocation(String username, String password,
			Location location) throws UnsupportedEncodingException {
		
		String url = "http://crm.beek.co/ewws/EWCreate/.json?$KB=BeekOrderly";

		url += "&$table=location";
		url += "&$login=" + username.trim();
		url += "&$lang=en";
		url += "&$password=" + password.trim();
		url += "&locationid=" + location.getId();
		url += "&location_name=" + URLEncoder.encode(location.getTitle(), "UTF-8");

		return url;
				
	}

	public static String formatUpdateLocation(String username, String password,
											  Location location) throws UnsupportedEncodingException {

		String url = "http://crm.beek.co/ewws/EWUpdate/.json?$KB=BeekOrderly";

		url += "&$table=location";
		url += "&$login=" + username.trim();
		url += "&$lang=en";
		url += "&$password=" + password.trim();
		url += "&id=" + location.getEwId();
		url += "&status=" + location.getStatus();
		url += "&scene_count=" + location.getSceneCount();
		url += "&location_name=" + URLEncoder.encode(location.getTitle(), "UTF-8");

		return url;

	}

	public static String formatUpdateUserStitchTemplate(String username, String password, String id, String template) {
		String url = "http://crm.beek.co/ewws/EWUpdate/.json?$KB=BeekOrderly";

		url += "&$table=contacts";
		url += "&$login=" + username.trim();
		url += "&$lang=en";
		url += "&$password=" + password.trim();
		url += "&id=" + id;
		url += "&stitchTemplate=" + template;

		return url;
	}
	
	public static String formatGetLocationUsers(String username, String password, String locationID) {
		String url = "http://crm.beek.co/ewws/EWSearch/.json?$KB=BeekOrderly";
		
		url += "&$table=contacts";
		url += "&$login=" + username.trim();
		url += "&$lang=en";
		url += "&$password=" + password.trim();
		url += "&field=_login";
		url += "&field=first_name";
		url += "&field=last_name";
		url += "&field=email";
		url += "&query=~LOC_" + locationID;
		
		return url;
	}
	
	public static String formatGetGuideUsers(String username, String password, String guideID) {
		String url = "http://crm.beek.co/ewws/EWSearch/.json?$KB=BeekOrderly";
		
		url += "&$table=contacts";
		url += "&$login=" + username.trim();
		url += "&$lang=en";
		url += "&$password=" + password.trim();
		url += "&field=_login";
		url += "&field=first_name";
		url += "&field=last_name";
		url += "&field=email";
		url += "&query=~GUI_" + guideID;
		
		return url;
	}
	
	public static String formatSearchUserByEmail(String username, String password, String emailAddress) {
		String url = "http://crm.beek.co/ewws/EWSearch/.json?$KB=BeekOrderly";
		
		url += "&$table=contacts";
		url += "&$login=" + username.trim();
		url += "&$lang=en";
		url += "&$password=" + password.trim();
		url += "&field=first_name";
		url += "&field=last_name";
		url += "&field=id";
		url += "&field=type";
		url += "&query=email='" + emailAddress + "'";
		
		
		
		return url;
	}
	
	public static String formatAddUserByEmail(String username, String password, String emailAddress, String locationID) {
		String url = "http://crm.beek.co/ewws/EWCreate/.json?$KB=BeekOrderly";
		
		url += "&$table=contacts";
		url += "&$login=" + username.trim();
		url += "&$lang=en";
		url += "&$password=" + password.trim();
		url += "&email=" + emailAddress;
		url += "&new_location=" + locationID;
		url += "&source=GMS_Invite";
		
		//http://crm.bee$lang=en&$login=gms&$password=B33kb33k&email=ben@beek.co&new_location=103

		
		return url;
	}
	public static String formatAddGuideUserByEmail(String username, String password, String emailAddress, String guideID) {
		String url = "http://crm.beek.co/ewws/EWCreate/.json?$KB=BeekOrderly";
		
		url += "&$table=contacts";
		url += "&$login=" + username.trim();
		url += "&$lang=en";
		url += "&$password=" + password.trim();
		url += "&email=" + emailAddress;
		url += "&new_guide=" + guideID;
		url += "&source=GMS_Invite";
		
		//http://crm.bee$lang=en&$login=gms&$password=B33kb33k&email=ben@beek.co&new_location=103

		
		return url;
	}
	
	

	
	public static String formatRemoveUserByEmail(String username, String password, String id, String type, String locationID) {
		String url = "http://crm.beek.co/ewws/EWUpdate/.json?$KB=BeekOrderly";
		
		if (!type.contains("contact"))
			url += "&$table=contacts." + type;
		else
			url += "&$table=contacts";
		url += "&$login=" + username.trim();
		url += "&$lang=en";
		url += "&$password=" + password.trim();
		url += "&id=" + id;
		url += "&remove_location=" + locationID;
		
		//http://crm.bee$lang=en&$login=gms&$password=B33kb33k&email=ben@beek.co&new_location=103
		//http://crm.beek.co/ewws/EWUpdate/.json?$KB=BeekOrderly&$table=contacts&$login=gms&$lang=en&$password=B33kb33k&id=1593&new_guide=104

		
		return url;
	}
	
	public static String formatRemoveGuideUserByEmail(String username, String password, String id, String type, String guideID) {
		String url = "http://crm.beek.co/ewws/EWUpdate/.json?$KB=BeekOrderly";
		
		if (!type.contains("contact"))
			url += "&$table=contacts." + type;
		else
			url += "&$table=contacts";
		url += "&$login=" + username.trim();
		url += "&$lang=en";
		url += "&$password=" + password.trim();
		url += "&id=" + id;
		url += "&remove_guide=" + guideID;
		
		//http://crm.bee$lang=en&$login=gms&$password=B33kb33k&email=ben@beek.co&new_location=103
		//http://crm.beek.co/ewws/EWUpdate/.json?$KB=BeekOrderly&$table=contacts&$login=gms&$lang=en&$password=B33kb33k&id=1593&new_guide=104

		
		return url;
	}
	
	public static EWContactsResponse getSuperUserResponse() {
		EWContact contact = EWContact.getSuperUserContact();

		List<EWContact> contacts = new ArrayList<EWContact>();
		contacts.add(contact);

		EWContactsResponse response = new EWContactsResponse();
		response.setSuccess(true);
		response.setResult(contacts);

		return response;
	}

	private List<EWContact> contacts = new ArrayList<EWContact>();

	public void setResult(List<EWContact> value) {
		this.contacts = value;
	}

	public List<EWContact> getContacts() {
		return contacts;
	}

	public boolean loginSuccessful() {
		return getSuccess() && contacts != null && contacts.size() > 0;
	}

	public EWContact firstContact() {
		if (contacts.size() > 0)
			return contacts.get(0);
		return null;
	}




}
