package co.beek.pano.model.dao.enterprizewizard;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/*
 * {
 *	   "success":true,
 *	   "message":"",
 *	   "result":{
 *	      "create_transaction_token":"|1959|43|1334312014218|",
 *	      "date_updated":"May 09 2012 15:30:37",
 *	      "shoot_preffered_date":"2012-4-18,2012-4-26",
 *	      "record_imported":"Yes",
 *	      "deleteable":"Yes",
 *	      "_1880_full_name":"Ed Letifov",
 *	      "client_token":"CLIENT_5_",
 *	      "DAOphoto_shoot_to_customer":{
 *	         "customer_token":"CUSTOMER_21_",
 *	         "id":21
 *	      },
 *	      "DAOphoto_shoot_to_contacts":{
 *	         "_1880_full_name":"Ed Letifov",
 *	         "id":366
 *	      },
 *	      "lead_id":"21",
 *	      "wfstate":"Assigned",
 *	      "history":null,
 *	      "customer_token":"CUSTOMER_21_",
 *	      "customer_name":"Ben's restaurant",
 *	      "destination":[
 *	         "Wellington"
 *	      ],
 *	      "scenes":"{\"1\":{\"location\":\"Ben's restaurant\",\"scene_name\":\"Exterior\",\"shooting_requirements\":\"Show the great front door and tables. Help people understand where restaurant is located.\",\"id\":\"8\"},\"2\":{\"scene_name\":\"Bar area\",\"shooting_requirements\":\"Show the great selection, happy staff and seating places. Help people get why it's a great bar to visit.\",\"id\":\"9\"},\"3\":{\"scene_name\":\"Dining area\",\"shooting_requirements\":\"oh, yeah baby\",\"id\":\"null\"},\"4\":{\"scene_name\":\"Kitchen\",\"shooting_requirements\":\"All clear!\",\"id\":\"null\"},\"5\":{\"scene_name\":\"Office\",\"shooting_requirements\":\"shiny computers: Macs and Acers\",\"id\":\"null\"}}",
 *	      "DAOphoto_shoot_to_orderbeek":{
 *	         "scenes":"{\"1\":{\"location\":\"Ben's restaurant\",\"scene_name\":\"Exterior\",\"shooting_requirements\":\"Show the great front door and tables. Help people understand where restaurant is located.\",\"id\":\"8\"},\"2\":{\"scene_name\":\"Bar area\",\"shooting_requirements\":\"Show the great selection, happy staff and seating places. Help people get why it's a great bar to visit.\",\"id\":\"9\"},\"3\":{\"scene_name\":\"Dining area\",\"shooting_requirements\":\"oh, yeah baby\",\"id\":\"null\"},\"4\":{\"scene_name\":\"Kitchen\",\"shooting_requirements\":\"All clear!\",\"id\":\"null\"},\"5\":{\"scene_name\":\"Office\",\"shooting_requirements\":\"shiny computers: Macs and Acers\",\"id\":\"null\"}}",
 *	         "shoot_instructions":null,
 *	         "customer_name":"Ben's restaurant",
 *	         "scene_location":"Ben's restaurant",
 *	         "order_lead_id":"21",
 *	         "shoot_telephone":null,
 *	         "shoot_preffered_date":"2012-4-18,2012-4-26",
 *	         "shoot_email":null,
 *	         "shoot_full_name":null,
 *	         "order_id":"43",
 *	         "id":43
 *	      },
 *	      "_1888_full_name":"Test Photographer",
 *	      "order_id":"43",
 *	      "type":"photo_shoot",
 *	      "DAOphoto_shoot_to_client":{
 *	         "client_token":"CLIENT_5_",
 *	         "id":5
 *	      },
 *	      "owned_by":"techtime",
 *	      "DAOphoto_shoot_to_contacts2":{
 *	         "creator_token":null,
 *	         "creator_customer_token":"CUSTOMER_1_",
 *	         "id":366
 *	      },
 *	      "DAOphoto_shoot_to_contacts1":{
 *	         "owned_by":"techtime",
 *	         "id":366
 *	      },
 *	      "DAOphoto_shoot_to_contacts0":{
 *	         "_1888_full_name":"Test Photographer",
 *	         "id":387
 *	      },
 *	      "url":null,
 *	      "DAOphoto_shoot_to_lead":{
 *	         "destination":[
 *	            "Wellington"
 *	         ],
 *	         "lead_id":"21",
 *	         "id":21
 *	      },
 *	      "creator_customer_token":"CUSTOMER_1_",
 *	      "order_lead_id":"21",
 *	      "date_created":"Apr 20 2012 16:30:00",
 *	      "scene_location":"Ben's restaurant",
 *	      "id":"7"
 *	   }
 *	}
 */

public class EWCorrespondenceResponse extends EWResponse implements
		Serializable {
	private static final long serialVersionUID = 1L;

	/*
	 * http://tsubaki.techtime.org/ewws/EWCreate?$KB=BeekOrderly
	 * &$table=correspondence&$lang=en
	 * &$login=beek_photographer&$password=welcome
	 * 
	 * &send_to_customer_token=CUSTOMER_68_ &template=Test1
	 * &url=http%3A%2F%2Fbeek.co &body=bla
	 * &DAOcorrespondence_to_template.template=:Test1
	 * &DAOcorrespondence_to_sendtocustomer.send_to_customer_token=:CUSTOMER_68_
	 */
	public static String sendCorrespondenceUrl(String username,
			String password, String teamId, String guideId, String template, String body,
			String link, String toAddress) throws UnsupportedEncodingException {

		String url = "http://crm.beek.co/ewws/EWCreate/.json?$KB=BeekOrderly"
				+ "&$table=correspondence&$lang=en"
				// The login of the photographer
				+ "&$login=" + username + "&$password=" + password;
		// more nasty EW stuff
		url += "&DAOcorrespondence_to_template.template=:" + template;

		if (teamId != null) {
			url += "&DAOcorrespondence_to_sendtocustomer.send_to_customer_token=:"
					+ teamId;
			url += "&send_to_customer_token=" + teamId;
		}
		url += "&template=" + template;
		if (link != null)
			url += "&url=" + URLEncoder.encode(link, "UTF-8");

		if (toAddress != null)
			url += "&to_email=" + URLEncoder.encode(toAddress, "UTF-8");
		
		if (guideId != null)
			url += "&guide_id=" + guideId;
		
		url += "&body=" + URLEncoder.encode(body, "UTF-8");


		return url;
	}

	public void setResult(int value) {
	}
}
