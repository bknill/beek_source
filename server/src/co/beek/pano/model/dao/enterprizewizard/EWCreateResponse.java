package co.beek.pano.model.dao.enterprizewizard;

import com.sun.org.apache.xpath.internal.operations.Bool;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * {
   "success":true,
   "message":"",
	 "result":312
}
 * @author Daniel
 *
 */
public class EWCreateResponse implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Boolean success;
	private String message;
	private int result;


	public Boolean getSuccess() {
		return success;
	}
	
	public void setSuccess(Boolean value) {
		this.success = value;
	}

	public String getMessage() {
	     return message;
    }

    public void setMessage(String value) {
        this.message = value;
    }

	public String getResult() {
		return String.valueOf(result);
	}

	public void setResult(int result) {
		this.result = result;
	}
}
