package co.beek.pano.model.dao.enterprizewizard;

/*
 *{
 *	"DAOcustomer_to_contacts3" : { 
 *		"direct_phone" : null,
 *        "id" : null,
 *        "main_contact" : null,
 *        "main_contact_email" : null,
 *        "mobile_phone" : null
 *      },
 *    "client_token" : "CLIENT_5_",
 *    "customer_name" : "Beek Test Customer",
 *    "customer_token" : "CUSTOMER_4_",
 *    "history" : null,
 *    "id" : "4",
 *    "type" : "customer"
 *}
 */
public class EWCustomer {
	public String id;
	public String customer_name;
	public String customer_token;
	public String guideIDs;
	public String locationIDs;
	
	public String getName()
	{
		return customer_name;
	}

	public String getToken()
	{
		return customer_token;
	}
	
	public EWTeam getTeam()
	{
		return new EWTeam(customer_token);
	}
	public EWTeam getGuideIDs()
	{
		return new EWTeam(customer_token);
	}
	
}
