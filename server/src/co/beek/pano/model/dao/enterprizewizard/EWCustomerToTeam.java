package co.beek.pano.model.dao.enterprizewizard;

/*
 *"DAOcontacts_to_customer_teams" : [ { "id" : 69,
 "readable_team_name" : "Alexey",
 "team_token_name" : null
 } ]
 */
public class EWCustomerToTeam {
	public Integer id;
	public String readable_team_name;
	public String team_token_name;

	public EWCustomerToTeam() {
		// empty constructor
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return readable_team_name;
	}

	public String getTeamId() {
		return team_token_name;
	}
}
