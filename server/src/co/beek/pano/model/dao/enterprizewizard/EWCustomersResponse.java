package co.beek.pano.model.dao.enterprizewizard;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/*
 *{ "message" : "",
 *  "result" : [ 
 *      { "DAOcustomer_to_contacts3" : 
 *      	{ 
 *      	  "direct_phone" : null,
 *            "id" : null,
 *            "main_contact" : null,
 *            "main_contact_email" : null,
 *            "mobile_phone" : null
 *          },
 *        "client_token" : "CLIENT_5_",
 *        "customer_name" : "Beek Test Customer",
 *        "customer_token" : "CUSTOMER_4_",
 *        "history" : null,
 *        "id" : "4",
 *        "type" : "customer"
 *      }
 *  ],
 *  "success" : true
 *}
 *
 * @author Daniel
 */
public class EWCustomersResponse extends EWResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	public static String getUrl(String username, String password) {
		String url = "http://crm.beek.co/ewws/EWSearch/.json?$KB=BeekOrderly";

		url += "&$table=customer&$lang=en";

		url += "&$login=" + username;
		url += "&$password=" + password;

		url += "&field=customer_name";
		url += "&field=customer_token";
		url += "&field=destination";
		return url;
	}

	private List<EWCustomer> customers = new ArrayList<EWCustomer>();

	public void setResult(List<EWCustomer> value) {
		this.customers = value;
	}

	public List<EWCustomer> getCustomers() {
		return customers;
	}
}
