package co.beek.pano.model.dao.enterprizewizard;

import java.io.Serializable;

/**
{
	"message": "EWWrongDataException has occurred: [ajp-127.0.0.1-8009-6][1334612527033] Invalid login/password combination beek_salesx/****** for knowledgebase BeekOrderly"
}
 * @author Daniel
 *
 */
public class EWError implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String message;

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
