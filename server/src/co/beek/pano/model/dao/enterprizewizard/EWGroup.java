package co.beek.pano.model.dao.enterprizewizard;

import java.io.Serializable;

/**
{
       "f_group":"Sales",
        "id":300
 }
 * @author Daniel
 *
 */
public class EWGroup implements Serializable {
	private static final long serialVersionUID = 3489452426500158330L;
	
	public static String GROUP_SALES = "Sales";
	public static String GROUP_CUSTOMER = "Customer";
	public static String GROUP_PHOTOGRAPHER = "Photographer";
	public static String GROUP_ADMIN = "Client Admin";

	private int id;

	private String f_group;
	
	public EWGroup()
	{
		// empty constructor
	}
	
	public EWGroup(int id, String name)
	{
		this.id = id;
		this.f_group = name;
	}

	public int getId() {
		return id;
	}
	
	public void setId(int value) {
		this.id = value;
	}

    public void setF_group(String value) {
        this.f_group = value;
    }
    
    public String getF_group() {
		return f_group;
	}
    
    @Override
    public String toString() {
        return "EWGroup[ id=" + id + " ]";
    }
}
