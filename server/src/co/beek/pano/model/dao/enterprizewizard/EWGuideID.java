package co.beek.pano.model.dao.enterprizewizard;

import java.io.Serializable;

public class EWGuideID implements Serializable {
	private static final long serialVersionUID = -3269670632643458754L;
	//change serialversionUID? I made leading 4 into a 3

	public static String TEAM_SUPER_USER = "super_user_team";
	
	//private int id;

	private String guideID;
	
	public EWGuideID()
	{
		// empty constructor
	}
	
	public EWGuideID(String guideID)
	{
		//this.id = id;
		this.guideID = guideID;
	}

	//public int getId() {
	//	return id;
	//}
	
	//public void setId(int value) {
	//	this.id = value;
	//}
	
    public void setGuideID(String value) {
        this.guideID = value;
    }
    
    public String getGuideID() {
		return guideID;
	}
    
    @Override
    public String toString() {
        return "EWGuideID[ guideid=" + guideID + " ]";
    }
}
