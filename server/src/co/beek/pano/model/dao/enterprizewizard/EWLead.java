package co.beek.pano.model.dao.enterprizewizard;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import flexjson.JSON;

/*
 * {
 *       "last_name":"Client",
 *       "DAO_dao3_link":[
 *          {
 *             "f_group":"Customer",
 *             "id":305
 *          }
 *       ],
 *       "DAOcontacts_to_destination":[
 *          {
 *              "destination":"Wellington",
 *             "id":3
 *          },
 *          {
 *             "destination":"Testland",
 *             "id":10
 *          }
 *       ],
 *       "client_token":"CLIENT_5_",
 *       "DAO_dao3_link1":[
 *          {
 *             "teams":"CUSTOMER_4_",
 *             "id":395
 *          }
 *       ],
 *       "contact_history":null,
 *       "f_group":[
 *          "Customer"
 *       ],
 *       "first_name":"Test",
 *       "destination":[
 *          "Wellington",
 *          "Testland"
 *       ],
 *       "type":"Customer",
 *       "teams":[
 *          "CUSTOMER_4_"
 *       ],
 *       "_login":"beek_client",
 *       "id":"385"
 *    }
 */
public class EWLead implements Serializable {

	private static final long serialVersionUID = -8968689243491431989L;
	
	public static String ADMIN_USER_USERNAME = "gms";
	public static String ADMIN_USER_PASSWORD = "B33kb33k";

	// TODO change these for obscure values
	public static String SUPER_USER_ID = "99";
	public static String SUPER_USER_USERNAME = "admin@beek.co";
	public static String SUPER_USER_PASSWORD = "KeyJn8mw";

	public static EWContact getSuperUserContact() {
		// Create a list of teams
		List<EWTeam> teams = new ArrayList<EWTeam>();
		teams.add(new EWTeam(EWTeam.TEAM_SUPER_USER));

		// Create a list of groups
		List<EWGroup> groups = new ArrayList<EWGroup>();
		groups.add(new EWGroup(98, EWGroup.GROUP_ADMIN));

		// Create our super user
		EWContact superUser = new EWContact();
		superUser.id = EWContact.SUPER_USER_ID;
		superUser.first_name = "Ben";
		superUser.last_name = "Knill";
		superUser.setGroups(groups);
		superUser.setEWTeams(teams);

		return superUser;
	}

	public String id;
	
	public String bn;
	
	public Integer pc;

	public String business_name;

	public String next_action;

	public String next_action_date;
	
	public String percent_complete;
	
	public String business_email;
	
	public String business_telephone;
	
	public String business_website;
	
	public String comments;
	public String new_comments;
	
	public String contact_name;
	public String contact_surname;
	public String contact_email;
	public String contact_telephone;


	public String related1452822542167;

	public List<EWLeadEmail> emails;

	public boolean changed;
	

	public List<String> campaign_name;

	public String new_campaign_name;
	
	public boolean getChanged() {
		return changed;
	}
	
	public void setChanged(boolean changed) {
		this.changed = changed;
	}
	
	public String getbn() {
		return bn;
	}
	public void setbn(String bn) {
		this.bn = bn;
	}
	
	public Integer getpc() {
		return pc;
	}
	public void setpc(Integer pc) {
		this.pc = pc;
	}
	
	public String getId() 
	{return id;}
	
	public void setId(String id) 
	{this.id=id;}
	
	public String getBusinessName()
	{return business_name;}
	
	public void setBusinessName(String business_name) 
	{this.business_name=business_name;}
	
	public String getBusinessEmail()
	{return business_email;}
	
	public void setBusinessEmail(String business_email) 
	{this.business_email=business_email;}
	
	public String getBusinessWebsite()
	{return business_website;}
	
	public void setBusinessWebsite(String business_website) 
	{this.business_website=business_website;}
	
	public String getcontact_email()
	{return contact_email;}
	
	
	public String getComments()
	{return comments;}
	
	public void setComments(String comments) 
	{this.comments=comments;}
	
	public String getNewComments()
	{return new_comments;}

	public String getNewCommentsShort()
	{return new_comments != null ? new_comments.length() > 100 ? new_comments.substring(0, 100) + "..." : new_comments : null;}
	
	public void setNewComments(String new_comments) 
	{this.new_comments=new_comments;}
	
	public String getcontact_telephone()
	{return contact_telephone;}
	
	public String getcontact_name()
	{return contact_name;}
	
	public String getcontact_surname()
	{return contact_surname;}
	
	public String getnext_action()
	{return next_action;}
	
	public void setnext_action(String next_action) 
	{this.next_action=next_action;}
	
	public String getnext_action_date()
	{return next_action_date;}
	
	public void setnext_action_date(String next_action_date) 
	{this.next_action_date=next_action_date;}
	
	public String getBusinessTelephone()
	{return business_telephone;}
	
	public void setBusinessTelephone(String business_telephone) 
	{this.business_telephone=business_telephone;}
	
	public String getContactName()
	{return contact_name;}
	
	public void setContactName(String contact_name) 
	{this.contact_name=contact_name;}
	
	public String getContactSurname()
	{return contact_surname;}
	
	public void setContactSurname(String contact_surname) 
	{this.contact_surname=contact_surname;}
	
	public List<String> getCampaigns()
	{return campaign_name;}
	
	public List<String> getcampaign_name()
	{return campaign_name;}
	
	public void setCampaigns(List<String> Campaigns) 
	{this.campaign_name=Campaigns;}
	
	
	public void setNewCampaignName(String NewCampaignName) 
	{this.new_campaign_name=NewCampaignName;}
	
	
	public String getContactEmail()
	{return contact_email;}
	
	public void setContactEmail(String contact_email) 
	{this.contact_email=contact_email;}
	
	public String getContactTelephone()
	{return contact_telephone;}
	
	public void setContactTelephone(String contact_telephone) 
	{this.contact_telephone=contact_telephone;}
	
	public String getNextAction()
	{return next_action;}
	
	public String getNextActionDate()
	{return next_action_date;}
	
	public void setNexActionDate(String next_action_date)
	{this.next_action_date=next_action_date;}
	
	public String getPercentComplete() 
	{return percent_complete;}
	
	public void setPercentComplete(String percent_complete) 
	{this.percent_complete=percent_complete;}
	
	public boolean nextActionIsEmail(){
		return next_action.equals("Email");
	}
	
	public boolean nextActionIsCall(){
		return next_action.equals("Call");
	}

	public List<EWLeadEmail> getEmails() {
		return emails;
	}

	public void setEmails(List<EWLeadEmail> emails) {
		this.emails = emails;
	}


	/**
	 * Returns true of the EWContact has not had the correct settings applied
	 * email.
	 */
	@JSON(include = false)
	public boolean isSuperUser() {
		return id.equals(SUPER_USER_ID);
	}

}


