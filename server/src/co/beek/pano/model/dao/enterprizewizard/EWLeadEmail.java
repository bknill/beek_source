package co.beek.pano.model.dao.enterprizewizard;

import flexjson.JSON;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/*
 * {
 *       "last_name":"Client",
 *       "DAO_dao3_link":[
 *          {
 *             "f_group":"Customer",
 *             "id":305
 *          }
 *       ],
 *       "DAOcontacts_to_destination":[
 *          {
 *              "destination":"Wellington",
 *             "id":3
 *          },
 *          {
 *             "destination":"Testland",
 *             "id":10
 *          }
 *       ],
 *       "client_token":"CLIENT_5_",
 *       "DAO_dao3_link1":[
 *          {
 *             "teams":"CUSTOMER_4_",
 *             "id":395
 *          }
 *       ],
 *       "contact_history":null,
 *       "f_group":[
 *          "Customer"
 *       ],
 *       "first_name":"Test",
 *       "destination":[
 *          "Wellington",
 *          "Testland"
 *       ],
 *       "type":"Customer",
 *       "teams":[
 *          "CUSTOMER_4_"
 *       ],
 *       "_login":"beek_client",
 *       "id":"385"
 *    }
 */
public class EWLeadEmail implements Serializable {

	private static final long serialVersionUID = -8968689243491431989L;

	public static EWContact getSuperUserContact() {
		// Create a list of teams
		List<EWTeam> teams = new ArrayList<EWTeam>();
		teams.add(new EWTeam(EWTeam.TEAM_SUPER_USER));

		// Create a list of groups
		List<EWGroup> groups = new ArrayList<EWGroup>();
		groups.add(new EWGroup(98, EWGroup.GROUP_ADMIN));

		// Create our super user
		EWContact superUser = new EWContact();
		superUser.id = EWContact.SUPER_USER_ID;
		superUser.first_name = "Ben";
		superUser.last_name = "Knill";
		superUser.setGroups(groups);
		superUser.setEWTeams(teams);

		return superUser;
	}

	public String id;

	public String email_from;
	public String content;
	public String subject;
	public String date_updated;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmail_from() {
		return email_from != null ? email_from : "Telephone call";
	}

	public void setEmail_from(String email_from) {
		this.email_from = email_from;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDate_updated() {
		return date_updated;
	}

	public void setDate_updated(String date_updated) {
		this.date_updated = date_updated;
	}
}


