package co.beek.pano.model.dao.enterprizewizard;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * { "succes":true, "message":"", "result":[ { "first_name":"Sales",
 * "client_token":"CLIENT_5_", "type":"employees", "contact_history":null,
 * "_login":"beek_sales", "DAO_dao3_link1":[ { "teams":"Sales Team", "id":390 },
 * { "teams":"CUSTOMER_4_", "id":395 } ], "teams":[ "Sales Team", "CUSTOMER_4_"
 * ], "last_name":"Guy", "DAO_dao3_link":[ { "f_group":"Sales", "id":300 } ],
 * "f_group":[ "Sales" ], "id":"380" } ] }
 * 
 * @author Daniel
 * 
 */
public class EWLeadEmailResponse extends EWResponse implements Serializable {

	private static final long serialVersionUID = 1502811422513474126L;


	public static String formatGetLeadCommunications(String username, String password, String id) {
		String url = "http://crm.beek.co/ewws/EWSearch/.json?$KB=BeekOrderly";

		url += "&$table=unify_communications";
		url += "&$login=" + username.trim();
		url += "&$lang=en";
		url += "&$password=" + password.trim();
		url += "&field=content";
		url += "&field=subject";
		url += "&field=email_from";
		url += "&field=date_updated";
		url += "&query=lead_id=" + id;



		return url;
	}
	
	public static EWContactsResponse getSuperUserResponse() {
		EWContact contact = EWContact.getSuperUserContact();

		List<EWContact> contacts = new ArrayList<EWContact>();
		contacts.add(contact);

		EWContactsResponse response = new EWContactsResponse();
		response.setSuccess(true);
		response.setResult(contacts);

		return response;
	}


	public void setResult(List<EWLeadEmail> value) {
		this.leadEmails = value;
	}

	private List<EWLeadEmail> leadEmails = new ArrayList<EWLeadEmail>();
	public List<EWLeadEmail> getLeadsEmails() {
		return leadEmails;
	}
	
	private String id;
	
	public String getId() {
		
		return id;
	}

}
