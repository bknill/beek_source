package co.beek.pano.model.dao.enterprizewizard;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import co.beek.pano.model.dao.entities.GuideDetail;

/**
 * { "succes":true, "message":"", "result":[ { "first_name":"Sales",
 * "client_token":"CLIENT_5_", "type":"employees", "contact_history":null,
 * "_login":"beek_sales", "DAO_dao3_link1":[ { "teams":"Sales Team", "id":390 },
 * { "teams":"CUSTOMER_4_", "id":395 } ], "teams":[ "Sales Team", "CUSTOMER_4_"
 * ], "last_name":"Guy", "DAO_dao3_link":[ { "f_group":"Sales", "id":300 } ],
 * "f_group":[ "Sales" ], "id":"380" } ] }
 * 
 * @author Daniel
 * 
 */
public class EWLeadResponse extends EWResponse implements Serializable {

	private static final long serialVersionUID = 1502811422513474126L;

	public static String formatGetUserLeads(String username, String password) {
		String url = "http://crm.beek.co/ewws/EWSearch/.json?$KB=BeekOrderly";
		
		url += "&$table=lead";
		url += "&$login=" + username.trim();
		url += "&$lang=en";
		url += "&$password=" + password.trim();
	    url += "&search=gmsleads";
		url += "&field=business_name";	
		url += "&field=contact_email";	
		url += "&field=contact_telephone";	
		url += "&field=contact_name";	
		url += "&field=contact_surname";	
		url += "&field=new_comments";	
		url += "&field=id";
		url += "&field=next_action";
		url += "&field=next_action_date";
		url += "&field=percent_complete";
		url += "&field=campaign_name";
		return url;
	}
	
	public static String formatUpdateLeadPercentComplete(String username, String password, String userid, String id, String percent_complete) {
		String url = "http://crm.beek.co/ewws/EWUpdate/.json?$KB=BeekOrderly";
		
		url += "&$table=lead";
		url += "&$login=" + username.trim();
		url += "&$lang=en";
		url += "&$password=" + password.trim();
		url += "&id=" + id;
		url += "&percent_complete="+percent_complete;
		
		return url;
	}
	
	public static String formatUpdateLeadCampaign(String username, String password, String id, String new_campaign_name) throws UnsupportedEncodingException {
		String url = "http://crm.beek.co/ewws/EWUpdate/.json?$KB=BeekOrderly";
		
		url += "&$table=lead";
		url += "&$login=" + username.trim();
		url += "&$lang=en";
		url += "&$password=" + password.trim();
		url += "&id=" + id;
		url += "&add_campaign="+ URLEncoder.encode(new_campaign_name, "UTF-8");
		
		return url;
	}
	
	public static String formatUpdateLeadNextAction(String username, String password, String id, String next_action,  String next_action_date) throws UnsupportedEncodingException {
		String url = "http://crm.beek.co/ewws/EWUpdate/.json?$KB=BeekOrderly";
		
		url += "&$table=lead";
		url += "&$login=" + username.trim();
		url += "&$lang=en";
		url += "&$password=" + password.trim();
		url += "&id=" + id;
		url += "&next_action="+ URLEncoder.encode(next_action, "UTF-8");
		url += "&next_action_date="+ URLEncoder.encode(next_action_date, "UTF-8");
		
		return url;
	}
	
	public static String formatUpdateLeadIntroEmail(String username, String password,String id, String intro_email, String emailSubject, String campaign_name) throws UnsupportedEncodingException {
		String url = "http://crm.beek.co/ewws/EWUpdate/.json?$KB=BeekOrderly";
		
		url += "&$table=lead";
		url += "&$login=" + username.trim();
		url += "&$lang=en";
		url += "&$password=" + password.trim();
		url += "&id=" + id;
		url += "&intro_email="+URLEncoder.encode(intro_email, "UTF-8");
    	url += "&email_subject="+URLEncoder.encode(emailSubject, "UTF-8");
    	url += "&email_campaign_name="+URLEncoder.encode(campaign_name, "UTF-8");
		
		return url;
	}

	public static String formatUpdateLeadCallNotes(String username, String password,String id, String call_notes) throws UnsupportedEncodingException {
		String url = "http://crm.beek.co/ewws/EWUpdate/.json?$KB=BeekOrderly";

		url += "&$table=lead";
		url += "&$login=" + username.trim();
		url += "&$lang=en";
		url += "&$password=" + password.trim();
		url += "&id=" + id;
		url += "&call_notes="+URLEncoder.encode(call_notes, "UTF-8");

		return url;
	}
	
	
	public static String formatUpdateLead(String username, String password, String id, String contactName, String contactSurname, 
			String email, String telephone, String busName, String busWebsite, String comments, String percentComplete, String nextAction, String nextActionDate) throws UnsupportedEncodingException {
			String url = "http://crm.beek.co/ewws/EWUpdate/.json?$KB=BeekOrderly";
		
		url += "&$table=lead";
		url += "&$login=" + username.trim();
		url += "&$lang=en";
		url += "&$password=" + password.trim();
		url += "&id=" + id;
		if (!(contactName == null))
		url += "&contact_name="+URLEncoder.encode(contactName, "UTF-8");
		if (!(contactSurname == null))
		url += "&contact_surname="+URLEncoder.encode(contactSurname, "UTF-8");
		if (!(email == null))
		url += "&contact_email="+URLEncoder.encode(email, "UTF-8");
		if (!(telephone == null))
		url += "&contact_telephone="+URLEncoder.encode(telephone, "UTF-8");
		if (!(busName == null))
		url += "&business_name="+URLEncoder.encode(busName, "UTF-8");
		if (!(busWebsite == null))
		url += "&business_website="+URLEncoder.encode(busWebsite, "UTF-8");
		if (!(comments == null))
		url += "&new_comments="+ URLEncoder.encode(comments, "UTF-8");
		if (!(percentComplete == null))
			url += "&percent_complete="+ URLEncoder.encode(percentComplete, "UTF-8");
		if (!(nextAction == null))
			url += "&next_action="+ URLEncoder.encode(nextAction, "UTF-8");
		if (!(nextActionDate == null))
			url += "&next_action_date="+ URLEncoder.encode(nextActionDate, "UTF-8");
		
			return url;
	}
	
	public static String formatCreateLead(String username, String password) throws UnsupportedEncodingException {
			String url = "http://crm.beek.co/ewws/EWCreate/.json?$KB=BeekOrderly";
		
			url += "&$table=lead";
			url += "&$login=" + username.trim();
			url += "&$lang=en";
			url += "&$password=" + password.trim();
			
				return url;
	}
	

	public static String formatReadLead(String username, String password, String id) {
		String url = "http://crm.beek.co/ewws/EWRead/.json?$KB=BeekOrderly";
		
		url += "&$table=lead";
		url += "&$login=" + username.trim();
		url += "&$lang=en";
		url += "&$password=" + password.trim();
		url += "&id=" + id;
		
		return url;
	}
	
	
	
	public static String formatGetLead(String username, String password, String id) {
		String url = "http://crm.beek.co/ewws/EWSearch/.json?$KB=BeekOrderly";
		
		url += "&$table=lead";
		url += "&$login=" + username.trim();
		url += "&$lang=en";
		url += "&$password=" + password.trim();
		url += "&field=business_name";
		url += "&field=business_email";
		url += "&field=business_telephone";
		url += "&field=business_website";
		url += "&field=contact_name";
		url += "&field=contact_surname";
		url += "&field=contact_email";
		url += "&field=contact_telephone";
		url += "&field=contact_id";
		url += "&field=comments";
		url += "&field=new_comments";
		url += "&field=id";
		url += "&field=next_action";
		url += "&field=next_action_date";
		url += "&field=percent_complete";
		url += "&field=campaign_name";
		url += "&query=id=" + id;
		
		
		
		return url;
	}


	
	public static EWContactsResponse getSuperUserResponse() {
		EWContact contact = EWContact.getSuperUserContact();

		List<EWContact> contacts = new ArrayList<EWContact>();
		contacts.add(contact);

		EWContactsResponse response = new EWContactsResponse();
		response.setSuccess(true);
		response.setResult(contacts);

		return response;
	}

	private List<EWLead> leads = new ArrayList<EWLead>();

	public void setResult(List<EWLead> value) {
		this.leads = value;
	}

	public List<EWLead> getLeads() {
		return leads;
	}

	private List<EWLeadEmail> leadEmails = new ArrayList<EWLeadEmail>();
	public List<EWLeadEmail> getLeadsEmails() {
		return leadEmails;
	}
	
	private String id;
	
	public String getId() {
		
		return id;
	}
	

	public boolean loginSuccessful() {
		return getSuccess() && leads != null && leads.size() > 0;
	}

	public EWLead firstContact() {
		if (leads.size() > 0)
			return leads.get(0);
		return null;
	}




}
