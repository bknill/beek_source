package co.beek.pano.model.dao.enterprizewizard;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import flexjson.JSONDeserializer;

/*

 * 
 *  id, customer_name, photographer_name (this is hist first name), 
 *  assigned_photographer (full name), 
 *  photographer_id, photographer_login, 
 *  order_id, 
 *  scene location, 
 *  scenes (JSON encoded text for all scenes), 
 *  lead_id, 
 *  customer_token 
 *  (this is effectively the team name, unique customer identifier within CRM), 
 *  destination, 
 *  shoot_email, 
 *  shoot_full_name, 
 *  shoot_instructions, 
 *  shoot_preferred_date, 
 *  shoot_telephone, 
 *  shoot_time, 
 *  notes (this is a specially encoded append-only text field where "internal" people - photographer or sales will be able to add their notes), 
 *  comments (this is the append-only field for externally visible i.e. customer comments)
 *  
 *   New, 
 *   Assigned, 
 *   Declined, 
 *   Accepted,
 *   Cancelled, 
 *   Processed, 
 *   Changes Requested, 
 *   Approved, 
 *   Completed
 */

class PhotoShootComparator implements Comparator<EWPhotoShoot> {
	public int compare(EWPhotoShoot ps1, EWPhotoShoot ps2) {
		return (ps1.getOrder() - ps2.getOrder());
	}
}

public class EWPhotoShoot {

	public static final PhotoShootComparator stateComparator = new PhotoShootComparator();
	
	public static enum PhotoShootStatus {
		NEW("New"),
		//
		ASSIGNED("Assigned"),
		//
		ACCEPTED("Accepted"),
		//
		DECLINED("Declined"),
		//
		PROCESSED("Processed"),
		//
		CHANGES_REQUESTED("Changes Requested"),
		//
		APPROVED("Approved"),
		//
		CANCELLED("Cancelled");

		private String code;

		private PhotoShootStatus(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}

	}

	private String id;
	private String customer_name;

	private String wfstate;
	private String destination;
	private String scene_location;

	/**
	 * The team Id
	 */
	private String customer_token;
	/**
	 * (full name)
	 */
	private String assigned_photographer;
	private String photographer_id;
	private String photographer_login;
	private String photographer_name;

	private String order_id;

	private String shoot_instructions;
	private String shoot_full_name;
	private String shoot_telephone;
	private String shoot_email;
	private String shoot_time;
	private String notes;
	private String comments;

	private String ordered_scenes_count;
	// 2012-5-1
	DateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
	private List<Date> dates = new ArrayList<Date>();

	private HashMap<String, HashMap<String, String>> ewScenes;

	/**
	 * (JSON encoded text for all scenes)
	 */
	public void setScenes(String scenes) {
		ewScenes = new JSONDeserializer<HashMap<String, HashMap<String, String>>>()
				.deserialize(scenes.replaceAll("/", ""));
	}

	public void setWfstate(String wfstate) {
		this.wfstate = wfstate;
	}

	public String getWfstate() {
		return wfstate;
	}

	public String getStatus() {
		return wfstate;
	}

	public void setScene_location(String scene_location) {
		this.scene_location = scene_location;
	}

	public String getLocationTitle() {
		return scene_location;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

	public String getOrder_id() {
		return order_id;
	}

	public int getOrder() {
		if (wfstate.equals("New"))
			return 0;
		if (wfstate.equals("Assigned"))
			return 1;
		if (wfstate.equals("Declined"))
			return 2;
		if (wfstate.equals("Accepted"))
			return 3;
		if (wfstate.equals("Processed"))
			return 4;
		if (wfstate.equals("Changes Requested"))
			return 5;
		if (wfstate.equals("Approved"))
			return 6;
		// wfstate.equals("Cancelled")
		return 7;
	}

	public void setShoot_instructions(String shoot_instructions) {
		this.shoot_instructions = shoot_instructions;
	}

	public String getShoot_instructions() {
		return shoot_instructions;
	}

	public void setShoot_full_name(String shoot_full_name) {
		this.shoot_full_name = shoot_full_name;
	}

	public String getShoot_full_name() {
		return shoot_full_name;
	}

	public void setShoot_telephone(String shoot_telephone) {
		this.shoot_telephone = shoot_telephone;
	}

	public String getShoot_telephone() {
		return shoot_telephone;
	}

	public void setShoot_email(String shoot_email) {
		this.shoot_email = shoot_email;
	}

	public String getShoot_email() {
		return shoot_email;
	}

	public void setShoot_time(String shoot_time) {
		this.shoot_time = shoot_time;
	}

	public String getShoot_time() {
		return shoot_time;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getNotes() {
		return notes;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getComments() {
		return comments;
	}

	public HashMap<String, HashMap<String, String>> getEwScenes() {
		return ewScenes;
	}

	public List<HashMap<String, String>> getScenesMap() {
		if (ewScenes == null)
			return null;
		return new ArrayList<HashMap<String, String>>(ewScenes.values());
	}

	public int getNumScenes() {
		if (ewScenes == null)
			return 0;
		return ewScenes.size();
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setPhotographer_id(String photographer_id) {
		this.photographer_id = photographer_id;
	}

	public String getPhotographer_id() {
		return photographer_id;
	}

	public void setPhotographer_login(String photographer_login) {
		this.photographer_login = photographer_login;
	}

	public String getPhotographer_login() {
		return photographer_login;
	}

	public void setAssigned_photographer(String assigned_photographer) {
		this.assigned_photographer = assigned_photographer;
	}

	public String getAssigned_photographer() {
		return assigned_photographer;
	}

	public void setPhotographer_name(String photographer_name) {
		this.photographer_name = photographer_name;
	}

	public String getPhotographer_name() {
		return photographer_name;
	}

	public void setShoot_preffered_date(String value) {
		try {
			String[] vals = value.split(",");
			for (String s : vals)
				dates.add(formatter.parse(s));
		} catch (ParseException e) {
			// what to do?
			System.out.println("error formatting dates:" + value);
		}
	}

	public Date getFirstDate() {
		if (dates.size() > 0)
			return dates.get(0);

		return null;
	}

	public List<Date> getDates() {
		return dates;
	}

	public void setCustomer_token(String customer_token) {
		this.customer_token = customer_token;
	}

	public String getTeamId() {
		return customer_token;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getDestination() {
		return destination;
	}

	public void setOrdered_scenes_count(String ordered_scenes_count) {
		this.ordered_scenes_count = ordered_scenes_count;
	}

	public String getOrdered_scenes_count() {
		return ordered_scenes_count;
	}

	public int getSceneOrderCount() {
		if (ordered_scenes_count == null)
			return getNumScenes();
		return Integer.parseInt(ordered_scenes_count);
	}
}
