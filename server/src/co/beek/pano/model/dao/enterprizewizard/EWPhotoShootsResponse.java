package co.beek.pano.model.dao.enterprizewizard;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/*
 * {
 *	   "success":true,
 *	   "message":"",
 *	   "result":{
 *	      "create_transaction_token":"|1959|43|1334312014218|",
 *	      "date_updated":"May 09 2012 15:30:37",
 *	      "shoot_preffered_date":"2012-4-18,2012-4-26",
 *	      "record_imported":"Yes",
 *	      "deleteable":"Yes",
 *	      "_1880_full_name":"Ed Letifov",
 *	      "client_token":"CLIENT_5_",
 *	      "DAOphoto_shoot_to_customer":{
 *	         "customer_token":"CUSTOMER_21_",
 *	         "id":21
 *	      },
 *	      "DAOphoto_shoot_to_contacts":{
 *	         "_1880_full_name":"Ed Letifov",
 *	         "id":366
 *	      },
 *	      "lead_id":"21",
 *	      "wfstate":"Assigned",
 *	      "history":null,
 *	      "customer_token":"CUSTOMER_21_",
 *	      "customer_name":"Ben's restaurant",
 *	      "destination":[
 *	         "Wellington"
 *	      ],
 *	      "scenes":"{\"1\":{\"location\":\"Ben's restaurant\",\"scene_name\":\"Exterior\",\"shooting_requirements\":\"Show the great front door and tables. Help people understand where restaurant is located.\",\"id\":\"8\"},\"2\":{\"scene_name\":\"Bar area\",\"shooting_requirements\":\"Show the great selection, happy staff and seating places. Help people get why it's a great bar to visit.\",\"id\":\"9\"},\"3\":{\"scene_name\":\"Dining area\",\"shooting_requirements\":\"oh, yeah baby\",\"id\":\"null\"},\"4\":{\"scene_name\":\"Kitchen\",\"shooting_requirements\":\"All clear!\",\"id\":\"null\"},\"5\":{\"scene_name\":\"Office\",\"shooting_requirements\":\"shiny computers: Macs and Acers\",\"id\":\"null\"}}",
 *	      "DAOphoto_shoot_to_orderbeek":{
 *	         "scenes":"{\"1\":{\"location\":\"Ben's restaurant\",\"scene_name\":\"Exterior\",\"shooting_requirements\":\"Show the great front door and tables. Help people understand where restaurant is located.\",\"id\":\"8\"},\"2\":{\"scene_name\":\"Bar area\",\"shooting_requirements\":\"Show the great selection, happy staff and seating places. Help people get why it's a great bar to visit.\",\"id\":\"9\"},\"3\":{\"scene_name\":\"Dining area\",\"shooting_requirements\":\"oh, yeah baby\",\"id\":\"null\"},\"4\":{\"scene_name\":\"Kitchen\",\"shooting_requirements\":\"All clear!\",\"id\":\"null\"},\"5\":{\"scene_name\":\"Office\",\"shooting_requirements\":\"shiny computers: Macs and Acers\",\"id\":\"null\"}}",
 *	         "shoot_instructions":null,
 *	         "customer_name":"Ben's restaurant",
 *	         "scene_location":"Ben's restaurant",
 *	         "order_lead_id":"21",
 *	         "shoot_telephone":null,
 *	         "shoot_preffered_date":"2012-4-18,2012-4-26",
 *	         "shoot_email":null,
 *	         "shoot_full_name":null,
 *	         "order_id":"43",
 *	         "id":43
 *	      },
 *	      "_1888_full_name":"Test Photographer",
 *	      "order_id":"43",
 *	      "type":"photo_shoot",
 *	      "DAOphoto_shoot_to_client":{
 *	         "client_token":"CLIENT_5_",
 *	         "id":5
 *	      },
 *	      "owned_by":"techtime",
 *	      "DAOphoto_shoot_to_contacts2":{
 *	         "creator_token":null,
 *	         "creator_customer_token":"CUSTOMER_1_",
 *	         "id":366
 *	      },
 *	      "DAOphoto_shoot_to_contacts1":{
 *	         "owned_by":"techtime",
 *	         "id":366
 *	      },
 *	      "DAOphoto_shoot_to_contacts0":{
 *	         "_1888_full_name":"Test Photographer",
 *	         "id":387
 *	      },
 *	      "url":null,
 *	      "DAOphoto_shoot_to_lead":{
 *	         "destination":[
 *	            "Wellington"
 *	         ],
 *	         "lead_id":"21",
 *	         "id":21
 *	      },
 *	      "creator_customer_token":"CUSTOMER_1_",
 *	      "order_lead_id":"21",
 *	      "date_created":"Apr 20 2012 16:30:00",
 *	      "scene_location":"Ben's restaurant",
 *	      "id":"7"
 *	   }
 *	}
 */
public class EWPhotoShootsResponse extends EWResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	public static String getPhotoShootsUrl(String username, String password) {
		String url = "http://crm.beek.co/ewws/EWSearch/.json"
				+ "?$KB=BeekOrderly&$table=photo_shoot&$lang=en"
				// The login of the photographer
				+ "&$login=" + username + "&$password=" + password;

		// / The state of the job
		// + "&query=wfstate%3DAssigned";

		// fields
		url += "&field=customer_name";
		url += "&field=customer_token";

		// this is hist first name
		url += "&field=order_id";
		url += "&field=lead_id";
		url += "&field=photographer_name";
		url += "&field=assigned_photographer";
		url += "&field=photographer_id";
		url += "&field=photographer_login";
		url += "&field=scene_location";
		url += "&field=scenes";
		url += "&field=destination";
		url += "&field=wfstate";

		url += "&field=shoot_email";
		url += "&field=shoot_full_name";
		url += "&field=shoot_instructions";
		url += "&field=shoot_preffered_date";
		url += "&field=shoot_telephone";
		url += "&field=shoot_time";

		url += "&field=notes";
		url += "&field=comments";

		return url;
	}

	public static String updatePhotoShootUrl(String email, String password,
			String orderId, String state, String customerUrl) {

		String url = "http://beek.techtime.org/ewws/EWUpdate/.json?$KB=BeekOrderly"
				+ "&$table=photo_shoot&$lang=en"
				// The login of the photographer
				+ "&$login=" + email + "&$password=" + password;

		url += "&id=" + orderId;
		url += "&wfstate=" + state;
		url += "&url=" + customerUrl;

		return url;
	}

	private List<EWPhotoShoot> photoShoots = new ArrayList<EWPhotoShoot>();

	public void setResult(List<EWPhotoShoot> value) {
		this.photoShoots = value;
	}

	public List<EWPhotoShoot> getPhotoShoots() {
		return photoShoots;
	}

	public EWPhotoShoot getPhotoShoot(String shootId) {
		for (EWPhotoShoot shoot : photoShoots)
			if (shoot.getId().equals(shootId))
				return shoot;

		return null;
	}

	public void removePhotoShoot(String shootId) {
		for (int i = 0; i < photoShoots.size(); i++)
			if (photoShoots.get(i).getId().equals(shootId))
				photoShoots.remove(i);
				
	}

	public void removePhotoShoot(EWPhotoShoot shoot) {
		for (int i = 0; i < photoShoots.size(); i++)
			if (photoShoots.get(i).getId().equals(shoot.getId()))
				photoShoots.remove(i);
	}

	public void updatePhotoShoot(EWPhotoShoot shoot) {
		for (int i = 0; i < photoShoots.size(); i++)
			if (photoShoots.get(i).getId().equals(shoot.getId()))
				photoShoots.set(i, shoot);
	}
}
