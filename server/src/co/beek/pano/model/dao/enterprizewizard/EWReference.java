package co.beek.pano.model.dao.enterprizewizard;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.faces.model.SelectItem;

import org.apache.commons.lang.StringUtils;

import flexjson.JSON;


public class EWReference implements Serializable {

	private static final long serialVersionUID = -8968689243491431989L;
	

	public String shortname;
	public String additional_information;
	public String id;
	public String text;

	
	public String getshortname() {
		return shortname;
	}
	
	public void setshortname(String shortname) {
	{this.shortname=shortname;}
	}
	
	public String getText() {
		
		return additional_information;
	}
	
	public void setText(String text) {
	{this.additional_information=text;}
	}
	
	public String getId() {
		return id;
	}
	

	
	public void setId(String referenceId) 
	{this.id=referenceId;}

}




