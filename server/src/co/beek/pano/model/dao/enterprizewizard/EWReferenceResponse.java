package co.beek.pano.model.dao.enterprizewizard;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import co.beek.pano.model.dao.entities.GuideDetail;

public class EWReferenceResponse extends EWResponse implements Serializable {

	private static final long serialVersionUID = 1502811422513474126L;

	public static String getReferences(String username, String password) {
		String url = "http://crm.beek.co/ewws/EWSearch/.json?$KB=BeekOrderly";
		
		url += "&$table=reference";
		url += "&$login=" + username.trim();
		url += "&$lang=en";
		url += "&$password=" + password.trim();
		url += "&field=id";
		url += "&field=shortname";
		url += "&field=additional_information";
		url += "&search=shortName";
		
		return url;
	}
	
	public static String newReference(String username, String password, String newReferenceName, String newReferenceBody) throws UnsupportedEncodingException {
		String url = "http://crm.beek.co/ewws/EWCreate/.json?$KB=BeekOrderly";
		
		url += "&$table=reference";
		url += "&$login=" + username.trim();
		url += "&$lang=en";
		url += "&$password=" + password.trim();
		url += "&shortname=" + URLEncoder.encode(newReferenceName, "UTF-8");
		url += "&additional_information=" + URLEncoder.encode(newReferenceBody, "UTF-8");
		url += "&context=Correspondence";
		url += "&sub_context=Email%20-%20Lead%20template";
		
		return url;
	}

	
	
	private List<EWReference> References = new ArrayList<EWReference>();

	public void setResult(List<EWReference> value) {
		this.References = value;
	}

	public List<EWReference> getReferences() {
		return References;
	}

	public boolean loginSuccessful() {
		return getSuccess() && References != null && References.size() > 0;
	}





}
