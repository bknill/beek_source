package co.beek.pano.model.dao.enterprizewizard;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * {
   "success":true,
   "message":"",
}
 * @author Daniel
 *
 */
public class EWResponse implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private boolean success;

	private String message;

	private List<EWError> errors = new ArrayList<EWError>();
	
	public boolean getSuccess() {
		return success;
	}
	
	public void setSuccess(boolean value) {
		this.success = value;
	}

/*	public String getMessage() {
	     return message;
    }

    public void setMessage(String value) {
        this.message = value;
    }*/
    
	public void setErrors(List<EWError> errors) {
		this.errors = errors;
	}

	public List<EWError> getErrors() {
		return errors;
	}
	
	public String getErrorMessage()
	{
		if(errors.size() > 0)
			return errors.get(0).getMessage();
		
		return null;
	}
}
