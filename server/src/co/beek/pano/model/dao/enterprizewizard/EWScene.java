package co.beek.pano.model.dao.enterprizewizard;

/**
 * { "location":"New Zealand", "scene_name":"Test1",
 * "shooting_requirements":"ABC", "id":"" }
 * 
 * @author Daniel
 * 
 */
public class EWScene {
	private String id;
	private String location;
	private String scene_name;
	private String shooting_requirements;

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getLocation() {
		return location;
	}

	public void setScene_name(String scene_name) {
		this.scene_name = scene_name;
	}

	public String getScene_name() {
		return scene_name;
	}

	public void setShooting_requirements(String shooting_requirements) {
		this.shooting_requirements = shooting_requirements;
	}

	public String getShooting_requirements() {
		return shooting_requirements;
	}
}
