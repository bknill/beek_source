package co.beek.pano.model.dao.enterprizewizard;

import java.io.Serializable;

/**
 * {
       "teams":"Sales Team",
       "id":390
    }
 * @author Daniel
 *
 */
public class EWTeam implements Serializable {
	private static final long serialVersionUID = -4269670632643458754L;

	public static String TEAM_SUPER_USER = "super_user_team";
	
	//private int id;

	private String teams;
	
	public EWTeam()
	{
		// empty constructor
	}
	
	public EWTeam(String teams)
	{
		//this.id = id;
		this.teams = teams;
	}

	//public int getId() {
	//	return id;
	//}
	
	//public void setId(int value) {
	//	this.id = value;
	//}
	
    public void setTeams(String value) {
        this.teams = value;
    }
    
    public String getTeams() {
		return teams;
	}
    
    @Override
    public String toString() {
        return "EWTeam[ team=" + teams + " ]";
    }
}
