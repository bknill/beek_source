package co.beek.pano.model.dao.enterprizewizard;

import java.io.Serializable;

public class LoginResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	private EWContact user;
	private String error;
	
	public LoginResponse(EWContact user, String error)
	{
		this.user = user;
		this.error = error;
	}

	public EWContact getUser() {
		return user;
	}

	public String getError() {
		return error;
	}
}
