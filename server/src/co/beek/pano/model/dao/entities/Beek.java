package co.beek.pano.model.dao.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "beek")
public class Beek implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@NotNull
	@Column(name = "id")
	private int id = 0;

	@NotNull
	@Column(name = "player_increment")
	private int playerIncrement;

	@NotNull
	@Column(name = "admin_increment")
	private int adminIncrement;

	@Column(name = "ami")
	private String ami;

	public int getId() {
		return id;
	}

	public int getPlayerIncrement() {
		return playerIncrement;
	}
	
	public void setPlayerIncrement(int value) {
		playerIncrement = value;
	}

	public void incrementPlayer() {
		this.playerIncrement++;
	}

	public String getPlayerFileName() {
		return "player_" + playerIncrement + ".swf";
	}

	public int getAdminIncrement() {
		return adminIncrement;
	}

	public void setAdminIncrement(int value) {
		adminIncrement = value;
	}

	public void incrementAdmin() {
		this.adminIncrement++;
	}

	public String getAdminFileName() {
		return "admin_" + adminIncrement + ".swf";
	}

	public String getAmi() {
		return ami;
	}

	public void setAmi(String ami) {
		this.ami = ami;
	}
}
