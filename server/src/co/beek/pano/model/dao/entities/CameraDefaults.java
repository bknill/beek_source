package co.beek.pano.model.dao.entities;

import javax.persistence.Entity;
import java.io.Serializable;

@Entity
public class CameraDefaults implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;

    private Double theta;
    private Double phi;
    private Integer fov;

    public Double getTheta() {
        return theta;
    }

    public void setTheta(Double theta) {
        this.theta = theta;
    }

    public Double getPhi() {
        return phi;
    }

    public void setPhi(Double phi) {
        this.phi = phi;
    }

    public Integer getFov() {
        return fov;
    }

    public void setFov(Integer fov) {
        this.fov = fov;
    }
}
