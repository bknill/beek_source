package co.beek.pano.model.dao.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "files")
public class FileData implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @NotNull
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id = 0l;

    @Column(name = "team_id")
    private String teamId;

    @Column(name = "folder_id")
    private String folderId;

    @NotNull
    @Size(max = 100)
    @Column(name = "filename")
    private String fileName;

    @NotNull
    @Size(max = 40)
    @Column(name = "realname")
    private String realName;


    public FileData(Long id, String fileName) {
        this.id = id;
        this.fileName = fileName;
    }
    

    public FileData(String teamId, String fileName) {
    	this.id = 0L;
    	this.teamId =   teamId;
        this.fileName = fileName;
        
        String extension = fileName.substring(fileName.lastIndexOf("."));
        this.realName = UUID.randomUUID().toString() + extension;
    }

    public FileData() {
        this.id = 0L;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }
    
    public void setFolderId(String folderId) {
		this.folderId = folderId;
	}

	public String getFolderId() {
		return folderId;
	}
	
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }



    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof FileData)) {
            return false;
        }
        FileData other = (FileData) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "File[ id=" + id + " ]";
    }

	

}
