package co.beek.pano.model.dao.entities;

import com.sun.org.apache.xpath.internal.operations.Bool;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
public class GameChoice implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;

    public String title;
    private String id;
    public  Boolean correct;
    public  Integer order;
    
    public GameChoice cloneNow(String guideId) throws CloneNotSupportedException
	{
		GameChoice clone = (GameChoice) super.clone();
		//clone.guideId = guideId;
		return clone;
	}
    
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getCorrect() {
        return correct;
    }

    public void setCorrect(Boolean correct) {
        this.correct = correct;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }
}
