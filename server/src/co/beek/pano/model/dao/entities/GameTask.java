package co.beek.pano.model.dao.entities;

import co.beek.pano.model.beans.*;
import co.beek.pano.model.beans.GameTaskOutput;
import flexjson.JSON;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;

class GameTaskComparator implements Comparator<GameTask> {
    public int compare(GameTask object1, GameTask object2) {
        return object1.getOrder() - object2.getOrder();
    }
}

@Entity
@Table(name = "gametasks")
public class GameTask implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;

    public static final GameTaskComparator orderComparator = new GameTaskComparator();

    @Id
    @NotNull
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id = 0L;


    @NotNull
    @Column(name = "guide_id")
    public String guideId;

    @Column(name = "[order]")
    public int order;
    
    @NotNull
    @Size(max = 40)
    @Column(name = "code")
    public String code;
    
    @Column(name = "title")
    public String title;

    @Column(name = "input")
    public String input;

    @Size(max = 1000)
    @Column(name = "instructions")
    public String instructions;

    @Size(max = 255)
    @Column(name = "feedback")
    public String feedback;
    
    @Column(name = "output")
    public String output;

    @Column(name = "scene")
    public String scene;

    @Column(name = "load_scene")
    public String load_scene;

    @Column(name = "parentId")
    public String parentId;

    @Column(name = "score")
    public String score;

    @Column(name = "node")
    public String node;
    
    public GameTask cloneNow(String guideId) throws CloneNotSupportedException
	{
		GameTask clone = (GameTask) super.clone();
		clone.guideId = guideId;
		return clone;
	}

    public String getId(){
        return id.toString();
    }

    public String getInstructions(){
        return instructions;
    }

    public void setInstructions(String instructions){
        this.instructions = instructions;
    }

    public String getTitle(){
        return title;
    }

    public void setTitle(String  title){
        this.title =  title;
    }

    public String getFeedback(){
        return feedback;
    }

    public void setFeedback(String  feedback){
        this.feedback = feedback;
    }

    public String getInput(){
        return input;
    }

    public void setInput(String input){
        this.input = input;
    }

    public String getOutput(){
        return output;
    }

    public void setOutput(String output){
        this.output = output;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getGuideId() {
        return guideId;
    }

    public void setGuideId(String guideId) {
        this.guideId = guideId;
    }

    public String getCode() {
        if(code == null)
        return "findhotspots";
            else
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getScene() {
        return scene;
    }

    public void setScene(String scene) {
            this.scene = scene;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getNode() {
        return node;
    }

    public void setNode(String node) {
        this.node = node;
    }

    @JSON(include = false)
    public GameTaskInput getGameTaskInput(){
        if(input == null)
            return new GameTaskInput();

        try {
            return new JSONDeserializer<GameTaskInput>().use(null, GameTaskInput.class).deserialize(input);
        }
        catch (Exception e){
            GameTaskInput g = new GameTaskInput();
            try {
                ArrayList<GameTaskHotspot> hotspotList = new JSONDeserializer<ArrayList<GameTaskHotspot>>().use("values",GameTaskHotspot.class).deserialize(input);
                g.hotspots.addAll(hotspotList);
                return g;

            }
            catch (Exception e2){
                g.setVideoTime(input);
            }

            return g;
        }
    }

    public void setGameTaskInput(GameTaskInput gameTaskInput){
        try {
            input = new JSONSerializer().deepSerialize(gameTaskInput);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @JSON(include = false)
    public GameTaskOutput getGameTaskOutput(){
        if(output == null)
            return new GameTaskOutput();

        try {
            return new JSONDeserializer<GameTaskOutput>().use(null, GameTaskOutput.class).deserialize(output);
        }
        catch (Exception e){
            return new GameTaskOutput();
        }
    }

    public void setGameTaskOutput(GameTaskOutput gameTaskOutput){
        try {
            output = new JSONSerializer().deepSerialize(gameTaskOutput);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}
