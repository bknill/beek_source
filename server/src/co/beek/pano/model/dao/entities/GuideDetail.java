package co.beek.pano.model.dao.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.enterprizewizard.EWGroup;
import flexjson.JSON;

@Entity
@Table(name = "guides")
public class GuideDetail implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;
	
	public GuideDetail shallowClone() throws CloneNotSupportedException
	{
		GuideDetail clone = (GuideDetail) super.clone();
		clone.gameTasks = new ArrayList<GameTask>();
		clone.guideSections = new ArrayList<GuideSection>();
		clone.saveIncrement = 0;
		clone.status = Guide.STATUS_FREE;
		return clone;
	}
	
	public GuideDetail copyCollections(GuideDetail clone) throws CloneNotSupportedException
	{
		for(GameTask task : gameTasks)
			clone.gameTasks.add(task.cloneNow(clone.id));
		
		for(GuideSection section : guideSections)
			clone.guideSections.add(section.cloneNow(clone.id));
		
		return clone;
	}
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String id;

	@NotNull
	@Column(name = "team_id")
	private String teamId;

	@NotNull
	@Column(name = "destination_id")
	private String destinationId;
	
	@Column(name = "status")
	public int status;
	
	@Column(name = "title")
	@Size(max = 100, message = "Invalid title(0-100 characters}")
	private String title;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "phone")
	private String phone;
	
	@Column(name = "infourl")
	private String infoURL;
	
	@Column(name = "bookingurl")
	private String bookingURL;

	@Column(name = "colorscene")
	@Size(max = 6, message = "Invalid color scene(0-6 characters}")
	private String colorScene;

	@Column(name = "mapstyle")
	@Size(max = 20, message = "Invalid map style(0-20 characters}")
	private String mapStyle;

	@Column(name = "font")
	public int font;
	
	@Column(name = "saveincrement")
	public int saveIncrement;

	@Column(name = "coverdesign")
	public String coverDesign;
	
	@Column(name = "thumbincrement")
	private int thumbIncrement;
	
    @Column(name = "otherguides", columnDefinition = "TINYINT")
    private boolean otherguides;

    @Column(name = "gamelocked", columnDefinition = "TINYINT")
    public boolean gameLocked;

    @Column(name = "gamecode", columnDefinition = "TINYINT")
    public String gameCode;
    
	@Column(name = "subdomain")
	public String subdomain;
	
	@Column(name = "parent")
	public String parent;

	@Column(name = "soundtrack")
	public String soundtrack;

	@Column(name = "output_video")
	public String outputVideo;

	@Immutable
	@ManyToOne()
	@Fetch(value = FetchMode.JOIN)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "destination_id", insertable = false, updatable = false)
	private Destination destination;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "guide_id", updatable = false)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<GuideSection> guideSections = new ArrayList<GuideSection>();

	
//	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
//	@JoinColumn(name = "guide_id", updatable = false)
//	@LazyCollection(LazyCollectionOption.FALSE)
//	private List<String> guideChildren = new ArrayList<String>();

	//@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	//@JoinColumn(name = "guide_id", updatable = false)
	//@LazyCollection(LazyCollectionOption.FALSE)
	//private List<GuideExclude> guideExcludes = new ArrayList<GuideExclude>();

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "guide_id", updatable = false)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<GameTask> gameTasks = new ArrayList<GameTask>();

	public GuideDetail() {
		// empty constructor
	}

	public GuideDetail(Destination destination, Location location) {
		setDestination(destination);
		populateFromLocation(location);
	}
	
	public void setDestination(Destination destination) {
		this.destination = destination;
		destinationId = destination.getId();
	}

	public void populateFromLocation(Location location) {
		//locationId = location.getId();
		title = location.getTitle() + " Guide";
		teamId = location.getTeamId();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getStatus() {
		return status;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}
	
	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}

	public String getTeamId() {
		return teamId;
	}

	public void addScenes(List<Scene> scenes) {
		for (Scene scene : scenes)
			addGuideScene(scene);
	}

	public void setFirstScene(Scene scene) {
		guideSections.get(0).setFirstScene(scene);
	}

	public void addGuideSection(GuideSection section) {
		guideSections.add(section);
	}

	public void addGuideScene(Scene scene) {
		if(getGuideScene(scene.getId()) == null)
		{
			if(guideSections.size() == 0)
				addGuideSection(GuideSection.getDefault(id));

			guideSections.get(0).addScene(scene);
		}
	}

	public void addGuideSceneToSection(GuideScene guideScene) {

		for(GuideSection guideSection : guideSections)
			if(guideScene.getGuideSectionId().equals(guideSection.id))
				guideSection.addGuideScene(guideScene);
	}

	public GuideScene getGuideScene(String sceneId) {

		for (GuideScene guideScene  : returnGuideScenes())
			if (guideScene.getSceneId().equals(sceneId))
				return guideScene;

		return null;
	}

	public void removeGuideScene(Scene scene) {
		for (GuideSection gs : guideSections)
		{
			GuideScene guideScene = gs.getGuideScene(scene.getId());
			if (guideScene != null)
				gs.removeGuideScene(guideScene);
		}
	}

	public Boolean removeGameTask(GameTask task) {
		return gameTasks.remove(task);
	}

	public Boolean removeGuideSection(GuideSection guideSection) {
			return guideSections.remove(guideSection);
	}
	
	public List<GuideScene> returnGuideScenes() {
		List<GuideScene> guideScenes = new ArrayList<GuideScene>();

		Collections.sort(guideSections, GuideSection.orderComparator);

		for (GuideSection gs : guideSections){
			List<GuideScene> sectionGuideScenes = gs.getGuideScenes();
			Collections.sort(sectionGuideScenes, GuideScene.orderComparator);
			guideScenes.addAll(sectionGuideScenes);
		}

		return guideScenes;
	}

	public Boolean videoProcessingComplete(){
		for(GuideScene guideScene : returnGuideScenes())
			if(guideScene.getVideo() == null)
				return false;

		return true;
	}
//
//	public List<GuideExclude> getGuideExcludes() {
//		return guideExcludes;
//	}
//
//	public void setGuideExcludes(List<GuideExclude> guideExcludes) {
//		this.guideExcludes = guideExcludes;
//	}

	public String getMapStyle() {
		return mapStyle;
	}

	public void setMapStyle(String mapStyle) {
		this.mapStyle = mapStyle;
	}

	public String getColorScene() {
		return colorScene;
	}



	public void setColorScene(String colorScene) {
		this.colorScene = colorScene;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCoverDesign() {
		return coverDesign;
	}


	public GuideScene returnFirstScene() {

		if (guideSections.size() == 0)
			return null;

		Collections.sort(guideSections, GuideSection.orderComparator);

		// now return the first scene
		if(guideSections.get(0).getFirstScene() != null)
			return guideSections.get(0).getFirstScene();
		else if(returnGuideScenes().size() > 0)
			return returnGuideScenes().get(0);
		else
			return null;
	}
	
	public GuideScene returnScene(String sceneId) {
		for (GuideSection section : guideSections)
			for (GuideScene guideScene : section.getGuideScenes())
				if(guideScene.getScene().getId().equals(sceneId))
					return guideScene;
		
		return null;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof GuideDetail)) {
			return false;
		}
		GuideDetail other = (GuideDetail) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Guide[ id=" + id + " ]";
	}

	public void setGameTasks(List<GameTask> tasks) {
		this.gameTasks = tasks;
	}

	public List<GameTask> getGameTasks() {
		return gameTasks;
	}

	public boolean hasWritePermission(EWContact contact) {
		if (contact.isInGroup(EWGroup.GROUP_ADMIN))
			return true;
		
		if(contact.getGuideID().indexOf(id) > -1)
			return true;

		return false;
	}

	public void setDestinationId(String destinationId) {
		this.destinationId = destinationId;
	}

	public String getDestinationId() {
		return destinationId;
	}

	@JSON(include = false)
	public Destination getDestination() {
		return destination;
	}
	
	public void incrementSave() {
		this.saveIncrement++;
	}

	public void setThumbIncrement(int thumbIncrement) {
		if(thumbIncrement > this.thumbIncrement)
		this.thumbIncrement = thumbIncrement;
	}

	public int getThumbIncrement() {
		return thumbIncrement;
	}

	public void incrementThumb() {
		this.thumbIncrement++;

	}

	public String getThumbName() {
		if (thumbIncrement == 0)
			return "guide_default_thumb_1.png";
		
		return "guide_" + id + "_thumb_" + thumbIncrement + ".png";
	}

	public void updateGuideSections() {

		List<GuideSection> guideSectionsToAdd = new ArrayList<GuideSection>();

		//deal with child guides - either add the sections or if just 1 section add the scenes to this section
		for(GuideSection section : guideSections)
		 	if(section.childGuide != null) {
				if(section.childGuide.guideSections.size() == 1)
				{
					for(GuideScene guideScene : section.childGuide.guideSections.get(0).getGuideScenes()) {
						GuideScene newGuideScene = null;
						try {
							newGuideScene = guideScene.cloneNow(section.id);
							section.addGuideScene(newGuideScene);

						} catch (CloneNotSupportedException e) {
							e.printStackTrace();
						}

					}

				}
				else if (section.childGuide.guideSections.size() > 1)
					for (GuideSection childGuideSection : section.childGuide.guideSections) {
						if (childGuideSection.parent_section_id == null)
							childGuideSection.parent_section_id = section.id;

						guideSectionsToAdd.add(childGuideSection);
					}
			}


		if (guideSectionsToAdd.size() > 0)
			guideSections.addAll(guideSectionsToAdd);
	}

	public List<GuideSection> getGuideSections() {

		Collections.sort(guideSections,GuideSection.orderComparator);
		return guideSections;
	}

	public void setGuideSections(List<GuideSection> guideSections) {
		this.guideSections = guideSections;
	}
	
//	public List<String> getGuideChildren() {
//		return guideChildren;
//	}
//
//	public void setGuideChildren(List<String> guideChildren) {
//		this.guideChildren = guideChildren;
//	}
	
	public String getStatusString()
	{
		if (status == Guide.STATUS_FREE)
			return "free";

		if (status == Guide.STATUS_PROMOTED)
			return "promoted";

		if (status == Guide.STATUS_PUBLISHED)
			return "published";

		return "Other";
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isOtherguides() {
		return otherguides;
	}

	public void setOtherguides(boolean otherguides) {
		this.otherguides = otherguides;
	}
	
	public String getBookingURL() {
		return bookingURL;
	}

	public void setBookingURL(String bookingURL) {
		this.bookingURL = bookingURL;
	}

	public String getInfoURL() {
		return infoURL;
	}

	public void setInfoURL(String infoURL) {
		this.infoURL = infoURL;
	}
	
	public String getPhone() {
		return phone;
	}

	public void setPhone(String Phone) {
		this.phone = Phone;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String Email) {
		this.email = Email;
	}
	
	public String getSubdomain() {
		return subdomain;
	}

	public void setSubdomain(String Subdomain) {
		this.subdomain = Subdomain;
	}
	
	public String getParent() {
		return parent;
	}

	public void setParent(String Parent) {
		this.parent = Parent;
	}

	public int getFont() {
		return font;
	}

	public void setFont(int font) {
		this.font = font;
	}

	public String getSoundtrack() {
		return soundtrack;
	}

	public void setSoundtrack(String soundtrack) {
		this.soundtrack = soundtrack;
	}

	public String getOutputVideo() {
		return outputVideo;
	}

	public void setOutputVideo(String outputVideo) {
		this.outputVideo = outputVideo;
	}
}
