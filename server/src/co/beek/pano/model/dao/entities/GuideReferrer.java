package co.beek.pano.model.dao.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "guidereferrals")
public class GuideReferrer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@NotNull
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String id = "";

	@Column(name = "guide_id")
	private String guideId;

	@NotNull
	@Column(name = "path")
	private String path;

	@NotNull
	@Column(name = "count")
	private int count;

	public GuideReferrer() {
		// empty for hibernate
	}

	public GuideReferrer(String guideId, String path) {
		this.guideId = guideId;
		this.path = path;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getGuideId() {
		return guideId;
	}

	public void setGuideId(String guideId) {
		this.guideId = guideId;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof GuideReferrer)) {
			return false;
		}
		GuideReferrer other = (GuideReferrer) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "GuideReferrer[ id=" + id + " ]";
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getPath() {
		return path;
	}

	public void incrementCount() {
		this.count++;
	}

	public int getCount() {
		return count;
	}
}
