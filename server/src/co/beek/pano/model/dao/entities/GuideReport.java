package co.beek.pano.model.dao.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.eclipse.jetty.util.ajax.JSON;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Type;

import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.enterprizewizard.EWGroup;
import co.beek.pano.model.dao.googleanalytics.Country;
import co.beek.pano.model.dao.googleanalytics.Event;
import co.beek.pano.model.dao.googleanalytics.LandingPage;
import co.beek.pano.model.dao.googleanalytics.Referral;
import co.beek.pano.model.dao.googleanalytics.VisitByPath;
import co.beek.pano.model.dao.googleanalytics.WeekData;
import co.beek.pano.model.dao.googleanalytics.YearMonthData;
import co.beek.pano.service.dataService.googleAnalytics.BasicData;
import co.beek.web.guide.bean.CityVisits;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;

class ReferralsComparator implements Comparator<Referral> {
	public int compare(Referral r1, Referral r2) {
		return (r2.getSessions() - r1.getSessions());
	}
}

class CountriesComparator implements Comparator<Country> {
	public int compare(Country r1, Country r2) {
		return (r2.getSessions() - r1.getSessions());
	}
}

class visitsComparator implements Comparator<GuideReport> {
	public int compare(GuideReport r1, GuideReport r2) {
		return (r2.getGaVisits() - r1.getGaVisits());
	}
}

class lastMonthComparator implements Comparator<GuideReport> {
	public int compare(GuideReport r1, GuideReport r2) {
		return (r2.getLastMonthSessions() - r1.getLastMonthSessions());
	}
}

@Entity
@Table(name = "guides")
public class GuideReport implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final ReferralsComparator referralsComparator = new ReferralsComparator();

	public static final CountriesComparator countriesComparator = new CountriesComparator();
	public static final visitsComparator visitsComparator = new visitsComparator();
	public static final lastMonthComparator lastMonthComparator = new lastMonthComparator();

	
	@Immutable
	private static JSONSerializer serializer = new JSONSerializer().prettyPrint(true);

	@Immutable
	private static JSONDeserializer<List<List<String>>> deserializer = new JSONDeserializer<List<List<String>>>();

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String id;

	@NotNull
	@Column(name = "team_id")
	private String teamId;

	@NotNull
	@Column(name = "destination_id")
	private String destinationId;
	
	@Column(name = "location_id")
	public String locationId;

	@Column(name = "title")
	@Size(max = 100, message = "Invalid title(0-100 characters}")
	private String title;


	@Column(name = "visits")
	private int visits;

	@Column(name = "pageviews")
	private int pageViews;

	@Column(name = "time")
	private double avgTimeOnSite;
	
	@Column(name = "ga_basic")
	private String gaBasic;


	@Column(name = "score")
	private int score;
	
	@Column(name = "status")
	public int status;

	@Column(name = "ga_events")
	private String gaEvents;

	@Column(name = "ga_landingpage")	
	private String gaLandingPage;

	@Column(name = "ga_visits")
	private String gaVisits;

	@Column(name = "ga_visitsbyweek")
	private String gaVisitsByWeek;

	@Column(name = "ga_visitsbypath")
	private String gaVisitsByPath;

	@Column(name = "ga_visitsfeed")
	private String gaVisitsFeed;

	@Column(name = "ga_referrals")
	private String referralsData;

	@Column(name = "ga_countries")
	private String countriesData;

	@Column(name = "ga_convertersevent")
	private String convertersEventData;

	@Column(name = "ga_cachetime")
	@Type(type = "date")
	private Date gaCacheTime = new Date();

	@Column(name = "emailreporttime")
	@Type(type = "date")
	private Date emailReportTime = new Date();

	@NotNull
	@Column(name = "thumbincrement")
	private int thumbIncrement;
	
	//public YearMonthData lastMonthsData;

//	@Immutable
//	@ManyToOne()
//	@Fetch(value = FetchMode.JOIN)
//	@NotFound(action = NotFoundAction.IGNORE)
//	@JoinColumn(name = "destination_id", insertable = false, updatable = false)
//	private Destination destination;
//
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "guide_id", updatable = false)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<GuideSection> guideSections = new ArrayList<GuideSection>();

//
//	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
//	@JoinColumn(name = "guide_id", updatable = false)
//	@LazyCollection(LazyCollectionOption.FALSE)
//	private List<GuideExclude> guideExcludes = new ArrayList<GuideExclude>();
//
//
//	
	public GuideReport() {
		// TODO Auto-generated constructor stub
	}

//	public GuideReport(Destination destination, Location location) {
//		setDestination(destination);
//		setLocation(location);
//	}
//	
//	public void setDestination(Destination destination) {
//		this.destination = destination;
//		destinationId = destination.getId();
//	}

	public void setLocation(Location location) {
		locationId = location.getId();
		title = location.getTitle() + " Guide";
		teamId = location.getTeamId();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}

	public String getTeamId() {
		return teamId;
	}

//	public int getReportIncrement() {
//		return reportIncrement;
//	}
//
//	public void setReportIncrement(int reportIncrement) {
//		this.reportIncrement = reportIncrement;
//	}
//
//	public String getReportFileName() {
//		return "guide_" + id + "_report_" + reportIncrement + ".pdf";
//	}
//
//	public void incrementReport() {
//		this.reportIncrement++;
//	}


	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

//	public Scene returnFirstScene() {
//
//		if (guideSections.size() == 0)
//			return null;
//
//		Collections.sort(guideSections, GuideSection.orderComparator);
//
//		// now return the first scene
//		return guideSections.get(0).getFirstScene().getScene();
//	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof GuideReport)) {
			return false;
		}
		GuideReport other = (GuideReport) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Guide[ id=" + id + " ]";
	}

/*	public boolean hasWritePermission(EWContact contact) {
		if (contact.isInGroup(EWGroup.GROUP_ADMIN))
			return true;

		if (contact.isInTeam(teamId)) {
			if (contact.isInGroup(EWGroup.GROUP_CUSTOMER))
				return true;

			if (contact.isInGroup(EWGroup.GROUP_SALES))
				return true;
		}

		return false;
	}*/

	public void setDestinationId(String destinationId) {
		this.destinationId = destinationId;
	}

	public String getDestinationId() {
		return destinationId;
	}

//	@JSON(include = false)
//	public Destination getDestination() {
//		return destination;
//	}

	public void setThumbIncrement(int thumbIncrement) {
		this.thumbIncrement = thumbIncrement;
	}

	public int getThumbIncrement() {
		return thumbIncrement;
	}

	public void incrementThumb() {
		this.thumbIncrement++;
	}

	public String getThumbName() {
		if (thumbIncrement == 0){

			if(guideSections.size() > 0)
				if(guideSections.get(0).getFirstScene() != null)
					return guideSections.get(0).getFirstScene().getScene().getThumbName();
			else
				return "guide_default_thumb_1.png";
		}


		return "guide_" + id + "_thumb_" + thumbIncrement + ".png";
	}

	public void setBasicData(String data)
	{
		this.gaBasic = data;

	}

	public BasicData getBasicData() {
		if(gaBasic != null)
			return new BasicData(deserializer.deserialize(gaBasic));
		else
			return null;
	}

	public int getVisits() {
		return visits;
	}

	public  int getGaVisits() {
		if(gaVisits != null){
			return Integer.parseInt(deserializer.deserialize(gaVisits).get(0).toArray()[0].toString());
		}
		else
			return 0;
	}


	public void setScore(int score) {
		this.score = score;
	}

	public int getScore() {
		return score;
	}

	public void setEventsData(String eventsData) {
		this.gaEvents = eventsData;
	}

	public List<Event> getEventsList() {
		List<Event> events = new ArrayList<Event>();
		if (gaEvents == null)
			return events;


		List<List<String>> list = deserializer.deserialize(gaEvents);
		for (int i = 1; i < list.size(); i++)
			events.add(new Event(list.get(i)));

		return events;
	}

//	public int getTotalConversions() {
//		int sum = 0;
//		List<Event> events = getEventsList();
//		for (int i = 0; i < events.size(); i++) {
//			Event event = events.get(i);
//
//			if (event.getCategory().equals(Event.CATEGORY_CONVERSION)) {
//
//				String sceneId = event.getLabel().substring(
//						event.getLabel().lastIndexOf("|") + 1);
//
//				if (event.getAction().equals(Event.ACTION_LOCATION_PAGE)
//						|| event.getAction().equals("content_element"))
//					sum += event.getTotal();
//
//				// else if: has a pipe (data is messy), on the scene page, on
//				// one of the scenes in this guide. because scene page
//				// conversions reflect the scene they are on
//				else if (event.getLabel().lastIndexOf("|") != -1
//						&& event.getAction().equals(Event.ACTION_SCENE_PAGE)
//						&& getGuideScene(sceneId) != null)
//					sum += event.getTotal();
//
//			}
//		}
//		return sum;
//	}

	public void setLandingPageData(String landingPageData) {
		this.gaLandingPage = landingPageData;
	}

	public LandingPage getLandingPageData() {
		if (gaLandingPage == null)
			return null;
		List<List<String>> list = deserializer.deserialize(gaLandingPage);
		return new LandingPage(list.get(1));

	}

	public void setVisitsData(String visitsData) {
		this.gaVisits = visitsData;
	}

	public int getTotalVisits() {
		if (gaVisits == null)
			return 0;
		List<List<String>> list = deserializer.deserialize(gaVisits);
		return Integer.parseInt(list.get(1).get(0));
	}

	public void setVisitsByWeek(String visitsByWeek) {
		this.gaVisitsByWeek = visitsByWeek;
	}

	public List<YearMonthData> getVisitsByWeek() {
		List<YearMonthData> months = new ArrayList<YearMonthData>();
		if (gaVisitsByWeek != null) {
			List<List<String>> list = deserializer.deserialize(gaVisitsByWeek);
			for (int i = 1; i < list.size(); i++){
				months.add(new YearMonthData(list.get(i)));
			//if(i == list.size() - 1)
					//lastMonthsData = new YearMonthData(list.get(i));
			}
		}
		return months;
	}
	
	public YearMonthData getLastMonthsData(){
		if(getVisitsByWeek() != null)
			return getVisitsByWeek().get(getVisitsByWeek().size() - 1);
		else
			return null;
	}
	
	public int getLastMonthSessions(){
		if(getVisitsByWeek() != null && getVisitsByWeek().size() > 0)
			return getVisitsByWeek().get(getVisitsByWeek().size() - 1).getSessions();
		else
			return 0;
		
	}

	public void setVisitsByPath(String visitsByPath) {
		this.gaVisitsByPath = visitsByPath;
	}
	
	public List<VisitByPath> getVisitsByPath() {
		List<VisitByPath> paths = new ArrayList<VisitByPath>();
		if (gaVisitsByPath != null) {
			List<List<String>> list = deserializer.deserialize(gaVisitsByPath);
			for (int i = 1; i < list.size(); i++)
				paths.add(new VisitByPath(list.get(i)));
		}
		return paths;
	}

	public void setVisitsFeedData(String visitsFeedData) {
		this.gaVisitsFeed = visitsFeedData;
	}

	public List<WeekData> getVisitsList() {
		List<WeekData> weeks = new ArrayList<WeekData>();
		if (gaVisitsFeed != null) {
			List<List<String>> list = deserializer.deserialize(gaVisitsFeed);
			for (int i = 1; i < list.size(); i++)
				weeks.add(new WeekData(list.get(i)));
		}
		return weeks;
	}

	public void setReferralsData(String referralsData) {
		this.referralsData = referralsData;
	}

	public List<List<String>> getReferralsData() {
		return deserializer.deserialize(referralsData);
	}

	public List<Referral> getReferralsList() {
		System.out.println("getReferralsList()");
		List<Referral> referrals = new ArrayList<Referral>();

		if (referralsData != null) {

			List<List<String>> list = deserializer.deserialize(referralsData);
			for (int i = 1; i < list.size(); i++)
				referrals.add(new Referral(list.get(i)));

			// sort referrals in order of most refrrals to least
			//Collections.sort(referrals, referralsComparator);
			return referrals;
		}

		return null;


	}

	public void setCountriesData(String countriesData) {
		this.countriesData = countriesData;
	}
	
	public String getCountriesData() {
		return countriesData;
	}

	public List<Country> getCountriesList() {
		List<Country> countries = new ArrayList<Country>();
		if (countriesData != null) {
			List<List<String>> list = deserializer.deserialize(countriesData);
			for (int i = 1; i < list.size(); i++)
				countries.add(new Country(list.get(i)));

			// sort referrals in order of most refrrals to least
			//Collections.sort(countries, countriesComparator);
		}
		return countries;
	}
	
	public List<CityVisits> getCityList() {
		List<CityVisits> countries = new ArrayList<CityVisits>();
		if (countriesData != null) {
			List<List<String>> list = deserializer.deserialize(countriesData);
			for (int i = 1; i < list.size(); i++)
				countries.add(new CityVisits(list.get(i)));

		}
		return countries;
	}

	public void setConvertersEventData(String[][] convertersEventData) {
		this.convertersEventData = serializer.serialize(convertersEventData);
	}

	public List<Event> getEventsConverterList() {
		List<Event> list = new ArrayList<Event>();
		if (convertersEventData != null) {
			List<List<String>> data = deserializer
					.deserialize(convertersEventData);
			for (int i = 1; i < data.size(); i++)
				list.add(new Event(data.get(i)));
		}
		// sort referrals in order of most refrrals to least
		// Collections.sort(list, countriesComparator);

		return list;
	}

	public void setGaCacheTime(Date gaCacheTime) {
		this.gaCacheTime = gaCacheTime;
	}

	public Date getGaCacheTime() {
		return gaCacheTime;
	}

	public void setEmailReportTime(Date emailReportTime) {
		this.emailReportTime = emailReportTime;
	}

	public Date getEmailReportTime() {
		return emailReportTime;
	}

	public int getPageViews() {
		return pageViews;
	}

	public void setPageViews(int pageViews) {
		this.pageViews = pageViews;
	}

	public double getAvgTimeOnSite() {
		return avgTimeOnSite;
	}

	public void setAvgTimeOnSite(double avgTimeOnSite) {
		this.avgTimeOnSite = avgTimeOnSite;
	}
	

	public void setGuideSceneData(List<VisitByPath> visitsByPath) {
		
		for(VisitByPath visit : visitsByPath){
			GuideScene guideScene = getGuideScene(visit.getSceneId());
			if(guideScene != null){
				guideScene.avgTime = visit.getAvgTimeOnPage();
				guideScene.visits = visit.getPageViews();
			}
			
		}
		
	}

	public GuideScene getGuideScene(String sceneId) {
		for (GuideSection gs : guideSections)
			if (gs.getGuideScene(sceneId) != null)
				return gs.getGuideScene(sceneId);
		
		return null;
	}

	public int getStatus() {
		return status;
	}

//	public List<GuideSection> getGuideSections() {
//		return guideSections;
//	}
//
//	public void setGuideSections(List<GuideSection> guideSections) {
//		this.guideSections = guideSections;
//	}
}
