package co.beek.pano.model.dao.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import flexjson.JSON;

@Entity
@Table(name = "guidesections")
public class GuideSection implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;

    public static GuideSection getDefault(String guideId)
    {
        GuideSection def = new GuideSection();
        def.id = UUID.randomUUID().toString();
        def.guideId = guideId;
        def.setTitle("Scenes");
        return def;
    }

    public static final GuideSectionComparator orderComparator = new GuideSectionComparator();

    @Id
    @NotNull
    @Column(name = "id")
    public String id;
    
    @Column(name = "guide_id")
    public String guideId;

    @Column(name = "[order]")
    public int order;

    @Column(name = "title")
    private String title;

    @Column(name = "parent_section_id")
    public String parent_section_id;
    
    @Column(name = "page_image")
    public String page_image;
    
    @Column(name = "page_image_increment")
    public Integer page_image_increment;
    
	@Column(name = "markers", columnDefinition = "TINYINT")
	 public int markers;
	
    @Column(name = "page_position_X")
    public Integer pagePositionX;
    
    @Column(name = "page_position_Y")
    public Integer pagePositionY;
    
    @Column(name = "description")
    public String description;

    @Column(name = "child_guide_id")
    public String childGuideId;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "guidesection_id", updatable = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<GuideScene> guideScenes = new ArrayList<GuideScene>();

    @OneToOne
    @JoinColumn(name = "child_guide_id", insertable = false, updatable = false)
    public GuideDetail childGuide;

    @JSON(include = false)
    public GuideDetail getChildGuide() {
        return childGuide;
    }

    public void setChildGuide(GuideDetail childGuide) {
        this.childGuide = childGuide;
    }

    public List<GuideScene> getGuideScenes() {
        return guideScenes;
    }

    public void setGuideScenes(List<GuideScene> guideScenes) {
        this.guideScenes = guideScenes;
    }

    public void addScene(Scene scene) {
        guideScenes.add(GuideScene.create(id, scene));
    }

    public void addGuideScene(GuideScene guideScene) {
        guideScenes.add(guideScene);
    }

    public void removeGuideScene(GuideScene guideScene) {
        guideScenes.remove(guideScene);
    }

    public GuideScene getGuideScene(String sceneId) {
        for (GuideScene gs : guideScenes)
            if (gs.getSceneId().equals(sceneId))
                return gs;

        return null;
    }

    @JSON(include = false)
    public GuideScene getFirstScene() {
        if (guideScenes.size() == 0)
            return null;

        Collections.sort(guideScenes, GuideScene.orderComparator);

        // now return the first scene
        return guideScenes.get(0);
    }

    public void setFirstScene(Scene scene) {
        for (GuideScene guideScene : guideScenes) {
            if (guideScene.getSceneId() == scene.getId())
                guideScene.setOrder(0);
            else
                guideScene.setOrder(1 + guideScene.getOrder());
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public GuideSection cloneNow(String guideId) throws CloneNotSupportedException
    {
        GuideSection clone = (GuideSection) super.clone();
        clone.id = UUID.randomUUID().toString();
        clone.guideId = guideId;

        clone.guideScenes = new ArrayList<GuideScene>();
        for(GuideScene scene : guideScenes)
            clone.guideScenes.add(scene.cloneNow(clone.id));

        return clone;
    }

}

class GuideSectionComparator implements Comparator<GuideSection> {
    public int compare(GuideSection object1, GuideSection object2) {
        return object1.order - object2.order;
    }
}