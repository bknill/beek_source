package co.beek.pano.model.dao.entities;

import org.apache.commons.lang.StringEscapeUtils;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "hotspots")
public class Hotspot implements Serializable, Cloneable{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public String id;
    
    @Column(name = "scene_id")
    public String sceneId;

    @Column(name = "guidescene_id")
    public String guideSceneId;

    @Column(name = "load_scene_id")
    public String loadSceneId;

    @Column(name = "load_guide_id")
    public String loadGuideId;

    @Column(name = "title")
    public String title;

    @Column(name = "configuration")
    public String config;

    @Column(name = "media")
    public String media;

    @Column(name = "button")
    public String button;

    @Column(name = "description")
    public String description;

    @Column(name = "behaviour")
    public String behaviour;

    @Column(name = "status")
    public int status;


    public Hotspot() {
        // empty constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSceneId() {
        return sceneId;
    }

    public void setSceneId(String sceneId) {
        this.sceneId = sceneId;
    }

    public String getGuideSceneId() {
        return guideSceneId;
    }

    public void setGuideSceneId(String guideSceneId) {
        this.guideSceneId = guideSceneId;
    }

    public String getLoadSceneId() {
        return loadSceneId;
    }

    public void setLoadSceneId(String loadSceneId) {
        this.loadSceneId = loadSceneId;
    }

    public String getLoadGuideId() {
        return loadGuideId;
    }

    public void setLoadGuideId(String loadGuideId) {
        if(loadGuideId.length() > 0)
        this.loadGuideId = loadGuideId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getButton() {
        return button;
    }

    public void setButton(String button) {
        if(button.length() > 0)
            this.button = button;
        else
            this.button = null;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        if(description.length() > 0)
            this.description = description;
        else
            this.description = null;
    }

    public String getBehaviour() {
        return behaviour;
    }

    public void setBehaviour(String behaviour) {
        if(behaviour.length() > 0)
            this.behaviour = behaviour;
        else
            this.behaviour = null;
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        if(config.length() > 0)
            this.config = config;
        else
            this.config = null;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        if(media.length() > 0)
            this.media = media;
        else
            this.media = null;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Hotspot)) {
            return false;
        }
        Hotspot other = (Hotspot) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Hotspot[ id=" + id + " ]";
    }
}
