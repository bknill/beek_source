package co.beek.pano.model.dao.entities;

import javax.persistence.Entity;
import java.io.Serializable;
import java.util.UUID;

@Entity
public class HotspotBehaviour implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;

    private String id;
    private String time;
    private Boolean visible = true;
    private String opacity;
    private String scale;
    private String position;
    private String rotation;

    public HotspotBehaviour(){
        this.id = UUID.randomUUID().toString();
        this.visible = true;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public String getOpacity() {
        return opacity;
    }

    public void setOpacity(String opacity) {
        this.opacity = opacity;
    }

    public String getScale() {
        return scale;
    }

    public void setScale(String scale) {
        this.scale = scale;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getRotation() {
        return rotation;
    }

    public void setRotation(String rotation) {
        this.rotation = rotation;
    }
}
