package co.beek.pano.model.dao.entities;

import javax.persistence.Entity;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Entity
public class HotspotMedia implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;

    private List<HotspotMediaFile> files;

    public HotspotMedia(){

    }

    public List<HotspotMediaFile> getFiles() {
        return files;
    }

    public void setFiles(List<HotspotMediaFile> files) {
        this.files = files;
    }
}
