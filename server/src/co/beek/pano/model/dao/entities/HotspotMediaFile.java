package co.beek.pano.model.dao.entities;

import javax.persistence.Entity;
import java.io.Serializable;

@Entity
public class HotspotMediaFile implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;

    private String type;
    private String name;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
