package co.beek.pano.model.dao.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import org.hibernate.annotations.*;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import flexjson.JSON;

@Entity
@Table(name = "scenes")
public class Scene implements Serializable {
	private static final long serialVersionUID = 1L;

	public static enum SceneStatus {
		NEW(0), PENDING_APPROVAL(1), REJECTED(2), CHANGE_REQUESTED(3), ACCEPTED(
				4), CANCELLED(5),UPLOADING(6);

		private int status;

		private SceneStatus(int status) {
			this.status = status;
		}

		public int getStatus() {
			return status;
		}
	}
	
	public static int THUMB_SQUARE = 200;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String id;

	@Column(name = "location_id")
	private String locationId;

	@NotNull
	@Column(name = "type")
	private int type;

	@NotNull
	@Column(name = "status")
	private int status;
	
	@NotNull
	@Column(name = "latitude")
	private double latitude;

	@NotNull
	@Column(name = "longitude")
	private double longitude;

	@NotNull
	@Column(name = "zoom")
	private int zoom;

	@NotNull
	@Column(name = "thumbincrement")
	private int thumbIncrement;

	@NotNull
	@Column(name = "panoincrement")
	private int panoIncrement;

	@Size(max = 255)
	@Column(name = "title")
	private String title;

	@Lob
	@Column(name = "description")
	private String description;

	@Lob
	@Column(name = "comments")
	private String comments;

	@NotNull
	@Column(name = "fov")
	private int fov;

	@NotNull
	@Column(name = "tilt")
	private int tilt;

	@NotNull
	@Column(name = "pan")
	private int pan;

	@NotNull
	@Column(name = "north")
	private int north;

	@Size(max = 200)
	@Column(name = "bookingurl")
	private String bookingURL;

	@Size(max = 200)
	@Column(name = "infourl")
	private String infoURL;
	
	@Column(name = "voiceover")
	private String voiceover;

	@Column(name = "video")
	private String video;

	@Column(name = "cameradefault")
	private String cameraDefault;

	@Column(name = "output_video")
	private String outputVideo;

	@NotNull
	@Column(name = "is_default", columnDefinition = "TINYINT")
	private boolean isDefault;

	@NotNull
	@Column(name = "is_uploaded", columnDefinition = "TINYINT")
	private boolean isUploaded;

	@ManyToOne()
	@Fetch(value = FetchMode.JOIN)
	@JoinColumn(name = "location_id", insertable = false, updatable = false)
	private Location location;

/*	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@Where(clause = "status > '-1'")
	@JoinColumn(name = "scene_id", updatable = false)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Hotspot> hotspots = new ArrayList<Hotspot>();*/


	public Scene() {
	}
	
	public Scene(Location location) {
		locationId = location.getId();
		type = location.getType();
		latitude = location.getLatitude();
		longitude = location.getLongitude();
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public int getZoom() {
		return zoom;
	}

	public void setZoom(int zoom) {
		this.zoom = zoom;
	}

	public int getThumbIncrement() {
		return thumbIncrement;
	}

	public void setThumbIncrement(int thumbIncrement) {
		this.thumbIncrement = thumbIncrement;
	}

	public void incrementThumb() {
		this.thumbIncrement++;
	}

	public void incrementPano() {
		this.panoIncrement++;
	}

	public int getPanoIncrement() {
		return panoIncrement;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getFov() {
		return fov;
	}

	public void setFov(int fov) {
		this.fov = fov;
	}

	public int getTilt() {
		return tilt;
	}

	public void setTilt(int tilt) {
		this.tilt = tilt;
	}

	public int getPan() {
		return pan;
	}

	public void setPan(int pan) {
		this.pan = pan;
	}

	public int getNorth() {
		return north;
	}

	public void setNorth(int north) {
		this.north = north;
	}

	public String getBookingURL() {
		return bookingURL;
	}

	public void setBookingURL(String bookingURL) {
		this.bookingURL = bookingURL;
	}

//	public String getInfoURL() {
//		return infoURL;
//	}

	public void setInfoURL(String infoURL) {
		this.infoURL = infoURL;
	}

	@JSON(include = false)
	public Location getLocation() {
		return location;
	}
	
	public String getLocationTitle() {
		return location.getTitle();
	}
	
	public String getEmail() {
		return location.getEmail();
	}
	
	public String getPhone() {
		return location.getPhone();
	}
	
	public String getinfoURL() {
		return location.getInfoURL();
	}
	

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	
	public String getVoiceover() {
		return voiceover;
	}

	public void setVoiceover(String voiceover) {
		this.voiceover = voiceover;
	}

	public String getVideo() {return video;}

	public void setVideo(String video) {this.video = video;}

	public String getCameraDefault() {
		return cameraDefault;
	}

	public void setCameraDefault(String cameraDefault) {
		this.cameraDefault = cameraDefault;
	}



	@JSON(include = false)
	public VideoSettings getVideoSettings(){

		if(video == null)
			return null;

		try {
			return new JSONDeserializer<VideoSettings>().use(null, VideoSettings.class).deserialize(video);
		}
		catch (Exception e){

			if(video != null){
				VideoSettings videoSettings = new VideoSettings();
				videoSettings.setFilename(video);
				videoSettings.setBrightness(0.0);
				videoSettings.setContrast(0.0);
				videoSettings.setStartTime(0.0);
				videoSettings.setEndTime(0.0);
				videoSettings.setVideoType(VideoSettings.THREESIXTY);
				setVideoSettings(videoSettings);
				return videoSettings;
			}

			return null;
		}
	}

	@JSON(include = false)
	public VideoSettings getOutputVideoSettings(){

		if(outputVideo == null)
			return null;

		try {
			return new JSONDeserializer<VideoSettings>().use(null, VideoSettings.class).deserialize(outputVideo);
		}
		catch (Exception e){
			return null;
		}
	}

	public void setVideoSettings(VideoSettings videoSettings){
		try {
			video = new JSONSerializer().serialize(videoSettings);
		}
		catch (Exception e){
			e.printStackTrace();
		}
	}

	public void setOutputVideoSettings(VideoSettings videoSettings){
		try {
			outputVideo = new JSONSerializer().serialize(videoSettings);
		}
		catch (Exception e){
			e.printStackTrace();
		}
	}

	public String getOutputVideo() {
		return outputVideo;
	}

	public void setOutputVideo(String outputVideo) {
		this.outputVideo = outputVideo;
	}

	public String getThumbName() {
		return "scene_" + id + "_thumb_" + thumbIncrement + ".jpg";
	}

	public String getPanoPrefix() {
		return "scene_" + id + "_pano_" + panoIncrement;
	}

	public String getPanoName() {
		return getPanoPrefix() + ".jpg";
	}


	public boolean isDefault() {
		return isDefault;
	}

	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}

	public boolean isUploaded() {
		return isUploaded;
	}

	public void setUploaded(boolean isUploaded) {
		this.isUploaded = isUploaded;
	}


	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Scene)) {
			return false;
		}
		Scene other = (Scene) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Scene[ id=" + id + " ]";
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void toggleapproval(){status = status == 0 ? 1 : 0;}

	public int getStatus() {
		return status;
	}
	
	/**
	 * I know that this is not done right.
	 * Need to figure out how to use enums.
	 * @return
	 */
	public String getStatusText() {
		if(status == Scene.SceneStatus.NEW.getStatus())
			return "New";

		if(status == Scene.SceneStatus.ACCEPTED.getStatus())
			return "Accepted";

		if(status == Scene.SceneStatus.REJECTED.getStatus())
			return "Regected";

		if(status == Scene.SceneStatus.CHANGE_REQUESTED.getStatus())
			return "Changes Requested";
		
		return "Other Status";
	}

	public void setComments(String value) {
		this.comments = value;
	}

	/**
	 * The changes that are populated in the wizard, but not persisted in the
	 * database or serialized and sent to the client in json.
	 * 
	 * @return
	 */
	@JSON(include = false)
	public String getComments() {
		return comments;
	}

	@Transient
	private UploadedFile uploadedFile;

	public void handleFileUpload(FileUploadEvent event) {
		uploadedFile = event.getFile();
	}

	@JSON(include = false)
	public UploadedFile returnUpload() {
		return uploadedFile;
	}
}
