package co.beek.pano.model.dao.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import co.beek.pano.model.beans.GameTaskHotspot;
import org.hibernate.annotations.*;

import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.enterprizewizard.EWGroup;
import co.beek.pano.model.dao.entities.Hotspot;
import co.beek.pano.model.dao.hotspots.Bubble;
import co.beek.pano.model.dao.hotspots.Photo;
import co.beek.pano.model.dao.hotspots.Post;
import co.beek.pano.model.dao.hotspots.Poster;
import co.beek.pano.model.dao.hotspots.RssReader;
import co.beek.pano.model.dao.hotspots.Sound;
import co.beek.pano.model.dao.hotspots.Video;
import flexjson.JSON;

@Entity
@Table(name = "scenes")
public class SceneDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String id;

	@Column(name = "location_id")
	private long locationId;

	@NotNull
	@Column(name = "type")
	private int type;

	@NotNull
	@Column(name = "latitude")
	private double latitude;

	@NotNull
	@Column(name = "longitude")
	private double longitude;

	@NotNull
	@Column(name = "zoom")
	private int zoom;

	@NotNull
	@Column(name = "thumbincrement")
	private int thumbIncrement;

	@NotNull
	@Column(name = "panoincrement")
	private int panoIncrement;

	@Size(max = 255)
	@Column(name = "title")
	private String title;

/*	@Lob
	@Column(name = "description")
	private String description;*/

	@NotNull
	@Column(name = "fov")
	private int fov;

	@NotNull
	@Column(name = "tilt")
	private int tilt;

	@NotNull
	@Column(name = "pan")
	private int pan;

	@NotNull
	@Column(name = "north")
	private int north;

	@Size(max = 200)
	@Column(name = "bookingurl")
	private String bookingURL;

	@Size(max = 200)
	@Column(name = "infourl")
	private String infoURL;


	@Column(name = "video")
	private String video;

	@Column(name = "voiceover")
	private String voiceover;

	@Column(name = "cameradefault")
	private String cameraDefault;

	@Column(name = "output_video")
	private String outputVideo;

	@Immutable
	@ManyToOne()
	@Fetch(value = FetchMode.JOIN)
	@JoinColumn(name = "location_id", insertable = false, updatable = false)
	private Location location;


	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@Where(clause = "status > '-1'")
	@JoinColumn(name = "scene_id", updatable = false)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Hotspot> hotspots = new ArrayList<Hotspot>();

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "scene_id", updatable = false)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Bubble> bubbles = new ArrayList<Bubble>();


	public SceneDetail() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public int getZoom() {
		return zoom;
	}

	public void setZoom(int zoom) {
		this.zoom = zoom;
	}

	public int getThumbIncrement() {
		return thumbIncrement;
	}

	public void setThumbIncrement(int thumbIncrement) {
		this.thumbIncrement = thumbIncrement;
	}

	public void incrementThumb() {
		this.thumbIncrement++;
	}

	public void incrementPano() {
		this.panoIncrement++;
	}

	public int getPanoIncrement() {
		return panoIncrement;
	}

	public void setPanoIncrement(int panoIncrement) {
		this.panoIncrement = panoIncrement;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

/*	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}*/

	public int getFov() {
		return fov;
	}

	public void setFov(int fov) {
		this.fov = fov;
	}

	public int getTilt() {
		return tilt;
	}

	public void setTilt(int tilt) {
		this.tilt = tilt;
	}

	public int getPan() {
		return pan;
	}

	public void setPan(int pan) {
		this.pan = pan;
	}

	public int getNorth() {
		return north;
	}

	public void setNorth(int north) {
		this.north = north;
	}

	public String getBookingURL() {
		return bookingURL;
	}

	public void setBookingURL(String bookingURL) {
		this.bookingURL = bookingURL;
	}

	public String getInfoURL() {
		return infoURL;
	}

	public void setInfoURL(String infoURL) {
		this.infoURL = infoURL;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	@JSON(include = true)
	public List<Hotspot> getHotspots() {
		return hotspots;
	}

	@JSON(include = false)
	public List<GameTaskHotspot> getGameTaskHotspots(){

		ArrayList<GameTaskHotspot> tempArray = new ArrayList<GameTaskHotspot>();

		for(Hotspot hotspot : hotspots){
			GameTaskHotspot hotspotData = new GameTaskHotspot();
			hotspotData.setId(hotspot.getId());
			hotspotData.setTitle(hotspot.getTitle());
			tempArray.add(hotspotData);
		}


		return tempArray;
	}

	public void setHotspots(List<Hotspot> hotspots) {
		this.hotspots = hotspots;
	}

	public List<Bubble> getBubbles() {
		return bubbles;
	}

	public void setBubbles(List<Bubble> bubbles) {
		this.bubbles = bubbles;
	}

	public String getVoiceover() {
		return voiceover;
	}

	public void setVoiceover(String voiceover) {
		this.voiceover = voiceover;
	}

	public long getLocationId() {
		return locationId;
	}

	public void setLocationId(long locationId) {
		this.locationId = locationId;
	}

	public String getVideo() {
		return video;
	}

	public void setVideo(String video) {
		this.video = video;
	}

	public String getCameraDefault() {
		return cameraDefault;
	}

	public void setCameraDefault(String cameraDefault) {
		this.cameraDefault = cameraDefault;
	}

	public String getOutputVideo() {
		return outputVideo;
	}

	public void setOutputVideo(String outputVideo) {
		this.outputVideo = outputVideo;
	}

	/**
	 * We regenerate this client side too.
	 */
	@JSON(include = false)
	public String getThumbName() {
		return "scene_" + id + "_thumb_" + thumbIncrement + ".jpg";
	}

	public String getPanoPrefix() {
		return "scene_" + id + "_pano_" + panoIncrement;
	}

	public String getPanoName() {
		return getPanoPrefix() + ".jpg";
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof SceneDetail)) {
			return false;
		}
		SceneDetail other = (SceneDetail) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Scene[ id=" + id + " ]";
	}

	public boolean hasWritePermission(EWContact contact) {
		if (contact.isInGroup(EWGroup.GROUP_ADMIN))
			return true;

		if (contact.hasLocation(location.getId())) 
			return true;
		

		return false;
	}
}
