package co.beek.pano.model.dao.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "scenepanotask")
public class ScenePanoTask implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final int PENDING = 0;
	public static final int PROCESSING = 1;
	public static final int UPLOADING = 2;
	public static final int ERROR = 3;
	public static final int COMPLETE = 4;

	public static final int TYPE_PANO = 0;
	public static final int TYPE_TRANSCODE = 1;
	public static final int TYPE_GUIDEVIDEO = 2;
	public static final int TYPE_SCENEVIDEO = 3;
	public static final int TYPE_GUIDESCENEVIDEO = 4;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id = 0L;

	@Column(name = "email")
	private String email;


	@Column(name = "scene_id")
	private String sceneId;

	@Column(name = "guide_id")
	private String guideId;

	@Size(max = 255)
	@Column(name = "temp_file_name")
	private String tempFileName;

	@NotNull
	@Column(name = "status")
	public int status;

	@NotNull
	@Column(name = "type")
	public int type;

	@Column(name = "template")
	public String template;

	@Column(name = "options")
	public String options;



	@ManyToOne()
	@Fetch(value = FetchMode.JOIN)
	@Cascade( { org.hibernate.annotations.CascadeType.MERGE })
	@JoinColumn(name = "scene_id", insertable = false, updatable = false)
	private Scene scene;

	@ManyToOne()
	@Fetch(value = FetchMode.JOIN)
	@Cascade( { org.hibernate.annotations.CascadeType.MERGE })
	@JoinColumn(name = "guide_id", insertable = false, updatable = false)
	private GuideDetail guide;


	public Long getId() {
		return id;
	}

	public String getSceneId() {
		return sceneId;
	}

	public void setSceneId(String sceneId) {
		this.sceneId = sceneId;
	}

	public Scene getScene() {
		return scene;
	}

	public GuideDetail getGuide() {
		return guide;
	}

	public String getTempFileName() {
		return tempFileName;
	}

	public void setTempFileName(String tempFileName) {
		this.tempFileName = tempFileName;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof ScenePanoTask)) {
			return false;
		}
		ScenePanoTask other = (ScenePanoTask) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Task[ id=" + id + " ]";
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public String getOptions() {
		return options;
	}

	public void setOptions(String options) {
		this.options = options;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getGuideId() {
		return guideId;
	}

	public void setGuideId(String guideId) {
		this.guideId = guideId;
	}
}
