package co.beek.pano.model.dao.entities;

import javax.persistence.Entity;
import java.io.Serializable;
import java.util.List;

@Entity
public class VideoSettings implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;
    public static String THREESIXTY = "0";
    public static String THREESIXTYVR = "1";
    public static String ONEEIGHTY = "2";
    public static String ONEEIGHTYVR = "3";

    private String filename;
    private String lowRes;
    private String hiRes;
    private Double startTime;
    private Double endTime;
    private Double brightness;
    private Double contrast;
    private Double hue;
    private Double saturation;
    private Double volume;
    private String videoImage;
    private String videoType;

    private Integer width;
    private Integer height;
    private Double duration;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Double getStartTime() {
        return startTime;
    }

    public void setStartTime(Double startTime) {
        this.startTime = startTime;
    }

    public Double getEndTime() {
        return endTime;
    }

    public void setEndTime(Double endTime) {
        this.endTime = endTime;
    }

    public Double getDuration() {
        return duration;
    }

    public void setDuration(Double duration) {
        this.duration = duration;
    }

    public Double getBrightness() {
        return brightness;
    }

    public void setBrightness(Double brightness) {
        this.brightness = brightness;
    }

    public Double getContrast() {
        return contrast;
    }

    public void setContrast(Double contrast) {
        this.contrast = contrast;
    }

    public Double getHue() {
        return hue;
    }

    public void setHue(Double hue) {
        this.hue = hue;
    }

    public Double getSaturation() {
        return saturation;
    }

    public void setSaturation(Double saturation) {
        this.saturation = saturation;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public String getLowRes() {
        return lowRes;
    }

    public void setLowRes(String lowRes) {
        this.lowRes = lowRes;
    }

    public String getHiRes() {
        return hiRes;
    }

    public void setHiRes(String hiRes) {
        this.hiRes = hiRes;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getVideoImage() {
        return videoImage;
    }

    public void setVideoImage(String videoImage) {
        this.videoImage = videoImage;
    }

    public String getVideoType() {
            return videoType;
    }

    public void setVideoType(String videoType) {
        this.videoType = videoType;
    }
}
