package co.beek.pano.model.dao.entities;

import co.beek.pano.model.dao.googleanalytics.TrackedVisit;
import co.beek.pano.model.dao.googleanalytics.VisitByUser;
import co.beek.pano.model.dao.googleanalytics.VisitByUserEvent;
import flexjson.JSONDeserializer;
import org.hibernate.annotations.Immutable;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "visits")
public class Visit implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final VisitComparator orderComparator = new VisitComparator();
	
	private static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	@Immutable
	private static JSONDeserializer<List<List<String>>> deserializer = new JSONDeserializer<List<List<String>>>();

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long id;
	
	@NotNull
	@Column(name = "guide_id")
	public String guideId;

	@NotNull
	@Column(name = "[key]")
	private String key;
	
	@NotNull
	@Column(name = "email")
	private String email;
	
	@Column(name = "user_id")
	public String userId;
	
	@Column(name = "first_name")
	public String firstName;
	
	@Column(name = "last_name")
	public String lastName;


	@Column(name = "ga_visitData")
	public String visitData;


	@Column(name = "ga_eventData")
	public String eventData;

	@NotNull
	@Column(name = "timestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date timestamp;

	public Visit() {
		setTimestamp(new Date());
	}


	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String user_id) {
		this.userId = user_id;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String first_name) {
		this.firstName = first_name;
	}
	
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String last_name) {
		this.lastName = last_name;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	
	public String getDateFormatted()
	{
		return formatter.format(timestamp);
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	
	public String getGuideId() {
		return guideId;
	}
	
	public String getId() {
		return id.toString();
	}

	public String getVisitData() {
		return visitData;
	}

	public void setVisitData(String visitData) {
		this.visitData = visitData;
	}

	public String getEventData() {
		return eventData;
	}

	public void setEventData(String eventData) {
		this.eventData = eventData;
	}


	public List<VisitByUser> getVisitsByUser(){

		if (visitData == null)
			return null;

		List<VisitByUser> list = new ArrayList<VisitByUser>();

		List<List<String>> feed = deserializer.deserialize(visitData);
		for (int i = 1; i < feed.size(); i++){
			list.add(new VisitByUser(feed.get(i), this));
		}

		return list;
	}

	public List<VisitByUserEvent> getVisitsByUserEvents(){

		if (eventData == null)
			return null;

		List<VisitByUserEvent> list = new ArrayList<VisitByUserEvent>();

		List<List<String>> feed = deserializer.deserialize(eventData);
		for (int i = 1; i < feed.size(); i++){
			list.add(new VisitByUserEvent(feed.get(i), this));
		}

		return list;
	}

}




class VisitComparator implements Comparator<Visit> {
	public int compare(Visit object1, Visit object2) {
		return object1.getTimestamp().after(object2.getTimestamp()) ? 0 : 1;
	}
}
