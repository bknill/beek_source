package co.beek.pano.model.dao.entities;

import javax.persistence.Entity;
import java.io.Serializable;

@Entity
public class Voiceover implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;

    private String filename;
    private Double startTime;
    private Double endTime;
    private Double volume;

    private Double duration;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Double getStartTime() {
        return startTime;
    }

    public void setStartTime(Double startTime) {
        this.startTime = startTime;
    }

    public Double getEndTime() {
        return endTime;
    }

    public void setEndTime(Double endTime) {
        this.endTime = endTime;
    }

    public Double getDuration() {
        return duration;
    }

    public void setDuration(Double duration) {
        this.duration = duration;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }
}
