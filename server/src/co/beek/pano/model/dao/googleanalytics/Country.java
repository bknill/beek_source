package co.beek.pano.model.dao.googleanalytics;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

import co.beek.util.DateUtil;

public class Country {
	private List<String> data;

	private DecimalFormat df = new DecimalFormat("#.##");

	public Country(List<String> data) {
		this.data = data;
	}

	public String getName() {
		return data.get(0);
	}

	public String getCountry() {
		return data.toArray()[0].toString();
	}
	
	public String getCity() {
		return data.toArray()[1].toString();
	}

	public int getSessions() {
		return Integer.parseInt(data.toArray()[2].toString());
	}
	
	public String getSessionsString() {
		return NumberFormat.getIntegerInstance().format(getSessions());
	}
	
	public double getPageViewsPerSession() {
		return Double.parseDouble(data.toArray()[3].toString());
	}
	
	public String getPageViewsPerSessionString() {
		return df.format(getPageViewsPerSession());
	}
	
	public double getAvgSessionDuration() {
		return Double.parseDouble(data.toArray()[4].toString());
	}
	
	public String getAvgSessionDurationString() {
		return DateUtil.timeConversion(Integer.valueOf((int)Math.round(getAvgSessionDuration())));
	}
}
