package co.beek.pano.model.dao.googleanalytics;

import java.util.List;

public class Event {
	
	//String metric = "ga:totalEvents";
	//String dimensions = "ga:eventCategory,ga:eventAction,ga:eventLabel";
	//String sort = "-ga:totalEvents";
	
	
	public static String ACTION_SCENE_PAGE = "scene_page";

	public static String ACTION_LOCATION_PAGE = "location_page";

	public static String CATEGORY_CONVERSION = "conversion";

	public static String CATEGORY_SHARE = "share";

	private List<String> data;

	public Event(List<String> data) {
		this.data = data;
	}

	public String getCategory() {
		return data.toArray()[0].toString();
	}

	public String getAction() {
		return data.toArray()[1].toString();
	}

	public String getLabel() {
		return data.toArray()[2].toString();
	}

	public int getTotal() {
		return Integer.parseInt(data.toArray()[3].toString());
	}
}
