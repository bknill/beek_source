package co.beek.pano.model.dao.googleanalytics;

import java.util.List;

import co.beek.util.DateUtil;

public class LandingPage {
	private List<String> data;

	public LandingPage(List<String> data) {
		this.data = data;
	}

	public String getVisits() {
		return data.get(0);
	}

	public String getAvgTimeOnSite() {
		return data.get(1);
	}

	public String getAvgMinutesOnSite() {
		return DateUtil.convertToMinutes(data.get(1));
	}

	public String getPageViewsPerVisit() {
		return DateUtil.formatNumber(data.get(2));
	}
}
