package co.beek.pano.model.dao.googleanalytics;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

import co.beek.util.DateUtil;

public class Referral {
	private List<String> data;
	
	//String metric = "ga:sessions,ga:pageviewsPerSession,ga:avgSessionDuration";
	//String dimensions = "ga:source, ga:referralPath";

	private int sessions = 0;
	
	private DecimalFormat df = new DecimalFormat("#.##");

	public Referral(List<String> data) {
		this.data = data;
	}
	
	public String getSource() {
		if(data.toArray()[0] != null)
			return data.toArray()[0].toString();
		else 
			return null;
	}

	public String getReferralPath() {
		if(data.toArray()[1] != null)
			return data.toArray()[1].toString();
		else 
			return null;
	}
	
	public boolean wasFrom(String value) {
		return getSource().indexOf(value) > -1;
	}

	public int getSessions() {
		if(sessions != 0)
			return sessions;
		else
			return Integer.parseInt(data.toArray()[2].toString());
	}
	
	public void setSessions(int sessions){
		this.sessions = sessions;
		
	}
	
	public String getSessionsString() {
		return NumberFormat.getIntegerInstance().format(getSessions());
	}
	
	

	public Double getPageViewsPerSession() {
		return Double.parseDouble(data.toArray()[3].toString());
	}
	
	public String getPageViewsPerSessionString() {
		return df.format(getPageViewsPerSession());
	}

	public Double getAvgSessionDuration() {
		return Double.parseDouble(data.toArray()[4].toString());
	}
	
	public String getAvgSessionDurationString() {
		return DateUtil.timeConversion(Integer.valueOf((int)Math.round(getAvgSessionDuration())));
	}


}
