package co.beek.pano.model.dao.googleanalytics;

import com.google.gdata.data.analytics.DataEntry;

import co.beek.util.DateUtil;

/**
 * ga:source,ga:referralPath ga:visitors,ga:avgTimeOnSite,ga:pageviewsPerVisit
 * 
 * [1] String[4] (id=119) [0] "(direct)" (id=7587) [1] "98" (id=7588) [2]
 * "228.27884615384616" (id=7589) [3] "2.1826923076923075" (id=7590)
 * 
 * @author Daniel
 * 
 */
public class Referral2 {
	private DataEntry data;

	public Referral2(DataEntry data) {
		this.data = data;
	}

	public String getReferralSource() {
		return data.stringValueOf("ga:source");
	}

	public String getReferralPath() {
		return data.stringValueOf("ga:referralPath");
	}

	public boolean wasFrom(String value) {
		return getReferralPath().indexOf(value) > -1;
	}

	public long getVisits() {
		return data.longValueOf("ga:visitors");
	}

	public String getAvgTimeOnSite() {
		return DateUtil.formatNumber(data.doubleValueOf("ga:avgTimeOnSite"));
	}

	public String getPageViewsPerVisit() {
		return DateUtil
				.formatNumber(data.doubleValueOf("ga:pageviewsPerVisit"));
	}
}
