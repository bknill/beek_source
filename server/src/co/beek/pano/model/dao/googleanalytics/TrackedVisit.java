package co.beek.pano.model.dao.googleanalytics;

import co.beek.pano.model.dao.entities.Guide;
import co.beek.pano.model.dao.entities.Visit;
import flexjson.JSONDeserializer;
import org.hibernate.annotations.Immutable;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TrackedVisit  implements Serializable {
	private static final long serialVersionUID = 1L;

	@Immutable
	private static JSONDeserializer<List<List<String>>> deserializer = new JSONDeserializer<List<List<String>>>();

	private String data;
	public Guide guide;
	public Visit visit;

	public TrackedVisit(String list, Visit visit) {
		this.data= list;
		this.visit = visit;
	}



	public List<VisitByUser> getVisitsByUser(){

		List<VisitByUser> list = new ArrayList<VisitByUser>();

		//list<String> feed = deserializer.deserialize(data);
		//for (int i = 1; i < feed.size(); i++){
		//	list.add(new VisitByUser(feed.get(i)), visit);
		//}

		return list;
	}

	public Guide getGuide() {
		return guide;
	}

	public void setGuide(Guide guide) {
		this.guide = guide;
	}


	public Visit getVisit() {
		return visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}
}
