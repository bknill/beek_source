package co.beek.pano.model.dao.googleanalytics;

import java.util.List;

public class Visit {
	public static String METRICS = "ga:visits,ga:goal1Completions,ga:avgTimeOnSite,ga:pageViewsPerVisit";
	private List<String> data;

	public Visit(List<String> data) {
		this.data = data;
	}

	public Integer getVisits() {
		return Integer.parseInt(data.get(0));
	}

	public Double getGoal1Completions() {
		return Double.parseDouble(data.get(1));
	}

	public Double getAvgTimeOnSite() {
		return Double.parseDouble(data.get(2));
	}

	public Double getPageViewsPerVisit() {
		return Double.parseDouble(data.get(3));
	}
}
