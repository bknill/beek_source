package co.beek.pano.model.dao.googleanalytics;

import java.text.NumberFormat;
import java.util.List;

import co.beek.util.DateUtil;

/**
 * ga:pagePath,ga:pageviews,ga:avgTimeOnPage
 * @author Daniel
 *
 */
public class VisitByPath {
	private List<String> data;

	private String title;
	private String thumbName;

	public VisitByPath(List<String> data) {
		this.data = data;
	}


	public String getSceneId() {
		if(data.get(0) != null)
			return data.get(0).replaceAll("\\D+","");
		else
			return null;
	}

	public Integer getPageViews() {
		return Integer.parseInt(data.get(1).toString());
	}
	
	public String getPageViewsString(){
		return NumberFormat.getIntegerInstance().format(getPageViews());
	}

	public Double getAvgTimeOnPage() {
		return Double.parseDouble(data.get(2).toString());
	}
	
	public String getAvgTimeOnPageString(){
		return DateUtil.timeConversion(Integer.valueOf((int)Math.round(getAvgTimeOnPage())));
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}
	
	public void setThumbName(String thumbName) {
		this.thumbName = thumbName;
	}

	public String getThumbName() {
		return thumbName;
	}
}
