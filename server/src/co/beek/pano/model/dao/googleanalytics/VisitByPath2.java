package co.beek.pano.model.dao.googleanalytics;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.google.gdata.data.analytics.DataEntry;

import co.beek.util.DateUtil;

/**
 * ga:pagePath,ga:pageviews,ga:avgTimeOnPage
 * 
 * @author Daniel
 * 
 */
@SessionScoped
public class VisitByPath2 {
	private static Pattern GUIDE_MATCH = Pattern.compile("/g([0-9]+).*");
	private static Pattern SCENE_MATCH = Pattern.compile("/g[0-9]+/s([0-9]+).*");
	
	public DataEntry data;

	public String title;
	public String titleCleaned;
	public String ThumbName;

	private String guideId;
	public String visits;
	public String month;
	public String visitors;
	public String AvgTimeOnPage;
	public String AvgTimeOnSite;
	public long AvgTimeOnSiteInt;
	public Double pageviewsPerVisit;
	public Double AvgTimeOnSiteMins;
	public long newVisits;
	public long totalEvents;
	public long visitsWithEvent;
	public String key;
	public Date timestamp;

	public VisitByPath2(DataEntry path) {
		this.data = path;
	}

	public String getPagePath() {
		return data.stringValueOf("ga:pagePath");
	}
	
	public long getNewVisits() {
		return data.longValueOf("ga:newVisits");
	}
	
	public String getPagePathLevel1() {
		return data.stringValueOf("ga:pagePathLevel1");
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	
	public String getKey() {
		return data.stringValueOf("ga:customVarValue1");
	}

	public String getSceneId() {
		String path = getPagePath();
		int scenePathIndex = path.indexOf("/s");
		if (scenePathIndex > -1)
			return path.substring(scenePathIndex + 2);

		return null;
	}
	
	public String getGuideId() {
		Matcher matcher = GUIDE_MATCH.matcher(getPagePathLevel1());
		
		if(matcher.matches())
			return matcher.group(1);
		
		return null;
	}

	public long getPageViews() {
		return data.longValueOf("ga:pageviews");
	}
	
	public long getTotalEvents() {
		return data.longValueOf("ga:totalEvents");
	}
	
	public long getVisitsWithEvent() {
		return data.longValueOf("ga:visitsWithEvent");
	}
	
	public long getVisitsWithOutEvents() {
		long t = data.longValueOf("ga:visits");
		long v = data.longValueOf("ga:visitsWithEvent");
		return t - v;
	}
	
	public double getEventsPerVisitWithEvent() {
		return Math.round(data.doubleValueOf("ga:eventsPerVisitWithEvent") * 100.0 ) / 100.0;
	}
	
	
	public float getAvgTimeOnSiteInt() {
		return data.longValueOf("ga:avgTimeOnSite");
	}
	
	public Double getAvgTimeOnSiteMins() {
		return  Math.round((data.doubleValueOf("ga:avgTimeOnSite") / 60.0) * 100.0 ) / 100.0;
	}
	
	
	public double getpageviewsPerVisit() {
		return Math.round(data.doubleValueOf("ga:pageviewsPerVisit")* 100.0 ) / 100.0;
	}

	public String getAvgTimeOnPage() {
		return DateUtil.formatNumber(data.doubleValueOf("ga:avgTimeOnPage"));

	}
	
	public String getAvgTimeOnSite()  {
		
		return formatSecondsAsDuration(data.longValueOf("ga:avgTimeOnSite"));

	}
	
	public static String formatSecondsAsDuration(long seconds) {
	    long hour = seconds / 60 / 60;
	    long min = (seconds / 60) - (hour * 60);
	    long sec = seconds - (min * 60) - (hour * 60 * 60);

	    return (hour < 10 ? "0" : "") + hour + ":" + 
	           (min < 10 ? "0" : "") + min + ":" +
	           (sec < 10 ? "0" : "") + sec;
	}

	public void setAvgTimeOnSite(String AvgTimeOnSite) {
		this.AvgTimeOnSite = AvgTimeOnSite;
	}
	

	public void setTitle(String Title) {
		this.title = Title;
	}
	
	public void setThumbName(String ThumbName) {
		this.ThumbName = ThumbName;
	}
	
	public void setGuideId(String guideId) {
		this.guideId = guideId;
	}

	public String getTitle() {
		return title;
	}
	
	public String getMonth() {
		return data.stringValueOf("ga:month");
	}
	
	public String getNthMonth() {
		return data.stringValueOf("ga:nthMonth");
	}
	
	public String getYearMonth() {
		return data.stringValueOf("ga:yearMonth");
	}
	
	public String getTitleCleaned() {
		titleCleaned = title.replace("'", "");
		return titleCleaned;
	}
	
	public void setTitleCleaned(String TitleCleaned) {
		this.titleCleaned = TitleCleaned;
	}
	
	public String getThumbName() {
		return ThumbName;
	}

	public Long getVisits() {
		 return data.longValueOf("ga:visits");
	}

	public void setVisits(String visits) {
		this.visits = visits;
	}

	public Long getVisitors() {
		return data.longValueOf("ga:visitors");
	}

}