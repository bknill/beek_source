package co.beek.pano.model.dao.googleanalytics;

import co.beek.pano.model.dao.entities.Guide;
import co.beek.pano.model.dao.entities.Visit;
import co.beek.util.DateUtil;

import javax.persistence.Entity;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Entity
public class VisitByUser {
	public static String METRICS = "ga:visits,ga:avgTimeOnSite,ga:pageViewsPerVisit";

	public static final VisitByUserComparator visitByUserComparator = new VisitByUserComparator();

	private List<String> data;
	public Guide guide;
	public Visit visit;
	public List<VisitByUserEvent> events;
	public String eventData;

	public VisitByUser(List<String> list, Visit visit) {
		this.data = list;
		this.visit = visit;
		events = new ArrayList<VisitByUserEvent>();
	}

	public String getDateString() {

		SimpleDateFormat format = new SimpleDateFormat("hha dd MMM yyyy");

		return format.format(getDate());
	}

	public Date getDate() {
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHH");
		try {
			return format.parse(data.get(0));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getDateStamp(){ return data.toArray()[0].toString();};

	public String getDurationString(){
		return DateUtil.timeConversion(getDuration());
	}

	public Integer getScenesCount() {
		return Integer.parseInt(data.get(1));
	}

	public Integer getDuration() {
		String n = data.get(2).toString();
		return Integer.parseInt(n.substring(0,n.indexOf(".")));
	}

	public Guide getGuide() {
		return guide;
	}

	public void setGuide(Guide guide) {
		this.guide = guide;
	}


	public Visit getVisit() {
		return visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

	public String getEventData() {
		return eventData;
	}

	public void setEventData(String eventData) {
		this.eventData = eventData;
	}

	public List<VisitByUserEvent> getEvents() {
		return events;
	}

	public void setEvents(List<VisitByUserEvent> events) {
		this.events = events;
	}
}

class VisitByUserComparator implements Comparator<VisitByUser> {
	public int compare(VisitByUser object1, VisitByUser object2) {
		return object1.getDate().after(object2.getDate()) ? 0 : 1;
	}


}