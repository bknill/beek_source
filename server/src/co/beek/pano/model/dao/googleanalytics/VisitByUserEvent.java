package co.beek.pano.model.dao.googleanalytics;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import co.beek.pano.model.dao.entities.Visit;


public class VisitByUserEvent {

	private List<String> data;
	private Visit visit;

	public VisitByUserEvent(List<String> data, Visit visit) {
		this.data = data;
		this.visit = visit;
	}

	public Date getDate() {
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHH");
		try {
			return format.parse(data.get(0));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getDateStamp(){ return data.toArray()[0].toString();};

	public String getCategory() {
		return data.toArray()[1].toString();
	}

	public String getAction() {
		return data.toArray()[2].toString();
	}

	public String getLabel() {
		return data.toArray()[3].toString();
	}

	public int getTotal() {
		return Integer.parseInt(data.toArray()[4].toString());
	}
}
