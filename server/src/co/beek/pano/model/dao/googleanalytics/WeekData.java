package co.beek.pano.model.dao.googleanalytics;

import java.util.List;

public class WeekData {
	private List<String> data;

	public WeekData(List<String> data) {
		this.data = data;
	}

	public int getWeek() {
		return Integer.parseInt(data.get(0));
	}

	public Double getVisits() {
		return Double.parseDouble(data.get(1));
	}

	public Double getGoal1Completions() {
		return Double.parseDouble(data.get(2));
	}

	public Double getAvgTimeOnSite() {
		return Double.parseDouble(data.get(3));
	}

	public Double getPageViewsPerVisit() {
		return Double.parseDouble(data.get(4));
	}
}
