package co.beek.pano.model.dao.googleanalytics;

import java.util.List;

import flexjson.JSONDeserializer;

public class YearMonthData {
	private List<String> data;
	
	
	private static JSONDeserializer<List<List<String>>> deserializer = new JSONDeserializer<List<List<String>>>();


	public YearMonthData(List<String> list) {
		this.data = list;
	}
	
	//ga:sessions,ga:visitsWithEvent,ga:avgTimeOnSite,ga:pageviewsPerSession;
	

	public String getYearMonth() {
		return data.toArray()[0].toString();
	}

	public Integer getSessions() {
		
		if(Integer.parseInt(data.toArray()[1].toString()) > 0)
			return Integer.parseInt(data.toArray()[1].toString());
		else
			return 0;
	}

	public Integer getVisitsWithEvent() {
		return Integer.parseInt(data.toArray()[2].toString());
	}

	public Double getAvgTimeOnSite() {
		return Double.parseDouble(data.toArray()[3].toString());
	}
	
	public Double getAvgTimeMins(){
		return getAvgTimeOnSite()/60;	
	}

	public Double getPageviewsPerSession() {
		return Double.parseDouble(data.toArray()[4].toString());
	}
}
