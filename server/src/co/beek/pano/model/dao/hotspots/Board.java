package co.beek.pano.model.dao.hotspots;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "boards")
public class Board implements Serializable {
    public static final long serialVersionUID = 1L;

    @Id
    @NotNull
    @Column(name = "id")
    private String id = "";
    
    @NotNull
    @Column(name = "scene_id")
    public long sceneId;

    @NotNull
    @Column(name = "pan")
    public double pan;

    @NotNull
    @Column(name = "tilt")
    public double tilt;

    @NotNull
    @Column(name = "distance")
    public double distance;

    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "rotationy")
    public Double rotationY;

    @Column(name = "title")
    public String title;
    
    @JoinColumn(name = "board_id",updatable = false)
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@LazyCollection(LazyCollectionOption.FALSE)
    public List<BoardSign> boardSigns = new ArrayList<BoardSign>();
    
    public String getId()
    {
    	return id;
    }
    
    public String getTitle()
    {
    	return title;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Board)) {
            return false;
        }
        Board other = (Board) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Board[ id=" + id + " ]";
    }
}
