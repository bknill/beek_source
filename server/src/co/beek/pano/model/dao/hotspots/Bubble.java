package co.beek.pano.model.dao.hotspots;

import flexjson.JSON;
import org.hibernate.annotations.*;

import co.beek.pano.model.dao.entities.Scene;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "bubbles")
public class Bubble implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @NotNull
    @Column(name = "id")
    private String id;

    @NotNull
    @Column(name = "pan")
    private int pan;

    @NotNull
    @Column(name = "tilt")
    private int tilt;

    @NotNull
    @Column(name = "distance")
    private int distance;

    @Size(max = 100)
    @Column(name = "title")
    private String title;

    @Size(max = 200)
    @Column(name = "description")
    private String description;

    @Column(name = "scene_id")
    private String sceneId;

    @NotNull
    @Column(name = "target_scene_id")
    private String targetSceneId;

    @Immutable
    @ManyToOne()
    @Fetch(value = FetchMode.JOIN)
    @JoinColumn(name = "target_scene_id", insertable = false, updatable = false)
    private Scene targetScene;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getPan() {
        return pan;
    }

    public void setPan(int pan) {
        this.pan = pan;
    }

    public int getTilt() {
        return tilt;
    }

    public void setTilt(int tilt) {
        this.tilt = tilt;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleMerged() {
    	if(title != null && !title.equals(""))
    		return title;
    	
    	return targetScene.getTitle();
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

//    public Scene getScene() {
//        return scene;
//    }
//
//    public void setScene(Scene scene) {
//        this.scene = scene;
//    }

    public String getSceneId() {
        return sceneId;
    }

    public void setSceneId(String sceneId) {
        this.sceneId = sceneId;
    }

    public String getTargetSceneId() {
        return targetSceneId;
    }

    public void setTargetSceneId(String targetSceneId) {
        this.targetSceneId = targetSceneId;
    }

    @JSON(include = false)
    public Scene getTargetScene() {
        return targetScene;
    }

    public void setTargetScene(Scene targetScene) {
        this.targetScene = targetScene;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Bubble)) {
            return false;
        }
        Bubble other = (Bubble) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Bubble[ id=" + id + " ]";
    }

}
