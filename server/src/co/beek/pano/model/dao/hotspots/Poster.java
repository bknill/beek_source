package co.beek.pano.model.dao.hotspots;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang.StringEscapeUtils;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import co.beek.pano.model.dao.entities.Scene;

@Entity
@Table(name = "posters")
public class Poster implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;

    @Id
    @NotNull
    @Column(name = "id")
    public String id;
    
    @Column(name = "scene_id")
    public String sceneId;

    @Column(name = "guidescene_id")
    public String guideSceneId;
    
    @Column(name = "selectable", columnDefinition = "TINYINT")
    public boolean selectable;

    /**
     * The scene that is opened when the button is clicked.
     */
    @Column(name = "button_scene_id")
    public String buttonSceneId;
    
    @ManyToOne()
    @Fetch(value = FetchMode.SELECT)
    @NotFound(action = NotFoundAction.IGNORE)
    @Cascade({org.hibernate.annotations.CascadeType.MERGE})
    @JoinColumn(name = "button_scene_id", insertable = false, updatable = false)
    public Scene buttonScene;
    
    @Column(name = "button_favourite", columnDefinition = "TINYINT")
    public boolean buttonFavourite;
    
    @Column(name = "listen_hotspot_id")
    public String listenHotspotId;

    @Column(name = "show_icon", columnDefinition = "TINYINT")
    public boolean showIcon;
    
    @NotNull
    @Column(name = "frame")
    public int frame;
    
    @NotNull
    @Column(name = "pan")
    public double pan;

    @NotNull
    @Column(name = "tilt")
    public double tilt;

    @NotNull
    @Column(name = "distance")
    public double distance;

    @Column(name = "rotationx")
    public Double rotationX;

    @Column(name = "rotationy")
    public Double rotationY;

    @Column(name = "rotationz")
    public Double rotationZ;
    
    @Size(max = 200)
    @Column(name = "title")
    public String title;

    @Size(max = 100)
    @Column(name = "button_label")
    public String buttonLabel;
    
    @Size(max = 200)
    @Column(name = "button_url")
    public String buttonUrl;
    
    
    @Column(name = "text")
    public String text;
    
    public String getId()
    {
    	return id;
    }
    
    public String getTitle()
    {
    	return title;
    }
    
    public String getText()
    {
    	
    	return text;
    }
    
    public String getCleanText()
    {
    	if(text != null){
    	String cleanText = StringEscapeUtils.unescapeXml(text);
    	cleanText = cleanText.replaceAll("\\<[^>]*>","");
		return cleanText;
    	}
		return "";
    }
    
    public Poster cloneNow(String guideSceneId) throws CloneNotSupportedException {
    	Poster clone = (Poster) super.clone();
    	clone.id = UUID.randomUUID().toString();
    	clone.guideSceneId = guideSceneId;
    	//clone.file = null;
		return clone;
	}
}
