package co.beek.pano.model.dao.hotspots;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import co.beek.pano.model.dao.entities.FileData;

@Entity
@Table(name = "sounds")
public class Sound implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;

    @Id
    @NotNull
    @Column(name = "id")
    private String id;
    
    @Column(name = "scene_id")
    private String sceneId;

    @Column(name = "guidescene_id")
    private String guideSceneId;
    
    @Column(name = "listen_hotspot_id")
    public String listenHotspotId;
    
    @NotNull
    @Column(name = "pan")
    private double pan;

    @NotNull
    @Column(name = "tilt")
    private double tilt;

    @NotNull
    @Column(name = "distance")
    private double distance;
    
    @NotNull
    @Column(name = "ambience")
    private double ambience;

    @Column(name = "looping")
    public String looping = "0";

    @Size(max = 200)
    @Column(name = "title")
    private String title;
    
    @Column(name = "file_id", insertable = true, updatable = true)
    private long fileId;


    @ManyToOne()
    @Fetch(value = FetchMode.JOIN)
    @NotFound(action = NotFoundAction.IGNORE)
    @Cascade({org.hibernate.annotations.CascadeType.MERGE})
    @JoinColumn(name = "file_id", insertable = false, updatable = false)
    private FileData file;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSceneId() {
        return sceneId;
    }

    public void setSceneId(String sceneId) {
        this.sceneId = sceneId;
    }
    
	public void setGuideSceneId(String guideSceneId) {
		this.guideSceneId = guideSceneId;
	}

	public String getGuideSceneId() {
		return guideSceneId;
	}

    public double getPan() {
        return pan;
    }

    public void setPan(double pan) {
        this.pan = pan;
    }

    public double getTilt() {
        return tilt;
    }

    public void setTilt(double tilt) {
        this.tilt = tilt;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Sound)) {
            return false;
        }
        Sound other = (Sound) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Sound[ id=" + id + " ]";
    }

	public void setFile(FileData file) {
		this.file = file;
	}

	public FileData getFile() {
		return file;
	}

	public void setFileId(long fileId) {
		this.fileId = fileId;
	}

	public long getFileId() {
		return fileId;
	}

	public void setAmbience(double ambience) {
		this.ambience = ambience;
	}

	public double getAmbience() {
		return ambience;
	}
	
    public Sound cloneNow(String guideSceneId) throws CloneNotSupportedException {
    	Sound clone = (Sound) super.clone();
    	clone.id = UUID.randomUUID().toString();
    	clone.guideSceneId = guideSceneId;
    	//clone.file = null;
		return clone;
	}
}
