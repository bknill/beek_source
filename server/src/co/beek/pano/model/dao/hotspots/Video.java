package co.beek.pano.model.dao.hotspots;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import co.beek.pano.model.dao.entities.FileData;

@Entity
@Table(name = "videos")
public class Video implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;

    @Id
    @NotNull
    @Column(name = "id")
    private String id;
    
    @Column(name = "scene_id")
    private String sceneId;
    
    @Column(name = "guidescene_id")
    private String guideSceneId;
    
    @NotNull
    @Column(name = "file_id", insertable = true, updatable = true)
    private long fileId;
    
    @Column(name = "listen_hotspot_id")
    public String listenHotspotId;

    @Column(name = "selectable", columnDefinition = "TINYINT")
    public boolean selectable;
    
    @NotNull
    @Column(name = "frame")
    private int frame;
    
    @NotNull
    @Column(name = "pan")
    private double pan;

    @NotNull
    @Column(name = "tilt")
    private double tilt;

    @NotNull
    @Column(name = "distance")
    private double distance;

    @Column(name = "rotationx")
    private Double rotationX;

    @Column(name = "rotationy")
    private Double rotationY;

    @Column(name = "rotationz")
    private Double rotationZ;

    @Size(max = 200)
    @Column(name = "title")
    private String title;
   
    @Column(name = "looping", columnDefinition = "TINYINT")
    public boolean looping;
   
    @NotNull
    @Column(name = "volume")
    private double volume;

    @NotNull
    @Column(name = "ambience")
    private double ambience;

    @Immutable
    @ManyToOne()
    @Fetch(value = FetchMode.JOIN)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "file_id", insertable = false, updatable = false)
    private FileData file;
    
    @Column(name = "preview")
    public String preview;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSceneId() {
        return sceneId;
    }

    public void setSceneId(String sceneId) {
        this.sceneId = sceneId;
    }
    
	public void setGuideSceneId(String guideSceneId) {
		this.guideSceneId = guideSceneId;
	}

	public String getGuideSceneId() {
		return guideSceneId;
	}

    public double getPan() {
        return pan;
    }

    public void setPan(double pan) {
        this.pan = pan;
    }

    public double getTilt() {
        return tilt;
    }

    public void setTilt(double tilt) {
        this.tilt = tilt;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public Double getRotationX() {
        return rotationX;
    }

    public void setRotationX(Double rotationX) {
        this.rotationX = rotationX;
    }

    public Double getRotationY() {
        return rotationY;
    }

    public void setRotationY(Double rotationY) {
        this.rotationY = rotationY;
    }

    public Double getRotationZ() {
        return rotationZ;
    }

    public void setRotationZ(Double rotationZ) {
        this.rotationZ = rotationZ;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Video)) {
            return false;
        }
        Video other = (Video) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Video[ id=" + id + " ]";
    }

	public void setFileId(long fileId) {
		this.fileId = fileId;
	}

	public long getFileId() {
		return fileId;
	}

	public void setFile(FileData file) {
		this.file = file;
	}

	public FileData getFile() {
		return file;
	}

	public void setAmbience(double ambience) {
		this.ambience = ambience;
	}

	public double getAmbience() {
		return ambience;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}

	public double getVolume() {
		return volume;
	}

	public void setFrame(int frame) {
		this.frame = frame;
	}

	public int getFrame() {
		return frame;
	}
	
    public Video cloneNow(String guideSceneId) throws CloneNotSupportedException {
    	Video clone = (Video) super.clone();
    	clone.id = UUID.randomUUID().toString();
    	clone.guideSceneId = guideSceneId;
    	//clone.file = null;
		return clone;
	}
}
