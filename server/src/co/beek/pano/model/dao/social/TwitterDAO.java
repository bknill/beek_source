package co.beek.pano.model.dao.social;



public class TwitterDAO {
	
	private String status;
	private String user;
	private String date;

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}
	
	public void setUser(String user) {
		this.user = user;
	}

	public String getUser() {
		return user;
	}



	public void setDate(String date) {
		this.date = date;
	}

	public String getDate() {
		return date;
	}


}
