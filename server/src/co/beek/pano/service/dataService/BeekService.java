package co.beek.pano.service.dataService;

import javax.inject.Inject;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.beek.pano.model.dao.entities.Beek;
import co.beek.pano.model.dao.entities.ClientError;

@Service
@Scope("prototype")
@Transactional
public class BeekService {
	@Inject
	private SessionFactory sessionFactory;

	public Beek getBeek() throws HibernateException {
		return (Beek) sessionFactory.getCurrentSession().get(Beek.class, 1);
	}

	public void updateBeek(Beek beek) {
		sessionFactory.getCurrentSession().merge(beek);
	}

	public void logError(ClientError error) throws HibernateException {
		sessionFactory.getCurrentSession().save(error);
	}
}
