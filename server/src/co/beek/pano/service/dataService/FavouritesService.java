package co.beek.pano.service.dataService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.entities.Favourite;
import co.beek.pano.model.dao.entities.GuideDetail;
import co.beek.pano.model.dao.entities.GuideScene;
import co.beek.pano.model.dao.entities.Visit;
import co.beek.pano.model.dao.hotspots.Photo;
import co.beek.pano.model.dao.hotspots.Poster;
import co.beek.pano.service.dataService.enterprizewizardService.EWService;
import co.beek.pano.service.dataService.guideService.GuideService;
import co.beek.pano.service.dataService.hotspotService.HotspotService;
import co.beek.util.DateUtil;

@Service
@Scope("prototype")
@Transactional
public class FavouritesService {
	@Inject
	private SessionFactory sessionFactory;
	
	@Inject
	private GuideService guideService;
	
	@Inject
	private MailService mailService;
	
	@Inject
	private EWService ewService;
	
	@Inject
	private HotspotService hotspotService;
	
	public void logVisit(Visit visit) throws HibernateException {
		sessionFactory.getCurrentSession().save(visit);
	}

	public void saveVisit(Visit visit) throws HibernateException {
		sessionFactory.getCurrentSession().update(visit);
	}

	@SuppressWarnings("unchecked")
	public List<Visit> getVisits(String guideId) {
		Query query = sessionFactory.getCurrentSession()
				.createQuery("FROM Visit v WHERE v.guideId = :guideId");
		
		query.setParameter("guideId", guideId);
		return (List<Visit>) query.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Favourite> getVisitsByGuideScene(String guideId, String sceneId) {
		Query query = sessionFactory.getCurrentSession()
				.createQuery("FROM Favourite v WHERE v.guideId = :guideId AND v.sceneId = :sceneId AND v.favourite = :digit");
		
		query.setParameter("guideId", guideId);
		query.setParameter("sceneId", sceneId);
		query.setParameter("digit", 0);
		return (List<Favourite>) query.list();
	}

	@SuppressWarnings("unchecked")
	public List<Visit> getVisitsByEmail(String email) {
		Query query = sessionFactory.getCurrentSession()
				.createQuery("FROM Visit v WHERE v.email = :email");
		
		query.setParameter("email", email);
		return (List<Visit>) query.list();
	}
	
	public void logFavourite(Favourite favourite) throws HibernateException {
		sessionFactory.getCurrentSession().save(favourite);
	}

	@SuppressWarnings("unchecked")
	public List<Favourite> getFavouties() {
		return (List<Favourite>) sessionFactory.getCurrentSession()
				.createQuery("from Favourite d").list();
	}

	public void deleteFavourite(Favourite favourite) {
		sessionFactory.getCurrentSession().delete(favourite);
	}
	
	public void sendFavouriteEmails() {
		
		
		List<Favourite> allFavourites = getFavouties();
		if (allFavourites.size() == 0)
			return;

		// sorts the favourites from youngest to oldest
		Collections.sort(allFavourites, Favourite.orderComparator);

		Date tenMinutesAgo = DateUtil.tenMinutesAgo();

		List<Favourite> onlyOld = new ArrayList<Favourite>(allFavourites);
		for (Favourite f : allFavourites) {
			// remove all favourtes that have new favourite in hte last hour
			if (f.timestamp.after(tenMinutesAgo))
				removeAllWithEmail(onlyOld, f.email);
		}

		try {
			sendOnlyOldEmails(onlyOld);
		} finally {
			// delete the sent favourties
			System.out.println("Deleteing favourites");

			for (Favourite fsent : onlyOld) {
				System.out.print("Deleteing sent email to:" + fsent.id);
				deleteFavourite(fsent);
			}
		}
	}
	
	private List<Favourite> removeAllWithEmail(List<Favourite> list,
			String email) {
		List<Favourite> all = new ArrayList<Favourite>();
		for (Iterator<Favourite> itr = list.iterator(); itr.hasNext();) {
			Favourite element = itr.next();
			if (element.email.equals(email)) {
				itr.remove();
				all.add(element);
			}
		}
		return all;
	}

	private void sendOnlyOldEmails(List<Favourite> onlyOld) {
		List<Favourite> uniqueFavourites = getUniqueFavourites(onlyOld);
		for (Favourite unique : uniqueFavourites) {
			GuideDetail guide = guideService.getGuideDetail(unique.guideId);
			List<Favourite> emailFavourites = getAllEmailFavourites(onlyOld,
					unique.email);
			List<Favourite> sceneFavourites = getAllSceneFavourites(emailFavourites);
			List<Favourite> hotspotFavourites = getHotspotFavourites(emailFavourites);

			String message = "";/*<h1>" + guide.getTitle()
					+ " Guide favourites</h1>";*/

			if (sceneFavourites.size() > 0) {
				message += "<h2>Scenes:</h2>";
				for (Favourite favourite : sceneFavourites) {
					GuideScene scene = guide.getGuideScene(favourite.sceneId);

					if (scene != null) {

						message += "<h3><a href='" + favourite.url() + "'>"
								+ scene.getTitleMerged() + "</a></h3>";
						message += scene.getDescriptionMergedCleaned();
					}
					List<Favourite> sceneHotspotFavourites = removeHotspotFavourites(
							hotspotFavourites, favourite.sceneId);

					message += buildHotspotsMessage(sceneHotspotFavourites);
				}
			}

			// by now, there should only be isolated hotspots remaining
			if (hotspotFavourites.size() > 0) {
				message += "<h2>Hotspots:</h2>";
				message += buildHotspotsMessage(hotspotFavourites);
			}

			try {
				System.out.println("Sending email to:" + unique.id);
				ewService.sendEmail(EWContact.ADMIN_USER_USERNAME,
						EWContact.ADMIN_USER_PASSWORD, guide.getTeamId(),
						guide.getId(), "FavouritesTemplate", message, null,
						unique.email);
			} catch (IOException e) {
				mailService.emailError("Favourites email failed to send",
						e.getMessage() + message);
			}
		}
	}

	private List<Favourite> getUniqueFavourites(List<Favourite> favourites) {
		List<Favourite> uniques = new ArrayList<Favourite>();
		for (Favourite f : favourites)
			if (!hasEmailAndGuide(uniques, f))
				uniques.add(f);
		return uniques;
	}
	
	private boolean hasEmailAndGuide(List<Favourite> uniques,
			Favourite favourite) {
		for (Favourite f : uniques)
			if (f.guideId.equals(favourite.guideId)
					&& f.email.equals(favourite.email))
				return true;
		return false;
	}
	
	private List<Favourite> getAllEmailFavourites(List<Favourite> favourites,
			String email) {
		List<Favourite> all = new ArrayList<Favourite>();
		for (Favourite favourite : favourites)
			if (favourite.email.equals(email))
				all.add(favourite);
		return all;
	}
	
	private List<Favourite> getAllSceneFavourites(List<Favourite> favourites) {
		List<Favourite> all = new ArrayList<Favourite>();
		for (Favourite favourite : favourites)
			if (favourite.hotspotId.equals("0"))
				all.add(favourite);
		return all;
	}

	private List<Favourite> getHotspotFavourites(List<Favourite> favourites) {
		List<Favourite> all = new ArrayList<Favourite>();
		for (Favourite favourite : favourites)
			if (!favourite.hotspotId.equals("0"))
				all.add(favourite);
		return all;
	}
	
	private List<Favourite> removeHotspotFavourites(List<Favourite> list,
			String sceneId) {
		List<Favourite> all = new ArrayList<Favourite>();
		for (Iterator<Favourite> itr = list.iterator(); itr.hasNext();) {
			Favourite element = itr.next();
			if (element.sceneId.equals(sceneId)
					&& !element.hotspotId.equals("0")) {
				itr.remove();
				all.add(element);
			}
		}
		return all;
	}
	
	private String buildHotspotsMessage(List<Favourite> favourites) {
		String hotspotsMessage = "<ul>";
/*		for (Favourite myf : favourites) {
			if (!myf.hotspotId.equals("0")) {
				hotspotsMessage += "<li>";
				if (myf.type == Favourite.TYPE_PHOTO) {
					Photo photo = hotspotService.getPhoto(myf.hotspotId);
					hotspotsMessage += "<a href='" + myf.url() + "'>"
							+ photo.title + "</a><br />";
					if (photo.description != null)
						hotspotsMessage += stripFonts(photo.description)
								+ " <br />";
				}

				if (myf.type == Favourite.TYPE_POSTER) {
					Poster poster = hotspotService.getPoster(myf.hotspotId);
					hotspotsMessage += "<a href='" + myf.url() + "'>"
							+ poster.title + "</a><br />";

					if (poster.text != null)
						hotspotsMessage += stripFonts(poster.text) + " <br />";
				}
				hotspotsMessage += "</li>";
			}
		}
		hotspotsMessage += "</ul>";*/

		return hotspotsMessage;
	}
	
	private String stripFonts(String html) {
		// remove all formatting
		html = html.replaceAll("(?i)<[^>]*>", "");

		html = html.substring(0, Math.min(200, html.length() - 1));
		html = html.substring(0, html.lastIndexOf(" ")) + "...";

		// html = html.replaceAll("(?i)<TEXTFORMAT[^>]*>", "");
		// html = html.replaceAll("(?i)</TEXTFORMAT>", "");

		// html = html.replaceAll("(?i)<font[^>]*>", "");
		return html.replaceAll("(?i)</font>", "");
	}
	
}
