package co.beek.pano.service.dataService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import ec2.service.PanoService;
import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import co.beek.Constants;
import co.beek.pano.model.dao.entities.Scene;
import co.beek.pano.model.dao.entities.ScenePanoTask;
import co.beek.pano.service.dataService.sceneService.SceneService;
import co.beek.pano.service.dataService.taskService.TaskService;
import co.beek.pano.service.dataService.uploadService.S3UploadService;

@Service
@Scope("prototype")
public class FileUploadService {
	private UploadedFile uploadedFile;
	@Inject
	private TaskService taskService;
	@Inject
	private SceneService sceneService;
	@Inject
	private S3UploadService s3UploadService;

	@Value("#{buildProperties.assetsBucket}")
	private String assetsBucket;

	private UploadedFile stitchTemplate;


	public void handleSceneFileUpload(FileUploadEvent event, String sceneId,
			String email) {
		uploadedFile = event.getFile();
		File file = null;
		try {
			Scene scene = sceneService.getScene(sceneId);
			if (scene == null) {
				return;
			} else if (uploadedFile == null) {
				FacesContext.getCurrentInstance()
						.addMessage(
								null,
								new FacesMessage("Upload Operation",
										"No file uploaded"));
				return;
			}
			String tempFileName = UUID.randomUUID().toString() + ".jpg";
			file = new File(createTempDirFilePath(tempFileName));
			copyUploadedFileTo(file);

			// ScenePanoTask queuedTask = taskService.getTask();

			addPanoTask(scene.getId(), tempFileName, email);

			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage("Upload Operation",
							"File successfully uploaded"));

		} catch (IOException e) {
			e.printStackTrace();
			if (file != null)
				file.delete();
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
							"Error occured file upload failed"));
		}
	}

	public File getUploadedFile(String realname) throws IOException {
		String tempFileName = createTempDirFilePath(realname);
		java.io.File file = new java.io.File(tempFileName);
		OutputStream output = new FileOutputStream(file);
		IOUtils.copy(uploadedFile.getInputstream(), output);
		IOUtils.closeQuietly(output);

		return file;
	}



	public UploadedFile getStitchTemplate() {
		return stitchTemplate;
	}

	public void setStitchTemplate(UploadedFile file) {
		this.stitchTemplate = file;
	}

	public void uploadStitchTemplate(){

	}

	// copy uploaded file to tempDir
	public void copyUploadedFileTo(File targetFile) throws IOException {
		OutputStream output = null;
		output = new FileOutputStream(targetFile);
		IOUtils.copy(uploadedFile.getInputstream(), output);
		IOUtils.closeQuietly(output);
	}

	// add background task
	public ScenePanoTask addPanoTask(String sceneId, String tempFileName, String template) {
		ScenePanoTask task = new ScenePanoTask();
		task.setType(ScenePanoTask.TYPE_PANO);
		task.setSceneId(sceneId);
		if(tempFileName != null)
			task.setTempFileName(tempFileName);
		if(template != null)
			task.setTemplate(template);
		task.status = ScenePanoTask.PENDING;
		taskService.addTask(task);
		return task;
	}

	public ScenePanoTask addSceneVideoTask(String sceneId) {
		ScenePanoTask task = new ScenePanoTask();
		task.setType(ScenePanoTask.TYPE_SCENEVIDEO);
		task.setSceneId(sceneId);
		task.status = ScenePanoTask.PENDING;
		taskService.addTask(task);
		return task;
	}

	public void addGuideVideoTask(String guideId) {
		ScenePanoTask task = new ScenePanoTask();
		task.setType(ScenePanoTask.TYPE_GUIDEVIDEO);
		task.setGuideId(guideId);
		task.status = ScenePanoTask.PENDING;
		taskService.addTask(task);
	}

	public void addGuideSceneVideoTask(String guideId, String sceneId) {
		ScenePanoTask task = new ScenePanoTask();
		task.setType(ScenePanoTask.TYPE_GUIDESCENEVIDEO);
		task.setGuideId(guideId);
		task.setSceneId(sceneId);
		task.status = ScenePanoTask.PENDING;
		taskService.addTask(task);
	}
	
	public ScenePanoTask getPanoTask(String sceneId)
	{
		return taskService.getSceneTask(sceneId);
	}

	// creates complete path of file in tempDir
	public String createTempDirFilePath(String fileName) {
		return Constants.TEMP_DIR_PATH + fileName;
	}

	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}
}