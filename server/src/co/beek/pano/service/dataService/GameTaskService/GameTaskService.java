package co.beek.pano.service.dataService.GameTaskService;

import co.beek.pano.model.dao.enterprizewizard.EWTeam;
import co.beek.pano.model.dao.entities.*;
import org.hibernate.HibernateException;

import java.util.Date;
import java.util.List;

public interface GameTaskService {

	public GameTask getGameTask(String taskId);

	public void saveGameTask(GameTask task);

	public void deleteGameTask(GameTask task);

}
