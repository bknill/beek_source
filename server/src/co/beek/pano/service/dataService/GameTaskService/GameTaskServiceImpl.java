package co.beek.pano.service.dataService.GameTaskService;

import co.beek.pano.model.dao.GameTaskDAO;
import co.beek.pano.model.dao.GuideDAO;
import co.beek.pano.model.dao.enterprizewizard.EWTeam;
import co.beek.pano.model.dao.entities.*;
import flexjson.JSONDeserializer;
import flexjson.JSONException;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Scope("prototype")
@Transactional
public class GameTaskServiceImpl implements GameTaskService{

	@Inject
	public GameTaskDAO gameTaskDAO;

	@Inject
	private SessionFactory sessionFactory;

	@Override
	public GameTask getGameTask(String gameTaskId) throws HibernateException {
		return gameTaskDAO.getGameTask(gameTaskId);
	}

	@Override
	public void deleteGameTask(GameTask task) {
		Object taskMerged = sessionFactory.getCurrentSession().merge(task);
		sessionFactory.getCurrentSession().delete(taskMerged);
	}

	@Override
	public void saveGameTask(GameTask task){
		sessionFactory.getCurrentSession().merge(task);
	}





}
