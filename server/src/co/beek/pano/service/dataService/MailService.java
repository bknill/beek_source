package co.beek.pano.service.dataService;

import java.io.File;
import java.util.List;

import javax.inject.Inject;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.google.gdata.util.common.base.StringUtil;

import co.beek.pano.model.dao.entities.Favourite;

@Service
@Scope("prototype")
public class MailService {
	@Inject
	private JavaMailSender mailSender;

	@Inject
	private SimpleMailMessage alertMailMessage;

	@Async
	public void emailError(String subject, Throwable e) {
		String message = ExceptionUtils.getStackTrace(e);
		emailError(subject, message);
	}

	@Async
	public void emailError(String subject, String message) {
		System.out.println("MailService.emailError(" + subject + ", " + message
				+ ")");

		// if (svnRevision.equals("SVN_REVISION"))
		// return;
		try {

			SimpleMailMessage mailMessage = new SimpleMailMessage(
					alertMailMessage);
			mailMessage.setTo("ben@beek.co");
			mailMessage.setSubject(subject);
			mailMessage.setText(message);
			mailSender.send(mailMessage);

		} catch (MailException e) {
			e.printStackTrace();
		}
	}

	@Async
	public void sendEmail(String emailAddress, String subject, String message) {
		try {
			SimpleMailMessage mailMessage = new SimpleMailMessage(
					alertMailMessage);
			mailMessage.setTo(emailAddress);
			mailMessage.setSubject(subject);
			mailMessage.setText(message);
			mailSender.send(mailMessage);

		} catch (MailException e) {
			e.printStackTrace();
		}
	}

	@Async
	public void sendHTMLEmail(String emailAddress, String subject, String html,
			List<MailAttachment> attachments) {
		try {

			MimeMessage message = mailSender.createMimeMessage();

			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setTo(emailAddress);
			helper.setSubject(subject);
			helper.setText(
					StringUtil.stripHtmlTags(html),
					"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"	\"http://www.w3.org/TR/html4/loose.dtd\"><html><body>"
							+ html + "</body></html>");

			for (MailAttachment attachment : attachments)
				helper.addInline(attachment.cid, attachment.file);

			mailSender.send(message);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
