package co.beek.pano.service.dataService.destinationService;

import co.beek.pano.model.dao.entities.Destination;

import java.util.List;

public interface DestinationService {
	public Destination getDestination(String destinationId);

	public Destination getDestinationByTitle(String title);

	public List<Destination> getAllDestinations();
	
	public List<Destination> getDestinationsByTitles(List<String> titles);
	
	public void save(Destination destination);
	
	//public void delete(Destination destination);
}
