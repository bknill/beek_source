package co.beek.pano.service.dataService.destinationService;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.hibernate.HibernateException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.beek.pano.model.dao.DestinationDAO;
import co.beek.pano.model.dao.entities.Destination;

@Service
@Scope("prototype")
@Transactional
public class DestinationServiceImpl implements DestinationService {
	@Inject
	private DestinationDAO destinationDAO;

	@Override
	public Destination getDestination(String destinationId)
			throws HibernateException {
		return destinationDAO.getDestination(destinationId);
	}

	@Override
	public List<Destination> getAllDestinations() {
		return destinationDAO.getAllDestinations();
	}

	@Override
	public Destination getDestinationByTitle(String title)
			throws HibernateException {
		return destinationDAO.getDestinationByTitle(title);
	}

	@Override
	public List<Destination> getDestinationsByTitles(List<String> titles) {
		List<Destination> destinations = new ArrayList<Destination>();
		for (String title : titles) {
			Destination destination = destinationDAO
					.getDestinationByTitle(title);
			if (destination != null)
				destinations.add(destination);
		}
		return destinations;
	}

	public void save(Destination destination) throws HibernateException {
		if (destination.getId() == null)
			destinationDAO.addDestination(destination);
		else
			destinationDAO.updateDestination(destination);
	}

	public void delete(Destination destination) throws HibernateException {
		destinationDAO.deleteDestination(destination);
	}
}
