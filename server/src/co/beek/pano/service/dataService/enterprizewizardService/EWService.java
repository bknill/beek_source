package co.beek.pano.service.dataService.enterprizewizardService;

import java.io.IOException;
import java.util.List;

import co.beek.pano.model.dao.enterprizewizard.*;
//
import co.beek.pano.model.dao.entities.Destination;
import co.beek.pano.model.dao.entities.GuideDetail;
import co.beek.pano.model.dao.entities.Location;

public interface EWService {

    LoginResponse login(String username, String password) throws Exception;

    public EWCustomersResponse queryCustomers(String username, String password)
            throws IOException;

    public void sendPasswordEmail(String username) throws IOException;

    public EWPhotoShootsResponse queryPhotoShoots(String username,
                                                  String password) throws Exception;

    public EWPhotoShootResponse updatePhotoShootState(String username,
                                                      String password, String orderId, String state) throws Exception;

    public EWPhotoShootResponse requestApproval(String username,
                                                String password, String photoShootId, String state,
                                                String customerUrl, String photographersComments) throws Exception;

    public EWPhotoShootResponse approvePhotos(String username, String password,
                                              String photoShootId, String state, String customerUrl,
                                              Integer approvedScenesCount, String message) throws Exception;

    public EWCorrespondenceResponse sendEmail(String username, String password,
                                              String teamId, String guideId, String templateId, String body, String link,
                                              String toAddress) throws IOException;

    public EWResponse updateGuide(String username, String password,
                                  GuideDetail guide, Destination destination) throws Exception;

    public void updateGuideID(String username, String password, String id,
                              List<String> updateGuideID) throws Exception;

    public EWLeadResponse updateLeadPercentComplete(String username, String password, String userid,
                                                    String id, String percent_complete) throws Exception;

    public EWContactsResponse getLocationUsers(String username, String password, String locationID) throws Exception;

    public EWContactsResponse getGuideUsers(String username, String password, String guideID) throws Exception;

    public EWContactsResponse searchUserByEmail(String username, String password, String locationID) throws Exception;

    public EWContactsResponse addUserByEmail(String username, String password, String emailAddress, String locationID) throws Exception;

    public EWContactsResponse addGuideUserByEmail(String username, String password, String emailAddress, String guideID) throws Exception;

    public EWContactsResponse removeUserByEmail(String username, String password, String id, String type, String locationID) throws Exception;

    public EWContactsResponse removeGuideUserByEmail(String username, String password, String id, String type, String guideID) throws Exception;

    public EWCreateResponse createLocation(String username, String password, Location location) throws Exception;

    public EWContactsResponse updateLocation(String username, String password, Location location) throws Exception;

    public EWContactsResponse updateLocationID(String username, String password, String id, String type, String locationid) throws Exception;

    public EWContactsResponse updateGuideID(String username, String password, String id, String type, String guideid) throws Exception;

    public EWContactsResponse updateStitchTemplate(String username, String password, String id, String template) throws Exception;

    public EWLeadResponse getUserLeads(String username, String password) throws Exception;

    public EWCampaignResponse getCampaigns(String username, String password) throws Exception;

    public EWReferenceResponse getReferences(String username, String password) throws Exception;

    public EWReferenceResponse newReferences(String username, String password, String newReferenceName, String newReferenceBody) throws Exception;

    public EWCampaignResponse newCampaign(String username, String password, String newCampaign) throws Exception;

    public EWCampaignResponse updateCampaignEmail(String username, String password, String id, String email, String subject) throws Exception;

    public EWLeadResponse readLead(String username, String password, String id) throws Exception;

    public EWLeadResponse getLead(String username, String password, String id) throws Exception;

    public EWLeadEmailResponse getLeadCommunications(String username, String password, String id) throws Exception;

    public EWLeadResponse createLead(String username, String password) throws Exception;


    public EWLeadResponse updateLead(String username, String password, String id, String contactName, String contactSurname, String email, String telephone, String orgName, String orgWebsite, String comments, String percentComplete, String nextAction, String nextActionDate) throws Exception;

    public EWLeadResponse updateLeadIntroEmail(String username, String password, String id, String introEmail, String emailSubject, String campaign_name) throws Exception;

    public EWLeadResponse updateLeadCallNotes(String username, String password, String id, String call_notes) throws Exception;


    public EWLeadResponse UpdateLeadCampaign(String username, String password, String id, String new_campaign_name) throws Exception;

    public EWLeadResponse UpdateLeadNextAction(String username, String password, String id, String next_action, String next_action_date) throws Exception;

}
