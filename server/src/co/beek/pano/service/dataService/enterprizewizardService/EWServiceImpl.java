package co.beek.pano.service.dataService.enterprizewizardService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import co.beek.pano.model.dao.enterprizewizard.*;
import org.springframework.stereotype.Service;

import co.beek.Constants;
import co.beek.pano.model.dao.entities.Destination;
import co.beek.pano.model.dao.entities.GuideDetail;
import co.beek.pano.model.dao.entities.Location;

import flexjson.JSONDeserializer;

@Service
public class EWServiceImpl implements EWService {
	
	List<String> temp = new ArrayList<String>();
	
	public EWServiceImpl()
	{}
//

	/**
	 * Our login service is via EnterpriseWizard (EW).
	 * 
	 * We query EW contacts table for the user with the matching username and
	 * password.
	 * 
	 * We have a fail-over login in case EW goes down.
	 */
	public LoginResponse login(String username, String password)
			throws Exception {
		EWContactsResponse loginResponse = queryContacts(username, password);
		EWContact user = loginResponse.firstContact();
		//user.populateGuideLocationIDs();
		return new LoginResponse(user, loginResponse.getErrorMessage());
	}

	public void updateGuideID(String username, String password, String id, List<String> guideid)
			throws Exception {
		//EWContactsResponse loginResponse = updateContacts(username, password, id, guideid);
		//EWContact user = loginResponse.firstContact();
		//return new LoginResponse(user, loginResponse.getErrorMessage());
	}
	
	public void createGuide(String username, String password, GuideDetail guide) throws Exception
	{
		EWContactsResponse loginResponse = updateGuide(username, password, guide);
	}


	
	public EWContactsResponse getLocationUsers(String username, String password, String locationID) throws Exception
	{
		if (username.equals(EWContact.SUPER_USER_USERNAME)
				&& password.equals(EWContact.SUPER_USER_PASSWORD))
			return EWContactsResponse.getSuperUserResponse();

		String url = EWContactsResponse.formatGetLocationUsers(username, password, locationID);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		try {
			return new JSONDeserializer<EWContactsResponse>().deserializeInto(
					input, new EWContactsResponse());

		} catch (Exception e) {
			throw new Exception("Error decoding json into EWContactsResponse: "
					+ url+" : "+e.getMessage());
		}
	}
	
	public EWContactsResponse getGuideUsers(String username, String password, String guideID) throws Exception
	{
		if (username.equals(EWContact.SUPER_USER_USERNAME)
				&& password.equals(EWContact.SUPER_USER_PASSWORD))
			return EWContactsResponse.getSuperUserResponse();

		String url = EWContactsResponse.formatGetGuideUsers(username, password, guideID);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		try {
			return new JSONDeserializer<EWContactsResponse>().deserializeInto(
					input, new EWContactsResponse());

		} catch (Exception e) {
			throw new Exception("Error decoding json into EWContactsResponse: "
					+ url+" : "+e.getMessage());
		}
	}
	
	public EWContactsResponse addUserByEmail(String username,
			String password, String emailAddress, String locationID) throws Exception {
		if (username.equals(EWContact.SUPER_USER_USERNAME)
				&& password.equals(EWContact.SUPER_USER_PASSWORD))
			return EWContactsResponse.getSuperUserResponse();

		String url = EWContactsResponse.formatAddUserByEmail(username, password, emailAddress, locationID);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		try {
			return new JSONDeserializer<EWContactsResponse>().deserializeInto(
					input, new EWContactsResponse());

		} catch (Exception e) {
			throw new Exception("Error decoding json into EWContactsResponse: "
					+ url+" : "+e.getMessage());
		}
	}
	
	public EWContactsResponse addGuideUserByEmail(String username,
			String password, String emailAddress, String guideID) throws Exception {
		if (username.equals(EWContact.SUPER_USER_USERNAME)
				&& password.equals(EWContact.SUPER_USER_PASSWORD))
			return EWContactsResponse.getSuperUserResponse();

		String url = EWContactsResponse.formatAddGuideUserByEmail(username, password, emailAddress, guideID);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		try {
			return new JSONDeserializer<EWContactsResponse>().deserializeInto(
					input, new EWContactsResponse());

		} catch (Exception e) {
			throw new Exception("Error decoding json into EWContactsResponse: "
					+ url+" : "+e.getMessage());
		}
	}
	
	public EWContactsResponse removeUserByEmail(String username,
			String password, String id, String type, String locationID) throws Exception {
		if (username.equals(EWContact.SUPER_USER_USERNAME)
				&& password.equals(EWContact.SUPER_USER_PASSWORD))
			return EWContactsResponse.getSuperUserResponse();

		String url = EWContactsResponse.formatRemoveUserByEmail(username, password, id, type, locationID);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		try {
			return new JSONDeserializer<EWContactsResponse>().deserializeInto(
					input, new EWContactsResponse());

		} catch (Exception e) {
			throw new Exception("Error decoding json into EWContactsResponse: "
					+ url+" : "+e.getMessage());
		}
	}
	
	public EWContactsResponse removeGuideUserByEmail(String username,
			String password, String id, String type, String guideID) throws Exception {
		if (username.equals(EWContact.SUPER_USER_USERNAME)
				&& password.equals(EWContact.SUPER_USER_PASSWORD))
			return EWContactsResponse.getSuperUserResponse();

		String url = EWContactsResponse.formatRemoveGuideUserByEmail(username, password, id, type, guideID);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		try {
			return new JSONDeserializer<EWContactsResponse>().deserializeInto(
					input, new EWContactsResponse());

		} catch (Exception e) {
			throw new Exception("Error decoding json into EWContactsResponse: "
					+ url+" : "+e.getMessage());
		}
	}
	
	
	public EWContactsResponse searchUserByEmail(String username,
			String password, String emailAddress) throws Exception {
		if (username.equals(EWContact.SUPER_USER_USERNAME)
				&& password.equals(EWContact.SUPER_USER_PASSWORD))
			return EWContactsResponse.getSuperUserResponse();

		String url = EWContactsResponse.formatSearchUserByEmail(username, password, emailAddress);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		try {
			return new JSONDeserializer<EWContactsResponse>().deserializeInto(
					input, new EWContactsResponse());

		} catch (Exception e) {
			throw new Exception("Error decoding json into EWContactsResponse: "
					+ url+" : "+e.getMessage());
		}
	}
	
	public EWLeadResponse getUserLeads(String username,
			String password) throws Exception {

		String url = EWLeadResponse.formatGetUserLeads(username, password);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		try {
			return new JSONDeserializer<EWLeadResponse>().deserializeInto(
					input, new EWLeadResponse());

		} catch (Exception e) {
			throw new Exception("Error decoding json into EWContactsResponse: "
					+ url+" : "+e.getMessage());
		}
	}
	
	public EWCampaignResponse getCampaignResponse(String username,
			String password) throws Exception {

		String url = EWCampaignResponse.getCampaigns(username, password);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		try {
			return new JSONDeserializer<EWCampaignResponse>().deserializeInto(
					input, new EWCampaignResponse());

		} catch (Exception e) {
			throw new Exception("Error decoding json into EWContactsResponse: "
					+ url+" : "+e.getMessage());
		}
	}
	
	public EWLeadResponse updateLead(String username, String password, String id, String contactName,  String contactSurname,
	String email, String telephone, String orgName, String orgWebsite, String comments,  String percentComplete, String nextAction, String nextActionDate) throws Exception {
		
		String url = EWLeadResponse.formatUpdateLead(username, password, id, contactName, contactSurname, email, telephone, orgName, orgWebsite, comments, percentComplete, nextAction, nextActionDate);
		
		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		try {
			return new JSONDeserializer<EWLeadResponse>().deserializeInto(
					input, new EWLeadResponse());

		} catch (Exception e) {
			throw new Exception("Error decoding json into EWContactsResponse: "
					+ url+" : "+e.getMessage());
		}
		
		
	}
	

	public EWLeadResponse createLead(String username, String password) throws Exception {
		
		String url = EWLeadResponse.formatCreateLead(username, password);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		try {
			return new JSONDeserializer<EWLeadResponse>().deserializeInto(
					input, new EWLeadResponse());

		} catch (Exception e) {
			throw new Exception("Error decoding json into EWContactsResponse: "
					+ url+" : "+e.getMessage());
		}
		
		
	}
	
	public EWLeadResponse updateLeadIntroEmail(String username, String password, String id, String introEmail, String emailSubject, String campaign_name) throws Exception
	{
		String url = EWLeadResponse.formatUpdateLeadIntroEmail(username, password, id, introEmail, emailSubject, campaign_name);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		try {
			return new JSONDeserializer<EWLeadResponse>().deserializeInto(
					input, new EWLeadResponse());

		} catch (Exception e) {
			throw new Exception("Error decoding json into EWContactsResponse: "
					+ url+" : "+e.getMessage());
		}
		
	}

	public EWLeadResponse updateLeadCallNotes(String username, String password, String id, String callNotes) throws Exception
	{
		String url = EWLeadResponse.formatUpdateLeadCallNotes(username, password, id, callNotes);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		try {
			return new JSONDeserializer<EWLeadResponse>().deserializeInto(
					input, new EWLeadResponse());

		} catch (Exception e) {
			throw new Exception("Error decoding json into EWContactsResponse: "
					+ url+" : "+e.getMessage());
		}

	}

	
	
	public EWContactsResponse updateLocationID(String username,
			String password, String id, String type, String locationid) throws Exception {
		if (username.equals(EWContact.SUPER_USER_USERNAME)
				&& password.equals(EWContact.SUPER_USER_PASSWORD))
			return EWContactsResponse.getSuperUserResponse();

		String url = EWContactsResponse.formatUpdateLocationID(username, password, id, type, locationid);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		try {
			return new JSONDeserializer<EWContactsResponse>().deserializeInto(
					input, new EWContactsResponse());

		} catch (Exception e) {
			throw new Exception("Error decoding json into EWContactsResponse: "
					+ url+" : "+e.getMessage());
		}
	}
	
	public EWContactsResponse updateGuideID(String username,
			String password, String id, String type, String guideid) throws Exception {
		if (username.equals(EWContact.SUPER_USER_USERNAME)
				&& password.equals(EWContact.SUPER_USER_PASSWORD))
			return EWContactsResponse.getSuperUserResponse();

		String url = EWContactsResponse.formatUpdateGuideID(username, password, id, type, guideid);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		try {
			return new JSONDeserializer<EWContactsResponse>().deserializeInto(
					input, new EWContactsResponse());

		} catch (Exception e) {
			throw new Exception("Error decoding json into EWContactsResponse: "
					+ url+" : "+e.getMessage());
		}
	}
	
	public EWContactsResponse updateGuide(String username, String password, GuideDetail guide)
			throws Exception {
		if (username.equals(EWContact.SUPER_USER_USERNAME)
				&& password.equals(EWContact.SUPER_USER_PASSWORD))
			return EWContactsResponse.getSuperUserResponse();

		String url = EWContactsResponse.formatCreateGuide(username, password, guide);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		try {
			return new JSONDeserializer<EWContactsResponse>().deserializeInto(
					input, new EWContactsResponse());

		} catch (Exception e) {
			throw new Exception("Error decoding json into EWContactsResponse: "
					+ url+" : "+e.getMessage());
		}
	}

	public EWContactsResponse updateStitchTemplate(String username,
											String password, String id, String template) throws Exception {
		if (username.equals(EWContact.SUPER_USER_USERNAME)
				&& password.equals(EWContact.SUPER_USER_PASSWORD))
			return EWContactsResponse.getSuperUserResponse();

		String url = EWContactsResponse.formatUpdateTemplate(username, password, id, template);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		try {
			return new JSONDeserializer<EWContactsResponse>().deserializeInto(
					input, new EWContactsResponse());

		} catch (Exception e) {
			throw new Exception("Error decoding json into EWContactsResponse: "
					+ url+" : "+e.getMessage());
		}
	}
	
	
	public EWCreateResponse createLocation(String username, String password, Location location)
			throws Exception {

		String url = EWContactsResponse.formatCreateLocation(username, password, location);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		try {
			return new JSONDeserializer<EWCreateResponse>().deserializeInto(
					input, new EWCreateResponse());

		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new Exception("Error decoding json into EWCreateResponse: "
					+ url+" : "+e.getMessage());
		}
	}

	public EWContactsResponse updateLocation(String username, String password, Location location)
			throws Exception {
		if (username.equals(EWContact.SUPER_USER_USERNAME)
				&& password.equals(EWContact.SUPER_USER_PASSWORD))
			return EWContactsResponse.getSuperUserResponse();

		String url = EWContactsResponse.formatUpdateLocation(username, password, location);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		try {
			return new JSONDeserializer<EWContactsResponse>().deserializeInto(
					input, new EWContactsResponse());

		} catch (Exception e) {
			throw new Exception("Error decoding json into EWContactsResponse: "
					+ url+" : "+e.getMessage());
		}
	}
	

	public EWLeadResponse updateLeadPercentComplete(String username, String password, String userid,
			String id, String percent_complete) throws Exception {

		String url = EWLeadResponse.formatUpdateLeadPercentComplete(username, password, userid, id, percent_complete);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		try {
			return new JSONDeserializer<EWLeadResponse>().deserializeInto(
					input, new EWLeadResponse());

		} catch (Exception e) {
			throw new Exception("Error decoding json into EWContactsResponse: "
					+ url+" : "+e.getMessage());
		}
	}
	
	
	public EWLeadResponse readLead(String username, String password, String id) throws Exception {

		String url = EWLeadResponse.formatReadLead(username, password, id);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		try {
			return new JSONDeserializer<EWLeadResponse>().deserializeInto(
					input, new EWLeadResponse());

		} catch (Exception e) {
			throw new Exception("Error decoding json into EWContactsResponse: "
					+ url+" : "+e.getMessage());
		}
	}
	
	public EWLeadResponse getLead(String username, String password, String id) throws Exception {

		String url = EWLeadResponse.formatGetLead(username, password, id);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		try {
			return new JSONDeserializer<EWLeadResponse>().deserializeInto(
					input, new EWLeadResponse());

		} catch (Exception e) {
			throw new Exception("Error decoding json into EWContactsResponse: "
					+ url+" : "+e.getMessage());
		}
	}

	public EWLeadEmailResponse getLeadCommunications(String username, String password, String id) throws Exception {

		String url = EWLeadEmailResponse.formatGetLeadCommunications(username, password, id);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		try {
			return new JSONDeserializer<EWLeadEmailResponse>().deserializeInto(
					input, new EWLeadEmailResponse());

		} catch (Exception e) {
			throw new Exception("Error decoding json into EWContactsResponse: "
					+ url+" : "+e.getMessage());
		}
	}
	
	
	public EWContactsResponse queryContacts(String username, String password)
			throws Exception {
		if (username.equals(EWContact.SUPER_USER_USERNAME)
				&& password.equals(EWContact.SUPER_USER_PASSWORD))
			return EWContactsResponse.getSuperUserResponse();

		String url = EWContactsResponse.format(username, password);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		try {
			return new JSONDeserializer<EWContactsResponse>().deserializeInto(
					input, new EWContactsResponse());

		} catch (Exception e) {
			throw new Exception("Error decoding json into EWContactsResponse: "
					+ url+" : "+e.getMessage());
		}
	}

	public EWPhotoShootsResponse queryPhotoShoots(String username,
			String password) throws Exception {
		String url = EWPhotoShootsResponse
				.getPhotoShootsUrl(username, password);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		try {
			return new JSONDeserializer<EWPhotoShootsResponse>()
					.deserializeInto(input, new EWPhotoShootsResponse());
		} catch (Exception e) {
			throw new Exception(
					"Error decoding json into EWPhotoShootsResponse: " + url);
		}
	}

	/**
	 * @throws IOException
	 * 
	 */
	// public EWPhotoShootResponse updatePhotoShoot(String username,
	// String password, String photoShootId, String state,
	// String customerUrl) throws Exception {
	//
	// String url = EWPhotoShootResponse.updatePhotoShootUrl(username,
	// password, photoShootId, state, customerUrl, null, null);
	//
	// System.out.println(url);
	// BufferedReader input = new BufferedReader(new InputStreamReader(
	// new URL(url).openStream()));
	//
	// try {
	// return new JSONDeserializer<EWPhotoShootResponse>()
	// .deserializeInto(input, new EWPhotoShootResponse());
	// } catch (Exception e) {
	// throw new Exception(
	// "Error decoding json into EWPhotoShootResponse: " + url);
	// }
	// }

	public EWCustomersResponse queryCustomers(String username, String password)
			throws IOException {
		String url = EWCustomersResponse.getUrl(username, password);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		return new JSONDeserializer<EWCustomersResponse>().deserializeInto(
				input, new EWCustomersResponse());
	}

	public void sendPasswordEmail(String username) throws IOException {

		String url = "http://crm.beek.co/ewws/EWCreate/.async?$KB=beekorderly";

		url += "&$login=" + EWContact.ADMIN_USER_USERNAME;
		url += "&$password=" + EWContact.ADMIN_USER_PASSWORD;

		url += "&$lang=en&$table=integration_request";
		url += "&request_type=forgotten_password";
		url += "&user_login=" + username;
		url += "&url=http%3A%2F%2Fgms." + Constants.domain;

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));
	}

	public EWCorrespondenceResponse sendEmail(String username, String password, 
			String teamId, String guideId, String templateId, String body, String link,
			String toAddress)
			throws IOException {
		String url = EWCorrespondenceResponse.sendCorrespondenceUrl(username,
				password, teamId, guideId, templateId, body, link, toAddress);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		return new JSONDeserializer<EWCorrespondenceResponse>()
				.deserializeInto(input, new EWCorrespondenceResponse());
	}

	/**
	 * Simply
	 */
	@Override
	public EWPhotoShootResponse updatePhotoShootState(String username,
			String password, String photoShootId, String state)
			throws Exception {
		return updatePhotoShoot(username, password, photoShootId, state, null,
				null, null, null);
	}

	@Override
	public EWPhotoShootResponse requestApproval(String username,
			String password, String photoShootId, String state,
			String customerUrl, String photographersComments) throws Exception {
		return updatePhotoShoot(username, password, photoShootId, state,
				customerUrl, null, null, photographersComments);
	}

	public EWPhotoShootResponse approvePhotos(String username, String password,
			String photoShootId, String state, String customerUrl,
			Integer approvedScenesCount, String comments) throws Exception {
		return updatePhotoShoot(username, password, photoShootId, state,
				customerUrl, approvedScenesCount, comments, null);
	}

	private EWPhotoShootResponse updatePhotoShoot(String username,
			String password, String photoShootId, String state,
			String customerUrl, Integer approvedScenesCount, String comments,
			String photographersComments) throws Exception {

		String url = EWPhotoShootResponse.updatePhotoShootUrl(username,
				password, photoShootId, state, customerUrl,
				approvedScenesCount, comments, photographersComments);

		System.out.println(url);
		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		try {
			return new JSONDeserializer<EWPhotoShootResponse>()
					.deserializeInto(input, new EWPhotoShootResponse());
		} catch (Exception e) {
			throw new Exception(
					"Error decoding json into EWPhotoShootResponse: " + url);
		}
	}

	/**
	 * http://crm.beek.co/ewws/EWCreate/.json?$KB=beekorderly
	 * &$login=gms&$password=B33kb33k&$lang=en&$table=guides
	 * &guide_name=Wellington+guide
	 * &guideid=66
	 * &customer_token=:CUSTOMER_123_
	 * &destination=Wellington
	 * &status=free
	 */
	public EWResponse updateGuide(String username, String password,
			GuideDetail guide, Destination destination) throws Exception {

		String url = "http://crm.beek.co/ewws/EWCreate/.json?$KB=beekorderly"
				+ "&$table=guides&$lang=en"
				// The login of the photographer
				+ "&$login=" + username + "&$password=" + password;
		
		url += "&guide_name=" + URLEncoder.encode(guide.getTitle(), "UTF-8");
		url += "&guideid=" + guide.getId();
		url += "&customer_token=:" + guide.getTeamId();
		url += "&destination=" + destination.getTitle();
		url += "&status=" + guide.getStatusString();

		try {
			return new JSONDeserializer<EWResponse>().deserializeInto(
					new BufferedReader(new InputStreamReader(new URL(url)
							.openStream())), new EWResponse());
		} catch (Exception e) {
			throw new Exception(
					"Error decoding json into EWPhotoShootResponse: " + url);
		}
	}

	public EWCampaignResponse getCampaigns(String username, String password)
			throws Exception {
		// TODO Auto-generated method stub
		String url = EWCampaignResponse.getCampaigns(username,
				password);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		try {
			return new JSONDeserializer<EWCampaignResponse>()
					.deserializeInto(input, new EWCampaignResponse());
		} catch (Exception e) {
			throw new Exception(
					"Error decoding json into EWPhotoShootResponse: " + url);
		}
	}


	public EWCampaignResponse newCampaign(String username, String password,
			String newCampaign) throws Exception {
		// TODO Auto-generated method stub
		String url = EWCampaignResponse.newCampaign(username,
				password, newCampaign);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));
		
		try {
			return new JSONDeserializer<EWCampaignResponse>()
					.deserializeInto(input, new EWCampaignResponse());
		} catch (Exception e) {
			throw new Exception(
					"Error adding new campaign" + url);
		}
	}

	public EWLeadResponse UpdateLeadCampaign(String username, String password,
			String id, String new_campaign_name) throws Exception {

		String url = EWLeadResponse.formatUpdateLeadCampaign(username, password, id, new_campaign_name);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		try {
			return new JSONDeserializer<EWLeadResponse>().deserializeInto(
					input, new EWLeadResponse());

		} catch (Exception e) {
			throw new Exception("Can't add campaign "
					+ url+" : "+e.getMessage());
		}
	}
	
	public EWLeadResponse UpdateLeadNextAction(String username, String password,
			String id, String next_action, String next_action_date) throws Exception {

		String url = EWLeadResponse.formatUpdateLeadNextAction(username, password, id, next_action, next_action_date);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		try {
			return new JSONDeserializer<EWLeadResponse>().deserializeInto(
					input, new EWLeadResponse());

		} catch (Exception e) {
			throw new Exception("Can't update next action:  "
					+ url+" : "+e.getMessage());
		}
	}
	
	public EWCampaignResponse updateCampaignEmail(String username, String password, String id, String email, String subject) throws Exception {

		String url = EWCampaignResponse.updateCampaignEmail(username, password, id, email, subject);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		try {
			return new JSONDeserializer<EWCampaignResponse>().deserializeInto(
					input, new EWCampaignResponse());

		} catch (Exception e) {
			throw new Exception("Can't add campaign "
					+ url+" : "+e.getMessage());
		}
	}


	public EWReferenceResponse getReferences(String username, String password)
			throws Exception {
		
		String url = EWReferenceResponse.getReferences(username, password);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));

		try {
			return new JSONDeserializer<EWReferenceResponse>().deserializeInto(
					input, new EWReferenceResponse());

		} catch (Exception e) {
			throw new Exception("Error decoding json into EWContactsResponse: "
					+ url+" : "+e.getMessage());
		}
	}


	public EWReferenceResponse newReferences(String username, String password,
			String newReferenceName, String newReferenceBody) throws Exception {
		String url = EWReferenceResponse.newReference(username,
				password, newReferenceName, newReferenceBody);

		BufferedReader input = new BufferedReader(new InputStreamReader(
				new URL(url).openStream()));
		
		try {
			return new JSONDeserializer<EWReferenceResponse>()
					.deserializeInto(input, new EWReferenceResponse());
		} catch (Exception e) {
			throw new Exception(
					"Error adding new campaign" + url);
		}

	}


}


