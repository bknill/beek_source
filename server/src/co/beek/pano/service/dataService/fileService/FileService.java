package co.beek.pano.service.dataService.fileService;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletInputStream;

import org.springframework.web.multipart.MultipartFile;

import co.beek.pano.model.dao.entities.FileData;

public interface FileService {
	public List<FileData> getPublicFiles();

	public FileData uploadBytes(String fileName, ServletInputStream inputStream)
			throws Exception;

	public FileData uploadFile( MultipartFile upload)
			throws Exception;

	public FileData cacheFile(String url, String filename)
			throws Exception;

	public FileData uploadVideo(MultipartFile upload)
			throws Exception;
}
