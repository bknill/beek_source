package co.beek.pano.service.dataService.fileService;

import co.beek.Constants;
import co.beek.pano.model.dao.FileDAO;
import co.beek.pano.model.dao.entities.FileData;
import co.beek.pano.service.dataService.uploadService.S3UploadService;
import co.beek.pano.util.ImageUtil;
import co.beek.pano.util.VideoUtil;

import org.apache.commons.io.FileUtils;
import org.hibernate.HibernateException;
import org.jets3t.service.S3ServiceException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import javax.servlet.ServletInputStream;

import java.io.File;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@Service
@Scope("prototype")
@Transactional
public class FileServiceImpl implements FileService {
	@Value("#{buildProperties.assetsBucket}")
	private String assetsBucket;

	@Inject
	private FileDAO fileDAO;

	@Inject
	private S3UploadService s3UploadService;

	@Override
	public List<FileData> getPublicFiles() throws HibernateException {
		return fileDAO.getPublicFiles();
	}

	@Override
	public FileData uploadBytes( String fileName,
			ServletInputStream inputStream) throws Exception {
		FileData fileData = new FileData();
		fileData.setTeamId("1");
		fileData.setFileName(fileName);
		fileData.setRealName(ImageUtil.getRealName(fileName));

		java.io.File tempFile = new java.io.File(Constants.TEMP_DIR_PATH
				+ fileData.getRealName());

		ImageUtil.copyTo(inputStream, tempFile);
		
		s3UploadService.uploadFile(assetsBucket, tempFile);

		// Save the file to the database
		fileDAO.addFile(fileData);


			// delete the temp fiel after uploading
			tempFile.delete();

		// delete the temp file after uploading
		tempFile.delete();

		return fileData;
	}

	/**
	 * Handles a Multi-part file upload from the rest service. Renames and
	 * uploads the file to S3. Makes an insert into the files table.
	 * 
	 * @throws S3ServiceException
	 * @throws NoSuchAlgorithmException
	 */
	@Override
	public FileData uploadFile( MultipartFile upload)
			throws Exception {

		System.out.println("uploadFile " + upload.getName());
		// Generate the hashed filename for realname.
		String filename = upload.getOriginalFilename();
		String realname = ImageUtil.getRealName(filename);

		// save the new file
		FileData file = new FileData();
		file.setFileName(filename);
		file.setRealName(realname);
		file.setTeamId("1");

		// use realname for temp file to reduce potential conflict.
		java.io.File tempFile = new java.io.File(Constants.TEMP_DIR_PATH
				+ realname);
		upload.transferTo(tempFile);

		s3UploadService.uploadFile(assetsBucket, tempFile);
		
		// Save the file to the database
		fileDAO.addFile(file);

		// delete the temp fiel after uploading
		tempFile.delete();

		// Return the file to the rest service
		return file;
	}
	
	@Override
	public FileData uploadVideo(MultipartFile upload)
			throws Exception {

		System.out.println("uploadVideo " + upload.getName());
		// Generate the hashed filename for realname.
		String filename = upload.getOriginalFilename();
		String realname = ImageUtil.getRealName(filename);

		// save the new file
		FileData file = new FileData();
		file.setFileName(filename);
		file.setRealName(realname);

		// use realname for temp file to reduce potential conflict.
		java.io.File tempFile = new java.io.File(Constants.TEMP_DIR_PATH
				+ realname);
		upload.transferTo(tempFile);

        File mp4File  =  VideoUtil.addMoovAtomAtBeginning(tempFile);
        File webmFile =  VideoUtil.toWebm(mp4File);
        File ogvFile  =  VideoUtil.toOgv(mp4File);

		// perform the upload first
		s3UploadService.uploadFile(assetsBucket, mp4File);
        s3UploadService.uploadFile(assetsBucket, webmFile);
        s3UploadService.uploadFile(assetsBucket, ogvFile);

		// Save the file to the database
		fileDAO.addFile(file);

		// delete the temp fiel after uploading
		tempFile.delete();
        mp4File.delete();
        webmFile.delete();
        ogvFile.delete();

		// Return the file to the rest service
		return file;
	}

	@Override
	public FileData cacheFile( String url, String videoId)
			throws Exception {
		String realname = ImageUtil.getRealName("preview.jpg");

		// save the new file
		FileData file = new FileData();
		file.setFileName(videoId);
		file.setRealName(realname);

		// use realname for temp file to reduce potential conflict.
		java.io.File tempFile = new java.io.File(Constants.TEMP_DIR_PATH
				+ realname);

		FileUtils.copyURLToFile(new URL(url), tempFile);

		// perform the upload first
		s3UploadService.uploadFile(assetsBucket, tempFile);

		// Save the file to the database
		fileDAO.addFile(file);

		// delete the temp fiel after uploading
		tempFile.delete();

		// Return the file to the rest service
		return file;
	}
}
