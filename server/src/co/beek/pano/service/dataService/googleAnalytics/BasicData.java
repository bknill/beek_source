package co.beek.pano.service.dataService.googleAnalytics;

import java.util.List;


public class BasicData {

	private List<String> data;

	public BasicData(List<List<String>> list) {
		this.data = list.get(0);
	}

	public Integer getVisits() {
		if(data.toArray()[0] != null)
			return Integer.parseInt(data.toArray()[0].toString());
		else
			return 0;
	}

	public Integer getPageViews() {
		if(data.toArray()[1] != null)
			return Integer.parseInt(data.toArray()[1].toString());
		else
			return 0;
	}

	public double getAvgTime() {
		if(data.toArray()[2] != null)
			return Double.parseDouble(data.toArray()[2].toString());
		else
			return 0.0;
	}
	
	public double getAvgScenes() {
		if(data.toArray()[3] != null)
			return Double.parseDouble(data.toArray()[3].toString());
		else
			return 0.0;
	}
}
