/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.beek.pano.service.dataService.googleAnalytics;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gdata.client.analytics.AnalyticsService;
import com.google.gdata.client.analytics.DataQuery;
import com.google.gdata.data.Link;
import com.google.gdata.data.analytics.Aggregates;
import com.google.gdata.data.analytics.DataEntry;
import com.google.gdata.data.analytics.DataFeed;
import com.google.gdata.data.analytics.DataSource;
import com.google.gdata.data.analytics.Dimension;
import com.google.gdata.data.analytics.Metric;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;

/**
 * Sample program demonstrating how to make a data request to the GA Data Export
 * using client login authentication as well as accessing important data in the
 * feed.
 */
@Service
@Scope("prototype")
@Transactional
public class BeekAnalayticsService {

	private static final String CLIENT_USERNAME = "gms@beek.co";
	private static final String CLIENT_PASS = "B33kb33k";
	private static final String PROFILE_ID = "54190402";
	private static final String URL = "https://www.googleapis.com/analytics/v2.4/data";
	private static final String API_KEY = "AIzaSyD7LPriAgkDl6i-VjHZNCZhBGDl3kaF714";

	public static int numQueries;

	private AnalyticsService as;

	// ------------------------------------------------------
	// Configure GA API
	// ------------------------------------------------------
	public BeekAnalayticsService()  {
	}
	
	public void init() throws AuthenticationException
	{
		if(as == null)
		{
			as = new AnalyticsService("54190402-beek-v2");
			as.setUserCredentials(CLIENT_USERNAME, CLIENT_PASS);
		}
	}

	// ------------------------------------------------------
	// GA Data Feed
	// ------------------------------------------------------
	// first build the query
	public DataQuery newQuery()
			throws MalformedURLException {
		numQueries++;
		
		DataQuery query = new DataQuery(new URL(URL));
		query.setIds("ga:" + PROFILE_ID);
		query.setStringCustomParameter("key", API_KEY);
		return query;
	}

	// first build the query
	public DataQuery newQuery(String startDate, String endDate, int maxResults)
			throws MalformedURLException {
		DataQuery query = newQuery();
		query.setStartDate(startDate);
		query.setEndDate(endDate);
		query.setMaxResults(maxResults);
		return query;
	}

	/**
	 * Requests data from analytics given every parameter. If you don't want an
	 * optional parameter just give it a blank string
	 * 
	 * @param as
	 * @param dimensions
	 * @param metrics
	 * @param segment
	 * @param start
	 * @param end
	 * @param maxResults
	 * @return
	 * @throws ServiceException
	 * @throws IOException
	 */
	// public List<DataEntry> makeRequest(GARequest request) throws Exception {
	// DataQuery query = makeQuery(request.start, request.end,
	// request.maxResults);
	//
	// if (request.dimensions != null)
	// query.setDimensions(request.dimensions);
	//
	// if (request.metrics != null)
	// query.setMetrics(request.metrics);
	//
	// if (request.filter != null)
	// query.setFilters(request.filter);
	//
	// if (request.sort != null)
	// query.setSort(request.sort);
	//
	// if (request.segment != null)
	// query.setSegment(request.segment);
	//
	// return makeRequest(query);
	// }

	public List<DataEntry> executeQuery(DataQuery query) throws IOException,
			ServiceException {
		DataFeed feed = as.getFeed(query.getUrl(), DataFeed.class);
		return feed.getEntries();
	}

	private DataFeed makeFeedFromUrl(AnalyticsService as, URL url) {
		DataFeed feed = null;
		try {
			feed = as.getFeed(url, DataFeed.class);
		} catch (IOException e) {
			System.err.println("Network error trying to retrieve feed: "
					+ e.getMessage());
			return feed;
		} catch (ServiceException e) {
			System.err
					.println("Analytics API responded with an error message: "
							+ e.getMessage());
			return feed;
		}
		return feed;
	}

	// ------------------------------------------------------
	// Format Feed Related Data
	// ------------------------------------------------------
	/**
	 * Output the information specific to the feed.
	 * 
	 * @param {DataFeed} feed Parameter passed back from the feed handler.
	 */
	private static void outputFeedData(DataFeed feed) {
		System.out.println("\nFeed Title      = "
				+ feed.getTitle().getPlainText() + "\nFeed ID         = "
				+ feed.getId() + "\nTotal Results   = "
				+ feed.getTotalResults() + "\nSart Index      = "
				+ feed.getStartIndex() + "\nItems Per Page  = "
				+ feed.getItemsPerPage() + "\nStart Date      = "
				+ feed.getStartDate().getValue() + "\nEnd Date        = "
				+ feed.getEndDate().getValue());
	}

	/**
	 * Output information about the data sources in the feed. Note: the GA
	 * Export API currently has exactly one data source.
	 * 
	 * @param {DataFeed} feed Parameter passed back from the feed handler.
	 */
	private void outputFeedDataSources(DataFeed feed) {
		DataSource gaDataSource = feed.getDataSources().get(0);
		System.out.println("\nTable Name      = "
				+ gaDataSource.getTableName().getValue()
				+ "\nTable ID        = " + gaDataSource.getTableId().getValue()
				+ "\nWeb Property Id = "
				+ gaDataSource.getProperty("ga:webPropertyId")
				+ "\nProfile Id      = "
				+ gaDataSource.getProperty("ga:profileId")
				+ "\nAccount Name    = "
				+ gaDataSource.getProperty("ga:accountName"));
	}

	/**
	 * Output all the metric names and values of the aggregate data. The
	 * aggregate metrics represent values across all of the entries selected by
	 * the query and not just the rows returned.
	 * 
	 * @param {DataFeed} feed Parameter passed back from the feed handler.
	 */
	private void outputFeedAggregates(DataFeed feed) {
		Aggregates aggregates = feed.getAggregates();
		List<Metric> aggregateMetrics = aggregates.getMetrics();
		for (Metric metric : aggregateMetrics) {
			System.out.println("\nMetric Name  = " + metric.getName()
					+ "\nMetric Value = " + metric.getValue()
					+ "\nMetric Type  = " + metric.getType()
					+ "\nMetric CI    = "
					+ metric.getConfidenceInterval().toString());
		}
	}

	/**
	 * Output all the important information from the first entry in the data
	 * feed.
	 * 
	 * @param {DataFeed} feed Parameter passed back from the feed handler.
	 */
	private void outputEntryRowData(DataFeed feed) {
		List<DataEntry> entries = feed.getEntries();
		if (entries.size() == 0) {
			System.out.println("Analytics - No entries found");
			return;
		}
		DataEntry singleEntry = entries.get(0);

		// properties specific to all the entries returned in the feed
		System.out.println("Entry ID    = " + singleEntry.getId());
		System.out.println("Entry Title = "
				+ singleEntry.getTitle().getPlainText());

		// iterate through all the dimensions
		List<Dimension> dimensions = singleEntry.getDimensions();
		for (Dimension dimension : dimensions) {
			System.out.println("Dimension Name  = " + dimension.getName());
			System.out.println("Dimension Value = " + dimension.getValue());
		}

		// iterate through all the metrics
		List<Metric> metrics = singleEntry.getMetrics();
		for (Metric metric : metrics) {
			System.out.println("Metric Name  = " + metric.getName());
			System.out.println("Metric Value = " + metric.getValue());
			System.out.println("Metric Type  = " + metric.getType());
			System.out.println("Metric CI    = "
					+ metric.getConfidenceInterval().toString());
		}
	}

	/**
	 * Get the data feed values in the feed as a string.
	 * 
	 * @param {DataFeed} feed Parameter passed back from the feed handler.
	 * @return {String} This returns the contents of the feed.
	 */
	private String getFeedTable(DataFeed feed) {
		List<DataEntry> entries = feed.getEntries();
		if (entries.size() == 0) {
			return "No entries found";
		}
		DataEntry singleEntry = entries.get(0);
		List<Dimension> dimensions = singleEntry.getDimensions();
		List<Metric> metrics = singleEntry.getMetrics();
		List<String> feedDataNames = new ArrayList<String>();
		String feedDataValues = "";

		// put all the dimension and metric names into an array
		for (Dimension dimension : dimensions) {
			feedDataNames.add(dimension.getName());
		}
		for (Metric metric : metrics) {
			feedDataNames.add(metric.getName());
		}

		// put the values of the dimension and metric names into the table
		for (DataEntry entry : entries) {
			for (String dataName : feedDataNames) {
				feedDataValues += "\n" + dataName + "\t= "
						+ entry.stringValueOf(dataName);
			}
			feedDataValues += "\n";
		}
		return feedDataValues;
	}

	public void printInfo(List<DataEntry> entries) {
		if (entries.size() == 0) {
			System.out.println("No entries found");
			return;
		}

		DataEntry singleEntry = entries.get(0);
		List<Dimension> dimensions = singleEntry.getDimensions();
		List<Metric> metrics = singleEntry.getMetrics();
		List<String> feedDataNames = new ArrayList<String>();

		System.out.println("");
		// put all the dimension and metric names into an array
		for (Dimension dimension : dimensions) {
			System.out.print(dimension.getName() + ",");
			feedDataNames.add(dimension.getName());
		}
		for (Metric metric : metrics) {
			System.out.print(metric.getName() + ",");
			feedDataNames.add(metric.getName());
		}

		for (int i = 0; i < entries.size(); i++) {
			for (int j = 0; j < feedDataNames.size(); j++) {
				System.out.print(entries.get(i).stringValueOf(
						feedDataNames.get(j))
						+ ",");
			}
			System.out.println("");
		}
	}

	/**
	 * Function I wrote to make tables out of data feeds that span multiple
	 * pages it gets the first page and then continously requests more data and
	 * adds to one output table until it has gone through everything
	 * 
	 * @param feed
	 * @param as
	 * @return
	 */
	private String[][] getFeedTableMultiple(DataFeed feed) {

		String[][] table = getFeedTable2(feed);

		// if (feed.getTotalResults()==0) throw new
		// Error("0 results for time period specified");

		// some queries may have no results
		if (feed.getTotalResults() == 0)
			return new String[0][0];

		int pages = (feed.getTotalResults() - 1) / feed.getItemsPerPage() + 1;

		for (int i = 1; i < pages; i++)

		{
			Link nextLink = feed.getNextLink();

			String url = nextLink.getHref();

			URL nextUrl = null;
			try {
				nextUrl = new URL(url);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			DataFeed nextFeed = makeFeedFromUrl(as, nextUrl);
			String[][] nextPage = getFeedTable2(nextFeed);
			table = (String[][]) ArrayUtils.addAll(table, nextPage);

			feed = nextFeed;

		}
		// print out the table

		// for (int i=0;i<table.length;i++){
		// System.out.println("Row: "+Integer.toString(i));
		// for (int j=0;j<table[i].length;j++){
		// System.out.print(table[i][j]+"  ");
		// }
		// System.out.println("");
		// }
		//
		//
		return table;

	}

	/**
	 * Function that takes a data feed and reformats the data into a two
	 * dimensional string array
	 */
	private String[][] getFeedTable2(DataFeed feed) {

		List<DataEntry> entries = feed.getEntries();
		if (entries == null)
			throw new Error(
					"Feed object is null, probably request to analytics");
		if (entries.isEmpty())
			return new String[1][1];

		DataEntry singleEntry = entries.get(0);
		List<Dimension> dimensions = singleEntry.getDimensions();
		List<Metric> metrics = singleEntry.getMetrics();
		List<String> feedDataNames = new ArrayList<String>();

		// put all the dimension and metric names into an array
		for (Dimension dimension : dimensions) {
			feedDataNames.add(dimension.getName());
		}
		for (Metric metric : metrics) {
			feedDataNames.add(metric.getName());
		}

		String[][] table = new String[entries.size() + 1][feedDataNames.size()];
		// put the values of the dimension and metric names into the table

		// for (int i = 0; i < feedDataNames.size(); i++) {
		// table[0][i] = feedDataNames.get(i);
		// }

		for (int i = 0; i < entries.size(); i++) {
			for (int j = 0; j < feedDataNames.size(); j++) {

				table[i + 1][j] = entries.get(i).stringValueOf(
						feedDataNames.get(j));
			}

		}
		return table;
	}

	/**
	 * Separate function that returns a two dimensional string array from a data
	 * feed made just for a table with referrals I made this because the
	 * referral urls are returned from analytics in two parts base and path this
	 * function combines them
	 * 
	 * @param feed
	 * @return
	 */
	private String[][] getReferralTable(DataFeed feed) {

		List<DataEntry> entries = feed.getEntries();

		if (entries.isEmpty()) {
			return new String[1][1];
		}

		DataEntry singleEntry = entries.get(0);
		List<Dimension> dimensions = singleEntry.getDimensions();
		List<Metric> metrics = singleEntry.getMetrics();
		List<String> feedDataNames = new ArrayList<String>();

		// put all the dimension and metric names into an array
		for (Dimension dimension : dimensions) {
			feedDataNames.add(dimension.getName());
		}
		for (Metric metric : metrics) {
			feedDataNames.add(metric.getName());
		}

		String[][] table = new String[entries.size() + 1][feedDataNames.size()];
		// put the values of the dimension and metric names into the table

		for (int i = 1; i < feedDataNames.size(); i++) {

			table[0][i - 1] = feedDataNames.get(i);
		}

		for (int i = 0; i < entries.size(); i++) {
			for (int j = 1; j < feedDataNames.size(); j++) {

				if (j == 1) {
					String url = entries.get(i).stringValueOf(
							feedDataNames.get(0))
							+ entries.get(i)
									.stringValueOf(feedDataNames.get(j));
					table[i + 1][0] = url;
					table[i + 1][3] = url;
				} else {
					table[i + 1][j - 1] = entries.get(i).stringValueOf(
							feedDataNames.get(j));
				}
			}

		}

		return table;
	}

}