package co.beek.pano.service.dataService.googleAnalytics;

import java.io.IOException;

import co.beek.pano.model.dao.entities.GuideReport;
import co.beek.pano.model.dao.entities.Visit;

public interface GAService {
//	public String[][] loadAllGuidesBasicData(String startDate, String endDate)
//			throws IOException, Exception;

//	public String[][] loadVisitsByWeek(String guideId, String startDate,
//			String endDate) throws IOException, Exception;
//	
//	public String[][] loadVisitsByPath(String guideId, String startDate,
//			String endDate) throws IOException, Exception;

	public GuideReport cacheGAData(GuideReport guide, String start, String end)
			throws IOException, Exception;

	public Visit getVisitData(Visit visit, String start, String end) throws Exception;
}
