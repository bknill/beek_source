package co.beek.pano.service.dataService.googleAnalytics;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import co.beek.pano.model.dao.entities.Visit;
import co.beek.pano.model.dao.googleanalytics.Event;
import co.beek.pano.model.dao.googleanalytics.VisitByUser;
import co.beek.web.guide.bean.VisitData;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.beek.pano.model.beans.APImethods;
import co.beek.pano.model.dao.entities.GuideReport;
import co.beek.util.DateUtil;

import com.google.api.services.analytics.Analytics;
import com.google.api.services.analytics.model.GaData;
import com.google.gdata.client.analytics.AnalyticsService;
import com.google.gdata.data.analytics.DataFeed;
import com.google.gson.Gson;
import com.google.gson.JsonArray;

import flexjson.JSON;

@Service
@Scope("prototype")
@Transactional
public class GAServiceImpl implements GAService {

	Analytics analytics = APImethods.initialise();
	
	private String getFilter(String id){
		
		return "ga:pagePathLevel1=~g" + id + "/";
	}

	public String loadBasicData(String guideId, String startDate,
			String endDate) throws IOException, Exception {

		String filter = getFilter( guideId );
		String metric = "ga:sessions, ga:pageviews, ga:avgTimeOnSite, ga:pageviewsPerSession";
		String dimensions = "";
		String sort = "-ga:sessions";

		
		GaData feed = APImethods.makeRequest(
				startDate, endDate, metric, dimensions, sort, filter);
		
		if (feed.getRows() == null)
			return null;
		
		 return new Gson().toJson(feed.getRows());
	}


	private String loadEventsData(String guideId, String startDate,
			String endDate) throws IOException, Exception {

		String filter = getFilter( guideId );
		String metric = "ga:totalEvents";
		String dimensions = "ga:eventCategory,ga:eventAction,ga:eventLabel";
		String sort = "-ga:totalEvents";

		GaData feed = APImethods.makeRequest(
				startDate, endDate, metric, dimensions, sort, filter);

		
		if (feed.getRows() == null)
			return null;
		

		return new Gson().toJson(feed.getRows());
	}

	private String loadLandingPageData(String guideId, String startDate,
			String endDate) throws IOException, Exception {

		String filter = getFilter( guideId );
		String metric = "ga:sessions,ga:avgTimeOnSite,ga:pageviewsPerVisit";
		String dimensions = "ga:landingPagePath";
		String sort = "-ga:sessions";

		GaData feed = APImethods.makeRequest(
				startDate, endDate, metric, dimensions, sort, filter);
		
		if (feed.getRows() == null)
			return null;
		

		return new Gson().toJson(feed.getRows());
	}

	private String loadVisitsData(String guideId, String startDate,
			String endDate) throws IOException, Exception {
	
		String filter = getFilter( guideId );
		String metric = "ga:sessions";
		String dimensions = "";
		String sort = "-ga:sessions";
		
		GaData feed = APImethods.makeRequest(
				startDate, endDate, metric, dimensions, sort, filter);
		
		
		if (feed.getRows() == null)
			return null;

		return new Gson().toJson(feed.getRows());
	}

	public String loadVisitsByMonth(String guideId, String startDate,
			String endDate) throws Exception {

		String filter = getFilter( guideId );
		String metric = "ga:sessions,ga:sessionsWithEvent,ga:avgSessionDuration,ga:pageviewsPerSession";
		String dimensions = "ga:yearMonth";
		String sort = "ga:yearMonth";
		
		GaData feed = APImethods.makeRequest(
				startDate, endDate, metric, dimensions, sort, filter);
		

		if (feed.getRows() == null)
			return null;

		return new Gson().toJson(feed.getRows());
	}

	public String loadVisitsByPath(String guideId, String startDate,
			String endDate) throws IOException, Exception {


		String filter = getFilter( guideId );
		String metric = "ga:pageviews, ga:avgTimeOnPage";
		String dimensions = "ga:pagePathLevel2";
		String sort = "-ga:pageviews";
		
		GaData feed = APImethods.makeRequest(
				startDate, endDate, metric, dimensions, sort, filter);

		if (feed.getRows() == null)
			return null;

		return new Gson().toJson(feed.getRows());
	}

	private String loadVisitsFeedData(String guideId, String startDate,
			String endDate) throws IOException, Exception {
	
		String filter = getFilter( guideId );
		String metric = "ga:sessions,ga:goal1Completions,ga:avgSessionDuration,ga:pageViewsPerVisit";
		String dimensions = "ga:week";
		String sort = "-ga:sessions";
		
		GaData feed = APImethods.makeRequest(
				startDate, endDate, metric, dimensions, sort, filter);

		if (feed.getRows() == null)
			return null;
		

		return new Gson().toJson(feed.getRows());
	}

	private String loadReferralsData(String guideId, String startDate,
			String endDate) throws IOException, Exception {
	

		String filter = getFilter( guideId );
		String metric = "ga:sessions,ga:pageviewsPerSession,ga:avgSessionDuration";
		String dimensions = "ga:source, ga:referralPath";
		String sort = "-ga:sessions";
		
		GaData feed = APImethods.makeRequest(
				startDate, endDate, metric, dimensions, sort, filter);

		if (feed.getRows() == null)
			return null;
		
		
		return new Gson().toJson(feed.getRows());
	}

	private String loadCountriesData(String guideId, String startDate,
			String endDate) throws IOException, Exception {

		String filter = getFilter( guideId );
		String metric = "ga:sessions,ga:pageviewsPerSession,ga:avgSessionDuration";
		String dimensions = "ga:country, ga:city";
		String sort = "-ga:sessions";
		
		GaData feed = APImethods.makeRequest(
				startDate, endDate, metric, dimensions, sort, filter);

		if (feed.getRows() == null)
			return null;
		

		return new Gson().toJson(feed.getRows());
	}

//	/**
//	 * removes duplicates and combines their stats. trims urls for display
//	 */
//	public String[][] fixReferralTable(String[][] table)
//
//	{
//		// calculate sum to tell if a referrer is less than x% of the total and
//		// should be put in the "Other" category
////		for (int i = 1; i < table.length; i++) {
////			sum += Integer.parseInt(table[i][1]);
////		}
//
//		HashMap<String, String[]> pastUrls = new HashMap<String, String[]>();
//
//		for (int i = 1; i < table.length; i++) {
//			// if ( Double.parseDouble(table[i][1]) / (double)sum < .05)
//			// table[i][0] = "Other";
//			if (Double.parseDouble(table[i][1]) < 5)
//				table[i][0] = "Other";
//
//			String shortUrl = table[i][0];
//			String url = table[i][0];
//
//			int count = table[i][0].split("\\/", -1).length - 1; // counts
//																	// occurences
//																	// of / in
//																	// the url
//			if (count > 1)
//				shortUrl = DateUtil.shortenURL(table[i][0]); // if there are
//																// more than 2
//			// it truncates it
//
//			String[] otherURLstats = pastUrls.get(shortUrl.toLowerCase());
//
//			if (otherURLstats == null) // not a repeat and more than 2 visits
//			{
//				String[] stats = { table[i][1], table[i][2], url };
//				pastUrls.put(shortUrl.toLowerCase(), stats); // adds to hash
//
//			}
//
//			else // is a repeat
//			{
//				// combine visits and average time
//				String visits = otherURLstats[0];
//				String avgViews = otherURLstats[1];
//
//				int totalVisits = Integer.parseInt(visits)
//						+ Integer.parseInt(table[i][1]);
//				float allAvgViews = Float.parseFloat(visits)
//						/ (float) totalVisits * Float.parseFloat(avgViews)
//						+ Float.parseFloat(table[i][1]) / (float) totalVisits
//						* Float.parseFloat(table[i][2]);
//				// update the entry in past urls
//				pastUrls.put(
//						table[i][0].toLowerCase(),
//						new String[] { Integer.toString(totalVisits),
//								Float.toString(allAvgViews), url });
//			}
//
//		}
//
//		// unpack the hash into a new table
//
//		String[] urls = pastUrls.keySet().toArray(new String[pastUrls.size()]);
//		String[][] newTable = new String[urls.length][4];
//		for (int i = 0; i < urls.length; i++) {
//			String[] columns = pastUrls.get(urls[i]);
//			newTable[i] = new String[] { urls[i], columns[0], columns[1],
//					columns[2] };
//		}
//
//		return newTable;
//	}
//
//	/**
//	 * Makes a large table containing all of the events done by converters
//	 * (people who had at least one conversion)
//	 * 
//	 * @return
//	 * @throws Exception
//	 * @throws IOException
//	 */
//	private String[][] loadConvertersEventData(String guideId,
//			String startDate, String endDate) throws IOException, Exception {
//		System.out.println("loadConvertersEventData");
//
//		// String segment = "gaid::1908247010"; // this one commented out
//		// because it was for andrew@beek.co
//		// pre defined segment of people who have converted
//		String segment = "gaid::1574668806";
//		String filter = "ga:landingPagePath=~^/g" + guideId + "/s*";
//
//		DataFeed feed = APImethods.makeRequest(as,
//				"ga:eventCategory,ga:eventAction,ga:eventLabel",
//				"ga:eventValue,ga:totalEvents", segment, filter, startDate,
//				endDate, 500);
//
//		return APImethods.getFeedTableMultiple(feed, as);
//		
//		
//		
//	}

	public GuideReport cacheGAData(GuideReport guide, String start, String end)
			throws IOException, Exception {
		String guideId = guide.getId();

		guide.setBasicData(loadBasicData(guideId, start, end));
		guide.setEventsData(loadEventsData(guideId, start, end));
		guide.setLandingPageData(loadLandingPageData(guideId, start, end));
		guide.setVisitsData(loadVisitsData(guideId, start, end));
		guide.setVisitsByWeek(loadVisitsByMonth(guideId, start, end));
		guide.setVisitsByPath(loadVisitsByPath(guideId, start, end));
		guide.setVisitsFeedData(loadVisitsFeedData(guideId, start, end));
		guide.setReferralsData(loadReferralsData(guideId, start, end));
		guide.setCountriesData(loadCountriesData(guideId, start, end));
		
		guide.setGuideSceneData(guide.getVisitsByPath());

		guide.setGaCacheTime(new Date());

		return guide;
	}




	private String getVisitsFilter(Visit visit) {

		return "ga:customVarValue1==" + visit.getKey();
	}


	public String loadVisitBasicData(Visit visit, String start, String end)  throws Exception {

		String filter = getVisitsFilter(visit);
		String metric = "ga:pageviews, ga:sessionDuration";
		String dimensions = "ga:dateHour";
		String sort = "-ga:dateHour";

		GaData feed = APImethods.makeRequest(
				start, end, metric, dimensions, sort, filter);

		if (feed.getRows() == null)
			return null;

		return new Gson().toJson(feed.getRows());

	}

	public String loadVisitEventData(Visit visit, String start, String end)  throws Exception {

		String filter = getVisitsFilter(visit);
		String metric = "ga:totalEvents";
		String dimensions = "ga:dateHour, ga:eventCategory,ga:eventAction,ga:eventLabel";
		String sort = "-ga:dateHour";

		GaData feed = APImethods.makeRequest(
				start, end, metric, dimensions, sort, filter);

		if (feed.getRows() == null)
			return null;

		return new Gson().toJson(feed.getRows());

	}

	public Visit getVisitData(Visit visit, String start, String end) throws Exception {

		visit.setVisitData(loadVisitBasicData(visit, start, end));
		visit.setEventData(loadVisitEventData(visit, start, end));

		return visit;
	}

}
