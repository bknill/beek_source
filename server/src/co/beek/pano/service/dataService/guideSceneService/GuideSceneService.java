package co.beek.pano.service.dataService.guideSceneService;

import co.beek.pano.model.dao.entities.GuideScene;
import co.beek.pano.model.dao.entities.Location;
import co.beek.pano.model.dao.entities.Scene;
import co.beek.pano.model.dao.entities.SceneDetail;
import co.beek.pano.model.dao.hotspots.BoardSign;
import co.beek.pano.model.dao.hotspots.Bubble;

import java.util.List;

public interface GuideSceneService {
	public GuideScene getGuideScene(String guideSceneId);

	public GuideScene saveGuideScene(GuideScene guideScene);

	public void deleteGuideScene(GuideScene guideScene);

}
