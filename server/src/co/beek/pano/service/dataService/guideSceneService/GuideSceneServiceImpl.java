package co.beek.pano.service.dataService.guideSceneService;

import co.beek.pano.model.dao.GuideSceneDAO;
import co.beek.pano.model.dao.SceneDAO;
import co.beek.pano.model.dao.entities.GuideScene;
import co.beek.pano.model.dao.entities.Location;
import co.beek.pano.model.dao.entities.Scene;
import co.beek.pano.model.dao.entities.SceneDetail;
import co.beek.pano.model.dao.hotspots.BoardSign;
import co.beek.pano.model.dao.hotspots.Bubble;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Service
@Scope("prototype")
@Transactional
public class GuideSceneServiceImpl implements GuideSceneService {
	@Inject
	private GuideSceneDAO guideSceneDAO;

	@Inject
	private SessionFactory sessionFactory;

	@Override
	public GuideScene getGuideScene(String sceneId) throws HibernateException {
		return guideSceneDAO.getGuideScene(sceneId);
	}

	@Override
	public GuideScene saveGuideScene(GuideScene scene) throws HibernateException {
		if (scene.getId() == null)
			return guideSceneDAO.addGuideScene(scene);

		else
			return guideSceneDAO.updateGuideScene(scene);
	}

	@Override
	public void deleteGuideScene(GuideScene scene) throws HibernateException {
		guideSceneDAO.deleteGuideScene(scene);
	}
}
