package co.beek.pano.service.dataService.guideService;

import java.util.Date;
import java.util.List;

import co.beek.pano.model.dao.entities.*;
import org.hibernate.HibernateException;

import co.beek.pano.model.dao.enterprizewizard.EWTeam;

public interface GuideService {

	public Guide getGuide(String guideId);
	
	public List<Guide> getAllGuides();

	public GuideDetail getGuideDetail(String guideId);

	public GuideReport getGuideReport(String guideId);

	public void saveGuideDetail(GuideDetail guide);

	public void saveGuide(Guide guide);

	public void saveGuideReport(GuideReport guide);

	public void deleteGuide(GuideDetail guide);

	public GuideDetail saveGuideJSON(String jsonObject);

	public List<Guide> getGuidesForDestination(String destinationId);

	public List<Guide> getGuidesForTeams(List<EWTeam> teams);

	public List<Guide> getGuidesForTeam(String teamId);
	

	public List<Guide> getGuidesContainingScenes(List<Scene> scenes);

	List<Guide> getGuidesForDestinations(List<Destination> destinations);

	public List<GuideReferrer> getGuideReferrals(String guideId)
			throws HibernateException;

	public GuideReferrer getGuideReferrer(String guideId, String path)
			throws HibernateException;

	public void saveGuideReferrer(GuideReferrer referrer)
			throws HibernateException;

	public void deleteGameTask(GameTask task);

	public List<String> getAllGuideIds();

	/**
	 * Returns the guides that are displayed in the guide browser.
	 */
	public List<Guide> getLibraryGuides();

	public List<Guide> searchGuides(String value);
	
	public List<Guide> getGuideChildren(String value);

	public List<GuideScene> getGuideScenes(String sceneId);
	
	public List<Scene> getScenesinGuide(String guideId);
	
	

	public void deleteGuideScene(GuideScene guideScene);

	public List<GuideReport> getGuidesNeedingReports(Date prevDate);
	
	public List<GuideReport> getAllGuidesReports();
	
    public List<Guide> getGuideBySubdomain(String subdomain);
	
	public GuideDetail cloneGuide(GuideDetail guide)throws CloneNotSupportedException;
}
