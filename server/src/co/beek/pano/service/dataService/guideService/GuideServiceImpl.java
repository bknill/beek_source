package co.beek.pano.service.dataService.guideService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import co.beek.pano.model.dao.entities.*;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.beek.pano.model.dao.GuideDAO;
import co.beek.pano.model.dao.enterprizewizard.EWTeam;
import flexjson.JSONDeserializer;
import flexjson.JSONException;

@Service
@Scope("prototype")
@Transactional
public class GuideServiceImpl implements GuideService {
	@Inject
	private GuideDAO guideDAO;

	@Inject
	private SessionFactory sessionFactory;

	@Override
	public Guide getGuide(String guideId) throws HibernateException {
		return guideDAO.getGuide(guideId);
	}

	public List<Guide> getAllGuides() throws HibernateException {
		return guideDAO.getAllGuides();
	}
	
	@Override
	public GuideDetail getGuideDetail(String guideId) throws HibernateException {
		return guideDAO.getGuideDetail(guideId);
	}

	@Override
	public GuideReport getGuideReport(String guideId) throws HibernateException {
		return guideDAO.getGuideReport(guideId);
	}

	@Override
	public void saveGuideDetail(GuideDetail guide) throws HibernateException {
		if (guide.getId() == null)
			guideDAO.addGuide(guide);
		else
			guideDAO.updateGuide(guide);
	}

	@Override
	public void saveGuide(Guide guide) throws HibernateException {
		guideDAO.updateGuide(guide);
	}

	@Override
	public void saveGuideReport(GuideReport guide) throws HibernateException {
		guideDAO.updateGuide(guide);
	}

	// @Override
	// public void addGuide(Guide guide) throws HibernateException {
	// guideDAO.addGuide(guide);
	// }

	// @Override
	// public void updateGuide(Guide guide) throws HibernateException {
	// guideDAO.updateGuide(guide);
	// }

	@Override
	public void deleteGuide(GuideDetail guide) throws HibernateException {
		guideDAO.deleteGuide(guide);
	}

	@Override
	public GuideDetail saveGuideJSON(String jsonObject)
			throws HibernateException, JSONException {
		GuideDetail guide = new JSONDeserializer<GuideDetail>().deserialize(
				jsonObject, GuideDetail.class);
		guideDAO.updateGuide(guide);

		return guide;
	}

	public List<Guide> getGuidesForDestination(String destinationId) {
		return guideDAO.getGuidesForDestination(destinationId);
	}

	@Override
	public List<Guide> getGuidesForDestinations(List<Destination> destinations) {
		List<Guide> guides = new ArrayList<Guide>();
		for (Destination dest : destinations)
			guides.addAll(guideDAO.getGuidesForDestination(dest.getId()));

		return guides;
	}

	public List<Guide> getGuidesForTeams(List<EWTeam> teams) {
		List<Guide> guides = new ArrayList<Guide>();
		for (EWTeam team : teams)
			guides.addAll(guideDAO.getGuidesForTeam(team.getTeams()));

		return guides;
	}

	public List<Guide> getGuidesForTeam(String teamId) {
		return guideDAO.getGuidesForTeam(teamId);
	}

	public List<Guide> getGuidesContainingScenes(List<Scene> scenes) {
		List<Guide> guides = new ArrayList<Guide>();
		List<GuideScene> guideScenes = new ArrayList<GuideScene>();
		for (Scene scene : scenes)
			guideScenes.addAll(guideDAO.getGuideScenes(scene.getId()));

		for (GuideScene guideScene : guideScenes) {
			GuideSection guideSection = guideDAO.getGuideSection(guideScene
					.getGuideSectionId());
			Guide guide = guideDAO.getGuide(guideSection.guideId);
			if (guide != null)
				guides.add(guide);
		}

		return guides;
	}

	@SuppressWarnings("unchecked")
	public List<GuideReferrer> getGuideReferrals(String guideId)
			throws HibernateException {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"FROM GuideReferrer gf WHERE gf.guideId = :guideId ");
		query.setParameter("guideId", guideId);
		return (List<GuideReferrer>) query.list();
	}

	public GuideReferrer getGuideReferrer(String guideId, String path)
			throws HibernateException {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"FROM GuideReferrer gf WHERE gf.guideId = :guideId "
						+ "AND gf.path = :path");
		query.setParameter("guideId", guideId);
		query.setParameter("path", path);
		return (GuideReferrer) query.uniqueResult();
	}

	@Override
	public void saveGuideReferrer(GuideReferrer referrer)
			throws HibernateException {
		if (referrer.getId() == null)
			sessionFactory.getCurrentSession().save(referrer);
		else
			sessionFactory.getCurrentSession().merge(referrer);
	}

	@SuppressWarnings("unchecked")
	public List<String> getAllGuideIds() {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"select g.id from Guide g");
		return (List<String>) query.list();
	}

	@SuppressWarnings("unchecked")
	public List<Guide> getLibraryGuides() {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"select g from Guide g WHERE g.status = :paidStatus");

		query.setParameter("paidStatus", Guide.STATUS_PROMOTED);
		return (List<Guide>) query.list();
	}

	@SuppressWarnings("unchecked")
	public List<Guide> searchGuides(String value) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"select g from Guide g WHERE (g.id = :idValue "
						+ "OR g.title LIKE :titleValue "
						+ "OR g.description LIKE :descriptionValue) "
						+ "AND g.status != :freeStatus");

		query.setParameter("idValue", value);
		query.setParameter("titleValue", "%" + value + "%");
		query.setParameter("descriptionValue", "%" + value + "%");
		query.setParameter("freeStatus", Guide.STATUS_FREE);

		return (List<Guide>) query.list();
	}

	@SuppressWarnings("unchecked")
	public List<GuideScene> getGuideScenes(String sceneId) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"select gs from GuideScene gs WHERE gs.sceneId = :sceneId");
		query.setParameter("sceneId", sceneId);
		return (List<GuideScene>) query.list();
	}
	
	
	 @SuppressWarnings("unchecked")
	public List<Scene> getScenesinGuide(String guideId) {
		List<GuideSection> guideSections = getGuideSections(guideId);
		List<GuideScene> guideScenes = new ArrayList<GuideScene>();
		List<Scene> scenes = new ArrayList<Scene>();
		for (GuideSection section : guideSections)
			 {
				Query query = sessionFactory.getCurrentSession().createQuery(
				"select gs from GuideScene gs WHERE gs.guideSectionId = :sectionGuideId");
				query.setParameter("sectionGuideId", section.id);
				guideScenes.addAll(query.list());

				 if(section.childGuide != null)
					 scenes.addAll(getScenesinGuide(section.childGuide.getId()));


			}
		for(GuideScene guideScene: guideScenes){
			scenes.add(guideScene.getScene());
		}
		
		return (List<Scene>) scenes;
	}
	 
		
	 @SuppressWarnings("unchecked")
	public List<Guide> getGuideChildren(String guideId) {
		 
			Query query = sessionFactory.getCurrentSession().createQuery(
					"select g from Guide g WHERE g.parent = ?");

			query.setParameter(0, guideId);
			
			return (List<Guide>) query.list();
	}

	@Override
	public void deleteGameTask(GameTask task){
		sessionFactory.getCurrentSession().delete(task);
	}
	
	
	@SuppressWarnings("unchecked")
	public List<GuideSection> getGuideSections(String guideId) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"select gs from GuideSection gs WHERE gs.guideId = :guideId");
		query.setParameter("guideId", guideId);
		return (List<GuideSection>) query.list();
	}
	
	

	public void deleteGuideScene(GuideScene guideScene) {
		sessionFactory.getCurrentSession().delete(guideScene);
	}



	@SuppressWarnings("unchecked")
	public List<GuideReport> getGuidesNeedingReports(Date prevCacheTime) {
		Query query = sessionFactory
				.getCurrentSession()
				.createQuery(
						"select g from GuideReport g WHERE g.gaCacheTime < :prevCacheTime");
		query.setParameter("prevCacheTime", prevCacheTime);
		return (List<GuideReport>) query.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<GuideReport> getAllGuidesReports() {
		Query query = sessionFactory
				.getCurrentSession()
				.createQuery(
						"select g from GuideReport g WHERE g.status > -1" );
		return (List<GuideReport>) query.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Guide> getGuideBySubdomain(String subdomain) {
		Query query = sessionFactory
				.getCurrentSession()
				.createQuery(
						"select g from Guide g WHERE g.subdomain = :subdomain");
		query.setParameter("subdomain", subdomain);
		return (List<Guide>) query.list();
	}

	public GuideDetail cloneGuide(GuideDetail guide)
			throws CloneNotSupportedException {
//		GuideDetail clone = new GuideDetail();
//		clone.setDestinationId(guide.getDestinationId());
//		clone.setTeamId(guide.getTeamId());
//		guideDAO.addGuide(clone);
		
		
		GuideDetail clone = guide.shallowClone();
		guideDAO.addGuide(clone);
		
		guide.copyCollections(clone);
		
		guideDAO.updateGuide(clone);
		return clone;
	}


}
