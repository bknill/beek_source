package co.beek.pano.service.dataService.hotspotService;

import co.beek.pano.model.dao.entities.Hotspot;
import co.beek.pano.model.dao.hotspots.Bubble;
import co.beek.pano.model.dao.hotspots.Photo;
import co.beek.pano.model.dao.hotspots.Poster;

public interface HotspotService {

	public Hotspot getHotspot(String hotspotId);

	public void saveHotspot(Hotspot hotspot);

	public Hotspot addHotspot(Hotspot hotspot);

	public void deleteHotspot(Hotspot hotspot);
}