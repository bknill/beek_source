package co.beek.pano.service.dataService.hotspotService;

import javax.inject.Inject;

import co.beek.pano.model.dao.HotspotsDAO;
import co.beek.pano.model.dao.entities.Hotspot;
import co.beek.pano.model.dao.hotspots.Bubble;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.beek.pano.model.dao.hotspots.Photo;
import co.beek.pano.model.dao.hotspots.Poster;

@Service
@Scope("prototype")
@Transactional
public class HotspotServiceImpl implements HotspotService {

	@Inject
	private SessionFactory sessionFactory;

	@Inject
	private HotspotsDAO hotspotsDAO;

	@Override
	public Hotspot getHotspot(String hotspotId) throws HibernateException {
		return hotspotsDAO.getHotspot(hotspotId);
	}

	@Override
	public void saveHotspot(Hotspot hotspot) {
		hotspotsDAO.saveHotspot(hotspot);
	}

	@Override
	 public Hotspot addHotspot(Hotspot hotspot) {
		return hotspotsDAO.addHotspot(hotspot);
	};

	@Override
	public void deleteHotspot(Hotspot hotspot) {
		hotspot.status = -1;
		saveHotspot(hotspot);
	}


}
