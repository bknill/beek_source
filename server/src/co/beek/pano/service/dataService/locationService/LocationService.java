package co.beek.pano.service.dataService.locationService;

import java.util.List;

import co.beek.pano.model.dao.enterprizewizard.EWTeam;
import co.beek.pano.model.dao.entities.Location;

public interface LocationService {
	public Location getLocation(String locationId);

	public Location getLocationForOrder(String photoShootId);

	public void saveLocation(Location location);
	
	public List<Location> getLocationsForTeam(String teamId);

	public List<Location> getLocationsForDestination(String destinationId);
	
	public List<Location> getLocationsWithin(double minLat, double minLon,
			double maxLat, double maxLon);

	public List<Location> getLocations(List<EWTeam> teams);
	
	public List<Location> getAllLocations();
}
