package co.beek.pano.service.dataService.locationService;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.hibernate.HibernateException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.beek.pano.model.dao.LocationDAO;
import co.beek.pano.model.dao.enterprizewizard.EWTeam;
import co.beek.pano.model.dao.entities.Location;

@Service
@Scope("prototype")
@Transactional
public class LocationServiceImpl implements LocationService {
	@Inject
	private LocationDAO locationDAO;

	public Location getLocation(String locationId) throws HibernateException {
		return locationDAO.getLocation(locationId);
	}


	@Override
	public Location getLocationForOrder(String photoShootId)
			throws HibernateException {
		return locationDAO.getLocationForOrder(photoShootId);
	}

	@Override
	public void saveLocation(Location location) throws HibernateException {
		if (location.getId() == null)
			locationDAO.addLocation(location);
		else
			locationDAO.updateLocation(location);
	}

	public void deleteLocation(Location location) throws HibernateException {
		locationDAO.deleteLocation(location);
	}

	@Override
	public List<Location> getLocationsWithin(double minLat, double minLon,
			double maxLat, double maxLon) throws HibernateException {
		return locationDAO.getLocations(minLat, minLon, maxLat, maxLon);
	}

	@Override
	public List<Location> getLocationsForTeam(String teamId) {
		return locationDAO.getLocations(teamId);
	}

	public List<Location> getLocations(List<EWTeam> teams) {
		List<Location> locations = new ArrayList<Location>();
		for (EWTeam team : teams)
			locations.addAll(locationDAO.getLocations(team.getTeams()));

		return locations;
	}

	@Override
	public List<Location> getLocationsForDestination(String destinationId) {
		return locationDAO.getLocationsForDestination(destinationId);
	}
	
	@Override
	public List<Location> getAllLocations() {
		return locationDAO.getAllLocations();
	}
}
