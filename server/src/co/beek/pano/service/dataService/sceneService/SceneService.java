package co.beek.pano.service.dataService.sceneService;

import co.beek.pano.model.dao.entities.Location;
import co.beek.pano.model.dao.entities.Scene;
import co.beek.pano.model.dao.entities.SceneDetail;
import co.beek.pano.model.dao.hotspots.BoardSign;
import co.beek.pano.model.dao.hotspots.Bubble;

import java.util.List;

public interface SceneService {
	public Scene getScene(String sceneId);

	public SceneDetail getSceneDetail(String sceneId);

	public Scene saveScene(Scene scene);

	public List<Scene> saveScenes(List<Scene> scene);

	public void updateSceneDetail(SceneDetail scene);

	public void deleteScene(Scene scene);

	public long countScenes(long locationId);

	public List<Scene> getScenes(int first, int pageSize, long locationId);

	public List<Scene> getScenesForLocation(String locationId);

	public List<Scene> getAllScenesForLocation(String locationId);

	public List<Scene> getAllApprovedScenesForLocation(String locationId);

	public List<Scene> getScenesWithin(double lat, double lon, int meters);

	public List<String> getAllSceneIds();
	/**
	 * Get all scenes for hte list of locations. Used in the owner dashboard for
	 * getting guides the user is in.
	 */
	public List<Scene> getScenes(List<Location> locations);
	
	
	public List<Bubble> getBubblesTargeting(Scene scene);
	public void deleteBubble(Bubble bubble);
	
	public List<BoardSign> getBoardSignsTargeting(Scene scene);
	public void deleteBoardSign(BoardSign boardSign);
	
}
