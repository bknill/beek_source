package co.beek.pano.service.dataService.sceneService;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.beek.pano.model.dao.SceneDAO;
import co.beek.pano.model.dao.entities.Location;
import co.beek.pano.model.dao.entities.Scene;
import co.beek.pano.model.dao.entities.SceneDetail;
import co.beek.pano.model.dao.hotspots.BoardSign;
import co.beek.pano.model.dao.hotspots.Bubble;

@Service
@Scope("prototype")
@Transactional
public class SceneServiceImpl implements SceneService {
	@Inject
	private SceneDAO sceneDAO;

	@Inject
	private SessionFactory sessionFactory;

	@Override
	public Scene getScene(String sceneId) throws HibernateException {
		return sceneDAO.getScene(sceneId);
	}


	@Override
	public SceneDetail getSceneDetail(String sceneId) throws HibernateException {
		return sceneDAO.getSceneDetail(sceneId);
	}


	@Override
	public Scene saveScene(Scene scene) throws HibernateException {
		if (scene.getId() == null)
			return sceneDAO.addScene(scene);

		else
			return sceneDAO.updateScene(scene);
	}

	@Override
	public List<Scene> saveScenes(List<Scene> scenes) throws HibernateException {
		for (Scene s : scenes)
			s = saveScene(s);
		return scenes;
	}

	@Override
	public void updateSceneDetail(SceneDetail scene) throws HibernateException {
		sceneDAO.updateSceneDetail(scene);
	}

	@Override
	public void deleteScene(Scene scene) throws HibernateException {
		sceneDAO.deleteScene(scene);
	}

	@Override
	public long countScenes(long locationId) throws HibernateException {
		return sceneDAO.countScenes(locationId);
	}

	@Override
	public List<Scene> getScenes(int first, int pageSize, long locationId)
			throws HibernateException {
		return sceneDAO.getScenes(first, pageSize, locationId);
	}

	@Override
	public List<Scene> getScenesForLocation(String locationId)
			throws HibernateException {
		return sceneDAO.getScenes(locationId);
	}

	@Override
	public List<Scene> getAllScenesForLocation(String locationId)
			throws HibernateException {
		return sceneDAO.getAllScenes(locationId);
	}

	@Override
	public List<Scene> getAllApprovedScenesForLocation(String locationId)
			throws HibernateException {
		return sceneDAO.getAllApprovedScenes(locationId);
	}

	@Override
	public List<Scene> getScenesWithin(double lat, double lon, int meters)
			throws HibernateException {
		double rad = ((double) meters) / 1000; // the radius of the bounding
												// circle in km
		double R = 6371; // earth's radius, km

		// first-cut bounding box (in degrees)
		double maxLat = lat + rad2deg(rad / R);
		double minLat = lat - rad2deg(rad / R);
		// compensate for degrees longitude getting smaller with increasing
		// latitude
		double maxLon = lon + rad2deg(rad / R / Math.cos(deg2rad(lat)));
		double minLon = lon - rad2deg(rad / R / Math.cos(deg2rad(lat)));

		// System.out
		// .println("getScenesWithin" + lat + ", " + lon + ", " + meters);
		// System.out.println("sceneDAO.getScenes(" + minLat + ", " + minLon +
		// ","
		// + maxLat + ", " + maxLon + ")");

		return sceneDAO.getScenes(minLat, minLon, maxLat, maxLon);
	}

	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
	/* :: This function converts decimal degrees to radians : */
	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
	private double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
	/* :: This function converts radians to decimal degrees : */
	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
	private double rad2deg(double rad) {
		return (rad * 180.0 / Math.PI);
	}

	// @Override
	// public String serializeScene(String sceneId) throws HibernateException,
	// JSONException {
	// SceneDetail scene = getSceneDetail(sceneId);
	// return serializeToJSON(scene);
	// }
	//
	// @Override
	// public String serializeScene(SceneDetail scene) throws JSONException {
	// return serializeToJSON(scene);
	// }

	// private String serializeToJSON(SceneDetail scene) throws JSONException {
	// JSONSerializer serializer = new JSONSerializer().prettyPrint(true);
	// serializer.exclude("*.class");
	// serializer.include("bubbles").exclude("bubbles.targetScene.location");
	// serializer.include("boards", "boards.boardSigns").exclude(
	// "boards.boardSigns.scene.location");
	// serializer.include("posts", "posts.postSigns");
	// // .exclude("posts.postSigns.location.defaultScene");
	// serializer.include("sounds", "sounds.file");
	//
	// serializer.include("buttons", "buttons.file");
	// serializer.include("photos", "photos.file");
	// serializer.include("posters");
	// serializer.include("videos", "videos.file");
	//
	// return serializer.serialize(scene);
	// }

	public List<Scene> getScenes(List<Location> locations) {
		List<Scene> scenes = new ArrayList<Scene>();
		for (Location location : locations)
			scenes.addAll(sceneDAO.getScenes(location.getId()));

		return scenes;
	}

	@SuppressWarnings("unchecked")
	public List<String> getAllSceneIds() {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"select s.id from SceneDetail s");
		return (List<String>) query.list();
	}

	@SuppressWarnings("unchecked")
	public List<Bubble> getBubblesTargeting(Scene scene) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"SELECT b FROM Bubble b WHERE b.targetSceneId = :sceneId");
		query.setParameter("sceneId", scene.getId());
		return (List<Bubble>) query.list();
	}

	public void deleteBubble(Bubble bubble) {
		sessionFactory.getCurrentSession().delete(bubble);
	}

	@SuppressWarnings("unchecked")
	public List<BoardSign> getBoardSignsTargeting(Scene scene) {
		Query query = sessionFactory
				.getCurrentSession()
				.createQuery(
						"select bs from BoardSign bs WHERE bs.sceneId = :sceneId");
		query.setParameter("sceneId", scene.getId());
		return (List<BoardSign>) query.list();
	}

	public void deleteBoardSign(BoardSign boardSigns) {
		sessionFactory.getCurrentSession().delete(boardSigns);
	}
}
