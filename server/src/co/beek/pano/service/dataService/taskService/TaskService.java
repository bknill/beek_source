package co.beek.pano.service.dataService.taskService;

import co.beek.pano.model.dao.entities.ScenePanoTask;

public interface TaskService {
    public ScenePanoTask getTaskFromQueue();

    public ScenePanoTask getSceneTask(String sceneId);

    public ScenePanoTask getGuideTask(String guideId);

    public void addTask(ScenePanoTask queue);

    public void updateTask(ScenePanoTask queue);

    public void deleteTask(ScenePanoTask queue);
}
