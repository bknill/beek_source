package co.beek.pano.service.dataService.taskService;

import javax.inject.Inject;

import org.hibernate.HibernateException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.beek.pano.model.dao.TaskDAO;
import co.beek.pano.model.dao.entities.ScenePanoTask;

@Service
@Scope("prototype")
@Transactional
public class TaskServiceImpl implements TaskService {
    @Inject
    private TaskDAO taskDAO;

    @Override
    public ScenePanoTask getTaskFromQueue() throws HibernateException {
        return taskDAO.getTaskFromQueue();
    }
    
    @Override
    public ScenePanoTask getSceneTask(String sceneId) throws HibernateException {
    	return taskDAO.getSceneTask(sceneId);
    }

    @Override
    public ScenePanoTask getGuideTask(String guideId) throws HibernateException {
        return taskDAO.getGuideTask(guideId);
    }

    @Override
    public void addTask(ScenePanoTask queue) throws HibernateException {
        taskDAO.addTaskToQueue(queue);
    }

    @Override
    public void updateTask(ScenePanoTask queue) throws HibernateException {
    	taskDAO.updateTask(queue);
    }

    @Override
    public void deleteTask(ScenePanoTask queue) throws HibernateException {
        taskDAO.deleteTaskFromQueue(queue);
    }
}
