package co.beek.pano.service.dataService.uploadService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.*;

@Service
@Scope("prototype")
public class LocalUploadService {

    @Value("#{buildProperties.localUploadDir}")
    private String destDir;

    @PostConstruct
    private void init() {
    }

    public boolean startService(File srcDir) {
        try {
            copyFolder(srcDir, new File(destDir + File.separator + srcDir.getName()));
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        System.out.println("local file upload done");
        return true;
    }

    private void copyFolder(File src, File dest) throws IOException {
        if (src.isDirectory()) {
            if (!dest.exists()) {
                dest.mkdirs();
            }
            String files[] = src.list();
            for (String file : files) {
                File srcFile = new File(src, file);
                File destFile = new File(dest, file);
                copyFolder(srcFile, destFile);
            }
        } else {
            InputStream in = new FileInputStream(src);
            OutputStream out = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = in.read(buffer)) > 0) {
                out.write(buffer, 0, length);
            }
            out.flush();
            in.close();
            out.close();
        }
    }
}
