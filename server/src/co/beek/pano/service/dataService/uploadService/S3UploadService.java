package co.beek.pano.service.dataService.uploadService;

import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import co.beek.Constants;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import org.apache.commons.io.IOUtils;
import org.jets3t.service.S3Service;
import org.jets3t.service.S3ServiceException;
import org.jets3t.service.ServiceException;
import org.jets3t.service.acl.AccessControlList;
import org.jets3t.service.impl.rest.httpclient.RestS3Service;
import org.jets3t.service.model.S3Object;
import org.jets3t.service.model.StorageObject;
import org.jets3t.service.multi.DownloadPackage;
import org.jets3t.service.multi.s3.S3ServiceEventAdaptor;
import org.jets3t.service.multithread.S3ServiceSimpleMulti;
import org.jets3t.service.security.AWSCredentials;
import org.jets3t.service.utils.ByteFormatter;
import org.jets3t.service.utils.MultipartUtils;
import org.jets3t.service.utils.TimeFormatter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("prototype")
public class S3UploadService {
	private static long MAX_BYTES = 1024 * 1024 * 20;

	private S3Service s3Service;
	// private File directoryContainingTileImages;
	// private List<StorageObject> objectsToUploadAsMultipart;
	private final ByteFormatter byteFormatter = new ByteFormatter();
	private final TimeFormatter timeFormatter = new TimeFormatter();

	@Value("#{buildProperties.awsAccessKey}")
	private String awsAccessKey;

	@Value("#{buildProperties.awsSecretKey}")
	private String awsSecretKey;

	@PostConstruct
	private void init() throws S3ServiceException {
		AWSCredentials awsCredentials = new AWSCredentials(awsAccessKey,
				awsSecretKey);
		s3Service = new RestS3Service(awsCredentials);
	}

	/**
	 * Scans a directory and creates a list of objects. Uploads the list of
	 * objects to the S3 bucket specified.
	 * 
	 * @param dir
	 *            The dir to be scanned for files to be uploaded
	 * @param bucketName
	 *            The bucket on S3 To recieve the file list
	 * @throws Exception
	 */
	public void uploadDir(File dir, String bucketName) throws Exception {
		System.out.println("S3UploadService.uploadDir(" + dir + ", "
				+ bucketName + ")");

		int rootPathLength = dir.getAbsolutePath().length() + 1;

		// Scan the directory and populate the oobject list
		List<StorageObject> objectList = traverseDirForItems(dir,
				new ArrayList<StorageObject>(), rootPathLength);

		System.out.println("Uploading " + objectList.size() + " items");
		// for(StorageObject object : objectList)
		// System.out.println("	"+object.getKey());

		// Upload the object list using MultipartUtils
		MultipartUtils mpUtils = new MultipartUtils(MAX_BYTES);
		mpUtils.uploadObjects(bucketName, s3Service, objectList,
				new S3ServiceEventAdaptor() {
				});

		// objectsToUploadAsMultipart.clear();
		System.out.println("s3 file upload done");
	}

	private List<StorageObject> traverseDirForItems(File file,
			List<StorageObject> objectList, int rootPathLength)
			throws NoSuchAlgorithmException, IOException {
		for (File r : file.listFiles()) {
			if (r.isDirectory()) {
				objectList = traverseDirForItems(r, objectList, rootPathLength);
			} else {
				S3Object s3Object = new S3Object(new File(r.getAbsolutePath()));

				String key = r.getAbsolutePath().substring(rootPathLength);

				// Create url friendly forward slashes.
				// mimics folders on s3.
				key = key.replace('\\', '/');
				// System.out.println("s3 key - " + key);

				s3Object.setKey(key);
				s3Object.setAcl(AccessControlList.REST_CANNED_PUBLIC_READ);

				objectList.add(s3Object);
			}
		}

		return objectList;
	}

	public boolean uploadFile(String bucketName, File file)
			throws NoSuchAlgorithmException, IOException, S3ServiceException {
		S3Object s3Object = new S3Object(file);
		s3Object.setKey(file.getName());
		s3Object.setAcl(AccessControlList.REST_CANNED_PUBLIC_READ);

		s3Service.putObject(bucketName, s3Object);
		file.delete();
		System.out.println("s3 file upload done: " + file.getName() + " sze:"
				+ file.getTotalSpace());
		return true;
	}

	public File downloadFile(String bucketName,String filename)
	{

		File panoFile = new File(Constants.TEMP_DIR_PATH + filename);
		System.out.println("downloading pano from s3");
		try {
			S3Object objectComplete = s3Service.getObject(bucketName, filename);
			FileOutputStream fileOutputStream =  new FileOutputStream(panoFile);
			IOUtils.copy(objectComplete.getDataInputStream(),fileOutputStream);
			fileOutputStream.close();
			objectComplete.closeDataInputStream();
			return panoFile;
		} catch (S3ServiceException e) {
			e.printStackTrace();
		} catch (ServiceException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

}
