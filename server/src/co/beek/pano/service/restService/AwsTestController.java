package co.beek.pano.service.restService;


import com.amazonaws.util.EC2MetadataUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AwsTestController {
    @ResponseBody
    @RequestMapping(value = "/ip", method = RequestMethod.GET)
    public String getPublicIP() {
        try {
            return EC2MetadataUtils.getData("/latest/meta-data/public-ipv4");
        } catch (Exception e) {
            e.printStackTrace();
            return "error";
        }
    }
}
