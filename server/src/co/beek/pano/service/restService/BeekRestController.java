package co.beek.pano.service.restService;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.service.dataService.MailService;
import co.beek.util.BeekSession;
import flexjson.JSONSerializer;

public class BeekRestController {

	JSONSerializer serializer = new JSONSerializer().prettyPrint(true);

	@Inject
	private MailService mailService;

	public BeekRestController() {
		serializer.exclude("*.class");
	}

	protected BeekSession getSession(HttpServletRequest request) {
		return new BeekSession(request.getSession());
	}

	protected EWContact getUser(HttpServletRequest request) {
		return getSession(request).getUser();
	}

	protected boolean isLoggedIn(HttpServletRequest request) {
		return getUser(request) != null;
	}

	protected boolean isSuperUser(HttpServletRequest request) {
		return getUser(request).id.equals(EWContact.SUPER_USER_ID);
	}

	protected ResponseEntity<String> getPayloadResponse(Object payload) {
		ServiceResponse response = new ServiceResponse();
		response.setPayload(payload);

		return getResponse(response);
	}

	protected ResponseEntity<String> getPayloadResponse(Serializable payload) {

		ServiceResponse response = new ServiceResponse();
		response.setPayload(payload);

		return getResponse(response);
	}

	protected ResponseEntity<String> getNodesResponse(List nodes, List edges) {

		ServiceResponse response = new ServiceResponse();
		response.setNodes(nodes);
		response.setEdges(edges);

		return getResponse(response);
	}

	protected ResponseEntity<String> getSuccessResponse() {
		return getResponse(new ServiceResponse());
	}

	protected ResponseEntity<String> getErrorResponse(Throwable e) {
		String message = ExceptionUtils.getStackTrace(e);
		return getErrorResponse(123, message);
	}

	protected ResponseEntity<String> getErrorResponse(int error, String message) {
		System.out.println("Error: " + (error) + " : " + message);
		ServiceResponse response = new ServiceResponse();
		response.setError(error);
		response.setMessage(message);

		return getResponse(response);
	}

	protected ResponseEntity<String> getResponse(ServiceResponse response) {

		String sessionJson = serializer.exclude("*.class").deepSerialize(response);
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "application/json; charset=utf-8");
		return new ResponseEntity<String>(sessionJson, responseHeaders,
				HttpStatus.OK);
	}

	protected void sendErrorDan(String subject, Throwable e) {
		e.printStackTrace();
		String message = ExceptionUtils.getStackTrace(e);
		mailService.emailError(subject, message);
	}
}
