package co.beek.pano.service.restService;

import java.io.*;
import java.net.*;

import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest;

import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.entities.FileData;
import co.beek.pano.service.dataService.fileService.FileService;
import flexjson.JSONSerializer;

@Controller
public class FileRestController extends BeekRestController {
	@Inject
	private FileService fileService;

	@ResponseBody
	@RequestMapping(value = "/files/public/", method = RequestMethod.GET)
	public ResponseEntity<String> getFiles(HttpServletRequest request) {
		if (!isLoggedIn(request))
			return getErrorResponse(123456, "Not Logged in");

		List<FileData> files = fileService.getPublicFiles();

		return getPayloadResponse(files);
	}

	// Error #2032: Stream Error. URL: http://gms.beekdev.co
	// files/uploadBytes/CUSTOMER_75_/transitionvideo.flv/;jsessionid=wltzrl6qrch21nbw8tfxdidos
	@ResponseBody
	@RequestMapping(value = "/files/uploadBytes/{fileName}/", method = RequestMethod.POST)
	public ResponseEntity<String> uploadBytes(HttpServletRequest request,
			@PathVariable("fileName") String fileName) {
	

		if (!isLoggedIn(request))
			return getErrorResponse(123456, "not logged in");

		// check permissions on this user uploading for this team
		/*if (!isSuperUser(request) && getUser(request).isInTeam(teamId) == false)
			return getErrorResponse(123456,
					"does not have permission to upload for team:" + teamId);*/

		try {
			FileData file = fileService.uploadBytes(fileName,
					request.getInputStream());
			return getPayloadResponse(file);

		} catch (Exception e) {
			return getErrorResponse(e);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/files/uploadMultipart/", method = RequestMethod.POST)
	public ResponseEntity<String> uploadMultipart(
			DefaultMultipartHttpServletRequest request) {

		HttpHeaders responseHeaders = new HttpHeaders();
		EWContact contact = getUser(request);

		if (!isLoggedIn(request))
			return getErrorResponse(123456, "not logged in");

		// check permissions on this user uploading for this team
	/*	if (!isSuperUser(request) && contact.hasLocation()
			return getErrorResponse(123456,
					"does not have permission to upload for team:" + teamId);*/

		MultipartFile upload = request.getFile("file");
		if (upload == null)
			return getErrorResponse(123456, "no upload found");

		try {

			FileData file = fileService.uploadFile(upload);

			JSONSerializer serializer = new JSONSerializer().prettyPrint(true);
			serializer.exclude("*.class");
			responseHeaders.add("Content-Type",
					"application/json; charset=utf-8");
			return new ResponseEntity<String>(serializer.serialize(file),
					responseHeaders, HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("upload failed", responseHeaders,
					HttpStatus.OK);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/cacheYouTubeImage/{videoId}", method = RequestMethod.GET)
	public ResponseEntity<String> cacheYouTubeImage(HttpServletRequest request,
			@PathVariable("videoId") String videoId) {

		EWContact contact = getUser(request);

		if (!isLoggedIn(request))
			return getErrorResponse(123456, "not logged in");

		// check permissions on this user uploading for this team
		/*if (!isSuperUser(request) && contact.isInTeam(teamId) == false)
			return getErrorResponse(123456,
					"does not have permission to upload for team:" + teamId);*/

		try {
			String url = "http://img.youtube.com/vi/" + videoId
					+ "/hqdefault.jpg";
			FileData file = fileService.cacheFile( url, videoId);
			return getPayloadResponse(file);

		} catch (Exception e) {
			return getErrorResponse(e);
		}
	}

	@ResponseBody
		 @RequestMapping(value = "/proxy/{url:.+}", method = RequestMethod.GET)
		 public void proxyUrl(HttpServletRequest request,
							  HttpServletResponse response,@PathVariable("url") String u) throws Exception {

		//String u = request.getParameter("u");

		String ue = URLEncoder.encode(u, "UTF-8");
		URL url = new URL("http://cdn.beek.co/" + ue);
		URLConnection con = url.openConnection();

		InputStream input = con.getInputStream();
		OutputStream output = response.getOutputStream();
		response.setContentLength(con.getContentLength());
		byte[] buffer = new byte[10240];

		try {
			for (int length = 0; (length = input.read(buffer)) > 0;) {
				output.write(buffer, 0, length);
			}
		} finally {
			try {
				output.close();
			} catch (IOException ignore) {
			}
			try {
				input.close();
			} catch (IOException ignore) {
			}
		}

	}

	@ResponseBody
	@RequestMapping(value = "/file/{file:.+}", method = RequestMethod.GET)
	public void doGet(HttpServletRequest request, HttpServletResponse response,@PathVariable("file") String f) throws Exception {

	String filename = URLDecoder.decode(f, "UTF-8");
		File file = new File("resources/files/", filename);
		response.setHeader("Content-Type", "video/mp4");
		response.setHeader("Content-Length", String.valueOf(file.length()));
		response.setHeader("Content-Disposition", "inline; filename=\"" + file.getName() + "\"");

	}


	
	@ResponseBody
	@RequestMapping(value = "/files/uploadVideo/{fileName}/", method = RequestMethod.POST)
	public ResponseEntity<String> uploadVideo(
			DefaultMultipartHttpServletRequest request,
			@PathVariable("fileName") String fileName) {

		HttpHeaders responseHeaders = new HttpHeaders();
		EWContact contact = getUser(request);

		if (!isLoggedIn(request))
			return getErrorResponse(123456, "not logged in");


		MultipartFile upload = request.getFile("file");
		if (upload == null)
			return getErrorResponse(123456, "no upload found");

		try {

			FileData file = fileService.uploadFile(upload);

			JSONSerializer serializer = new JSONSerializer().prettyPrint(true);
			serializer.exclude("*.class");
			responseHeaders.add("Content-Type",
					"application/json; charset=utf-8");
			return new ResponseEntity<String>(serializer.serialize(file),
					responseHeaders, HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("upload failed", responseHeaders,
					HttpStatus.OK);
		}
	}

	
	// @ResponseBody
	// @RequestMapping(value = "/photo/{photoId}/thumb", method =
	// RequestMethod.POST)
	// public ResponseEntity<String> savePhotoThumb(
	// DefaultMultipartHttpServletRequest request,
	// @PathVariable("photoId") String photoId,
	// @RequestParam("thumb") MultipartFile thumb) {
	//
	// if (!isLoggedIn(request))
	// return getErrorResponse(123456, "not logged in");
	//
	// try {
	// File thumbFile = ImageUtil.getTempFile(thumb);
	// fileService.uploadFile(thumbFile);
	// return getSuccessResponse();
	//
	// } catch (Exception e) {
	// return getErrorResponse(e);
	// }
	// }

}
