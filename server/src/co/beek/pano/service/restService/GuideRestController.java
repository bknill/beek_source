package co.beek.pano.service.restService;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.beek.pano.model.dao.entities.*;
import co.beek.pano.service.dataService.GameTaskService.GameTaskService;
import co.beek.pano.service.dataService.guideSceneService.GuideSceneService;
import co.beek.pano.service.dataService.sceneService.SceneService;
import co.beek.util.CacheManifestBuilder;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import flexjson.JSONSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import co.beek.Constants;
import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.enterprizewizard.EWCorrespondenceResponse;
import co.beek.pano.service.dataService.MailService;
import co.beek.pano.service.dataService.destinationService.DestinationService;
import co.beek.pano.service.dataService.enterprizewizardService.EWService;
import co.beek.pano.service.dataService.guideService.GuideService;
import co.beek.pano.service.dataService.locationService.LocationService;
import co.beek.pano.service.dataService.uploadService.S3UploadService;
import co.beek.pano.util.ImageUtil;
import co.beek.util.BeekSession;
import co.beek.util.CacheManifestBuilder.Cache;
import co.beek.util.CacheManifestBuilder.Network;

import flexjson.JSONDeserializer;
import flexjson.JSONException;

@Controller
public class GuideRestController extends BeekRestController {
	@Inject
	private GuideService guideService;

	@Inject
	private GuideSceneService guideSceneService;

	@Inject
	private SceneService sceneService;

	@Inject
    private GameTaskService gameTaskService;

	@Inject
	private LocationService locationService;

	@Inject
	private EWService ewService;

	@Inject
	private DestinationService destinationService;

	@Inject
	private S3UploadService s3UploadService;
	@Value("#{buildProperties.assetsBucket}")
	private String assetsBucket;

	@Value("#{buildProperties.cdnUrl}")
	private String cdnUrl;

	public static final String HIGH = "hi";
	public static final String LOW = "low";

	@ResponseBody
	@RequestMapping(value = "/crossdomain.xml", method = RequestMethod.GET)
	public String getCrossdomain(HttpServletResponse response) {
		response.setContentType("application/xml");
		return "<?xml version=\"1.0\"?>"
				+ "<!DOCTYPE cross-domain-policy SYSTEM \"http://www.macromedia.com/xml/dtds/cross-domain-policy.dtd\">"
				+ "<cross-domain-policy>"
				+ "<allow-access-from domain=\"*\" to-ports=\"*\" />"
				+ "<site-control permitted-cross-domain-policies=\"all\"/>"
				+ "</cross-domain-policy> ";
	}

	@ResponseBody
	@RequestMapping(value = "/guide/{guideId}/json", method = RequestMethod.GET)
	public ResponseEntity<String> getGuideJson(
			@PathVariable("guideId") String guideId) {
		try {
			GuideDetail guide = guideService.getGuideDetail(guideId);
			guide.updateGuideSections();
			return getPayloadResponse(guide);
		} catch (JSONException e) {
			return getErrorResponse(1234, "Error encoding guide: "
					+ e.getCause().toString());
		}
	}

	@ResponseBody
	@RequestMapping(value = "/guide/{guideId}/files", method = RequestMethod.GET)
	public ResponseEntity<String> getGuideFiles(
			@PathVariable("guideId") String guideId) {
		try {
			List<String> files = getGuideMediaFiles(guideId, HIGH);
			return getPayloadResponse(files);
		} catch (JSONException e) {
			return getErrorResponse(1234, "Error encoding files: "
					+ e.getCause().toString());
		}
	}

	@ResponseBody
	@RequestMapping(value = "/guide/{guideId}/files/hi/", method = RequestMethod.GET)
	public ResponseEntity<String> getGuideFilesHiRes(
			@PathVariable("guideId") String guideId) {
		try {
			List<String> files = getGuideMediaFiles(guideId,HIGH);
			return getPayloadResponse(files);
		} catch (JSONException e) {
			return getErrorResponse(1234, "Error encoding files: "
					+ e.getCause().toString());
		}
	}

	@ResponseBody
	@RequestMapping(value = "/guide/{guideId}/files/low/", method = RequestMethod.GET)
	public ResponseEntity<String> getGuideFilesLowRes(
			@PathVariable("guideId") String guideId) {
		try {
			List<String> files = getGuideMediaFiles(guideId,LOW);
			return getPayloadResponse(files);
		} catch (JSONException e) {
			return getErrorResponse(1234, "Error encoding files: "
					+ e.getCause().toString());
		}
	}

	@ResponseBody
	@RequestMapping(value = "/guide/{guideId}/filesJsonP", method = RequestMethod.GET)
	public ResponseEntity<String> getGuideFilesJsonP(
			@PathVariable("guideId") String guideId,
			@RequestParam("callback") String callback) {
		try {
			List<String> files = getGuideMediaFiles(guideId,LOW);
			String sessionJson = callback + "("
					+ serializer.deepSerialize(files) + ");";
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type",
					"application/javascript; charset=utf-8");
			return new ResponseEntity<String>(sessionJson, responseHeaders,
					HttpStatus.OK);
		} catch (JSONException e) {
			return getErrorResponse(1234, "Error encoding files: "
					+ e.getCause().toString());
		}
	}

	@ResponseBody
	@RequestMapping(value = "/guide/{guideId}/filesJsonPLowRes", method = RequestMethod.GET)
	public ResponseEntity<String> getGuideFilesJsonPLowRes(
			@PathVariable("guideId") String guideId,
			@RequestParam("callback") String callback) {
		try {
			List<String> files = getGuideMediaFiles(guideId,LOW);
			String sessionJson = callback + "("
					+ serializer.deepSerialize(files) + ");";
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type",
					"application/javascript; charset=utf-8");
			return new ResponseEntity<String>(sessionJson, responseHeaders,
					HttpStatus.OK);
		} catch (JSONException e) {
			return getErrorResponse(1234, "Error encoding files: "
					+ e.getCause().toString());
		}
	}

	@ResponseBody
	@RequestMapping(value = "/guide/{guideId}/cache.manifest", method = RequestMethod.GET)
	public String getGuideManifest(
			@PathVariable("guideId") String guideId,HttpServletResponse response) {
		response.setContentType("text/cache-manifest");

		CacheManifestBuilder builder = CacheManifestBuilder.newInstance();

		try {
			File manifestFile = new File("resources/manifest/beek.manifest");
			String expectedManifest = builder.readFile(manifestFile);
			builder.withVersion("2010-06-18:v2");
			Cache cache = builder.getCache();
			Network network = builder.getNetwork();
			network.add(cdnUrl);
			network.add("http://gms.beek.co");

			for(String file : getGuideMediaFiles(guideId,HIGH))
				cache.add(cdnUrl + "/" +file);

			return builder.toRawString();

		} catch (Exception e) {
			e.printStackTrace();

			return null;
		}

	}

	private List<String> getGuideMediaFiles(String guideId, String res){

		GuideDetail guide = guideService.getGuideDetail(guideId);

		List<String> files = new ArrayList<String>();

		if(guide.soundtrack != null)
			files.add(guide.soundtrack);

		for(GuideScene guideScene : guide.returnGuideScenes()){
			SceneDetail scene = sceneService.getSceneDetail(guideScene.getSceneId());

			 VideoSettings videoSettings = null;

			//stop using guideScene for now, because of orientation problem with hotspots
/*				if(guideScene.getVideo() != null){
					videoSettings =  new JSONDeserializer<VideoSettings>().use(null, VideoSettings.class).deserialize(guideScene.getVideo());
				}
				else */
				if (scene.getOutputVideo() != null){
					videoSettings = new JSONDeserializer<VideoSettings>().use(null, VideoSettings.class).deserialize(scene.getOutputVideo());
				}

				else if(scene.getVideo() != null){
					videoSettings = new JSONDeserializer<VideoSettings>().use(null, VideoSettings.class).deserialize(scene.getVideo());
				}

				else
					files.add(scene.getPanoName());


				if(videoSettings != null){
					if(videoSettings.getLowRes() != null && res == LOW)
					{
						files.add(videoSettings.getLowRes());
					}
					else if(videoSettings.getHiRes() != null && res == HIGH){
						files.add(videoSettings.getHiRes());
					}
					else{
						files.add(videoSettings.getFilename());
					}

				}

			if(guideScene.getVoiceovers() != null){
				for(String voiceover : guideScene.getVoiceovers())
					files.add(voiceover);
			}
			else if(scene.getVoiceover() != null && scene.getOutputVideo() == null){
				Voiceover voiceover = new Gson().fromJson(scene.getVoiceover(),Voiceover.class);
				if(voiceover != null)
				files.add(voiceover.getFilename());
			}

			for(Hotspot hotspot : scene.getHotspots()){
				if(hotspot.media != null){
					HotspotMedia media = new Gson().fromJson(hotspot.media,HotspotMedia.class);

					if(media.getFiles() != null)
						for(HotspotMediaFile file : media.getFiles()){
							files.add(file.getName());
						}
				}
			}
		}



		if(guide.getGameTasks() != null){

			List<GameTask> list = guide.getGameTasks();
			Collections.sort(list,GameTask.orderComparator);

			for(GameTask task : list){
				if(task.getGameTaskInput().getVoiceFile() != null){

					if(list.get(0) != task)
						files.add(task.getGameTaskInput().getVoiceFile());
					else
						files.add(0,task.getGameTaskInput().getVoiceFile());
				}

				if(task.getGameTaskOutput().getRightAnswerVoiceFile() != null)
					files.add(task.getGameTaskOutput().getRightAnswerVoiceFile());

				if(task.getGameTaskOutput().getWrongAnswerVoiceFile() != null)
					files.add(task.getGameTaskOutput().getWrongAnswerVoiceFile());
			}
		}

		return files;
	}
	
	@ResponseBody
	@RequestMapping(value = "/guide/{guideId}/scenes", method = RequestMethod.GET)
	public ResponseEntity<String> getScenesinGuide(
			@PathVariable("guideId") String guideId) {
		try {
			List<Scene> guideScenes = guideService.getScenesinGuide(guideId);
			return getPayloadResponse(guideScenes);
		} catch (JSONException e) {
			return getErrorResponse(1234, "Error getting guide Scenes: "
					+ e.getCause().toString());
		}
	}
	

	@ResponseBody
	@RequestMapping(value = "/guide/{guideId}/jsonp", method = RequestMethod.GET)
	public ResponseEntity<String> getGuideJsonP(
			@PathVariable("guideId") String guideId,
			@RequestParam("callback") String callback) {
		try {
			GuideDetail guide = guideService.getGuideDetail(guideId);

			guide.updateGuideSections();

			String sessionJson = callback + "("
					+ serializer.deepSerialize(guide) + ");";
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type",
					"application/javascript; charset=utf-8");
			return new ResponseEntity<String>(sessionJson, responseHeaders,
					HttpStatus.OK);
		} catch (JSONException e) {
			return getErrorResponse(1234, "Error encoding guide: "
					+ e.getCause().toString());
		}
	}

	@ResponseBody
	@RequestMapping(value = "/guide/{guideId}/jsonbasic", method = RequestMethod.GET)
	public ResponseEntity<String> getGuideBasicJson(
			@PathVariable("guideId") String guideId) {
		try {
			Guide guide = guideService.getGuide(guideId);
			return getPayloadResponse(guide);
		} catch (JSONException e) {
			return getErrorResponse(1234, "Error encoding guide: "
					+ e.getCause().toString());
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/guide/{guideId}/children", method = RequestMethod.GET)
	public ResponseEntity<String> getGuideChildren(
			@PathVariable("guideId") String guideId) {
		try {
			List<Guide> guides = guideService.getGuideChildren(guideId);
				return getPayloadResponse(guides);
		} catch (JSONException e) {
			return getErrorResponse(1234, "Error getting guides: "
					+ e.getCause().toString());
		}
	}

	// HttpServletRequest request,
	@ResponseBody
	@RequestMapping(value = "/guide/{guideId}/save", method = RequestMethod.POST)
	public ResponseEntity<String> saveGuide(HttpServletRequest request,
			@PathVariable("guideId") String guideId,
			@RequestParam("json") String json) {

		// Get the user object from the session
		BeekSession session = getSession(request);
		EWContact contact = session.getUser();

		// Check that we are logged in
		if (contact == null)
			return getErrorResponse(1234, "not logged in");

		GuideDetail guide = guideService.getGuideDetail(guideId);

		// if the logged in user is part of the guide team
		if (!guide.hasWritePermission(contact))
			return getErrorResponse(1234, "does not have permissions");

		// List<GuideLocation> prevGuideLocations = guide.getGuideLocations();

		guide = new JSONDeserializer<GuideDetail>()
				.deserializeInto(json, guide);
		guide.incrementSave();

		// Check if the user has added any new location links
		// checkNewLocationLinks(session, prevGuideLocations, guide);

		// MultipartFile thumb = request.getFile("thumb");
		/*
		 * if (thumb != null && thumb.getSize() > 0) { try { // We have a thumb
		 * to save, increment now to create unique name guide.incrementThumb();
		 * 
		 * // Create and populate the thumb with the new bitmap File file = new
		 * File(Constants.TEMP_DIR_PATH + guide.getThumbName());
		 * thumb.transferTo(file);
		 * 
		 * // upload thumb to the scenes bucket
		 * s3UploadService.uploadFile(assetsBucket, file); } catch (Exception e)
		 * { return getErrorResponse(e); }
		 * 
		 * // http://crm.beek.co/ewws/EWCreate/.json?$KB=beekorderly&$login=gms&
		 * $password
		 * =B33kb33k&$lang=en&$table=guides&guide_name=Wellington+guide&
		 * guideid=66
		 * &customer_token=:CUSTOMER_123_&destination=Wellington&status=free }
		 */

		try {
			guideService.saveGuideDetail(guide);

			if (!session.getUser().isSuperUser())
				ewService.updateGuide(session.getUsername(), session
						.getPassword(), guide, destinationService
						.getDestination(guide.getDestinationId()));
		} catch (Exception e) {
			return getErrorResponse(e);
		}

		return getSuccessResponse();
	}

	@ResponseBody
	@RequestMapping(value = "/guide/{id}/thumb", method = RequestMethod.POST)
	public ResponseEntity<String> saveGuideThumb(HttpServletRequest request,
			@PathVariable("id") String guideId,
			@RequestParam("thumbName") String thumbName,
			@RequestParam("thumb") MultipartFile thumb) {
		try {
			File file = new File(Constants.TEMP_DIR_PATH + thumbName);
			thumb.transferTo(file);
			// upload thumb to the scenes bucket
			s3UploadService.uploadFile(assetsBucket, file);
			return getSuccessResponse();
		} catch (Exception e) {
			sendErrorDan("Error Uploading Thumb", e);
			return getErrorResponse(2354, "Error uploading thumb");
		}
	}

	
	@ResponseBody
	@RequestMapping(value = "/guideSection/{id}/image", method = RequestMethod.POST)
	public ResponseEntity<String> saveGuideSectionImage(HttpServletRequest request,
			@PathVariable("id") String sectionId,
			@RequestParam("pageImageName") String imageName,
			@RequestParam("pageImage") MultipartFile image) {
		try {
			File file = new File(Constants.TEMP_DIR_PATH + imageName);
			image.transferTo(file);
			// upload thumb to the scenes bucket
			s3UploadService.uploadFile(assetsBucket, file);
			return getSuccessResponse();
		} catch (Exception e) {
			sendErrorDan("Error Uploading Thumb", e);
			return getErrorResponse(2354, "Error uploading thumb");
		}
	}



	// @ResponseBody
	// @RequestMapping(value = "/guide/{guideId}/saveZip", method =
	// RequestMethod.POST)
	// public ResponseEntity<String> saveGuideZip(HttpServletRequest request,
	// @PathVariable("guideId") String guideId) {
	// try {
	// Guide guide = guideService.getGuide(guideId);
	// guide.zipIncrement++;
	//
	// // Create the temp file for storing the upload
	// File tempFile = new File(Constants.TEMP_DIR_NAME + "/"
	// + guide.getZipName());
	//
	// // Copy the uploaded bytes to the temp file
	// ImageUtil.copyTo(request.getInputStream(), tempFile);
	//
	// guide.zipBytes = (int) tempFile.length();
	//
	// // upload zip to s3
	// s3UploadService.uploadFile(assetsBucket, tempFile);
	//
	// // Save the guide with the new zip increment
	// guideService.saveGuide(guide);
	//
	// return getPayloadResponse(guide);
	// } catch (Exception e) {
	// return getErrorResponse(e);
	// }
	// }

	@ResponseBody
	@RequestMapping(value = "/guides/share/{guideId}", method = RequestMethod.POST)
	public ResponseEntity<String> shareViaEmail(
			@PathVariable("guideId") String guideId,
			@RequestParam("emailAddress") String emailAddress,
			@RequestParam("emailContent") String emailContent,
			@RequestParam("guideUrl") String guideUrl) {
		try {
			EWCorrespondenceResponse response = ewService.sendEmail(
					EWContact.ADMIN_USER_USERNAME,
					EWContact.ADMIN_USER_PASSWORD, null, guideId,
					"ShareViaEmailTemplate", emailContent, guideUrl,
					emailAddress);

			if (!response.getSuccess())
				throw new Exception("Could not send email:"
						+ response.getErrorMessage());

			return getSuccessResponse();
		} catch (Exception e) {
			return getErrorResponse(e);
		}
	}

	/**
	 * Request to link my team to this guide;
	 */
	@ResponseBody
	@RequestMapping(value = "/guide/{myGuideId}/linked/{yourLocationId}/guide/{yourGuideId}/linkrequest/{myLocationId}", method = RequestMethod.POST)
	public ResponseEntity<String> sendLinkRequest(HttpServletRequest request,
			@PathVariable("myGuideId") String myGuideId,
			@PathVariable("yourLocationId") String yourLocationId,
			@PathVariable("yourGuideId") String yourGuideId,
			@PathVariable("myLocationId") String myLocationId) {
		try {
			Guide myGuide = guideService.getGuide(myGuideId);
			Location yourLocation = locationService.getLocation(yourLocationId);

			Guide yourGuide = guideService.getGuide(yourGuideId);
			Location myLocation = locationService.getLocation(myLocationId);

			BeekSession session = getSession(request);
			EWContact user = session.getUser();

			String message = user.first_name + " " + user.last_name
					+ " has added '" + yourLocation.getTitle()
					+ "' to their guide '" + myGuide.getTitle() + ". ";

			message += "They have requested that you add their location '"
					+ myLocation.getTitle() + "' to your guide '"
					+ yourGuide.getTitle() + "'.";

			String path = "/guide/{guideId}/link/{locationId}";
			path = path.replace("{guideId}", yourGuideId);
			path = path.replace("{locationId}", myLocationId);

			String guideLinkUrl = "http://gms." + Constants.domain + path;

			EWCorrespondenceResponse response = ewService.sendEmail(
					session.getUsername(), session.getPassword(),
					yourGuide.getTeamId(), yourGuide.getId(),
					"GuideLinkRequest", message, guideLinkUrl, null);
			if (response.getSuccess())
				return getSuccessResponse();

			else
				return getErrorResponse(1234, response.getErrorMessage());

		} catch (IOException e) {
			return getErrorResponse(1324, "asdf");
		}
	}

	@ResponseBody
	@RequestMapping(value = "/guide/{guideId}/clone", method = RequestMethod.GET)
	public ResponseEntity<String> cloneGuide(HttpServletRequest request,
			@PathVariable("guideId") String guideId)
			throws CloneNotSupportedException {

		BeekSession session = getSession(request);
		EWContact contact = session.getUser();

		// Check that we are logged in
		if (contact == null)
			return getErrorResponse(1234, "not logged in");

		GuideDetail guide = guideService.getGuideDetail(guideId);
		GuideDetail clone = guideService.cloneGuide(guide);

		return getPayloadResponse(clone);
	}

	@ResponseBody
	@RequestMapping(value = "/guides/library/", method = RequestMethod.GET)
	public ResponseEntity<String> getLibraryGuides() {
		try {
			List<Guide> guides = guideService.getLibraryGuides();
			return getPayloadResponse(guides);
		} catch (Exception e) {
			return getErrorResponse(123456, e.getMessage());
		}
	}

	@ResponseBody
	@RequestMapping(value = "/guides/all/", method = RequestMethod.GET)
	public ResponseEntity<String> getAllGuides() {
		try {
			List<Guide> guides = guideService.getAllGuides();
			Collections.reverse(guides);
			return getPayloadResponse(guides);
		} catch (Exception e) {
			return getErrorResponse(123456, e.getMessage());
		}
	}

	@ResponseBody
	@RequestMapping(value = "/guides/all/jsonp", method = RequestMethod.GET)
	public ResponseEntity<String> getAllGuidesJsonP(@RequestParam("callback") String callback) {
		try {
			List<Guide> guides = guideService.getAllGuides();
			Collections.reverse(guides);

			String sessionJson = callback + "("
					+ serializer.deepSerialize(guides) + ");";
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type",
					"application/javascript; charset=utf-8");
			return new ResponseEntity<String>(sessionJson, responseHeaders,
					HttpStatus.OK);
		} catch (JSONException e) {
			return getErrorResponse(1234, "Error encoding guides: "
					+ e.getCause().toString());
		}
	}

	@ResponseBody
	@RequestMapping(value = "/guides/library.jsonp", method = RequestMethod.GET)
	public ResponseEntity<String> getLibraryGuidesJsonP(@RequestParam("callback") String callback) {
		try {
			List<Guide> guides = guideService.getLibraryGuides();
			Collections.reverse(guides);
			String sessionJson = callback + "("
					+ serializer.deepSerialize(guides) + ");";
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type",
					"application/javascript; charset=utf-8");
			return new ResponseEntity<String>(sessionJson, responseHeaders,
					HttpStatus.OK);
		} catch (JSONException e) {
			return getErrorResponse(1234, "Error encoding guide: "
					+ e.getCause().toString());
		}
	}


	@ResponseBody
	@RequestMapping(value = "/guides/search/{value}/", method = RequestMethod.GET)
	public ResponseEntity<String> searchGuides(
			@PathVariable("value") String value) {
		try {
			List<Guide> guides = guideService.searchGuides(value);
			return getPayloadResponse(guides);
		} catch (Exception e) {
			return getErrorResponse(123456, e.getMessage());
		}
	}

	@ResponseBody
	@RequestMapping(value = "/destinations/", method = RequestMethod.GET)
	public ResponseEntity<String> getDestinations() {
		try {
			List<Destination> destinations = destinationService
					.getAllDestinations();
			return getPayloadResponse(destinations);
		} catch (Exception e) {
			return getErrorResponse(123456, e.getMessage());
		}
	}
}
