package co.beek.pano.service.restService;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import co.beek.pano.model.dao.entities.Guide;
import flexjson.JSONException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import co.beek.pano.model.dao.entities.Location;
import co.beek.pano.model.dao.entities.Scene;
import co.beek.pano.service.dataService.locationService.LocationService;
import co.beek.pano.service.dataService.sceneService.SceneService;

@Controller
public class LocationRestController extends BeekRestController {
	@Inject
	private LocationService locationService;
	
	@Inject
	private SceneService sceneService;

	@ResponseBody
	@RequestMapping(value = "/location/{locationId}/scenes", method = RequestMethod.GET)
	public ResponseEntity<String> getScenesForLocation(
			@PathVariable("locationId") String locationId) {

		try {
			List<Scene> scenes = sceneService.getAllScenesForLocation(locationId);
			return getPayloadResponse(scenes);
		} catch (Exception e) {
			return getErrorResponse(123456, e.getMessage());
		}
	}

	@ResponseBody
	@RequestMapping(value = "/location/create/{name}/{lat}/{lon}/", method = RequestMethod.GET)
	public ResponseEntity<String> createLocation(
			@PathVariable("name") String locationName,@PathVariable("lat") String lat,@PathVariable("lon") String lon) {
			Location location;
			location = new Location();
			location.setStatus(Location.STATE_NEW);
			location.setTitle(locationName);
			location.setTeamId("CUSTOMER_1_");
			location.setDestinationId("1");
		    location.setLatitude(Double.parseDouble(lat));
		    location.setLongitude(Double.parseDouble(lon));

		try {
			locationService.saveLocation(location);
			return getPayloadResponse(location);
		} catch (Exception e) {
			return getErrorResponse(123456, e.getMessage());
		}
	}

	@ResponseBody
	@RequestMapping(value = "/location/{locationId}/createscene/{sceneName}", method = RequestMethod.GET)
	public ResponseEntity<String> createLocationScene(
			@PathVariable("locationId") String locationId) {
			Scene scene = new Scene();
		    scene.setLocation(locationService.getLocation(locationId));
		try {
			sceneService.saveScene(scene);
			return getPayloadResponse(scene);
		} catch (Exception e) {
			return getErrorResponse(123456, e.getMessage());
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/locations/within/{minLat}/{minLon}/{maxLat}/{maxLon}/", method = RequestMethod.GET)
	public ResponseEntity<String> getWith(
			@PathVariable("minLat") double minLat,
			@PathVariable("minLon") double minLon,
			@PathVariable("maxLat") double maxLat,
			@PathVariable("maxLon") double maxLon) {	
		try {
			List<Location> scenes = locationService.getLocationsWithin(minLat,
					minLon, maxLat, maxLon);
			return getPayloadResponse(scenes);
		} catch (Exception e) {
			return getErrorResponse(123456, e.getMessage());
		}
	}

	@ResponseBody
	@RequestMapping(value = "/locations/destination/{destinationId}", method = RequestMethod.GET)
	public ResponseEntity<String> getForDestination(
			@PathVariable("destinationId") String destinationId) {
		try {
			List<Location> locations = locationService.getLocationsForDestination(destinationId);
			return getPayloadResponse(locations);
		} catch (Exception e) {
			return getErrorResponse(123456, e.getMessage());
		}
	}

	@ResponseBody
	@RequestMapping(value = "/locations/all/", method = RequestMethod.GET)
	public ResponseEntity<String> getAllLocations()
			 {
		try {
			List<Location> locations = locationService.getAllLocations();
			Collections.reverse(locations);
			return getPayloadResponse(locations);
		} catch (Exception e) {
			return getErrorResponse(123456, e.getMessage());
		}
	}


	@ResponseBody
	@RequestMapping(value = "/locations/all/jsonp", method = RequestMethod.GET)
	public ResponseEntity<String> getAllLocationsJsonP(@RequestParam("callback") String callback) {
		try {
			List<Location> locations = locationService.getAllLocations();
			Collections.reverse(locations);
			String sessionJson = callback + "("
					+ serializer.deepSerialize(locations) + ");";
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type",
					"application/javascript; charset=utf-8");
			return new ResponseEntity<String>(sessionJson, responseHeaders,
					HttpStatus.OK);
		} catch (JSONException e) {
			return getErrorResponse(1234, "Error encoding guides: "
					+ e.getCause().toString());
		}
	}

	@ResponseBody
	@RequestMapping(value = "/location/{locationId}/scenes/jsonp", method = RequestMethod.GET)
	public ResponseEntity<String> getAllScenesForLocationJsonP(@PathVariable("locationId") String locationId, @RequestParam("callback") String callback) {
		try {
			List<Scene> scenes = sceneService.getAllScenesForLocation(locationId);
			String sessionJson = callback + "("
					+ serializer.deepSerialize(scenes) + ");";
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type",
					"application/javascript; charset=utf-8");
			return new ResponseEntity<String>(sessionJson, responseHeaders,
					HttpStatus.OK);
		} catch (JSONException e) {
			return getErrorResponse(1234, "Error encoding guides: "
					+ e.getCause().toString());
		}
	}
}
