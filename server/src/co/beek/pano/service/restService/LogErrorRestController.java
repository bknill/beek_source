package co.beek.pano.service.restService;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import co.beek.pano.model.dao.entities.ClientError;
import co.beek.pano.service.dataService.BeekService;

@Controller
public class LogErrorRestController extends BeekRestController {

	@Inject
	private BeekService beekService;

	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(value = "/clienterrors/logerror", method = RequestMethod.POST)
	public ResponseEntity<String> logError(
			@RequestParam("message") String message,
			@RequestParam("info") String info) {

		ClientError error = new ClientError();
		error.message = message;
		error.info = info;
		beekService.logError(error);

		return getSuccessResponse();
	}
}
