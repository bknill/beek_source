package co.beek.pano.service.restService;


import java.util.List;

public class ServiceResponse {
	
	private Object payload;

	private List nodes;

	private List edges;
	
	private int error;

	private String message;

	public void setPayload(Object payload) {
		this.payload = payload;
	}

	public Object getPayload() {
		return payload;
	}

	public List getNodes() {
		return nodes;
	}

	public void setNodes(List nodes) {
		this.nodes = nodes;
	}

	public List getEdges() {
		return edges;
	}

	public void setEdges(List edges) {
		this.edges = edges;
	}

	public void setError(int error) {
		this.error = error;
	}

	public int getError() {
		return error;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
	
}
