package co.beek.pano.service.restService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import co.beek.pano.model.dao.entities.Guide;
import co.beek.pano.model.dao.social.TwitterDAO;
import co.beek.pano.service.dataService.guideService.GuideService;

import co.beek.pano.service.dataService.SocialService.SocialService;
import flexjson.JSONException;

@Controller
public class SocialRestController extends BeekRestController {
	@Inject
	private GuideService guideService;

	@Inject
	private SocialService socialService;
	

	@ResponseBody
	@RequestMapping(value = "/social/{guideId}/{sceneId}/{type}", method = RequestMethod.GET)
	public ResponseEntity<String> getSocialData(
			@PathVariable("guideId") String guideId,
			@PathVariable("sceneId") String sceneId,
			@PathVariable("type") String type) {
		try {
			
			if(type.equals("twitter")){
				TwitterDAO tweet = socialService.getTwitter(guideId, sceneId);
				return getPayloadResponse(tweet);
			}
			
			Guide guide = guideService.getGuide(guideId);
			return getPayloadResponse(guide);
		} catch (JSONException e) {
			return getErrorResponse(1234, "Error encoding guide: "
					+ e.getCause().toString());
		}
	}

	
}
