package co.beek.pano.service.restService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import co.beek.Constants;
import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.entities.Guide;
import co.beek.pano.model.dao.entities.Location;
import co.beek.pano.service.dataService.enterprizewizardService.EWService;
import co.beek.pano.service.dataService.guideService.GuideService;
import co.beek.pano.service.dataService.locationService.LocationService;
import co.beek.pano.service.restService.result.LinkRequestOptions;
import co.beek.util.BeekSession;

@Controller
public class TeamRestController extends BeekRestController {
	@Inject
	private GuideService guideService;

	@Inject
	private LocationService locationService;

	@Inject
	private EWService ewService;

	/**
	 * Get the options for the logged in user linking to the following location
	 */
	@ResponseBody
	@RequestMapping(value = "location/{locationId}/linkOptions", method = RequestMethod.GET)
	public ResponseEntity<String> getLinkRequestOptions(
			HttpServletRequest request,
			@PathVariable("locationId") String locationId) {
		try {
			EWContact user = getSession(request).getUser();
			LinkRequestOptions res = getGuidesToJoin(user, locationId);
			return getPayloadResponse(res);
		} catch (Exception e) {
			e.printStackTrace();
			return getErrorResponse(1324, "asdf");
		}
	}

	private LinkRequestOptions getGuidesToJoin(EWContact user, String locationId)
			throws Exception {
		// The remote location that the logged in user has just linked to
		Location theirLocation = locationService.getLocation(locationId);

		// Create the return result
		LinkRequestOptions res = new LinkRequestOptions(locationId);

		// Get the locations for the logged in user
		res.myLocations = locationService
				.getLocations(user.getTeams());

		// Get the guides from the team of the remote location
		res.theirGuides = guideService.getGuidesForTeam(theirLocation
				.getTeamId());

		// remove invalid guides
		res.theirGuides = filterGuidesWithoutLocations(res.myLocations,
				res.theirGuides);

		return res;
	}

	private List<Guide> filterGuidesWithoutLocations(
			List<Location> myLocations, List<Guide> teamGuides)
			throws Exception {
		List<Guide> guidesToJoin = new ArrayList<Guide>();

		// If any of their guides have any of my locations
		// then we are already linked
		for (Guide guide : teamGuides)
			//if (!myLocationsAreInGuide(myLocations, guide))
				guidesToJoin.add(guide);

		return guidesToJoin;
	}

	private boolean myLocationsAreInGuide(List<Location> myLocations,
			Guide guide) {
		for (Location location : myLocations)
		//	if (guideService.getGuideLocation(guide.getId(), location.getId()) != null)
				return true;

		return false;
	}

	/**
	 * Request to link my team to this guide;
	 
	@ResponseBody
	@RequestMapping(value = "/team/{teamId}/link/{guideId}", method = RequestMethod.POST)
	public ResponseEntity<String> sendLinkRequest(HttpServletRequest request,
			@PathVariable("teamId") String teamId,
			@PathVariable("guideId") String guideId,
			@RequestParam("message") String message) {
		try {
			BeekSession session = getSession(request);

			String path = "/guide/{guideId}/link/{teamId}";
			path = path.replace("{guideId}", guideId);
			path = path.replace("{teamId}", teamId);

			String guideLinkUrl = "http://" + Constants.domain + path;

			Guide guide = guideService.getGuide(guideId);

			ewService.sendEmail(session.getUsername(), session.getPassword(),
					guide.getTeamId(), "GuideLinkRequest", message,
					guideLinkUrl);
			return getSuccessResponse();
		} catch (IOException e) {
			return getErrorResponse(1324, "asdf");
		}
	}
	*/
}
