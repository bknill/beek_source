package co.beek.pano.service.restService;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import co.beek.Constants;
import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.enterprizewizard.LoginResponse;
import co.beek.pano.model.dao.entities.Destination;
import co.beek.pano.model.dao.entities.Favourite;
import co.beek.pano.model.dao.entities.Visit;
import co.beek.pano.service.dataService.FavouritesService;
import co.beek.pano.service.dataService.destinationService.DestinationService;
import co.beek.pano.service.dataService.enterprizewizardService.EWService;
import co.beek.util.BeekSession;
import co.beek.util.DateUtil;
import co.beek.jobs.SendFavouriteEmailsJob;

@Controller
@Scope(value = "session")
public class UserRestController extends BeekRestController {
	@Inject
	private EWService EWService;

	@Inject
	private DestinationService destinationService;

	@Inject
	private FavouritesService favouritesService;
	
	@Inject
	private FavouritesService favouiteService;

	@ResponseBody
	@RequestMapping(value = "/user/session/", method = RequestMethod.GET)
	public ResponseEntity<String> getSessionData(HttpServletRequest request)
			throws Exception {
		BeekSession session = getSession(request);
		EWContact contact = getUser(request);

		if (contact != null)
			return getPayloadResponse(new SessionData(session.getId(), contact));

		else
			return getErrorResponse(123456, "Not Logged In");
	}

	@ResponseBody
	@RequestMapping(value = "/user/bind/{guideId}/{key}/{email}/{user_id}/{first_name}/{last_name}/", method = RequestMethod.GET)
	public ResponseEntity<String> bindEmail(HttpServletRequest request,
			@PathVariable("guideId") String guideId,
			@PathVariable("key") String key,
			@PathVariable("email") String email,
			@PathVariable("user_id") String user_id,
			@PathVariable("first_name") String first_name,
			@PathVariable("last_name") String last_name){
		try {

			// String text = request.getParameter("text");
			// remove the contact from the session
			Visit visit = new Visit();
			visit.setKey(key);
			visit.setEmail(email);
			visit.setUserId(user_id);
			visit.setFirstName(first_name);
			visit.setLastName(last_name);
			visit.guideId = guideId;

			favouritesService.logVisit(visit);

			// return success
			return getSuccessResponse();

		} catch (Exception e) {
			return getErrorResponse(e);
		}
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/user/{guideId}/{sceneId}", method = RequestMethod.GET)
	public ResponseEntity<String> getHaveBeens(HttpServletRequest request,
			@PathVariable("guideId") String guideId,
			@PathVariable("sceneId") String sceneId){
		try {

			List<Favourite> haveBeens = favouritesService.getVisitsByGuideScene(guideId, sceneId);

			// return success
			return getPayloadResponse(haveBeens);

		} catch (Exception e) {
			return getErrorResponse(e);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/user/login/", method = RequestMethod.POST)
	public ResponseEntity<String> login(HttpServletRequest request,
			@RequestParam("email") String email,
			@RequestParam("pass") String pass) throws Exception {

		try {
			LoginResponse loginResponse = EWService.login(email, pass);
			if (loginResponse.getError() != null)
				return getErrorResponse(1234, loginResponse.getError());

			else
				return getLoginResponse(request, email, pass,
						loginResponse.getUser());

		} catch (Exception e) {
			return getErrorResponse(Constants.ERROR_SERVICE, "Service error: "
					+ e.getMessage());
		}
	}

	private ResponseEntity<String> getLoginResponse(HttpServletRequest request,
			String username, String password, EWContact user)
			throws IOException {

		List<Destination> destinations;
		if (user.isAdmin())
			destinations = destinationService.getAllDestinations();
		else
			destinations = getDestinations(user.getDestinations());

		// set the EWContact on the session
		BeekSession session = getSession(request);
		session.setUsername(username);
		session.setPassword(password);
		session.setUser(user);
		session.setDestinations(destinations);


		// Populate the data to send to the client
		SessionData sessionData = new SessionData(session.getId(), user);

		return getPayloadResponse(sessionData);
	}

	private List<Destination> getDestinations(List<String> destinationTitles) {
		List<Destination> destinations = new ArrayList<Destination>();
		for (String title : destinationTitles) {
			Destination destination = destinationService
					.getDestinationByTitle(title);
			if (destination != null)
				destinations.add(destination);
		}

		return destinations;
	}

	@ResponseBody
	@RequestMapping(value = "/user/logout/", method = RequestMethod.GET)
	public ResponseEntity<String> login(HttpServletRequest request)
			throws Exception {
		// remove the contact from the session
		HttpSession session = request.getSession();
		session.setAttribute("contact", null);

		// return success
		return getSuccessResponse();
	}

	@ResponseBody
	@RequestMapping(value = "/user/{email}/favourite/{guideId}/{sceneId}/{hotspotId}/{type}/{favouriteBoolean}", method = RequestMethod.GET)
	public ResponseEntity<String> favourite(HttpServletRequest request,
			@PathVariable("email") String email,
			@PathVariable("guideId") String guideId,
			@PathVariable("sceneId") String sceneId,
			@PathVariable("hotspotId") String hotspotId,
			@PathVariable("type") int type,
			@PathVariable("favouriteBoolean") int favouriteBoolean) {
		try {

			// String text = request.getParameter("text");
			// remove the contact from the session
			Favourite favourite = new Favourite();
			favourite.email = email;
			favourite.guideId = guideId;
			favourite.sceneId = sceneId;
			favourite.hotspotId = hotspotId;
			favourite.type = type;
			favourite.favourite = favouriteBoolean;
			

			favouritesService.logFavourite(favourite);

			// return success
			return getSuccessResponse();

		} catch (Exception e) {
			return getErrorResponse(e);
		}
	}
	
	
	
	@ResponseBody
	@RequestMapping(value = "/user/{email}/send/{guideId}", method = RequestMethod.GET)
	public ResponseEntity<String> sendFavouritesEmail(HttpServletRequest request)
	{
		
		favouiteService.sendFavouriteEmails();
		
		System.out.println("I should be sending emails about now");

		// return success
		return getSuccessResponse();
	}

}

final class SessionData implements Serializable {
	private static final long serialVersionUID = 1L;

	private String sessionId;

	private EWContact contact;

	public SessionData(String sessionId, EWContact contact) {
		this.sessionId = sessionId;
		this.contact = contact;
	}

	public String getSessionId() {
		return sessionId;
	}

	public EWContact getContact() {
		return contact;
	}
}