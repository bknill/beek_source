package co.beek.pano.service.restService;

import org.atmosphere.config.service.DeliverTo;
import org.atmosphere.config.service.Disconnect;
import org.atmosphere.config.service.ManagedService;
import org.atmosphere.config.service.Message;
import org.atmosphere.config.service.PathParam;
import org.atmosphere.config.service.Ready;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.AtmosphereResourceEvent;
import org.atmosphere.cpr.AtmosphereResourceFactory;
import org.atmosphere.cpr.BroadcasterFactory;
import org.atmosphere.cpr.MetaBroadcaster;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;	

import javax.inject.Inject;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;


/**
 * Simple annotated class that demonstrate the power of Atmosphere. This class supports all transports, support
 * message length guarantee, heart beat, message cache thanks to the {@link ManagedService}.
 */
@ManagedService(path = "/websocket/{id: [a-zA-Z][a-zA-Z_0-9]*}")
public class WebSocketController {
    private final Logger logger = LoggerFactory.getLogger(WebSocketController.class);

    private final ConcurrentHashMap<String, String> users = new ConcurrentHashMap<String, String>();

    private final static String WEBSOCKET = "/websocket/";

    @PathParam("id")
    private static String socketId;

    @Inject
    private BroadcasterFactory factory;

    @Inject
    private AtmosphereResourceFactory resourceFactory;

    @Inject
    private MetaBroadcaster metaBroadcaster;
    

    private String broadcaster;
    private String receiver;


    /**
     * Invoked when the connection as been fully established and suspended, e.g ready for receiving messages.
     *
     * @param r
     */
    @Ready
    @DeliverTo(DeliverTo.DELIVER_TO.RESOURCE)
    public void onReady(final AtmosphereResource r) {
        logger.info("Browser {} connected." + socketId, r.uuid() );
      //  logger.info("BroadcasterFactory used {}", factory.getClass().getName());
        System.out.println("CONNECTIONS");
        if(broadcaster == null)
        	broadcaster = r.uuid();
    	else
    		receiver = r.uuid();
    }


    @Disconnect
    public void onDisconnect(AtmosphereResourceEvent event) {
        if (event.isCancelled()) {
            // We didn't get notified, so we remove the user.
            users.values().remove(event.getResource().uuid());
            logger.info("Browser {} unexpectedly disconnected", event.getResource().uuid());
        } else if (event.isClosedByClient()) {
            logger.info("Browser {} closed the connection", event.getResource().uuid());
        }
    }

    @Message
    public String onMessage(String message) throws IOException {
        logger.info("message " + message);
        System.out.println("message received");
        return message;	
    }


}