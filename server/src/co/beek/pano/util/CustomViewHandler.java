package co.beek.pano.util;

import com.sun.faces.application.view.MultiViewHandler;
import javax.faces.context.FacesContext;

public class CustomViewHandler extends MultiViewHandler {

    @Override
    public String getActionURL(FacesContext context, String viewId) {
        /*
        * if viewId does not contains extension it means it is
        * outcome from controller classes, so remove
        * extension from actionURL as well
        */
        String actionURL = super.getActionURL(context, viewId);
        if(viewId.lastIndexOf(".") == -1){
            return actionURL.substring(0, actionURL.lastIndexOf("."));
        }else{
            return actionURL;
        }
    }
}