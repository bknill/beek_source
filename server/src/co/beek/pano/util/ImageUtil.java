package co.beek.pano.util;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.apache.commons.io.IOUtils;
import org.imgscalr.Scalr;
import org.primefaces.model.UploadedFile;
import org.springframework.web.multipart.MultipartFile;

import co.beek.Constants;

public class ImageUtil {

	public static void resizeImage(int newImageWidth, File sourceImage,
			File destinationImage) throws IOException {
		String filename = sourceImage.getName();
		String extension = filename.substring(filename.lastIndexOf(".") + 1);

		BufferedImage originalBufferedImage = ImageIO.read(sourceImage);
		BufferedImage resizedBufferedImage = Scalr.resize(
				originalBufferedImage, newImageWidth,
				BufferedImage.TYPE_INT_ARGB);
		ImageIO.write(resizedBufferedImage, extension, destinationImage);
	}

	public static void resizeImage(File sourceImage, int newImageWidth)
			throws IOException {
		String filename = sourceImage.getName();
		String extension = filename.substring(filename.lastIndexOf(".") + 1);

		BufferedImage originalBufferedImage = ImageIO.read(sourceImage);
		BufferedImage resizedBufferedImage = Scalr.resize(
				originalBufferedImage, newImageWidth,
				BufferedImage.TYPE_INT_ARGB);
		ImageIO.write(resizedBufferedImage, extension, sourceImage);
	}

	public static File getTempFile(UploadedFile upload) throws IOException {
		String filename = upload.getFileName();
		String realname = getRealName(filename);
		java.io.File tempFile = new java.io.File(Constants.TEMP_DIR_PATH
				+ realname);

		OutputStream output = null;
		output = new FileOutputStream(tempFile);
		IOUtils.copy(upload.getInputstream(), output);
		IOUtils.closeQuietly(output);

		return tempFile;
	}

	public static File getTempFile(MultipartFile multipartFile)
			throws IOException {

		File tempFile = new File(Constants.TEMP_DIR_PATH
				+ multipartFile.getName());
		multipartFile.transferTo(tempFile);

		return tempFile;
	}

	public static String getRealName(String fileName) {
		String extension = fileName.substring(fileName.lastIndexOf("."));
		extension = extension.toLowerCase().replace("jpeg", "jpg");
		return UUID.randomUUID().toString() + extension;
	}

	public static void copyTo(InputStream inputStream, File outputFile) {
		try {
			FileOutputStream outputStream = new FileOutputStream(outputFile);
			byte[] buffer = new byte[4096];
			int bytesRead;
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outputStream.write(buffer, 0, bytesRead);
			}
			inputStream.close();
			outputStream.close();
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
	}
	
	public static int[][] convertImageToPixels(BufferedImage image) {

		final byte[] pixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
		final int width = image.getWidth();
		final int height = image.getHeight();
		final boolean hasAlphaChannel = image.getAlphaRaster() != null;

		int[][] result = new int[height][width];
		if (hasAlphaChannel) {
			final int pixelLength = 4;
			for (int pixel = 0, row = 0, col = 0; pixel < pixels.length; pixel += pixelLength) {
				int argb = 0;
				argb += (((int) pixels[pixel] & 0xff) << 24); // alpha
				argb += ((int) pixels[pixel + 1] & 0xff); // blue
				argb += (((int) pixels[pixel + 2] & 0xff) << 8); // green
				argb += (((int) pixels[pixel + 3] & 0xff) << 16); // red
				result[row][col] = argb;
				col++;
				if (col == width) {
					col = 0;
					row++;
				}
			}
		} else {
			final int pixelLength = 3;
			for (int pixel = 0, row = 0, col = 0; pixel < pixels.length; pixel += pixelLength) {
				int argb = 0;
				argb += -16777216; // 255 alpha
				argb += ((int) pixels[pixel] & 0xff); // blue
				argb += (((int) pixels[pixel + 1] & 0xff) << 8); // green
				argb += (((int) pixels[pixel + 2] & 0xff) << 16); // red
				result[row][col] = argb;
				col++;
				if (col == width) {
					col = 0;
					row++;
				}
			}
		}
		return result;
	}
	
}
