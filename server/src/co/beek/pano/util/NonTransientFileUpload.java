package co.beek.pano.util;

/**
 * Created by Ben on 17/08/2017.
 */
public class NonTransientFileUpload extends org.primefaces.component.fileupload.FileUpload {

    @Override
    public boolean isTransient() {
        return false;
    }
}
