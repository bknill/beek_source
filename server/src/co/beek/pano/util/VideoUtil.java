package co.beek.pano.util;

import co.beek.Constants;

import com.coremedia.iso.IsoFile;
import com.coremedia.iso.boxes.Box;
import com.coremedia.iso.boxes.ChunkOffsetBox;
import com.coremedia.iso.boxes.StaticChunkOffsetBox;
import com.googlecode.mp4parser.util.Path;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import javax.imageio.ImageIO;

import org.apache.commons.lang3.StringUtils;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacpp.avcodec;
import org.bytedeco.javacpp.avutil;
import org.bytedeco.javacv.*;



import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import static org.bytedeco.javacpp.avutil.AV_LOG_QUIET;
import static org.bytedeco.javacpp.avutil.av_log_set_level;



public class VideoUtil {

    static {
        try {
            System.setProperty("java.library.path", Constants.NATIVE_DIR_PATH);
            Field fieldSysPath = ClassLoader.class.getDeclaredField("sys_paths");
            fieldSysPath.setAccessible(true);
            fieldSysPath.set(null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static File toWebm(File file)
            throws FrameGrabber.Exception, FrameRecorder.Exception, InterruptedException {

        //comment it if you want to see logs produced by ffmpeg
        av_log_set_level(AV_LOG_QUIET);

        String fileName = getFileNameWithoutExt(file);
        File webmFile = new File(Constants.TEMP_DIR_PATH + fileName + ".webm");

        FFmpegFrameGrabber grabber = new FFmpegFrameGrabber(file.getPath());
        grabber.start();

        FFmpegFrameRecorder recorder = new FFmpegFrameRecorder(webmFile.getPath(),
                grabber.getImageWidth(), grabber.getImageHeight());

        recorder.setFormat("webm");
        recorder.setVideoOption("preset", "ultrafast");

        recorder.setVideoQuality(5);
        recorder.setVideoCodec(avcodec.AV_CODEC_ID_VP8);
        recorder.setVideoBitrate(grabber.getVideoBitrate());
        recorder.setPixelFormat(avutil.AV_PIX_FMT_YUV420P);

        recorder.setAudioCodec(avcodec.AV_CODEC_ID_VORBIS);
        recorder.setAudioBitrate(grabber.getAudioBitrate());
        recorder.setAudioChannels(grabber.getAudioChannels());

        recorder.setSampleRate(grabber.getSampleRate());
        recorder.setFrameRate(grabber.getFrameRate());
        recorder.setSampleFormat(avutil.AV_SAMPLE_FMT_FLTP);

        encode(grabber, recorder);
        return webmFile;
    }

    public static File toOgv(File file)
            throws FrameGrabber.Exception, FrameRecorder.Exception, InterruptedException {

    	
    	System.out.println("toOgv " + file.toString());
        //comment it if you want to see logs produced by ffmpeg
        av_log_set_level(AV_LOG_QUIET);

        String fileName = getFileNameWithoutExt(file);
        File ogvFile = new File(Constants.TEMP_DIR_PATH + fileName + ".ogv");

        FFmpegFrameGrabber grabber = new FFmpegFrameGrabber(file.getPath());
        grabber.start();

        FFmpegFrameRecorder recorder = new FFmpegFrameRecorder(ogvFile.getPath(),
                grabber.getImageWidth(), grabber.getImageHeight());

        recorder.setFormat("ogv");
        recorder.setVideoOption("preset", "ultrafast");

        recorder.setVideoQuality(5);
        recorder.setVideoCodec(avcodec.AV_CODEC_ID_THEORA);
        recorder.setVideoBitrate(grabber.getVideoBitrate());
        recorder.setPixelFormat(avutil.AV_PIX_FMT_YUV420P);

        recorder.setAudioCodec(avcodec.AV_CODEC_ID_VORBIS);
        recorder.setAudioBitrate(grabber.getAudioBitrate());
        recorder.setAudioChannels(grabber.getAudioChannels());

        recorder.setSampleRate(grabber.getSampleRate());
        recorder.setFrameRate(grabber.getFrameRate());
        recorder.setSampleFormat(grabber.getSampleFormat());

        encode(grabber, recorder);
        return ogvFile;
    }

    public static File addMoovAtomAtBeginning(File file) throws IOException {
    	System.out.println("addMoovAtomAtBeginning " + file.toString());
        FileOutputStream fos = null;
        IsoFile isoFile = null;
        File tempFile = null;
        try {
            isoFile = new IsoFile(file.getPath());
            List<Box> boxes = new LinkedList<Box>(isoFile.getBoxes());

            if (boxes.size() >= 2) {
                if (boxes.get(1).getType().equals("moov")) {
                    System.out.println("File already in progressive download mode");
                    return file;
                }
            }

            String tempFileName = UUID.randomUUID().toString() + ".mp4";
            tempFile = new File(Constants.TEMP_DIR_PATH + tempFileName);

            for (int i = 0; i < boxes.size(); i++) {
                if (boxes.get(i).getType().equals("moov")) {
                    isoFile.getBoxes().remove(i);
                    isoFile.getBoxes().add(1, boxes.get(i));
                }
            }

            if (isoFile.getMovieBox() != null)
                correctChunkOffsets(isoFile, isoFile.getMovieBox().getSize());

            fos = new FileOutputStream(tempFile);
            isoFile.writeContainer(fos.getChannel());

            return tempFile;
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                if (fos != null) fos.close();
                if (isoFile != null) {
                    isoFile.close();
                    isoFile = null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                System.gc();
            }
        }
    }

    private static void correctChunkOffsets(IsoFile tempIsoFile, long correction) {
        List<Box> chunkOffsetBoxes = Path.getPaths(tempIsoFile, "/moov[0]/trak/mdia[0]/minf[0]/stbl[0]/stco[0]");
        for (Box chunkOffsetBox : chunkOffsetBoxes) {
            LinkedList<Box> stblChildren = new LinkedList<Box>(chunkOffsetBox.getParent().getBoxes());
            stblChildren.remove(chunkOffsetBox);
            long[] cOffsets = ((ChunkOffsetBox) chunkOffsetBox).getChunkOffsets();
            for (int i = 0; i < cOffsets.length; i++) {
                cOffsets[i] += correction;
            }
            StaticChunkOffsetBox cob = new StaticChunkOffsetBox();
            cob.setChunkOffsets(cOffsets);
            stblChildren.add(cob);
            chunkOffsetBox.getParent().setBoxes(stblChildren);
        }
    }

    private static void encode(FFmpegFrameGrabber grabber, FFmpegFrameRecorder recorder)
            throws FrameGrabber.Exception, FrameRecorder.Exception, InterruptedException {

    	System.out.println("encode()");
        recorder.start();
        Frame frame = null;
        while ((frame = grabber.grabFrame()) != null) {
            if (frame != null) {
                if (frame.image != null) {
                    recorder.record(frame.image);
                } else if (frame.samples != null) {
                    recorder.record(frame.samples);
                }
            } else {
                break;
            }
            Thread.sleep((long) grabber.getFrameRate());
        }
        if (grabber != null) {
            grabber.stop();
            grabber.release();
        }

        if (recorder != null) {
            recorder.stop();
            recorder.release();
        }
    }

    private static String getFileNameWithoutExt(File file) {
        String fileName = file.getName();
        return fileName.substring(0, fileName.lastIndexOf("."));
    }

/*    public static File getFrame(File file) throws  JCodecException {

        try {
            BufferedImage frame = FrameGrab.getFrame(file, 10);


            File tempFile = new File(Constants.TEMP_DIR_PATH + file.getName() + ".jpg");
            ImageIO.write(frame, "jpg", tempFile);
            return tempFile;


        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;

    }*/

    public static BufferedImage convert(BufferedImage src, int bufImgType) {
        BufferedImage img= new BufferedImage(src.getWidth(), src.getHeight(), bufImgType);
        Graphics2D g2d= img.createGraphics();
        g2d.drawImage(src, 0, 0, null);
        g2d.dispose();
        return img;
    }

    public static Boolean shortenVideo(File file, Double startTime, Double endTime){

        Boolean start = startTime == 0.0;
        Boolean duration = endTime == 0.0;
        File outFile = new File(Constants.TEMP_DIR_PATH +"_" + file.getName());

        Process ffmpeg = null;
        ProcessBuilder pb = new ProcessBuilder(
                "ffmpeg", "-y", "-i",
                file.getAbsolutePath(),
                start  ? "" : "-ss", start  ? "" : startTime.toString(),
                duration ? "" : "-to", duration ? "" : endTime.toString(),
                 "-map", "0",
                outFile.getAbsolutePath()
        );

        pb.redirectErrorStream(true);
        System.out.println(pb.command());
        try {
            ffmpeg = pb.start();
            if(runProcess(ffmpeg)){
                file.delete();
                outFile.renameTo(new File(Constants.TEMP_DIR_PATH + file.getName()));
                return true;
            }
            else
                return false;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    public static Boolean setVideoCentre(Double theta, File video){

        Integer width = getVideoWidth(video);
        Integer height = width / 2;
        Double per = theta / 180.00;
        Double wPer = (double)height * per;
        Integer dist1 = height + wPer.intValue();
        Integer dist2 = height - wPer.intValue();

        File outFile1 = new File(Constants.TEMP_DIR_PATH +"1_" + video.getName());
        File outFile2 = new File(Constants.TEMP_DIR_PATH +"2_" + video.getName());
        File outFile3 = new File(Constants.TEMP_DIR_PATH +"_" + video.getName());

        Process ffmpeg = null;
        ProcessBuilder pb1 = new ProcessBuilder(
                "ffmpeg", "-y", "-i",
                video.getAbsolutePath(),
                "-filter:v","\"crop=" + dist1 +":" + height +":0:0\"",
                outFile1.getAbsolutePath()
        );
        pb1.redirectErrorStream(true);

        ProcessBuilder pb2 = new ProcessBuilder(
                "ffmpeg", "-y", "-i",
                video.getAbsolutePath(),
                "-filter:v","\"crop=" + dist2 +":" + height +":" + dist1 +":0\"",
                outFile2.getAbsolutePath()
        );
        pb2.redirectErrorStream(true);

        ProcessBuilder pb3 = new ProcessBuilder(
                "ffmpeg", "-y", "-i",
                outFile2.getAbsolutePath(),
                "-i",
                outFile1.getAbsolutePath(),
                "-filter_complex","\"[0:v][1:v]hstack=inputs=2[v];[0:a][1:a]amerge[a]\"","-map","\"[v]\"","-map","\"[a]\"","-ac","2",
                outFile3.getAbsolutePath()
        );
        pb3.redirectErrorStream(true);

        try {
            ffmpeg = pb1.start();
            if(runProcess(ffmpeg)){
                ffmpeg = pb2.start();
                if(runProcess(ffmpeg)){
                    ffmpeg = pb3.start();
                    if(runProcess(ffmpeg)){
                        video.delete();
                        outFile1.delete();
                        outFile2.delete();
                        outFile3.renameTo(new File(Constants.TEMP_DIR_PATH + video.getName()));
                        return true;
                    }
                    else{
                        outFile1.delete();
                        outFile2.delete();
                        outFile3.delete();
                        return false;
                    }
                }
            }
            else
                return false;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }


    public static Integer getVideoWidth(File video){
        Process ffmpeg = null;
        ProcessBuilder pb = new ProcessBuilder(
                "ffprobe", "-v", "error", "-select_streams","v:0", "-show_entries", "stream=coded_width", //"-of default=noprint_wrappers=1:nokey=1",
                video.getAbsolutePath()
        );

        pb.redirectErrorStream(true);
        try {
            ffmpeg = pb.start();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(ffmpeg.getInputStream()));
        String line = null;
        try {
            while ((line = reader.readLine()) != null)
                if(line.indexOf("coded_width=") > -1)
                    return Integer.valueOf(line.replaceAll("\\D+",""));

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Boolean mergeVideoAndAudio(File video, File audio){


        File outFile = new File(Constants.TEMP_DIR_PATH +"_" + video.getName());

        Process ffmpeg = null;

        //keep old one in case new one fucks up
/*        ProcessBuilder pb = new ProcessBuilder(
                "ffmpeg", "-y", "-i", video.getAbsolutePath(),
                "-i", audio.getAbsolutePath(),
                "-filter_complex", "\"[0:a][1:a]amix=inputs=2:duration=longest[out]\"", "-map", "0:v", "-map", "[out]",
                outFile.getAbsolutePath()
        );*/

        ProcessBuilder pb = new ProcessBuilder(
                "ffmpeg", "-y", "-i", audio.getAbsolutePath(),
                "-i", video.getAbsolutePath(),
                "-filter_complex", "\"[0:a]aformat=sample_fmts=fltp:sample_rates=44100:channel_layouts=stereo,volume=1.0[a1]; [1:a]aformat=sample_fmts=fltp:sample_rates=44100:channel_layouts=stereo,volume=0.5[a2]; [a1][a2]amerge,pan=stereo:c0<c0+c2:c1<c1+c3[out]\"", "-map", "1:v", "-map", "[out]","-c:v" , "copy", "-c:a", "aac", "-strict", "-2",
                outFile.getAbsolutePath()
        );
       // System.out.println(pb.command());
        pb.redirectErrorStream(true);

        try {
            ffmpeg = pb.start();
            if(runProcess(ffmpeg)){
                video.delete();
                outFile.renameTo(new File(Constants.TEMP_DIR_PATH + video.getName()));
                return true;
            }
            else{
                System.out.println("failed");
                return false;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    return false;

    }


    public static File joinVideos(File listFile){

        File out = new File(Constants.TEMP_DIR_PATH + UUID.randomUUID().toString() + ".mp4");
        Process ffmpeg = null;
        ProcessBuilder pb = new ProcessBuilder(
                "ffmpeg", "-f",
                "concat", "-i", listFile.getAbsolutePath(),
                "-safe", "0",
                "-c:v","copy","-c:a","aac","-strict","experimental",
                out.getAbsolutePath()
        );
        pb.redirectErrorStream(true);
        try {
            ffmpeg = pb.start();
            if(runProcess(ffmpeg)){
                return out;
            }
            else
                return null;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static Boolean runProcess(Process ffmpeg){

        BufferedReader reader = new BufferedReader(new InputStreamReader(ffmpeg.getInputStream()));
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            if (ffmpeg.waitFor() == 0) {
                ffmpeg.destroy();
                reader.close();
                System.out.println("Successful encoding");
                return true;

            } else {
                System.out.println("Unsuccessful encoding");
                return false;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

}