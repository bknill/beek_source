package co.beek.pdf;

import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;

public class BeekTable extends PdfPTable {
	public BeekTable(int columns) {
		super(columns);
	}

	/**
	 * makes a title cell for make table function
	 * 
	 * @param text
	 * @return
	 */
	protected PdfPCell makeTitleCell(String text) {
		Font colored = new Font(FontFamily.HELVETICA, 10);

		colored.setColor(128, 128, 128);
		Phrase p = new Phrase(text, colored);
		PdfPCell cell = new PdfPCell(p);
		cell.setColspan(2);
		cell.setPaddingBottom(10f);
		cell.setBorder(Rectangle.BOTTOM);

		return cell;

	}

	protected PdfPCell makeHeadingCell(String text) {
		Font colored = new Font(FontFamily.HELVETICA, 10);

		colored.setColor(128, 128, 128);
		Phrase p = new Phrase(text, colored);
		PdfPCell cell = new PdfPCell(p);
		cell.setPaddingBottom(10f);
		cell.setBorder(Rectangle.BOTTOM);

		return cell;

	}

	/**
	 * Makes a cell that has text for the make table function
	 */
	protected PdfPCell makeLabelCell(String text) {
		Font colored = new Font(FontFamily.HELVETICA, 9);

		colored.setColor(128, 128, 128);
		Phrase p = new Phrase(text, colored);

		PdfPCell cell = new PdfPCell(p);
		cell.setBorder(Rectangle.NO_BORDER);

		// cell.setPaddingTop(5f);

		return cell;

	}

	/**
	 * Makes a table cell that contains a number for make table
	 */
	protected static PdfPCell makeNumberCell(String text) {

		Font colored = new Font(FontFamily.HELVETICA, 9);
		colored.setColor(0, 76, 171);
		Phrase p = new Phrase(text, colored);

		PdfPCell cell = new PdfPCell(p);
		cell.setBorder(Rectangle.NO_BORDER);

		// cell.setPaddingTop(5f);

		return cell;

	}

	/**
	 * Makes a table cell that contains a number for make table
	 */
	protected static PdfPCell makeNumberCell(int number) {
		return makeNumberCell(Integer.toString(number));
	}

}
