package co.beek.pdf;

import co.beek.pano.model.dao.googleanalytics.Country;

public class DemographicsTable extends BeekTable {
	public DemographicsTable() {
		super(4);
		addCell(makeHeadingCell("Country"));
		addCell(makeHeadingCell("Visits"));
		addCell(makeHeadingCell("PageViewsPerVisit"));
		addCell(makeHeadingCell("AvgTimeOnSite"));
	}

	public void addRow(Country country) {
		addCell(makeLabelCell(country.getName()));
	//	addCell(makeNumberCell(country.getVisits()));
	//	addCell(makeNumberCell(country.getPageViewsPerVisit()));
	//(makeNumberCell(country.getAvgTimeOnSite()));
	}
}
