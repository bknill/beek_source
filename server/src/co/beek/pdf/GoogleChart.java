package co.beek.pdf;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import co.beek.pano.model.beans.DataListRow;
import co.beek.pano.model.beans.HiddenTableRow;
import co.beek.pano.model.beans.HotspotTableRow;
import co.beek.pano.model.beans.ReferralGuideTableRow;
import co.beek.pano.model.beans.TableRowUtil;
import co.beek.pano.model.beans.TableRowUtilComparator;

import com.googlecode.charts4j.AxisLabels;
import com.googlecode.charts4j.AxisLabelsFactory;
import com.googlecode.charts4j.AxisStyle;
import com.googlecode.charts4j.AxisTextAlignment;
import com.googlecode.charts4j.BarChart;
import com.googlecode.charts4j.BarChartPlot;
import com.googlecode.charts4j.Color;
import com.googlecode.charts4j.Data;
import com.googlecode.charts4j.DataUtil;
import com.googlecode.charts4j.Fill;
import com.googlecode.charts4j.Fills;
import com.googlecode.charts4j.GCharts;
import com.googlecode.charts4j.LegendPosition;
import com.googlecode.charts4j.Line;
import com.googlecode.charts4j.LineChart;
import com.googlecode.charts4j.LineStyle;
import com.googlecode.charts4j.PieChart;
import com.googlecode.charts4j.Plots;
import com.googlecode.charts4j.Shape;
import com.googlecode.charts4j.Slice;


public class GoogleChart {

	/**
	 * Makes line chart out of the given data through the google charts api. Small scale sets the scale to 0-8 and is 
	 * used for the average minutes spent on site or average scenes viewed graph. Otherwise scale is set dynamically.
	 */
	public static String getLineChart(ArrayList<Double> data, ArrayList<String> labels, String series)
    {

		
		int[] iterBound = getBounds(data);
		
		int iter = iterBound[0];
		int upperBound = iterBound[1];
		
		Data scaledData = DataUtil.scaleWithinRange(0,upperBound,data);
		
		
        Line line1 = Plots.newLine(scaledData, Color.newColor("78B4FF"));
        line1.setLineStyle(LineStyle.newLineStyle(3, 1, 0));
        //line1.addShapeMarkers(Shape.CIRCLE, Color.newColor("127BFE"), 4);
        //line1.addShapeMarkers(Shape.CIRCLE, Color.WHITE, 8);
      

        // Defining chart.
        LineChart chart = GCharts.newLineChart(line1); //,line2,line3...
        chart.setSize(800, 300);
       // chart.setTitle(series+" by Week", Color.BLACK, 14);

        chart.setGrid(25, 25, 3, 2);

        // Defining axis info and styles
        AxisStyle axisStyle = AxisStyle.newAxisStyle(Color.BLACK, 12, AxisTextAlignment.CENTER);

        AxisLabels xAxis = AxisLabelsFactory.newAxisLabels(labels);
        xAxis.setAxisStyle(axisStyle);
        
        ArrayList<String> axisLabels = new ArrayList<String>();
        axisLabels.add("0");
        for (int i=iter;i<upperBound;i+=iter)
        	axisLabels.add(Integer.toString(i));
        axisLabels.add(Integer.toString(upperBound));
        
        AxisLabels yAxis = AxisLabelsFactory.newAxisLabels(axisLabels);
        
        
        yAxis.setAxisStyle(axisStyle);


        // Adding axis info to chart.
        chart.addXAxisLabels(xAxis);
        chart.addYAxisLabels(yAxis);

        // Defining background and chart fills.
        chart.setBackgroundFill(Fills.newSolidFill(Color.newColor("FFFFFF")));
        Fill fill =  Fills.newSolidFill(Color.newColor("F7F9FA"));
        chart.setAreaFill(fill);
        String url = chart.toURLString();
        
        return url;
    }
	
	/**
	 * Makes the 4 charts with no labels in the top right quadrant of the guide report
	 */
	public static String getSparkChart (ArrayList<Double> data, ArrayList<String> labels, String series)
    {

		
		int[] iterBound = getBounds(data);
		
		int iter = iterBound[0];
		int upperBound = iterBound[1];
		
		Data scaledData = DataUtil.scaleWithinRange(0,upperBound,data);
		
		
        Line line1 = Plots.newLine(scaledData, Color.newColor("78B4FF"));
        line1.setLineStyle(LineStyle.newLineStyle(3, 1, 0));
        line1.addShapeMarkers(Shape.CIRCLE, Color.newColor("127BFE"), 12);
        line1.addShapeMarkers(Shape.CIRCLE, Color.WHITE, 8);

        
        // Defining chart.
        LineChart chart = GCharts.newLineChart(line1); //,line2,line3...
        
        chart.setGrid(25, 25, 3, 2);
        
        
        ArrayList<String> axisLabels = new ArrayList<String>();
        axisLabels.add("0");
        for (int i=iter;i<upperBound;i+=iter)
        	axisLabels.add(Integer.toString(i));
        axisLabels.add(Integer.toString(upperBound));
        
        AxisLabels yAxis = AxisLabelsFactory.newAxisLabels(axisLabels);
        
        AxisStyle axisStyle = AxisStyle.newAxisStyle(Color.BLACK, 20, AxisTextAlignment.CENTER);
        yAxis.setAxisStyle(axisStyle);
        
        chart.addYAxisLabels(yAxis);

        // Defining background and chart fills.
        chart.setBackgroundFill(Fills.newSolidFill(Color.newColor("FFFFFF")));
        Fill fill =  Fills.newSolidFill(Color.newColor("FFFFFF"));
        chart.setAreaFill(fill);
        chart.setSize((int)PdfCreator.sparkchartWidth*3, (int)PdfCreator.sparkchartHeight*3);
        String url = chart.toURLString();
        
        return url;
    }
	
	
	/**
	 * Makes the pie chart that shows off referrals
	 */
	public static String referralPie (ArrayList<TableRowUtil> rows){
		String[] colors = new String[] {"0C5AA6","26527C","04396C","408AD2","679ED2", "CCCCCC", "999999", "666666", "333333", "000000" };

		Collections.sort(rows, new TableRowUtilComparator());
		ArrayList<Slice> slices = makeSlices(rows, colors);

		PieChart chart = GCharts.newPieChart(slices);

		chart.setSize(900, 300);
		chart.setMargins(200, 200, 0, 40);
		
		chart.setThreeD(false);
		String url = chart.toURLString();

		return url;
	}
	
	/**
	 * Makes an arraylist of slice objects for use in a pie chart
	 */
	private static ArrayList<Slice> makeSlices(ArrayList<TableRowUtil> rows, String[] colors )
	{
		
		ArrayList<Slice> slices = new ArrayList<Slice>();
		int lastIndex = 0;

		int upTo = (rows.size()<9 ? rows.size(): 9);
		for(int i=0;i<upTo;i++)
		{
			int colorIndex = (i<colors.length ? i:i-colors.length);
			String label = rows.get(i).getC2()+" - "+rows.get(i).getC1();
			Slice newSlice = Slice.newSlice(Integer.parseInt(rows.get(i).getC2()),Color.newColor(colors[colorIndex]),  label);
			slices.add(newSlice);
			lastIndex = colorIndex;

		}
		//other needs to be added for all but the first 9
		if (rows.size()>9){
			int sum = 0;
			for (int i=10; i<rows.size(); i++){
				sum += Integer.parseInt(rows.get(i).getC2());
			}
			int colorIndex = (lastIndex+1<colors.length ? lastIndex+1:lastIndex+1-colors.length);
			slices.add(Slice.newSlice(sum, Color.newColor(colors[colorIndex]), Integer.toString(sum)+" - Other"));
		}
		
		return slices;
	}
	
	/**
	 * Makes the pie chart that shows of demographics
	 */
	public static String regionPie (ArrayList<TableRowUtil> rows){
		String[] colors = new String[] {"0C5AA6","26527C","04396C","408AD2","679ED2", "CCCCCC", "999999", "666666", "333333", "000000" };

		ArrayList<Slice> slices = makeSlices(rows, colors);
		PieChart chart = GCharts.newPieChart(slices);

		chart.setSize(500, 250);
		chart.setMargins(10, 10, 0, 0);
		chart.setThreeD(false);
		String url = chart.toURLString();

		return url;
	}
	
	/**
	 * Makes the 4 bar charts that show how each individual scene is performing 
	 */
	public static String sceneBarChart( ArrayList<Double> data, ArrayList<String> labels, String series ) {
		
		 //more info, time spent, or total conversions
		
		ArrayList<String> labels1 = new ArrayList<String>() ;
		ArrayList<String> labels2 = new ArrayList<String>() ;
		
		//split the labels into two lists for the two axes
		for (int i =0;i<labels.size();i++){
			if (i==0 || i%2 == 0){
				labels1.add(labels.get(i));
				labels2.add("");
			}
			else{
				labels1.add("");
				labels2.add(labels.get(i));
			}
		}
		
		int[] iterBound = getBounds(data);
		
		int iter = iterBound[0];
		int upperBound = iterBound[1];
		
		Data scaledData =  DataUtil.scaleWithinRange(0, upperBound, data);
		
        AxisStyle axisStyle = AxisStyle.newAxisStyle(Color.BLACK, 12, AxisTextAlignment.CENTER);
        axisStyle.setDrawTickMarks(true);
        
        AxisLabels xAxis = AxisLabelsFactory.newAxisLabels(labels1);
        AxisLabels xAxis2 = AxisLabelsFactory.newAxisLabels(labels2);
        
        xAxis.setAxisStyle(axisStyle);
        xAxis2.setAxisStyle(axisStyle);
        
        ArrayList<String> yAxisLabels = new ArrayList<String>();
        yAxisLabels.add("0");
        for (int i=iter;i<upperBound;i+=iter)
        	yAxisLabels.add(Integer.toString(i));
        
        yAxisLabels.add(Integer.toString(upperBound));
        
        AxisLabels yAxis = AxisLabelsFactory.newAxisLabels(yAxisLabels);
        
	    BarChartPlot views = Plots.newBarChartPlot(scaledData, Color.newColor("78B4FF") );
        
        BarChart sceneChart = GCharts.newBarChart(views);
        
        sceneChart.addXAxisLabels(xAxis);
        sceneChart.addXAxisLabels(xAxis2);
        sceneChart.addYAxisLabels(yAxis);
        
        sceneChart.setGrid(25, 25, 3, 2);

        sceneChart.setBackgroundFill(Fills.newSolidFill(Color.newColor("FFFFFF")));

        sceneChart.setBarWidth(30);
        sceneChart.setSpaceBetweenGroupsOfBars(40);
        Fill fill =  Fills.newSolidFill(Color.newColor("FFFFFF"));
        sceneChart.setAreaFill(fill);
        
          
        sceneChart.setSize(1000,300);
        
        
        return ( sceneChart.toURLString() );

	}
	
	/**
	 * Makes the pie chart that shows how people came into a scene
	 */
	public static String getHotspotPie (ArrayList<HotspotTableRow> rows)
	{
		String[] colors = new String[] {"0C5AA6","26527C","04396C","408AD2","679ED2", "CCCCCC", "999999", "666666", "333333", "000000" };
		
		//alternative colors
		//"E41A1C", "377EB8", "4DAF4A" , "984EA3" , "FF7F00", "FFFF33", "A65628", "F781BF" , "999999"
		
		HashMap<String, String> lookup = new HashMap<String, String>();
		lookup.put("scene_page", "Scene page");
		lookup.put("location_page", "Location page");
		lookup.put("bubble", "Bubble");
		lookup.put("sign_board", "Sign board");
		lookup.put("sign_post", "Sign post");
		lookup.put("destination_page", "Destination page");
		
		ArrayList<Slice> slices = new ArrayList<Slice>();
		
		int i = 0;
		for (HotspotTableRow r: rows)
		{
			String label = (lookup.get(r.getType())+" to "+r.getEnd());
			slices.add(Slice.newSlice(r.getTimesChosen(),Color.newColor(colors[i]),  label));
			i++;
			if (i==colors.length) i = 0;
		}
		
		PieChart chart = GCharts.newPieChart(slices);
		
		chart.setLegendPosition(LegendPosition.LEFT);
		
		chart.setTitle("Hotspots", Color.BLACK, 16);
        chart.setSize(600, 350);
        chart.setMargins(200, 200, 0, 40);
        chart.setThreeD(false);
        String url = chart.toURLString();
		
		return url;
	}
	
	/**
	 * Makes the sideways bar chart on the location report that shows how many conversions and time spent your location is getting
	 * from other guides
	 */
	public static String getGuideBar (ArrayList<ReferralGuideTableRow> data)
	{
		// sort the data by conversion
		class ReferralGuideTableRowComparator implements Comparator<ReferralGuideTableRow> {
		    public int compare(ReferralGuideTableRow r1, ReferralGuideTableRow r2) {
		    	if (r2.getConversions()==0 && r1.getConversions()==0) //if both have 0 conversions sort by time
		    		return (int) (r2.getTimeSpent()- (int) r1.getTimeSpent());
		    	
		        return r2.getConversions() - r1.getConversions();
		    }
		}
		
		Collections.sort(data, new ReferralGuideTableRowComparator() );
		
		ArrayList<String> labels = new ArrayList<String>();
		ArrayList<Double> time = new ArrayList<Double>();
		ArrayList<Double> converted = new ArrayList<Double>();
		
		int counter = 0;
		for (ReferralGuideTableRow r: data)
		{
			if (counter< 8){
			labels.add(r.getGuideTitle());
			time.add(r.getTimeSpent());
			System.out.print(r.getTimeSpent()+", ");
			converted.add( (double) r.getConversions() );
			}
			
			counter++;
		}
		

		int[] iterBound = getBounds(time);
		int[] iterBound2 = getBounds(converted);
		
		int iter = iterBound[0];
		int upperBound = iterBound[1];
		
		int iter2 = iterBound2[0];
		int upperBound2 = iterBound2[1];
		
		Data scaledConversions =  DataUtil.scaleWithinRange(0, upperBound, converted);
		Data scaledTime =  DataUtil.scaleWithinRange(0, upperBound, time);
		
		
        AxisStyle axisStyle = AxisStyle.newAxisStyle(Color.BLACK, 12, AxisTextAlignment.CENTER);
        axisStyle.setDrawTickMarks(true);
        
        AxisLabels xAxis = AxisLabelsFactory.newAxisLabels(labels);
        
        xAxis.setAxisStyle(axisStyle);
        
        //create labels for time series
        ArrayList<String> yAxisLabels = new ArrayList<String>();
        yAxisLabels.add("0");
        for (int i=iter;i<upperBound;i+=iter)
        	yAxisLabels.add(Integer.toString(i));
        yAxisLabels.add(Integer.toString(upperBound));
        
        
        //create labels for conversion series
        ArrayList<String> yAxisLabels2 = new ArrayList<String>();
        yAxisLabels2.add("0");
        for (int i=iter2;i<upperBound2;i+=iter2)
        	yAxisLabels2.add(Integer.toString(i));
        yAxisLabels2.add(Integer.toString(upperBound2));
        
        AxisLabels yAxis = AxisLabelsFactory.newAxisLabels(yAxisLabels);
        AxisLabels yAxis2 = AxisLabelsFactory.newAxisLabels(yAxisLabels2);
        
        yAxis.setAxisStyle(AxisStyle.newAxisStyle(Color.newColor("78B4FF"), 12, AxisTextAlignment.CENTER));
        yAxis2.setAxisStyle(AxisStyle.newAxisStyle(Color.newColor("E01B1B"), 12, AxisTextAlignment.CENTER));

		
        //assemble chart elements
        BarChartPlot conversionsPlot = Plots.newBarChartPlot(scaledConversions, Color.newColor("E01B1B") , "Conversions" );
        BarChartPlot timePlot = Plots.newBarChartPlot(scaledTime, Color.newColor("78B4FF") , "Time Spent" );
        
        
        BarChart contentChart = GCharts.newBarChart(conversionsPlot, timePlot );

        contentChart.addXAxisLabels(yAxis);
        contentChart.addYAxisLabels(xAxis);
        contentChart.addTopAxisLabels(yAxis2);
        
        
        contentChart.setGrid(25, 25, 3, 2);

        contentChart.setHorizontal(true);
        

        contentChart.setBackgroundFill(Fills.newSolidFill(Color.newColor("FFFFFF")));

        contentChart.setBarWidth(15);
        contentChart.setSpaceBetweenGroupsOfBars(15);
        Fill fill =  Fills.newSolidFill(Color.newColor("F7F9FA"));
        contentChart.setAreaFill(fill);
        
		contentChart.setSize(400, 740);
        
        
        System.out.println();
		return contentChart.toURLString();

	}
	
	/**
	 * Makes the sideways bar chart that shows 
	 */
	public static String getContentElementBar (ArrayList<HiddenTableRow> data)
	{
		ArrayList<String> labels = new ArrayList<String>();
		ArrayList<Double> clicked = new ArrayList<Double>();
		ArrayList<Double> converted = new ArrayList<Double>();
		if (data==null ) return "";
		
		for (HiddenTableRow r: data )
		{
			int index = insertionIndex(clicked, r.getTimesClicked());
			labels.add(index, r.getElementTitle());
			clicked.add(index, (double)r.getTimesClicked());
			converted.add(index, (double)r.getTimesConverted());
		}
		
		int[] iterBound = getBounds(clicked);
		
		int iter = iterBound[0];
		int upperBound = iterBound[1];
		
		
		Data scaledClicks =  DataUtil.scaleWithinRange(0, upperBound, clicked);
		Data scaledConversions =  DataUtil.scaleWithinRange(0, upperBound, converted);
		
        AxisStyle axisStyle = AxisStyle.newAxisStyle(Color.BLACK, 12, AxisTextAlignment.CENTER);
        axisStyle.setDrawTickMarks(true);
        
        AxisLabels xAxis = AxisLabelsFactory.newAxisLabels(labels);
        
        xAxis.setAxisStyle(axisStyle);
        
        ArrayList<String> yAxisLabels = new ArrayList<String>();
        yAxisLabels.add("0");
        for (int i=iter;i<upperBound;i+=iter)
        	yAxisLabels.add(Integer.toString(i));
        yAxisLabels.add(Integer.toString(upperBound));
        
        AxisLabels yAxis = AxisLabelsFactory.newAxisLabels(yAxisLabels);
        
        
        
		
        BarChartPlot clicks = Plots.newBarChartPlot(scaledClicks, Color.newColor("78B4FF") , "Clicks" );
        BarChartPlot conversions = Plots.newBarChartPlot(scaledConversions, Color.newColor("E01B1B") , "Conversions" );
        
        BarChart contentChart = GCharts.newBarChart(clicks, conversions);
        
        contentChart.addXAxisLabels(yAxis);
        contentChart.addYAxisLabels(xAxis);
        
        contentChart.setGrid(25, 25, 3, 2);

        contentChart.setHorizontal(true);
        
        contentChart.setTitle("Content Element Clicks and Conversions", Color.BLACK, 16);

        contentChart.setBackgroundFill(Fills.newSolidFill(Color.newColor("FFFFFF")));

        contentChart.setBarWidth(20);
        contentChart.setSpaceBetweenGroupsOfBars(20);
        Fill fill =  Fills.newSolidFill(Color.newColor("F7F9FA"));
        contentChart.setAreaFill(fill);
        
        
        contentChart.setSize  (  450, (data.size()*40+260 <660) ?  data.size()*40+160 : 660);
		
		return contentChart.toURLString();
		
	}
	
	/**
	 * Makes the bar chart that shows which destinations were visited from your guide
	 */
	public static String destinationsBar (ArrayList<DataListRow> rows){
		
		
		 ArrayList<Double> data = new ArrayList<Double>();
		 ArrayList<String> labels = new ArrayList<String>();
		 String series = "Destinations Visited from Your Guide";
		
		for (DataListRow r: rows){
			labels.add(r.getFirst());
			data.add(Double.parseDouble(r.getSecond()));
		}
		
		return sceneBarChart(data, labels, series );
		
	}

	/**
	 * Finds insersion index so the arraylist containing the data for content element bar
	 * can be assembled already sorted
	 */
	public static int insertionIndex (ArrayList<Double> data, double point)
	{
		for (int i=0;i<data.size();i++)
			if ( point > data.get(i))
				return i;
		return data.size();
	}
	
	/**
	 * Calculates and returns the upper limit of the graph and the iterator to be used in a label creation loop
	 * 
	 */
	public static int[] getBounds (ArrayList<Double> data)
	{
		//find max
		double max = 1;
		for (int i=0; i<data.size(); i++)
			if (data.get(i)>max)
				max = data.get(i);
		int iter =0 ;

		//dynamically create scale of data series
		if (max > 12 ){

			// this block divides by 4 and then rounds up to the nearest multiple of 5
			if ( (int)Math.ceil(max/4.0) % 5 == 0 )
				iter =   (int) Math.ceil(max/4.0);
			else
				iter = (int) Math.ceil(max/4.0) - ( (int) Math.ceil(max/4.0)  % 5  ) + 5 ;

			return new int[] { iter, iter*4  } ;
		}
		
		if (max > 8 )
			return new int[] { 3, 12 } ; 
		
		if (max > 4 && max <= 8)
			return new int[] { 2, 8 } ;
		
		return new int[] {1, 4 };
	}
	
	
}
