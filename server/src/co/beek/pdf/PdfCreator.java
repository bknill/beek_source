package co.beek.pdf;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;

import co.beek.Constants;
import co.beek.pano.model.dao.entities.Guide;
import co.beek.pano.service.dataService.guideService.GuideService;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class PdfCreator {

	// constants I made to help position elements
	static float topHeader = 60f;
	static float margin = 36f;

	static float sparkchartWidth = 115f;
	static float sparkchartHeight = 103f;

	static float padding = 6f;

	static float locationPieWidth = 266f;
	static float locationPieHeight = 133f;

	static float barWidth = 417f;
	static float barHeight = 125;

	private static Document document;

	/**
	 * saves pdf report to server/pdfs
	 * 
	 * @param id
	 * @param title
	 * @param reportCreator
	 * @throws DocumentException
	 * @throws IOException
	
	public static void createPdf(String id, String title,
			AndrewsGuideReportController reportCreator,
			GuideService guideService) throws DocumentException, IOException {

		Guide guide = reportCreator.getGuide();
		guide.setReportIncrement(guide.getReportIncrement() + 1);
		String fileName = "guide_" + id + "_report_"
				+ guide.getReportIncrement() + ".pdf";
		String filePath = Constants.TEMP_DIR_PATH + fileName;

		document = new Document();

		PdfWriter writer = PdfWriter.getInstance(document,
				new FileOutputStream(filePath));

		document.open();

		// creates the 4 line charts
		ArrayList<Image> images = new ArrayList<Image>();
		for (int i = 1; i < 5; i++) {
			String url = reportCreator.getGoogleLineChart(i, true);
			Image image = Image.getInstance(new URL(url));
			image.scaleAbsolute(sparkchartWidth, sparkchartHeight);
			images.add(image);
		}

		// referrals pie chart
		images.add(Image.getInstance(new URL(reportCreator.getReferralPie())));
		images.get(4).scaleAbsolute(500, (float) (500f / 3.0));

		// regions of new zealand pie chart
		images.add(Image.getInstance(new URL(reportCreator.getRegionPie())));
		images.get(5).scaleAbsolute(locationPieWidth, locationPieHeight);

		// country pie chart
		images.add(Image.getInstance(new URL(reportCreator.getCountryPie())));
		images.get(6).scaleAbsolute(locationPieWidth, locationPieHeight);

		// sets the position of the 4 line charts
		images.get(1).setAbsolutePosition(
				document.getPageSize().getWidth() - margin * 2
						- sparkchartWidth,
				document.getPageSize().getHeight() - topHeader - margin * 2
						- sparkchartHeight);
		images.get(0).setAbsolutePosition(
				document.getPageSize().getWidth() - margin * 3
						- sparkchartWidth * 2,
				document.getPageSize().getHeight() - topHeader - margin * 2
						- sparkchartHeight);
		images.get(2).setAbsolutePosition(
				document.getPageSize().getWidth() - margin * 2
						- sparkchartWidth,
				document.getPageSize().getHeight() - topHeader - margin * 3
						- sparkchartHeight * 2);
		images.get(3).setAbsolutePosition(
				document.getPageSize().getWidth() - margin * 3
						- sparkchartWidth * 2,
				document.getPageSize().getHeight() - topHeader - margin * 3
						- sparkchartHeight * 2);

		// sets the position of the pie charts
		images.get(4).setAbsolutePosition(
				document.getPageSize().getWidth() - margin * 2
						- sparkchartWidth * 4,
				document.getPageSize().getHeight() - topHeader - margin * 5
						- sparkchartHeight * 2 - locationPieHeight);
		images.get(5).setAbsolutePosition(
				document.getPageSize().getWidth() - locationPieWidth - margin,
				document.getPageSize().getHeight() - topHeader - margin * 6.5f
						- sparkchartHeight * 2 - locationPieHeight * 2);
		images.get(6).setAbsolutePosition(
				0,
				document.getPageSize().getHeight() - topHeader - margin * 6.5f
						- sparkchartHeight * 2 - locationPieHeight * 2);

		// 4 sparkcharts
		document.add(images.get(0));
		document.add(images.get(1));
		document.add(images.get(2));
		document.add(images.get(3));

		// 3 pie charts
		document.add(images.get(4));
		document.add(images.get(5));
		document.add(images.get(6));

		// establishes text writer and fonts
		PdfContentByte textLayer = writer.getDirectContent();
		textLayer.beginText();
		Font helvetica = new Font(FontFamily.HELVETICA, 12);
		Font bold = new Font(FontFamily.HELVETICA, 12, Font.BOLD);
		bold.setColor(0, 76, 171);
		BaseFont bf_helv = helvetica.getCalculatedBaseFont(false);

		// header text
		Font header = new Font(FontFamily.HELVETICA, 9);
		Font headerBold = new Font(FontFamily.HELVETICA, 9, Font.BOLD);
		Paragraph p = new Paragraph();
		p.add(new Phrase("Here is your guide's report. The ", header));
		p.add(new Phrase("performance overview ", headerBold));
		// (by hitting the \"book now\" or \"more info\" buttons for example)
		p.add(new Phrase(
				"relates to your guide's visitors: how long they spend on the guide,"
						+ " the number of scenes they view and how often they convert. "
						+ "The charts refer to how these metrics have been tracking over time. The ",
				header));
		p.add(new Phrase("referrals overview ", headerBold));
		p.add(new Phrase(
				"highlights which websites link to your guide and how much traffic they generate. The ",
				header));
		p.add(new Phrase("demographics ", headerBold));
		p.add(new Phrase(
				"shows the locations of your international and domestic visitors.",
				header));
		p.setAlignment(Element.ALIGN_LEFT);
		p.setLeading(10f);
		document.add(p);

		// headers above the 4 line charts
		textLayer.setFontAndSize(bf_helv, 10);
		textLayer.showTextAligned(Element.ALIGN_LEFT, "Conversions", document
				.getPageSize().getWidth() - margin * 2 - sparkchartWidth,
				document.getPageSize().getHeight() - topHeader - margin * 2
						+ padding, 0);
		textLayer.showTextAligned(Element.ALIGN_LEFT, "Visits", document
				.getPageSize().getWidth() - margin * 3 - sparkchartWidth * 2,
				document.getPageSize().getHeight() - topHeader - margin * 2
						+ padding, 0);
		textLayer.showTextAligned(Element.ALIGN_LEFT, "Time", document
				.getPageSize().getWidth() - margin * 2 - sparkchartWidth,
				document.getPageSize().getHeight() - topHeader - margin * 3
						- sparkchartHeight + padding, 0);
		textLayer.showTextAligned(Element.ALIGN_LEFT, "Scenes", document
				.getPageSize().getWidth() - margin * 3 - sparkchartWidth * 2,
				document.getPageSize().getHeight() - topHeader - margin * 3
						- sparkchartHeight + padding, 0);

		// headers above the two bottom pie charts
		textLayer
				.showTextAligned(Element.ALIGN_CENTER, "Domestic", document
						.getPageSize().getWidth()
						- locationPieWidth
						* .5f
						- margin, document.getPageSize().getHeight()
						- topHeader - margin * 6.5f - sparkchartHeight * 2
						- locationPieHeight + padding, 0);
		textLayer.showTextAligned(Element.ALIGN_CENTER, "International",
				locationPieWidth * .5f, document.getPageSize().getHeight()
						- topHeader - margin * 6.5f - sparkchartHeight * 2
						- locationPieHeight + padding, 0);

		// x-axis labels for the 4 line charts
		textLayer.setFontAndSize(bf_helv, 8);
		textLayer.showTextAligned(Element.ALIGN_CENTER, "Week",
				document.getPageSize().getWidth() - margin * 3
						- sparkchartWidth * 1.5f, document.getPageSize()
						.getHeight()
						- topHeader
						- margin
						* 2
						- sparkchartHeight - padding, 0);
		textLayer.showTextAligned(Element.ALIGN_CENTER, "Week",
				document.getPageSize().getWidth() - margin * 3
						- sparkchartWidth * 1.5f, document.getPageSize()
						.getHeight()
						- topHeader
						- margin
						* 3
						- sparkchartHeight * 2 - padding, 0);
		textLayer.showTextAligned(Element.ALIGN_CENTER, "Week", document
				.getPageSize().getWidth() - margin * 2 - sparkchartWidth * .5f,
				document.getPageSize().getHeight() - topHeader - margin * 3
						- sparkchartHeight * 2 - padding, 0);
		textLayer.showTextAligned(Element.ALIGN_CENTER, "Week", document
				.getPageSize().getWidth() - margin * 2 - sparkchartWidth * .5f,
				document.getPageSize().getHeight() - topHeader - margin * 2
						- sparkchartHeight - padding, 0);

		// main header
		textLayer.setFontAndSize(bold.getCalculatedBaseFont(false), 12);
		textLayer.showTextAligned(Element.ALIGN_LEFT, "Guide " + id + " - "
				+ title, margin, document.getPageSize().getHeight() - margin
				+ padding, 0);
		textLayer.showTextAligned(Element.ALIGN_RIGHT, getMonth(), document
				.getPageSize().getWidth() - margin, document.getPageSize()
				.getHeight() - margin + padding, 0);

		// sub headers
		textLayer.setFontAndSize(bold.getCalculatedBaseFont(false), 10);
		textLayer.showTextAligned(Element.ALIGN_LEFT, "Performance Overview",
				margin, document.getPageSize().getHeight() - topHeader - margin
						+ padding, 0);
		textLayer.showTextAligned(Element.ALIGN_LEFT, "Referrals Overview",
				margin, document.getPageSize().getHeight() - topHeader - margin
						* 3.5f - sparkchartHeight * 2 + padding, 0);
		textLayer.showTextAligned(Element.ALIGN_LEFT, "Demographics Overview",
				margin, document.getPageSize().getHeight() - topHeader - margin
						* 5.5f - sparkchartHeight * 2 - locationPieHeight
						+ padding, 0);

		// basic facts table
		PdfPTable guideTable = guideBasicTable(reportCreator);
		float guideTableHeight = guideTable.calculateHeights();
		guideTable.writeSelectedRows(0, 10, margin, document.getPageSize()
				.getHeight() - topHeader - 2 * margin + 18f, textLayer);

		// shares table
		sharesTable(reportCreator).writeSelectedRows(
				0,
				10,
				margin,
				document.getPageSize().getHeight() - topHeader - 3 * margin
						- guideTableHeight - padding - 3f, textLayer);

		textLayer.endText();

		PdfContentByte lines = writer.getDirectContent();

		// under performance overview heading
		horizontalLine(lines, document.getPageSize().getHeight() - topHeader
				- margin, .8f);
		// under referrals overview heading
		horizontalLine(lines, document.getPageSize().getHeight() - topHeader
				- margin * 3.5f - sparkchartHeight * 2, .8f);
		// under demographics heading
		horizontalLine(lines, document.getPageSize().getHeight() - topHeader
				- margin * 5.5f - sparkchartHeight * 2 - locationPieHeight, .8f);

		lines.stroke();

		document.newPage();
		// next page

		// create array of bar charts
		ArrayList<Image> barCharts = new ArrayList<Image>();
		for (int i = 1; i < 4; i++) {
			String url = reportCreator.getSceneChart(i);
			Image image = Image.getInstance(new URL(url));
			image.scaleAbsolute(barWidth, barHeight);
			barCharts.add(image);
		}

		Image destinations = Image.getInstance(new URL(reportCreator
				.getDestinationsBarChart(false)));
		destinations.scaleAbsolute(barWidth, barHeight);
		barCharts.add(destinations);

		// sets the position of the 3 bar charts
		barCharts.get(0).setAbsolutePosition(margin,
				document.getPageSize().getHeight() - barHeight - 2 * margin);
		barCharts.get(1)
				.setAbsolutePosition(
						margin,
						document.getPageSize().getHeight() - barHeight * 2 - 4
								* margin);
		barCharts.get(2)
				.setAbsolutePosition(
						margin,
						document.getPageSize().getHeight() - barHeight * 3 - 6
								* margin);
		barCharts.get(3).setAbsolutePosition(
				margin,
				document.getPageSize().getHeight() - barHeight * 4 - 7.5f
						* margin);

		// adds the bar charts
		document.add(barCharts.get(0));
		document.add(barCharts.get(1));
		document.add(barCharts.get(2));
		document.add(barCharts.get(3));

		// adds the headers for the bar charts
		PdfContentByte textLayer2 = writer.getDirectContent();
		textLayer2.beginText();
		textLayer.setFontAndSize(bf_helv, 10);
		textLayer2
				.showTextAligned(Element.ALIGN_LEFT, "Visits", margin, document
						.getPageSize().getHeight() - 1.5f * margin + padding, 0);
		textLayer2.showTextAligned(Element.ALIGN_LEFT, "Times", margin,
				document.getPageSize().getHeight() - barHeight * 1 - 3.5f
						* margin + padding, 0);
		textLayer2.showTextAligned(Element.ALIGN_LEFT, "Conversions", margin,
				document.getPageSize().getHeight() - barHeight * 2 - 5.5f
						* margin + padding, 0);
		textLayer2
				.showTextAligned(Element.ALIGN_LEFT, "Other Locations Viewed",
						margin, document.getPageSize().getHeight() - barHeight
								* 3 - 7f * margin + padding, 0);

		// main header for the second page
		textLayer2.setFontAndSize(bold.getCalculatedBaseFont(false), 12);
		textLayer2.showTextAligned(Element.ALIGN_LEFT, "Scene Performance",
				margin, document.getPageSize().getHeight() - margin + padding,
				0);

		textLayer2.endText();

		PdfContentByte lines2 = writer.getDirectContent();

		// under visits heading
		horizontalLine(lines2, document.getPageSize().getHeight() - 1.5f
				* margin, .8f);
		// under times heading
		horizontalLine(lines2, document.getPageSize().getHeight() - barHeight
				* 1 - 3.5f * margin, .8f);
		// under conversions heading
		horizontalLine(lines2, document.getPageSize().getHeight() - barHeight
				* 2 - 5.5f * margin, .8f);
		// under destinations visited heading
		horizontalLine(lines2, document.getPageSize().getHeight() - barHeight
				* 3 - 7f * margin, .8f);

		lines2.stroke();

		document.close();

		// We have incremented the guide report increment
		// at the start of the create process
		guideService.updateGuide(guide);

		// TODO upload to s3
		// upload thumb to the scenes bucket
		// s3UploadService.uploadFile(assetsBucket, new File(filename));

	} */

	/**
	 * Makes a horizontal line across the page from margin to margin
	 */
	private static void horizontalLine(PdfContentByte lines, float height,
			float width) {

		lines.setColorStroke(BaseColor.GRAY);
		lines.setLineWidth(width);
		lines.moveTo(document.getPageSize().getWidth() - margin, height);
		lines.lineTo(margin, height);

	}

	/**
	 * Makes the table that displays the views, time, and scenes private static
	 * PdfPTable guideBasicTable (AndrewsGuideReportController reportCreator){
	 * ArrayList<DataListRow> dataList = (ArrayList<DataListRow>)
	 * reportCreator.getGuideBasicList(); ArrayList<String> labels = new
	 * ArrayList<String>(); ArrayList<String> numbers = new ArrayList<String>();
	 * 
	 * 
	 * String title = null; for (int i=0;i<dataList.size();i++){ if (i==0) title
	 * = dataList.get(i).getSecond(); else{
	 * labels.add(dataList.get(i).getFirst());
	 * numbers.add(dataList.get(i).getSecond()); } } return makeTable (title,
	 * labels, numbers); }
	 */

	/**
	 * Makes the table that shows likes, facebook shares, web shares, etc.
	 * private static PdfPTable sharesTable (AndrewsGuideReportController
	 * reportCreator){ ArrayList<DataListRow> dataList =
	 * (ArrayList<DataListRow>) reportCreator.getSharesList(); ArrayList<String>
	 * labels = new ArrayList<String>(); ArrayList<String> numbers = new
	 * ArrayList<String>();
	 * 
	 * String title = "Social Media"; for (int i=0;i<dataList.size();i++){
	 * labels.add(dataList.get(i).getFirst());
	 * numbers.add(dataList.get(i).getSecond()); }
	 * 
	 * return makeTable (title, labels, numbers);
	 * 
	 * }
	 */

	/**
	 * Makes a two column table given its elements. Used by guideBasicTable and
	 * sharesTable
	 */
	private static PdfPTable makeTable(String title, ArrayList<String> labels,
			ArrayList<String> numbers) {
		PdfPTable table = new PdfPTable(2);

		table.addCell(makeTitleCell(title));

		if (!(labels.size() == numbers.size()))
			throw new Error("Different amounts of labels and numbers");

		for (int i = 0; i < labels.size(); i++) {
			if (i == 0) // first row, top cell padding is needed
			{
				table.addCell(makeLabelCell(labels.get(i), true));
				table.addCell(makeNumberCell(numbers.get(i), true));
			}

			else {
				table.addCell(makeLabelCell(labels.get(i)));
				table.addCell(makeNumberCell(numbers.get(i)));
			}
		}

		table.setTotalWidth(150);
		return table;
	}

	/**
	 * Makes a table cell that contains a number for make table
	 */
	private static PdfPCell makeNumberCell(String text, boolean topPadding) {

		Font colored = new Font(FontFamily.HELVETICA, 9);
		colored.setColor(0, 76, 171);
		Phrase p = new Phrase(text, colored);

		PdfPCell cell = new PdfPCell(p);
		cell.setBorder(Rectangle.NO_BORDER);

		if (topPadding)
			cell.setPaddingTop(5f);

		return cell;

	}

	// overloaded so if you dont specificy top padding it defaults to false
	private static PdfPCell makeNumberCell(String text) {
		return makeNumberCell(text, false);
	}

	/**
	 * Makes a cell that has text for the make table function
	 */
	private static PdfPCell makeLabelCell(String text, boolean topPadding) {
		Font colored = new Font(FontFamily.HELVETICA, 9);

		colored.setColor(128, 128, 128);
		Phrase p = new Phrase(text, colored);

		PdfPCell cell = new PdfPCell(p);
		cell.setBorder(Rectangle.NO_BORDER);

		if (topPadding)
			cell.setPaddingTop(5f);

		return cell;

	}

	// overloaded so if you dont specificy top padding it defaults to false
	private static PdfPCell makeLabelCell(String text) {
		return makeLabelCell(text, false);
	}

	/**
	 * makes a title cell for make table function
	 * 
	 * @param text
	 * @return
	 */
	private static PdfPCell makeTitleCell(String text) {
		Font colored = new Font(FontFamily.HELVETICA, 10);

		colored.setColor(128, 128, 128);
		Phrase p = new Phrase(text, colored);
		PdfPCell cell = new PdfPCell(p);
		cell.setColspan(2);
		cell.setPaddingBottom(10f);
		cell.setBorder(Rectangle.BOTTOM);

		return cell;

	}

	/**
	 * Gets current month and year ex. "March 2012" For top right corner of
	 * report
	 */
	public static String getMonth() {
		String[] monthName = { "January", "February", "March", "April", "May",
				"June", "July", "August", "September", "October", "November",
				"December" };

		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		return monthName[cal.get(Calendar.MONTH)] + " " + year;
	}

}
