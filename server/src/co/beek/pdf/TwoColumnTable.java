package co.beek.pdf;


public class TwoColumnTable extends BeekTable {
	public TwoColumnTable(String heading) {
		super(2);
		addCell(makeTitleCell(heading));
	}


	public void addRow(String col1, int col2) {
		addCell(makeLabelCell(col1));
		addCell(makeNumberCell(Integer.toString(col2)));
	}

	public void addRow(String col1, String col2) {
		addCell(makeLabelCell(col1));
		addCell(makeNumberCell(col2));
	}
}
