package co.beek.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import co.beek.Constants;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;

public class BeekDocument extends com.itextpdf.text.Document {

	private File file;

	public BeekDocument(String filename) throws FileNotFoundException,
			DocumentException {

		String tempFileName = Constants.TEMP_DIR_PATH + filename;

		PdfWriter.getInstance(this, new FileOutputStream(
				tempFileName));

		file = new File(tempFileName);
	}

	public void addImage(String url) throws BadElementException,
			MalformedURLException, DocumentException, IOException {
		add(Image.getInstance(new URL(url)));
	}

	public File getFile() {
		return file;
	}
}
