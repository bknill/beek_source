package co.beek.util;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.exception.ExceptionUtils;

import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.enterprizewizard.EWPhotoShoot;
import co.beek.pano.model.dao.enterprizewizard.LoginResponse;
import co.beek.pano.model.dao.entities.Destination;
import co.beek.pano.service.dataService.enterprizewizardService.EWService;
import co.beek.pano.service.dataService.enterprizewizardService.EWServiceImpl;
import flexjson.JSON;

public class BeekSession {

	private HttpSession session;


	public BeekSession(HttpSession session) {
		this.session = session;
	}

	public boolean isLoggedIn() {
		return getUser() != null;
	}
	
	public void setLoginRedirect(String loginRedirect) {
		session.setAttribute("loginRedirect", loginRedirect);
	}

	public String getLoginRedirect() {
		return (String) session.getAttribute("loginRedirect");
	}


	public String getId() {
		return session.getId();
	}

	public void setUser(EWContact user) {
		session.setAttribute("user", user);
	}

	public EWContact getUser() {
		return (EWContact) session.getAttribute("user");
	}

	public String getUsername() {
		return (String) session.getAttribute("username");
	}

	public void setUsername(String username) {
		session.setAttribute("username", username);
	}

	public String getPassword() {
		return (String) session.getAttribute("password");
	}

	public void setPassword(String password) {
		session.setAttribute("password", password);
	}
	
	public String getLat() {
		return (String) session.getAttribute("lat");
	}
	
	public void setLat(String lat) {
		session.setAttribute("lat", lat);
	}
	
	public String getLon() {
		return (String) session.getAttribute("lon");
	}
	
	public void setLon(String lon) {
		session.setAttribute("lon", lon);
	}


	@SuppressWarnings("unchecked")
	public List<String> getGuideID() {
		return (List<String>) session.getAttribute("guideid");
	}

	public void setGuideID(List<String> guideid) {
		session.setAttribute("guideid", guideid);
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getLocationID() {
		return (List<String>) session.getAttribute("locationid");
	}

	public void setLocationID(List<String> locationid) {
		session.setAttribute("locationid", locationid);
	}
	
	@SuppressWarnings("unchecked")
	public String getType() {
		return (String) session.getAttribute("type");
	}

	public void setType(String type) {
		session.setAttribute("type", type);
	}
	
	
	
	public void setPhotoShoots(List<EWPhotoShoot> shoots) {
		session.setAttribute("shoots", shoots);
	}

	@SuppressWarnings("unchecked")
	public List<EWPhotoShoot> getPhotoShoots() {
		return (List<EWPhotoShoot>) session.getAttribute("shoots");
	}

	public void setDestinations(List<Destination> destinations) {
		session.setAttribute("destinations", destinations);
	}

	@SuppressWarnings("unchecked")
	public List<Destination> getDestinations() {
		return (List<Destination>) session.getAttribute("destinations");
	}

	public Destination getDestination(String id) {
		List<Destination> destinations = getDestinations();
		for (Destination d : destinations)
			if (d.getId().equals(id))
				return d;
		return null;
	}

	@JSON(include = false)
	public boolean isInDestination(String destinationTitle) {
		if (destinationTitle == null)
			return false;

		List<Destination> destinations = getDestinations();
		for (Destination destination : destinations)
			if (destination.getTitle().toLowerCase()
					.equals(destinationTitle.toLowerCase()))
				return true;

		return false;
	}
	
	protected void showError(String message) {
		System.out.println(message);
		FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR,
				"Error!", message);
		FacesContext.getCurrentInstance().addMessage(null, m);
	}

	public void logout() {
		
		session.setAttribute("user", null);
		session.setAttribute("username", null);
		session.setAttribute("password", null);
		session.invalidate();
	}
	
	
	public void sendError(String subject, Throwable e) {
	
		showError(subject);
		String message = ExceptionUtils.getStackTrace(e);
		FacesMessage m2 = new FacesMessage(FacesMessage.SEVERITY_ERROR,
				message, message);
		FacesContext.getCurrentInstance().addMessage(null, m2);
		
	}

}

/*
 * ----------------------------------------------------------
List<String> guideid = getGuideID();
String updateGuideID = guideid.get(guideid.size()-1);

try {
	LoginResponse loginResponse; //ewservice is not initializing
	//loginResponse = 
	ewServiceImpl.updateGuideID(getUsername(), getPassword(), getUser().id, getGuideID());
	
	
	System.out.println("1st");
	FacesMessage m1 = new FacesMessage(FacesMessage.SEVERITY_ERROR,
			"1st", "1st");
	FacesContext.getCurrentInstance().addMessage(null, m1);
	
	//if (loginResponse.getError() != null)
		//showError(loginResponse.getError());
	
} catch (Exception e) {
	sendError("Hit the exception", e);
	FacesMessage m2 = new FacesMessage(FacesMessage.SEVERITY_ERROR,
			"Hit exception", "Hit exception");
	FacesContext.getCurrentInstance().addMessage(null, m2);
}
*/







