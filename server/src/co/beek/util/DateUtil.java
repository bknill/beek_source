package co.beek.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	public static final long MONTH_MILLIS = 2592000000L;
	public static final long DAY_MILLIS = 86400000L;
	public static final long HOUR_MILLIS = 3600000L;
	public static final long TEN_SECONDS_MILLIS = 10000L;
	public static final long MINUTE_MILLIS = 60000L;
	public static final long FIVE_MINUTE_MILLIS = 300000L;

	public static String GA_START_DATE = "2012-02-01";

	/**
	 * Gets the current date in google analytics readable format
	 */
	public static String getGAStartDate() {
		return "2012-02-01";
	}

	public static String getCurrentDate() {
		return formatDate(new Date());
	}

	public static String formatDate(Date date) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		return dateFormat.format(date);
	}

	/**
	 * Gets one month previous to the current date in ga format
	 * 
	 * @return
	 */
	public static String getLastMonth() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -1);
		return dateFormat.format(calendar.getTime());
	}

	public static Date aMinuteAgo() {
		return new Date(System.currentTimeMillis() - MINUTE_MILLIS);
	}

	public static Date tenMinutesAgo() {
		return new Date(System.currentTimeMillis() - (MINUTE_MILLIS * 10));
	}

	public static Date anHourAgo() {
		return new Date(System.currentTimeMillis() - HOUR_MILLIS);
	}

	public static Date aMonthAgo() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -1);
		return calendar.getTime();
	}

	public static Date getHourAgo() {
		return new Date(System.currentTimeMillis() - HOUR_MILLIS);
	}

	public static String convertToMinutes(String time) {
		double minutes = stringToDouble(time) / 60.0;
		return formatNumber(Double.toString(minutes));
	}
	
    public static String timeConversion(int totalSeconds) {

        final int MINUTES_IN_AN_HOUR = 60;
        final int SECONDS_IN_A_MINUTE = 60;

        int seconds = totalSeconds % SECONDS_IN_A_MINUTE;
        int totalMinutes = totalSeconds / SECONDS_IN_A_MINUTE;
        int minutes = totalMinutes % MINUTES_IN_AN_HOUR;
        int hours = totalMinutes / MINUTES_IN_AN_HOUR;

        if(hours > 0)
        	return hours + " hours " + minutes + " mins " + seconds + " secs";
        else if (minutes > 0)
        	return minutes + " mins " + seconds + " secs";
        else
        	return seconds + " secs";
        	
    }

	public static double stringToDouble(String s) {
		double d = -1;
		try {
			d = Double.valueOf(s.trim()).doubleValue();
		} catch (NumberFormatException e) {
			System.out.println("NumberFormatException: " + e.getMessage());
		}

		return d;
	}

	/**
	 * Makes a number have two figures after the decimal
	 */
	public static String formatNumber(String number) {
		return formatNumber(Double.parseDouble(number));
	}

	public static String formatNumber(Double number) {
		return String.format("%.2f%n", number);
	}

	/**
	 * Shortens url so the legend of the referral table does not overflow
	 * 
	 * @param url
	 * @return
	 */
	public static String shortenURL(String url) {
		String firstPart = url.substring(0, url.indexOf("/"));
		String secondPart = url.substring(url.lastIndexOf("/"));
		url = firstPart + "..." + secondPart;
		return url;
	}
}
