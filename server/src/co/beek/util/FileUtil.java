package co.beek.util;

import java.io.File;

import co.beek.Constants;

public class FileUtil {

	public static File createTempFile(String filename)
	{
		return new File(Constants.TEMP_DIR_PATH+filename);
	}
}
