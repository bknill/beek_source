package co.beek.web;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import co.beek.Constants;
import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.enterprizewizard.EWCustomer;
import co.beek.pano.model.dao.enterprizewizard.EWTeam;
import co.beek.pano.model.dao.entities.Destination;
import co.beek.pano.model.dao.entities.Guide;
import co.beek.pano.model.dao.entities.Location;
import co.beek.pano.service.dataService.destinationService.DestinationService;
import co.beek.pano.service.dataService.guideService.GuideService;
import co.beek.pano.service.dataService.locationService.LocationService;
import co.beek.util.BeekSession;

@Scope("session")
@Controller("AdminDashboardController")
public class AdminDashboardController extends BaseController implements
		Serializable {
	private static final long serialVersionUID = -1081621548447371624L;

	@Inject
	private GuideService guideService;

	@Inject
	private LocationService locationService;

	@Inject
	private DestinationService destinationsService;

	@Value("#{buildProperties.cdnUrl}")
	private String cdnUrl;

	private List<Destination> destinations;

	private String destinationId;
	private List<Guide> destinationGuides;
	public List<Location> destinationLocations;
	private List<EWCustomer> destinationCustomers;
	
	public EWContact user;
	
	public List<Location> allLocations;

	private HashMap<String, String> flashVars = new HashMap<String, String>();

	private String searchText = "";
	

	private String customerId;
	
	
	public void preRenderView() throws IOException {

		if (FacesContext.getCurrentInstance().isPostback())
			return;

		BeekSession session = getSession();
		user = session.getUser();
		if (session.getUser().isAdmin() == false)
			return;
		
		destinations = destinationsService.getAllDestinations();
		allLocations = locationService.getAllLocations();
		destinationLocations = locationService.getAllLocations();


		flashVars.put("domain", Constants.domain);
		flashVars.put("assetCdn", cdnUrl);
		flashVars.put("buildCdn", getBuildCdn());

		//searchText = "";
	}
	/*
	public void getAllLocations() {
		allLocations = locationService.getAllLocations();
	}
	*/

	public List<Destination> getDestinations() {
		return destinations;
	}

	public List<EWTeam> getCustomers() {
		List<EWTeam> customers = new ArrayList<EWTeam>();
		customers.add(new EWTeam("Select Customer"));
		customers.addAll(getUser().getTeams());
		return customers;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) throws IOException {
		this.customerId = customerId;
	}

	public void goToCustomerPage() throws IOException {
		redirect("/customer/" + customerId + "/data");
	}

	public String getMapSwfUrl() {
		return "http://gms."+Constants.domain+"/resources/swf_map.swf?sdf";
	}

	public String getMapFlashVars() {
		List<String> props = new ArrayList<String>();

		Iterator<Entry<String, String>> it = flashVars.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, String> prop = it.next();
			props.add(prop.getKey() + "=" + prop.getValue());
		}
		return StringUtils.join(props, "&");
	}

	public void setDestinationGuides(List<Guide> destinationGuides) {
		this.destinationGuides = destinationGuides;
	}

	public List<Guide> getDestinationGuides() {
		if (searchText != null && searchText.length() >= 1) {
			searchText = searchText.toLowerCase();
			List<Guide> searchResults = new ArrayList<Guide>();
			for (Guide guide : destinationGuides) {
				String title = guide.getTitle();
				if (title != null && title.toLowerCase().contains(searchText))
					searchResults.add(guide);
				
				else if(guide.getId().equals(searchText))
					searchResults.add(guide);
			}
			return searchResults;
		}

		return destinationGuides;
	}

	public void setDestinationLocations(List<Location> destinationLocations) {
		this.destinationLocations = destinationLocations;
	}
	
	public List<Location> getDestinationLocations() {
		return destinationLocations;
	}
	
	public List<Location> getAllLocations() {
		destinationLocations = allLocations;
		return destinationLocations;
	}
	
	public List<String> getAllLats() {
		List<String> allLats = new ArrayList<String>();
		for (int i = 0; i < allLocations.size() -1; i++) 
			{
			allLats.add(""+allLocations.get(i).getLatitude());
			}
		return allLats;
	}
	
	public List<String> getAllLons() {
		List<String> allLons = new ArrayList<String>();
		for (int i = 0; i < allLocations.size() -1; i++) 
			{
			allLons.add(""+allLocations.get(i).getLongitude());
			}
		return allLons;
	}
	
	public List<String> getAllLocationsTitles() {
		List<String> allLocationTitles = new ArrayList<String>();
		for (int i = 0; i < allLocations.size() -1; i++)
		{
			if (allLocations.get(i).getTitle().contains("'"))
			{
			String unencoded = allLocations.get(i).getTitle();
			String encoded = unencoded.replaceAll("'", "%27");
			allLocationTitles.add("'"+encoded+"'");
			}
			else
				allLocationTitles.add("'"+allLocations.get(i).getTitle()+"'");
		}
		return allLocationTitles;
	}
	
	/*
	List<String> allLats = new ArrayList<String>();
	for (int i = 0; i < allLocations.size() -1; i++) 
		{
		allLats.add(""+allLocations.get(i).getLatitude());
		}
	return allLats;
	*/
	/*
	public void printLocationTitles()
	{
		destinationLocations = locationService.getLocationsWithin(0, -180, 90, 180);
		for (int i = 0; i<destinationLocations.size()-1; i++)
		{
			System.out.println(destinationLocations.get(i).getTitle());
		}
	}
	*/

	public void setDestinationId(String destinationId) {
		this.destinationId = destinationId;

		destinationGuides = guideService.getGuidesForDestination(destinationId);
		destinationLocations = locationService
				.getLocationsForDestination(destinationId);

		flashVars.put("destinationId", destinationId);
	}

	public String getGeoCommaDelimted() {
		Destination d = getDestination(destinationId);
		return d.getLatitude() + "," + d.getLongitude() + "," + d.getZoom();
	}

	public String getBuildCdn() {
		return "http://gms."+Constants.domain+"/resources/swf/swf_map.swf";
	}

	public String getDestinationId() {
		return destinationId;
	}

	public Destination getDestination(String destinationId) {
		for (Destination d : destinations)
			if (d.getId().equals(destinationId))
				return d;
		return null;
	}

	public void createDestination() throws IOException {
		redirect("/destination/create");
	}

	public void editDestination() throws IOException {
		redirect("/destination/" + destinationId + "/edit");
	}

	public void createGuide() throws IOException {
		redirect("/guide/create");
	}

	public void editGuide(Guide guide) throws IOException {
		redirect("/guide/" + guide.getId() + "/edit");
	}

	public void viewReport(Guide guide) throws IOException {
		redirect("/guide/" + guide.getId() + "/report");
	}

	public void createLocation() throws IOException {
		redirect("/location/create");
	}

	public void editLocation(Location location) throws IOException {
		redirect("/location/" + location.getId() + "/process");
	}

	public void setDestinationCustomers(List<EWCustomer> destinationCustomers) {
		this.destinationCustomers = destinationCustomers;
	}

	public List<EWCustomer> getDestinationCustomers() {
		return destinationCustomers;
	}

	// public void setCustomerId(String customerId) {
	// this.customerId = customerId;
	// flashVars.put("customerId", customerId);
	//
	// if (customerId == null) {
	// customerGuides = destinationGuides;
	// customerLocations = destinationLocations;
	// } else {
	// customerGuides = new ArrayList<Guide>();
	// for (Guide g : destinationGuides)
	// if (g.getTeamId().equals(customerId))
	// customerGuides.add(g);
	//
	// customerLocations = new ArrayList<Location>();
	// for (Location l : destinationLocations)
	// if (l.getTeamId().equals(customerId))
	// customerLocations.add(l);
	// }
	//
	// Collections.sort(customerGuides, Guide.scoreComparator);
	// }

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public String getSearchText() {
		return searchText;
	}
}
