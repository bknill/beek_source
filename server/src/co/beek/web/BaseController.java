package co.beek.web;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.exception.ExceptionUtils;

import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.entities.Destination;
import co.beek.pano.service.dataService.MailService;
import co.beek.util.BeekSession;

/**
 * Utility class that just makes common functions a lot easier.
 * 
 * @author Daniel
 * 
 */
public class BaseController implements Serializable {
	private static final long serialVersionUID = -1081621548447371624L;

	@Inject
	MailService emailService;

	public boolean isLoggedIn() {
		return getSession().getUser() != null;
	}

	protected boolean redirectingToLogin() throws IOException {
		return redirectingToLogin(getRequest());
	}

	protected boolean redirectingToLogin(HttpServletRequest request)
			throws IOException {
		if (!isLoggedIn(request)) {
			redirectLogin(request);
			return true;
		}
		return false;
	}

	protected boolean isLoggedIn(HttpServletRequest request) {
		return (getSession(request).getUser() != null);
	}

	public boolean isPostback() {
		return FacesContext.getCurrentInstance().isPostback();
	}

	protected void redirectHome() throws IOException {
		redirect("/");
	}
	
	protected void redirectLocationDashboard() throws IOException {
		redirect("/location/dashboard");
	}
	
	protected void redirectLocationCreate() throws IOException {
		redirect("/location/create");
	}
	
	protected void redirectGuideDashboard() throws IOException {
		redirect("/guide/dashboard");
	}
	
	protected void redirectGuideEdit(String guideid) throws IOException {
		redirect("/guide/" + guideid + "/admin");
	}
	
	protected void redirectLocationEdit(String locationid) throws IOException {
		redirect("/location/" + locationid + "/edit");
	}
	
	public void redirectLeadEmail(String leadid) throws IOException {
		redirect("/lead/" + leadid + "/email");
	}
	
	public void redirectLeadEdit(String leadid) throws IOException {
		redirect("/lead/" + leadid + "/edit");
	}
	
	public void redirectCampaignEmail(String campaignId) throws IOException {
		redirect("/campaign/" + campaignId + "/email");
	}
	
	
	protected void redirectCreateLead() throws IOException {
		redirect("/lead/create");
	}
	
	protected void redirectLeadList() throws IOException {
		redirect("/lead/list");
	}

	public void redirectToGuideEdit(String id) throws IOException {
		redirect("/guide/" +id+ "/admin");
	}
	
	public void redirectToGuideCreate() throws IOException {
		redirect("/guide/create");
	}
	
	
	protected void redirectLogin(HttpServletRequest request) throws IOException {
		String req = request.toString();
		String path = req.substring(req.indexOf('/'), req.indexOf(')'));
		getSession(request).setLoginRedirect(path);
		redirectHome();
	}

	protected void redirect(String path) throws IOException {
		FacesContext.getCurrentInstance().getExternalContext().redirect(path);
	}

	protected HttpServletRequest getRequest() {
		return (HttpServletRequest) FacesContext.getCurrentInstance()
				.getExternalContext().getRequest();
	}

	public BeekSession getSession() {
		return new BeekSession((HttpSession) FacesContext.getCurrentInstance()
				.getExternalContext().getSession(false));
	}

	public EWContact getUser() {
		return getSession().getUser();
	}


	protected BeekSession getSession(HttpSession session) {
		return new BeekSession(session);
	}

	protected BeekSession getSession(HttpServletRequest request) {
		return new BeekSession(request.getSession());
	}

	protected void showMessage(String title, String message) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(title, message));
	}

	protected void showError(String message) {
		System.out.println(message);
		FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR,
				"Error!", message);
		FacesContext.getCurrentInstance().addMessage(null, m);
	}

	protected void sendErrorDan(String subject, String message) {
		showError(subject);
		emailService.emailError(subject, message);
	}

	protected void sendErrorDan(String subject, Throwable e) {
		showError(subject);
		String message = ExceptionUtils.getStackTrace(e);
		emailService.emailError(subject, message);
	}

	protected void sendErrorBen(String message) {
		showError(message);
		//emailService.sendEmail("ben@beek.co", "Error on server!", message);
	}
}
