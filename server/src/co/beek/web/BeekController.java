package co.beek.web;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.apache.commons.io.IOUtils;
import org.jets3t.service.S3ServiceException;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import co.beek.Constants;
import co.beek.pano.model.dao.entities.Beek;
import co.beek.pano.service.dataService.BeekService;
import co.beek.pano.service.dataService.uploadService.S3UploadService;

@Scope("session")
@Controller("BeekController")
public class BeekController extends BaseController implements Serializable {
	private static final long serialVersionUID = -1081621548447371624L;

	@RequestMapping(value = "/beek", method = RequestMethod.GET)
	public String mapView() {
		return "/views/beek.jsf";
	}

	@Value("#{buildProperties.assetsBucket}")
	private String assetsBucket;

	@Inject
	private BeekService beekService;

	@Inject
	private S3UploadService s3UploadService;

	private Beek beek;

	private UploadedFile uploadedPlayer;

	private UploadedFile uploadedAdmin;

	public void preRenderView() throws Exception {
		if (isPostback())
			return;

		if (redirectingToLogin())
			return;

		beek = beekService.getBeek();
	}

	public Beek getBeek() {
		return beek;
	}

	/**
	 * Can only upload on beektest
	 */
	public Boolean getUploadable() {
		return Constants.domain.indexOf("beektest") > -1;
	}

	public UploadedFile getPlayerFile() {
		return uploadedPlayer;
	}

	public void setPlayerFile(UploadedFile file) {
		this.uploadedPlayer = file;
	}

	public UploadedFile getAdminFile() {
		return uploadedAdmin;
	}

	public void setAdminFile(UploadedFile file) {
		this.uploadedAdmin = file;
	}

	public void upload() throws IOException, NoSuchAlgorithmException,
			S3ServiceException {

		if (uploadedPlayer != null && !uploadedPlayer.getFileName().equals("")
				&& uploadedPlayer.getFileName().indexOf("swf_player") > -1) {
			beek.incrementPlayer();
			upload(uploadedPlayer, beek.getPlayerFileName());
			FacesMessage msg = new FacesMessage("Succesful",
					"player is uploaded.");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}

		if (uploadedAdmin != null && !uploadedAdmin.getFileName().equals("")
				&& uploadedAdmin.getFileName().indexOf("swf_admin") > -1) {
			beek.incrementAdmin();
			upload(uploadedAdmin, beek.getAdminFileName());
			FacesMessage msg = new FacesMessage("Succesful",
					"admin is uploaded.");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}

		beekService.updateBeek(beek);
	}

	public void update(){
		beekService.updateBeek(beek);
	}

	private void upload(UploadedFile upload, String fileName)
			throws IOException, NoSuchAlgorithmException, S3ServiceException {
		File temp = new File(Constants.TEMP_DIR_PATH + fileName);

		InputStream input = upload.getInputstream();
		FileOutputStream output = new FileOutputStream(temp);

		try {
			IOUtils.copy(input, output);
		} finally {
			IOUtils.closeQuietly(input);
			IOUtils.closeQuietly(output);
		}

		s3UploadService.uploadFile(assetsBucket, temp);

		temp.delete();
	}
}
