package co.beek.web;

import java.io.IOException;
import java.io.Serializable;
import java.text.NumberFormat;
import java.util.*;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import co.beek.pano.model.beans.DataTable;
import co.beek.pano.model.dao.entities.*;
import co.beek.pano.service.dataService.enterprizewizardService.EWService;
import co.beek.pano.service.dataService.googleAnalytics.BasicData;
import co.beek.util.DateUtil;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.enterprizewizard.EWTeam;
import co.beek.pano.service.dataService.guideService.GuideService;
import co.beek.pano.service.dataService.locationService.LocationService;
import co.beek.pano.service.dataService.sceneService.SceneService;
import co.beek.util.BeekSession;

@Scope("session")
@Controller("CustomerDashboardController")
public class CustomerDashboardController extends BaseController implements
		Serializable {
	private static final long serialVersionUID = -1081621548447371624L;

	@Inject
	private GuideService guideService;

	@Inject
	private LocationService locationService;

	@Inject
	private SceneService sceneService;

	@Inject
	private EWService ewService;


	private List<Guide> destinationGuides;

	private List<GuideReport> myGuides;
	private GuideReport selectedGuide;

	private List<Location> myLocations;
	private List<Location> pendingLocations;



	private Location selectedLocation;
	private Scene selectedScene;
	private List<Scene> locationScenes;
	private List<Guide> guidesImIn;
	public Boolean hasLocations;

	private EWContact contact;

	private String searchText;

	public void preRenderView() throws Exception {

		if (FacesContext.getCurrentInstance().isPostback())
			return;

		try {
			BeekSession session = getSession();
			contact = session.getUser();

			// Get the data for this user

			loadUserLocations();
			loadUserGuides();

		// set the default
			if (myLocations.size() > 0)
				setSelectedLocation(myLocations.get(0));

			updateLocationScenes(selectedLocation);

			if(locationScenes != null)
				if(locationScenes.size() == 0)
				{
					selectedLocation.setStatus(0);
					updateLocationScenes(selectedLocation);
				}
		/*
			// no guides, and one location pending, send user to that
			if (myGuides.size() == 0 && myLocations.size() == 1
					&& myLocations.get(0).getStatus() == Location.STATE_PENDING) {
				reviewLocation(myLocations.get(0));
				return;
			}

			guidesImIn = getGuidesImIn(myLocations);
			for (Guide guide : myGuides)
				guidesImIn.remove(guide);
			Collections.sort(guidesImIn, Guide.scoreComparator);

			destinationGuides = getDestinationGuides(session.getDestinations());
			for (Guide guide : myGuides)
				destinationGuides.remove(guide);

			for (Guide guide : guidesImIn)
				destinationGuides.remove(guide);
			Collections.sort(destinationGuides, Guide.scoreComparator);*/

		} catch (Exception e) {
			// sendErrorDan("Error rendering Owner Dashboard", e);
			throw e;
		}
	}

	public void createGuide() throws IOException {
		redirect("/guide/create");
	}

	public List<Location> loadUserLocations() {
		List<Location> foundUserLocations = new ArrayList<Location>();
		for (int i = 0; i < contact.locationids.size(); i++)
		{
			if(locationService.getLocation(contact.locationids.get(i)) != null)
				foundUserLocations.add(locationService.getLocation(contact.locationids.get(i)));
		}

		myLocations = foundUserLocations;

		return foundUserLocations;
	}

	public List<GuideReport> loadUserGuides() {
		List<GuideReport> guides = new ArrayList<GuideReport>();
		for (int i = 0; i < contact.guideids.size(); i++)
		{
			if(guideService.getGuide(contact.guideids.get(i)) != null)
				guides.add(guideService.getGuideReport(contact.guideids.get(i)));
		}

		myGuides = guides;

		return guides;
	}

	public void selectGuide(GuideReport guide) {
		this.selectedGuide = guide;
	}

	public void viewSelectedGuide() throws IOException {
		if (selectedGuide != null)
			viewGuide(selectedGuide);
	}

	public void deleteSelectedGuide() {
		if (selectedGuide != null)
			deleteGuide(selectedGuide);

		if (myGuides.size() > 0)
			selectedGuide = myGuides.get(0);

		else
			selectedGuide = null;
	}

	public void viewSelectedGuideReport() throws IOException {
		if (selectedGuide != null)
			viewGuideReport(selectedGuide);
	}

	public void viewGuide(GuideReport guide) throws IOException {
		FacesContext.getCurrentInstance().getExternalContext()
				.redirect("/guide/" + guide.getId() + "/admin");
	}

	public void deleteGuide(GuideReport guideReport) {
		myGuides.remove(guideReport);

		GuideDetail guide = guideService.getGuideDetail(guideReport.getId());
		guideService.deleteGuide(guide);

		showMessage("Delete Operation", "Guide successfully deleted");
	}

	public void viewGuideReport(GuideReport guide) throws IOException {
		redirect("/guide/" + guide.getId() + "/report");
	}

	/**
	 * Gets the list of guides to show in the list under the destination combo
	 * box
	 */
	public List<Guide> getDestinationGuides(List<Destination> destinations) {
		try {
			return guideService.getGuidesForDestinations(destinations);

		} catch (Exception e) {
			showError("Error querying destination guides:" + e.getMessage());
			return new ArrayList<Guide>();
		}
	}

	/**
	 * Gets the list of locations for the locations combo box;
	 */
	private void loadLocations(List<EWTeam> teams) {
		pendingLocations = new ArrayList<Location>();
		myLocations = new ArrayList<Location>();

		try {
			List<Location> loc = locationService.getLocations(teams);
			for (Location location : loc) {
				if (location.getStatus() == Location.STATE_PENDING)
					pendingLocations.add(location);

				if (location.getStatus() == Location.STATE_NEW)
					myLocations.add(location);
			}

		} catch (Exception e) {
			showError("Error loading locations for teams:" + e.getMessage());
		}
	}

	/**
	 * Get the guides referencing locations of the logged in user.
	 */
	private List<Guide> getGuidesImIn(List<Location> locations) {
		try {
			// Get all scenes for the list of locations
			List<Scene> scenes = sceneService.getScenes(locations);

			List<Guide> guides = new ArrayList<Guide>();
			//guides.addAll(guideService.getGuidesContainingLocations(locations));
			guides.addAll(guideService.getGuidesContainingScenes(scenes));

			// removes the duplicates
			HashSet<Guide> set = new HashSet<Guide>(guides);
			return new ArrayList<Guide>(set);
		} catch (Exception e) {
			showError("Error querying guides I'm in: " + e.getMessage());
			return new ArrayList<Guide>();
		}
	}

	public List<Guide> getDestinationGuides() {
		if (searchText != null && searchText.length() >= 3) {
			searchText = searchText.toLowerCase();
			List<Guide> searchResults = new ArrayList<Guide>();
			for (Guide guide : destinationGuides)
				if (guide.getTitle() != null
						&& guide.getTitle().toLowerCase().contains(searchText))
					searchResults.add(guide);
			return searchResults;
		}

		return destinationGuides;
	}

	public List<GuideReport> getMyGuides() {
		return myGuides;
	}

	public List<Location> getLocations() {
		return myLocations;
	}

	public Location getSelectedLocation() {
		return selectedLocation;
	}

	public void setLocationScenes(List<Scene> value) {
		locationScenes = value;
	}

	public List<Scene> getLocationScenes() {
		return locationScenes;
	}

	public List<Scene> getLocationApprovedScenes() {

		List<Scene> list = new ArrayList<Scene>();

		if(locationScenes != null)
		for(Scene scene : locationScenes)
			if(scene.getStatus() > 0)
				list.add(scene);

		return list;
	}


	public List<String> getVisitsData(String sceneId) {

		Integer visits = 0;
		int totalSeconds = 0;
		int avgSecs = 0;
		String avgTime;


		List<String> data = new ArrayList<String>();
		List<GuideScene> GuideScenes = guideService.getGuideScenes(sceneId);

		if(GuideScenes.size() < 1)
			return null;

		for(GuideScene guideScene : GuideScenes){
			if(guideScene.visits != null) {
				visits += guideScene.visits;
				Double val = guideScene.avgTime;
				int mins = guideScene.avgTime.intValue();
				double seconds = (guideScene.avgTime % 1) * 100;
				int secs = (int) seconds;
				totalSeconds += (mins * 60) + secs;
			}
		}

		if(totalSeconds > 0)
			avgSecs = totalSeconds/visits;

		int minutes = (avgSecs % 3600) / 60;
		int seconds = avgSecs % 60;


		if(minutes > 0)
			avgTime = minutes + " m " + seconds + " s";
		else
			avgTime =  seconds + " s";

		data.add(visits.toString());
		data.add(avgTime);

		return data;
	}

	public void setSelectedLocation(Location location) {
		this.selectedLocation = location;
	}

	public void updateLocationScenes(Location location){
		try {

			if(location.getStatus() == 0)
				this.locationScenes = sceneService.getAllScenesForLocation(location.getId());
			else
				this.locationScenes = sceneService.getAllApprovedScenesForLocation(location.getId());

		} catch (Exception e) {
			showError("Error querying scenes for location:" + e.getMessage());
		}

	}

	public void setSelectedGuide(GuideReport guide) {
		this.selectedGuide = guide;
	}

	public GuideReport getSelectedGuide() {
		return selectedGuide;
	}
	public void setSelectedScene(Scene scene) {
		this.selectedScene = scene;
	}

	public Scene getSelectedScene() {
		return selectedScene;
	}

	public void saveSelectedScene(){
		if(selectedScene != null) {
		//	if(selectedScene.getVideoSettings() != null)
			//	selectedScene.s
			sceneService.saveScene(selectedScene);
		}
	}

	public void saveSelectedLocation(){
		if(selectedLocation != null)
			locationService.saveLocation(selectedLocation);
	}


	public List<Guide> getGuidesImIn() {
		return guidesImIn;
	}

	public void setSelectedLocationId(String value) {
		for (Location location : myLocations)
			if (location.getId().equals(value))
				setSelectedLocation(location);
	}

	public String getSelectedLocationId() {
		if (selectedLocation != null)
			return selectedLocation.getId();

		return null;
	}


	public String editSelectedLocation() throws IOException {
		if (selectedLocation != null)
			editLocation(selectedLocation);

		return null;
	}

	public void editLocation(Location location) throws IOException {
		redirect("/location/" + location.getId() + "/edit");
	}

	public void reviewLocation(Location location) throws IOException {
		redirect("/location/" + location.getId() + "/wizard");
	}

	public void editScene(Scene scene) throws IOException {
		redirect("/scene/" + scene.getId() + "/edit");
	}

	public void joinGuideRequest(Guide guide) throws IOException {
		redirect("/guide/" + guide.getId() + "/linkrequest");
	}

	public List<Location> getPendingLocations() {
		return pendingLocations;
	}

	public void setSearchText(String value) {
		this.searchText = value;
	}

	public String getSearchText() {
		return searchText;
	}

	public Boolean hasLocations(){return myLocations.size() > 0;}

	public DataTable getPerformance(String guideId) throws Exception {

		GuideReport guideReport = guideService.getGuideReport(guideId);
		BasicData data = guideReport.getBasicData();

		if(data == null)
			return null;


		DataTable table = new DataTable();



		table.addRow("Total Visitors", NumberFormat.getIntegerInstance().format(data.getVisits()));
		table.addRow("Total Scenes",
				NumberFormat.getIntegerInstance().format(data.getPageViews()));
		table.addRow( "Avg Time",
				DateUtil.timeConversion((int) data.getAvgTime()));
		table.addRow(
				"Avg Scenes",
				String.format("%.2f",data.getAvgScenes()));


		return table;
	}

	public Boolean minSceneApproved(){
		System.out.println(selectedLocation.getSceneOrderCount());
		System.out.println(getLocationApprovedScenes().size());
		return getLocationApprovedScenes().size() < selectedLocation.getSceneOrderCount();
	}

	public Boolean allApprovedScenesHaveDescriptions(){

		if(selectedLocation == null)
			return false;

		if(selectedLocation.getStatus() == 0)
			return false;

			for(Scene scene : getLocationApprovedScenes())
				if(scene.getDescription() == null)
					return true;

		return false;
	}

	public void approveLocation(){

			selectedLocation.setStatus(1);

			int i = 0;
			for(Scene scene : locationScenes) {
				if(scene.getStatus() > 0)
					i++;
				sceneService.saveScene(scene);
			}

			selectedLocation.setSceneApprovedCount(i);
			saveSelectedLocation();
			updateLocationScenes(selectedLocation);


		if(selectedLocation.getEwId() != null)
		try {

			ewService.updateLocation(getSession().getUsername(), getSession().getPassword(), selectedLocation);
			FacesMessage m2 = new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Location Approved", "Location Approved");
			FacesContext.getCurrentInstance().addMessage(null, m2);

		} catch (Exception e) {
			FacesMessage m2 = new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Location Approved", "Error updating CRM");
			FacesContext.getCurrentInstance().addMessage(null, m2);

		}
	}

}
