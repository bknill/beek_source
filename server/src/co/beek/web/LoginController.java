package co.beek.web;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Controller;

import co.beek.Constants;
import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.enterprizewizard.EWCustomer;
import co.beek.pano.model.dao.enterprizewizard.EWCustomersResponse;
import co.beek.pano.model.dao.enterprizewizard.EWTeam;
import co.beek.pano.model.dao.enterprizewizard.LoginResponse;
import co.beek.pano.model.dao.entities.Destination;
import co.beek.pano.service.dataService.destinationService.DestinationService;
import co.beek.pano.service.dataService.enterprizewizardService.EWService;
import co.beek.util.BeekSession;

@Controller("loginController")
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class LoginController extends BaseController implements Serializable {
    private static final long serialVersionUID = 284259433912057854L;

    @Inject
    private EWService ewService;

    @Inject
    private DestinationService destinationService;

    private String username;

    private String password;

    private String passwordRequestUsername;

    public void preRenderView() throws IOException {
        username = null;
        password = null;

        // remove the two possible subdomains from the request
        Constants.domain = getRequest().getServerName().replace("gms.", "");
        Constants.domain = Constants.domain.replace("www.", "");
    }

    public boolean isLoggedIn() {
        return getSession().isLoggedIn();
    }

    public void login() throws Exception {
        // check incase this is being called twice.
        if(getSession().isLoggedIn())
            return;

        // check for null first
        if(username == null || password == null)
        {
            showMessage("Invalid Details",
                "Please enter username and password to login");
            return;
        }

        // strings must be trimmed before storing
        username = username.trim();
        password = password.trim();

        // check that we are getting something
        if(username.equals("") || password.equals(""))
        {
            showMessage("Invalid Details",
                "Please enter username and password to login");
            return;
        }

        try {
            login(username, password);

        } finally {
            // clear the password from the form
            password = null;
        }
    }

    private void login(String username, String password) {
        try {
            LoginResponse loginResponse = ewService.login(username, password);

            if (loginResponse.getError() != null)
                showError(loginResponse.getError());

            else
                handleLoginResponse(username, password, loginResponse.getUser());

        } catch (Exception e) {
            String message = ExceptionUtils.getStackTrace(e);
            showError(message);
            sendErrorDan("Service error logging in", e);
        }
    }

    private void handleLoginResponse(String username, String password,
            EWContact user) throws IOException {
        // check if data exists
        if (user == null)
            return;


        List<String> guideid = user.getGuideID();
        List<String> locationid = user.getLocationsID();
        String type = user.getType();


        // set the user on the session
        BeekSession session = getSession();
        session.setUsername(username);
        session.setPassword(password);
        session.setUser(user);
        session.setGuideID(guideid);
        session.setLocationID(locationid);
        session.setType(type);

        System.out.println(session.getUser().first_name);

        String loginRedirect = session.getLoginRedirect();
        session.setLoginRedirect(null);


        if (loginRedirect != null)
            redirect(loginRedirect);
        else
            redirectHome();
    }

    private void sendIncompleteError(EWContact user) {
        String message = "Cannot login, user has no: ";
        if (user.getDestinations().size() == 0)
            message += "'destinations' ";
        if (user.getTeams().size() == 0)
            message += "'teams' ";
        if (user.getGroups().size() == 0)
            message += "'groups' ";

        sendErrorBen(message);
    }

    private List<EWTeam> loadAllTeams(String username, String password) {
        List<EWTeam> teams = new ArrayList<EWTeam>();
        try {
            EWCustomersResponse response = ewService.queryCustomers(username,
                    password);
            if (!response.getSuccess())
                throw new Exception("Error loading teams: "
                        + response.getErrorMessage());

            List<EWCustomer> list = response.getCustomers();

            for (EWCustomer c : list)
                if (c.getName() != null && c.getToken() != null)
                    teams.add(c.getTeam());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return teams;
    }

    private List<Destination> getDestinations(List<String> destinationTitles) {
        List<Destination> destinations = new ArrayList<Destination>();
        for (String title : destinationTitles) {
            Destination destination = destinationService
                    .getDestinationByTitle(title);
            if (destination != null)
                destinations.add(destination);
            else
                sendErrorBen("The destination: " + title
                        + " does not exist in the GMS");
        }

        return destinations;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void retrievePassword() throws IOException {
        ewService.sendPasswordEmail(passwordRequestUsername);
        showMessage("Password reminder sent",
                "A password reminder email has been sent to your email address");
    }

    public void setPasswordRequestUsername(String passwordRequestUsername) {
        this.passwordRequestUsername = passwordRequestUsername;
    }

    public String getPasswordRequestUsername() {
        return passwordRequestUsername;
    }
}