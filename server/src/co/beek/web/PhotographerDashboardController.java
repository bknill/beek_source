package co.beek.web;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import co.beek.pano.model.dao.enterprizewizard.EWPhotoShoot;
import co.beek.pano.model.dao.enterprizewizard.EWPhotoShootsResponse;
import co.beek.pano.model.dao.entities.Location;
import co.beek.pano.service.dataService.enterprizewizardService.EWService;
import co.beek.pano.service.dataService.locationService.LocationService;
import co.beek.util.BeekSession;

@Scope("session")
@Controller("PhotographerDashboardController")
public class PhotographerDashboardController extends BaseController implements
		Serializable {
	private static final long serialVersionUID = -1081621548447371624L;

	@Inject
	private EWService ewService;

	@Inject
	private LocationService locationService;

	private List<EWPhotoShoot> myPhotoShoots;

	private List<Location> myLocations;

	private List<Location> locationsToProcess;

	public void preRenderView() throws Exception {
		try {
			queryPhotoShoots();
			queryLocations();
		} catch (Exception e) {
			sendErrorDan("Error rendering photographer dashboard", e);
			throw e;
		}
	}

	private void queryPhotoShoots() throws Exception {
		BeekSession session = getSession();
		EWPhotoShootsResponse photoShootsResponse = ewService.queryPhotoShoots(
				session.getUsername(), session.getPassword());

		List<EWPhotoShoot> allShoots = photoShootsResponse.getPhotoShoots();

		// Filter out shoots that are incomplete according to beek rules.
		myPhotoShoots = filterPhotoShoots(session, allShoots);

		Collections.sort(myPhotoShoots, EWPhotoShoot.stateComparator);

		// need to be set on the session so that
		// it can be queried in OrderController
		session.setPhotoShoots(allShoots);
	}

	private List<EWPhotoShoot> filterPhotoShoots(BeekSession session,
			List<EWPhotoShoot> allShoots) {
		List<EWPhotoShoot> filteredShoots = new ArrayList<EWPhotoShoot>();
		for (EWPhotoShoot shoot : allShoots) {
			if (shoot.getTeamId() == null)
				showError("Shoot had no customer_token: "
						+ shoot.getLocationTitle());

			else if (shoot.getDestination() == null)
				showError("Shoot had no destination: "
						+ shoot.getLocationTitle());

			else if (!session.isInDestination(shoot.getDestination()))
				showError("User was not in destination: "
						+ shoot.getDestination() + " for shoot: "
						+ shoot.getLocationTitle());

			else if (!shoot.getWfstate().equals(
					EWPhotoShoot.PhotoShootStatus.APPROVED.getCode()))
				filteredShoots.add(shoot);
		}

		return filteredShoots;
	}

	private void queryLocations() throws Exception {
		// get all locations for this user
		List<Location> locations = locationService.getLocations(getUser()
				.getTeams());

		// remove all the photoShoots that are already processed into
		// locations
		// for (Location location : locations)
		// removeOrder(location.getPhotoShootId());

		myLocations = new ArrayList<Location>();
		locationsToProcess = new ArrayList<Location>();
		for (Location location : locations) {

			if (!isListedInShoots(location.getPhotoShootId()))
				myLocations.add(location);
		}
	}

	private boolean isListedInShoots(String photoShootId) {
		if (photoShootId == null)
			return false;

		for (int i = 0; i < myPhotoShoots.size(); i++)
			if (myPhotoShoots.get(i).getId().equals(photoShootId))
				return true;

		return false;
	}

	public int getStateNew() {
		return Location.STATE_NEW;
	}

	public int getStatePending() {
		return Location.STATE_PENDING;
	}

	public List<EWPhotoShoot> getMyPhotoShoots() throws Exception {
		return myPhotoShoots;
	}

	public void viewPhotoShoot(EWPhotoShoot photoShoot) throws IOException {
		redirect("/shoot/" + photoShoot.getId() + "/view");
	}

	public void editPhotoShoot(EWPhotoShoot photoShoot) throws IOException {
		redirect("/shoot/" + photoShoot.getId() + "/process");
	}

	public void viewLocation(Location location) throws IOException {
		redirect("/location/" + location.getId() + "/edit");
	}

	public void createLocation() throws IOException {
		redirect("/location/create");
	}

	public List<Location> getLocationsToProcess() {
		return locationsToProcess;
	}

	public List<Location> getMyLocations() {
		return myLocations;
	}
}
