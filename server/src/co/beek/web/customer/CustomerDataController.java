package co.beek.web.customer;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import co.beek.pano.model.dao.entities.Guide;
import co.beek.pano.model.dao.entities.Location;
import co.beek.pano.service.dataService.guideService.GuideService;
import co.beek.pano.service.dataService.locationService.LocationService;
import co.beek.web.BaseController;

@Scope("session")
@Controller("CustomerDataController")
public class CustomerDataController extends BaseController implements
		Serializable {
	private static final long serialVersionUID = -1081621548447371624L;

	@RequestMapping(value = "/customer/{id}/data", method = RequestMethod.GET)
	public String mapView(@PathVariable("id") String id) {
		return "/views/customer/data.jsf?id=" + id;
	}

	@Inject
	private GuideService guideService;

	@Inject
	private LocationService locationService;

	private List<Guide> guides;
	private List<Location> locations;

	public void preRenderView() throws Exception {
		// if (redirectingToLogin())
		// return;

		if (isPostback())
			return;

		String teamId = getRequest().getParameter("id");

		guides = guideService.getGuidesForTeam(teamId);
		locations = locationService.getLocationsForTeam(teamId);
	}

	public List<Location> getLocations() {
		return locations;
	}

	public List<Guide> getGuides() {
		return guides;
	}

	public void editLocation(Location location) throws IOException {
		redirect("/location/" + location.getId() + "/edit");
	}

	public void editGuide(Guide guide) throws IOException {
		redirect("/guide/" + guide.getId() + "/edit");
	}

	public void viewGuideReport(Guide guide) throws IOException {
		redirect("/guide/" + guide.getId() + "/report");
	}
}
