package co.beek.web.destination;

import java.io.IOException;
import java.io.Serializable;

import javax.inject.Inject;

import org.primefaces.event.map.MarkerDragEvent;
import org.primefaces.event.map.PointSelectEvent;
import org.primefaces.event.map.StateChangeEvent;
import org.primefaces.model.map.MapModel;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import co.beek.pano.model.beans.BeekMapModel;
import co.beek.pano.model.dao.entities.Destination;
import co.beek.pano.service.dataService.destinationService.DestinationService;
import co.beek.web.BaseController;

@Controller
@Scope("session")
public class DestinationController extends BaseController implements
		Serializable {
	private static final long serialVersionUID = 739561117787527869L;

	@RequestMapping(value = "/destination/{id}/edit", method = RequestMethod.GET)
	public String mapEditView(@PathVariable("id") String id) {
		return "/views/destination/edit.jsf?id=" + id;
	}

	@RequestMapping(value = "/destination/create", method = RequestMethod.GET)
	public String mapCreateView() {
		return "/views/destination/edit.jsf?id=new";
	}

	@Inject
	private DestinationService destinationService;

	private Destination destination;

	private BeekMapModel mapModel;

	public void preRenderView() throws IOException {
		if (redirectingToLogin())
			return;
		
		String destinationId = getRequest().getParameter("id");
		if (destinationId.equals("new"))
			destination = new Destination();

		else
			destination = destinationService.getDestination(destinationId);

		mapModel = new BeekMapModel(destination.getLatitude(),
				destination.getLongitude());
	}

	public void onPointSelect(PointSelectEvent event) {
		destination.setLatitude(event.getLatLng().getLat());
		destination.setLongitude(event.getLatLng().getLng());
	}

	public void onStateChange(StateChangeEvent event) {
		destination.setZoom(event.getZoomLevel());
		destination.setLatitude(event.getCenter().getLat());
		destination.setLongitude(event.getCenter().getLng());
	}

	public void onMarkerDrag(MarkerDragEvent event) {
		destination.setLatitude(event.getMarker().getLatlng().getLat());
		destination.setLongitude(event.getMarker().getLatlng().getLng());
	}

	public void save() throws IOException {
		destinationService.save(destination);
		showMessage("Save Compelte", "Destination successfully saved");
		redirectHome();
	}

	public Destination getDestination() {
		return destination;
	}

	public MapModel getMapModel() {
		return mapModel;
	}
}