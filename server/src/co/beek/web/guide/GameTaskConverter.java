package co.beek.web.guide;

import co.beek.pano.model.dao.entities.GameTask;
import co.beek.pano.model.dao.entities.Guide;
import co.beek.pano.service.dataService.GameTaskService.GameTaskService;
import co.beek.pano.service.dataService.guideService.GuideService;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by Ben on 21/10/2015.
 */
@Named
public class GameTaskConverter implements Converter {

    @Inject
    public GameTaskService gameTaskService;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String submittedValue) {
        if (submittedValue == null || submittedValue.isEmpty()) {
            return null;
        }

        try {
            return gameTaskService.getGameTask(submittedValue);
        } catch (NumberFormatException e) {
            throw new ConverterException(new FacesMessage(String.format("%s is not a valid Guide ID", submittedValue)), e);
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object modelValue) {
        if (modelValue == null) {
            return "";
        }


        if (modelValue instanceof GameTask) {
            return String.valueOf(((GameTask) modelValue).getId());
        } else {
            throw new ConverterException(new FacesMessage(String.format("%s is not a valid Guide", modelValue)));
        }
    }

}