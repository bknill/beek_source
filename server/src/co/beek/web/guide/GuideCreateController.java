package co.beek.web.guide;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.enterprizewizard.EWTeam;
import co.beek.pano.model.dao.entities.Destination;
import co.beek.pano.model.dao.entities.GuideDetail;
import co.beek.pano.model.dao.entities.GuideSection;
import co.beek.pano.model.dao.entities.Location;
import co.beek.pano.model.dao.entities.Scene;
import co.beek.pano.service.dataService.enterprizewizardService.EWServiceImpl;
import co.beek.pano.service.dataService.guideService.GuideService;
import co.beek.pano.service.dataService.locationService.LocationService;
import co.beek.pano.service.dataService.sceneService.SceneService;
import co.beek.util.BeekSession;
import co.beek.web.BaseController;
import co.beek.web.guide.bean.GuideSceneSelector;

@Scope("session")
@Controller("GuideCreateController")
public class GuideCreateController extends BaseController implements
		Serializable {
	private static final long serialVersionUID = -1081621548447371624L;

	@Inject
	private GuideService guideService;

	@Inject
	private LocationService locationService;

	@Inject
	private SceneService sceneService;

	private GuideDetail guide;
	
	public GuideDashboardController dashboardGuides;

	private List<Location> myLocations;

	private List<Destination> destinations;

	private List<GuideSceneSelector> selectorScenes;
	
	private String locationId;


	private Location location;
	
	public EWServiceImpl ewServiceImpl = new EWServiceImpl();

	@RequestMapping(value = "/guide/create", method = RequestMethod.GET)
	public String guideCreate() {
		return "/views/guide/create.jsf";
	}

	public List<Location> getMyLocations() {
		return myLocations;
	}

	public void setMyLocations(List<Location> myLocations) {
		this.myLocations = myLocations;
	}

	public void preRenderView() throws IOException {

		if (isPostback())
			return;

		if (redirectingToLogin())
			return;

		BeekSession session = getSession();
		EWContact contact = session.getUser();

		guide = new GuideDetail();
		guide.setTitle("New Guide");
		guide.setTeamId("CUSTOMER_1_");
		//TODO fix or take out
		//guide.setTeamId(contact.getTeams().get(0).getTeams());
		guide.setDestinationId("1");
		

		// load all destinationLocations for the logged in user
		myLocations = locationService.getAllLocations();
		Collections.reverse(myLocations);
		destinations = session.getDestinations();
		locationId = null;
		selectorScenes = new ArrayList<GuideSceneSelector>();
	}

	public List<Location> getDestinationLocations() {
		List<Location> locations = new ArrayList<Location>();
		for (Location l : myLocations)
			if (l.getDestinationId().equals(guide.getDestinationId()))
				locations.add(l);
		return locations;
	}

/*	private Location getLocation(String locationId) {
		for (Location l : myLocations)
			if (l.getId().equals(locationId))
				return l;
		return null;
	}*/

	public GuideDetail getGuide() {
		return guide;
	}

	public List<EWTeam> getTeams() {
		return getUser().getTeams();
	}

	public void setDestinations(List<Destination> destinations) {
		this.destinations = destinations;
	}

	public List<Destination> getDestinations() {
		return destinations;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	
	public String getLocationId() {
		return this.locationId;
	}

	public List<GuideSceneSelector> getSelectorScenes() {
		return selectorScenes;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}



	public void saveGuide() throws IOException {

		// Save guide so we can add scenes
		if (guide.getId() == null)
			guideService.saveGuideDetail(guide);
		
		// Always add a default section to the guide
		guide.addGuideSection(GuideSection.getDefault(guide.getId()));

		if(location != null){
			List<Scene> scenes = sceneService.getAllScenesForLocation(location.getId());

			for(Scene scene : scenes)
				guide.addGuideScene(scene);
		}


		// update the guide with the new details
		guideService.saveGuideDetail(guide);
		guide.setTeamId("CUSTOMER_1_");
		guideService.saveGuideDetail(guide);
		
		BeekSession session = getSession();
		EWContact user = session.getUser();
		List<String> updateGuideID = session.getGuideID();
		updateGuideID.add(guide.getId());
		session.setGuideID(updateGuideID);
		user.setGuideID(updateGuideID);
		
		
		saveGuideToCRM();
		redirectGuideEdit(guide.getId());
		
		
	}
	

	public void doneSaveGuide() throws IOException {

		// Save guide so we can add scenes
		if (guide.getId() == null)
			guideService.saveGuideDetail(guide);
		
		// Always add a default section to the guide
		guide.addGuideSection(GuideSection.getDefault(guide.getId()));

		if(locationId != null){
			List<Scene> scenes = sceneService.getAllScenesForLocation(locationId);

			for(Scene scene : scenes)
					guide.addGuideScene(scene);
		}
		
		// update the guide with the new details
		guide.setDestinationId("1");
		guide.setTeamId("CUSTOMER_1_");
		guideService.saveGuideDetail(guide);
		
		BeekSession session = getSession();
		EWContact user = session.getUser();
		List<String> updateGuideID = session.getGuideID();
		updateGuideID.add(guide.getId());
		session.setGuideID(updateGuideID);
		user.setGuideID(updateGuideID);

		
		saveGuideToCRM();
		redirectGuideDashboard();

	}
	
	private void saveGuideToCRM()//GuideDetail guide)
	{
		try {
			ewServiceImpl.createGuide(getSession().getUsername(), getSession().getPassword(), guide);
			
			
		} catch (Exception e) {
			FacesMessage m2 = new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Hit exception", "Hit exception");
			FacesContext.getCurrentInstance().addMessage(null, m2);
		}
		
	}

	private void reloadGuide() {
		guide = guideService.getGuideDetail(guide.getId());
	}
	
	/**
	 * Used in the jsf for absolute urls back to this server
	 */
	public String getBeekDomain() {
		return getRequest().getServerName().replace("gms", "www");
	}

}
