package co.beek.web.guide;

import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.event.FlowEvent;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gdata.client.analytics.DataQuery;
import com.google.gdata.data.BaseEntry;
import com.google.gdata.data.analytics.DataEntry;
import com.google.gdata.util.AuthenticationException;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.chart.CartesianChartModel;  
import org.primefaces.model.chart.ChartSeries;  

import co.beek.pano.model.beans.DataTable;
import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.enterprizewizard.EWContactsResponse;
import co.beek.pano.model.dao.enterprizewizard.EWTeam;
import co.beek.pano.model.dao.entities.Guide;
import co.beek.pano.model.dao.entities.GuideDetail;
import co.beek.pano.model.dao.entities.GuideReport;
import co.beek.pano.model.dao.entities.Location;
import co.beek.pano.model.dao.entities.Scene;
import co.beek.pano.model.dao.googleanalytics.VisitByPath2;
import co.beek.pano.service.dataService.enterprizewizardService.EWService;
import co.beek.pano.service.dataService.enterprizewizardService.EWServiceImpl;
import co.beek.pano.service.dataService.googleAnalytics.BeekAnalayticsService;
import co.beek.pano.service.dataService.guideService.GuideService;
import co.beek.pano.service.restService.ServiceResponse;
import co.beek.util.BeekSession;
import co.beek.util.DateUtil;
import co.beek.web.BaseController;
import co.beek.web.guide.GuideReportController;
import co.beek.web.guide.bean.CountryVisits;
import co.beek.web.guide.bean.GuideData;
import co.beek.web.guide.bean.MediaEventData;
import co.beek.web.guide.bean.Status;
import co.beek.web.guide.bean.VisitData;



import flexjson.JSONSerializer;

@Controller("GuideDashboardController")
public class GuideDashboardController extends BaseController implements Serializable {
	
	private static final long serialVersionUID = 998192212096448706L;
	
	private List<Status> statuses = new ArrayList<Status>();

	JSONSerializer serializer = new JSONSerializer().prettyPrint(true);

	private GuideReport guide;
	private List<GuideReport> guideList;
	private List<GuideReport> userGuideList;
	private EWContact user;
	private EWService ewService = new EWServiceImpl();
	private List<GuideReport> dashboardGuides;
	private String searchTerm;
	private List<GuideReport> searchResults;
	private boolean searched = false;
	private List<VisitByPath2> visits;
	public GuideReport selectedGuide;
	private BeekSession session;
	public String type;
    private String startDate;
    private String endDate;
    private String Id;
    private Integer deleted;
    private boolean loaded;
    public boolean dataLoaded;
    public CartesianChartModel visitChart;  
    public CartesianChartModel timeChart;  

	@Inject
	private GuideService guideService;
	
    @Inject
    private BeekAnalayticsService gaService;

	@RequestMapping(value = "/guide/dashboard", method = RequestMethod.GET)
	public String guideDashboard() {
		return "/views/guide/dashboard.jsf";
	}

	public void preRenderView() throws Exception {
		if (isPostback())
			return;

		if (redirectingToLogin())
			return;
		
		if(statuses.size() == 0)
		{
			statuses.add(new Status(Guide.STATUS_FREE, "Free"));
			statuses.add(new Status(Guide.STATUS_PUBLISHED, "Published"));
			statuses.add(new Status(Guide.STATUS_PROMOTED, "Promoted"));
		}
		
		Id = null;
		deleted = -1;
		
		loadData();

	}
	
	public void cacheData() throws Exception{
	    startDate = DateUtil.getGAStartDate();
	    endDate = DateUtil.getCurrentDate();
	
	}

	public void loadData(){

//		if(dashboardGuides != null)
	//		return;

		user = getUser();
		dashboardGuides = null;

		if (searched)
			dashboardGuides = searchResults;
		else
		{

			if (user.type.toLowerCase().contains("employee")){

				dashboardGuides = guideList = guideService.getAllGuidesReports();
				Collections.reverse(dashboardGuides);

			}

			else
			{
				dashboardGuides = getUserGuides();
			}
		}

	}
	
	
	public List<GuideReport> getDashboardGuides() {
		return dashboardGuides;
	}
	
	public void setDashboardGuides(List<GuideReport> dashboardGuides) {
		this.dashboardGuides=dashboardGuides;
	}

	public String getStartDate() {
		return startDate;
	}
	
	public void setStartDate(String startDate) {
		
		this.startDate=startDate;
	}
	

	
	public GuideReport getGuide() {
		return guide;
	}
	
	public void saveGuide(Guide guide)
	{
		guideService.saveGuide(guide);
	}
	
	
 
    
    
  
	
	public List<GuideReport> getUserGuides() {
		List<GuideReport> tempList = new ArrayList<GuideReport>();
		for (int i = 0; i < user.guideids.size(); i++)
		{
			if(guideService.getGuide(user.guideids.get(i)) != null && guideService.getGuideReport(user.guideids.get(i)).status != -1)
				tempList.add(guideService.getGuideReport(user.guideids.get(i)));
			
		}

		return tempList;
	}
	
	public List<GuideReport> getGuideList() {
		return guideList;
	}
	
	
	public List<String> getAllGuideTitles() {
		List<String> guideTitles = new ArrayList<String>();
		for (int i = 0; i < guideList.size(); i++)//need a check for null
		{
			if(guideList.get(i).getTitle() == null || guideList.get(i).getTitle().isEmpty())
				guideTitles.add("No Title");
			else
				guideTitles.add(guideList.get(i).getTitle());
		}
		
		return guideTitles;
	}
	
	public List<Status> getStatuses() {
		return statuses;
	}

	
	public String getId(){
		return Id;
	}
    
    
	public void setId(String Id) {
		this.Id = Id;
	}
	
	public GuideReport getSelectedGuide(){
		return selectedGuide;
	}
    
    
	public void setSelectedGuide(GuideReport guide) {
		this.selectedGuide = guide;
	}
		

	public String getSearchTerm() {
		return searchTerm;
	}
	
	public void setSearchTerm(String searchTerm) {
		this.searchTerm = searchTerm;
	}

	public void saveGuide()
	{
		guideService.saveGuideReport(guide);
	}
	
	public void showAllGuidesButton()
	{
		searched = false;
		try {
			redirectGuideDashboard();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void searchButton(boolean redirect) {

		if(user == null)
			user = getUser();

		if (!user.type.toLowerCase().contains("employee"))
			searchGuides(userGuideList, redirect);
		else
			searchGuides(guideList, redirect);
	}
	
	public void searchGuides(List<GuideReport> userGuideList2, boolean redirect) {
		List<GuideReport> foundGuides = new ArrayList<GuideReport>();
		for (int i = 0; i < userGuideList2.size(); i++)
		{
			if (userGuideList2.get(i).getTitle() == null)
				continue;
			if (userGuideList2.get(i).getTitle() != null || !userGuideList2.get(i).getTitle().isEmpty() || userGuideList2.get(i).getStatus() != -1)
			{
				if (userGuideList2.get(i).getTitle().toLowerCase().contains(searchTerm.toLowerCase()) || userGuideList2.get(i).getId().contains(searchTerm.toLowerCase()))
				foundGuides.add(userGuideList2.get(i));//.getTitle());
			}
		}
		searchResults = foundGuides;
		try {
			searched = true;
			if(redirect)
				redirectGuideDashboard();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public boolean isAdmin() {
		if(user.type.contains("employee"))
			return true;
		else 
			return false;
	}
	
	public String onFlowProcess(FlowEvent event) throws IOException {
		if (event.getOldStep().equals("editTab"))
			reloadGuide();

		return event.getNewStep();
	}
	
	private void reloadGuide() {
		guide = guideService.getGuideReport(guide.getId());
	}
	

	/**
	 * Used in the jsf for absolute urls back to this server
	 */
	public String getBeekDomain() {
		return getRequest().getServerName().replace("gms", "www");
	}
	
	public void selectGuide(GuideReport guide) {
		this.selectedGuide = guide;
	}
	
	public void deleteSelectedGuide() {
		
			deleteGuide(selectedGuide);

/*		if (dashboardGuides.size() > 0)
			selectedGuide = dashboardGuides.get(0);
		else
			selectedGuide = null;
		
		deleteGuide(selectedGuide);*/
	}
	
	public void deleteGuide(GuideReport guide) {
		GuideDetail guideDetail = guideService.getGuideDetail(guide.getId());
		dashboardGuides.remove(guide);
		guideDetail.status = deleted;
        guideService.saveGuideDetail(guideDetail);
	}
	
	public void cloneGuide (GuideReport guide){
		GuideDetail guideClone = guideService.getGuideDetail(guide.getId());
		GuideDetail clone = null;
		try {
			clone = guideService.cloneGuide(guideClone);
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		GuideReport guideCloned = guideService.getGuideReport(clone.getId());
		dashboardGuides.add(0,guideCloned);
	}
	


	public void viewGuideReport(Guide guide) throws IOException {
		redirect("/guide/" + guide.getId() + "/report");
	}

	public boolean isLoaded() {
		return loaded;
	}

	public void setLoaded(boolean loaded) {
		this.loaded = loaded;
	}

	public boolean isDataLoaded() {
		return dataLoaded;
	}

	public void setDataLoaded(boolean dataLoaded) {
		this.dataLoaded = dataLoaded;
	}
	
	public CartesianChartModel getVisitChart() {
		return visitChart;
	}
	
	public CartesianChartModel getTimeChart() {
		return timeChart;
	}

	public List<GuideReport> getSearchResults() {
		return searchResults;
	}

	public void setSearchResults(List<GuideReport> searchResults) {
		this.searchResults = searchResults;
	}
}