package co.beek.web.guide;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;


import java.text.DateFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;

import org.primefaces.event.FlowEvent;
import org.primefaces.json.JSONArray;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import co.beek.Constants;
import co.beek.pano.model.beans.DataTable;
import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.enterprizewizard.EWContactsResponse;
import co.beek.pano.model.dao.enterprizewizard.EWTeam;
import co.beek.pano.model.dao.entities.Guide;
import co.beek.pano.model.dao.entities.GuideReport;
import co.beek.pano.model.dao.entities.Scene;
import co.beek.pano.model.dao.entities.Visit;
import co.beek.pano.model.dao.googleanalytics.Country;
import co.beek.pano.model.dao.googleanalytics.Event;
import co.beek.pano.model.dao.googleanalytics.Referral;
import co.beek.pano.model.dao.googleanalytics.VisitByPath;
import co.beek.pano.model.dao.googleanalytics.VisitByPath2;
import co.beek.pano.model.dao.googleanalytics.YearMonthData;
import co.beek.pano.service.dataService.FavouritesService;
import co.beek.pano.service.dataService.enterprizewizardService.EWService;
import co.beek.pano.service.dataService.enterprizewizardService.EWServiceImpl;
import co.beek.pano.service.dataService.googleAnalytics.BasicData;
import co.beek.pano.service.dataService.googleAnalytics.BeekAnalayticsService;
import co.beek.pano.service.dataService.guideService.GuideService;
import co.beek.pano.service.dataService.sceneService.SceneService;
import co.beek.util.BeekSession;
import co.beek.util.DateUtil;
import co.beek.web.BaseController;
import co.beek.web.guide.bean.MediaEventData;
import co.beek.web.guide.bean.Status;
import co.beek.web.guide.bean.VisitData;
import flexjson.JSONSerializer;

@Controller("GuideEditController")
@Scope("session")
public class GuideEditController extends BaseController implements Serializable {
	private static final long serialVersionUID = 897552663451118806L;
	
	private static List<Status> statuses = new ArrayList<Status>();

	JSONSerializer serializer = new JSONSerializer().prettyPrint(true);

	private Guide guide;

	private EWContact user;
	private List<EWContact> users;
	private List<String> usersEmails;
	public String emailAddress;
	
	public String subdomain;
	public String parentId;
	
	private boolean userReload;
	public int activeIndex;
	
	//private EWServiceImpl ewServiceImpl;
	private EWService ewService = new EWServiceImpl();
	
	private BeekSession session;
	@Inject
	private GuideService guideService;
	
	private GuideReport guideReport;
	
    @Inject
    private BeekAnalayticsService gaService;
	
    @Inject
    private FavouritesService favouriteService;
    
    @Inject
    private SceneService sceneService;
	
	//@Inject
	//private GuideDetail guideDetail;

/*	@RequestMapping(value = "/guide/{guideId}/edit", method = RequestMethod.GET)
	public String mapEditView(HttpServletRequest request,
			@PathVariable("guideId") String guideId) {
		return "/views/guide/edit.jsf?id=" + guideId;
		//return "guide/"+guideId+"/edit";
	}*/
	public String guideId;
	
    private String startDate;

    private String endDate;
    
    private String dbId;
    
    private List<Referral> referrals;

    private List<MediaEventData> eventsData;

    public LineChartModel visitChart;
    public LineChartModel timeChart;


	public void preRenderView() throws Exception {
		if (isPostback())
			return;

		if (redirectingToLogin())
			return;
		
		if(statuses.size() == 0)
		{
			statuses.add(new Status(Guide.STATUS_FREE, "Free"));
			statuses.add(new Status(Guide.STATUS_PUBLISHED, "Published"));
			statuses.add(new Status(Guide.STATUS_PROMOTED, "Promoted"));
		}

		session = getSession();
		String guideId = getRequest().getParameter("id");
		guide = guideService.getGuide(guideId);
		guideReport = guideService.getGuideReport(guideId);
		//getGuideUsers(guideId);
		user = getUser();
		getGuideUsers(guideId);

		
		if(!isUserReload())
			activeIndex = 0;
		
		userReload=false;
        
        subdomain = guide.subdomain;
        parentId = guide.parent;
        startDate = DateUtil.getGAStartDate();
        endDate = DateUtil.getCurrentDate();
		
	}
	
	public boolean isUserReload() {
		return userReload;
	}
		// test permissions in edit mode
		//if (!guide.hasWritePermission(user))
			//throw new Exception("You dont have permission to edit this guide");

	public void test() {}

	public Guide getGuide() {
		return guide;
	}
	
	public List<Status> getStatuses() {
		return statuses;
	}

	public List<EWTeam> getTeams() {
		return getUser().getTeams();
	}

	public void setUsersEmails(List<String> emails) {
		usersEmails = emails;
	}
	public List<String> getUsersEmails()
	{
		return usersEmails;
	}
	
	public void saveGuide()
	{
		guide.subdomain = subdomain;
		guide.parent = parentId;
		guideService.saveGuide(guide);
	}
	
	public String onFlowProcess(FlowEvent event) throws IOException {
		if (event.getOldStep().equals("editTab"))
			reloadGuide();

		return event.getNewStep();
	}
	
	private void reloadGuide() {
		guide = guideService.getGuide(guide.getId());
	}

	public void getGuideUsers(String guideid) {
		EWContactsResponse ewContactsResponse = null;
		try {
			ewContactsResponse = ewService.getGuideUsers(session.getUsername(), session.getPassword(), guideid);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		users = ewContactsResponse.getContacts();
		usersEmails = new ArrayList<String>();
		for (int i = 0; i < ewContactsResponse.getContacts().size(); i++)
		{
			String temp =ewContactsResponse.getContacts().get(i).email;
			usersEmails.add(temp);
		}
		
	}
	
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
	public String getEmailAddress()
	{
		return emailAddress;
	}
	
	public void setSubdomain(String SubDomain) {
		this.subdomain = SubDomain;
	}
	
	public String getSubdomain()
	{
		return subdomain;
	}
	
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	
	public String getParentId()
	{
		return parentId;
	}
	
	public void addUser() {
				EWContactsResponse ewContactsResponse = null;
				
				try {
					ewContactsResponse = ewService.searchUserByEmail(session.getUsername(), session.getPassword(), emailAddress);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					//use this for if it comes back null
				}
				if (ewContactsResponse.getContacts().size() == 0)
				{
					System.out.println("This is a new user - adding a user");
					try {
						ewContactsResponse = ewService.addGuideUserByEmail(session.getUsername(), session.getPassword(), emailAddress, guide.getId());
						System.out.println("added a user");
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						//use this for if it comes back null
					}
					}
				
				else	
				{
					System.out.println("User exists - add user to guide");
					try {
						ewContactsResponse = ewService.updateGuideID(session.getUsername(), session.getPassword(), ewContactsResponse.getContacts().get(0).id, ewContactsResponse.getContacts().get(0).type,  guide.getId());
						System.out.println("updated user");
					} 
					catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace(); 
						//use this for if it comes back null
					}
				}
				
				getGuideUsers(guide.getId());
				//reloadToUserTab();
	}
	
	public void removeUser(String email) {
		EWContactsResponse ewContactsResponse = null;
		EWContactsResponse ewContactsResponse1 = null;
		try {
			ewContactsResponse = ewService.searchUserByEmail(session.getUsername(), session.getPassword(), email);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//use this for if it comes back null
		}
		if (ewContactsResponse.getContacts().size() != 0)
		try {
			ewContactsResponse1 = ewService.removeGuideUserByEmail(session.getUsername(), session.getPassword(), ewContactsResponse.getContacts().get(0).id, ewContactsResponse.getContacts().get(0).type, guide.getId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//use this for if it comes back null
		}	
		
		
		
		//reloadToUserTab();
	}
	
	public void reloadToUserTab() {
		
		try {
		    Thread.sleep(6000);
		} catch(InterruptedException ex) {
		    Thread.currentThread().interrupt();
		}
		activeIndex = 3;
		userReload = true;
		try {
			redirectGuideEdit(guide.getId());
			} 
		catch (Exception e) {
			e.printStackTrace();
		}	
		//activeIndex = 0;
	}
	
	public void setActiveIndex(int activeIndex) throws Exception {
		if(activeIndex == 3)
			reportFunctions();
		this.activeIndex = activeIndex;
	}
	
	public int getActiveIndex() {
		return activeIndex;
	}
	
	public boolean isAdmin() {
		if(user.type.contains("employee"))
			return true;
		else 
			return false;
	}
	
	
	/**
	 * Used in the jsf for absolute urls back to this server
	 */
	public String getBeekDomain() {
		return getRequest().getServerName().replace("gms", "www");
	}
	
    private String getGuideFilter() {
	        return "ga:pagePath=~g" + guide.getId() + "(\\/s.*)?$";
    }

    public DataTable getPerformance() throws Exception {

    	BasicData data = guideReport.getBasicData();

		DataTable table = new DataTable();

		if(data == null)
			return null;

            table.addRow("Total Visitors",NumberFormat.getIntegerInstance().format(data.getVisits()));
            table.addRow("Total Scenes",
            		NumberFormat.getIntegerInstance().format(data.getPageViews()));
            table.addRow( "Avg Time",
            		DateUtil.timeConversion((int) data.getAvgTime()));
            table.addRow(
                    "Avg Scenes",
                    String.format("%.2f",data.getAvgScenes()));
        

        return table;
    }
	    
	    


//	    public DataTable getEngagement() throws Exception {
//
//	        DataQuery query = gaService.newQuery(startDate, endDate, 10);
//	        query.setDimensions("ga:eventCategory");
//	        query.setMetrics("ga:totalEvents");
//	        query.setFilters(getGuideFilter()
//	                + ";ga:eventCategory==share,ga:eventCategory==conversion,ga:eventCategory==favourite");
//
//	        List<DataEntry> data = gaService.executeQuery(query);
//
//	        gaService.printInfo(data);
//
//	        DataTable table = new DataTable();
//	        if (data.size() > 0)
//	            table.addRow("Conversions",
//	                    data.get(0).stringValueOf("ga:totalEvents"));
//
//	        if (data.size() > 1)
//	            table.addRow("Shares", data.get(1).stringValueOf("ga:totalEvents"));
//
//	        if (data.size() > 2)
//	            table.addRow("Favourites",
//	                    data.get(2).stringValueOf("ga:totalEvents"));
//
//	        return table;
//	    }
//
//
//	    private List<DataEntry> getLineGraphData() throws Exception {
//	        DataQuery query = gaService.newQuery(startDate, endDate, 100);
//	        query.setDimensions("ga:month");
//	        query.setMetrics("ga:visitors,ga:avgTimeOnSite,ga:pageviewsPerVisit");
//	        query.setFilters(getGuideFilter());
//	        return gaService.executeQuery(query);
//	    }
//	    
//	    
    
    
    
    	public void reportFunctions() throws Exception{
    		getReferals();
    		
    	}
    
	    public List<VisitByPath> getVisitsByScene() throws Exception {

	        List<VisitByPath> paths = new ArrayList<VisitByPath>();

	        
	        for (VisitByPath path : guideReport.getVisitsByPath()) {

	            if (path.getSceneId() == null)
	               continue;

	            if(path.getAvgTimeOnPage() < 1.0)
	            	continue;
	            
	            if(path.getPageViews() < 5)
	            	continue;
	            
	            Scene scene = sceneService.getScene(path.getSceneId());
	            if (scene == null)
	                continue;
	            
            	path.setTitle(scene.getTitle());
            	path.setThumbName(scene.getThumbName());
	            paths.add(path);
	            
	        }
	        
	        //get data ready for processing
	        eventsData = getEventsData();
	        Collections.reverse(eventsData);
	        
	        return paths;
	    }


	    public List<Referral> getReferals() {


			if(guideReport.getReferralsList() == null)
				return null;

	        referrals = new ArrayList<Referral>();
	        
	        for (Referral referal : guideReport.getReferralsList()) {
	        		
	            if (referal.wasFrom("localhost") || referal.wasFrom("beektest")
	                    || referal.wasFrom("beekdev")  || referal.wasFrom("beekdev"))
	                continue;

	            referrals.add(referal);
	        }


	        return referrals;
	    }
	    
	    public String getSourceData() {


			if(getReferals() == null)
				return null;


	    	List<Referral> referralSources = new ArrayList<Referral>();
	    	referralSources.add(referrals.get(0));

		    	for(Referral referal : referrals)
		    		for(int i=0;i<referralSources.size();i++) { 
		    			
		    			if(referal.getSource().equals(referralSources.get(i).getSource())){
		    				referralSources.get(i).setSessions(referal.getSessions() + referralSources.get(i).getSessions());
		    				break;
		    			}
		    			else if(i == referralSources.size() - 1)
		    				referralSources.add(referal);
		    			
		    		}
		    			
    	
	    	
	    	List<List<Object>> list = new ArrayList<List<Object>>();
	    	
	    	
	    		for(Referral referal : referralSources){
	    		
	    		if(referal.getSessions() < 2 )
	    			continue;
	    		
	    		ArrayList<Object> referals = new ArrayList<Object>();
	    		referals.add(referal.getSource());
	    		referals.add(referal.getSessions());
	    		//referals.add("Total Visits: " + referal.getSessions() + "<br/>Avg Scenes: " + referal.getAvgSessionDurationString() + "<br/>Avg Time: " + referal.getPageViewsPerSessionString());
		    	
	    		
	    		list.add(referals);

	    	}
	    		
	    	
	    	JSONArray JSONArray = new JSONArray(list);
	    	return JSONArray.toString();
	    	
	    }
	    
	    public String getReferrerData(){
	    	
	    	List<List<Object>> list = new ArrayList<List<Object>>();
	    	
	    	
    		for(Referral referal : referrals){
    		
    		if(referal.getSessions() < 2 )
    			continue;
    		
    		ArrayList<Object> referals = new ArrayList<Object>();
    		referals.add(referal.getReferralPath());
    		referals.add(referal.getSessions());
    		
    		list.add(referals);

    	}
    		
    	
    	JSONArray JSONArray = new JSONArray(list);
    	return JSONArray.toString();
	    	
	    	
	    }
	    
//	    
//	    public List<CityVisits> getCitiesData() throws Exception {
//	        DataQuery query = gaService.newQuery(startDate, endDate, 100);
//	        query.setDimensions("ga:city");
//	        query.setMetrics("ga:visits");
//	        query.setFilters(getGuideFilter());
//	        query.setSort("-ga:visits");
//	        List<DataEntry> data = gaService.executeQuery(query);
//
//	        List<CityVisits> visits = new ArrayList<CityVisits>();
//
//	        for (DataEntry city : data)
//	            visits.add(new CityVisits(city));
//
//	        return visits;
//	    } 
//
//	    /**
//	     * https://www.googleapis.com/analytics/v3/data/ga?ids=ga:54190402&
//	     * dimensions
//	     * =ga:country&metrics=ga:visitors,ga:avgTimeOnSite,ga:pageviewsPerVisit
//	     * &filters
//	     * =ga:pagePath=~g39&sort=-ga:visitors&start-date=2012-04-04&end-date
//	     * =2013-04-18&max-results=50
//	     *
//	     * @return
//	     * @throws Exception
//	     */
	    public List<String> getCountriesMapData() throws Exception {

	    	List<String> list = new ArrayList<String>();
	    //	list.add(e)
	  	
	    	for(Country country : guideReport.getCountriesList()){
	    			list.add(serializer.serialize(country));
	    	}
	    	
	        return list;
	    }
	    
	    public String getCountriesData(){
	    	
	    	return guideReport.getCountriesData();
	    }
	    
	    public String getCitiesData(){

	    	
	    	List<List<Object>> list = new ArrayList<List<Object>>();
	    	
    		int counter = 0;
	    	
	    	for(Country visit : guideReport.getCountriesList()){
	    		
	    		if(visit.getSessions() < 2 || visit.getCity().endsWith("(not set)"))
	    			continue;
	    		
	    		ArrayList<Object> city = new ArrayList<Object>();
	    		city.add(visit.getCity() + ", " + visit.getCountry());
	    		city.add(visit.getSessions());
	    		city.add("Total Visits: " + visit.getSessions() + "<br/>Avg Scenes: " + visit.getAvgSessionDurationString() + "<br/>Avg Time: " + visit.getPageViewsPerSessionString());
	    		list.add(city);
	    		
	    		counter++;
	    		
	    		//if(counter > 20)
	    			//break;
	    	}
	    		
	    	
	    	JSONArray JSONArray = new JSONArray(list);
	    	return JSONArray.toString();
	    }
	    
	    
	    
	    
	    public Map<String, List<Country>> getCountriesHashMap() throws Exception {
	    	 
	        Map<String, List<Country>> map = new HashMap< String, List<Country>>();

	        for (Country data : guideReport.getCountriesList()){
	            String key  = data.getCountry();
	            if(map.containsKey(key)){
	                List<Country> list = map.get(key);
	                list.add(data);

	            }else{
	                List<Country> list = new ArrayList<Country>();
	                list.add(data);
	                map.put(key, list);
	            }

	        }
	        return map;
	    }
    
	    public List<VisitByPath2> getVisitsChartData() throws Exception {


	        List<VisitByPath2> visits = new ArrayList<VisitByPath2>();

	        visitChart = new LineChartModel();
	        timeChart = new LineChartModel();
	        
		        ChartSeries visitorsData = new ChartSeries();  
		        ChartSeries activeVisitorsData = new ChartSeries();  
		        ChartSeries timeData = new ChartSeries();  
		        ChartSeries scenesData = new ChartSeries();  
		        visitorsData.setLabel("Inactive Visitors");
		        activeVisitorsData.setLabel("Active Visitors");
		        timeData.setLabel("Avg Time");
		        scenesData.setLabel("Avg Scenes");
	        
		        for (YearMonthData data : guideReport.getVisitsByWeek()) {


		            if(data.getSessions() > 0 && data.getYearMonth().length() > 2){
		            
		            	//visits.add(new VisitByPath2(data));
		            
			            String yearMonth = data.getYearMonth();
			            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
			            Date date = sdf.parse(yearMonth);
			            sdf= new SimpleDateFormat("MMM yyyy");
			            String monthName = sdf.format(date);
			            visitorsData.set(monthName,data.getSessions());
			            activeVisitorsData.set(monthName,data.getVisitsWithEvent());
			            timeData.set(monthName,data.getAvgTimeMins());
			            scenesData.set(monthName,data.getPageviewsPerSession());
			          }
	        	}
		        
		        visitChart.addSeries(activeVisitorsData);
		        visitChart.addSeries(visitorsData);
		        timeChart.addSeries(timeData);
		        timeChart.addSeries(scenesData);


	        return visits;
	    }
	    
		public LineChartModel getVisitChart() throws Exception {
			System.out.println("getVisitChart()");
			getVisitsChartData();
			return visitChart;
		}


		
		public String getMonth(int month) {
		    return new DateFormatSymbols().getMonths()[month-1];
		}
		
		public LineChartModel getTimeChart() {
			return timeChart;
		}
	    
	    
	    public List<VisitData> getAvgChartData() throws Exception {
//	        DataQuery query = gaService.newQuery(startDate, endDate, 100);
//	        query.setDimensions("ga:month");
//	        query.setMetrics("ga:pageviewsPerVisit");
//	        query.setFilters(getGuideFilter());
//	        query.setSort("ga:month");
//	        List<DataEntry> data = gaService.executeQuery(query);

	        List<VisitData> visits = new ArrayList<VisitData>();

//	        for (DataEntry referal : data)
//	            visits.add(new VisitData(referal));

	        return visits;
	    }
	    


	    public List<MediaEventData> getEventsData() throws Exception {
	    	
	        List<MediaEventData> events = new ArrayList<MediaEventData>();
	        for (Event event : guideReport.getEventsList())
	            events.add(new MediaEventData(event));
	        return events;
	    }

	    public DataTable getGuideActions() {
	        DataTable table = new DataTable();
	        table.addRow("Next Button", sumEventsData("guide", "next"));
	        table.addRow("Sections", sumEventsData("guide", "sections"));
	        table.addRow("Scene", sumEventsData("guide", "scene"));
	        table.addRow("Close Button", sumEventsData("guide", "close"));
	        table.addRow("Close Button", sumEventsData("guide", "open"));
	        return table;
	    }

	    private long sumEventsData(String category, String action) {
	        long total = 0;
	        for (MediaEventData event : eventsData)
	            if (event.isCategory(category) && event.isAction(action))
	                total += event.getTotal();

	        return total;
	    }

	    public List<MediaEventData> getMediaEvents() throws Exception {
	    	eventsData = getEventsData();
	        List<MediaEventData> mediaEvents = new ArrayList<MediaEventData>();
	        for (MediaEventData entry : eventsData) {
	            if (entry.isCategory("hotspot") & entry.isAction("media") 
	            		|| entry.isCategory("content element"));
	            	Scene scene = sceneService.getScene(entry.getSceneId());
	            	//entry.setScene(scene);
	                mediaEvents.add(entry);
	        }
	        return mediaEvents;
	    }
	    
	    public List<MediaEventData> getNavigationEvents() throws Exception {
	    	eventsData = getEventsData();
	        List<MediaEventData> mediaEvents = new ArrayList<MediaEventData>();
	        for (MediaEventData entry : eventsData) {
	            if (entry.isCategory("hotspot") & entry.isAction("bubble")
	            		|| entry.isCategory("navigation")){
	            	Scene scene = sceneService.getScene(entry.getSceneId());
	            	Scene targetScene = sceneService.getScene(entry.getTargetSceneId());
	            	
	            	//entry.setScene(scene);
	            	//entry.setTargetScene(targetScene);
	                mediaEvents.add(entry);
	            }
	        }
	        return mediaEvents;
	    }
	    
	    
	    public List<MediaEventData> getHotspotByScene(String sceneId) throws Exception {
	    	
	        List<MediaEventData> mediaEvents = new ArrayList<MediaEventData>();
	        
	       // System.out.println(eventsData.size());
	        
        	for (int i=eventsData.size()-1; i> -1; i--) {
        		
        		 MediaEventData entry = eventsData.get(i);
	        	
        		 if (!entry.getSceneId().equals(sceneId) || entry.getTargetSceneId().equals(guide.getId()))
        			 continue;
        		 
	            if (entry.isCategory("hotspot") & entry.isAction("bubble")
	            		|| entry.isCategory("navigation")){
	            	//Scene scene = sceneService.getScene(entry.getSceneId());
	            	Scene targetScene = sceneService.getScene(entry.getTargetSceneId());
	            	
	            	if(targetScene.getTitle().length() < 22)
	            		entry.setHotspotTitle(targetScene.getTitle());
	            	else
	            		entry.setHotspotTitle(targetScene.getTitle().subSequence(0, 25) + "..");
	                eventsData.remove(entry);
	                mediaEvents.add(entry);
	            }
	            
	            if(mediaEvents.size() > 5)
	            	break;
	        }
	        	
	        return mediaEvents;
	    }
	    
	    public boolean sceneDataReady(String sceneId){
	    	return true;
	    }
	    
	    public List<MediaEventData> getGuideEvents() throws Exception {
	    	eventsData = getEventsData();
	        List<MediaEventData> mediaEvents = new ArrayList<MediaEventData>();
	        for (MediaEventData entry : eventsData) {
	            if (entry.isCategory("guide")){
	            	Scene scene = sceneService.getScene(entry.getSceneId());
	            	//Scene targetScene = sceneService.getScene(entry.getTargetSceneId());
	            	
	            //	entry.setScene(scene);
	            	//entry.setTargetScene(targetScene);
	                mediaEvents.add(entry);
	            }
	        }
	        return mediaEvents;
	    }
	    

	    public String getGuidePath() {
	        return "http://" + Constants.domain + "/guide/" + guide.getId();
	    }
	    public  List<Visit> getIndividualVisits()
	    {
	        return favouriteService.getVisits(guide.getId());
	    }
	    
	    
	    
	    

	 
}