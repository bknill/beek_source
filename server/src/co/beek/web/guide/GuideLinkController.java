package co.beek.web.guide;

import java.io.Serializable;

import javax.inject.Inject;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import co.beek.Constants;
import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.enterprizewizard.EWCorrespondenceResponse;
import co.beek.pano.model.dao.entities.Guide;
import co.beek.pano.model.dao.entities.Location;
import co.beek.pano.service.dataService.enterprizewizardService.EWService;
import co.beek.pano.service.dataService.guideService.GuideService;
import co.beek.pano.service.dataService.locationService.LocationService;
import co.beek.util.BeekSession;
import co.beek.web.BaseController;

/**
 * Controller for the view where the owner of the guide in the url is presented
 * with UI to link to the team in the url
 */
@Scope("session")
@Controller("GuideLinkController")
public class GuideLinkController extends BaseController implements Serializable {
	private static final long serialVersionUID = -1081621548447371624L;

	@RequestMapping(value = "/guide/{guideId}/link/{locationId}", method = RequestMethod.GET)
	public String mapView(@PathVariable("guideId") String guideId,
			@PathVariable("locationId") String locationId) {
		return "/views/guide/link.jsf?guideId=" + guideId + "&locationId="
				+ locationId;
	}

	@Inject
	private GuideService guideService;

	@Inject
	private LocationService locationService;

	@Inject
	private EWService ewService;

	private Guide guide;

	//private GuideLocation guideLocation;

	private Location location;

	public void preRenderView() throws Exception {
		if (redirectingToLogin())
			return;

		String guideId = getRequest().getParameter("guideId");
		String locationId = getRequest().getParameter("locationId");

		EWContact user = getUser();

		// the guide for showing hte user what they are doing
		guide = guideService.getGuide(guideId);
		location = locationService.getLocation(locationId);

		if (!guide.hasWritePermission(user))
			throw new Exception("You need to be on team " + guide.getTeamId()
					+ " to add links to this guide.");

		// init the variable;
//		guideLocation = new GuideLocation();
//		guideLocation.setGuideId(guideId);
//		guideLocation.setLocationId(locationId);
	}

//	public GuideLocation getGuideLocation() {
//		return guideLocation;
//	}

	public Guide getGuide() {
		return guide;
	}

	public Location getLocation() {
		return location;
	}

	public void addLink() {
		try {
			// Add the link
			//guideService.saveGuideLocation(guideLocation);

			sendLinkEmail();

			showMessage("Guide Link Added",
					"The location '" + location.getTitle()
							+ " was added to your guide '" + guide.getTitle());

			redirectHome();

		} catch (Exception e) {
			sendErrorDan("failed to link location to guide", e);
		}
	}

	private void sendLinkEmail() throws Exception {
		BeekSession session = getSession();
		EWContact user = session.getUser();

		String body = user.first_name + " " + user.last_name + " "
				+ "has added your location '" + location.getTitle() + "' "
				+ "to their guide '" + guide.getTitle() + "'.";

		String path = "/guide/{guideId}/view";
		path = path.replace("{guideId}", guide.getId());

		String guideViewUrl = "http://gms."+Constants.domain + path;

		EWCorrespondenceResponse response = ewService.sendEmail(
				session.getUsername(), session.getPassword(),
				location.getTeamId(), guide.getId(), "GuideLinkAdded", body, guideViewUrl, null);

		if (!response.getSuccess())
			throw new Exception("Error in EW sendEmail "
					+ response.getErrorMessage());
	}

}
