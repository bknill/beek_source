package co.beek.web.guide;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.inject.Inject;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import co.beek.Constants;
import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.enterprizewizard.EWCorrespondenceResponse;
import co.beek.pano.model.dao.entities.Guide;
import co.beek.pano.model.dao.entities.Location;
import co.beek.pano.service.dataService.enterprizewizardService.EWService;
import co.beek.pano.service.dataService.guideService.GuideService;
import co.beek.pano.service.dataService.locationService.LocationService;
import co.beek.util.BeekSession;
import co.beek.web.BaseController;

/**
 * Controller for the view where the owner of the guide in the url is presented
 * with UI to link to the team in the url
 */
@Scope("session")
@Controller("GuideLinkRequestController")
public class GuideLinkRequestController extends BaseController implements
		Serializable {
	private static final long serialVersionUID = -1081621548447371624L;

	@RequestMapping(value = "/guide/{guideId}/linkrequest", method = RequestMethod.GET)
	public String mapView(@PathVariable("guideId") String guideId) {
		return "/views/guide/linkrequest.jsf?guideId=" + guideId;
	}

	@Inject
	private GuideService guideService;

	@Inject
	private LocationService locationService;

	@Inject
	private EWService ewService;

	private Guide guide;

	private List<Location> locations;

	private String locationId;

	private String url;

	public void preRenderView() throws Exception {
		if (redirectingToLogin())
			return;

		String guideId = getRequest().getParameter("guideId");

		EWContact user = getUser();

		// the guide for showing hte user what they are doing
		guide = guideService.getGuide(guideId);

		locations = locationService.getLocations(user.getTeams());
		if (locations.size() > 0)
			locationId = locations.get(0).getId();

	}
	
	public String getUrl() throws UnsupportedEncodingException
	{
		BeekSession session = getSession();
		Location location = getLocation(locationId);
		
		String message = session.getUser().first_name + " "
		+ session.getUser().last_name
		+ " has requested that you add their location '"
		+ location.getTitle() + "' to your guide '" + guide.getTitle()
		+ "'.";
		
		String path = "guide/{guideId}/link/{locationId}";
		path = path.replace("{guideId}", guide.getId());
		path = path.replace("{locationId}", location.getId());
		
		String guideLinkUrl = "http://gms."+ Constants.domain + "/" + path;
		
		return EWCorrespondenceResponse
		.sendCorrespondenceUrl(session.getUsername(),
				session.getPassword(), guide.getTeamId(), guide.getId(),
				"GuideLinkRequest", message, guideLinkUrl, null);
	}
	

	public Guide getGuide() {
		return guide;
	}

	public List<Location> getLocations() {
		return locations;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getLocationId() {
		return locationId;
	}

	public void sendLinkRequest() {
		try {
			BeekSession session = getSession();

			Location location = getLocation(locationId);

			sendLinkRequest(session, location, guide);

			showMessage("Link Request Sent",
					"The request to have your location '" + location.getTitle()
							+ "' added to the guide '" + guide.getTitle()
							+ "' has been sent.");

			redirectHome();
		} catch (Exception e) {
			sendErrorDan("failed to link location to guide", e);
		}
	}

	private Location getLocation(String locationId) {
		for (Location location : locations)
			if (location.getId().equals(locationId))
				return location;

		return null;
	}

	private void sendLinkRequest(BeekSession session, Location location, Guide guide)
			throws Exception {

		String message = session.getUser().first_name + " "
				+ session.getUser().last_name
				+ " has requested that you add their location '"
				+ location.getTitle() + "' to your guide '" + guide.getTitle()
				+ "'.";

		String path = "guide/{guideId}/link/{locationId}";
		path = path.replace("{guideId}", guide.getId());
		path = path.replace("{locationId}", location.getId());

		String guideLinkUrl = "http://gms."+ Constants.domain + "/" + path;

		EWCorrespondenceResponse response = ewService
				.sendEmail(session.getUsername(), session.getPassword(),
						guide.getTeamId(), guide.getId(), "GuideLinkRequest", message,
						guideLinkUrl, null);

		if (!response.getSuccess())
			throw new Exception("Error in EW sendEmail "
					+ response.getErrorMessage());
	}

}
