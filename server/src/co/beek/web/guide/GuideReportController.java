package co.beek.web.guide;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import co.beek.Constants;
import co.beek.pano.model.beans.DataTable;
import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.entities.GuideReport;
import co.beek.pano.model.dao.entities.Scene;
import co.beek.pano.model.dao.entities.Visit;
import co.beek.pano.model.dao.googleanalytics.Referral2;
import co.beek.pano.model.dao.googleanalytics.VisitByPath2;
import co.beek.pano.service.dataService.FavouritesService;
import co.beek.pano.service.dataService.googleAnalytics.BeekAnalayticsService;
import co.beek.pano.service.dataService.guideService.GuideService;
import co.beek.pano.service.dataService.sceneService.SceneService;
import co.beek.util.DateUtil;
import co.beek.web.BaseController;
import co.beek.web.guide.bean.CountryVisits;
import co.beek.web.guide.bean.MediaEventData;
import co.beek.web.guide.bean.VisitData;

import com.google.gdata.client.analytics.DataQuery;
import com.google.gdata.data.analytics.DataEntry;
import com.google.gdata.util.AuthenticationException;

import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;  

@Controller("GuideReportController")
public class GuideReportController extends BaseController {
    private static final long serialVersionUID = 1L;

    @RequestMapping(value = "/guide/{id}/report", method = RequestMethod.GET)
    public String mapReportView(@PathVariable("id") String guideId) {
        return "/views/guide/reportNoAccordion.jsf?id=" + guideId;
    }

    @RequestMapping(value = "/guide/{id}/reportNoAccordion", method = RequestMethod.GET)
    public String mapReportNoAccordionView(@PathVariable("id") String guideId) {
        return "/views/guide/reportNoAccordion.jsf?id=" + guideId;
    }

    @Inject
    private GuideService guideService;

    @Inject
    private SceneService sceneService;

    @Inject
    private BeekAnalayticsService gaService;

    @Inject
    private FavouritesService favouriteService;

    private GuideReport guide;

    private String startDate;

    private String endDate;
    
    private String dbId;

    private List<DataEntry> lineGraphData;

    private List<MediaEventData> eventsData;
    
    private List<VisitData> guideData;
    private List<VisitByPath2> visits;
    public CartesianChartModel visitChart;  
    public CartesianChartModel timeChart;  

    public void preRenderView() throws Exception {
//        if (isPostback())
//            return;
//
//        if (redirectingToLogin())
//            return;

        try {
            gaService.init();
        } catch (AuthenticationException e) {
            e.printStackTrace();
        }

        EWContact user = getSession().getUser();
        String guideId = getRequest().getParameter("id");
        guide = guideService.getGuideReport(guideId);

        if (!user.hasGuide(guide.getId()))
            throw new Exception("You need to be on team" + guide.getTeamId()
                    + " to view the report for this guide.");

        // Cache the line graph data for the 3 graphs
        startDate = DateUtil.getGAStartDate();
        endDate = DateUtil.getCurrentDate();
        lineGraphData = getLineGraphData();
        eventsData = getEventsData();

    }

    public GuideReport getGuide() {
        return guide;
    }

    public String getGuidePath() {
        return "http://" + Constants.domain + "/guide/" + guide.getId();
    }
    
    

    private String getGuideFilter() {
        return "ga:pagePath=~g" + guide.getId() + "(\\/s.*)?$";
    }

    public DataTable getPerformance() throws Exception {

        DataQuery query = gaService.newQuery(startDate, endDate, 3);
        query.setMetrics("ga:visitors,ga:visits,ga:pageviewsPerVisit,ga:avgTimeOnSite");
        query.setFilters(getGuideFilter());

        List<DataEntry> data = gaService.executeQuery(query);

        DataTable table = new DataTable();
        if (data != null) {
            table.addRow("Total Visitors",
                    data.get(0).stringValueOf("ga:visitors"));
            table.addRow("Total Visits", data.get(0).stringValueOf("ga:visits"));
            table.addRow(
                    "Avg Scenes",
                    DateUtil.formatNumber(data.get(0).doubleValueOf(
                            "ga:pageviewsPerVisit")));
            table.addRow(
                    "Avg Time",
                    DateUtil.formatNumber(data.get(0).doubleValueOf(
                            "ga:avgTimeOnSite")));
        }

        return table;
    }
    
    


    /**
     * https://www.googleapis.com/analytics/v3/data/ga?ids=ga:54190402
     * &dimensions=ga:eventCategory &metrics=ga:totalEvents
     * &filters=ga:pagePath=
     * ~g39,ga:eventCategory==share,ga:eventCategory==conversion
     * &start-date=2012-04-04 &end-date=2013-04-18
     *
     * @return
     * @throws Exception
     */
    public DataTable getEngagement() throws Exception {

        DataQuery query = gaService.newQuery(startDate, endDate, 10);
        query.setDimensions("ga:eventCategory");
        query.setMetrics("ga:totalEvents");
        query.setFilters(getGuideFilter()
                + ";ga:eventCategory==share,ga:eventCategory==conversion,ga:eventCategory==favourite");

        List<DataEntry> data = gaService.executeQuery(query);

        gaService.printInfo(data);

        DataTable table = new DataTable();
        if (data.size() > 0)
            table.addRow("Conversions",
                    data.get(0).stringValueOf("ga:totalEvents"));

        if (data.size() > 1)
            table.addRow("Shares", data.get(1).stringValueOf("ga:totalEvents"));

        if (data.size() > 2)
            table.addRow("Favourites",
                    data.get(2).stringValueOf("ga:totalEvents"));

        return table;
    }

    /**
     * https://www.googleapis.com/analytics/v3/data/ga?ids=ga:54190402
     * &dimensions=ga:month
     * &metrics=ga:visitors,ga:avgTimeOnSite,ga:pageviewsPerVisit
     * &filters=ga:pagePath=~g39 &start-date=2012-04-04 &end-date=2013-04-18
     *
     * @throws Exception
     */

    private List<DataEntry> getLineGraphData() throws Exception {
        DataQuery query = gaService.newQuery(startDate, endDate, 100);
        query.setDimensions("ga:month");
        query.setMetrics("ga:visitors,ga:avgTimeOnSite,ga:pageviewsPerVisit");
        query.setFilters(getGuideFilter());
        return gaService.executeQuery(query);
    }
    
    
    public List<VisitByPath2> getVisitsByScene() throws Exception {
        DataQuery query = gaService.newQuery(startDate, endDate, 100);
        query.setDimensions("ga:pagePath");
        query.setMetrics("ga:pageviews,ga:avgTimeOnPage");
        query.setFilters(getGuideFilter());
        query.setSort("-ga:pageviews");
        List<DataEntry> data = gaService.executeQuery(query);

        List<VisitByPath2> paths = new ArrayList<VisitByPath2>();

        for (DataEntry path : data) {
            VisitByPath2 visitByPath = new VisitByPath2(path);
            if (visitByPath.getSceneId() == null)
                continue;

            Scene scene = sceneService.getScene(visitByPath.getSceneId());
            if (scene == null)
                continue;

            visitByPath.setTitle(scene.getTitle());
            visitByPath.setThumbName(scene.getThumbName());
            paths.add(visitByPath);
        }

        return paths;
    }
    
    

    /**
     * https://www.googleapis.com/analytics/v3/data/ga?ids=ga:54190402
     * &dimensions=ga:source
     * &metrics=ga:visitors,ga:avgTimeOnSite,ga:pageviewsPerVisit
     * &filters=ga:pagePath=~g39 &sort=-ga:visitors &start-date=2012-04-04
     * &end-date=2013-04-18
     *
     * @return
     * @throws Exception
     */
    public List<Referral2> getReferals2() throws Exception {
        DataQuery query = gaService.newQuery(startDate, endDate, 100);
        query.setDimensions("ga:source,ga:referralPath");
        query.setMetrics("ga:visitors,ga:avgTimeOnSite,ga:pageviewsPerVisit");
        query.setFilters(getGuideFilter());
        query.setSort("-ga:visitors");
        List<DataEntry> data = gaService.executeQuery(query);

        List<Referral2> referrals = new ArrayList<Referral2>();

        for (DataEntry referal : data) {
            Referral2 r = new Referral2(referal);

            // remove the dev referrals
            if (r.wasFrom("localhost") || r.wasFrom("beektest")
                    || r.wasFrom("beekdev"))
                continue;

            referrals.add(r);
        }

        return referrals;
    }
    


    /**
     * https://www.googleapis.com/analytics/v3/data/ga?ids=ga:54190402&
     * dimensions
     * =ga:country&metrics=ga:visitors,ga:avgTimeOnSite,ga:pageviewsPerVisit
     * &filters
     * =ga:pagePath=~g39&sort=-ga:visitors&start-date=2012-04-04&end-date
     * =2013-04-18&max-results=50
     *
     * @return
     * @throws Exception
     */
    public List<CountryVisits> getCountriesMapData() throws Exception {
        DataQuery query = gaService.newQuery(startDate, endDate, 100);
        query.setDimensions("ga:country");
        query.setMetrics("ga:visitors");
        query.setFilters(getGuideFilter());
        query.setSort("-ga:visitors");
        List<DataEntry> data = gaService.executeQuery(query);

        List<CountryVisits> visits = new ArrayList<CountryVisits>();

    //    for (DataEntry referal : data)
          //  visits.add(new CountryVisits(referal));

        return visits;
    }
    

    
    public List<VisitByPath2> getVisitsChartData() throws Exception {
    	
        DataQuery query = gaService.newQuery(startDate, endDate, 100);
        query.setDimensions("ga:month");
        query.setMetrics("ga:visits,ga:visitsWithEvent");
        query.setFilters(getGuideFilter());
        query.setSort("ga:month");
        List<DataEntry> data = gaService.executeQuery(query);

        List<VisitByPath2> visits = new ArrayList<VisitByPath2>();

        visitChart = new CartesianChartModel(); 
	        ChartSeries visitorsData = new ChartSeries();  
	        ChartSeries activeVisitorsData = new ChartSeries();  
	        visitorsData.setLabel("Inactive Visitors");
	        activeVisitorsData.setLabel("Active Visitors");
        
	        for (DataEntry visit : data) {
	            VisitByPath2 visitChartData = new VisitByPath2(visit);
	            
	            visits.add(new VisitByPath2(visit));
	            visitorsData.set(visitChartData.getMonth(),visitChartData.getVisitsWithOutEvents());
	            activeVisitorsData.set(visitChartData.getMonth(),visitChartData.getVisitsWithEvent());
        	}
	        
	        visitChart.addSeries(activeVisitorsData);
	        visitChart.addSeries(visitorsData);

        return visits;
    }
    
	public CartesianChartModel getVisitChart() {
		return visitChart;
	}
	
	public CartesianChartModel getTimeChart() {
		return timeChart;
	}
    
    
    public List<VisitData> getAvgChartData() throws Exception {
        DataQuery query = gaService.newQuery(startDate, endDate, 100);
        query.setDimensions("ga:month");
        query.setMetrics("ga:pageviewsPerVisit");
        query.setFilters(getGuideFilter());
        query.setSort("ga:month");
        List<DataEntry> data = gaService.executeQuery(query);

        List<VisitData> visits = new ArrayList<VisitData>();

        for (DataEntry referal : data)
            visits.add(new VisitData(referal));

        return visits;
    }
    


    public List<MediaEventData> getEventsData() throws Exception {
        DataQuery query = gaService.newQuery(startDate, endDate, 1000);
        query.setDimensions("ga:eventCategory,ga:eventAction,ga:eventLabel");
        query.setMetrics("ga:totalEvents");
        query.setFilters(getGuideFilter());
        query.setSort("-ga:totalEvents");
        List<DataEntry> raw = gaService.executeQuery(query);

        gaService.printInfo(raw);

        List<MediaEventData> events = new ArrayList<MediaEventData>();
       // for (DataEntry entry : raw)
            //events.add(new MediaEventData(entry));
        return events;
    }

    public DataTable getGuideActions() {
        DataTable table = new DataTable();
        table.addRow("Next Button", sumEventsData("guide", "next"));
        table.addRow("Sections", sumEventsData("guide", "sections"));
        table.addRow("Scene", sumEventsData("guide", "scene"));
        table.addRow("Close Button", sumEventsData("guide", "close"));
        table.addRow("Close Button", sumEventsData("guide", "open"));
        return table;
    }

    private long sumEventsData(String category, String action) {
        long total = 0;
        for (MediaEventData event : eventsData)
            if (event.isCategory(category) && event.isAction(action))
                total += event.getTotal();

        return total;
    }

    public List<MediaEventData> getMediaEvents() throws Exception {
        eventsData = getEventsData();
        List<MediaEventData> mediaEvents = new ArrayList<MediaEventData>();
        for (MediaEventData entry : eventsData) {
            if (entry.isCategory("content element"))
                mediaEvents.add(entry);
        }
        return mediaEvents;
    }


    public  List<Visit> getIndividualVisits()
    {
        return favouriteService.getVisits(guide.getId());
    }
    
 

}