package co.beek.web.guide;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import co.beek.pano.model.dao.entities.GuideReport;
import co.beek.pano.model.dao.entities.Scene;
import co.beek.pano.service.dataService.googleAnalytics.BeekAnalayticsService;
import co.beek.pano.service.dataService.guideService.GuideService;
import co.beek.pano.service.dataService.hotspotService.HotspotService;
import co.beek.pano.service.dataService.sceneService.SceneService;
import co.beek.util.DateUtil;
import co.beek.web.BaseController;
import co.beek.web.guide.bean.MediaEventData;
import co.beek.web.guide.bean.VisitData;

import com.google.gdata.client.analytics.DataQuery;
import com.google.gdata.data.analytics.DataEntry;
import com.google.gdata.util.AuthenticationException;

@ManagedBean
@Controller("GuideReportUserController")
public class GuideReportUserController extends BaseController {
	private static final long serialVersionUID = 1L;

	@RequestMapping(value = "/guide/{id}/report/{key}", method = RequestMethod.GET)
	public String mapView(@PathVariable("id") String id,
			@PathVariable("key") String key) {
		return "/views/guide/reportUser.jsf?id=" + id + "&key=" + key;
	}

	@Inject
	private GuideService guideService;

	@Inject
	private SceneService sceneService;
	
	@Inject
	private HotspotService hotspotService;

	@Inject
	private BeekAnalayticsService gaService;

	private GuideReport guide;

	private String startDate;

	private String endDate;

	private String key;

	private List<MediaEventData> events;

	private List<VisitData> visits;
	
	public List<VisitData> getVisits() {
		return visits;
	}

	public void setVisits(List<VisitData> visits) {
		this.visits = visits;
	}

	private VisitData visit;

	public void preRenderView() throws Exception {
		if (isPostback())
			return;

		if (redirectingToLogin())
			return;

		key = getRequest().getParameter("key");
		String guideId = getRequest().getParameter("id");
		guide = guideService.getGuideReport(guideId);

		startDate = DateUtil.getGAStartDate();
		endDate = DateUtil.getCurrentDate();

		try {
			gaService.init();
			visits = queryVisitData();
			visit = visits.size() > 0 ? visits.get(0) : null;
			
			setEvents(queryEventsData());
		} catch (AuthenticationException e) {
			e.printStackTrace();
		}
	}

	public String getKey() {
		return key;
	}

	public GuideReport getGuide() {
		return guide;
	}

	private String getVisitFilter() {
		return "ga:pagePath=~g" + guide.getId() + ";ga:customVarValue1==" + key;
		// return "ga:pagePath=~g" + guide.getId() + "(\\/s.*)?$ ";
	}

	public List<VisitData> queryVisitData() throws Exception {
		DataQuery query = gaService.newQuery(startDate, endDate, 300);
		query.setDimensions("ga:pagePath,ga:country,ga:city,ga:browser");
		query.setMetrics("ga:visits,ga:pageviews,ga:timeOnSite,ga:visitsWithEvent");
		query.setFilters(getVisitFilter());
		List<DataEntry> raw = gaService.executeQuery(query);

		//gaService.printInfo(raw);

		List<VisitData> visits = new ArrayList<VisitData>();
		for (DataEntry entry : raw)
		{
			VisitData v = new VisitData(entry);
			
			if(v.getGuideId() == null)
				continue;

			if(!v.getGuideId().equals(guide.getId()))
				continue;
			
			if(v.getSceneId() == null)
				continue;
			
			Scene scene = sceneService.getScene(v.getSceneId());
			if (scene == null)
				continue;
			
			v.setSceneTitle(scene.getTitle());
			visits.add(v);
		}
		return visits;
	}

	public VisitData getVisitData() throws Exception {
		return visit;
	}

	public List<MediaEventData> queryEventsData() throws Exception {
//		DataQuery query = gaService.newQuery(startDate, endDate, 1000);
//		query.setDimensions("ga:eventCategory,ga:eventAction,ga:eventLabel");
//		query.setMetrics("ga:totalEvents");
//		query.setFilters(getVisitFilter());
//		query.setSort("-ga:totalEvents");
//		List<DataEntry> raw = gaService.executeQuery(query);

		List<MediaEventData> events = new ArrayList<MediaEventData>();
		//for (DataEntry entry : raw) {
		//	MediaEventData event = new MediaEventData(entry);
			//Scene scene = sceneService.getScene(event.getSceneId());
			//if (scene == null)
				//continue;
		//	event.setSceneTitle(scene.getTitle());

			//content element
//			if (event.getCategory().equals("content element")
//					&& event.getAction().equals("poster"))
//			{
//				event.setHotspotTitle(event.getHotspotTitleFromLabel());
//				
//			}
//
//			if (event.getCategory().equals("favourites")
//					&& event.getAction().equals("photo"))
//			{
//				Photo photo = hotspotService.getPhoto(event.getHotspotId());
//				if(photo != null)
//					event.setHotspotTitle(photo.getTitle());
//			}
//
//			if (event.getCategory().equals("favourites")
//					&& event.getAction().equals("poster"))
//			{
//				Poster poster = hotspotService.getPoster(event.getHotspotId());
//				if(poster != null)
//					event.setHotspotTitle(poster.getTitle());
//			}
//			//event.setSceneTitle(sceneService.getScene(sceneId).getTitle(););
//			events.add(event);
//		}
		return events;
	}

	public List<MediaEventData> getEvents() {
		return events;
	}

	public void setEvents(List<MediaEventData> events) {
		this.events = events;
	}
}
