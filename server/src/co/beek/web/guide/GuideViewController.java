package co.beek.web.guide;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.*;
import java.util.Map.Entry;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import co.beek.pano.model.dao.entities.*;
import co.beek.pano.service.dataService.FavouritesService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import co.beek.Constants;
import co.beek.pano.service.dataService.BeekService;
import co.beek.pano.service.dataService.guideService.GuideService;
import co.beek.web.BaseController;
import flexjson.JSONSerializer;

@Controller("GuideViewController")
@Scope("session")
public class GuideViewController extends BaseController implements Serializable {
	private static final long serialVersionUID = 897552663451118806L;

	JSONSerializer serializer = new JSONSerializer().prettyPrint(true);

	private Beek beek;

	private Guide guide;
	
	private GuideDetail selectedGuide;

	private GuideScene selectedScene;
	
	private String connectId;



	private String userId;
	
	private String hotspotId;

	@Inject
	private GuideService guideService;

	@Inject
	private BeekService beekService;

	@Inject
	private FavouritesService favouritesService;

	private HashMap<String, String> flashVars;

	@Value("#{buildProperties.gaCode}")
	private String gaCode;

	@Value("#{buildProperties.cdnUrl}")
	private String cdnUrl;

	@RequestMapping(value = "/guide/{guideId}/view", method = {RequestMethod.GET, RequestMethod.HEAD})
	public String map(HttpServletRequest request,
			@PathVariable("guideId") String guideId) {
		return "/views/guide/view.jsf?g=" + guideId + "&e="
				+ request.getParameter("email");
	}
	
	@RequestMapping(value = "/subdomain/{guidename}/", method = {RequestMethod.GET, RequestMethod.HEAD})
	public String mapSubdomain(HttpServletRequest request,
			@PathVariable("guidename") String guidename) throws Exception {
			List <Guide> guides = guideService.getGuideBySubdomain(guidename);
			if(guides.isEmpty())
				return null;
			Guide guide = guides.get(0);
			String id = guide.getId();
		return "/views/guide/view.jsf?g=" + id + "&e="
				+ request.getParameter("email");
	}

	@RequestMapping(value = "/guide/{guideId}/scene/{sceneId}", method = {RequestMethod.GET, RequestMethod.HEAD})
	public String mapView1(@PathVariable("guideId") String guideId,
			@PathVariable("sceneId") String sceneId) {
		return "/views/guide/view.jsf?g=" + guideId + "&s=" + sceneId;
	}

	@RequestMapping(value = "/guide/{guideId}/scene/{sceneId}/hotspot/{hotspotId}", method = RequestMethod.GET)
	public String mapView2(@PathVariable("guideId") String guideId,
			@PathVariable("sceneId") String sceneId,
			@PathVariable("hotspotId") String hotspotId) {
		return "/views/guide/view.jsf?g=" + guideId + "&s=" + sceneId + "&h="
				+ hotspotId;
	}
	
	@RequestMapping(value = "/guide/{guideId}/scene/{sceneId}/connect/{cId}", method = RequestMethod.GET)
	public String mapView3(@PathVariable("guideId") String guideId,
			@PathVariable("sceneId") String sceneId,
			@PathVariable("cId") String connectId) {
		return "/views/guide/view.jsf?g=" + guideId + "&s=" + sceneId + "&c="
				+ connectId;
	}

	@RequestMapping(value = "/guide/{guideId}/scene/{sceneId}/visit/{visitId}", method = RequestMethod.GET)
	public String mapView4(@PathVariable("guideId") String guideId,
						   @PathVariable("sceneId") String sceneId,
						   @PathVariable("visitId") String visitId) {
			return "/views/guide/view.jsf?g=" + guideId + "&s=" + sceneId + "&v="
                    + visitId;
	}

	public void validateGuideViewPageURL() throws Exception {
		HttpServletRequest request = getRequest();

		// Get the beek data
		beek = beekService.getBeek();

		//if (Constants.domain == null)
		//	throw new Exception("Please visit the login page");

		String guideId = request.getParameter("id");
		if (guideId == null || guideId.equals(""))
			guideId = request.getParameter("g");

		selectedGuide = guideService.getGuideDetail(guideId);
		if (selectedGuide == null){
			//throw new Exception("Guide not found");
			System.out.println("Guide not found");
			return;
		}

		if (selectedGuide.status == -1)
			throw new Exception("Guide is removed");

		selectedScene = null;

		String sceneId = request.getParameter("s");
		if (sceneId != null && !sceneId.equals(""))
			selectedScene = selectedGuide.returnScene(sceneId);

		if (selectedScene == null)
			selectedScene = selectedGuide.returnFirstScene();

		connectId = request.getParameter("c");

		userId = request.getParameter("v");

/*		flashVars = new HashMap<String, String>();
		flashVars.put("domain", Constants.domain);
		flashVars.put("assetCdn", cdnUrl);
		flashVars.put("adminSwf", getAdminSwfUrl());
		flashVars.put("gaCode", gaCode);
		flashVars.put("hotspotId", hotspotId);
		flashVars.put("userId", userId);
		flashVars.put("connectId", connectId);

		if (selectedGuide != null) {
			flashVars.put("guideId", selectedGuide.getId());
			flashVars.put("hideotherguides",
					selectedGuide.isOtherguides() ? "false" : "true");
		}

		if (selectedScene != null)
			flashVars.put("sceneId", selectedScene.getScene().getId());

		if (getSession(request).getUser() != null)
			flashVars.put("autologin", "true");*/
	}

	public GuideDetail getSelectedGuide() {
		return selectedGuide;
	}
	
	public List<GuideSection> getSelectedGuideSections() {
		List<GuideSection> temp = new ArrayList<GuideSection>();
		for(GuideSection section : selectedGuide.getGuideSections())
			if(section.parent_section_id == null)
				temp.add(section);
		
		return temp;
	}

	public void setSelectedGuide(GuideDetail selectedGuide) {
		this.selectedGuide = selectedGuide;
	}

	public GuideScene getSelectedScene() {
		return selectedScene;
	}

	public String getUserId() {return userId;}

	public void setUserId(String userId) {this.userId = userId;}
	
	public String getHotspotId() {
		return hotspotId;
	}

	public String getDomain() {
		return Constants.domain;
	}

	public String getJSPath(){
		if (Constants.domain.contains("beekdev.co"))
			return "//beekdev.co/src/resources/js/";

		if (Constants.domain.contains("beeksuper"))
			return "//beeksuper/src/resources/js/";

/*		if (Constants.domain.contains("beektest.co"))
			return "//beektest.co/src/resources/js/";*/

		return "/resources/js/";
	}

	public String getCSSPath(){
		if (Constants.domain.contains("beekdev.co"))
			return "//beekdev.co/src/resources/css/";

		if (Constants.domain.contains("beeksuper"))
			return "//beeksuper/src/resources/css/";

		if (Constants.domain.contains("beektest.co"))
			return "//beektest.co/src/resources/css/";

		return "/resources/css/";
	}

	public String getPlayerSwfUrl() {
		if (Constants.domain.contains("beekdev.co"))
			return "http://gms.beekdev.co/resources/swf/swf_player.swf";

		return cdnUrl + "/" + beek.getPlayerFileName();
	}

	public String getAdminSwfUrl() {
		if (Constants.domain.contains("beekdev.co"))
			return "http://gms.beekdev.co/resources/swf/swf_admin.swf";

		return cdnUrl + "/" + beek.getAdminFileName();
	}

	public String getScenePath() {
		if (selectedScene != null)
			return getGuidePath() + "/s" + selectedScene.getScene().getId();

		return getGuidePath();
	}

	public String getGuidePath() {
		if (selectedGuide != null)
			return "/g" + selectedGuide.getId();

		return "";
	}
	
	public String getConnectId() {
		return connectId;
	}
	
	public String getGuideTitleURl() {
		if (selectedGuide != null)
			return selectedGuide.getTitle().replace(' ', '-');

		return "";
	}
	
	public String getGuideSceneURl() {
		if (selectedGuide != null)
			return selectedGuide.getTitle().replace(' ', '-');

		return "";
	}

	/**
	 * Returns a comma delimted version of the flash vars
	 * 
	 * @return
	 */
	public String getFlashVarsCD() {
		List<String> props = new ArrayList<String>();

		Iterator<Entry<String, String>> it = flashVars.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, String> prop = it.next();
			props.add(prop.getKey() + "=" + prop.getValue());
		}
		return StringUtils.join(props, "&");
	}

	public String getFlashVarsJSON() {
		return serializer.serialize(flashVars);
	}

	public String getGuideNamesCommaDelimited() {
		List<String> props = new ArrayList<String>();

		List<GuideSection> sections = selectedGuide.getGuideSections();
		for (GuideSection section : sections) {
			for (GuideScene scene : section.getGuideScenes())
				props.add(scene.getTitleMerged());
		}
		return StringUtils.join(props, ", ");
	}
}