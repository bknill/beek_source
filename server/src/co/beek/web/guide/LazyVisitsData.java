package co.beek.web.guide;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import co.beek.pano.model.dao.entities.Visit;
import co.beek.pano.service.dataService.FavouritesService;

public class LazyVisitsData extends LazyDataModel<Visit> {

	private static final long serialVersionUID = 1L;


	private FavouritesService favouriteService;
	
	private String guideId;
	
	public LazyVisitsData(FavouritesService favouriteService, String guideId)
	{
		this.favouriteService = favouriteService;
		this.guideId = guideId;
	}
	
//	@Override
//	public List<Visit> load(int arg0, int arg1, String arg2, SortOrder arg3,
//			Map<String, String> arg4) {
//		
//		List<Visit> v = favouriteService.getVisits(guideId);
//		
//		return favouriteService.getVisits(guideId);
//	}

}
