package co.beek.web.guide.bean;

import java.util.List;

import com.google.gdata.data.analytics.DataEntry;
import com.google.gson.annotations.Expose;

/**
 * ga:eventCategory ga:eventAction ga:eventLabel ga:totalEvents
 * 
 * @author Daniel
 * 
 */
public class CityVisits {
	
	private List<String> data;
	
	@Expose
	private String city;
	@Expose
	private String sessions;
	@Expose
	private String viewsPerSessions;
	@Expose
	private String avgDuration;
	

	public CityVisits(List<String> entry) {
		this.data = entry;
		
		this.city = getCity();
		this.sessions = getSessionsString();
		this.viewsPerSessions = getPageViewsPerSessionString();
		this.avgDuration = getAvgSessionDurationString();

	}
	
	public String getCity() {
		return data.toArray()[1].toString();
	}

	public int getSessions() {
		return Integer.parseInt(data.toArray()[2].toString());
	}
	
	public String getSessionsString() {
		return data.toArray()[2].toString();
	}
	
	public double getPageViewsPerSession() {
		return Double.parseDouble(data.toArray()[3].toString());
	}
	
	public String getPageViewsPerSessionString() {
		return data.toArray()[3].toString();
	}
	
	public double getAvgSessionDuration() {
		return Double.parseDouble(data.toArray()[4].toString());
	}
	
	public String getAvgSessionDurationString() {
		return data.toArray()[3].toString();
	}
}
