package co.beek.web.guide.bean;

import java.util.List;



/**
 * 	String metric = "ga:sessions,ga:pageviewsPerSession,ga:avgSessionDuration";
 *  String dimensions = "ga:country, ga:city";
 * 
 * @author Daniel
 * 
 */
public class CountryVisits {
	private List<String> data;

	public CountryVisits(List<String> data) {
		this.data = data;
	}

	public String getCountry() {
		return data.toArray()[0].toString();
	}
	
	public String getCity() {
		return data.toArray()[1].toString();
	}

	public int getSessions() {
		return Integer.parseInt(data.toArray()[2].toString());
	}
	
	public double getPageViewsPerSession() {
		return Double.parseDouble(data.toArray()[3].toString());
	}
	
	public double getAvgSessionDuration() {
		return Double.parseDouble(data.toArray()[4].toString());
	}
}
