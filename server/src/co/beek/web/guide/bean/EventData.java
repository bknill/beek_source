package co.beek.web.guide.bean;

import com.google.gdata.data.analytics.DataEntry;

public class EventData {
	private DataEntry entry;

	private String[] labelParts;

	public EventData(DataEntry entry) {
		this.entry = entry;
		labelParts = getLabel().split("\\|");
	}

	public String getCategory() {
		return entry.stringValueOf("ga:eventCategory");
	}

	public boolean isCategory(String value) {
		String c = getCategory();
		return c != null && c.indexOf(value) > -1;
	}

	public String getAction() {
		return entry.stringValueOf("ga:eventAction");
	}

	public boolean isAction(String value) {
		String c = getAction();
		return c != null && c.indexOf(value) > -1;
	}

	public String getLabel() {
		return entry.stringValueOf("ga:eventLabel");
	}

	public long getTotal() {
		return entry.longValueOf("ga:totalEvents");
	}

	public String getHotspotTitle() {
		return labelParts[0];
	}

	public String getHotspotId() {
		return labelParts[1];
	}

	public String getSceneId() {
		return labelParts[labelParts.length - 1];
	}
}
