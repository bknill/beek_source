package co.beek.web.guide.bean;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gdata.data.analytics.DataEntry;

/**
 * query.setDimensions("ga:pagePath,ga:country,ga:city,ga:browser");
		query.setMetrics("ga:timeOnSite,ga:pageviews,ga:totalEvents");
		
 * @author danieltaylor
 *
 */
public class GuideData {
	private static Pattern GUIDE_MATCH = Pattern.compile("/g([0-9]+).*");
	private static Pattern SCENE_MATCH = Pattern.compile("/g[0-9]+/s([0-9]+).*");
	
	private DataEntry entry;
	
	private String sceneTitle;
	public String guideId;

	public GuideData(DataEntry entry) {
		this.entry = entry;
	}

	public String getPagePath() {
		return entry.stringValueOf("ga:pagePath");
	}
	
	public String getPagePathLevel1() {
		return entry.stringValueOf("ga:pagePathLevel1");
	}

	public String getGuideId() {
		Matcher matcher = GUIDE_MATCH.matcher(getPagePath());
		
		if(matcher.matches())
			return matcher.group(1);
		
		return null;
	}


	public String getCountry() {
		return entry.stringValueOf("ga:country");
	}

	public String getCity() {
		return entry.stringValueOf("ga:city");
	}
	
	public String getMonth() {
		return entry.stringValueOf("ga:month");
	}
	
	public String getVisits() {
		return entry.stringValueOf("ga:visits");
	}
	
	public String getBrowser() {
		return entry.stringValueOf("ga:browser");
	}
	
	public String getTimeOnSite() {
		return entry.stringValueOf("ga:timeOnSite");
	}
	
	public String getPageViews() {
		return entry.stringValueOf("ga:pageviews");
	}
	
	public String getPageViewsPerVisit() {
		return entry.stringValueOf("ga:pageviewsPerVisit");
	}
	
	public String getTotalEvents() {
		return entry.stringValueOf("ga:totalEvents");
	}

	public String getSceneTitle() {
		return sceneTitle;
	}

	public void setSceneTitle(String sceneTitle) {
		this.sceneTitle = sceneTitle;
	}
}
