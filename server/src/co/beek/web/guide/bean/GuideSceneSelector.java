package co.beek.web.guide.bean;

import co.beek.pano.model.dao.entities.Scene;

public class GuideSceneSelector {
	private Scene scene;
	private boolean included = true;

	public GuideSceneSelector(Scene scene) {
		this.scene = scene;
	}

	public Scene getScene() {
		return scene;
	}

	public void setIncluded(boolean included) {
		this.included = included;
	}

	public boolean isIncluded() {
		return included;
	}
}
