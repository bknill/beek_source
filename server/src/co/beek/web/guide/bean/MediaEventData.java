package co.beek.web.guide.bean;

import java.util.Comparator;

import javax.inject.Inject;

import co.beek.pano.model.dao.googleanalytics.Event;
import co.beek.pano.service.dataService.sceneService.SceneService;


class totalComparator implements Comparator<MediaEventData> {
	public int compare(MediaEventData r1, MediaEventData r2) {
		return ((int)r2.getTotal() - (int)r1.getTotal());
	}
}

public class MediaEventData {

	public static final totalComparator totalComparator = new totalComparator();

	
    @Inject
    private SceneService sceneService;
	
	private String[] labelParts;
	
	private String hotspotTitle;
	private String sceneTitle;
	private String sceneId;


	private Event data;

	public MediaEventData(Event event) {
		this.data = event;
		labelParts = getLabel().split("\\|");
		sceneId = getSceneId();
	}

	public String getCategory() {
		return data.getCategory();
	}

	public boolean isCategory(String value) {
		String c = getCategory();
		return c != null && c.indexOf(value) > -1;
	}

	public String getAction() {
		return data.getAction();
	}

	public boolean isAction(String value) {
		String c = getAction();
		return c != null && c.indexOf(value) > -1;
	}

	public String getLabel() {
		return data.getLabel();
	}

	public long getTotal() {
		return data.getTotal();
	}
	
	public long getTotalInt() {
		return data.getTotal();
	}

	public String getHotspotTitle() {
		return hotspotTitle;
	}

	public String getHotspotTitleFromLabel() {
		return labelParts[0];
	}
	
	public String getTargetSceneId() {
		return labelParts[0];
	}


	public String getHotspotId() {
		return labelParts[1];
	}

	public String getSceneId() {
		return labelParts[labelParts.length - 1];
	}

	

	public void setSceneTitle(String sceneTitle) {
		this.sceneTitle =  sceneTitle;
	}

	public void setHotspotTitle(String hotspotTitle) {
		this.hotspotTitle = hotspotTitle;
	}
	
	public void setSceneId(String sceneId) {
		this.sceneId = sceneId;
	}
	

}
