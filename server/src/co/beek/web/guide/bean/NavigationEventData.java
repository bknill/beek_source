package co.beek.web.guide.bean;

import javax.inject.Inject;

import co.beek.pano.model.dao.googleanalytics.Event;
import co.beek.pano.service.dataService.sceneService.SceneService;


public class NavigationEventData {

    @Inject
    private SceneService sceneService;
	
	private String[] labelParts;
	
	private String hotspotTitle;
	private String sceneTitle;
	private String sceneId;

	private Event data;

	public NavigationEventData(Event event) {
		this.data = event;
		labelParts = getLabel().split("\\|");
	}

	public String getCategory() {
		return data.getCategory();
	}

	public boolean isCategory(String value) {
		String c = getCategory();
		return c != null && c.indexOf(value) > -1;
	}

	public String getAction() {
		return data.getAction();
	}

	public boolean isAction(String value) {
		String c = getAction();
		return c != null && c.indexOf(value) > -1;
	}

	public String getLabel() {
		return data.getLabel();
	}

	public long getTotal() {
		return data.getTotal();
	}

	public String getHotspotTitle() {
		return hotspotTitle;
	}

	public String getHotspotTitleFromLabel() {
		return labelParts[0];
	}

	public String getHotspotId() {
		return labelParts[1];
	}

	public String getSceneId() {
		return labelParts[labelParts.length - 1];
	}

//	public String getSceneTitle() {
//			return sceneService.getScene(sceneId).getTitle();
//	}
	
	public String getSceneTitle() {
		return sceneTitle;
	}
	
	

	public void setSceneTitle(String sceneTitle) {
		this.sceneTitle =  sceneTitle;
	}

	public void setHotspotTitle(String hotspotTitle) {
		this.hotspotTitle = hotspotTitle;
	}
	
	
}
