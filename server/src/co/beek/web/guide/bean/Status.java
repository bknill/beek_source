package co.beek.web.guide.bean;

public class Status {
	private int code;
	private String descriptor;
	
	public Status(int code, String descriptor)
	{
		this.code = code;
		this.descriptor = descriptor;
	}
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getDescriptor() {
		return descriptor;
	}
	public void setDescriptor(String descriptor) {
		this.descriptor = descriptor;
	}
}
