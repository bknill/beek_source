package co.beek.web.guide.bean;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gdata.data.analytics.DataEntry;

/**
 * query.setDimensions("ga:pagePath,ga:country,ga:city,ga:browser");
		query.setMetrics("ga:timeOnSite,ga:pageviews,ga:totalEvents");
		
 * @author danieltaylor
 *
 */
public class VisitData {
	private static Pattern GUIDE_MATCH = Pattern.compile("/g([0-9]+).*");
	private static Pattern SCENE_MATCH = Pattern.compile("/g[0-9]+/s([0-9]+).*");
	
	private DataEntry entry;
	
	private String sceneTitle;

	public VisitData(DataEntry entry) {
		this.entry = entry;
	}

	public String getPagePath() {
		return entry.stringValueOf("ga:pagePath");
	}
	
	public String getPagePathLevel1() {
		return entry.stringValueOf("ga:pagePathLevel1");
	}

	public String getGuideId() {
		Matcher matcher = GUIDE_MATCH.matcher(getPagePath());
		
		if(matcher.matches())
			return matcher.group(1);
		
		return null;
	}

	public String getSceneId() {
		Matcher matcher = SCENE_MATCH.matcher(getPagePath());
		
		if(matcher.matches())
			return matcher.group(1);
		
		return null;
	}

	public String getCountry() {
		return entry.stringValueOf("ga:country");
	}

	public String getCity() {
		return entry.stringValueOf("ga:city");
	}
	
	public String getMonth() {
		return entry.stringValueOf("ga:month");
	}
	
	public String getVisits() {
		return entry.stringValueOf("ga:visits");
	}
	
	public String getBrowser() {
		return entry.stringValueOf("ga:browser");
	}
	
/*	public String getTimeOnSite() {
		return entry.stringValueOf("ga:timeOnSite");
	}*/
	
	public String getTimeOnSite() {
		return formatSecondsAsDuration(entry.longValueOf("ga:timeOnSite"));
	}
	
	public static String formatSecondsAsDuration(long seconds) {
	    long hour = seconds / 60 / 60;
	    long min = (seconds / 60) - (hour * 60);
	    long sec = seconds - (min * 60) - (hour * 60 * 60);

	    return (hour < 10 ? "0" : "") + hour + ":" + 
	           (min < 10 ? "0" : "") + min + ":" +
	           (sec < 10 ? "0" : "") + sec;
	}
	
	public String getPageViews() {
		return entry.stringValueOf("ga:pageviews");
	}
	
	public String getPageViewsPerVisit() {
		return entry.stringValueOf("ga:pageviewsPerVisit");
	}
	
	public String getTotalEvents() {
		return entry.stringValueOf("ga:totalEvents");
	}

	public String getSceneTitle() {
		return sceneTitle;
	}

	public void setSceneTitle(String sceneTitle) {
		this.sceneTitle = sceneTitle;
	}
	
	public long getVisitsWithEvent() {
		return entry.longValueOf("ga:visitsWithEvent");
	}
	
	public long getVisitsWithOutEvents() {
		long t = entry.longValueOf("ga:visits");
		long v = entry.longValueOf("ga:visitsWithEvent");
		return t - v;
	}
	
	public double getEventsPerVisitWithEvent() {
		return Math.round(entry.doubleValueOf("ga:eventsPerVisitWithEvent") * 100.0 ) / 100.0;
	}
	
}
