package co.beek.web.lead;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;







import org.primefaces.event.SlideEndEvent;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import co.beek.Constants;
import co.beek.pano.model.dao.enterprizewizard.EWCampaignResponse;
import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.enterprizewizard.EWContactsResponse;
import co.beek.pano.model.dao.enterprizewizard.EWCustomer;
import co.beek.pano.model.dao.enterprizewizard.EWLead;
import co.beek.pano.model.dao.enterprizewizard.EWLeadResponse;
import co.beek.pano.model.dao.enterprizewizard.EWReference;
import co.beek.pano.model.dao.enterprizewizard.EWReferenceResponse;
import co.beek.pano.model.dao.enterprizewizard.EWResponse;
import co.beek.pano.model.dao.enterprizewizard.EWTeam;
import co.beek.pano.model.dao.entities.Destination;
import co.beek.pano.model.dao.entities.Guide;
import co.beek.pano.model.dao.entities.Location;
import co.beek.pano.service.dataService.destinationService.DestinationService;
import co.beek.pano.service.dataService.enterprizewizardService.EWService;
import co.beek.pano.service.dataService.guideService.GuideService;
import co.beek.pano.service.dataService.locationService.LocationService;
import co.beek.util.BeekSession;
import co.beek.web.BaseController;

@Scope("session")
@Controller("LeadCampaignEmailController")
public class LeadCampaignEmailController extends BaseController implements
		Serializable {

	private static final long serialVersionUID = -1355233192481523589L;

	@Inject
	private GuideService guideService;

	@Inject
	private EWService ewService;
	

	@Value("#{buildProperties.cdnUrl}")
	private String cdnUrl;
	
	public EWContact user;
	public List<EWLead> userLeadList;
	public List<String> userLeadNames;
	public List<Integer> userLeadPercentComplete;
	
	public List<String> userGuideNames;
	public List<Guide> userGuideList;
	public List<Guide> allGuideList;
	public List<Guide> guideList;
	public List<EWReference> references;

	BeekSession session;


	public String leadId;
	public String leadEmail;
	public String leadCampaign;
	
	public boolean emailSent;
	
	public EWLead lead;
	
	public String emailText;
	
	public String campaign_name;
	public String CampaignId;
	public String emailSubject;

	

	public void preRenderView() throws IOException {
		if (redirectingToLogin())
			return;

		if (isPostback())
			return;
		
		session = getSession();
		user = session.getUser();
		EWLeadResponse response = null;

		emailText = "Hi {$contact_name},";
		
		if(user.type.contains("employee"))
		{
		allGuideList = getAllGuides();
		guideList = allGuideList;
		}
		else
		{
		userGuideList = getUserGuides();
		guideList = userGuideList;
		getUserGuideListNames();
		}
		
		if (emailSent)
		{
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Email Sent", "Email Sent");
		FacesContext.getCurrentInstance().addMessage(null, m);
		emailSent = false;
		redirect("/lead/" + leadId + "/edit");
		}
		else
		{
			System.out.println("Email wasn't sent" + emailSubject);
		}
	}
	
	@RequestMapping(value = "/campaign/{id}/email", method = RequestMethod.GET)
	public String mapView(@PathVariable("id") String id) {
		CampaignId = id;
		return "/views/lead/campaign_email.jsf?id=" + id;
	
	}
	
	
	
	public List<Guide> getUserGuideList() {
		return userGuideList;
	}
	
	public void setUserGuideList() {
		this.userGuideList = userGuideList;
	}
	
	public List<Guide> getAllGuideList() {
		return allGuideList;
	}
	
	public void setAllGuideList(List<Guide> allGuideList) {
		this.allGuideList = allGuideList;
	}

	public List<Guide> getGuideList() {
		return guideList;
	}
	
	public void setGuideList(List<Guide> guideList) {
		this.guideList = guideList;
	}
	
	public void getUserGuideListNames() {
		List<String> tempList = new ArrayList<String>();
		for (int i = 0; i < userGuideList.size(); i++)
		{
			tempList.add(userGuideList.get(i).getEditedTitle());
		}
		userGuideNames = tempList;
	}
	
	
	public List<Guide> getUserGuides() {
		List<Guide> tempList = new ArrayList<Guide>();
		for (int i = 0; i < user.guideids.size(); i++)
		{
			tempList.add(guideService.getGuide(user.guideids.get(i)));
		}
		userGuideList = tempList;

		
		return tempList;
	}
	
	public List<Guide> getAllGuides() {
		List<Guide> tempList = new ArrayList<Guide>();
		for(int i = 0; i < 280; i++)
		{
			if(guideService.getGuide(""+i) != null && guideService.getGuide(""+i).getStatus() != 0 && guideService.getGuide(""+i).getTitle() != null && !guideService.getGuide(""+i).getTitle().equals(""))
				tempList.add(guideService.getGuide(""+i));
		}
		Collections.reverse(tempList);
		return tempList;
	}
	
	public List<String> getUserGuideNames()
	{
		return userGuideNames;
	}
	
	public void setUserGuideNames(List<String> userGuideNames)
	{
		this.userGuideNames = userGuideNames;
	}
	

	public String getcampaign_name() {
		return campaign_name;
	}
	
	public void setcampaign_name(String campaign_name) {
		this.campaign_name = campaign_name;
	}

	

	
	public String getEmailText() {
		return emailText;
	}
	
	public void setEmailText(String emailText) {
		this.emailText = emailText;
	}
	
	
	public String getEmailSubject() {
		return emailSubject;
	}
	
	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}
	
	
	public void sendEmailButton() throws IOException {
		
		
		EWCampaignResponse response = null;
		try {

			response = ewService.updateCampaignEmail(session.getUsername(), session.getPassword(), CampaignId, emailText, emailSubject);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			
		}
		emailSent = true;
		emailText = null;
		emailSubject = null;
		super.redirectLeadList();
	}
		
	

	
	public void redirectToLeadList(String id) throws IOException {
		redirect("/lead/list");
	}
	
	public List<EWReference> getReferences()
	{
		EWReferenceResponse response = null;

        try {

            response = ewService.getReferences(session.getUsername(), session.getPassword());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        List<EWReference> referenceList = response.getReferences();
        
    return referenceList;
	}
	
	public void setReferences(List<EWReference> references) {
		this.references = references;
	}


	
}
