package co.beek.web.lead;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Calendar;  
import java.text.ParseException;
import java.text.SimpleDateFormat; 
import java.util.Date;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SlideEndEvent;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import co.beek.Constants;
import co.beek.pano.model.dao.enterprizewizard.EWCampaign;
import co.beek.pano.model.dao.enterprizewizard.EWCampaignResponse;
import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.enterprizewizard.EWCustomer;
import co.beek.pano.model.dao.enterprizewizard.EWLead;
import co.beek.pano.model.dao.enterprizewizard.EWLeadResponse;
import co.beek.pano.model.dao.enterprizewizard.EWResponse;
import co.beek.pano.model.dao.enterprizewizard.EWTeam;
import co.beek.pano.model.dao.entities.Destination;
import co.beek.pano.model.dao.entities.Guide;
import co.beek.pano.model.dao.entities.Location;
import co.beek.pano.model.dao.entities.Visit;
import co.beek.pano.service.dataService.FavouritesService;
import co.beek.pano.service.dataService.destinationService.DestinationService;
import co.beek.pano.service.dataService.enterprizewizardService.EWService;
import co.beek.pano.service.dataService.guideService.GuideService;
import co.beek.pano.service.dataService.locationService.LocationService;
import co.beek.util.BeekSession;
import co.beek.web.BaseController;

@Scope("session")
@Controller("LeadCreateController")
public class LeadCreateController extends BaseController implements
		Serializable {

	private static final long serialVersionUID = -7803472918002319087L;

	@Inject
	private GuideService guideService;

	@Inject
	private EWService ewService;
	
	@Inject
	private FavouritesService favouritesService;
	
	@Inject
	private LocationService locationService;

	@Inject
	private DestinationService destinationsService;

	@Value("#{buildProperties.cdnUrl}")
	private String cdnUrl;
	
	public Visit leadVisit;
	public EWContact user;
	public List<EWLead> userLeadList;
	public List<String> userLeadNames;
	public List<Integer> userLeadPercentComplete;

	public List<EWCampaign> Campaigns;
	public List<Location> allLocations;

	private String searchText = ""; //
	
	BeekSession session;

	private String customerId;
	
	public String leadId;
	
	public List<EWLead> lead;
	
	public String firstName;
	public String lastName;
	public String email;
	public String telephone;
	public String orgName;
	public String orgWebsite;
	public String nextAction;
	public String nextActionDate;
	public Date nextActionDateFormat;
	public String new_nextActionDate;
	public String comments;
	public String newComments;
	public List<String> campaign_name;
	public List<EWCampaign> campaignList;
	public List<String> leadCampaigns;
	public String newCampaign;	
	public String new_campaign_name;
	public EWLead sliderLead;
	public int sliderChange;
	public String percent_complete;
	public Float percentComplete;
	public String percentCompleteString;
	public Integer percentCompleteInt;
	public Integer SlidepercentCompleteInt;

	public boolean saved;
	public boolean created;
	

	@SuppressWarnings("unused")
	public void preRenderView() throws IOException {
		if (redirectingToLogin())
			return;
		
		percentCompleteInt = 0;

		session = getSession();
		user = session.getUser();
		SlidepercentCompleteInt = 25;
		percentCompleteInt = 25;
		firstName = null;
		lastName = null;
		email = null;
		telephone = null;
		orgName = null;
		orgWebsite = null;
		
		EWLeadResponse response = null;
		
		if (saved)
		{
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Saved Lead", "Saved Lead");
		FacesContext.getCurrentInstance().addMessage(null, m);
		saved = false;
		}
		
	}
	
	   public List<EWCampaign> getCampaigns() throws IOException {
	        EWCampaignResponse response = null;

		        try {
		
		            response = ewService.getCampaigns(session.getUsername(), session.getPassword());
		        } catch (Exception e) {
		            // TODO Auto-generated catch block
		            e.printStackTrace();
		        }
		        List<EWCampaign> campaignList = response.getCampaigns();
	        return campaignList;


	    }
	
	public List<String> CampaignName(){
		
		
		return campaign_name;
		
	
	}
	
	public List<String> getLeadCampaigns()
	{
		return leadCampaigns;
	}
	
	public void setLeadCampaigns(List<String> leadCampaigns)
	{
		this.leadCampaigns = leadCampaigns;
	}
	
	public void setNewCampaignName(String newCampaignName)
	{
		this.new_campaign_name = newCampaignName;
	}
	
	public String getnewCampaign() {
		return newCampaign;
	}
	
	public void setnewCampaign(String newCampaign) {
		this.newCampaign = newCampaign;
	}
	
	public List<String> getCampaignNames(){
		List<String> campaignNames = new ArrayList<String>();
		for(int i =0; i < campaignList.size(); i++) 
		{
			campaignNames.add(campaignList.get(i).getcampaign_name());
		}
		return campaignNames;
	}
	
	public void redirectLeadEmail() throws IOException {
		
		super.redirectLeadEmail(leadId);
	}
	
	public Integer getPercentCompleteInteger()
	{
		return percentCompleteInt;
	}
	
	public void setPercentCompleteInteger(Integer percentCompleteInt) {
		this.percentCompleteInt = percentCompleteInt;
	}
	public void showSaveMessage() {
		
			
	}
	
	public List<String> getUserLeadNames()
	{
		return userLeadNames;
	}
	
	public void setUserLeadNames(List<String> userLeadNames)
	{
		this.userLeadNames = userLeadNames;
	}
	
	public List<EWLead> getUserLeadList()
	{
		return userLeadList;
	}
	
	public void setUserLeadList(List<EWLead> userLeadList)
	{
		this.userLeadList = userLeadList;
	}
	

	public List<Integer> getUserLeadPercentComplete()
	{
		return userLeadPercentComplete;
	}
	
	public void setUserLeadPercentComplete(List<Integer> userLeadPercentComplete)
	{
		this.userLeadPercentComplete = userLeadPercentComplete;
	}


	public Float getpercentComplete() {
		Float percentComplete = Float.parseFloat(percent_complete);
		return percentComplete;
	}
	
	public String setpercentComplete() {
		String percent_complete = Float.toString(percentComplete);
		return percent_complete;
	}
	
	public void onSlideEnd(SlideEndEvent event) {  
		
		
		SlidepercentCompleteInt = event.getValue();
		
    } 
	
	 public String getNextActionDateString() {
			SimpleDateFormat formatter = new SimpleDateFormat("MMM dd yyyy");
		 	String new_nextActionDate = formatter.format(nextActionDate);
			return new_nextActionDate;
	 }
			
	
	
	public String getComments() {
		return comments;
	}
	
	
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	public String getNextAction() {
		return nextAction;
	}
	
	public void setNextAction(String nextAction) {
		this.nextAction = nextAction;
	}
	
	public String getNextActionDate() {
		return nextActionDate;
	}
	
	
	public void setNextActionDate(String nextActionDate) {
		this.nextActionDate = nextActionDate;
	}
	public String getNewComments() {
		return newComments;
	}
	
	public void setNewComments(String newComments) {
		this.newComments = newComments;
	}
	
	public List<String> getcampaign_name() {
		return campaign_name;
	}
	
	public void setcampaign_name(List<String> campaign_name) {
		this.campaign_name = campaign_name;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getTelephone() {
		return telephone;
	}
	
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	
	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getOrgWebsite() {
		return orgWebsite;
	}
	
	public void setOrgWebsite(String orgWebsite) {
		this.orgWebsite = orgWebsite;
	}
	
	public String getnew_campaign_name() {
		return new_campaign_name;
	}
	
	public void setnew_campaign_name(String new_campaign_name) {
		this.new_campaign_name = new_campaign_name;
	}

	
	
	@RequestMapping(value = "/lead/create", method = RequestMethod.GET)
	public String mapView() {
		return "/views/lead/create.jsf";
	
	}
	

	
		
	
	public void redirectToLeadEdit(String id) throws IOException {
		redirect("/lead/" + id + "/edit");
	}
	

	public String getPercentCompleteIntString() {
		return Integer.toString(percentCompleteInt);
	}
	
	public void saveLead(Integer n) throws IOException {
	
			EWLeadResponse response = null;
			try {
			
			response = ewService.createLead(session.getUsername(), session.getPassword());
			lead = response.getLeads();
			leadId = lead.toString().replace("[", "").replace("]", "");
			} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			}
			
			
			created=true;
		
			saved = true;
			if(n == 1) {redirectToLeadEdit(leadId);};
			if(n == 2) {redirectCreateLead();}
	}

	

	public void addCampaign() throws IOException {
		
	    System.out.print(new_campaign_name);
		EWLeadResponse response = null;
		try {
			
			response = ewService.UpdateLeadCampaign(session.getUsername(), session.getPassword(), leadId, new_campaign_name);
			new_campaign_name = null;
			preRenderView();
			RequestContext.getCurrentInstance().update("j_idt25:campaigns");
			RequestContext.getCurrentInstance().update("j_idt25:leadCampaigns");
		    
		
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
		


	}


	
	
	
}
