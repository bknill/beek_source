package co.beek.web.lead;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.Map.Entry;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.lang.StringBuilder;

import co.beek.pano.model.dao.enterprizewizard.*;
import co.beek.pano.model.dao.googleanalytics.VisitByUser;
import co.beek.pano.model.dao.googleanalytics.VisitByUserEvent;
import co.beek.pano.service.dataService.googleAnalytics.GAService;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SlideEndEvent;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gdata.client.analytics.DataQuery;
import com.google.gdata.data.analytics.DataEntry;
import com.google.gdata.util.AuthenticationException;

import co.beek.Constants;
import co.beek.pano.model.dao.entities.Destination;
import co.beek.pano.model.dao.entities.Guide;
import co.beek.pano.model.dao.entities.Location;
import co.beek.pano.model.dao.entities.Visit;
import co.beek.pano.model.dao.googleanalytics.VisitByPath2;
import co.beek.pano.service.dataService.FavouritesService;
import co.beek.pano.service.dataService.destinationService.DestinationService;
import co.beek.pano.service.dataService.enterprizewizardService.EWService;
import co.beek.pano.service.dataService.googleAnalytics.BeekAnalayticsService;
import co.beek.pano.service.dataService.guideService.GuideService;
import co.beek.pano.service.dataService.locationService.LocationService;
import co.beek.util.BeekSession;
import co.beek.util.DateUtil;
import co.beek.web.BaseController;

@Scope("session")
@Controller("LeadEditController")
@ViewScoped
public class LeadEditController extends BaseController implements
		Serializable {

	private static final long serialVersionUID = -7803472918002319087L;

	@Inject
	private GuideService guideService;

	@Inject
	private EWService ewService;
	
	@Inject
	private FavouritesService favouritesService;
	
	@Inject
	private LocationService locationService;

	@Inject
	private GAService gaService;

	@Autowired
	LeadEmailController leadEmailController;

	@Value("#{buildProperties.cdnUrl}")
	private String cdnUrl;
	
	public Visit leadVisit;
	public EWContact user;
	public List<EWLead> userLeadList;
	public List<String> userLeadNames;
	public List<Integer> userLeadPercentComplete;
    private String startDate;
    private String endDate;
	public List<String> Campaigns;

	private String searchText = ""; 
	
	BeekSession session;

	private String customerId;



	public String leadId;
	
	public EWLead lead;
	
	public String firstName;
	public String contactID;
	public String lastName;
	public String email;
	public String telephone;
	public String orgName;
	public String orgWebsite;
	public String nextAction;
	public String nextActionDate;
	public Date nextActionDateFormat;
	public String new_nextActionDate;
	public String comments;
	public String newComments;
	public List<String> campaign_name;
	public List<String> leadCampaigns;
	public String newCampaign;	
	public String new_campaign_name;
	public EWLead sliderLead;
	public int sliderChange;
	public String percentComplete;
	public String percentCompleteString;
	public Integer percentCompleteInt;
	public Integer SlidepercentCompleteInt;



	public boolean visitsDataLoaded;
	public String callNotes;
	public List<Visit> leadVisits;
	public VisitByUser selectedVisit;


	public List<VisitByUser> visitsData;
	public DateFormat df = new SimpleDateFormat("MMM dd yyyy");


	public List<EWLeadEmail> leadEmails;
	
	public boolean saved;
	public boolean pcSet;

	public void preRenderView() throws IOException, ParseException {
		if (redirectingToLogin())
			return;



		loadData(leadId);

		leadEmailController.setData(lead);
		leadEmailController.preRenderView();


		if (saved)
		{
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Saved Lead", "Saved Lead");
		FacesContext.getCurrentInstance().addMessage(null, m);
		saved = false;
		}

	}



	public void loadData(String id) throws ParseException {


		session = getSession();
		user = session.getUser();
		EWLeadResponse response = null;
		leadId = id;

		try {

			response = ewService.getLead(session.getUsername(), session.getPassword(), id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		lead = response.getLeads().get(0);

		newComments = lead.new_comments;
		searchText = "";
		firstName = lead.contact_name;
		lastName = lead.contact_surname;
		email = lead.contact_email;
		telephone = lead.contact_telephone;
		orgName = lead.business_name;
		orgWebsite = lead.business_website;
		nextAction = lead.next_action;
		nextActionDate = lead.next_action_date;
		comments = lead.comments;
		leadCampaigns = lead.campaign_name;
		leadVisits = getVisits();

		RequestContext.getCurrentInstance().update("leadDetails");
		RequestContext.getCurrentInstance().update("leadDetails:leadEmailList");
				updateNextActionDate();

		visitsDataLoaded = false;
		visitsData = null;
		callNotes = null;



		if(!pcSet) {
			percentComplete = lead.percent_complete;
			percentCompleteInt = Integer.parseInt(percentComplete.substring(0, percentComplete.indexOf(".")));
		}

	}


	public void loadSecondaryData() {

		if(leadVisits.size() > 0)
		{

			startDate = DateUtil.getGAStartDate();
			endDate = DateUtil.getCurrentDate();

			try {
				visitsData = loadVisitsData();

				visitsDataLoaded = true;
				RequestContext.getCurrentInstance().update("leadDetails:trackedVisitsContainer");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}


	   public List<EWCampaign> getCampaigns() throws IOException {
	        EWCampaignResponse response = null;

		        try {
		
		            response = ewService.getCampaigns(session.getUsername(), session.getPassword());
		        } catch (Exception e) {
		            // TODO Auto-generated catch block
		            e.printStackTrace();
		        }
		        List<EWCampaign> campaignList = response.getCampaigns();
	        return campaignList;


	    }
	
	public void redirectLeadEmail() throws IOException {

		leadEmailController.setData(lead);
	}
	
	

	
	public Integer getPercentCompleteInt()
	{
		return percentCompleteInt;
	}
	
	public void setPercentCompleteInt(Integer percentCompleteInt) {
		this.percentCompleteInt = percentCompleteInt;
		
	}
	
	
	public void showSaveMessage() {
		
			
	}
	
	public List<String> getUserLeadNames()
	{
		return userLeadNames;
	}
	
	public void setUserLeadNames(List<String> userLeadNames)
	{
		this.userLeadNames = userLeadNames;
	}
	
	public List<String> getLeadCampaigns()
	{
		return leadCampaigns;
	}
	
	public void setLeadCampaigns(List<String> leadCampaigns)
	{
		this.leadCampaigns = leadCampaigns;
	}
	
	public void setNewCampaignName(String newCampaignName)
	{
		this.new_campaign_name = newCampaignName;
	}
	
	public String getnewCampaign() {
		return newCampaign;
	}
	
	public void setnewCampaign(String newCampaign) {
		this.newCampaign = newCampaign;
	}
	

	public List<EWLead> getUserLeadList()
	{
		return userLeadList;
	}
	
	public void setUserLeadList(List<EWLead> userLeadList)
	{
		this.userLeadList = userLeadList;
	}


	public String getpercentComplete() {
		return percentComplete;
	}
	
	public void setpercentComplete(String percentComplete) {
		
		this.percentComplete = percentComplete;
	}
	
	public void onSlideEnd(SlideEndEvent event) throws IOException {
		
		SlidepercentCompleteInt = event.getValue();
		percentCompleteInt = event.getValue();
		percentComplete = Integer.toString(SlidepercentCompleteInt);
		pcSet = true;
    } 
	
	 public void updateNextActionDate() throws ParseException {

		 if(nextActionDate == null)
			 return;

		   nextActionDateFormat = df.parse(nextActionDate);
		   Date today = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
           
		   if(today.after(nextActionDateFormat)){
				df.setTimeZone(TimeZone.getTimeZone("NZ"));
            	nextActionDate = df.format(today);
            	nextActionDateFormat = today;
		   }
	 }
			
	
	
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	public String getnew_campaign_name() {
		return new_campaign_name;
	}
	
	public void setnew_campaign_name(String new_campaign_name) {
		this.new_campaign_name = new_campaign_name;
	}

	
	public String getNextAction() {
		return nextAction;
	}
	
	public void setNextAction(String nextAction) {
		this.nextAction = nextAction;
	}
	
	public String getNextActionDate() {
		return nextActionDate;
	}
	
	public void setNextActionDate(String nextActionDate) {
		this.nextActionDate = nextActionDate;
	}
	
	public Date getNextActionDateFormat() {
		return nextActionDateFormat;
	}
	
	public void setNextActionDateFormat(Date nextActionDateFormat) {
		this.nextActionDateFormat = nextActionDateFormat;
		nextActionDate = df.format(nextActionDateFormat);
	}
	
	
	public String getNewComments() {
		return newComments;
	}
	
	public void setNewComments(String newComments) {
		this.newComments = newComments;
	}
	
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getcontactID() {
		return contactID;
	}
	
	public void setcontactID(String contactID) {
		this.contactID = contactID;
	}
	
	public String getTelephone() {
		return telephone;
	}
	
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	
	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getOrgWebsite() {
		return orgWebsite;
	}
	
	public void setOrgWebsite(String orgWebsite) {
		this.orgWebsite = orgWebsite;
	}


	public String getCallNotes() {
		return callNotes;
	}

	public void setCallNotes(String callNotes) {
		this.callNotes = callNotes;
	}



	@RequestMapping(value = "/lead/{id}/edit", method = RequestMethod.GET)
	public String mapView(@PathVariable("id") String id) {
		leadId = id;
		return "/views/lead/edit.jsf?id=" + id;
	
	}
	
	public void sortUserLeadList()
	{
		//using selection sort, slow but the easiest to write
		Integer i, j;
		Integer min = 0;
		
		for(j = 0; j < userLeadList.size()-1; j++)
		{
			min = j;
			for (i = j+1; i< userLeadList.size(); i++)
			{
				if (userLeadList.get(i).pc < userLeadList.get(min).pc)
					min = i;
			}
			
			if (min != j)
			{
			EWLead temp = userLeadList.get(j);
			userLeadList.set(j, userLeadList.get(min));
			userLeadList.set(min, temp);
			}
		}
		}
	
	public void sortUserLeadListOp()
	{
		//using selection sort, slow but the easiest to write
		Integer i, j;
		Integer max = 0;
		
		for(j = userLeadList.size()-1; j > 0; j--)
		{
			max = j;
			for (i = j-1; i> 0; i--)
			{
				if (userLeadList.get(i).pc > userLeadList.get(max).pc)
					max = i;
			}
			
			if (max != j)
			{
			EWLead temp = userLeadList.get(j);
			userLeadList.set(j, userLeadList.get(max));
			userLeadList.set(max, temp);
			}
		}
		}
		
	
	public void redirectToLeadEdit(String id) throws IOException {
		redirect("/lead/" + id + "/edit");
	}
	

	
	public String getGuideNameFromId(String guideId)
	{
		Guide tempGuide = guideService.getGuide(guideId);
		return tempGuide.getTitle();
	}	
	
	public String getGuideThumbFromId(String guideId)
	{
		Guide tempGuide = guideService.getGuide(guideId);
		return tempGuide.getThumbName();
	}	

	public String getPercentCompleteIntString() {
		return Integer.toString(percentCompleteInt);
	}
	
	public void saveLead() throws IOException {
	
		
		EWLeadResponse response = null;
		try {
			response = ewService.updateLead(session.getUsername(), session.getPassword(), leadId, firstName, lastName,
			email, telephone, orgName, orgWebsite, newComments, percentComplete, nextAction, nextActionDate);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.getStackTrace());

		}
		saved = true;
		pcSet = false;


		RequestContext context = RequestContext.getCurrentInstance();
		context.update("Leads:leadlist");

	}

	
public void addCampaign() throws IOException {
	
	    System.out.print(new_campaign_name);
		EWLeadResponse response = null;
		try {
			
			response = ewService.UpdateLeadCampaign(session.getUsername(), session.getPassword(), leadId, new_campaign_name);
			new_campaign_name = null;
			//preRenderView();
			RequestContext.getCurrentInstance().update("j_idt25:campaigns");
			RequestContext.getCurrentInstance().update("j_idt25:leadCampaigns");
		    
		
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

	}


	
	
	public void newCampaignButton() {
		try {

			EWCampaignResponse response = ewService.newCampaign(session.getUsername(), session.getPassword(),newCampaign);	
			if(response != null){
				 new_campaign_name = newCampaign;
				 addCampaign();
				 newCampaign = null;
				RequestContext.getCurrentInstance().update("j_idt24:tabView:newCampaign");
				RequestContext.getCurrentInstance().update("j_idt24:tabView:campaignsList");
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

			
	}

	public String getLeadId() {
		return leadId;
	}

	public void setLeadId(String leadId) {
		this.leadId = leadId;
	}


	public List<EWLeadEmail> getLeadEmails() {

		session = getSession();
		user = session.getUser();
		EWLeadEmailResponse response = null;

		try {

			response = ewService.getLeadCommunications(session.getUsername(), session.getPassword(), leadId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		leadEmails = response.getLeadsEmails();
		Collections.reverse(leadEmails);
		return leadEmails;
	}


	public void addCall() {

		EWLeadResponse response = null;

		if(callNotes == null)
			callNotes = "-";

		try {
			response = ewService.updateLeadCallNotes(session.getUsername(), session.getPassword(), leadId, callNotes);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

		callNotes = null;
		RequestContext context = RequestContext.getCurrentInstance();
		context.update("leadDetails:leadEmailList");
	}


	public List<String> getVisitKey() {
		String email1 = email;
		List<Visit> tempList = new ArrayList<Visit>();
		tempList = favouritesService.getVisitsByEmail(email1);
		List<String> tempKeyList = new ArrayList<String>();
		for(int i =0; i < tempList.size(); i++)
		{
			tempKeyList.add(tempList.get(i).getKey());
		}
		return tempKeyList;
	}

	public List<Visit> getVisits() {
		return favouritesService.getVisitsByEmail(email);
	}

	public List<VisitByUser> getVisitsData() {
		return visitsData;
	}


	public void setVisitsData(List<VisitByUser> visitsData) {
		this.visitsData = visitsData;
	}


	public List<VisitByUser> loadVisitsData() throws Exception {

		List<VisitByUser> list = new ArrayList<VisitByUser>();

		List<Visit> visits = getVisits();
		Collections.reverse(visits);

		Date currentDate = new Date();
		long day90 = 90l * 24 * 60 * 60 * 1000;

		for(Visit visit : visits){

			if(visit.visitData == null && visit.getTimestamp().after(new Date((currentDate.getTime() - day90)))) {
				gaService.getVisitData(visit, startDate, endDate);
				favouritesService.saveVisit(visit);
			}

			List<VisitByUser> visitsByUser = visit.getVisitsByUser();
			List<VisitByUserEvent> visitByUserEvents = visit.getVisitsByUserEvents();

			if(visitsByUser  == null || visit.visitData == null)
				continue;

			for(VisitByUser vbu : visitsByUser) {
				vbu.guide = guideService.getGuide(visit.guideId);

				for(VisitByUserEvent vbue : visitByUserEvents)
					if (vbu.getDate().equals(vbue.getDate()))
						vbu.events.add(vbue);


				list.add(vbu);
			}

		}

		Collections.sort(list, VisitByUser.visitByUserComparator);

		return list;
	}

	public List<Visit> getLeadVisits() {
		return leadVisits;
	}

	public void setLeadVisits(List<Visit> leadVisits) {
		this.leadVisits = leadVisits;
	}

	public boolean isVisitsDataLoaded() {
		return visitsDataLoaded;
	}

	public void setVisitsDataLoaded(boolean visitsDataLoaded) {
		this.visitsDataLoaded = visitsDataLoaded;
	}

	public VisitByUser getSelectedVisit() {
		return selectedVisit;
	}

	public void setSelectedVisit(VisitByUser selectedVisit) {
		this.selectedVisit = selectedVisit;
	}
	
}
