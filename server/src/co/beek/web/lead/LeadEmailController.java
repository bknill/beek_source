package co.beek.web.lead;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.Map.Entry;


import co.beek.pano.model.dao.entities.*;
import co.beek.pano.service.dataService.FavouritesService;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SlideEndEvent;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import co.beek.Constants;
import co.beek.pano.model.dao.enterprizewizard.EWCampaign;
import co.beek.pano.model.dao.enterprizewizard.EWCampaignResponse;
import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.enterprizewizard.EWContactsResponse;
import co.beek.pano.model.dao.enterprizewizard.EWCustomer;
import co.beek.pano.model.dao.enterprizewizard.EWLead;
import co.beek.pano.model.dao.enterprizewizard.EWReference;
import co.beek.pano.model.dao.enterprizewizard.EWReferenceResponse;
import co.beek.pano.model.dao.enterprizewizard.EWLeadResponse;
import co.beek.pano.model.dao.enterprizewizard.EWResponse;
import co.beek.pano.model.dao.enterprizewizard.EWTeam;
import co.beek.pano.service.dataService.destinationService.DestinationService;
import co.beek.pano.service.dataService.enterprizewizardService.EWService;
import co.beek.pano.service.dataService.guideService.GuideService;
import co.beek.pano.service.dataService.locationService.LocationService;
import co.beek.util.BeekSession;
import co.beek.web.BaseController;

@Scope("session")
@Controller("LeadEmailController")
@ViewScoped
public class LeadEmailController extends BaseController implements
		Serializable {

	private static final long serialVersionUID = -1355233192481523589L;

	@Inject
	private GuideService guideService;

	@Inject
	private FavouritesService favouritesService;

	@Inject
	private EWService ewService;
	
	@Inject
	private LocationService locationService;

	@Inject
	private DestinationService destinationsService;

	@Value("#{buildProperties.cdnUrl}")
	private String cdnUrl;
	
	public EWContact user;
	public List<EWLead> userLeadList;
	public List<String> userLeadNames;
	public List<Integer> userLeadPercentComplete;
	public int percentComplete;
	
	public List<String> userGuideNames;
	public List<Guide> userGuideList;
	public List<Guide> allGuideList;
	public List<Guide> guideList;
	public List<EWReference> references;
	
	public List<Location> allLocations;
	
	BeekSession session;
	
	public String leadId;
	public String leadEmail;
	public String leadName;
	public String leadBusinessName;
	public String leadWebsite;
	public String leadCampaign;
	public String newReferenceName;
	
	public boolean emailSent;
	
	public EWLead lead;
	
	public String emailText;
	public String emailSubject;
	
	public String campaign_name;
	public List<String> leadCampaigns;
	
	
	public void preRenderView() throws IOException {

		session = getSession();
		user = session.getUser();
		emailSubject = null;

		
/*		if(user.type.contains("employee"))
		{
			allGuideList = getAllGuides();
			guideList = allGuideList;
		}
		else
		{
			userGuideList = getUserGuides();
			guideList = userGuideList;
			getUserGuideListNames();
		}
		
		

		
		if (emailSent)
		{
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Email Sent", "Email Sent");
		FacesContext.getCurrentInstance().addMessage(null, m);
		emailSent = false;
		redirect("/lead/" + leadId + "/edit");
		}
		else
		{
			System.out.println("Email wasn't sent" + emailSubject);
		}*/
	}

	public void setData(EWLead lead){

		newReferenceName = null;
		emailSubject = null;

		leadId = lead.id;

		if(lead.contact_email != null)
		{
			leadEmail = lead.contact_email;
			leadBusinessName = lead.business_name;
			leadWebsite = lead.business_website;
			leadCampaigns = lead.campaign_name;
			leadName = lead.contact_name;
		}

		emailText = "Hi " + leadName;
	}
	
	public List<Guide> getUserGuideList() {
		return userGuideList;
	}
	
	public void setUserGuideList() {
		this.userGuideList = userGuideList;
	}
	
	public List<Guide> getAllGuideList() {
		return allGuideList;
	}
	
	public void setAllGuideList(List<Guide> allGuideList) {
		this.allGuideList = allGuideList;
	}
	
	public List<String> getleadCampaigns() {
		return leadCampaigns;
	}
	
	public void setleadCampaigns(List<String> leadCampaigns) {
		this.leadCampaigns = leadCampaigns;
	}
	
	public List<Guide> getGuideList() {
		return guideList;
	}
	
	public void setGuideList(List<Guide> guideList) {
		this.guideList = guideList;
	}
	
	public List<EWReference> getReferences()
	{
		EWReferenceResponse response = null;

        try {

            response = ewService.getReferences(session.getUsername(), session.getPassword());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        List<EWReference> referenceList = response.getReferences();
        
    return referenceList;
	}
	
	public void setReferences(List<EWReference> references) {
		this.references = references;
	}
	
	
	public void getUserGuideListNames() {
		List<String> tempList = new ArrayList<String>();
		for (int i = 0; i < userGuideList.size(); i++)
		{
			tempList.add(userGuideList.get(i).getEditedTitle());
		}
		userGuideNames = tempList;
	}
	
	
	public List<Guide> getUserGuides() {
		List<Guide> tempList = new ArrayList<Guide>();
		for (int i = 0; i < user.guideids.size(); i++)
		{
			if(guideService.getGuide(user.guideids.get(i)).status > -1)
			tempList.add(guideService.getGuide(user.guideids.get(i)));
		}
		userGuideList = tempList;

		
		return tempList;
	}
	
	public List<Guide> getAllGuides() {
		List<Guide> tempList = new ArrayList<Guide>();
		guideList = guideService.getAllGuides();
		for(Guide guide : guideList)
		{ 
			if(guide.status != -1)
				tempList.add(guide);
		}
		
		Collections.sort(tempList, Guide.idComparator);
		return tempList;
	}
	
	public List<String> getUserGuideNames()
	{
		return userGuideNames;
	}
	
	public void setUserGuideNames(List<String> userGuideNames)
	{
		this.userGuideNames = userGuideNames;
	}
	
	public List<EWLead> getUserLeadList()
	{
		return userLeadList;
	}
	
	public void setUserLeadList(List<EWLead> userLeadList)
	{
		this.userLeadList = userLeadList;
	}
	
	public String getLeadEmail() {
		return leadEmail;
	}
	
	public void setLeadEmail(String leadEmail) {
		this.leadEmail = leadEmail;
	}
	
	public String getLeadName() {
		return leadName;
	}
	
	public void setLeadName(String leadName) {
		this.leadName = leadName;
	}
	
	public String getLeadBusinessName() {
		return leadBusinessName;
	}
	
	public void setLeadBusinessName(String leadBusinessName) {
		this.leadBusinessName = leadBusinessName;
	}
	
	public String getLeadWebsite() {
		return leadWebsite;
	}
	
	public void setLeadWebsite(String leadWebsite) {
		this.leadWebsite = leadWebsite;
	}
	
	
	public String getNewReferenceName() {
		return newReferenceName;
	}
	
	public void setNewReferenceName(String newReferenceName) {
		this.newReferenceName = newReferenceName;
	}
	
	public String getcampaign_name() {
		return campaign_name;
	}
	
	public void setcampaign_name(String campaign_name) {
		this.campaign_name = campaign_name;
	}

	
	public int getPercentComplete() {
		return percentComplete;
	}
	
	public void setPercentComplete(int percentComplete) {
		this.percentComplete = percentComplete;
	}
	
	public String getEmailText() {
		return emailText;
	}
	
	public void setEmailText(String emailText) {
		this.emailText = emailText;
	}
	
	
	public String getEmailSubject() {
		return emailSubject;
	}
	
	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}


/*	@RequestMapping(value = "/lead/{id}/email", method = RequestMethod.GET)
	public String mapView(@PathVariable("id") String id) {
		leadId = id;
		return "/views/lead/email.jsf?id=" + id;
	
	}*/
	
	
	
	public void sendEmailButton() throws IOException {
		
		
		EWLeadResponse response = null;
		try {
			if(campaign_name == null){campaign_name = "";}
			response = ewService.updateLeadIntroEmail(session.getUsername(), session.getPassword(), leadId, emailText, emailSubject, campaign_name);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	
		}
		
		if(newReferenceName != null)
		{
			EWReferenceResponse ReferenceResponse = null;
			
			try {
				ReferenceResponse = ewService.newReferences(session.getUsername(), session.getPassword(), newReferenceName, emailText);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		
			}
			
			
		}
		
		emailSent = true;
		RequestContext context = RequestContext.getCurrentInstance();
		context.update("leadDetails:leadEmailList");
	}

	public void addGuideLink(){

		String guideId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("guideId");
		GuideDetail guide = guideService.getGuideDetail(guideId);
		String email = leadEmail;

		Visit visit = new Visit();
		String userId = UUID.randomUUID().toString();
		visit.setKey(userId);
		visit.setEmail(email);
		visit.guideId = guideId;

		favouritesService.logVisit(visit);

		String url = "http://beek.co/g" + guideId + "/s" + guide.returnFirstScene().getSceneId() + "/v" + userId;
		emailText = emailText + "<br/><a href='" + url + "'target='_blank' > " + guide.getTitle() + "</a>";
		RequestContext.getCurrentInstance().update("editor");
	}
	
}
