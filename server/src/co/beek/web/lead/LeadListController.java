package co.beek.web.lead;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.SlideEndEvent;
import org.primefaces.model.ScheduleEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import co.beek.Constants;
import co.beek.pano.model.dao.enterprizewizard.EWCampaign;
import co.beek.pano.model.dao.enterprizewizard.EWCampaignResponse;
import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.enterprizewizard.EWCustomer;
import co.beek.pano.model.dao.enterprizewizard.EWLead;
import co.beek.pano.model.dao.enterprizewizard.EWLeadResponse;
import co.beek.pano.model.dao.enterprizewizard.EWResponse;
import co.beek.pano.model.dao.entities.Guide;
import co.beek.pano.model.dao.entities.Location;
import co.beek.pano.model.dao.entities.Visit;
import co.beek.pano.service.dataService.destinationService.DestinationService;
import co.beek.pano.service.dataService.enterprizewizardService.EWService;
import co.beek.pano.service.dataService.guideService.GuideService;
import co.beek.pano.service.dataService.locationService.LocationService;
import co.beek.pano.service.dataService.FavouritesService;
import co.beek.util.BeekSession;
import co.beek.web.BaseController;

@Scope("session")
@Controller("LeadListController")
public class LeadListController extends BaseController implements
		Serializable {

	private static final long serialVersionUID = -1556328919286681960L;

	@Inject
	private GuideService guideService;

	@Inject
	private EWService ewService;
	
	@Inject
	private FavouritesService favouritesService;
	
	@Value("#{buildProperties.cdnUrl}")
	private String cdnUrl;

	@Autowired
	LeadEditController leadEditController;

	@Autowired
	LeadListToDoController leadListToDoController;


	
	public EWContact user;
	public List<EWLead> userLeadList;
	public List<EWCampaign> Campaigns;
    public List<String> campaign_name;
    public String campaign_name_search;	
    public List<EWCampaign> campaignList;
	public List<String> userLeadNames;
	public List<String> userLeadCampaigns;
	public List<Integer> userLeadPercentComplete;
	public int percentComplete;
	public Visit leadVisit;
	public LeadListScheduleController leadListScheduleController;
	public LeadListToDoController LeadListToDoController;
	public boolean disable;
	
	public EWLead sliderLead;
	public int sliderChange;
	
	public List<EWLead> showLeadList;



	public EWLead lead;

	public List<EWLead> searchResults;
	public boolean searched;
	public boolean filtered;
	
	public List<Location> allLocations;

	public String searchTerm;
	
	BeekSession session;

	private String customerId;
	
	public String nextAction;
	public String nextActionDate;
	public String firstName;
	public String lastName;
	public String email;
	public String telephone;
	public String name;
	public String website;
	public String newCampaign;	
	public String new_campaign_name;
	public List<String> campaignsList;
	public String campaignId;


	public void preRenderView() throws IOException, ParseException {
		
		
		if (redirectingToLogin())
			return;

		if (isPostback())
			return;

		session = getSession();
		
		user = session.getUser();
	
		disable= true;

		updateShowLeadList();


		leadEditController.leadId = lead.id;
		leadEditController.preRenderView();
		
	}
	
	public void updateShowLeadList(){

		getLeadList();
		populateUserLeadNamesAndPercentComplete();
		sortUserLeadList();
		Collections.reverse(userLeadList);
		lead = userLeadList.get(0);

		if(campaign_name_search != null)
			filterLeads(userLeadList);
		else
			showLeadList = userLeadList;

	}
	
	public List<EWLead> getShowLeadList() {
		return showLeadList;
	}
	
	public void setShowLeadList(List<EWLead> showLeadList) {
		this.showLeadList = showLeadList;
	}
	
	public List<String> getUserLeadNames()
	{
		return userLeadNames;
	}
	
	public void setUserLeadNames(List<String> userLeadNames)
	{
		this.userLeadNames = userLeadNames;
	}
	
	

    public List<EWCampaign> getCampaigns() throws IOException {
        EWCampaignResponse response = null;

	        try {
	
	            response = ewService.getCampaigns(session.getUsername(), session.getPassword());
	        } catch (Exception e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
	        campaignList = response.getCampaigns();
	        
        return campaignList;


    }
    
	public List<EWLead> getUserLeadList()
	{
		return userLeadList;
	}
	
	public void setUserLeadList(List<EWLead> userLeadList)
	{
		this.userLeadList = userLeadList;
	}
	

	public List<Integer> getUserLeadPercentComplete()
	{
		return userLeadPercentComplete;
	}
	
	public void setUserLeadPercentComplete(List<Integer> userLeadPercentComplete)
	{
		this.userLeadPercentComplete = userLeadPercentComplete;
	}
	
	public int getPercentComplete() {
		return percentComplete;
	}
	
	public void setPercentComplete(int percentComplete) {
		this.percentComplete = percentComplete;
	}
	

	@RequestMapping(value = "/lead/list", method = RequestMethod.GET)
	public String leadList() {
		return "/views/lead/list.jsf";
	}
	
	

	
	public void sortUserLeadList()
	{
		//using selection sort, slow but the easiest to write
		Integer i, j;
		Integer min = 0;
		
		for(j = 0; j < userLeadList.size()-1; j++)
		{
			min = j;
			for (i = j+1; i< userLeadList.size(); i++)
			{
				if (userLeadList.get(i).pc < userLeadList.get(min).pc)
					min = i;
			}
			
			if (min != j)
			{
			EWLead temp = userLeadList.get(j);
			userLeadList.set(j, userLeadList.get(min));
			userLeadList.set(min, temp);
			}
		}
	}
	
		
	
	public void populateUserLeadNamesAndPercentComplete() {
		userLeadNames = new ArrayList<String>();

		userLeadPercentComplete = new ArrayList<Integer>();
		for (int i = 0; i < userLeadList.size(); i++)
		{
			userLeadNames.add(userLeadList.get(i).getBusinessName());
			userLeadPercentComplete.add(Integer.parseInt(userLeadList.get(i).getPercentComplete().split("\\.", 2)[0]));
		}
	}


	public void loadLead(String id) throws IOException, ParseException {
		leadEditController.loadData(id);

	}

	public void updateLeadDetailForm(){

		RequestContext context = RequestContext.getCurrentInstance();
		context.update("leadDetails");
	}

	public void getLeadList()
	{ 


		EWLeadResponse response = null;
		
		try {
			response = ewService.getUserLeads(session.getUsername(), session.getPassword());
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//use this for if it comes back null
		}
		for (int i = 0; i<response.getLeads().size();i++)
		{
			response.getLeads().get(i).bn = response.getLeads().get(i).business_name;
			response.getLeads().get(i).pc = Integer.parseInt(response.getLeads().get(i).percent_complete.split("\\.", 2)[0]);
		}
		userLeadList = response.getLeads();
			
	}
	

	public String getCustomerId() {
		return customerId;
	}
	
	
	public String getemail() {
		return email;
	}

	public void setCustomerId(String customerId) throws IOException {
		this.customerId = customerId;
	}

	public void goToCustomerPage() throws IOException {
		redirect("/customer/" + customerId + "/data");
	}

	

	public String getBuildCdn() {
		return "http://gms."+Constants.domain+"/resources/swf/swf_map.swf";
	}


	public void redirectCreateLead() throws IOException {
		super.redirectCreateLead();
	}
	
	public String getSearchTerm() {
		return searchTerm;
	}
	
	public void setSearchTerm(String searchTerm) {
		this.searchTerm = searchTerm;
	}
	
	public void searchButton() {
		searchLeads(userLeadList);
	}
	
	public String getcampaignId() {
		return campaignId;
	}
	
	public void setcampaignId(String campaignId) {
		this.campaignId = campaignId;
	}
	
	public String getcampaign_name_search() {
		return campaign_name_search;
	}
	
	public void setcampaign_name_search(String campaign_name_search) {
		this.campaign_name_search = campaign_name_search;
	}
	
	public void searchCampaign() {
		getCampaignName();
		disable = false;
		showLeadList = filterLeads(userLeadList);
	}
	
	public void getCampaignName() {
		for (int i = 0; i < campaignList.size(); i++)
		{if (campaignList.get(i).id.contains(campaignId))
		campaign_name_search = campaignList.get(i).campaign_name;
		}
	}
	
	
	public void searchLeads(List<EWLead> searchLeads) {
		
		System.out.println("searchLeads("+searchTerm+")");
		List<EWLead> foundLeads = new ArrayList<EWLead>();
		for (int i = 0; i < searchLeads.size(); i++)
		{
			if (searchLeads.get(i).business_name == null)
				continue;
			if (searchLeads.get(i).business_name != null || !searchLeads.get(i).business_name.isEmpty())
			{

				if (searchLeads.get(i).business_name.toLowerCase().contains(searchTerm.toLowerCase()) || searchLeads.get(i).getId().contains(searchTerm.toLowerCase()))
				foundLeads.add(searchLeads.get(i));
			}
			
			if (searchLeads.get(i).contact_name != null)
			{

				if (searchLeads.get(i).contact_name.toLowerCase().contains(searchTerm.toLowerCase()))
				foundLeads.add(searchLeads.get(i));
			}
			
			if (searchLeads.get(i).contact_surname != null)
			{

				if (searchLeads.get(i).contact_surname.toLowerCase().contains(searchTerm.toLowerCase()))
				foundLeads.add(searchLeads.get(i));
			}
		}
		
		showLeadList = foundLeads;
		searchTerm = null;

	}

	public List<EWLead> filterLeads(List<EWLead> searchLeads) {

		if(campaign_name_search == null)
			return searchLeads;

		List<EWLead> foundLeads = new ArrayList<EWLead>();
		for (int i = 0; i < searchLeads.size(); i++)
		{
			if (searchLeads.get(i).campaign_name == null)
				continue;
			if (searchLeads.get(i).campaign_name != null || !searchLeads.get(i).campaign_name.isEmpty())
			{
				if (searchLeads.get(i).campaign_name.contains(campaign_name_search)) 
				foundLeads.add(searchLeads.get(i));
			}
		}
		
		return foundLeads;
	}
	

	public void onSlideEnd(SlideEndEvent event) {  
		
		sliderChange = event.getValue();

    }  
	

	
	public void sliderChangeSave(EWLead lead) {
		sliderLead = lead;
		String editedSliderChange = ""+sliderChange;
		if (editedSliderChange.equals("0."))
			editedSliderChange="0.0";
		try {
			EWLeadResponse response = ewService.updateLeadPercentComplete(session.getUsername(), session.getPassword(), session.getId(), lead.id, ""+editedSliderChange);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/*	public List<String> getVisitKey(String email) {

		List<Visit> tempList = new ArrayList<Visit>();
		tempList = favouritesService.getVisitsByEmail(email);
			List<String> tempKeyList = new ArrayList<String>();
			for(int i =0; i < tempList.size(); i++)
			{
				tempKeyList.add(tempList.get(i).getKey());
			}
			return tempKeyList;
	}
	
	public List<Visit> getVisits(String email) throws UnsupportedEncodingException {
		byte ptext[] = email.getBytes();
		String Email = new String(ptext, "UTF-8");
		List<Visit> tempList = new ArrayList<Visit>();
		tempList = favouritesService.getVisitsByEmail(Email);
			return tempList;
	}*/
	
	public String getGuideNameFromId(String guideId)
	{
		Guide tempGuide = guideService.getGuide(guideId);
		return tempGuide.getTitle();
	}	
	
	public String getGuideThumbFromId(String guideId)
	{
		Guide tempGuide = guideService.getGuide(guideId);
		return tempGuide.getThumbName();
	}
	
	public void redirectCampaignEmail() throws IOException {
		
		super.redirectCampaignEmail(campaignId);
	}

    public boolean isDisable() {
       return disable;
    }
    public void setDisable(boolean disable) {
       this.disable = disable;
    }

	public EWLead getLead() {
		return lead;
	}

	public void setLead(EWLead lead) {
		this.lead = lead;
	}

	public void oldLeads(){
		leadListToDoController.leadList = filterLeads(userLeadList);
		showLeadList = leadListToDoController.getOldToDoList();

		RequestContext context = RequestContext.getCurrentInstance();
		context.update("Leads:leadlist");
	}

	public void todayLeads(){
		leadListToDoController.leadList = filterLeads(userLeadList);
		showLeadList = leadListToDoController.getTodayToDoList();

		RequestContext context = RequestContext.getCurrentInstance();
		context.update("Leads:leadlist");
	}

	public void allLeads(){

		showLeadList = userLeadList;

		RequestContext context = RequestContext.getCurrentInstance();
		context.update("Leads:leadlist");

	}

	public void newLead(){
		EWLeadResponse response = null;
		try {

			response = ewService.createLead(session.getUsername(), session.getPassword());
			List<EWLead> leads = response.getLeads();
			String leadId = leads.toString().replace("[", "").replace("]", "");
			loadLead(leadId);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

	}
	
}
