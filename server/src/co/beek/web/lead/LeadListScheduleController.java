package co.beek.web.lead;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.annotation.*;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.ScheduleEntryResizeEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.SlideEndEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import co.beek.Constants;
import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.enterprizewizard.EWLead;
import co.beek.pano.service.dataService.enterprizewizardService.EWService;
import co.beek.util.BeekSession;
import co.beek.web.BaseController;

@ManagedBean
@Scope("session")
@Controller("LeadListScheduleController")
public class LeadListScheduleController extends BaseController implements
		Serializable {

	private static final long serialVersionUID = -1556328919286681960L;


	@Inject
	private EWService ewService;
	
	@Value("#{buildProperties.cdnUrl}")
	private String cdnUrl;
	
	public EWContact user;
	public BeekSession session;
	public String id;
	public String title;
	public Date date;
	public Object LeadId;
	public DefaultScheduleModel eventModel;  
	public ScheduleEvent event = new DefaultScheduleEvent();  
	public static List<EWLead> leadList;

	public void preRenderView() throws IOException {
		if (redirectingToLogin())
			return;

		if (isPostback())
			return;

		session = getSession();
		user = session.getUser();
	}



	public LeadListScheduleController() throws ParseException {
	        eventModel = new DefaultScheduleModel(); 
	        updateEventModel();

	    }
	    
	    private void updateEventModel()  throws ParseException{

	        for (int i = 0; i < leadList.size(); i++)
				if((leadList.get(i).next_action != null && !leadList.get(i).next_action.isEmpty())&& (leadList.get(i).next_action_date != null && !leadList.get(i).next_action_date.isEmpty()))
					if(dateIsInFuture(leadList.get(i).next_action_date))
						eventModel.addEvent(new DefaultScheduleEvent("" + leadList.get(i).next_action + " " + leadList.get(i).business_name.replace("'", ""), nextLeadActionDate(leadList.get(i).next_action_date), nextLeadActionDate(leadList.get(i).next_action_date), LeadId));

	    }

	      
	    public DefaultScheduleModel getEventModel() throws ParseException {
	    	updateEventModel();
	        return eventModel;  
	    }

	   public void setEventModel(DefaultScheduleModel eventModel) {
		this.eventModel = eventModel;
	}


	public Date nextLeadActionDate(String nextActionDateString) throws ParseException {
	    	DateFormat df = new SimpleDateFormat("MMM dd yyyy");
	    	Date nextActionDate  = df.parse(nextActionDateString);

	        return nextActionDate;
	    }

		public Boolean dateIsInFuture(String nextActionDateString) throws ParseException {
			Date today = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
			DateFormat df = new SimpleDateFormat("MMM dd yyyy");
			df.setTimeZone(TimeZone.getTimeZone("NZ"));
			Date nextActionDate  = df.parse(nextActionDateString);


			if(today.after(nextActionDate))
				return false;

			return true;
		}
	    
	    public ScheduleEvent getEvent() {  
	        return event;  
	    }  
	  
	    public void setEvent(ScheduleEvent event) {  
	        this.event = event;  
	    }  
	    

	      
	    public void onEventSelect(SelectEvent selectEvent) throws IOException {  
	        ScheduleEvent event = (ScheduleEvent) selectEvent.getObject();  
	        id = (String) event.getData();
	        redirect(id + "/edit");
	    }  

}
