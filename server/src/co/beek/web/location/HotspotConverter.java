package co.beek.web.location;

import co.beek.pano.model.dao.entities.Hotspot;
import co.beek.pano.service.dataService.hotspotService.HotspotService;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by Ben on 21/10/2015.
 */
@Named
public class HotspotConverter implements Converter {

    @Inject
    public HotspotService hotspotService;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String submittedValue) {
        if (submittedValue == null || submittedValue.isEmpty()) {
            return null;
        }

        try {
            return hotspotService.getHotspot(submittedValue);
        } catch (NumberFormatException e) {
            throw new ConverterException(new FacesMessage(String.format("%s is not a valid Hotspot ID", submittedValue)), e);
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object modelValue) {
        if (modelValue == null) {
            return "";
        }


        if (modelValue instanceof Hotspot) {
            return String.valueOf(((Hotspot) modelValue).getId());
        } else {
            throw new ConverterException(new FacesMessage(String.format("%s is not a valid Hotspot", modelValue)));
        }
    }

}