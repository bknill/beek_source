package co.beek.web.location;

import co.beek.pano.model.dao.entities.Location;
import co.beek.pano.service.dataService.locationService.LocationService;


import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by Ben on 21/10/2015.
 */
@Named
public class LocationConverter implements Converter {

    @Inject
    public LocationService locationService;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String submittedValue) {
        if (submittedValue == null || submittedValue.isEmpty()) {
            return null;
        }

        try {
            return locationService.getLocation(submittedValue);
        } catch (NumberFormatException e) {
            throw new ConverterException(new FacesMessage(String.format("%s is not a valid User ID", submittedValue)), e);
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object modelValue) {
        if (modelValue == null) {
            return "";
        }


        if (modelValue instanceof Location) {
            return String.valueOf(((Location) modelValue).getId());
        } else {
            throw new ConverterException(new FacesMessage(String.format("%s is not a valid Location", modelValue)));
        }
    }

}