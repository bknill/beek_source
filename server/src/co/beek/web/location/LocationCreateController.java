package co.beek.web.location;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import co.beek.pano.model.dao.enterprizewizard.EWCreateResponse;
import co.beek.pano.service.dataService.enterprizewizardService.EWService;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FlowEvent;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import co.beek.pano.model.beans.LocationMapModel;
import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.entities.Destination;
import co.beek.pano.model.dao.entities.Location;
import co.beek.pano.model.dao.entities.Scene;
import co.beek.pano.service.dataService.locationService.LocationService;
import co.beek.pano.service.dataService.sceneService.SceneService;
import co.beek.util.BeekSession;
import co.beek.web.BaseController;

@Scope("session")
@Controller("LocationCreateController")
public class LocationCreateController extends BaseController implements
        Serializable {
    private static final long serialVersionUID = -1081621548447371624L;

    @Inject
    private LocationService locationService;

    @Inject
    private SceneService sceneService;

    private Location location;

    private LocationMapModel locationMapModel;

    private List<Scene> scenes;

    @Inject
	public EWService ewService;

    @RequestMapping(value = "/location/create", method = RequestMethod.GET)
    public String mapView() {
        return "/views/location/create.jsf";
    }

    public void preRenderView() throws IOException {
        if (redirectingToLogin())
            return;

        if (isPostback())
            return;

        BeekSession session = getSession();
        EWContact contact = session.getUser();

        location = new Location();
        location.setStatus(Location.STATE_NEW);
        location.setTitle("New Location");
        location.setTeamId("CUSTOMER_1_");
        location.setDestinationId("1");
        //TODO make the teams work/take them out
        //location.setTeamId(contact.getTeams().get(0).getTeams());
        //location.setDestination(1);

        locationMapModel = new LocationMapModel(location);

        scenes = new ArrayList<Scene>();
    }

    public Location getLocation() {
        return location;
    }

    public LocationMapModel getLocationMapModel() {
        return locationMapModel;
    }

    public List<Destination> getDestinations() {
        return getSession().getDestinations();
    }

    public void handleDestinationChange() {
        Destination destination = getDestination(location.getDestinationId());
        location.setDestination(destination);
    }

    public String onFlowProcess(FlowEvent event) throws IOException {
        // Must save before adding scenes
        if (event.getOldStep().equals("locationTab")){
            locationService.saveLocation(location);
        	processSubmit();
        }

        if (event.getOldStep().equals("scenesTab")) {
            reloadScenesFromDB();

            if (event.getNewStep().equals("finishTab")) {
                if (scenes.size() > 0) {
                    location.setSceneCount(scenes.size());
                    location.setDefaultSceneId(scenes.get(0).getId());
                   // locationService.saveLocation(location);
                    
                    
                    
                } else {
                    // Stop progress if they have not created any scenes
                    showError("You must add at least one scene.");
                    return event.getOldStep();
                }
            }
        }
        return event.getNewStep();
    }

    private void reloadScenesFromDB() {
        this.scenes = sceneService.getAllScenesForLocation(location.getId());
        RequestContext.getCurrentInstance().update("scenesTab");
    }

    public List<Scene> getScenes() {
        return scenes;
    }

    public void addScene() {
        HttpServletRequest req = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();

        String sceneName = req.getParameter("sceneName");
        String sceneId = req.getParameter("sceneId");

        if(sceneId == null || sceneId.equals("")){
            return;
        }

        Scene scene = new Scene(location);

        if(sceneName == null || sceneName.equals("")){
            scene.setTitle("NEW SCENE");
        }else{
            scene.setTitle(sceneName);
        }

        scene = sceneService.saveScene(scene);
        scenes.add(scene);

        if (scenes.size() == 1) {
            location.setSceneCount(scenes.size());
            location.setDefaultSceneId(scenes.get(0).getId());
            locationService.saveLocation(location);
        }

        // pass the data back for javascript access
        RequestContext context = RequestContext.getCurrentInstance();
        context.addCallbackParam("temp_sceneId", sceneId);
        context.addCallbackParam("sceneId", scene.getId());
        context.addCallbackParam("sceneName", scene.getTitle());
    }

    private Destination getDestination(String destinationId) {
        List<Destination> destinations = getSession().getDestinations();
        for (Destination destination : destinations)
            if (destination.getId().equals(destinationId))
                return destination;

        return null;
    }

    public Scene getDefaultScene() {
        if (location.getDefaultSceneId() != null)
            return sceneService.getScene(location.getDefaultSceneId());

        return null;
    }

    public void processSubmit() throws IOException {
        //location.setStatus(Location.STATE_LIVE);
        // default scene has changed, submit
        location.setDestinationId("1");
        location.setTeamId("CUSTOMER_1_");
        locationService.saveLocation(location);
        BeekSession session = getSession();
        EWContact user = session.getUser();
        List<String> updateLocationID = session.getLocationID();
        updateLocationID.add(location.getId());
        session.setLocationID(updateLocationID);
        user.setLocationID(updateLocationID);
        saveLocationToCRM();

        redirectLocationEdit(location.getId());
    }
    
    
	private void saveLocationToCRM()
	{

        EWCreateResponse response = null;
		try {

            response = ewService.createLocation(getSession().getUsername(), getSession().getPassword(), location);
			location.setEwId(response.getResult());
            locationService.saveLocation(location);

		} catch (Exception e) {
			FacesMessage m2 = new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Location Failed", "There was an error saving this location to the CRM: " + e.getMessage());
			FacesContext.getCurrentInstance().addMessage(null, m2);
		}
		
	}
}