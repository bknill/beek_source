package co.beek.web.location;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import co.beek.pano.model.dao.entities.Scene;
import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import co.beek.pano.model.beans.LocationMapModel;
import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.entities.Location;
import co.beek.pano.service.dataService.locationService.LocationService;
import co.beek.pano.service.dataService.sceneService.SceneService;
import co.beek.util.BeekSession;
import co.beek.web.BaseController;

@Scope("session")
@Controller("LocationDashboardController")
public class LocationDashboardController extends BaseController implements
		Serializable {
	private static final long serialVersionUID = -379260275937322983L;

	@Inject
	private LocationService locationService;

	@Inject
	private SceneService sceneService;

	private Location location;

	private LocationMapModel locationMapModel;
	
	public List<Location> allLocations;
	public List<Location> mapLocations;
	public List<Location> userLocations;
	
	public EWContact user;
	
	private String lat;
	private String lon;
	
	private boolean searched = false;
	private String searchTerm = "";
	private List<Location> searchResults = new ArrayList<Location>();
	
	public String locationTitleKey;
	

	public void preRenderView() throws IOException {
		if (redirectingToLogin())
			return;

		BeekSession session = getSession();
		user = session.getUser();

		allLocations = locationService.getAllLocations();

		userLocations = getUserLocations();
		
		locationMapModel = new LocationMapModel(location);
		
		if(searched)
		{
			mapLocations = searchResults;
			searched = false;
		}
		else
		{
			if (user.type.toLowerCase().equals("employees"))
			mapLocations = allLocations;
			else
			{
				if (userLocations.size() != 0)
				mapLocations = userLocations;
				else
				mapLocations = new ArrayList<Location>();
				
			}
		}

		Collections.reverse(mapLocations);
	}

	public void fillAllLocations() {
		allLocations = locationService.getAllLocations();
	}
	
	public List<String> getAllLocationsTitles() {
		List<String> allLocationTitles = new ArrayList<String>();
		for (int i = 0; i < allLocations.size(); i++)
		{
			if (allLocations.get(i).getTitle().contains("'"))
			{
			String unencoded = allLocations.get(i).getTitle();
			String encoded = unencoded.replaceAll("'", "%27");
			allLocationTitles.add("'"+encoded+"'");
			}
			else
				allLocationTitles.add("'"+allLocations.get(i).getTitle()+"'");
		}
		return allLocationTitles;
	}
	
	public List<String> getMapLocationsTitles() {
		List<String> mapLocationTitles = new ArrayList<String>();
		for (int i = 0; i < mapLocations.size(); i++)
		{
			if (mapLocations.get(i).getTitle().contains("'"))
			{
			String unencoded = mapLocations.get(i).getTitle();
			String encoded = unencoded.replaceAll("'", "%27");
			mapLocationTitles.add("'"+encoded+"'");
			}
			else
				mapLocationTitles.add("'"+mapLocations.get(i).getTitle()+"'");
		}
		return mapLocationTitles;
	}
	
	public List<String> getAllLocationIDs() {
		List<String> allLocationIDs = new ArrayList<String>();
		for (int i = 0; i < allLocations.size(); i++)
		{
			allLocationIDs.add(allLocations.get(i).getId());
		}
		return allLocationIDs;
	}
	
	public List<String> getMapLocationIDs() {
		List<String> mapLocationIDs = new ArrayList<String>();
		for (int i = 0; i < mapLocations.size(); i++)
		{
			mapLocationIDs.add(mapLocations.get(i).getId());
		}
		return mapLocationIDs;
	}
	
	
	public List<String> getAllLats() {
		List<String> allLats = new ArrayList<String>();
		for (int i = 0; i < allLocations.size(); i++) 
			{
			allLats.add(""+allLocations.get(i).getLatitude());
			}
		return allLats;
	}
	
	public List<String> getMapLats() {
		List<String> mapLats = new ArrayList<String>();
		for (int i = 0; i < mapLocations.size(); i++) 
			{
			mapLats.add(""+mapLocations.get(i).getLatitude());
			}
		return mapLats;
	}
	
	public List<String> getAllLons() {
		List<String> allLons = new ArrayList<String>();
		for (int i = 0; i < allLocations.size(); i++) 
			{
			allLons.add(""+allLocations.get(i).getLongitude());
			}
		return allLons;
	}
	
	public List<String> getMapLons() {
		List<String> mapLons = new ArrayList<String>();
		for (int i = 0; i < mapLocations.size(); i++) 
			{
			mapLons.add(""+mapLocations.get(i).getLongitude());
			}
		return mapLons;
	}
	
	public String getLat() {
		return lat;
	}
	
	public String getLon() {
		return lon;
	}
	
	public String getSearchTerm() {
		return searchTerm;
	}
	
	public void setSearchTerm(String searchTerm) {
		this.searchTerm = searchTerm;
	}

	@RequestMapping(value = "/location/dashboard", method = RequestMethod.GET)
	public String mapView() {
		return "/views/location/dashboard.jsf";
	}
	
	public LocationMapModel getLocationMapModel() {
		return locationMapModel;
	}

	public void goToCreateLocation() throws IOException
	{
		redirect("/location/create");
	}
	
	public void showAllLocationsButton()
	{
		searched = false;

		if (!user.type.toLowerCase().equals("employees"))
			mapLocations = userLocations;
		else
			mapLocations = allLocations;


		RequestContext requestContext = RequestContext.getCurrentInstance();
		requestContext.execute("updateMarkers(" + getMapLocationsTitles() +"," + getMapLats() + "," + getMapLons() +  "," + getMapLocationIDs() +")");
	}

	public void searchButton() {
		if (!user.type.toLowerCase().equals("employees"))
			searchAllLocations(userLocations);
		else
			searchAllLocations(allLocations);


	}

	public void searchButtonUpdateMap() {
		if (!user.type.toLowerCase().equals("employees"))
			searchAllLocations(userLocations);
		else
			searchAllLocations(allLocations);


		RequestContext requestContext = RequestContext.getCurrentInstance();
		requestContext.execute("updateMarkers(" + getMapLocationsTitles() +"," + getMapLats() + "," + getMapLons() +  "," + getMapLocationIDs() +")");

	}
	
	public void searchAllLocations(List<Location> searchLocations) {
		List<Location> foundLocations = new ArrayList<Location>();
		for (int i = 0; i < searchLocations.size(); i++)
		{
			if (searchLocations.get(i).getTitle().toLowerCase().contains(searchTerm.toLowerCase()) || searchLocations.get(i).getId().contains(searchTerm))
				foundLocations.add(searchLocations.get(i));//.getTitle());
		}
		mapLocations = foundLocations;


		}


	
	public List<Location> getUserLocations() {
		List<Location> foundUserLocations = new ArrayList<Location>();
		for (int i = 0; i < user.locationids.size(); i++)
		{
			if(locationService.getLocation(user.locationids.get(i)) != null)
				foundUserLocations.add(locationService.getLocation(user.locationids.get(i)));
		}
		userLocations = foundUserLocations;
		return userLocations;
	}
	
	public List<Location> getSearchResults() {
		return searchResults;
	}


	public List<Location> getAllLocations() {
		return allLocations;
	}

	public void setAllLocations(List<Location> allLocations) {
		this.allLocations = allLocations;
	}

	public List<Location> getMapLocations() {
		return mapLocations;
	}

	public void setMapLocations(List<Location> mapLocations) {
		this.mapLocations = mapLocations;
	}

	public void setUserLocations(List<Location> userLocations) {
		this.userLocations = userLocations;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public List<Scene> getLocationScenes(){
		if(location != null)
			return sceneService.getAllScenesForLocation(location.getId());
		else
			return null;
	}
}
