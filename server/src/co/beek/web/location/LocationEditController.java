package co.beek.web.location;

import java.io.IOException;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.*;

import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import co.beek.Constants;
import co.beek.pano.model.dao.entities.*;
import co.beek.pano.service.dataService.FileUploadService;
import co.beek.pano.service.dataService.fileService.FileService;
import co.beek.pano.service.dataService.hotspotService.HotspotService;
import co.beek.pano.service.dataService.taskService.TaskService;
import co.beek.pano.service.dataService.uploadService.S3UploadService;
import com.google.gson.Gson;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import org.apache.commons.io.FileUtils;
import org.jets3t.service.S3ServiceException;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import co.beek.pano.model.beans.LocationMapModel;
import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.enterprizewizard.EWContactsResponse;
import co.beek.pano.model.dao.enterprizewizard.EWCustomer;
import co.beek.pano.model.dao.enterprizewizard.EWCustomersResponse;
import co.beek.pano.model.dao.enterprizewizard.EWTeam;
import co.beek.pano.model.dao.hotspots.BoardSign;
import co.beek.pano.model.dao.hotspots.Bubble;
import co.beek.pano.service.dataService.enterprizewizardService.EWService;
import co.beek.pano.service.dataService.enterprizewizardService.EWServiceImpl;
import co.beek.pano.service.dataService.guideService.GuideService;
import co.beek.pano.service.dataService.locationService.LocationService;
import co.beek.pano.service.dataService.sceneService.SceneService;
import co.beek.util.BeekSession;
import co.beek.web.BaseController;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

@Scope("session")
@Controller("LocationEditController")
public class LocationEditController extends BaseController implements
		Serializable {
	private static final long serialVersionUID = -169436702612286165L;

	@Value("#{buildProperties.assetsBucket}")
	private String assetsBucket;

	@Inject
	private S3UploadService s3UploadService;

	@Inject
	private LocationService locationService;

	@Inject
	private EWService ewService;

	@Inject
	private SceneService sceneService;

	@Inject
	private GuideService guideService;

	@Inject
	private HotspotService hotspotService;

	@Inject
	TaskService taskService;

	@Inject
	FileUploadService fileUploadService;

	public Location location;

	private List<Scene> scenes;
	public String sceneTitleUrl;
	private List<EWTeam> teams;

	public int activeIndex;

	private List<EWContact> users;
	private List<String> usersNames;
	private List<String> usersEmails;

	private boolean userReload;

	public String emailAddress;

	public String title;

	public String email;

	public String phone;

	public String infoURL;

	private LocationMapModel locationMapModel;

	public EWServiceImpl ewServiceImpl = new EWServiceImpl();

	private BeekSession session;

	private Scene selectedScene;

	private Hotspot selectedHotspot;

	private List<Hotspot> selectedSceneHotspots;

	private Scene selectedHotspotLoadScene;

	private HotspotBehaviour selectedHotspotBehaviour;
	private List<HotspotBehaviour> selectedHotspotBehaviours;

	//public VideoSettings selectedSceneVideoSettings;

	@RequestMapping(value = "/location/{id}/edit", method = RequestMethod.GET)
	public String mapView(@PathVariable("id") String id) {
	return "/views/location/edit.jsf?id=" + id;
	}

	public void preRenderView() throws IOException {

		if (redirectingToLogin())
			return;

		if (isPostback())
			return;

		session = getSession();
		usersNames = new ArrayList<String>();
		users = new ArrayList<EWContact>();

		String locationId = getRequest().getParameter("id");
		this.location = locationService.getLocation(locationId);

        if(this.location == null) return;

		title = location.getTitle();
		email = location.getEmail();
		phone = location.getPhone();
		infoURL = location.getInfoURL();

		getLocationUsers(locationId);
		this.scenes = sceneService.getAllScenesForLocation(locationId);

        Iterator<Scene> iterator =  this.scenes.iterator();
        while(iterator.hasNext()){
            if(iterator.next().getStatus() == -1){
                iterator.remove();
            }
        }
        this.locationMapModel = new LocationMapModel(location);

		if(!isUserReload())
			activeIndex = 0;

		userReload=false;

	}

	public boolean isUserReload() {
		return userReload;
	}

	public void addUser()
	{
		EWContactsResponse ewContactsResponse = null;
		EWContactsResponse ewContactsResponse1 = null;
		EWContactsResponse ewContactsResponse2 = null;
		try {
			ewContactsResponse = ewService.searchUserByEmail(session.getUsername(), session.getPassword(), emailAddress);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//use this for if it comes back null
		}
		if (ewContactsResponse.getContacts().size() == 0)
		{
			try {
				ewContactsResponse = ewService.addUserByEmail(session.getUsername(), session.getPassword(), emailAddress, location.getId());
				System.out.println("added a user");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				//use this for if it comes back null
			}
			//http://crm.beek.co/ewws/EWCreate/.json?$KB=BeekOrderly&$table=Contacts&$lang=en&$login=gms&$password=B33kb33k&email=ben@beek.co&new_location=103
			//need to create new user with that email

		}
		else
		{
			try {
				ewContactsResponse = ewService.updateLocationID(session.getUsername(), session.getPassword(), ewContactsResponse.getContacts().get(0).id, ewContactsResponse.getContacts().get(0).type, location.getId());
				System.out.println("added a user");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				//use this for if it comes back null
			}
			//update user with that locationID
			//http://crm.beek.co/ewws/EWUpdate/.json?$KB=BeekOrderly&$table=contacts&$login=gms&$lang=en&$password=B33kb33k&id=1593&new_location=104
		}
		reloadUserTab();
		}

	public void removeUser(String email)
	{
		EWContactsResponse ewContactsResponse = null;
		EWContactsResponse ewContactsResponse1 = null;
		try {
			ewContactsResponse = ewService.searchUserByEmail(session.getUsername(), session.getPassword(), email);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//use this for if it comes back null
		}
		if (ewContactsResponse.getContacts().size() != 0)
		try {
			ewContactsResponse1 = ewService.removeUserByEmail(session.getUsername(), session.getPassword(), ewContactsResponse.getContacts().get(0).id, ewContactsResponse.getContacts().get(0).type, location.getId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("no user to remove");
			e.printStackTrace();
			//use this for if it comes back null
		}

		reloadUserTab();
	}

	public void reloadUserTab() {

		userReload = true;
		try {
		    Thread.sleep(7000);
		} catch(InterruptedException ex) {
		    Thread.currentThread().interrupt();
		}
		activeIndex = 3;
		try {
			redirectLocationEdit(location.getId());
			}
		catch (Exception e) {
			e.printStackTrace();
		}

	}

	public int getActiveIndex() {
		return activeIndex;
	}

	public void setActiveIndex(int activeIndex) {
		this.activeIndex = activeIndex;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getEmailAddress()
	{
		return emailAddress;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}


	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone()
	{
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getInfoURL()
	{
		return infoURL;
	}

	public void setInfoURL(String infoUrl) {
		this.infoURL = infoUrl;
	}



	public String removeSpaces(String title){
		title = title.replace(' ', '-');
		return title;
	}

	public void setUsersEmails(List<String> emails) {
		usersEmails = emails;
	}
	public List<String> getUsersEmails()
	{
		return usersEmails;
	}




	public void printEmail()
	{
		System.out.println(emailAddress + "printed!");
	}



	public Location getLocation() {
		return location;
	}

	public void getLocationUsers(String locationID) {
		EWContactsResponse ewContactsResponse = null;
		try {
			ewContactsResponse = ewService.getLocationUsers(session.getUsername(), session.getPassword(), locationID);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		users = ewContactsResponse.getContacts();
		usersEmails = new ArrayList<String>();
		for (int i = 0; i < ewContactsResponse.getContacts().size(); i++)
		{
			String temp =ewContactsResponse.getContacts().get(i).email;
			usersEmails.add(temp);
		}
			printUsersNames();
	}

	public void printUsersNames() {
		for (int i = 0; i < users.size(); i++)
		{
			String temp = "";
			try
			{
				if(!users.get(i).first_name.equals(null))
				temp += users.get(i).first_name + " ";
			}
			catch (Exception e)
			{
				System.out.println("firstname contains null!");
			}
			try
			{
				if(!users.get(i).last_name.equals(null))
				temp += users.get(i).last_name;
			}
			catch (Exception e)
			{
				System.out.println("lastname null!");
			}
			//String temp = users.get(i).first_name + " " + users.get(i).last_name;
			System.out.println(temp);
			if (temp.equals(""))
				temp = "[No name]";
			usersNames.add(temp);

			}
		}


	public List<String> getUsersNames() {
		return usersNames;
	}

	public List<EWContact> getUsers() {
		return users;
	}

	public LocationMapModel getLocationMapModel() {
		return locationMapModel;
	}

	public List<Destination> getDestinations() {
		return getSession().getDestinations();
	}

	public void setSelectedSceneById(String id){
		selectedScene = sceneService.getScene(id);
	}

	public Scene getSelectedScene() {
		return selectedScene;
	}

	public void incrementSelectedSceneThumb(){
		selectedScene.incrementThumb();
		sceneService.saveScene(selectedScene);
	}

	public void updateSelectedScenePan(){
		String value = FacesContext.getCurrentInstance().
				getExternalContext().getRequestParameterMap().get("modeTab:sceneSettings:sceneSettingsForm:scenePan");
		//double d = Double.parseDouble(value);
		//selectedScene.setPan((int) d);
		selectedScene.setCameraDefault(value);
	}


	public void deleteVoiceover(){
		selectedScene.setVoiceover(null);
		saveSelectedScene();
	}

	public void handleAudioFileUpload(FileUploadEvent event) {

		UploadedFile upload = event.getFile();
		if(upload == null) return;

		String str = upload.getFileName();
		String ext = str.substring(str.lastIndexOf('.'), str.length());

		String tempFileName = UUID.randomUUID().toString() + ext;
		java.io.File tempFile = new java.io.File(Constants.TEMP_DIR_PATH + tempFileName);

		try {
			FileUtils.copyInputStreamToFile(upload.getInputstream(), tempFile);
			FacesMessage message = new FacesMessage("Successful", event.getFile().getFileName() + " is uploaded.");
			FacesContext.getCurrentInstance().addMessage(null, message);

			RequestContext context = RequestContext.getCurrentInstance();

			if(s3UploadService.uploadFile(assetsBucket, tempFile)){
				context.execute("onAudioUploadComplete('" + tempFileName + "')");
				selectedScene.setVoiceover(tempFileName);
				saveSelectedScene();
			};

		} catch (IOException e) {
			e.printStackTrace();
		} catch (S3ServiceException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}


	}

	public void handleSceneVideoProcessingJob(){
		System.out.println("handleSceneVideoProcessingJob");
	}

/*	public VideoSettings getSelectedSceneVideoSettings(){
			return selectedSceneVideoSettings;
	}*/

	public void getSceneData(){

		if(selectedScene == null)
			return;

		SceneDetail sceneDetail = sceneService.getSceneDetail(selectedScene.getId());
		String json = new JSONSerializer().serialize(sceneDetail);
		RequestContext reqCtx = RequestContext.getCurrentInstance();
		reqCtx.addCallbackParam("sceneData", json);
	}


/*	public void setSelectedSceneVideoSettings(VideoSettings settings) {
		this.selectedSceneVideoSettings = settings;
	}*/

	public void setSelectedScene(Scene selectedScene) {
		//selectedSceneVideoSettings = selectedScene.getVideoSettings();
		this.selectedScene = selectedScene;
	}

	public void saveSelectedScene(){
		if(selectedScene != null) {
/*				if(selectedScene.getVideoSettings() != null && selectedSceneVideoSettings != null)
				selectedScene.setVideoSettings(selectedSceneVideoSettings);*/
			sceneService.saveScene(selectedScene);
		}
	}

    public void updateVideoDuration(){

		/*String value = FacesContext.getCurrentInstance().
				getExternalContext().getRequestParameterMap().get("tab:modalScene:duration");

		if(selectedSceneVideoSettings.getEndTime().equals(0.0))
			selectedSceneVideoSettings.setEndTime(Double.valueOf(value));*/
	}

	public void updateVideoSettings(){

		System.out.println("updateVideoSettings");

		String value = FacesContext.getCurrentInstance().
				getExternalContext().getRequestParameterMap().get("modeTab:modalScene:videoSettings");

		selectedScene.setVideo(value);

		VideoSettings settings = selectedScene.getVideoSettings();
		VideoSettings outputSettings = selectedScene.getOutputVideoSettings();

		if(outputSettings != null){
			outputSettings.setVolume(settings.getVolume());
			outputSettings.setBrightness(settings.getBrightness());
			outputSettings.setContrast(settings.getContrast());
			outputSettings.setSaturation(settings.getSaturation());
			outputSettings.setHue(settings.getHue());
			selectedScene.setOutputVideoSettings(outputSettings);
		}

		//selectedScene.setOutputVideo(null);
/*		for (GuideScene guideScene : guideService.getGuideScenes(selectedScene.getId()))
			guideScene.video = null;*/

		sceneService.saveScene(selectedScene);

		RequestContext context = RequestContext.getCurrentInstance();
		context.update("modeTab:sceneSettings:outputs");

		//selectedSceneVideoSettings = new JSONDeserializer<VideoSettings>().use("values", VideoSettings.class).deserialize(value);
	}

	public void updateVoiceover(){

		System.out.println("updateVoiceover");

		String value = FacesContext.getCurrentInstance().
				getExternalContext().getRequestParameterMap().get("modeTab:modalScene:voiceoverSettings");

		System.out.println("updateVoiceover");
		selectedScene.setVoiceover(value);
		sceneService.saveScene(selectedScene);
	}

	public void updateAllOutputs(){

		for(Scene scene :scenes)
			 fileUploadService.addSceneVideoTask(scene.getId());
	}

	public void updateOutputs(){

			fileUploadService.addSceneVideoTask(selectedScene.getId());

/*		int count = 0;
		try {
			do {
				count ++;
				Thread.sleep(1000);
				if(count == 99)
					return;
			}
			while (task.status == ScenePanoTask.PENDING);

			FacesMessage msg = new FacesMessage("Successful", task.getScene().getTitle() + " is processing outputs");
			FacesContext.getCurrentInstance().addMessage(null, msg);

			count = 0;
			do {
				Thread.sleep(1000);
				if(count == 999)
					return;
			}
			while (task.status == ScenePanoTask.PROCESSING);

			if(task.status == ScenePanoTask.COMPLETE)
				msg = new FacesMessage("Successful", task.getScene().getTitle() + " has completed processing outputs");
			else if(task.status == ScenePanoTask.ERROR)
				msg = new FacesMessage("Failed", task.getScene().getTitle() + " processing outputs failed");

			FacesContext.getCurrentInstance().addMessage(null, msg);

			RequestContext context = RequestContext.getCurrentInstance();
			if(context != null && task.getScene() == selectedScene)
				context.update("modeTab:sceneSettings:outputs");

		} catch (InterruptedException e) {
			e.printStackTrace();
		}*/
	}

	public void addHotspot(){

		selectedHotspot = hotspotService.addHotspot(new Hotspot());
		selectedHotspot.title = "New hotspot - " + sceneService.getSceneDetail(selectedScene.getId()).getHotspots().size();
		selectedHotspot.sceneId = selectedScene.getId();

		RequestContext context = RequestContext.getCurrentInstance();
		context.addCallbackParam("hotspot", selectedHotspot);
		context.update("modeTab:sceneSettings:hotspotSettings");
	}

	public void removeHotspot(){
		System.out.println("remove hotspot");
		selectedHotspot.status = -1;
		saveSelectedHotspot();
		selectedHotspot = null;
		RequestContext context = RequestContext.getCurrentInstance();
		context.update("modeTab:sceneSettings:hotspotSettings");
	}

	public Scene getSelectedHotspotLoadScene() {

		if(selectedHotspot != null)
			if(selectedHotspot.loadSceneId != null) {
				selectedHotspotLoadScene = sceneService.getScene(selectedHotspot.loadSceneId);
				return selectedHotspotLoadScene;
			}

		return null;

	}

	public void setSelectedHotspotLoadScene(Scene selectedHotspotLoadScene) {
		this.selectedHotspotLoadScene = selectedHotspotLoadScene;

		if(selectedHotspot != null){

			if(selectedHotspotLoadScene != null)
				selectedHotspot.loadSceneId = selectedHotspotLoadScene.getId();
			else
				selectedHotspot.loadSceneId = null;
		}

		saveSelectedHotspot();
	}

	public List<Hotspot> getSelectedSceneHotspots() {

		if(selectedScene != null) {
			SceneDetail scene = sceneService.getSceneDetail(selectedScene.getId());

			List<Hotspot> hotspots = new ArrayList<Hotspot>();

			for(Hotspot hotspot : scene.getHotspots())
				if(hotspot.status > -1)
					hotspots.add(hotspot);

			return hotspots;
		}
		return null;
	}

	public Hotspot getSelectedHotspot() {
		return selectedHotspot;
	}

	public void setSelectedHotspot(Hotspot selectedHotspot) {
		this.selectedHotspot = selectedHotspot;
	}

	public void saveSelectedHotspot(){

		if(selectedHotspot != null)
			hotspotService.saveHotspot(selectedHotspot);
	}

	public void clearSelected(){
		selectedHotspot = null;
		selectedScene = null;
	}

	public void updateSelectedHotspotId(){
		String value = FacesContext.getCurrentInstance().
				getExternalContext().getRequestParameterMap().get("modeTab:sceneSettings:hotspotSettings:selectedHotspotId");

		if(value.length() < 1)
			selectedHotspot = null;

		if (selectedHotspot != null) {
			if (selectedHotspot.getId().equals(value))
				return;
			else
				selectedHotspot = hotspotService.getHotspot(value);
		}
		else
			selectedHotspot = hotspotService.getHotspot(value);

		RequestContext context = RequestContext.getCurrentInstance();
		context.update("modeTab:sceneSettings:hotspotSettings");
		context.execute("onHotspotChange()");
	}

	public void handleHotspotFileUpload(FileUploadEvent event) {

		event.getComponent().setTransient(false);
		UploadedFile upload = event.getFile();
		if(upload == null) return;

		String str = upload.getFileName();
		String ext = str.substring(str.lastIndexOf('.'), str.length());

		String tempFileName = UUID.randomUUID().toString() + ext;
		java.io.File tempFile = new java.io.File(Constants.TEMP_DIR_PATH + tempFileName);

		try {
			FileUtils.copyInputStreamToFile(upload.getInputstream(), tempFile);
			FacesMessage message = new FacesMessage("Successful", event.getFile().getFileName() + " is uploaded.");
			FacesContext.getCurrentInstance().addMessage(null, message);

			if(s3UploadService.uploadFile(assetsBucket, tempFile)){
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("onMediaUploadComplete('" + tempFileName + "')");
			}

		} catch (IOException e) {
			e.printStackTrace();
		} catch (S3ServiceException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}


	}

	public List<HotspotBehaviour> getSelectedHotspotBehaviours() {
		return selectedHotspotBehaviours;
	}

	public void setSelectedHotspotBehaviours(List<HotspotBehaviour> selectedHotspotBehaviours) {
		this.selectedHotspotBehaviours = selectedHotspotBehaviours;
	}

	public HotspotBehaviour getSelectedHotspotBehaviour() {

		if(selectedHotspotBehaviour == null)
			selectedHotspotBehaviour = new HotspotBehaviour();

		return selectedHotspotBehaviour;
	}

	public void setSelectedHotspotBehaviour(HotspotBehaviour selectedHotspotBehaviour) {
		this.selectedHotspotBehaviour = selectedHotspotBehaviour;
	}

	public void addBehaviour(){
		selectedHotspotBehaviour = new HotspotBehaviour();

		if(selectedHotspotBehaviours == null)
			selectedHotspotBehaviours = new ArrayList<HotspotBehaviour>();

		selectedHotspotBehaviours.add(selectedHotspotBehaviour);
	}

	public void updateSelectedBehaviour(){

		String value = FacesContext.getCurrentInstance().
				getExternalContext().getRequestParameterMap().get("modeTab:sceneSettings:hotspotSettings:hotspotOptions:timeField");

		if(selectedHotspotBehaviours == null)
			selectedHotspotBehaviours = new ArrayList<HotspotBehaviour>();

		for(HotspotBehaviour behaviour : selectedHotspotBehaviours)
			if(behaviour.getTime().equals(value))
				return;

		selectedHotspotBehaviour = new HotspotBehaviour();
		selectedHotspotBehaviour.setTime(value);
		selectedHotspotBehaviours.add(selectedHotspotBehaviour);

	}

	private Destination getDestination(String destinationId) {
		List<Destination> destinations = getSession().getDestinations();
		for (Destination destination : destinations)
			if (destination.getId().equals(destinationId))
				return destination;

		return null;
	}

	public List<EWTeam> getTeams() {
		if (!getUser().isAdmin())
			teams = getUser().getTeams();

		else if (teams == null)
			teams = loadTeams();

		return teams;
	}

	private List<EWTeam> loadTeams() {
		List<EWTeam> teams = new ArrayList<EWTeam>();
		try {
			BeekSession session = getSession();
			EWCustomersResponse response = ewService.queryCustomers(
					session.getUsername(), session.getPassword());
			List<EWCustomer> list = response.getCustomers();

			for (EWCustomer c : list)
				if (c.getName() != null && c.getToken() != null)
					teams.add(c.getTeam());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return teams;
	}

	public List<Scene> getScenes() {
		if(location != null) {
            location.setSceneCount(scenes.size());
            return scenes;
        }
        return new ArrayList<Scene>();
	}

	public void addScene() {
        HttpServletRequest req = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();

        String sceneName = req.getParameter("wizard_form:tabView:sceneNameLabel");
        String sceneId = req.getParameter("wizard_form:tabView:sceneId");

        if(sceneId == null || sceneId.equals("")){
            return;
        }

        Scene scene = new Scene(location);

        if(sceneName == null || sceneName.equals("")){
            scene.setTitle("NEW SCENE");
        }else{
            scene.setTitle(sceneName);
        }

		scene.setStatus(6);

        scene = sceneService.saveScene(scene);
        scenes.add(scene);
        location.setSceneCount(scenes.size());

        // pass the data back for javascript access
        RequestContext context = RequestContext.getCurrentInstance();
        context.addCallbackParam("temp_sceneId", sceneId);
        context.addCallbackParam("sceneId", scene.getId());
        context.addCallbackParam("sceneName", scene.getTitle());
		context.update("wizard_form:tabView:locationScenes");
	}


	private void reloadScenesFromDB() {
		this.scenes = sceneService.getAllScenesForLocation(location.getId());
		RequestContext.getCurrentInstance().update("scenesTab");
	}

	public Scene getDefaultScene() {
		if (location.getDefaultSceneId() != null)
			return sceneService.getScene(location.getDefaultSceneId());

		return null;
	}

	public List<Guide> getGuidesImIn(Location location) {
		try {
			// Get all scenes for the list of locations
			List<Location> locations = new ArrayList<Location>();
			locations.add(location);
			List<Scene> scenes = sceneService.getScenes(locations);

			List<Guide> guides = new ArrayList<Guide>();
			guides.addAll(guideService.getGuidesContainingScenes(scenes));

			// removes the duplicates
			HashSet<Guide> set = new HashSet<Guide>(guides);
			return new ArrayList<Guide>(set);
		} catch (Exception e) {
			showError("Error querying guides I'm in: " + e.getMessage());
			return new ArrayList<Guide>();
		}
	}


	public void cancelLocation() throws IOException {

		for (Scene scene : scenes) {
			scene.setStatus(Scene.SceneStatus.CANCELLED.getStatus());
			sceneService.saveScene(scene);

			List<GuideScene> guideScenes = guideService.getGuideScenes(scene
					.getId());
			for (GuideScene guideScene : guideScenes)
				guideService.deleteGuideScene(guideScene);

			List<Bubble> bubbles = sceneService.getBubblesTargeting(scene);
			for (Bubble bubble : bubbles)
				sceneService.deleteBubble(bubble);

			List<BoardSign> boardSigns = sceneService
					.getBoardSignsTargeting(scene);
			for (BoardSign boardSign : boardSigns)
				sceneService.deleteBoardSign(boardSign);
		}

		location.setStatus(Location.STATE_CANCELLED);
		locationService.saveLocation(location);
		redirectHome();
	}

	public void printTestMessage() {
		System.out.println("this button is working");

	}

	public void processSubmit() throws IOException {
		location.setSceneCount(scenes.size());

		location.setTitle(title);
		location.setEmail(email);
		location.setPhone(phone);
		location.setInfoURL(infoURL);
		locationService.saveLocation(location);

		for (Scene scene : scenes) {
			sceneService.saveScene(scene);
			System.out.println(scene.getTitle());
		}

		//saveLocationToCRM();
		FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Saved location", "Saved location");
		FacesContext.getCurrentInstance().addMessage(null, m);

	}

    public void deleteLocation() throws IOException {
        if(location != null){
            location.setStatus(-1);
            locationService.saveLocation(location);
            FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Location Deleted", "Location successfully deleted");
            FacesContext.getCurrentInstance().addMessage(null, m);
        }
    }


	public void deleteScene(Scene scene) throws IOException {

		if(scene != null){
			scene.setStatus(-1);
			sceneService.saveScene(scene);
			reloadScenesFromDB();
		}
	}

	public void duplicateScene(Scene s) throws IOException {

		Scene scene = new Scene(location);
		scene.setCameraDefault(s.getCameraDefault());
		scene.setTitle(s.getTitle() + " copy");
		scene.setThumbIncrement(s.getThumbIncrement());
		scene.setVideo(s.getVideo());
		sceneService.saveScene(scene);
	}


	private void saveLocationToCRM()
	{
		try {
			ewServiceImpl.createLocation(getSession().getUsername(), getSession().getPassword(), location);


		} catch (Exception e) {
			FacesMessage m2 = new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Hit exception", "Hit exception");
			FacesContext.getCurrentInstance().addMessage(null, m2);
		}

	}



}