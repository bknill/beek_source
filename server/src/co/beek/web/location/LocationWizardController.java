package co.beek.web.location;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FlowEvent;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import co.beek.pano.model.beans.LocationMapModel;
import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.enterprizewizard.EWPhotoShoot;
import co.beek.pano.model.dao.enterprizewizard.EWPhotoShootResponse;
import co.beek.pano.model.dao.entities.Destination;
import co.beek.pano.model.dao.entities.GuideDetail;
import co.beek.pano.model.dao.entities.Location;
import co.beek.pano.model.dao.entities.Scene;
import co.beek.pano.service.dataService.MailService;
import co.beek.pano.service.dataService.enterprizewizardService.EWService;
import co.beek.pano.service.dataService.guideService.GuideService;
import co.beek.pano.service.dataService.locationService.LocationService;
import co.beek.pano.service.dataService.sceneService.SceneService;
import co.beek.util.BeekSession;
import co.beek.web.BaseController;

@Scope("session")
@Controller("LocationWizardController")
public class LocationWizardController extends BaseController implements
		Serializable {

	@RequestMapping(value = "/location/{id}/wizard", method = RequestMethod.GET)
	public String mapView(@PathVariable("id") String id) {
		return "/views/location/wizard.jsf?id=" + id;
	}

	private static final long serialVersionUID = 6373042564014023228L;

	@Inject
	private LocationService locationService;

	@Inject
	private SceneService sceneService;

	@Inject
	private GuideService guideService;

	@Inject
	private MailService mailService;

	@Inject
	private EWService ewService;

	private Location location;

	private List<Scene> scenes;

	private GuideDetail guide;

	private LocationMapModel locationMapModel;

	public void preRenderView() throws Exception {
		if (isPostback())
			return;

		if (redirectingToLogin())
			return;

		EWContact user = getSession().getUser();
		if (user == null)
			throw new Exception("You are not logged in.");

		String locationId = getRequest().getParameter("id");
		this.location = locationService.getLocation(locationId);
		this.scenes = sceneService.getAllScenesForLocation(locationId);

		if (location == null)
			throw new Exception("Location does not exist.");

		if (!user.isInTeam(location.getTeamId()))
			throw new Exception("You need to be on team" + location.getTeamId()
					+ " to edit this location.");

		locationMapModel = new LocationMapModel(location);
		guide = null;
	}

	public Location getLocation() {
		return location;
	}

	public GuideDetail getGuide() {
		return guide;
	}

	public List<Scene> getUploadedScenes() {
		List<Scene> uploadedScenes = new ArrayList<Scene>();
		for (Scene s : scenes)
			if (s.getPanoIncrement() > 0)
				uploadedScenes.add(s);
		return scenes;// uploadedScenes;
	}

	public List<Scene> getApprovedScenes() {
		List<Scene> approvedScenes = new ArrayList<Scene>();
		for (Scene s : scenes)
			if (s.getStatus() == Scene.SceneStatus.ACCEPTED.getStatus())
				approvedScenes.add(s);
		return approvedScenes;
	}

	public String onFlowProcess(FlowEvent event) {
		if (event.getOldStep().equals("photographsTab")) {
			int numApprovedScenes = getApprovedScenes().size();
			updateStatusFromApprovedScenes(numApprovedScenes);
			if (numApprovedScenes < location.getSceneOrderCount()) {
				showError("You cannot proceed, "
						+ "as you have not approved enough scenes. You must approve at least "
						+ location.getSceneOrderCount() + " scenes.");
				return event.getOldStep();
			}
			saveLocationAndScenes();
		}

		if (event.getOldStep().equals("locationTab"))
			reloadScenesFromDB();

		if (event.getOldStep().equals("guideTab"))
			reloadGuideFromDatabase();

		return event.getNewStep();
	}

	private void saveLocationAndScenes() {
		try {
			sceneService.saveScenes(scenes);
			location.setSceneCount(scenes.size());
			locationService.saveLocation(location);

		} catch (Exception e) {
			sendErrorDan("Error Saving Location and Scenes", e);
		}
	}

	private void reloadScenesFromDB() {
		this.scenes = sceneService.getAllScenesForLocation(location.getId());

		location.setSceneCount(scenes.size());
		locationService.saveLocation(location);

		// update both tabs with scenes in them
		RequestContext.getCurrentInstance().update(
				"photographsTab");
		RequestContext.getCurrentInstance().update(
				"locationTab");

		if (guide == null)
			createGuide();
	}

	private void createGuide() {
		try {

			// Get the destination from the location
			Destination destination = getSession().getDestination(
					location.getDestinationId());

			// first add the guide, as we need a valid guideId for adding scenes
			guide = new GuideDetail(destination, location);
			guideService.saveGuideDetail(guide);

			// add the scenes to the guide
			List<Scene> apScenes = getApprovedScenes();
			for (Scene scene : apScenes)
				guide.addGuideScene(scene);

			// set the default location scene to the default guide scene
			if (location.getDefaultScene() != null)
				guide.setFirstScene(location.getDefaultScene());

			// update the guide with the new details
			guideService.saveGuideDetail(guide);
		} catch (Exception e) {
			sendErrorDan("Error creating Guide", e);
		}
	}

	private void reloadGuideFromDatabase() {
		try {
			guide = guideService.getGuideDetail(guide.getId());
		} catch (Exception e) {
			sendErrorDan("Error updating guide in location wizard", e);
		}
	}

	/**
	 * Used in the jsf for absolute urls back to this server
	 */
	public String getBeekDomain() {
		return getRequest().getServerName().replace("gms", "www");
	}

	/**
	 * Calculates the new status of the location/photoshoot based on the number
	 * of scenes that the user has approved. user.destinations Calls EW to
	 * update the state of the photoshoot.
	 */
	private void updateStatusFromApprovedScenes(int numApprovedScenes) {
		try {

			// Initialize both values to being not approved
			location.setStatus(Location.STATE_PENDING);
			String photoShootStatus = EWPhotoShoot.PhotoShootStatus.CHANGES_REQUESTED
					.getCode();

			// Update to approved, if the right amount of scenes have been
			// approved.
			if (numApprovedScenes >= location.getSceneOrderCount()) {
				location.setStatus(Location.STATE_LIVE);
				photoShootStatus = EWPhotoShoot.PhotoShootStatus.APPROVED
						.getCode();
			}
			// If there is not photoshoot, we only update locally.
			if (location.getPhotoShootId() == null)
				return;

			// Update the status of the photoshoot in Enterprize Wizard.
			updatePhotoShoot(numApprovedScenes, photoShootStatus);

			// Tells the user that something significant has happened
			showMessage("Photographer notified",
					"The photographer has been notified that "
							+ numApprovedScenes + " scenes have been approved");
		} catch (Exception e) {
			sendErrorDan("Error updating status of photoshoot and location", e);
		}
	}

	/**
	 * Calls the Enterprize Wizard service to update the original photo shoot.
	 * Updates the location/scenes in the db.
	 * 
	 * @throws Exception
	 */
	private void updatePhotoShoot(int numApprovedScenes, String photoShootStatus)
			throws Exception {
		HttpServletRequest request = getRequest();
		BeekSession session = getSession(request);

		String message = "Here are the commments for the scenes: ";
		for (Scene scene : scenes)
			message += scene.getComments() + "\\r\\n";

		// The url that is sent to the photographer to review the comments.
		String approvalUrl = "http://" + request.getServerName() + "/location/"
				+ location.getId() + "/view";

		// Call Enterprize Wizard with the new values
		EWPhotoShootResponse response = ewService.approvePhotos(
				session.getUsername(), session.getPassword(),
				location.getPhotoShootId(), photoShootStatus, approvalUrl,
				numApprovedScenes, message);

		if (!response.getSuccess())
			throw new Exception("Error response from EW: "
					+ response.getErrorMessage());
	}

	public LocationMapModel getLocationMapModel() {
		return locationMapModel;
	}
}