package co.beek.web.scene;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.Map.Entry;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import co.beek.pano.model.dao.entities.*;
import co.beek.pano.service.dataService.FileUploadService;
import co.beek.pano.service.dataService.enterprizewizardService.EWService;
import co.beek.pano.service.dataService.uploadService.S3UploadService;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.primefaces.push.EventBus;
import org.primefaces.push.EventBusFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import co.beek.Constants;
import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.service.dataService.BeekService;
import co.beek.pano.service.dataService.guideService.GuideService;
import co.beek.pano.service.dataService.sceneService.SceneService;
import co.beek.web.BaseController;
import flexjson.JSONSerializer;

@Controller("SceneController")
@Scope("session")
public class SceneController extends BaseController implements Serializable {
	private static final long serialVersionUID = 897552663451118806L;

	@Value("#{buildProperties.assetsBucket}")
	private String assetsBucket;

	@Inject
	private SceneService sceneService;

	@Inject
	private GuideService guideService;

    @Inject
    private FileUploadService fileUploadService;

	@Inject
	private S3UploadService s3UploadService;

	@Inject
	private EWService ewService;

	// private String domain;

	private Scene scene;

	private Beek beek;

	private HashMap<String, String> flashVars;

	@Value("#{buildProperties.gaCode}")
	private String gaCode;

	@Value("#{buildProperties.cdnUrl}")
	private String cdnUrl;

	@Inject
	private BeekService beekService;

	JSONSerializer serializer = new JSONSerializer().prettyPrint(true);

    public void handleFileUpload(FileUploadEvent event){
        HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();

        String sceneId = null;
        if(request.getParameter("sceneId") != null)
            sceneId = request.getParameter("sceneId");

		String id = (String) event.getComponent().getAttributes().get("sceneId");

		if(id != null)
			sceneId = id;
		else
		if(request.getParameter("wizard_form:tabView:sceneId") != null)
            sceneId = request.getParameter("wizard_form:tabView:sceneId");

        if(sceneId == null) return;

        UploadedFile upload = event.getFile();
        if(upload == null) return;

		String str = upload.getFileName();
		String ext = str.substring(str.lastIndexOf('.'), str.length());

        Scene scene = sceneService.getScene(sceneId);

		EventBus pushContext = EventBusFactory.getDefault().eventBus();

		//pano video
		if(ext.equals(".mp4")){
			try {
			String tempVideoFileName = UUID.randomUUID().toString() + ".mp4";
			java.io.File tempVideoFile = new java.io.File(Constants.TEMP_DIR_PATH + tempVideoFileName);
			FileUtils.copyInputStreamToFile(upload.getInputstream(), tempVideoFile);

			FacesMessage msg = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploading.");
				FacesContext.getCurrentInstance().addMessage(null, msg);

				String pushString = "{\"success\": \"true\", \"eventName\": \"updateSceneTab\", \"sceneId\": \""+sceneId+"\", \"sceneName\": \""+scene.getTitle()+"\", \"message\": \"Uploading\"}";
				pushContext.publish("/", pushString);

				if(s3UploadService.uploadFile(assetsBucket, tempVideoFile)){
					if(scene.getVideoSettings() == null){
						VideoSettings settings = new VideoSettings();
									settings.setFilename(tempVideoFileName);
									settings.setBrightness(0.0);
									settings.setContrast(0.0);
									settings.setStartTime(0.0);
									settings.setEndTime(0.0);
									settings.setHue(0.0);
									settings.setSaturation(0.0);

						scene.setVideoSettings(settings);
					}
					else{
						VideoSettings settings = scene.getVideoSettings();
						settings.setFilename(tempVideoFileName);
						scene.setVideoSettings(settings);
					}

					scene.setStatus(0);

					sceneService.saveScene(scene);
					fileUploadService.addSceneVideoTask(scene.getId());

					String finishedString = "{\"success\": \"true\", \"eventName\": \"enableSceneTab\", \"sceneId\": \""+sceneId+"\", \"sceneName\": \""+scene.getTitle()+"\", \"message\": \"Uploaded\"}";
					pushContext.publish("/", finishedString);

					System.out.println("uploaded new scene video " + tempVideoFileName);

					RequestContext reqCtx = RequestContext.getCurrentInstance();
					reqCtx.addCallbackParam("newVideoFileName", tempVideoFileName);
					reqCtx.update("wizard_form:tabView:locationScenes");
					//reqCtx.update("modeTab:sceneSettings:outputs");

					tempVideoFile.delete();
				}
			} catch (Exception e) {
				e.printStackTrace();
				FacesMessage msg = new FacesMessage("Error", event.getFile().getFileName() + " upload failed.");
				FacesContext.getCurrentInstance().addMessage(null, msg);

				String pushString = "{\"success\": \"false\", \"eventName\": \"updateSceneTab\", \"sceneId\": \""+sceneId+"\", \"sceneName\": \""+scene.getTitle()+"\", \"message\": \"Upload Failed\"}";
				pushContext.publish( "/", pushString);
			}

		}
		//pano image
		else
			try {
			//	scene_2315_pano_1.jpg

				scene.incrementPano();
				String tempFileName ="scene_" + sceneId + "_pano_" + scene.getPanoIncrement() + ".jpg";
				//String tempFileName = UUID.randomUUID().toString() + ".jpg";
				java.io.File tempFile = new java.io.File(Constants.TEMP_DIR_PATH + tempFileName);
				FileUtils.copyInputStreamToFile(upload.getInputstream(), tempFile);

				System.out.println(tempFileName);

				scene.setStatus(1);

				if(s3UploadService.uploadFile(assetsBucket,  tempFile)) {
					sceneService.saveScene(scene);
					fileUploadService.addPanoTask(sceneId, tempFileName, null);

					FacesMessage msg = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
					FacesContext.getCurrentInstance().addMessage(null, msg);

					String pushString = "{\"success\": \"true\", \"eventName\": \"updateSceneTab\", \"sceneId\": \"" + sceneId + "\", \"sceneName\": \"" + scene.getTitle() + "\", \"message\": \"Upload Finished\"}";
					pushContext.publish("/", pushString);
				}
				else
				{
					String pushString = "{\"success\": \"false\", \"eventName\": \"updateSceneTab\", \"sceneId\": \""+sceneId+"\", \"sceneName\": \""+scene.getTitle()+"\", \"message\": \"Upload to S3 Failed\"}";
					pushContext.publish( "/", pushString);
				}

			} catch (Exception e) {
				e.printStackTrace();
				FacesMessage msg = new FacesMessage("Error", event.getFile().getFileName() + " upload failed.");
				FacesContext.getCurrentInstance().addMessage(null, msg);

				scene.setStatus(1);
				sceneService.saveScene(scene);

				String pushString = "{\"success\": \"false\", \"eventName\": \"updateSceneTab\", \"sceneId\": \""+sceneId+"\", \"sceneName\": \""+scene.getTitle()+"\", \"message\": \"Upload Failed\"}";
				pushContext.publish( "/", pushString);

				RequestContext reqCtx = RequestContext.getCurrentInstance();
				reqCtx.update("wizard_form:tabView:locationScenes");

			}
    }

	Boolean firstVideo = true;
	String huginTemplate;

	public void updateOutputs(ActionEvent event){
		String sceneId = (String)event.getComponent().getAttributes().get("sceneId");
		ScenePanoTask task = fileUploadService.addSceneVideoTask(sceneId);
	}

	public void handleVideoToStitchUpload(FileUploadEvent event){
		HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();


		String sceneId = null;
		if(request.getParameter("sceneId") != null)
			sceneId = request.getParameter("sceneId");

		if(request.getParameter("wizard_form:tabView:sceneId") != null)
			sceneId = request.getParameter("wizard_form:tabView:sceneId");

		System.out.println(sceneId);

		if(sceneId == null) return;

		UploadedFile upload = event.getFile();
		if(upload == null) return;

		String str = upload.getFileName();
		String ext = str.substring(str.lastIndexOf('.'), str.length());

		if(ext.equals(".pto")) {
			try {
				String tempTemplateFileName = UUID.randomUUID().toString() + ".pto";
				java.io.File tempTemplate = new java.io.File(Constants.TEMP_DIR_PATH + tempTemplateFileName);
				FileUtils.copyInputStreamToFile(upload.getInputstream(), tempTemplate);

				FacesMessage msg = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploading.");
				FacesContext.getCurrentInstance().addMessage(null, msg);


				if (s3UploadService.uploadFile(assetsBucket, tempTemplate)) {

					huginTemplate = tempTemplateFileName;
					getSession().getUser().setStitchtemplate(huginTemplate);
					ewService.updateStitchTemplate(getSession().getUsername(), getSession().getPassword(), getSession().getUser().getId(), huginTemplate);

					tempTemplate.delete();
					return;
				}
				else
					return;


			} catch (Exception e) {
				e.printStackTrace();
				FacesMessage msg = new FacesMessage("Error", event.getFile().getFileName() + " upload failed.");
				FacesContext.getCurrentInstance().addMessage(null, msg);
				return;
			}

		}


		Scene scene = sceneService.getScene(sceneId);

		EventBus pushContext = EventBusFactory.getDefault().eventBus();

		try {

			String order;

			if(firstVideo){
				order = "1";
				firstVideo = false;
			}
			else{
				order = "2";
				firstVideo = true;
			}

			String tempVideoFileName = "scene_" + sceneId + "_panovideo_" + scene.getPanoIncrement() + '_' + order + ".mp4";

			java.io.File tempVideoFile = new java.io.File(Constants.TEMP_DIR_PATH + tempVideoFileName);
			FileUtils.copyInputStreamToFile(upload.getInputstream(), tempVideoFile);


			FacesMessage msg = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploading.");
			FacesContext.getCurrentInstance().addMessage(null, msg);

			String pushString = "{\"success\": \"true\", \"eventName\": \"updateSceneTab\", \"sceneId\": \""+sceneId+"\", \"sceneName\": \""+scene.getTitle()+"\", \"message\": \"Uploading\"}";
			pushContext.publish("/", pushString);

			if(huginTemplate == null)
				huginTemplate = getSession().getUser().getStitchtemplate();



		if(s3UploadService.uploadFile(assetsBucket, tempVideoFile)){

				tempVideoFile.delete();

			if(!firstVideo)
				fileUploadService.addPanoTask(sceneId, null, huginTemplate);

				String finishedString = "{\"success\": \"true\", \"eventName\": \"updateSceneTab\", \"sceneId\": \""+sceneId+"\", \"sceneName\": \""+scene.getTitle()+"\", \"message\": \"Uploaded\"}";
				pushContext.publish("/", finishedString);
			}
			else
				System.out.println("upload failed");



		} catch (Exception e) {
			e.printStackTrace();
			FacesMessage msg = new FacesMessage("Error", event.getFile().getFileName() + " upload failed.");
			FacesContext.getCurrentInstance().addMessage(null, msg);

			//String pushString = "{\"success\": \"false\", \"eventName\": \"updateSceneTab\", \"sceneId\": \""+sceneId+"\", \"sceneName\": \""+scene.getTitle()+"\", \"message\": \"Upload Failed\"}";
			//pushContext.publish( "/", pushString);
		}
	}

    @RequestMapping(value = "/scene/{id}/view",  method = {RequestMethod.GET, RequestMethod.HEAD})
	public String mapView(@PathVariable("id") long id) {
		return "/views/scene/view.jsf?s=" + id;
	}

	@RequestMapping(value = "/scene/{id}/edit", method = RequestMethod.GET)
	public String mapEditView(HttpServletRequest request,
			@PathVariable("id") String id) throws Exception {
		EWContact user = getSession(request).getUser();
		if (user == null)
			return "/views/login.jsf";

		Scene s = sceneService.getScene(id);
		//if (!user.isInTeam(s.getLocation().getTeamId()))

		if(!user.hasLocation(s.getLocationId()))
			throw new Exception("You dont have permission to edit this scene");

		return "/views/scene/edit.jsf?s=" + id + "&autologin=true";
	}

	public void preRenderView() {
		HttpServletRequest request = getRequest();

		// Get the beek data
		beek = beekService.getBeek();

		// remove the two possible subdomains from the request
		// domain = request.getServerName().replace("gms.", "");
		// domain = domain.replace("www.", "");

		String sceneId = request.getParameter("s");
		scene = sceneService.getScene(sceneId);

		flashVars = new HashMap<String, String>();
		flashVars.put("domain", Constants.domain);
		flashVars.put("assetCdn", cdnUrl);
		flashVars.put("adminSwf", getAdminSwfUrl());
		flashVars.put("gaCode", gaCode);
		flashVars.put("sceneId", scene.getId());

		// perform autologin in the flash
		EWContact user = getSession(request).getUser();
		String autologin = request.getParameter("autologin");
		if (autologin != null && autologin.equals("true") && user != null)
			flashVars.put("autologin", "true");
	}

	public Scene getScene() {
		return scene;
	}

	public void setScene(Scene scene) {
		this.scene = scene;
	}

	public String getDomain() {
		return Constants.domain;
	}

	public String getScenePath() {
		if (scene != null)
			return "/s" + scene.getId();

		return "";
	}

	
	public String getSessionId() {
		if (isLoggedIn())
			return getSession().getId();
		return null;
	}

	public String getPlayerSwfUrlFlash10() {
		if (Constants.domain.contains("beekdev.co"))
			return "http://gms.beekdev.co/resources/swf/fp10/swf_player.swf";

		return cdnUrl + "/assets-v861/swf_player.swf";
	}

	public String getPlayerSwfUrl() {
		if (Constants.domain.contains("beekdev.co"))
			return "http://gms.beekdev.co/resources/swf/swf_player.swf";

		return cdnUrl + "/" + beek.getPlayerFileName();
	}

	public String getAdminSwfUrl() {
		if (Constants.domain.contains("beekdev.co"))
			return "http://gms.beekdev.co/resources/swf/swf_admin.swf";

		return cdnUrl + "/" + beek.getAdminFileName();
	}

	public String getTeamsCD() {
		if (isLoggedIn())
			return getUser().teamsCommaDelimited();
		return null;
	}

	public String getGroupsCD() {
		if (isLoggedIn())
			return getUser().groupsCommaDelimited();
		return null;
	}

	public String getFlashVarsCommaDelimited() {
		List<String> props = new ArrayList<String>();

		Iterator<Entry<String, String>> it = flashVars.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, String> prop = it.next();
			props.add(prop.getKey() + "=" + prop.getValue());
		}
		return StringUtils.join(props, "&");
	}

	public String getFlashVarsJson() {
		return serializer.deepSerialize(flashVars);
	}

	/**
	 * unused because we dont have a page for editing scenes.
	 * 
	 * @throws IOException
	 */
	public void cancelScene() throws IOException {
		scene.setStatus(Scene.SceneStatus.CANCELLED.getStatus());
		sceneService.saveScene(scene);

		List<GuideScene> guideScenes = guideService.getGuideScenes(scene
				.getId());
		for (GuideScene guideScene : guideScenes)
			guideService.deleteGuideScene(guideScene);
	}

    public void deleteSelectedScene(String id) {
        Scene scene = sceneService.getScene(id);
        if(scene != null){
            try {
                scene.setStatus(-1);
                sceneService.saveScene(scene);

                FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, "Scene Deleted", "Scene successfully deleted");
                FacesContext.getCurrentInstance().addMessage(null, m);

                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("success", true);
            }catch (HibernateException e){
                FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "Error occurred");
                FacesContext.getCurrentInstance().addMessage(null, m);

                RequestContext context = RequestContext.getCurrentInstance();
                context.addCallbackParam("success", false);
            }
        }
    }
}