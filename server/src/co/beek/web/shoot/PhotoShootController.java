package co.beek.web.shoot;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import co.beek.pano.model.dao.enterprizewizard.EWContact;
import co.beek.pano.model.dao.enterprizewizard.EWPhotoShoot;
import co.beek.pano.model.dao.enterprizewizard.EWPhotoShootResponse;
import co.beek.pano.model.dao.enterprizewizard.EWPhotoShootsResponse;
import co.beek.pano.model.dao.enterprizewizard.EWTeam;
import co.beek.pano.service.dataService.enterprizewizardService.EWService;
import co.beek.util.BeekSession;
import co.beek.web.BaseController;

@Scope("session")
@Controller("PhotoShootController")
public class PhotoShootController extends BaseController implements
		Serializable {
	private static final long serialVersionUID = -1081621548447371624L;

	@Inject
	private EWService ewService;

	private EWPhotoShoot photoShoot;

	@RequestMapping(value = "/shoot/{id}/view", method = RequestMethod.GET)
	public String mapView(@PathVariable("id") long id) {
		return "/views/shoot/view.jsf?id=" + id;
	}

	public void preRenderView() throws Exception {
		HttpServletRequest request = getRequest();
		if (!isLoggedIn(request)) {
			redirectLogin(request);
			return;
		}

		String photoShootId = request.getParameter("id");
		photoShoot = getPhotoShoot(photoShootId);

		if (photoShoot == null)
			throw new Exception("Photoshoot not found");
	}

	private EWPhotoShoot getPhotoShoot(String shootId) throws Exception {
		BeekSession session = getSession();
		List<EWPhotoShoot> photoShoots = session.getPhotoShoots();
		if (photoShoots == null)
			photoShoots = loadPhotoShoots(session);

		for (EWPhotoShoot shoot : photoShoots)
			if (shoot.getId().equals(shootId))
				return shoot;

		return null;
	}

	private List<EWPhotoShoot> loadPhotoShoots(BeekSession session)
			throws Exception {
		EWPhotoShootsResponse photoShootsResponse = ewService.queryPhotoShoots(
				session.getUsername(), session.getPassword());
		List<EWPhotoShoot> photoShoots = photoShootsResponse.getPhotoShoots();
		session.setPhotoShoots(photoShoots);

		return photoShoots;
	}

	public EWPhotoShoot getPhotoShoot() {
		return photoShoot;
	}

	public void acceptPhotoShoot() {
		try {
			acceptPhotoShoot(photoShoot);
			redirectHome();

		} catch (Exception e) {
			sendErrorDan("Error accepting photo shoot", e);
			showError("There has been an error accepting this photo shoot. "
					+ "Beek has been notified of the problem.");
		}
	}

	private void acceptPhotoShoot(EWPhotoShoot photoShoot) throws Exception {
		BeekSession session = getSession();
		EWPhotoShootResponse response = ewService.updatePhotoShootState(
				session.getUsername(), session.getPassword(),
				photoShoot.getId(),
				EWPhotoShoot.PhotoShootStatus.ACCEPTED.getCode());

		if (response.getSuccess()) {
			photoShoot = response.getPhotoShoot();

			// update our cached photoShoots
			updateShootInSession(photoShoot);

			// update the teams for htis user
			updateTeamsInSession(photoShoot);

			showMessage("Photo Shoot Accepted",
					"The photoShoot " + photoShoot.getLocationTitle()
							+ " has been accepted.");
		} else {
			throw new Exception(response.getErrorMessage());
		}
	}

	private void updateTeamsInSession(EWPhotoShoot photoShoot) {
		// The photographer is added to this team automatically.
		EWContact user = getUser();
		if (!user.isInTeam(photoShoot.getTeamId()))
			user.addTeam(new EWTeam(photoShoot.getTeamId()));
	}

	private void updateShootInSession(EWPhotoShoot photoShoot) {
		List<EWPhotoShoot> photoShoots = getSession().getPhotoShoots();
		for (int i = 0; i < photoShoots.size(); i++)
			if (photoShoots.get(i).getId().equals(photoShoot.getId()))
				photoShoots.set(i, photoShoot);
	}

	public void declinePhotoShoot() {
		try {
			declinePhotoShoot(photoShoot);
			redirect("/");

		} catch (Exception e) {
			sendErrorDan("Error declining photo shoot", e);
			showError("There has been an error declining this photo shoot. "
					+ "Beek has been notified of the problem.");
		}
	}

	private void declinePhotoShoot(EWPhotoShoot photoShoot) throws Exception {
		BeekSession session = getSession();
		EWPhotoShootResponse response = ewService.updatePhotoShootState(
				session.getUsername(), session.getPassword(),
				photoShoot.getId(),
				EWPhotoShoot.PhotoShootStatus.DECLINED.getCode());
		if (response.getSuccess()) {
			photoShoot = response.getPhotoShoot();
			// update our cached photoShoots
			updateShootInSession(photoShoot);
			showMessage("PhotoShoot Declined",
					"The photoShoot " + photoShoot.getLocationTitle()
							+ " has been declined.");
		} else {
			// show the error message
			throw new Exception(response.getErrorMessage());
		}
	}

	public void processPhotoShoot() {
		try {
			// Location location = processPhotoShoot(photoShoot);
			redirect("/shoot/" + photoShoot.getId() + "/process");
		} catch (Exception e) {
			sendErrorDan("Error processing photo shoot", e);
			showError("There has been an error processing this photo shoot. "
					+ "Beek has been notified of the problem.");
		}
	}
}
