package co.beek.web.shoot;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FlowEvent;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import co.beek.pano.model.beans.LocationMapModel;
import co.beek.pano.model.dao.enterprizewizard.EWPhotoShoot;
import co.beek.pano.model.dao.enterprizewizard.EWPhotoShootResponse;
import co.beek.pano.model.dao.enterprizewizard.EWPhotoShootsResponse;
import co.beek.pano.model.dao.enterprizewizard.EWTeam;
import co.beek.pano.model.dao.entities.Destination;
import co.beek.pano.model.dao.entities.Location;
import co.beek.pano.model.dao.entities.Scene;
import co.beek.pano.service.dataService.destinationService.DestinationService;
import co.beek.pano.service.dataService.enterprizewizardService.EWService;
import co.beek.pano.service.dataService.locationService.LocationService;
import co.beek.pano.service.dataService.sceneService.SceneService;
import co.beek.util.BeekSession;
import co.beek.web.BaseController;

@Scope("session")
@Controller("PhotoShootProcessController")
public class PhotoShootProcessController extends BaseController implements
		Serializable {
	private static final long serialVersionUID = -1081621548447371624L;

	@Inject
	private LocationService locationService;

	@Inject
	private SceneService sceneService;

	@Inject
	private DestinationService destinationService;
	
	@Inject
	private EWService ewService;

	private EWPhotoShoot photoShoot;

	private Location location;

	private LocationMapModel locationMapModel;

	private List<Scene> scenes;

	private String message;

	@RequestMapping(value = "/shoot/{id}/process", method = RequestMethod.GET)
	public String mapView(@PathVariable("id") long id) {
		return "/views/shoot/process.jsf?id=" + id;
	}

	public void preRenderView() throws Exception {
		if (isPostback())
			return;

		if (redirectingToLogin())
			return;

		String photoShootId = getRequest().getParameter("id");
		photoShoot = getShootFromSession(photoShootId);

		if (photoShoot == null)
			throw new Exception("Photoshoot not found");

		System.out.println(photoShoot.getTeamId());
		getUser().addTeam(new EWTeam(photoShoot.getTeamId()));

		if (!getUser().isInTeam(photoShoot.getTeamId()))
			throw new Exception("Cannot edit photoshoot, is not on team: "
					+ photoShoot.getTeamId() + ". This team should have "
					+ "been added to the user when the photoshoot was Accepted");

		Destination destination = destinationService
				.getDestinationByTitle(photoShoot.getDestination());

		Location existing = locationService.getLocationForOrder(photoShootId);
		if (existing != null) {
			location = existing;
			scenes = sceneService.getAllScenesForLocation(location.getId());
		} else {
			location = new Location(destination, photoShoot);
			scenes = null;
		}

		locationMapModel = new LocationMapModel(location);
	}

	private EWPhotoShoot getShootFromSession(String shootId) throws Exception {
		BeekSession session = getSession();
		List<EWPhotoShoot> photoShoots = session.getPhotoShoots();
		if (photoShoots == null)
			photoShoots = loadPhotoShoots(session);

		for (EWPhotoShoot shoot : photoShoots)
			if (shoot.getId().equals(shootId))
				return shoot;

		return null;
	}

	private List<EWPhotoShoot> loadPhotoShoots(BeekSession session)
			throws Exception {
		EWPhotoShootsResponse photoShootsResponse = ewService.queryPhotoShoots(
				session.getUsername(), session.getPassword());
		List<EWPhotoShoot> photoShoots = photoShootsResponse.getPhotoShoots();
		session.setPhotoShoots(photoShoots);

		return photoShoots;
	}

	public Location getLocation() {
		return location;
	}

	public LocationMapModel getLocationMapModel() {
		return locationMapModel;
	}

	public List<Scene> getScenes() {
		return scenes;
	}

	public int getNumScenes() {
		if (scenes != null)
			return scenes.size();

		return photoShoot.getEwScenes().size();
	}

	public String onFlowProcess(FlowEvent event) {
		if (event.getOldStep().equals("locationTab"))
			createScenesFromPhotoshoot();

		if (event.getOldStep().equals("scenesTab"))
			reloadScenesFromDB();

		return event.getNewStep();
	}

	private void reloadScenesFromDB() {
		this.scenes = sceneService.getAllScenesForLocation(location.getId());
		RequestContext.getCurrentInstance().update("scenesTab");
	}

	private void createScenesFromPhotoshoot() {
		try {

			locationService.saveLocation(location);

			// Create the scenes if they have not already
			if (scenes == null)
				scenes = convertPhotoShootScenes(photoShoot, location);

			if (scenes.size() > 0)
				setDefaultScene(location, scenes);

		} catch (Exception e) {
			sendErrorDan("Error saving the scenes for photoshoot: "
					+ photoShoot.getId(), e);
			showError("There has been an error saving the scenes from this photoshoot. "
					+ "Beek has been notified of the problem.");
		}
	}

	private List<Scene> convertPhotoShootScenes(EWPhotoShoot photoShoot,
			Location location) {
		List<Scene> scenes = new ArrayList<Scene>();
		HashMap<String, HashMap<String, String>> ews = photoShoot.getEwScenes();
		Iterator<Entry<String, HashMap<String, String>>> it = ews.entrySet()
				.iterator();
		while (it.hasNext()) {
			HashMap<String, String> ewScene = it.next().getValue();
			Scene scene = new Scene(location);
			scene.setTitle(ewScene.get("scene_name"));

			// save the scene in db.
			scene = sceneService.saveScene(scene);

			// add the new scene to the list
			scenes.add(scene);
		}

		return scenes;
	}

	public void addScene() {
		Scene scene = new Scene(location);
		scene.setTitle("NEW SCENE");
		scene = sceneService.saveScene(scene);
		scenes.add(scene);

		location.setSceneCount(scenes.size());

		// pass the data back for javascript access
		RequestContext context = RequestContext.getCurrentInstance();
		context.addCallbackParam("sceneId", scene.getId());
	}

	private void setDefaultScene(Location location, List<Scene> scenes) {
		// update default scene id after scenes are inserted
		Scene firstScene = scenes.get(0);
		location.setSceneCount(scenes.size());
		location.setDefaultSceneId(firstScene.getId());
	}

	public Scene getDefaultScene() {
		if (location.getDefaultSceneId() != null)
			return sceneService.getScene(location.getDefaultSceneId());

		return null;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void requestApproval() {
		try {
			HttpServletRequest request = getRequest();

			String approvalUrl = "http://" + request.getServerName()
					+ "/location/" + location.getId() + "/wizard";

			BeekSession session = getSession(request);
			EWPhotoShootResponse response = ewService.requestApproval(
					session.getUsername(), session.getPassword(),
					location.getPhotoShootId(),
					EWPhotoShoot.PhotoShootStatus.PROCESSED.getCode(),
					approvalUrl, message);

			if (!response.getSuccess())
				throw new Exception("Error response from EW: "
						+ response.getErrorMessage());

			location.setStatus(Location.STATE_PENDING);
			location.setSceneCount(scenes.size());
			locationService.saveLocation(location);

			showMessage("Approval Requested",
					"The location " + location.getTitle()
							+ " has been sent to the owner for approval.");
			redirectHome();

		} catch (Exception e) {
			sendErrorDan("Error requesting approval", e);
		}

	}

}
