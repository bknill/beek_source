package co.beek.web.user;

import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import co.beek.web.BaseController;

@Scope("session")
@Controller("UserInfoController")
public class UserInfoController extends BaseController implements
		Serializable {
	private static final long serialVersionUID = -1081621548447371624L;

	@RequestMapping(value = "/user/info", method = RequestMethod.GET)
	public String mapView() {
		return "/views/user/info.jsf";
	}
	
	
	public void preRenderView() throws Exception {
		if (redirectingToLogin())
			return;
	}
}
