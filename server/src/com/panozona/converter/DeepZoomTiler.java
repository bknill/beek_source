package com.panozona.converter;


/*
Copyright 2009 Zephyr Renner
This file is part of EquirectangulartoCubic.java.
EquirectangulartoCubic is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
EquirectangulartoCubic is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with EquirectangulartoCubic. If not, see http://www.gnu.org/licenses/.

This code is modified from DeepJZoom, courtesy of Glenn Lawrence, which is also licensed under the GPL.  Thank you!
*/

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Iterator;
import java.util.Vector;

/**
 * @author Glenn Lawrence
 */
public class DeepZoomTiler {

    static final String xmlHeader = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
    static final String schemaName = "http://schemas.microsoft.com/deepzoom/2009";

    private enum CmdParseState {DEFAULT, OUTPUTDIR, TILESIZE, OVERLAP, QUALITY, INPUTFILE}

    ;
    static Boolean deleteExisting = true;
    static String tileFormat = "jpg";

    // The following can be overriden/set by the indicated command line arguments
    static int tileSize = 512;            // -tilesize
    static int tileOverlap = 0;           // -overlap
    static Float quality = 0.8f;      // -quality (0.0 to 1.0)
    static File outputDir = null;         // -outputdir or -o
    static Boolean verboseMode = true;   // -verbose or -v
    static Boolean debugMode = true;     // -debug
    static Vector<File> inputFiles = new Vector<File>();  // must follow all other args
    static Vector<File> outputFiles = new Vector<File>();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        try {
            try {
                parseCommandLine(args);
                if (outputFiles.size() == 1) {
                    File outputFile = outputFiles.get(0);
                    if (!outputFile.exists()) {
                        File parentFile = outputFile.getAbsoluteFile().getParentFile();
                        String fileName = outputFile.getName();
                        outputFile = createDir(parentFile, fileName);
                    }
                }
                if (outputFiles.size() == 0) {
                    Iterator<File> itr = inputFiles.iterator();
                    while (itr.hasNext()) {
                        File inputFile = itr.next();
                        File parentFile = inputFile.getAbsoluteFile().getParentFile();
                        String fileName = inputFile.getName();
                        String nameWithoutExtension = fileName.substring(0, fileName.lastIndexOf('.'));
                        File outputFile = createDir(parentFile, "tiles_" + nameWithoutExtension);
                        outputFiles.add(outputFile);
                    }
                }
                if (debugMode) {
                    System.out.printf("tileSize=%d ", tileSize);
                    System.out.printf("tileOverlap=%d ", tileOverlap);
                    System.out.printf("quality=%d ", quality);
                    //System.out.printf("outputDir=%s\n", outputDir.getPath());
                }

            } catch (Exception e) {
                System.out.println("Invalid command line: " + e.getMessage());
                return;
            }

//             Iterator<File> itr = inputFiles.iterator();
//             while (itr.hasNext())
//                  processImageFile(itr.next(), outputDir);
            for (int i = 0; i < inputFiles.size(); i++) {
                processImageFile(inputFiles.get(i), outputFiles.get(i));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process the command line arguments
     *
     * @param args the command line arguments
     */
    private static void parseCommandLine(String[] args) throws Exception {
        CmdParseState state = CmdParseState.DEFAULT;
        for (int count = 0; count < args.length; count++) {
            String arg = args[count];
            switch (state) {
                case DEFAULT:
                    if (arg.equals("-verbose") || arg.equals("-v"))
                        verboseMode = true;
                    else if (arg.equals("-debug")) {
                        verboseMode = true;
                        debugMode = true;
                    } else if (arg.equals("-outputdir") || arg.equals("-o"))
                        state = CmdParseState.OUTPUTDIR;
                    else if (arg.equals("-tilesize"))
                        state = CmdParseState.TILESIZE;
                    else if (arg.equals("-overlap"))
                        state = CmdParseState.OVERLAP;
                    else if (arg.equals("-quality"))
                        state = CmdParseState.QUALITY;
                    else
                        state = CmdParseState.INPUTFILE;
                    break;
                case OUTPUTDIR:
                    outputDir = new File(args[count]);
                    state = CmdParseState.DEFAULT;
                    break;
                case TILESIZE:
                    tileSize = Integer.parseInt(args[count]);
                    state = CmdParseState.DEFAULT;
                    break;
                case OVERLAP:
                    tileOverlap = Integer.parseInt(args[count]);
                    state = CmdParseState.DEFAULT;
                    break;
                case QUALITY:
                    quality = Float.parseFloat(args[count]);
                    state = CmdParseState.DEFAULT;
                    break;
            }
            if (state == CmdParseState.INPUTFILE) {
                File inputFile = new File(arg);
                if (!inputFile.exists())
                    throw new FileNotFoundException("Missing input file: " + inputFile.getPath());
                //check if file is folder.
                if (inputFile.isDirectory()) {
                    findImagesRecursive(inputFile);
                } else {
                    inputFiles.add(inputFile);
                }
            }
        }
        if (inputFiles.size() == 0)
            throw new Exception("No input files given");
    }

    private static void findImagesRecursive(File parent) {
        if (parent.isDirectory()) {
            Vector<String> exts = new Vector<String>();
            exts.add("jpg");
            exts.add("jpeg");
            exts.add("JPG");
            exts.add("JPEG");
            FileListFilter fileFilter = new FileListFilter(exts);
            File[] files = parent.listFiles(fileFilter);
            java.util.List fileList = java.util.Arrays.asList(files);
            for (java.util.Iterator itr = fileList.iterator(); itr.hasNext(); ) {
                File f = (File) itr.next();
                inputFiles.add((File) f);
            }

            DirectoryFilter dirFilter = new DirectoryFilter();
            files = parent.listFiles(dirFilter);
            fileList = java.util.Arrays.asList(files);
            for (java.util.Iterator itr = fileList.iterator(); itr.hasNext(); ) {
                File f = (File) itr.next();
                findImagesRecursive(f);
            }

        }
    }

    public static void init(File inFile) throws IOException {
        File parentFile = inFile.getAbsoluteFile().getParentFile();
        String fileName = inFile.getName();
        String nameWithoutExtension = fileName.substring(0, fileName.lastIndexOf('.'));
        createDir(parentFile, "tiles_" + nameWithoutExtension);
    }

    /**
     * Process the given image file, producing its Deep Zoom output files
     * in a subdirectory of the given output directory.
     *
     * @param inFile    the file containing the image
     * @param outputDir the output directory
     */
    public static void processImageFile(File inFile, File outputDir) throws IOException {
    	System.out.println("DeepZoomTiler.processImageFile()");
    	if (verboseMode)
            System.out.printf("Processing image file: %s\n", inFile);

       // System.out.printf("totalMemory: "+Runtime.getRuntime().totalMemory());
       // /System.out.printf("freeMemory: "+Runtime.getRuntime().freeMemory());
        
      //  System.gc();
        
        //System.out.printf("totalMemory2 : "+Runtime.getRuntime().totalMemory());
       // System.out.printf("freeMemory2 : "+Runtime.getRuntime().freeMemory());
        
        String fileName = inFile.getName();
        String nameWithoutExtension = fileName.substring(0, fileName.lastIndexOf('.'));
        String pathWithoutExtension = outputDir + File.separator + nameWithoutExtension;

        BufferedImage image = loadImage(inFile);

        if (debugMode) {
            System.out.printf("passed in width=%d\n", image.getWidth());
            System.out.printf("passed in Height=%d\n", image.getHeight());
        }

        // Hack to create the right sized image.
        // inboud image size is 1911.
        // Same hack exists in panozona converter.
        image = resizeImage(image, 2048, 2048);

        int originalWidth = image.getWidth();
        int originalHeight = image.getHeight();

        if (debugMode) {
            System.out.printf("originalWidth=%d\n", originalWidth);
            System.out.printf("originalHeight=%d\n", originalHeight);
        }

        double maxDim = Math.max(originalWidth, originalHeight);

        int nLevels = (int) Math.ceil(Math.log(maxDim) / Math.log(2));

        if (debugMode)
            System.out.printf("nLevels=%d\n", nLevels);

        // Delete any existing output files and folders for this image

        File descriptor = new File(pathWithoutExtension + ".xml");
        if (descriptor.exists()) {
            if (deleteExisting)
                deleteFile(descriptor);
            else
                throw new IOException("File already exists in output dir: " + descriptor);
        }

        File imgDir = new File(pathWithoutExtension);
        if (imgDir.exists()) {
            if (deleteExisting) {
                if (debugMode)
                    System.out.printf("Deleting directory: %s\n", imgDir);
                deleteDir(imgDir);
            } else
                throw new IOException("Image directory already exists in output dir: " + imgDir);
        }

        imgDir = createDir(outputDir, nameWithoutExtension);

        double width = originalWidth;
        double height = originalHeight;

        for (int level = nLevels; level >= 8; level--) {
            int nCols = (int) Math.ceil(width / tileSize);
            int nRows = (int) Math.ceil(height / tileSize);
            if (debugMode)
                System.out.printf("level=%d w/h=%f/%f cols/rows=%d/%d\n",
                        level, width, height, nCols, nRows);

            File dir = createDir(imgDir, Integer.toString(level));
            for (int col = 0; col < nCols; col++) {
                for (int row = 0; row < nRows; row++) {
                    BufferedImage tile = getTile(image, row, col);
                    saveImageAtQuality(tile, dir + File.separator + col + '_' + row, quality);
                }
            }

            // Scale down image for next level
            width = Math.ceil(width / 2);
            height = Math.ceil(height / 2);
            if (width > 10 && height > 10) {
                // resize in stages to improve quality
                image = resizeImage(image, width * 1.66, height * 1.66);
                image = resizeImage(image, width * 1.33, height * 1.33);
            }
            image = resizeImage(image, width, height);
        }

        //saveImageDescriptor(originalWidth, originalHeight, descriptor);
    }


    /**
     * Delete a file
     *
     * @param file the path of the directory to be deleted
     */
    private static void deleteFile(File file) throws IOException {
        if (!file.delete())
            throw new IOException("Failed to delete file: " + file);
    }

    /**
     * Recursively deletes a directory
     *
     * @param dir the path of the directory to be deleted
     */
    private static void deleteDir(File dir) throws IOException {
        if (!dir.isDirectory())
            deleteFile(dir);
        else {
            for (File file : dir.listFiles()) {
                if (file.isDirectory())
                    deleteDir(file);
                else
                    deleteFile(file);
            }
            if (!dir.delete())
                throw new IOException("Failed to delete directory: " + dir);
        }
    }

    /**
     * Creates a directory
     *
     * @param parent the parent directory for the new directory
     * @param name   the new directory name
     */
    private static File createDir(File parent, String name) throws IOException {
        assert (parent.isDirectory());
        File result = new File(parent + File.separator + name);
        if (!result.exists()) {
            if (!result.mkdir())
                throw new IOException("Unable to create directory: " + result);
        }
        return result;
    }

    /**
     * Loads image from file
     *
     * @param file the file containing the image
     */
    private static BufferedImage loadImage(File file) throws IOException {
        BufferedImage result = null;
        try {
            result = ImageIO.read(file);
        } catch (Exception e) {
            throw new IOException("Cannot read image file: " + file);
        }
        return result;
    }

    /**
     * Gets an image containing the tile at the given row and column
     * for the given image.
     *
     * @param img - the input image from whihc the tile is taken
     * @param row - the tile's row (i.e. y) index
     * @param col - the tile's column (i.e. x) index
     */
    private static BufferedImage getTile(BufferedImage img, int row, int col) {
        int x = col * tileSize - (col == 0 ? 0 : tileOverlap);
        int y = row * tileSize - (row == 0 ? 0 : tileOverlap);
        int w = tileSize + (col == 0 ? 1 : 2) * tileOverlap;
        int h = tileSize + (row == 0 ? 1 : 2) * tileOverlap;

        if (x + w > img.getWidth())
            w = img.getWidth() - x;
        if (y + h > img.getHeight())
            h = img.getHeight() - y;

        if (debugMode)
            System.out.printf("getTile: row=%d, col=%d, x=%d, y=%d, w=%d, h=%d\n",
                    row, col, x, y, w, h);

        assert (w > 0);
        assert (h > 0);

        BufferedImage result = new BufferedImage(w, h, img.getType());
        Graphics2D g = result.createGraphics();
        g.drawImage(img, 0, 0, w, h, x, y, x + w, y + h, null);

        return result;
    }

    /**
     * Returns resized image
     * NB - useful reference on high quality image resizing can be found here:
     * http://today.java.net/pub/a/today/2007/04/03/perils-of-image-getscaledinstance.html
     *
     * @param width  the required width
     * @param height the frequired height
     * @param img    the image to be resized
     */
    private static BufferedImage resizeImage(BufferedImage img, double width, double height) {
        int w = (int) width;
        int h = (int) height;
        BufferedImage result = new BufferedImage(w, h, img.getType());
        Graphics2D g = result.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                RenderingHints.VALUE_INTERPOLATION_BICUBIC);
        g.drawImage(img, 0, 0, w, h, 0, 0, img.getWidth(), img.getHeight(), null);
        return result;
    }

    /**
     * Saves image to the given file
     * @param img the image to be saved
     * @param path the path of the file to which it is saved (less the extension)
     */
//     private static void saveImage(BufferedImage img, String path) throws IOException {
//         File outputFile = new File(path + "." + tileFormat);
//         try {
//             ImageIO.write(img, tileFormat, outputFile);
//         } catch (IOException e) {
//             throw new IOException("Unable to save image file: " + outputFile);
//         }
//     }

    /**
     * Saves image to the given file
     *
     * @param img     the image to be saved
     * @param path    the path of the file to which it is saved (less the extension)
     * @param quality the compression quality to use (0-1)
     */
    private static void saveImageAtQuality(BufferedImage img, String path, float quality) throws IOException {
        File outputFile = new File(path + "." + tileFormat);
        Iterator iter = ImageIO.getImageWritersByFormatName("jpeg");
        ImageWriter writer = (ImageWriter) iter.next();
        ImageWriteParam iwp = writer.getDefaultWriteParam();
        iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        iwp.setCompressionQuality(quality);
        FileImageOutputStream output = new FileImageOutputStream(outputFile);
        writer.setOutput(output);
        IIOImage image = new IIOImage(img, null, null);
        try {
            writer.write(null, image, iwp);
        } catch (IOException e) {
            throw new IOException("Unable to save image file: " + outputFile);
        } finally {
            output.close();
            writer.dispose();
        }

    }

    /**
     * Write image descriptor XML file
     *
     * @param width  image width
     * @param height image height
     * @param file   the file to which it is saved
     */
    private static void saveImageDescriptor(int width, int height, File file) throws IOException {
        Vector<String> lines = new Vector<String>();
        lines.add(xmlHeader);
        lines.add("<Image TileSize=\"" + tileSize + "\" Overlap=\"" + tileOverlap +
                "\" Format=\"" + tileFormat + "\" ServerFormat=\"Default\" xmnls=\"" +
                schemaName + "\">");
        lines.add("<Size Width=\"" + width + "\" Height=\"" + height + "\" />");
        lines.add("</Image>");
        saveText(lines, file);
    }

    /**
     * Saves strings as text to the given file
     *
     * @param lines the image to be saved
     * @param file  the file to which it is saved
     */
    private static void saveText(Vector lines, File file) throws IOException {
        FileOutputStream fos = null;
        PrintStream ps = null;
        try {
            fos = new FileOutputStream(file);
            ps = new PrintStream(fos);
            for (int i = 0; i < lines.size(); i++)
                ps.println((String) lines.elementAt(i));
        } catch (IOException e) {
            throw new IOException("Unable to write to text file: " + file);
        } finally {
            fos.close();
            ps.close();
        }
    }

}

class FileListFilterXXX implements FilenameFilter {
    private Vector<String> extensions;

    public FileListFilterXXX(Vector<String> extensions) {
        this.extensions = extensions;
    }

    public boolean accept(File directory, String filename) {
        if (extensions != null) {
            Iterator<String> itr = extensions.iterator();
            while (itr.hasNext()) {
                String ext = (String) itr.next();
                if (filename.endsWith('.' + ext)) return true;
            }
        }
        return false;
    }
}

class DirectoryFilter implements FilenameFilter {
    public DirectoryFilter() {
    }

    public boolean accept(File directory, String filename) {
        if (directory.isDirectory()) return true;
        return false;
    }
}