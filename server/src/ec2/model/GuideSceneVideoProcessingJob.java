package ec2.model;

import java.io.Serializable;

/**
 * Created by Ben on 28/06/2017.
 */
public class GuideSceneVideoProcessingJob extends Job implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;
    private String panoImage;
    private String panoVideo;
    private String videoStartTime;
    private String videoEndTime;
    private String videoVolume;
    private String voiceover;
    private String voiceoverStartTime;
    private String voiceoverEndTime;
    private String startAngle;
    private String videoImage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPanoImage() {
        return panoImage;
    }

    public void setPanoImage(String panoImage) {
        this.panoImage = panoImage;
    }

    public String getPanoVideo() {
        return panoVideo;
    }

    public void setPanoVideo(String panoVideo) {
        this.panoVideo = panoVideo;
    }

    public String getVideoStartTime() {
        return videoStartTime;
    }

    public void setVideoStartTime(String videoStartTime) {
        this.videoStartTime = videoStartTime;
    }

    public String getVideoEndTime() {
        return videoEndTime;
    }

    public void setVideoEndTime(String videoEndTime) {
        this.videoEndTime = videoEndTime;
    }

    public String getVideoVolume() {
        return videoVolume;
    }

    public void setVideoVolume(String videoVolume) {
        this.videoVolume = videoVolume;
    }

    public String getVoiceover() {
        return voiceover;
    }

    public void setVoiceover(String voiceover) {
        this.voiceover = voiceover;
    }

    public String getVoiceoverStartTime() {
        return voiceoverStartTime;
    }

    public void setVoiceoverStartTime(String voiceoverStartTime) {
        this.voiceoverStartTime = voiceoverStartTime;
    }

    public String getVoiceoverEndTime() {
        return voiceoverEndTime;
    }

    public void setVoiceoverEndTime(String voiceoverEndTime) {
        this.voiceoverEndTime = voiceoverEndTime;
    }

    public String getStartAngle() {
        return startAngle;
    }

    public void setStartAngle(String startAngle) {
        this.startAngle = startAngle;
    }

    public String getVideoImage() {
        return videoImage;
    }

    public void setVideoImage(String videoImage) {
        this.videoImage = videoImage;
    }
}
