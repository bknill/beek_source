package ec2.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Ben on 28/06/2017.
 */
public class GuideVideoProcessingJob extends Job  {

    private List<GuideSceneVideoProcessingJob> guideSceneVideoProcessingJobList;
    private String guideId;
    private String filename;
    private String soundTrack;

    public List<GuideSceneVideoProcessingJob> getGuideSceneVideoProcessingJobList() {
        return guideSceneVideoProcessingJobList;
    }

    public void setGuideSceneVideoProcessingJobList(List<GuideSceneVideoProcessingJob> guideSceneVideoProcessingJobList) {
        this.guideSceneVideoProcessingJobList = guideSceneVideoProcessingJobList;
    }

    public String getGuideId() {
        return guideId;
    }

    public void setGuideId(String guideId) {
        this.guideId = guideId;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getSoundTrack() {
        return soundTrack;
    }

    public void setSoundTrack(String soundTrack) {
        this.soundTrack = soundTrack;
    }
}
