package ec2.model;

public class Job {
    public enum JobStatus {
        RAW, RUNNING, ERROR, DONE
    }
    public enum JobType {
        TRANSCODING, PANO_PROCESSING,GUIDEVIDEO, SCENEVIDEO, GUIDESCENEVIDEO
    }

    private String ec2InstanceId;
    private String fileName;
    private String message;
    private JobType jobType;
    private JobStatus jobStatus;

    public Job(){}

    public String getEc2InstanceId() {
        return ec2InstanceId;
    }

    public void setEc2InstanceId(String ec2InstanceId) {
        this.ec2InstanceId = ec2InstanceId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public JobType getJobType() {
        return jobType;
    }

    public void setJobType(JobType jobType) {
        this.jobType = jobType;
    }

    public JobStatus getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(JobStatus jobStatus) {
        this.jobStatus = jobStatus;
    }
}
