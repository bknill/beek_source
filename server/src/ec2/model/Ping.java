package ec2.model;

public class Ping {
    public enum ServiceState {
        RUNNING, ERROR, STOPPED
    }
    private String ec2InstanceId;
    private ServiceState serviceState;
    private Job.JobType jobType;

    public Ping() {}

    public String getEC2InstanceId() {
        return ec2InstanceId;
    }

    public void setEC2InstanceId(String ec2InstanceId) {
        this.ec2InstanceId = ec2InstanceId;
    }

    public ServiceState getServiceState() {
        return serviceState;
    }

    public void setServiceState(ServiceState serviceState) {
        this.serviceState = serviceState;
    }

    public Job.JobType getJobType() {
        return jobType;
    }

    public void setJobType(Job.JobType jobType) {
        this.jobType = jobType;
    }
}