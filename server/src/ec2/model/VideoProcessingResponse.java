package ec2.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ben on 3/07/2017.
 */
public class VideoProcessingResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    private String lowRes;
    private String hiRes;
    private String sceneId;
    private String guideId;
    public List<VideoProcessingResponse> guideScenes = new ArrayList<VideoProcessingResponse>();

    public String getLowRes() {
        return lowRes;
    }

    public void setLowRes(String lowRes) {
        this.lowRes = lowRes;
    }

    public String getHiRes() {
        return hiRes;
    }

    public void setHiRes(String hiRes) {
        this.hiRes = hiRes;
    }

    public String getGuideId() {
        return guideId;
    }

    public void setGuideId(String guideId) {
        this.guideId = guideId;
    }

    public String getSceneId() {
        return sceneId;
    }

    public void setSceneId(String sceneId) {
        this.sceneId = sceneId;
    }
}
