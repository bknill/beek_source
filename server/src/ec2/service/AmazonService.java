package ec2.service;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sqs.AmazonSQSClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import java.io.File;

public class AmazonService {
    protected AmazonEC2Client ec2Client;
    protected AmazonS3 s3Client;
    protected AmazonSQSClient sqsClient;
    protected AmazonSNSClient snsClient;

    public AmazonService(){
        try {
            File file = new File("./resources/keys/credentials.properties");
            AWSCredentials credentials = new PropertiesCredentials(file);
            Region usEast1 = Region.getRegion(Regions.US_EAST_1);

            ec2Client = new AmazonEC2Client(credentials);
            ec2Client.setRegion(usEast1);

            s3Client = new AmazonS3Client(credentials);
            s3Client.setRegion(usEast1);

            sqsClient = new AmazonSQSClient(credentials);
            sqsClient.setRegion(usEast1);

            snsClient = new AmazonSNSClient(credentials);
            snsClient.setRegion(usEast1);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public AmazonEC2Client getEc2Client() {
        return ec2Client;
    }

    public AmazonS3 getS3Client() {
        return s3Client;
    }

    public AmazonSQSClient getSqsClient() {
        return sqsClient;
    }

    public AmazonSNSClient getSnsClient() {
        return snsClient;
    }
}
