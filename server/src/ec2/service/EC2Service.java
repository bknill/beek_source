package ec2.service;

import com.amazonaws.services.ec2.model.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class EC2Service extends AmazonService{
    @Value("#{buildProperties.ec2ImageId}")
    private String imageId;

    @Value("#{buildProperties.ec2ProcessingGuideVideoImageId}")
    private String guideVideoProcessingImageId;

    @Value("#{buildProperties.ec2GroupId}")
    private String groupId;

    @Value("#{buildProperties.ec2KeyName}")
    private String keyName;

    @Value("#{buildProperties.ec2SubnetId}")
    private String subnetId;

    public boolean waitForInstanceToStart(Instance instance) {
        System.out.println("waitForInstanceToStart()");
        // to test locally and to simulate wait for ec2 to run, uncomment this

/*       try {
           Thread.sleep(20000);
        } catch (Exception e) {
            e.printStackTrace();
       }
        return true;*/

    return checkInstanceState(instance, "running");
    }

    public boolean waitForInstanceToStop(Instance instance) {
        return checkInstanceState(instance, "stopped");
    }

    private boolean checkInstanceState(Instance instance, String finalState) {
        System.out.println("checkInstanceState()");
        String instanceState = instance.getState().getName();
        System.out.println(instanceState);
        try {
            while (!instanceState.equals(finalState)) {
                Thread.sleep(30000);
                DescribeInstanceStatusResult statusRequest = ec2Client.describeInstanceStatus(
                        new DescribeInstanceStatusRequest().withInstanceIds(instance.getInstanceId()));

                System.out.println(statusRequest.getInstanceStatuses().size());
                System.out.println(statusRequest.getInstanceStatuses());

                if (statusRequest.getInstanceStatuses().size() > 0) {
                    InstanceStatus instanceStatus = statusRequest.getInstanceStatuses().get(0);
                    System.out.println(instanceStatus.getInstanceState().getName());
                    instance.setState(instanceStatus.getInstanceState());
                    instanceState = instanceStatus.getInstanceState().getName();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return instanceState.equals(finalState);
    }

    public Instance createAndRunInstance(String ami){
        System.out.println("createAndRunInstance()");
        Collection<String> groupIds = new ArrayList<String>();
        groupIds.add(groupId);
        RunInstancesRequest runInstancesRequest =  new RunInstancesRequest();
        runInstancesRequest.withImageId(ami)
                .withInstanceType(InstanceType.T2Medium)
                .withMinCount(1)
                .withMaxCount(1)
                .withKeyName(keyName)
                .withSubnetId(subnetId)
                .setSecurityGroupIds(groupIds);
        RunInstancesResult runInstancesResult = ec2Client.runInstances(runInstancesRequest);
        List<Instance> instances = runInstancesResult.getReservation().getInstances();
        System.out.println(instances.size());
        if(instances.size() > 0){
            return instances.get(0);
        }
        return null;
    }

    public StartInstancesResult startInstance(Instance instance) {
        StartInstancesRequest startRequest = new StartInstancesRequest().withInstanceIds(instance.getInstanceId());
        return ec2Client.startInstances(startRequest);
    }

    public StopInstancesResult stopInstance(Instance instance) {
        StopInstancesRequest stopRequest = new StopInstancesRequest().withInstanceIds(instance.getInstanceId());
        return ec2Client.stopInstances(stopRequest);
    }

    public void rebootInstance(Instance instance) {
        RebootInstancesRequest rebootRequest = new RebootInstancesRequest().withInstanceIds(instance.getInstanceId());
        ec2Client.rebootInstances(rebootRequest);
    }

    public void terminateInstance(Instance instance){
        TerminateInstancesRequest terminateRequest = new TerminateInstancesRequest().withInstanceIds(instance.getInstanceId());
        ec2Client.terminateInstances(terminateRequest);
    }

    public void printInstance(Instance instance) {
        System.out.println(instance.getImageId());
        System.out.println(instance.getInstanceType());
        System.out.println(instance.getKeyName());
        System.out.println(instance.getSecurityGroups());
        System.out.println(instance.getPublicIpAddress() + ", " + instance.getState());
        System.out.println(instance.getSubnetId());
        System.out.println(instance.getInstanceId());
        System.out.println(instance.getPublicIpAddress());
    }
}
