package ec2.service;

import co.beek.pano.model.dao.entities.*;
import co.beek.pano.service.dataService.guideSceneService.GuideSceneService;
import co.beek.pano.service.dataService.guideService.GuideService;
import co.beek.pano.service.dataService.sceneService.SceneService;
import co.beek.pano.service.dataService.taskService.TaskService;
import ec2.model.GuideSceneVideoProcessingJob;
import ec2.model.GuideVideoProcessingJob;
import com.amazonaws.services.ec2.model.Instance;
import com.google.gson.Gson;
import ec2.model.Job;
import ec2.model.Job.JobStatus;
import ec2.model.VideoProcessingResponse;
import flexjson.JSONDeserializer;
import flexjson.JSONException;
import flexjson.JSONSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GuideVideoService extends EC2Service {
    public static String S3_BUCKET_NAME = "testing-fo";
    public static String S3_RAW_DIR_NAME = "raw";

    @Value("#{buildProperties.jobsQueueURL}")
    private String jobsQueueURL;

    @Inject
    private GuideService guideService;

    @Inject
    private GuideSceneService guideSceneService;

    @Inject
    private TaskService taskService;

    @Async
    public void handlePing(Instance instance,ScenePanoTask scenePanoTask) {
        if (instance == null) return;

        try {
            if(scenePanoTask.type == ScenePanoTask.TYPE_GUIDEVIDEO){

                GuideVideoProcessingJob  job = createGuideVideoJob(scenePanoTask.getGuide());
                job.setJobType(Job.JobType.GUIDEVIDEO);
                job.setJobStatus(Job.JobStatus.RAW);
                job.setEc2InstanceId(instance.getInstanceId());
                String json =  new JSONSerializer().deepSerialize(job);
                System.out.println(json);
                sqsClient.sendMessage(jobsQueueURL,json);
            }
            else{
                GuideDetail guide = scenePanoTask.getGuide();

                if(guide == null)
                    guide = guideService.getGuideDetail(scenePanoTask.getGuideId());

                GuideScene guideScene = guide.getGuideScene(scenePanoTask.getSceneId());
                guideScene.setVideo(null);
                guideSceneService.saveGuideScene(guideScene);

                GuideSceneVideoProcessingJob job = createGuideSceneVideoProcessingJob(guideScene,false);
                job.setJobType(Job.JobType.GUIDESCENEVIDEO);
                job.setJobStatus(Job.JobStatus.RAW);
                job.setEc2InstanceId(instance.getInstanceId());
                String json =  new JSONSerializer().deepSerialize(job);
                System.out.println(json);
                sqsClient.sendMessage(jobsQueueURL,json);
            }

        } catch (Exception e) {
            e.printStackTrace();
            if (instance != null) {
                stopInstance(instance);

            }
            e.printStackTrace();
        }
    }

    @Async
    public Boolean handleJobResponse(Job job, Instance instance, ScenePanoTask scenePanoTask) {

        if (job.getJobStatus() == JobStatus.DONE) {
            VideoProcessingResponse response = null;

            try{
                response = new JSONDeserializer<VideoProcessingResponse>().use(null,VideoProcessingResponse.class).deserialize(job.getMessage());
            }
            catch (JSONException e){e.printStackTrace();}

            if(response != null){

                GuideDetail guide = scenePanoTask.getGuide();

                if(guide == null)
                    guide = guideService.getGuideDetail(scenePanoTask.getGuideId());


                if(scenePanoTask.getGuideId() != null && scenePanoTask.getSceneId() != null){

                    VideoSettings settings = new VideoSettings();
                    settings.setStartTime(0.0);
                    settings.setEndTime(0.0);
                    settings.setHiRes(response.getHiRes());
                    settings.setLowRes(response.getLowRes());

                    GuideScene guideScene = guide.getGuideScene(scenePanoTask.getSceneId());
                    String video = new JSONSerializer().serialize(settings);

                    guideScene.setVideo(video);
                    guideSceneService.saveGuideScene(guideScene);

                    if(guide.videoProcessingComplete()){
                        ScenePanoTask task = new ScenePanoTask();
                        task.setGuideId(scenePanoTask.getGuideId());
                        task.setType(ScenePanoTask.TYPE_GUIDEVIDEO);
                        task.status = ScenePanoTask.PENDING;
                        taskService.addTask(task);

                    }
                }
                else if(scenePanoTask.getGuideId() != null && scenePanoTask.getSceneId() == null){
                    guide.setOutputVideo(job.getFileName());
                    guideService.saveGuideDetail(guide);
                }

                taskService.deleteTask(scenePanoTask);

                if (instance != null) {
                    terminateInstance(instance);
                    stopInstance(instance);
                }
            }

        } else if (job.getJobStatus() == JobStatus.ERROR) {
            System.out.println("error");
        }


        return false;
    }


    public GuideVideoProcessingJob createGuideVideoJob(GuideDetail guide) {

        GuideVideoProcessingJob guideVideoProcessingJob = new GuideVideoProcessingJob();

        List<GuideSceneVideoProcessingJob> list = new ArrayList<GuideSceneVideoProcessingJob>();

        for (GuideScene guideScene :  guide.returnGuideScenes())
            list.add(createGuideSceneVideoProcessingJob(guideScene,true));

        guideVideoProcessingJob.setGuideSceneVideoProcessingJobList(list);
        guideVideoProcessingJob.setGuideId(guide.getId());

        if(guide.soundtrack != null)
            guideVideoProcessingJob.setSoundTrack(guide.soundtrack);

        return guideVideoProcessingJob;
    }

    public GuideSceneVideoProcessingJob createSceneVideoProcessingJob(Scene scene){

        GuideSceneVideoProcessingJob guideSceneVideoProcessingJob = new GuideSceneVideoProcessingJob();
        guideSceneVideoProcessingJob.setId(scene.getId());

        if (scene.getVideo() != null) {
            try{

                String videoJSON = scene.getVideo();
                VideoSettings videoSettings = new JSONDeserializer<VideoSettings>().use(null, VideoSettings.class).deserialize(videoJSON);

                if (videoSettings.getFilename() != null)
                    guideSceneVideoProcessingJob.setPanoVideo(videoSettings.getFilename());
                else
                    return null;

                if (videoSettings.getStartTime() != null)
                    guideSceneVideoProcessingJob.setVideoStartTime(videoSettings.getStartTime().toString());

                if (videoSettings.getEndTime() != null)
                    guideSceneVideoProcessingJob.setVideoEndTime(videoSettings.getEndTime().toString());

                if(videoSettings.getVolume() != null)
                    guideSceneVideoProcessingJob.setVideoVolume(videoSettings.getVolume().toString());

            }catch (JSONException e){
                guideSceneVideoProcessingJob.setPanoVideo(scene.getVideo());
            }

        } else
            guideSceneVideoProcessingJob.setPanoImage(scene.getPanoName());

        if(scene.getCameraDefault() != null){
            try{
                CameraDefaults cameraDefaults = new JSONDeserializer<CameraDefaults>().use(null, CameraDefaults.class).deserialize(scene.getCameraDefault());
                guideSceneVideoProcessingJob.setStartAngle(cameraDefaults.getTheta().toString());
            }
            catch (JSONException e){
                e.printStackTrace();
                // guideSceneVideoProcessingJob.setStartAngle(guideScene.getScene().getCameraDefault());
            }
        }

        if (scene.getVoiceover() != null) {

            Voiceover voiceover = new Gson().fromJson(scene.getVoiceover(), Voiceover.class);

            if (voiceover != null) {

                guideSceneVideoProcessingJob.setVoiceover(voiceover.getFilename());

                if (voiceover.getStartTime() != null)
                    guideSceneVideoProcessingJob.setVoiceoverStartTime(voiceover.getStartTime().toString());

                if (voiceover.getEndTime() != null)
                    guideSceneVideoProcessingJob.setVoiceoverEndTime(voiceover.getEndTime().toString());
            }
        }
        return guideSceneVideoProcessingJob;

    }

    public GuideSceneVideoProcessingJob createGuideSceneVideoProcessingJob(GuideScene guideScene, Boolean useFinal){

        GuideSceneVideoProcessingJob guideSceneVideoProcessingJob = new GuideSceneVideoProcessingJob();
        guideSceneVideoProcessingJob.setId(guideScene.getId());

        if(useFinal && guideScene.getVideo() != null)
        {
            VideoSettings guideSceneVideoSettings = new JSONDeserializer<VideoSettings>().use(null, VideoSettings.class).deserialize(guideScene.getVideo());
            guideSceneVideoProcessingJob.setPanoVideo(guideSceneVideoSettings.getHiRes());
            return guideSceneVideoProcessingJob;
        }

        if (guideScene.getScene().getVideo() != null) {
            try{
                String videoJSON = guideScene.getScene().getVideo();
                VideoSettings videoSettings = new JSONDeserializer<VideoSettings>().use(null, VideoSettings.class).deserialize(videoJSON);

/*                if(guideScene.getVideo() != null){
                    VideoSettings guideSceneVideoSettings = new JSONDeserializer<VideoSettings>().use(null, VideoSettings.class).deserialize(guideScene.getVideo());

                 if (guideSceneVideoSettings.getStartTime() != null && (guideSceneVideoSettings.getStartTime() > 0.0))
                        videoSettings.setStartTime(guideSceneVideoSettings.getStartTime());

                    if (guideSceneVideoSettings.getEndTime() != null && (guideSceneVideoSettings.getEndTime() > 0.0))
                        videoSettings.setEndTime(guideSceneVideoSettings.getEndTime());

                }*/

                if (videoSettings.getFilename() != null)
                    guideSceneVideoProcessingJob.setPanoVideo(videoSettings.getFilename());
                else
                    return null;

                if (videoSettings.getStartTime() != null)
                    guideSceneVideoProcessingJob.setVideoStartTime(videoSettings.getStartTime().toString());

                if (videoSettings.getEndTime() != null)
                    guideSceneVideoProcessingJob.setVideoEndTime(videoSettings.getEndTime().toString());

                if(videoSettings.getVolume() != null)
                    guideSceneVideoProcessingJob.setVideoVolume(videoSettings.getVolume().toString());

                if(videoSettings.getVideoImage() != null)
                    guideSceneVideoProcessingJob.setVideoImage(videoSettings.getVideoImage());

            }catch (JSONException e){
                e.printStackTrace();
                guideSceneVideoProcessingJob.setPanoVideo(guideScene.getScene().getVideo());
            }

        } else
            guideSceneVideoProcessingJob.setPanoImage(guideScene.getScene().getPanoName());

        if(guideScene.getScene().getCameraDefault() != null){
            try{
                CameraDefaults cameraDefaults = new JSONDeserializer<CameraDefaults>().use(null, CameraDefaults.class).deserialize(guideScene.getScene().getCameraDefault());
                guideSceneVideoProcessingJob.setStartAngle(cameraDefaults.getTheta().toString());
            }
            catch (JSONException e){
                e.printStackTrace();
                // guideSceneVideoProcessingJob.setStartAngle(guideScene.getScene().getCameraDefault());
            }
        }

        if (guideScene.getVoiceovers() != null || guideScene.getScene().getVoiceover() != null) {

            if(guideScene.getVoiceovers() != null )
                guideSceneVideoProcessingJob.setVoiceover(guideScene.getVoiceovers().toString());
            else{
                Voiceover voiceover = new Gson().fromJson(guideScene.getScene().getVoiceover(), Voiceover.class);

                if (voiceover != null) {

                    guideSceneVideoProcessingJob.setVoiceover(voiceover.getFilename());

                    if (voiceover.getStartTime() != null)
                        guideSceneVideoProcessingJob.setVoiceoverStartTime(voiceover.getStartTime().toString());

                    if (voiceover.getEndTime() != null)
                        guideSceneVideoProcessingJob.setVoiceoverEndTime(voiceover.getEndTime().toString());
                }
            }
        }

        return guideSceneVideoProcessingJob;

    }


}