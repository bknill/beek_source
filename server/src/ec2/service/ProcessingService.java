package ec2.service;

import co.beek.Constants;
import co.beek.pano.model.dao.entities.*;
import co.beek.pano.service.dataService.BeekService;
import co.beek.pano.service.dataService.MailService;
import co.beek.pano.service.dataService.sceneService.SceneService;
import co.beek.pano.service.dataService.taskService.TaskService;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import ec2.model.*;
import flexjson.JSONDeserializer;
import flexjson.JSONException;
import flexjson.JSONSerializer;
import org.primefaces.push.EventBus;
import org.primefaces.push.EventBusFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

@Service
public class ProcessingService extends EC2Service {
    @Inject private SnsService snsService;
    @Inject private TaskService taskService;
    @Inject private SceneService sceneService;
    @Inject private MailService emailService;
    @Inject private PanoService panoService;
    @Inject private TranscodingService transcodingService;
    @Inject private GuideVideoService guideVideoService;
    @Inject private SceneVideoService sceneVideoService;
    @Inject private BeekService beekService;

    private Beek beek;

    @Value("#{buildProperties.jobsQueueURL}")
    private String jobsQueueURL;

    @Value("#{buildProperties.tempAssetsBucket}")
    private String tempAssetsBucket;

    private Map<Instance, ScenePanoTask> jobsMap = new HashMap<Instance, ScenePanoTask>();

    @Async
    public void start() {

     if (!snsService.isSubscribe()){
            snsService.subscribe();
             return;
        }

        ScenePanoTask scenePanoTask = taskService.getTaskFromQueue();

        if (scenePanoTask == null)
            return;

        beek = beekService.getBeek();

        scenePanoTask.status = ScenePanoTask.PROCESSING;
        taskService.updateTask(scenePanoTask);

        if(scenePanoTask.getSceneId() != null)
          createServer(scenePanoTask);
        else if (scenePanoTask.getGuideId() != null){

            if(scenePanoTask.getType() == ScenePanoTask.TYPE_GUIDEVIDEO){
                if(scenePanoTask.getGuide().videoProcessingComplete() ){
                    System.out.println("video processing complete, creating server " + scenePanoTask.getGuideId());
                    createServer(scenePanoTask);
                    return;
                }

                System.out.println("video processing incomplete, creating tasks " + scenePanoTask.getGuide().returnGuideScenes().size());
                //taskService.deleteTask(scenePanoTask);

                for(GuideScene guideScene : scenePanoTask.getGuide().returnGuideScenes()){

                    if(guideScene.getVideo() != null)
                        continue;

                    ScenePanoTask guideSceneTask = new ScenePanoTask();
                    guideSceneTask.setGuideId(scenePanoTask.getGuideId());
                    guideSceneTask.setSceneId(guideScene.getSceneId());
                    guideSceneTask.type = ScenePanoTask.TYPE_GUIDESCENEVIDEO;
                    guideSceneTask.status = ScenePanoTask.PENDING;
                    taskService.addTask(guideSceneTask);
                }
            }
        }
    }

    @Async
    public void createServer(ScenePanoTask scenePanoTask) {

        System.out.println("createServer");

        Instance instance = createAndRunInstance(beek.getAmi());
        boolean isEc2Running = waitForInstanceToStart(instance);

        if (isEc2Running) {
            jobsMap.put(instance, scenePanoTask);
        } else {
            scenePanoTask.status = ScenePanoTask.PENDING;
            taskService.updateTask(scenePanoTask);
        }
    }


    @Async
    public void handlePing(Ping ping) {

        String instanceId = ping.getEC2InstanceId();
        Instance instance = null;
        ScenePanoTask scenePanoTask = null;

        for (Map.Entry<Instance, ScenePanoTask> entries : jobsMap.entrySet()) {
            if (entries.getKey().getInstanceId().equals(instanceId)) {
                instance = entries.getKey();
                scenePanoTask = entries.getValue();
                break;
            }
        }
        if (instance == null)
            return;

        if (scenePanoTask == null) {
            stop(instance);
            return;
        }

        System.out.println("HANDLE PING " + scenePanoTask.getType() + " " + scenePanoTask.getSceneId());

        if (scenePanoTask.getType() == ScenePanoTask.TYPE_PANO)
            panoService.handlePing(instance,scenePanoTask);
        else if(scenePanoTask.getType() == ScenePanoTask.TYPE_TRANSCODE)
            transcodingService.handlePing(instance,scenePanoTask);
        else  if(scenePanoTask.getType() == ScenePanoTask.TYPE_GUIDEVIDEO || scenePanoTask.getType() == ScenePanoTask.TYPE_GUIDESCENEVIDEO)
            guideVideoService.handlePing(instance,scenePanoTask);
        else  if(scenePanoTask.getType() == ScenePanoTask.TYPE_SCENEVIDEO)
            sceneVideoService.handlePing(instance,scenePanoTask);
    }

    @Async
    public void handleJob(Job job) {
        String instanceId = job.getEc2InstanceId();

        Instance instance = null;
        ScenePanoTask scenePanoTask = null;
        for (Map.Entry<Instance, ScenePanoTask> entries : jobsMap.entrySet()) {
            if (entries.getKey().getInstanceId().equals(instanceId)) {
                instance = entries.getKey();
                scenePanoTask = entries.getValue();
                break;
            }
        }
        if (instance == null)
            return;

        if (scenePanoTask == null) {
            stop(instance);
            return;
        }

        System.out.println("HANDLE JOB: " + scenePanoTask.getType() + scenePanoTask.getSceneId());

        if (scenePanoTask.getType() == ScenePanoTask.TYPE_PANO)
            panoService.handleJob(job,instance,scenePanoTask);
        else if(scenePanoTask.getType() == ScenePanoTask.TYPE_TRANSCODE)
            transcodingService.handleJobResponse(job, instance, scenePanoTask);
        else if(scenePanoTask.getType() == ScenePanoTask.TYPE_GUIDEVIDEO || scenePanoTask.getType() == ScenePanoTask.TYPE_GUIDESCENEVIDEO )
            guideVideoService.handleJobResponse(job, instance, scenePanoTask);
        else if(scenePanoTask.getType() == ScenePanoTask.TYPE_SCENEVIDEO)
            sceneVideoService.handleJobResponse(job, instance, scenePanoTask);
    }

    public void stop(Instance instance) {
        terminateInstance(instance);
//        stopInstance(instance);
        jobsMap.remove(instance);
    }

    public void pushProcessingStartsMsg(ScenePanoTask scenePanoTask){
        EventBus pushContext = EventBusFactory.getDefault().eventBus();
        String pushString = "{\"success\": \"true\", \"eventName\": \"updateSceneTab\", \"sceneId\": \""+scenePanoTask.getSceneId()+"\", \"sceneName\": \""+scenePanoTask.getScene().getTitle()+"\", \"message\": \"Processing Starts\"}";
        pushContext.publish("/", pushString);
    }
    public void pushProcessingFinishedMsg(ScenePanoTask scenePanoTask){
        EventBus pushContext = EventBusFactory.getDefault().eventBus();
        String pushString = "{\"success\": \"true\", \"eventName\": \"enableSceneTab\", \"sceneId\": \""+scenePanoTask.getSceneId()+"\", \"sceneName\": \""+scenePanoTask.getScene().getTitle()+"\", \"message\": \"Processing Finished\"}";
        pushContext.publish("/", pushString);
    }
    public void pushProcessingFailedMsg(ScenePanoTask scenePanoTask){
        EventBus pushContext = EventBusFactory.getDefault().eventBus();
        String pushString = "{\"success\": \"false\", \"eventName\": \"enableSceneTab\", \"sceneId\": \""+scenePanoTask.getSceneId()+"\", \"sceneName\": \""+scenePanoTask.getScene().getTitle()+"\", \"message\": \"Processing Failed\"}";
        pushContext.publish("/", pushString);
    }

/*    public static void main(String[] args) {
        ProcessingService panoService = new ProcessingService();
    }*/
}
