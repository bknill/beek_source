package ec2.service;

import co.beek.pano.model.dao.entities.*;
import co.beek.pano.service.dataService.guideService.GuideService;
import co.beek.pano.service.dataService.sceneService.SceneService;
import co.beek.pano.service.dataService.taskService.TaskService;
import com.amazonaws.services.ec2.model.Instance;
import com.google.gson.Gson;
import ec2.model.GuideSceneVideoProcessingJob;
import ec2.model.GuideVideoProcessingJob;
import ec2.model.Job;
import ec2.model.Job.JobStatus;
import ec2.model.VideoProcessingResponse;
import flexjson.JSONDeserializer;
import flexjson.JSONException;
import flexjson.JSONSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Service
public class SceneVideoService extends EC2Service {
    public static String S3_BUCKET_NAME = "testing-fo";
    public static String S3_RAW_DIR_NAME = "raw";

    @Value("#{buildProperties.jobsQueueURL}")
    private String jobsQueueURL;

    private Scene scene;

    @Inject
    private SceneService sceneService;

    @Inject
    private TaskService taskService;

    @Async
    public void handlePing(Instance instance,ScenePanoTask scenePanoTask) {
        if (instance == null) return;

        try {

            scene = scenePanoTask.getScene();

            System.out.println("handlePing " + scene.getId() + " - " +  scene.getVideo());

            GuideSceneVideoProcessingJob job = createSceneVideoJob(scenePanoTask.getScene());
            job.setJobStatus(JobStatus.RAW);
            job.setJobType(Job.JobType.SCENEVIDEO);
            job.setEc2InstanceId(instance.getInstanceId());
            String json =  new JSONSerializer().deepSerialize(job);
            System.out.println(json);
            sqsClient.sendMessage(jobsQueueURL,json);

        } catch (Exception e) {
            if (instance != null) {
                stopInstance(instance);

            }
            e.printStackTrace();
        }
    }

    @Async
    public void handleJobResponse(Job job, Instance instance, ScenePanoTask scenePanoTask) {
        System.out.println(job.getMessage());
        System.out.println("do something with scene video job = " + job.getEc2InstanceId() + " " + instance.getInstanceId() + " " + scenePanoTask.getSceneId());

        scene = scenePanoTask.getScene();

        System.out.println(job.getJobStatus());
        if (job.getJobStatus() == JobStatus.DONE) {
            VideoProcessingResponse response = null;

            try{
                response = new JSONDeserializer<VideoProcessingResponse>().use(null,VideoProcessingResponse.class).deserialize(job.getMessage());
                System.out.println("got response " + response.getGuideId());
            }
            catch (JSONException e){e.printStackTrace();}

            if(response != null){

                    {
                        try{
                            VideoSettings defaultSettings = scene.getVideoSettings();

                            VideoSettings settings = new VideoSettings();
                            settings.setHiRes(response.getHiRes());
                            settings.setLowRes(response.getLowRes());

                            if(defaultSettings != null){
                                settings.setVolume(defaultSettings.getVolume());
                                settings.setBrightness(defaultSettings.getBrightness());
                                settings.setContrast(defaultSettings.getContrast());
                                settings.setSaturation(defaultSettings.getSaturation());
                                settings.setHue(defaultSettings.getHue());
                            }

                            String json = new JSONSerializer().serialize(settings);
                            scene.setOutputVideo(json);
                            sceneService.saveScene(scene);
                            scenePanoTask.status = ScenePanoTask.COMPLETE;
                            taskService.updateTask(scenePanoTask);
                            if (instance != null) {
                                terminateInstance(instance);
                            }


                        }
                        catch (JSONException e){e.printStackTrace();}
                    }
                }
        }
         else if (job.getJobStatus() == JobStatus.ERROR) {
            System.out.println("error");
            }


        if (instance != null) {
            terminateInstance(instance);
        }
    }


    public GuideSceneVideoProcessingJob createSceneVideoJob(Scene scene) {
     //   handlePing 1760 - {"brightness":0.0,"class":"co.beek.pano.model.dao.entities.VideoSettings","contrast":0.0,"duration":null,"endTime":0.0,"filename":"\"34292072-408a-4895-85d7-f62592704996.mp4\"","height":null,"hiRes":null,"hue":null,"lowRes":null,"saturation":null,"startTime":0.0,"volume":null,"width":null}

        System.out.println("createSceneVideoJob " + scene.getId());

            GuideSceneVideoProcessingJob guideSceneVideoProcessingJob = new GuideSceneVideoProcessingJob();
            guideSceneVideoProcessingJob.setId(scene.getId());

            if (scene.getVideo() != null) {
                try{
                    VideoSettings videoSettings = new JSONDeserializer<VideoSettings>().use(null, VideoSettings.class).deserialize(scene.getVideo());

                    if (videoSettings.getFilename() != null)
                        guideSceneVideoProcessingJob.setPanoVideo(videoSettings.getFilename());


                    guideSceneVideoProcessingJob.setVideoStartTime(videoSettings.getStartTime().toString());

                    if(videoSettings.getEndTime() > 0.0)
                        guideSceneVideoProcessingJob.setVideoEndTime(videoSettings.getEndTime().toString());
                    else
                        guideSceneVideoProcessingJob.setVideoEndTime(videoSettings.getDuration().toString());

                }catch (JSONException e){
                       e.printStackTrace();
                    System.out.println(scene.getVideo());
                    System.out.println(scene.getVideo().replaceAll("^\"|\"$", ""));
                    guideSceneVideoProcessingJob.setPanoVideo(scene.getVideo().replaceAll("^\"|\"$", ""));
                }

            } else
                guideSceneVideoProcessingJob.setPanoImage(scene.getPanoName());

/*            if(scene.getCameraDefault() != null){
                try{
                    CameraDefaults cameraDefaults = new JSONDeserializer<CameraDefaults>().use(null, CameraDefaults.class).deserialize(scene.getCameraDefault());
                    guideSceneVideoProcessingJob.setStartAngle(cameraDefaults.getTheta().toString());

                }
                catch (JSONException e){
                    e.printStackTrace();
                   // guideSceneVideoProcessingJob.setStartAngle(guideScene.getScene().getCameraDefault());
                }
            }*/

            if (scene.getVoiceover() != null) {

                    Voiceover voiceover = new Gson().fromJson(scene.getVoiceover(), Voiceover.class);

                    if (voiceover != null) {

                        guideSceneVideoProcessingJob.setVoiceover(voiceover.getFilename());

                        if (voiceover.getStartTime() != null)
                            guideSceneVideoProcessingJob.setVoiceoverStartTime(voiceover.getStartTime().toString());

                        if (voiceover.getEndTime() != null)
                            guideSceneVideoProcessingJob.setVoiceoverEndTime(voiceover.getEndTime().toString());
                    }
                }

        return guideSceneVideoProcessingJob;
        }
    }


