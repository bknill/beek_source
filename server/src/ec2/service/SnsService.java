package ec2.service;

import com.amazonaws.services.sns.model.SubscribeRequest;
import com.amazonaws.util.EC2MetadataUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class SnsService extends AmazonService {
    private boolean subscribe = false;

    @Value("#{buildProperties.responseTopicArn}")
    private String topicArn;

    public void subscribe() {
        String ip = EC2MetadataUtils.getData("/latest/meta-data/public-ipv4");

        //String url = "http://107.22.237.77";
        String subURL = "/response";
        String port = ":9081";
        String address = "http://" + ip + port + subURL;
        try{
            SubscribeRequest subscribeRequest = new SubscribeRequest(topicArn, "http", address);
            snsClient.subscribe(subscribeRequest);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public String getTopicArn() {
        return topicArn;
    }

    public boolean isSubscribe() {
        return subscribe;
    }

    public void setSubscribe(boolean subscribe) {
        this.subscribe = subscribe;
    }
}
