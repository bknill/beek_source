package ec2.service;

import co.beek.pano.model.dao.entities.Scene;
import co.beek.pano.model.dao.entities.ScenePanoTask;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import ec2.model.Job;
import ec2.model.Job.JobStatus;
import ec2.model.Job.JobType;
import ec2.model.Ping;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TranscodingService extends EC2Service {
    public static String S3_BUCKET_NAME = "testing-fo";
    public static String S3_RAW_DIR_NAME = "raw";

    @Value("#{buildProperties.jobsQueueURL}")
    private String jobsQueueURL;

    private Map<Instance, String> runningJobs = new HashMap<Instance, String>();

    public void start() {
        String instanceId = "i-31c9b1b1";
        DescribeInstancesResult describeInstancesResult =
                ec2Client.describeInstances(new DescribeInstancesRequest().withInstanceIds(instanceId));

        List<Instance> instances = new ArrayList<Instance>();
        for (Reservation reservation : describeInstancesResult.getReservations()) {
            instances.addAll(reservation.getInstances());
        }
        if (instances.size() > 0) {
            Instance instance = instances.get(0);
            startInstance(instance);
            boolean isEC2Running = waitForInstanceToStart(instance);
            System.out.println("state = " + instance.getState().getName());
            if (isEC2Running) {
                //TODO get pano job or transcodin job id from DB and put in map
                runningJobs.put(instance, "1");
            } else {

            }
            System.out.println("returning");
        }
    }

    public static void main(String[] args) throws IOException {
        TranscodingService test = new TranscodingService();
        //test.start();
    }

    @Async
    public void handlePing(Instance instance,ScenePanoTask scenePanoTask) {


        if (instance == null) return;

        try {
            System.out.println("got alive response");
            File videoFile = new File(TranscodingService.class.getResource("bunny.mp4").toURI());
            System.out.println("uploading video to s3 starts");
//            Thread.sleep(120000);
            s3Client.putObject(new PutObjectRequest(
                    S3_BUCKET_NAME,
                    S3_RAW_DIR_NAME + "/" + videoFile.getName(),
                    videoFile));

            Job job = new Job();
            job.setJobStatus(JobStatus.RAW);
            //TODO get job from DB
            job.setFileName(videoFile.getName());
            job.setJobType(JobType.TRANSCODING);

            ObjectMapper mapper = new ObjectMapper();
            String jobJSON = mapper.writeValueAsString(job);

            System.out.println("sending job");
            sqsClient.sendMessage(jobsQueueURL, jobJSON);

        } catch (Exception e) {
            if (instance != null) {
                stopInstance(instance);
                runningJobs.remove(instance);
            }
            e.printStackTrace();
        }
    }

    @Async
    public void handleJobResponse(Job job, Instance instance, ScenePanoTask scenePanoTask) {
        String instanceId = job.getEc2InstanceId();
        System.out.println(job.getMessage());

        //pano job or encoding job
        String jobId = null;
        for (Map.Entry<Instance, String> entries : runningJobs.entrySet()) {
            if (entries.getKey().getInstanceId().equals(instanceId)) {
                instance = entries.getKey();
                jobId = entries.getValue();
                break;
            }
        }

        System.out.println("do something with job = " + jobId);
        if (job.getJobStatus() == JobStatus.DONE) {
            //TODO update DB
            System.out.println("done");
        } else if (job.getJobStatus() == JobStatus.ERROR) {
            //TODO update DB
            System.out.println("error");
        }
        if (instance != null) {
            stopInstance(instance);
            runningJobs.remove(instance);
        }
    }


}
