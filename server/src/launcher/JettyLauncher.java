package launcher;

import java.io.File;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;
import java.util.regex.*;

import co.beek.Constants;

public class JettyLauncher {

	public static void main(String[] args) throws Exception {
		try {
			System.out.println("JettyLauncher.main(" + args.toString() + ")");

			File tempDir = new File(Constants.TEMP_DIR_PATH);
			if (!tempDir.exists())
				tempDir.mkdir();

			WebAppContext webAppContext = new WebAppContext();
			webAppContext.setDescriptor(webAppContext + "/WEB-INF/web.xml");
			webAppContext.setResourceBase(".");
			webAppContext.setContextPath("/");
			webAppContext.setAttribute("webContext", webAppContext);
			webAppContext.setAttribute("WebInfIncludeJarPattern", ".*/.*jsp-api-[^/]\\.jar$|./.*jsp-[^/]\\.jar$|./.*taglibs[^/]*\\.jar$");

			webAppContext.setMaxFormContentSize(20000000);

			Server server = new Server(9081);
			server.setHandler(webAppContext);
			server.setStopAtShutdown(true);
			server.setAttribute("org.eclipse.jetty.server.Request.maxFormContentSize", -1);
			server.setAttribute("org.eclipse.jetty.server.webapp.WebInfIncludeJarPattern", ".*/.*jsp-api-[^/]\\.jar$|./.*jsp-[^/]\\.jar$|./.*taglibs[^/]*\\.jar$");
			server.start();

			server.join();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
